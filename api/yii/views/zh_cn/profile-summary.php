<!-- start profiling summary -->
<table class="yiiLog" width="100%" cellpadding="2" style="background:black;color:white; color: black;">
	<tr>
		<th style="background:black;color:white;" colspan="6">
			程序概要分析 - 报告概述
			(时间: <?php echo sprintf('%0.5f',Yii::getLogger()->getExecutionTime()); ?> 秒,
			内存: <?php echo number_format(Yii::getLogger()->getMemoryUsage()/1024); ?>KB)
		</th>
	</tr>
	<tr style="background-color: #DFFFE0;">
		<th>程序</th>
		<th>数量</th>
		<th>总计 (秒)</th>
		<th>平均 (秒)</th>
		<th>最小 (秒)</th>
		<th>最大 (秒)</th>
	</tr>
<?php

$data = array_reverse($data);

foreach($data as $index=>$entry)
{
	$color = '#DFFFE0';

	// preg_match("#.*query\((.*)\)#", $entry[0], $message);
	// $message='<pre>'.CHtml::encode(wordwrap($message[1])).'</pre>';

	$proc=CHtml::encode($entry[0]);
	$min=sprintf('%0.5f',$entry[2]);
	$max=sprintf('%0.5f',$entry[3]);
	$total=sprintf('%0.5f',$entry[4]);
	$average=sprintf('%0.5f',$entry[4]/$entry[1]);

	echo <<<EOD
	<tr style="background:#DFFFE0">
		<td>{$proc}</td>
		<td align="center">{$entry[1]}</td>
		<td align="center">{$total}</td>
		<td align="center">{$average}</td>
		<td align="center">{$min}</td>
		<td align="center">{$max}</td>
	</tr>
EOD;
}
?>
</table>
<!-- end of profiling summary -->