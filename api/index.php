<?php

// change the following paths if necessary
$yii=dirname(__FILE__).'/yii/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';
date_default_timezone_set('PRC');

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG', false);
defined('DEBUG_TOOL') or define('DEBUG_TOOL', true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

defined('DS') or define('DS', DIRECTORY_SEPARATOR);

require_once($yii);
Yii::createWebApplication($config)->run();

