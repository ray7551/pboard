/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50535
Source Host           : localhost:3306
Source Database       : pboard

Target Server Type    : MYSQL
Target Server Version : 50535
File Encoding         : 65001

Date: 2014-05-05 22:25:18
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for pb_board
-- ----------------------------
DROP TABLE IF EXISTS `pb_board`;
CREATE TABLE `pb_board` (
  `id` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `creator_id` int(9) unsigned NOT NULL COMMENT 'creator user id',
  `title` varchar(32) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pb_board
-- ----------------------------
INSERT INTO `pb_board` VALUES ('1', '1', 'Board#1', 'The first Board.');
INSERT INTO `pb_board` VALUES ('2', '1', 'Board#2', 'Board2');
INSERT INTO `pb_board` VALUES ('3', '1', 'Board#3', 'baord3');
INSERT INTO `pb_board` VALUES ('4', '1', 'Board#4', 'board4');
INSERT INTO `pb_board` VALUES ('5', '1', 'Board#5', 'This board belongs to PBoard project.');
INSERT INTO `pb_board` VALUES ('6', '2', 'Board#6', 'This board belongs to PBoard project.Created by Ray.');
INSERT INTO `pb_board` VALUES ('7', '1', 'Board#7', 'Board7');
INSERT INTO `pb_board` VALUES ('8', '1', 'Board#8', 'baord8');
INSERT INTO `pb_board` VALUES ('9', '1', 'Board#9', 'board9');

-- ----------------------------
-- Table structure for pb_card
-- ----------------------------
DROP TABLE IF EXISTS `pb_card`;
CREATE TABLE `pb_card` (
  `id` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `bid` int(9) unsigned NOT NULL,
  `lid` int(9) unsigned NOT NULL,
  `creator_id` int(9) unsigned NOT NULL,
  `title` varchar(16) DEFAULT NULL,
  `detail` tinytext,
  `cstatus` enum('0','1','2','3','4','5') NOT NULL DEFAULT '0' COMMENT '0：待认领，1：进行中，2：待验证，3：已关闭，4：已删除，5：已归档',
  `prev_id` int(9) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `board_id` (`bid`) USING BTREE,
  KEY `card_title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pb_card
-- ----------------------------
INSERT INTO `pb_card` VALUES ('1', '1', '1', '1', 'Card#1', '#This is the First Card', '0', '0');
INSERT INTO `pb_card` VALUES ('2', '1', '1', '1', 'Card#2', '#This is the Second Card', '0', '1');
INSERT INTO `pb_card` VALUES ('3', '2', '2', '2', 'Card#3', '#This is the 3rdCard', '0', '0');
INSERT INTO `pb_card` VALUES ('4', '1', '3', '2', 'Card#4', '#4', '0', '0');
INSERT INTO `pb_card` VALUES ('5', '1', '3', '1', 'Card#5', '555', '3', '4');
INSERT INTO `pb_card` VALUES ('6', '1', '1', '1', 'Card#6', 'a test card as a deleted one', '2', '2');
INSERT INTO `pb_card` VALUES ('7', '1', '1', '1', 'Card#7', 'a test card as a archivedone', '3', '6');
INSERT INTO `pb_card` VALUES ('8', '1', '1', '1', 'Card#8', '##8888888\\n- eesf\\n- ese\\n- List Test', '0', '7');

-- ----------------------------
-- Table structure for pb_comment
-- ----------------------------
DROP TABLE IF EXISTS `pb_comment`;
CREATE TABLE `pb_comment` (
  `id` int(9) NOT NULL,
  `uid` int(9) NOT NULL,
  `cid` int(9) NOT NULL COMMENT 'card id',
  `text` tinytext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pb_comment
-- ----------------------------

-- ----------------------------
-- Table structure for pb_doc
-- ----------------------------
DROP TABLE IF EXISTS `pb_doc`;
CREATE TABLE `pb_doc` (
  `id` int(9) NOT NULL,
  `bid` int(9) NOT NULL,
  `creator_id` int(9) NOT NULL,
  `text` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pb_doc
-- ----------------------------

-- ----------------------------
-- Table structure for pb_follow
-- ----------------------------
DROP TABLE IF EXISTS `pb_follow`;
CREATE TABLE `pb_follow` (
  `uid` int(9) unsigned NOT NULL,
  `cid` int(9) unsigned NOT NULL COMMENT 'card id',
  PRIMARY KEY (`uid`,`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pb_follow
-- ----------------------------

-- ----------------------------
-- Table structure for pb_issue
-- ----------------------------
DROP TABLE IF EXISTS `pb_issue`;
CREATE TABLE `pb_issue` (
  `id` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `bid` int(9) NOT NULL COMMENT 'board id',
  `cid` int(9) NOT NULL COMMENT 'card id',
  `log` varchar(255) NOT NULL,
  `diff` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pb_issue
-- ----------------------------

-- ----------------------------
-- Table structure for pb_list
-- ----------------------------
DROP TABLE IF EXISTS `pb_list`;
CREATE TABLE `pb_list` (
  `id` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `bid` int(9) unsigned NOT NULL COMMENT 'board id',
  `creator_id` int(9) unsigned NOT NULL,
  `title` varchar(64) DEFAULT NULL,
  `prev_id` tinyint(4) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pb_list
-- ----------------------------
INSERT INTO `pb_list` VALUES ('1', '1', '0', 'List#1', '0');
INSERT INTO `pb_list` VALUES ('2', '2', '1', 'List#2', '0');
INSERT INTO `pb_list` VALUES ('3', '1', '1', 'List#3', '1');
INSERT INTO `pb_list` VALUES ('4', '1', '1', 'List#4', '3');
INSERT INTO `pb_list` VALUES ('5', '1', '1', 'List#5', '4');
INSERT INTO `pb_list` VALUES ('6', '1', '0', 'aa', '5');
INSERT INTO `pb_list` VALUES ('10', '1', '0', 'sef', '6');
INSERT INTO `pb_list` VALUES ('11', '1', '0', 'ttt', '10');
INSERT INTO `pb_list` VALUES ('12', '1', '0', 'tttsssssssssssssssssss', '11');
INSERT INTO `pb_list` VALUES ('13', '1', '0', 'se', '12');
INSERT INTO `pb_list` VALUES ('14', '1', '0', 'se', '13');
INSERT INTO `pb_list` VALUES ('15', '1', '0', 'sss', '14');
INSERT INTO `pb_list` VALUES ('16', '1', '0', 'sdf', '15');
INSERT INTO `pb_list` VALUES ('17', '2', '0', 'xg', '2');
INSERT INTO `pb_list` VALUES ('18', '2', '0', 'b2l3', '17');
INSERT INTO `pb_list` VALUES ('19', '6', '0', 'test', '0');
INSERT INTO `pb_list` VALUES ('20', '6', '0', 'test1', '19');
INSERT INTO `pb_list` VALUES ('21', '6', '0', 'test2', '20');
INSERT INTO `pb_list` VALUES ('22', '6', '0', 'test3', '21');
INSERT INTO `pb_list` VALUES ('23', '6', '0', 'test3', '22');
INSERT INTO `pb_list` VALUES ('24', '4', '0', 'test', '0');
INSERT INTO `pb_list` VALUES ('25', '4', '0', 'test1', '24');
INSERT INTO `pb_list` VALUES ('26', '8', '0', 'B8L1', '0');
INSERT INTO `pb_list` VALUES ('27', '1', '0', 'tes', '16');
INSERT INTO `pb_list` VALUES ('28', '8', '0', 'B8L2', '26');
INSERT INTO `pb_list` VALUES ('29', '8', '0', 'B8L3', '28');
INSERT INTO `pb_list` VALUES ('30', '8', '0', 'B8L4', '29');
INSERT INTO `pb_list` VALUES ('31', '8', '0', 'B8L5', '30');
INSERT INTO `pb_list` VALUES ('32', '8', '0', 'B8L6', '31');
INSERT INTO `pb_list` VALUES ('33', '8', '0', 'B8L7', '32');
INSERT INTO `pb_list` VALUES ('34', '8', '0', 'B8L8', '33');
INSERT INTO `pb_list` VALUES ('35', '8', '0', 'B8L9', '34');
INSERT INTO `pb_list` VALUES ('36', '8', '0', 'B8L10', '35');
INSERT INTO `pb_list` VALUES ('37', '1', '0', 'test', '27');
INSERT INTO `pb_list` VALUES ('38', '1', '0', 'tes', '37');
INSERT INTO `pb_list` VALUES ('39', '1', '0', 'test', '38');
INSERT INTO `pb_list` VALUES ('40', '1', '0', 'test', '39');
INSERT INTO `pb_list` VALUES ('41', '1', '0', 'te', '40');
INSERT INTO `pb_list` VALUES ('42', '1', '0', 'tt', '41');
INSERT INTO `pb_list` VALUES ('43', '1', '0', 'tes', '42');
INSERT INTO `pb_list` VALUES ('44', '1', '0', 'te', '43');
INSERT INTO `pb_list` VALUES ('45', '7', '0', 'test', '0');
INSERT INTO `pb_list` VALUES ('46', '7', '0', 'B7L2', '45');
INSERT INTO `pb_list` VALUES ('47', '1', '0', 'aaa', '44');
INSERT INTO `pb_list` VALUES ('48', '7', '0', 'B6L3', '46');
INSERT INTO `pb_list` VALUES ('49', '9', '0', 'B9L1', '0');
INSERT INTO `pb_list` VALUES ('50', '9', '0', 'B9L2', '49');
INSERT INTO `pb_list` VALUES ('51', '9', '0', 'B8L3', '50');
INSERT INTO `pb_list` VALUES ('52', '1', '0', 'test', '47');

-- ----------------------------
-- Table structure for pb_participate
-- ----------------------------
DROP TABLE IF EXISTS `pb_participate`;
CREATE TABLE `pb_participate` (
  `bid` int(9) unsigned NOT NULL COMMENT 'board id',
  `uid` int(9) unsigned NOT NULL COMMENT 'user id',
  PRIMARY KEY (`bid`,`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pb_participate
-- ----------------------------

-- ----------------------------
-- Table structure for pb_user
-- ----------------------------
DROP TABLE IF EXISTS `pb_user`;
CREATE TABLE `pb_user` (
  `id` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pb_user
-- ----------------------------
INSERT INTO `pb_user` VALUES ('1', 'admin');
INSERT INTO `pb_user` VALUES ('2', 'Ray');
INSERT INTO `pb_user` VALUES ('3', 'Creager');
INSERT INTO `pb_user` VALUES ('4', 'Denis');
INSERT INTO `pb_user` VALUES ('5', 'Alice');
INSERT INTO `pb_user` VALUES ('6', 'Bob');
INSERT INTO `pb_user` VALUES ('7', 'Carlo');
INSERT INTO `pb_user` VALUES ('8', 'Dave');
INSERT INTO `pb_user` VALUES ('9', 'Eve');
INSERT INTO `pb_user` VALUES ('10', 'Todd');
