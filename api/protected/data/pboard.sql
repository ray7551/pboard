CREATE TABLE `board` (
`id` int(9) UNSIGNED NOT NULL AUTO_INCREMENT,
`creator_id` int(9) UNSIGNED NOT NULL COMMENT 'creator user id',
`title` varchar(32) NULL DEFAULT NULL,
`desc` varchar(255) NULL DEFAULT NULL,
PRIMARY KEY (`id`) 
);

CREATE TABLE `user` (
`id` int(9) UNSIGNED NOT NULL AUTO_INCREMENT,
`name` varchar(64) NOT NULL,
PRIMARY KEY (`id`) 
);

CREATE TABLE `participate` (
`bid` int(9) UNSIGNED NOT NULL COMMENT 'board id',
`uid` int(9) UNSIGNED NOT NULL COMMENT 'user id',
PRIMARY KEY (`bid`, `uid`) 
);

CREATE TABLE `list` (
`id` int(9) UNSIGNED NOT NULL AUTO_INCREMENT,
`bid` int(9) UNSIGNED NOT NULL COMMENT 'board id',
`title` varchar(64) NULL DEFAULT NULL,
PRIMARY KEY (`id`) 
);

CREATE TABLE `card` (
`id` int(9) UNSIGNED NOT NULL AUTO_INCREMENT,
`bid` int(9) UNSIGNED NOT NULL,
`lid` int(9) UNSIGNED NOT NULL,
`title` varchar(64) NULL DEFAULT NULL,
`detail` tinytext NULL,
PRIMARY KEY (`id`) 
);

CREATE TABLE `follow` (
`uid` int(9) UNSIGNED NOT NULL,
`cid` int(9) UNSIGNED NOT NULL COMMENT 'card id',
PRIMARY KEY (`uid`, `cid`) 
);

CREATE TABLE `comment` (
`id` int(9) NOT NULL,
`uid` int(9) NOT NULL,
`cid` int(9) NOT NULL COMMENT 'card id',
`text` tinytext NOT NULL,
PRIMARY KEY (`id`) 
);

CREATE TABLE `issue` (
`id` int(9) UNSIGNED NOT NULL AUTO_INCREMENT,
`bid` int(9) NOT NULL COMMENT 'board id',
`cid` int(9) NOT NULL COMMENT 'card id',
`log` varchar(255) NOT NULL,
`diff` text NULL,
PRIMARY KEY (`id`) 
);

CREATE TABLE `doc` (
`id` int(9) NOT NULL,
`bid` int(9) NOT NULL,
`creator_id` int(9) NOT NULL,
`text` text NULL,
PRIMARY KEY (`id`) 
);

