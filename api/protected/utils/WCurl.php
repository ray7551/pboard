<?php
/**
 * A curl warpper.
 */
class WCurl{
	private $url;
	private $ch;
	private $response;

	public function __construct($url){
		$this->url = $url;
		$this->ch = curl_init();
		curl_setopt($this->ch, CURLOPT_URL, $url);
	}

	public function post($data){
		curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($this->ch, CURLOPT_POST, 1);
		curl_setopt($this->ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data)
		));
		curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
		$this->response = curl_exec($this->ch);
		return $this->response;
	}
}
