<?php
class ListsController extends Controller
{
	public function actionCreate() {
		$list = new Lists;
		$listData = $_POST;

		$list->bid = $listData['bid'];
		$list->title = $listData['title'];
		$list->creator_id = $listData['creator_id'];
		$list->prev_id = $listData['prev_id'];

		if ($list->save()) {
			$listData['id'] = $list->id;
			$this->broadcast($listData, 'list_create');
			$this->_sendResponse(201, $list->id);
		} else {
			$this->_sendResponse(449, 'Could not Create Item, please refresh or retry later.');
		}
	}

	public function actionUpdate() {
		$listData = Yii::app()->request->getRestParams();
		$id = $this->getUrlId(Yii::app()->request->getUrl());
		$bid = $listData['bid'];
		$listData['id'] = $id;
		$list = Lists::model();

		$transaction = $list->dbConnection->beginTransaction();
		try {
			$oldPrevId = $list->findByPk($id)->prev_id;
			$newPrevId = $listData['prev_id'];
			$orderChanged = $oldPrevId !== $listData['prev_id'];
			if ($orderChanged) {
				$newNextList = Lists::model()->find(array(
					'select' => 'id',
					'condition' => 'prev_id=:prev_id and bid=:bid',
					'params' => array(':prev_id'=>$newPrevId, ':bid'=>$bid),
				));
				$oldNextList = Lists::model()->find(array(
					'select' => 'id',
					'condition' => 'prev_id=:prev_id',
					'params' => array(':prev_id'=>$id),
				));
				// update new next list's prev_id
				if(isset($newNextList->id)){
					Lists::model()->updateByPk($newNextList->id, array('prev_id'=>$id));
				}
				// update old next list's prev_id
				if(isset($oldNextList->id)){
					Lists::model()->updateByPk($oldNextList->id, array('prev_id'=>$oldPrevId));
				}
			}
			$list->updateByPk($id, $listData);

			$transaction->commit();


			$boardData = Cards::model()->getByBid($bid);
			$boardData['bid'] = $bid;
			$boardData['type'] = 'board';
			$this->broadcast($boardData, 'list_update');
			$this->_sendResponse(200);

		} catch(Exception $e) {
			$transaction->rollBack();
			$this->_sendResponse(412, 'Could not Update Item,  please refresh or retry later.' . $e->getMessage() . $list->getErrorStr());
		}

	}

	public function actionDelete() {
		$id = $this->getUrlId(Yii::app()->request->getUrl());
		$model = Lists::model();

		$transaction = $model->dbConnection->beginTransaction();
		try {

			$list = $model->findByPk($id);
			$bid = $list->bid;

			// 1. update next list's prev_id
			$criteria = new CDbCriteria;
			$criteria->condition = 'id=:id or prev_id=:prev_id';
			$criteria->params = array(
				':prev_id' => $list->id,
				':id' => $list->prev_id
			);
			$criteria->index = 'id';
			$boardData = Lists::model()->findAll($criteria);
			if (count($boardData) >= 1) {
				if (isset($boardData[$list->prev_id])) {
					$prev_id = $list->prev_id;
				} else {
					$prev_id = 0;
				}

				foreach ($boardData as $key => $value) {
					if ($value->prev_id == $list->id) {
						$nextId = $value['id'];
					}
				}
				if (isset($nextId)) {
					$nextList = Lists::model();
					$nextList->updateByPk($nextId, array(
						'prev_id' => $prev_id
					));
				}
			}

			// 2. delete list
			$list->delete();

			// 3. delete related cards
			$relatedCards = Cards::model()->findAll('lid=:lid', array(
				':lid' => $id
			));
			foreach ($relatedCards as $relatedCard) {
				$relatedCard->delete();
			}

			$transaction->commit();
		}
		catch(Exception $e) {
			$transaction->rollBack();
			$this->_sendResponse(412, 'Could not Delete Item,  please refresh or retry later.' . $e->getMessage() . $list->getErrorStr());
		}

		$boardData = Cards::model()->getByBid($bid);
		$boardData['bid'] = $bid;
		$boardData['type'] = 'board';
		$this->broadcast($boardData, 'list_delete');
		$this->_sendResponse(200);
	}

}
