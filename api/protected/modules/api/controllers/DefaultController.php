<?php

class DefaultController extends Controller
{
	public function actionIndex()
	{
		echo 'This is '.Yii::app()->name.' api.';
		//$this->render('index');
	}

	// A preflight request is basically an OPTIONS request to ask for permission to use cross-domain features.
	// https://gist.github.com/sourcec0de/4237402
	public function actionPreflight() {
		$content_type = 'application/json';
		$status = 200;

		// set the status
		$status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
		header($status_header);
		// if you add this Access-Control-Allow-Origin header in your server config,
		// comment it in case of sending duplicated header, which may cause error in bowser
		header("Access-Control-Allow-Origin: *");
		// header("Access-Control-Allow-Headers: Authorization");
		// header('Content-type: ' . $content_type);
	}
}