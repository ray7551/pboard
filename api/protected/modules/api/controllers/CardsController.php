<?php
class CardsController extends Controller
{

	public function actionList($bid = '', $lid = '') {
		if (is_numeric($lid)) {
			$lid = intval($lid);
			$boardData = Cards::model()->getBylid($lid);
            if (!empty($boardData)) {
                $this->jsonSuccessReturn($boardData);
            }
		}

		if (is_numeric($bid)) {
			$bid = intval($bid);
			$boardData = Cards::model()->getByBid($bid);
			$this->jsonSuccessReturn($boardData);
		} else {
			$boardData = array();
			$this->jsonFailReturn('INVALID_PARAM', 'Need to specify a board id or list id', $data = array());
		}
	}

	public function actionCreate() {
		$card = new Cards;
		$cardData = $_POST;

		$card->bid = $cardData['bid'];
		$card->lid = $cardData['lid'];
		$card->title = $cardData['title'];
		$card->creator_id = $cardData['creator_id'];
		$card->prev_id = $cardData['prev_id'];
		$card->cstatus = Cards::CSTATUS_TOCLAIME;

		if ($card->save()) {
			$cardData['id'] = $card->id;
			$this->broadcast($cardData, 'card_create');
			$this->_sendResponse(201, $card->id);
		} else {
			$this->_sendResponse(449, 'Could not Delete Item, please refresh or retry later.'.$card->getErrorStr());
		}
	}

	/**
	 * update a card, and update related card's prev_id
	 */
	public function actionUpdate() {
		$cardData = Yii::app()->request->getRestParams();
		$id = $this->getUrlId(Yii::app()->request->getUrl());
		$bid = $cardData['bid'];
		$cardData['id'] = $id;
		$model = Cards::model();

		$transaction = $model->dbConnection->beginTransaction();
		try {
			$card = $model->findByPk($id);
			$oldPrevId = $card->prev_id;
			$newPrevId = $cardData['prev_id'];
			$orderChanged = ($oldPrevId !== $cardData['prev_id']);
			$listChanged = ($card->lid !== $cardData['lid']);
			$newListId = $listChanged ? $cardData['lid'] : $card->lid;
			if ($orderChanged || $listChanged) {
				$newNextCard = Cards::model()->find(array(
					'select' => 'id',
					'condition' => 'prev_id=:prev_id and lid=:lid',
					'params' => array(':prev_id'=>$newPrevId, ':lid'=>$newListId),
				));
				$oldNextCard = Cards::model()->find(array(
					'select' => 'id',
					'condition' => 'prev_id=:prev_id',
					'params' => array(':prev_id'=>$id),
				));
				// update new next list's prev_id
				if(isset($newNextCard->id)){
					Cards::model()->updateByPk($newNextCard->id, array('prev_id'=>$id));
				}
				// update old next list's prev_id
				if(isset($oldNextCard->id)){
					Cards::model()->updateByPk($oldNextCard->id, array('prev_id'=>$oldPrevId));
				}
			}

			$card->updateByPk($id, $cardData);

			$transaction->commit();


			$boardData = Cards::model()->getByBid($bid);
			$boardData['bid'] = $bid;
			$boardData['type'] = 'board';
			$this->broadcast($boardData, 'card_update');
			$this->_sendResponse(200);

		} catch(Exception $e) {
			$transaction->rollBack();
			$this->_sendResponse(412, 'Could not Update Card,  please refresh or retry later.' . $e->getMessage() . $list->getErrorStr());
		}
	}

	/**
	 * delete a card, and update next card's prev_id
	 * @todo: transaction
	 * @todo: update next card's prev_id before delete
	 */
	public function actionDelete() {
		$id = $this->getUrlId(Yii::app()->request->getUrl());

		$model = Cards::model();
		$transaction = $model->dbConnection->beginTransaction();
		try{
			$card = $model->findByPk($id);
			$bid = $card->bid;

			// 1. update next card's prev_id
			$criteria = new CDbCriteria;
			$criteria->condition = 'id=:id or prev_id=:prev_id';
			$criteria->params = array(
				':prev_id' => $card->id,
				':id' => $card->prev_id
			);
			$criteria->index = 'id';
			$listData = Cards::model()->findAll($criteria);
			if (count($listData) >= 1) {
				if (isset($listData[$card->prev_id])) {
					$prev_id = $card->prev_id;
				} else {
					$prev_id = 0;
				}

				foreach ($listData as $value) {
					if ($card->id == $value->prev_id) {
						$nextId = $value['id'];
					}
				}
				if (isset($nextId)) {
					$nextList = Cards::model();
					$nextList->updateByPk($nextId, array(
						'prev_id' => $prev_id
					));
				}
			}

			// 2. delete card
			$card->delete();

			$transaction->commit();
		}catch (Eception $e){
			$transaction->rollback();
			$this->_sendResponse(412, 'Could not Delete Item,  please refresh or retry later.' . $e->getMessage() . $list->getErrorStr());
		}

		$listData = Cards::model()->getByBid($bid);
		$listData['bid'] = $bid;
		$listData['type'] = 'board';
		$this->broadcast($listData, 'card_delete');
		$this->_sendResponse(200);

    }
}
