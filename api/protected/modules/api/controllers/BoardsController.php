<?php
class BoardsController extends Controller
{

	/**
	 * @todo return current user's board
	 */
	public function actionList(){
		$boardInfo = Boards::model()->getAll();
		$this->jsonSuccessReturn($boardInfo);
	}

	public function actionShow($id){
		$boardInfo = Boards::model()->findRecordByPk($id);
		$this->jsonSuccessReturn($boardInfo);
	}

	public function actionUpdate(){
		$boardData = Yii::app()->request->getRestParams();
		$id = $this->getUrlId(Yii::app()->request->getUrl());
		$boardData['bid'] = $id;
		$boardData['type'] = 'board';
		$board = Boards::model();
		if($board->updateByPk($id, array('color'=>$boardData['color']))){
			$this->broadcast($boardData, 'board_update');
			$this->_sendResponse(200);
		}else{
			$this->_sendResponse(412, 'Could not Update Item,  please refresh or retry later.' . $board->getErrorStr());
		}
	}

	public function actionCreate() {
		$boardData = Yii::app()->request->getRestParams();
		$board = new Boards;
		$board->title = $boardData['title'];
		$board->desc = $boardData['desc'];
		$board->color = $boardData['color'];
		$board->creator_id = $boardData['creator_id'];
		if($boardData['svn'] == 'true'){
			$repoName = strtolower($boardData['title']);
			$this->addRepo($repoName);
			$repo = array();
			$repo = $this->getRepo($repoName);
			if(!empty($repo)){
				echo 'added repo!';
				$board->repo_url = $repo['repo_url'];
				$board->viewvc_url = $repo['viewvc_url'];
			}
		}

		if($board->save()){
			$this->_sendResponse(201, $board->id);
		}else{
			$this->_sendResponse(449, 'Could not Create Item, please refresh or retry later.');
		}
	}

	protected function getRepo($repoName){
		$csvnApiBase = 'http://admin:hongyanredrock@localhost:3343/csvn/api/1/';
		$csvnApiRepo = $csvnApiBase.'repository?format=json';
		$json = file_get_contents($csvnApiRepo);
		$repos = json_decode($json);
		foreach ($repos->repositories as $repo) {
			if($repo->name == $repoName){
				return array(
					'repo_url' => $repo->svnUrl,
					'viewvc_url' => $repo->viewvcUrl
				);
			}
		}
		return '';
	}

	protected function addRepo($repoName){
		$csvnApiBase = 'http://admin:hongyanredrock@localhost:3343/csvn/api/1/';
		$csvnApiRepo = $csvnApiBase.'repository?format=json';

		Yii::import('application.utils.WCurl');
		$data = array(
			'name' => $repoName,
			'applyStandardLayout' => false,
			'applyTemplateId' => 2
		);
		$dataJson = json_encode($data);
		$ch = new WCurl($csvnApiRepo);
		$response = $ch->post($dataJson);
		return $response;
	}

}
