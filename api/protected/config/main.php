<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'language' => 'zh_cn',
	'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'name' => 'PBoard',

	// preloading 'log' component
	'preload' => array(
		'log'
	) ,

	// autoloading model and component classes
	'import' => array(
		'application.models.*',
		'application.components.*',
	) ,

	'modules' => array(

		// uncomment the following to enable the Gii tool

		'api' => array(
			'class' => ('application.modules.api.ApiModule') ,
		)
	) ,

	// application components
	'components' => array(
		'user' => array(

			// enable cookie-based authentication
			'allowAutoLogin' => true,
		) ,
		'WebApplication' => array(

			//'viewpath'=> dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'../app/view'

		) ,

		// 'assetManager'=>array(
		// 	'basePath' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'../assets',
		// 	//'baseUrl'=>'/app/'
		// ),
		// uncomment the following to enable URLs in path-format

		'urlManager' => array(
			'urlFormat' => 'path',
			'showScriptName' => false,
			 // no index.php
			'rules' => array(
				array(
					'api/<controller>/list',
					'pattern' => '<controller:\w+>',
					'verb' => 'GET'
				) ,
				array(
					'api/<controller>/show',
					'pattern' => '<controller:\w+>/<id\d+>',
					'verb' => 'GET'
				) ,
				array(
					'api/<controller>/create',
					'pattern' => '<controller>',
					'verb' => 'POST'
				) ,
				array(
					'api/<controller>/update',
					'pattern' => '<controller>/<id\d+>',
					'verb' => 'PUT'
				) ,
				array(
					'api/<controller>/delete',
					'pattern' => '<controller>/<id\d+>',
					'verb' => 'DELETE'
				) ,
				array(
					'api/<controller>/test',
					'pattern' => '<controller>/test'
				) ,
				array(
					'api/default/preflight',
					'pattern' => '(.*)/?(.*)',
					'verb' => 'OPTIONS'
				)

				// '<controller:\w+>/'=>'<controller>/index',
				// '<controller:\w+>/<id:\d+>'=>'<controller>/show',
				// '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				// '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
				//'app/index'=>'site/index',
				//'app/'=>''
				// 'api/<controller:\w+>/<id:\d+>'=>'<controller>/view',
				// 'api/<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				// array('api/cards', 'pattern'=>'api/cards.*', 'verb'=>'GET'),
				// '/api/<controller:\w+>/<id:\d+>' => '/api/<controller>/view',
				// '/api/<controller:\w+>/<action:\w+>/<id:\d+>' => '/api/<controller>/<action>',
				// '/api/<controller:\w+>/<action:\w+>\?<params:.*>' => '/api/<controller>/<action>?<params>',
				// '/api/<controller:\w+>/<action:\w+>' => '/api/<controller>/<action>',

			) ,
		) ,

		/*'user'=>array(
			 // 启用 cookie-based 验证
			 'allowAutoLogin'=>true,
			 // 设置需要验证时用户被转到的 url
			 // 使用 null 出现 403 HTTP 错误
			 'loginUrl'=>null,
			 // 设置一个类的名字，
			 // 这个类扩展自 CWebUser 并且保存在
			 //protected/components/<classname>中。
			 //查阅www.yiiframework.com/doc/cookbook/60/
			 'class' => 'WebUser',
		),*/
		'db' => array(
			//'connectionString' => 'mysql:host=sqld.duapp.com;port=4050;dbname=hUoUiIfqbpJXPCGtCQcF',
			'connectionString' => 'mysql:host=localhost;dbname=pboard',
			'emulatePrepare' => true,
			'username' => 'redrock',
			'password' => 'hongyanredrock',
			'charset' => 'utf8',
			'tablePrefix' => 'pb_',
		) ,

		'errorHandler' => array(
			'errorAction' => 'site/error',
		) ,
		'log' => array(
			'class' => 'CLogRouter',
			'routes' => array(
				array(
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning',
				) ,

				// uncomment the following to show log messages on web pages

				// array(
				// 	'class'=>'CFileLogRoute',
				// 	'levels'=>'error,trace,info,warning',
				// 	//'filter'=>'CLogFilter',
				// 	'categories'=>'system.db.CDbCommand.query',
				// 	'logFile'=>'sql.log'
				// ),


			) ,
		) ,
	) ,

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params' => array(

		// this is used in contact page
		'adminEmail' => 'ray7551@gmail.com',
	) ,
);

