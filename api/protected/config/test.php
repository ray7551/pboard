<?php

return CMap::mergeArray(
	require(dirname(__FILE__).'/main.php'),
	array(
		'modules'=>array(
			// uncomment the following to enable the Gii tool

			'gii'=>array(
				'class'=>'system.gii.GiiModule',
				'password'=>'3140',
				// If removed, Gii defaults to localhost only. Edit carefully to taste.
				'ipFilters'=>array('127.0.0.1','::1'),
			),

		),
		'components'=>array(
			'fixture'=>array(
				'class'=>'system.test.CDbFixtureManager',
			),

			'db'=>array(
				'connectionString' => 'mysql:host=localhost;dbname=pboard',
				'emulatePrepare' => true,
				'username' => 'root',
				'password' => '3140@mysql',
				'charset' => 'utf8',
				'enableProfiling' => true,
				'enableParamLogging' => true,
			),

			'log'=>array(
				'class'=>'CLogRouter',
				'routes'=>array(
					// array(
					// 	'class'=>'ext.yii-debug-toolbar.YiiDebugToolbarRoute',
					// 	// Access is restricted by default to the localhost
					// 	'ipFilters'=>array('127.0.0.1','192.168.1.*'),
					// ),

					array(
						'class'=>'CFileLogRoute',
						'levels'=>'error,trace,info,warning,profile',
						//'filter'=>'CLogFilter',
						'categories'=>'system.db.*',
						'logFile'=>'sql.log'
					),

					// array(
					// 	'class'=>'ext.debug-tool.DebugLogRoute',
					// 	'levels'=>'error,trace,info,warning,profile',
					// 	'categories'=>'system.db.CDbCommand.query',
					// ),
				),
			),
		),
	)
);
