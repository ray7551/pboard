<?php
class Lists extends Model
{

	// delete this when Lists is no longer extends CAcitveRecord
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{list}}';
	}

	public function relations() {
		return array('board' => array(self::BELONGS_TO, 'Boards', 'bid', 'select' => array('id', 'title', 'creator_id', 'desc'), 'joinType' => 'INNER JOIN',), 'cards' => array(self::HAS_MANY, 'Cards', 'lid', 'select' => array('id', 'title', 'creator_id', 'cstatus', 'prev_id'), 'joinType' => 'LEFT JOIN', 'scopes' => array('visible'),),);
	}

	public function getByBid($bid) {
		$criteria = new CDbCriteria;
		$criteria->select = array('id', 'bid', 'title', 'creator_id', 'priority');
		$criteria->condition = 'bid=:bid';
		$criteria->params = array(':bid' => $bid);
		$criteria->order = 'priority';
		return $this->findAllRecord($criteria);
	}

	public function beforeSave() {
		if (parent::beforeSave()) {
			if($this->isNewRecord){
				$isDup = self::model()->countByAttributes(
					array(
						'bid' => $this->bid,
						'prev_id' => $this->prev_id
					)
				);

				$ret = $isDup > 0 ? false : true;
				return $ret;
			}else{
				return true;
			}
		} else {
			return false;
		}
	}
}
