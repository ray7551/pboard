<?php

/**
 * @property int id
 * @property int lid
 * @property int bid
 * @property enum cstatus
 * @property int prev_id
 * @property int creator_id
 * @property mixed title
 */
class Cards extends Model
{
	/**
	 * The followings are the available columns in table 'pboard_card':
	 * @var int $id
	 * @var int $bid		: board id
	 * @var int $lid		: list id
	 * @var string $title	: card title
	 * @var string $detail	: card detail text
	 * @var enum $cstatus	: card cstatus
	 */
	// card cstatus(0：待认领，1：进行中，2：待验证，3：已关闭, 4:Deleted, 5:Achived)
	const CSTATUS_TOCLAIME	= 0;
	const CSTATUS_DOING		= 1;
	const CSTATUS_TOVERIFY	= 2;
	const CSTATUS_CLOSED	= 3;
	const CSTATUS_DELETED	= 4;
	const CSTATUS_ARCHIVED	= 5;

	// delete this when Card is no longer extends CAcitveRecord
	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	public function tableName(){
		return '{{card}}';
	}

	public function rules(){
		return array(
			array('bid, lid', 'required'),
			array('title', 'length', 'max'=>64),
			// array('detail'),
			array('cstatus', 'default', 'value'=>Cards::CSTATUS_TOCLAIME),
		);
	}

	public function scopes() {
		return array(
			// Not be deleted or archived card
			'visible' => array(
				'condition' => $this->tableAlias.".`cstatus` not in ('".Cards::CSTATUS_DELETED."','".Cards::CSTATUS_ARCHIVED."')",
			),
		);
	}

	// delete this when Card is no longer extends CAcitveRecord
	public function relations(){
		return array(
			'list' => array(self::BELONGS_TO, 'Lists', 'lid'),
		);
	}


	public function getByBid($bid){

		$criteria = new CDbCriteria;
		$criteria->select = array('l.title', 'l.id', 'l.bid', 'l.creator_id', 'l.prev_id');
		$criteria->alias = 'l';
		$criteria->condition = 'l.bid=:bid';
		$criteria->params = array(':bid'=>$bid);
		$lists = Lists::model()->with('cards')->findAll($criteria);


		$boardData = array();

		foreach ($lists as $lkey => $list) {
			$listData = array();
			$listData['id'] 		= $list->id;
			$listData['title'] 		= $list->title;
			$listData['prev_id'] 	= $list->prev_id;
			$listData['creator_id'] = $list->creator_id;
			$listData['type']		= 'list';
			$listData['editing']	= false;
			$listData['cards']		= array();

			foreach ($list->cards as $ckey => $card) {
				$cardData = array();
				$cardData['id']			= $card->id;
				$cardData['title']		= $card->title;
				$cardData['prev_id']	= $card->prev_id;
				$cardData['creator_id']	= $card->creator_id;
				$cardData['cstatus']	= $card->cstatus;
				$cardData['fid']	= array();
				$cardData['editing']	= false;
				$cardData['type']		= 'card';
				$cardData['title']		= $card->title;

				$listData['cards'][$card->prev_id]	= $cardData;
			}
			$listData['cards']['length'] = count($list->cards);

			$boardData[$list->prev_id] = $listData;
		}
		$boardData['length'] = count($lists);

		return $boardData;

	}

	public function getBylid($lid){
		$criteria = new CDbCriteria;
		$criteria->select = array('l.title', 'l.id', 'l.bid', 'l.creator_id', 'l.prev_id');
		$criteria->alias = 'l';
		$criteria->condition = 'l.id=:lid';
		$criteria->params = array(':lid'=>$lid);
		$lists = Lists::model()->with('cards')->findAll($criteria);

		$listData = array();
		foreach ($lists as $lkey => $list) {
			$listData['id'] 		= $list->id;
			$listData['title'] 		= $list->title;
			$listData['prev_id'] 	= $list->prev_id;
			$listData['creator_id'] = $list->creator_id;
			$listData['type']		= 'list';
			$listData['editing']	= false;
			$listData['cards']		= array();

			foreach ($list->cards as $ckey => $card) {
				$cardData = array();
				$cardData['id']			= $card->id;
				$cardData['title']		= $card->title;
				$cardData['prev_id']	= $card->prev_id;
				$cardData['creator_id']	= $card->creator_id;
				$cardData['cstatus']	= $card->cstatus;
				$cardData['fid']	= array();
				$cardData['editing']	= false;
				$cardData['type']		= 'card';
				$cardData['title']		= $card->title;

				$listData['cards'][]	= $cardData;
			}
		}

		return $listData;
	}
}