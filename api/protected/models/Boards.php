<?php
class Boards extends Model
{

	// delete this when Boards is no longer extends CAcitveRecord
	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	public function tableName(){
		return '{{board}}';
	}

	public function relations(){
		return array(
			'lists' => array(self::HAS_MANY, 'Lists', 'bid'),
		);
	}

	// public function getByUid($uid){
	// 	$criteria = new CDbCriteria;
	// 	$criteria->condition = 'uid=:uid';
	// 	$criteria->params = array(':uid' => $uid);

	// 	$boardInfo = self::model()->with('joins')->findAll($criteria);
	// 	return $boardInfo;
	// }

	public function getAll(){
		$boardInfo = self::model()->findAllRecord(array('index' => 'id'));
		return $boardInfo;
	}

}