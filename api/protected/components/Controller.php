<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{

	public $errorMsg = array('NO_ERR' => 'Ok', 'INVALID_PARAM' => 'Invalid param', 'NO_PERMISSON' => 'Have no permission to access this resource', 'RETRY' => 'Please retry');

	public function init() {
		parent::init();
		if (DEBUG_TOOL === true) {
			Yii::import('application.utils.Debug');
		}
	}

	public function filters() {
		return array('accessControl');
	}

	public function accessControl() {
		return array(

		// allow authenticated users to access index action
		array('allow', 'actions' => array('index'), 'users' => array('@'),), array('deny', 'users' => array('*'),),);
	}

    protected function getUrlId($url) {
        return substr(strrchr($url, "/") , 1);
    }

    /**
     * print json data with code & msg
     * @param array $data
     * @param string $msg
     */
    public function jsonSuccessReturn($data = array(), $msg = '') {
		$this->jsonReturn('NO_ERR', $msg, $data);
	}

	/**
	 * print json data with code & msg
	 * @param string $code
	 * @param string $msg
	 * @param array $data
	 */
	public function jsonFailReturn($code, $msg, $data = array()) {
		$this->jsonReturn($code, $msg, $data);
	}

	public function jsonReturn($code, $msg, $data, $return=false) {
		if (isset($this->errorMsg[$code])) {
			$msg = $msg === '' ? $this->errorMsg[$code] : ($this->errorMsg[$code] . ': ' . $msg);
		}

		// Ensure returned data is an object
		if (empty($data)) {
			$data = (object)$data;
		}

		$ret = array('code' => $code, 'msg' => $msg, 'data' => $data,);
		header("Access-Control-Allow-Origin:*");
		header("Cache-Control:max-age=0, must-revalidate, no-cache, no-store");

		if (YII_DEBUG && isset($_GET['no_web_log'])) {
			header('Content-Type:text/html');
		}else{
			header('Content-Type:application/json');
		}
		if($return){
			return json_encode($ret);
		}else{
			echo json_encode($ret);
			Yii::app()->end();
		}
	}

	public function beforeAction($action) {
		if (isset($_GET['no_web_log']) || Yii::app()->request->isAjaxRequest) {
			foreach (Yii::app()->log->routes as $route) {
				if ($route instanceof CWebLogRoute) {
					$route->enabled = false;
				}
			}
		}
		return true;
	}

	public function _sendResponse($status = 200, $body = array(), $content_type = 'application/json') {
		$status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
		header($status_header);
		header('Content-Type:' . $content_type);
		if($content_type == 'application/json'){
			if($status >= 200 && $status <=299){
				$body = $this->jsonReturn($status, $this->_getStatusCodeMessage($status), $body, true);
			}else{
				$body = $this->jsonReturn($status, $this->_getStatusCodeMessage($status).': '.$body, array(), true);
			}
		}

		//header("Access-Control-Allow-Origin: *");
		echo $body;
		Yii::app()->end();
	}

	public function _getStatusCodeMessage($status) {
		$codes = array(
			100 => 'Continue',
			101 => 'Switching Protocols',
			102 => 'Processing',
			118 => 'Connection timed out',
			200 => 'OK',
			201 => 'Created',
			202 => 'Accepted',
			203 => 'Non-Authoritative',
			204 => 'No Content',
			205 => 'Reset Content',
			206 => 'Partial Content',
			207 => 'Multi-Status',
			210 => 'Content Different',
			300 => 'Multiple Choices',
			301 => 'Moved Permanently',
			302 => 'Found',
			303 => 'See Other',
			304 => 'Not Modified',
			305 => 'Use Proxy',
			307 => 'Temporary Redirect',
			310 => 'Too many Redirect',
			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			405 => 'Method Not Allowed',
			406 => 'Not Acceptable',
			407 => 'Proxy Authentication Required',
			408 => 'Request Time-out',
			409 => 'Conflict',
			410 => 'Gone',
			411 => 'Length Required',
			412 => 'Precondition Failed',
			413 => 'Request Entity Too Large',
			414 => 'Request-URI Too Long',
			415 => 'Unsupported Media Type',
			416 => 'Requested range unsatisfiable',
			417 => 'Expectation failed',
			418 => 'I’m a teapot',
			422 => 'Unprocessable entity',
			423 => 'Locked',
			424 => 'Method failure',
			425 => 'Unordered Collection',
			426 => 'Upgrade Required',
			449 => 'Retry With',
			450 => 'Blocked by Windows Parental Controls',
			500 => 'Internal Server Error',
			501 => 'Not Implemented',
			502 => 'Bad Gateway ou Proxy Error',
			503 => 'Service Unavailable',
			504 => 'Gateway Time-out',
			505 => 'HTTP Version not supported',
			507 => 'Insufficient storage',
			509 => 'Bandwidth Limit Exceeded',
		);


		return (isset($codes[$status])) ? $codes[$status] : '';
	}

		/**
	 * Broadcast to websocket server
	 * @param  array $entry the updated entry,
	 * $entry['bid'],$entry['type'] must be set, it relates to subscribe topic
	 * @param  string $action the action to be excute in the subscriber
	 * $entry sample: $entry = array(
	 * 	'bid' => 1,
	 * 	'type' => 'card',
	 * 	'id' => '1',
	 * 	'title' => 'Card#1',
	 * 	'creator_id' => 1,
	 * 	'prev_id' => 2
	 * );
	 */
	public function broadcast($entry, $action) {
		if (!isset($entry['bid'])) {
			$this->jsonFailReturn('INVALID_PARAM', 'Board id not found.');
		}
		$bcData['topic'] = 'board' . $entry['bid'];
		$bcData['action'] = $action;
		$bcData['type'] = $entry['type'];
		if(isset($entry['id'])){ // return url when client create, update, get a resource
			$bcData['url'] = $entry['type'] . 's/' . $entry['id'];
		}
		$bcData[$entry['type']] = $entry;

		// Push data to the websocket server via ZMQ
		$context = new ZMQContext();
		$socket = $context->getSocket(ZMQ::SOCKET_PUSH, 'board pusher');
		$socket->connect("tcp://127.0.0.1:5555");
		$socket->send(json_encode($bcData));
	}

	/**
	 * return a json to be broadcast
	 * @param  array	$entry	the entry data to be broadcast
	 * @return json string
	 */

	// protected function _broadcastData($entry) {
	// 	$entry['_url'] = $entry['type'].'s/'.$entry['id'];
	// 	return json_encode($entry);
	// }

}
