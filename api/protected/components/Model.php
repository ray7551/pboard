<?php
class Model extends CActiveRecord
{
	public function findAllRecord($condition = '', $params = array()) {
		$ars = $this->findAll($condition, $params);
		return array_map(create_function('$ar', 'return $ar->attributes;'), $ars);
	}

	public function findRecord($condition = '', $params = array()) {
		return $this->find($condition, $params)->attributes;
	}

	public function findRecordByPk($pk, $condition = '', $params = array()) {
		return $this->findByPk($pk, $condition, $params)->attributes;
	}

	public function getErrorStr() {
		$errStr = '';
		foreach ($this->getErrors() as $attrname => $errlist) {
			$errStr .= " Errorred attribute: $attrname\n";
			foreach ($errlist as $err) {
				$errStr .= "    $err\n";
			}
		}
		return $errStr;
	}
}
