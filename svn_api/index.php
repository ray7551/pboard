<?php
if (!isset($_SERVER['PHP_AUTH_USER'])) {
	header('WWW-Authenticate: Basic realm="My Realm"');
	header('HTTP/1.0 401 Unauthorized');
	die('Restricted Area!');
} else {
	$user = $_SERVER['PHP_AUTH_USER'];
	$pwd = $_SERVER['PHP_AUTH_PW'];
	if ($user != 'pboard' || $pwd != 'asecret' ) {
		die('Authentication Failed.');
	}
}

$svnBase = '/svnroot';

$allowedActions = array(
	'create', 'dump'
);

if(isset($_GET['action']) && !empty($_GET['action'])){
	if(in_array($_GET['action'], $allowedActions)){
		$action = $_GET['action'];
	}else{
		die('Need an action');
	}
}


