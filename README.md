#PBoard
A realtime collaborative platform.


##API
###Cross Origin Resource Sharing
For the convenience of debug, please disable your brower's same origin policy.

```bash
google-chrome --disable-web-security
```

Or add `header("Access-Control-Allow-Origin:*");` in yii/base/CErrorHandler.php or your own errorhandler.
```php
if(YII_DEBUG){
  header("Access-Control-Allow-Origin:*");
  $this->render('exception',$data);
}
```