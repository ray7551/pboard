var gulp = require('gulp');
var clean = require('gulp-clean');
var flatten = require('gulp-flatten');
var usemin = require('gulp-usemin');
var cssmin = require('gulp-minify-css');
var htmlmin = require('gulp-minify-html');
var uglify = require('gulp-uglify');
var less = require('gulp-less');
var shell = require( 'gulp-shell');
//var concat = require('gulp-concat');
//var watch = require('gulp-watch');
//var livereload = require('gulp-livereload');
//var server = livereload();


function packTask() {
	return gulp.src('src/index.html')
		.pipe(usemin({
			shiv: [],
			css: [cssmin()],
			jslib: [uglify()],
			jsNg: [uglify()],
			jsapp: []
	}));
}
function packTaskDev() {
	return gulp.src('src/index.html')
		.pipe(usemin({
			shiv: [],
			css: [],
			jslib: [],
			jsNg: [],
			jsapp: []
	}));
}

function getView() {
	return gulp.src('src/modules/*/*.html')
		.pipe(flatten());
}

gulp.task('clean', function () {
	return gulp.src('dist/*', {read: false})
		.pipe(clean());
});

// less to css
gulp.task('less', function() {
	return gulp.src('src/less/base.less')
		.pipe(less()).pipe(gulp.dest('src/css'));
});

// pack css and js files and index.html
gulp.task('pack', ['less'], function () {
	return packTask().pipe(gulp.dest('dist'));
});
gulp.task('pack-dev', ['less'], function () {
	return packTaskDev().pipe(gulp.dest('dist'));
});

// copy view files
gulp.task('copy-dev', function () {
	return getView().pipe(gulp.dest('dist/views'));
});
gulp.task('copy', function() {
	return getView().pipe(htmlmin({'quotes':true, 'conditionals':true, 'empty':true})).pipe(gulp.dest('dist/views'));
});

// fonts
gulp.task('fonts', function () {
	return gulp.src('fonts/*').pipe(gulp.dest('dist/fonts'));
});

// img
gulp.task('img', function() {
	return gulp.src('src/img/*').pipe(gulp.dest('dist/img'));
});

var notifySend = function (msg) {
	return shell.task(['notify-send "[gulp]'+ msg +'"', 'date', 'echo']);
};

gulp.task('build-dev', ['pack-dev', 'copy', 'fonts', 'img'],
	notifySend('build-dev completed!'),
	console.log('build completed')
);
gulp.task('build', ['clean', 'pack', 'copy', 'fonts', 'img'],
	// notifySend('build completed!')
	console.log('build completed')
);

gulp.task('default', ['clean', 'build-dev']);

gulp.task('watch', function() {
	return gulp.watch(['src/**/*', '!src/css/base.css'], ['build-dev']);
});

//module.exports = gulp;
