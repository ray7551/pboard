angular.module('PbApp')
	.value('version', '0.1')
	.factory('BoardInfoFactory', ['$http', 'SERVER',
		function ($http, SERVER) {
			return {
				get: function (bid) {
					bid = bid || '';
					return $http({
						method: 'GET',
						url: SERVER.apiUrlBase + 'boards/' + bid,
					//	timeout: 2000
					});
				}
			};
		}
	]).factory('syncService', ['$q', '$routeParams', 'SERVER',
		function ($q, $routeParams, SERVER) {
			var server = SERVER.wsUrlBase;
			//var server = 'ws://vm-1.ray7551kd.io:8989';
			var syncService = {};
			syncService = function (onopen, onclose) {
				return new ab.Session(server, onopen, onclose, {
					// Additional parameters, we're ignoring the WAMP sub-protocol for older browsers
					'skipSubprotocolCheck': true
				});
			};
			return syncService;
		}
	]).factory('dataFactory', ['$http', 'SERVER',
		function ($http, SERVER) {
			var urlBase = SERVER.apiUrlBase;
			var dataFactory = {};

			dataFactory.createBoard = function (postData) {
				return $http.post(urlBase + 'boards/', postData);
			};

			dataFactory.updateBoard = function (bid, putData) {
				return $http.put(urlBase + 'boards/' + bid, putData);
			};

			dataFactory.createCard = function (postData) {
				return $http.post(urlBase + 'cards/', postData);
			};

			dataFactory.getCardsByBid = function (bid) {
				return $http({
					method: 'GET',
					url: urlBase + 'cards?bid=' + bid,
				//	timeout: 4000
				});
			};

			dataFactory.getCardsByLid = function (lid) {
				return $http({
					method: 'GET',
					url: urlBase + 'cards?lid=' + lid,
			//		timeout: 4000
				});
			};

			dataFactory.updateCard = function (cid, postData) {
				return $http.put(urlBase + 'cards/' + cid, postData);
			};

			dataFactory.deleteCard = function (cid) {
				return $http({
					method: 'DELETE',
					url: urlBase + 'cards/' + cid,
				//	timeout: 3000
				});
			}

			dataFactory.createList = function (postData) {
				return $http.post(urlBase + 'lists/', postData);
			};

			dataFactory.updateList = function (lid, putData) {
				return $http({
					method: 'PUT',
					url: urlBase + 'lists/' + lid, // + '?XDEBUG_SESSION_START=PHPSTORM',
					data: putData
				});
				//return $http.put(urlBase + 'lists/' + lid, putData);
			};

			dataFactory.deleteList = function (lid) {
				return $http({
					method: 'DELETE',
					url: urlBase + 'lists/' + lid,
			//		timeout: 3000
				});
			};

			return dataFactory;
		}
	]);
