angular.module('PbApp').controller('MainCtrl',
	function ($rootScope, $scope, $route, $location, dataFactory) {

		$scope.bcolor = ['blue', 'green', 'cyan', 'orange', 'red'];
		$scope.colorDropdown = _.map($scope.bcolor, function (color) {
			return {
				color: color,
				text: color
			};
		});

		$scope.reload = function () {
			if ($route.current.originalPath === '/') {
				$route.reload();
			}
		};

		$scope.getBdClass = function () {
			var color = $rootScope.color ? 'bg-' + $rootScope.color : 'bg-default';
			if ($location.path() === '/') {
				return 'noscrool ' + color; // class noscrool here ensure no shake while openning a modal
			} else {
				return color;
			}

		};

		$scope.getNavClass = function () {
			if ($rootScope.color) {
				return 'nav-' + $rootScope.color;
			} else {
				return 'nav-default';
			}
		};


	}
);