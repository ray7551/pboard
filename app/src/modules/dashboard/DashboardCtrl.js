angular.module('PbApp').controller('DashboardCtrl',
	function ($scope, $timeout, $modal, $alert, BoardInfoFactory, dataFactory) {
		$scope.$root.color = '';


		var boardAddModal = $modal({
			scope: $scope,
			title: 'Add a Board',
			template: 'views/boardAddModal.tpl.html',
			placement: "center",
			show: false
		});

		$scope.showBoardModal = function () {
			boardAddModal.$promise.then(boardAddModal.show);
		};

		$scope.boardAddAlert = $alert();

		$scope.newBoardDefault = {
			name: '',
			color: 'blue',
			svn: true
		};
		$scope.newBoard = _.cloneDeep($scope.newBoardDefault);
		$scope.saveNewBoardColor = function (color) {
			$scope.newBoard.color = color;
		};
		$scope.saveNewBoard = function (newBoard) {
			if (newBoard.name.length === 0) {
				alert('Please add your new board\'s name');
				return;
			}
			console.log('[New Board]', $scope.newBoard);
			$scope.newBoard = _.cloneDeep($scope.newBoardDefault);

			this.$hide();

			var postData = {
				title: newBoard.name,
				desc: newBoard.desc,
				color: newBoard.color,
				svn: newBoard.svn,
				creator_id: 1
			};
			dataFactory.createBoard(postData).then(function (data) {
				$scope.boardAddAlert.$scope.content = 'New board ' + newBoard.name + ' added.';
				$scope.boardAddAlert.show();
				console.log('[api][created]The new board id=' + data.data.data);
			});
		};
		_.each($scope.colorDropdown, function (colorDd) {
			colorDd.click = "saveNewBoardColor(\"" + colorDd.color + "\")";
		});

		BoardInfoFactory.get().then(function (bdata, status, headers, config) {
			$scope.boardInfo = {};
			// @todo use _.each with $timeout
			$scope.keys = _.keys(bdata.data.data).reverse();
			var push2Boards = function () {
				if (_.isEmpty($scope.keys)) {
					return;
				}
				$scope.key = $scope.keys.pop();
				$scope.boardInfo[$scope.key] = bdata.data.data[$scope.key];
				$timeout(push2Boards, 300);
			};
			push2Boards();
		}, function (bdata, status, headers, config) {
			alert('No data recieved, please check your Internet connection.');
		});
	}
);
