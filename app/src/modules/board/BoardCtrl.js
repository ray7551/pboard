angular.module('PbApp').controller('BoardCtrl',
	function ($scope, $routeParams, $dropdown, BoardInfoFactory, dataFactory, syncService) {

		var handleError = function (bdata, status, headers, config) {
			alert('No data recieved, please check your Internet connection.');
			$scope.board = {};
		};
		$scope.aside = {
			'title': 'Board Settings'
		};

		_.each($scope.colorDropdown, function(colorDd){
			colorDd.click = "saveColor(\"" + colorDd.color + "\")";
		});
		$scope.saveColor = function (color) {
			var putdata = {
				id: $scope.id,
				color: color
			};
			dataFactory.updateBoard($scope.id, putdata).then(
				function (data) {
					// alert('New board color saved!');
				}, function (data) {
					alert("Sorry cann't save board color.Please retry later.");
				}
			);
		};

		$scope.editList = function (list) {
			list.editing = true;
		};

		$scope.cancelEditingList = function (list) {
			list.editing = false;
		};

		$scope.saveList = function (fields) {
			this.list.save(fields);
		};

		$scope.removeList = function (list) {
			if (window.confirm('Are you sure to remove this list?')) {
				list.destroy();
			}
		};

		$scope.listOrderChange = function (lid, prev_id) {
			var newListData = {
				id: lid,
				bid: $scope.id,
				type: 'list',
				prev_id: prev_id
			};
			dataFactory.updateList(lid, newListData);
		};

		$scope.cardOrderChange = function (cid, prev_id, lid) {
			lid = lid || '';
			var newCardData = {
				id: cid,
				bid: $scope.id,
				type: 'card',
				prev_id: prev_id
			};
			if (lid) {
				newCardData.lid = lid;
			}
			dataFactory.updateCard(cid, newCardData);
		};

		$scope.addCard = function (list) {
			if (!list.newCardName || list.newCardName.length === 0) {
				return;
			}
			var prev_id = list.cards.length === 0 ? 0 : list.cards[list.cards.length - 1].id;
			var newCardData = {
				bid: $scope.id,
				lid: list.id,
				title: list.newCardName,
				type: 'card',
				creator_id: 0,
				prev_id: prev_id
			};

			dataFactory.createCard(newCardData).then(function (data) {
				console.log('[api][created]The new card id=' + data.data.data);
			});
			list.newCardName = '';
		};

		$scope.updateCard = function (cid, card) {
			var newCardData = {
				id: cid,
				bid: $scope.id,
				type: 'card',
				prev_id: card.prev_id,
				title: card.title
			};
			dataFactory.updateCard(cid, newCardData);
		};

		$scope.removeCard = function (cid) {
			if (window.confirm('Are you sure to remove this card?')) {
				dataFactory.deleteCard(cid);
			}
		};

		$scope.options = {
			accept: function (sourceNode, destNodes) {
				var data = sourceNode.$modelValue;
				var destType = destNodes.$element.attr('data-type');
				return (data.type == destType); // only accept the same type
			},
			dropped: function (event) {
				var sourceNode = event.source.nodeScope;
				var sourceModel = sourceNode.$modelValue;
				var destNodes = event.dest.nodesScope;
				// var destModel = destNodes.$modelValue;
				var orderChanged = event.source.index !== event.dest.index;
				var isCard = destNodes.$element.attr('data-type') == 'card';
				var isList = destNodes.$element.attr('data-type') == 'list';
				var bdata = $scope.bdata.data.data;
				var ldata;
				var list;
				var lid;
				var lprevid = null;
				var newPrevId = null;

				if (isCard && destNodes.isParent(sourceNode) && orderChanged) {
					console.log('Card order changed within a list');
					list = destNodes.$nodeScope.$modelValue;
					lid = list.id;

					for (i = 0; data = $scope.bdata.data.data[i]; i++) {
						if (data.id === lid) {
							lprevid = data.prev_id;
							break;
						}
					}
					if (_.isNull(lprevid)) {
						return;
					}

					ldata = $scope.bdata.data.data[lprevid].cards;

					for (i = 0, index = 0; !_.isUndefined(ldata[index]); ++i, index = ldata[index].id) {
						if (i === event.dest.index) {
							if (i === 0) {
								newPrevId = 0;
								break;
							} else {
								if (event.dest.index > event.source.index) {
									newPrevId = ldata[index].id;
									break;
								} else {
									newPrevId = ldata[index].prev_id;
									break;
								}
							}
						}
					}

					$scope.cardOrderChange(sourceModel.id, newPrevId, lid);
				} else if (isCard && !destNodes.isParent(sourceNode)) {
					console.log('A card be dragged to another list');
					list = destNodes.$nodeScope.$modelValue;
					lid = list.id;

					for (i = 0; data = $scope.bdata.data.data[i]; i = $scope.bdata.data.data[i].id) {
						if (data.id === lid) {
							lprevid = data.prev_id;
							break;
						}
					}
					if (_.isNull(lprevid)) {
						return;
					}
					ldata = $scope.bdata.data.data[lprevid].cards;
					if (ldata.length === 0) {
						newPrevId = 0;
					}

					for (i = 0, index = 0; !_.isUndefined(ldata[index]); ++i, index = ldata[index].id) {
						if (i === event.dest.index) {
							if (i === 0) {
								newPrevId = 0;
								break;
							} else {
								newPrevId = ldata[index].prev_id;
								break;
							}

						} else if (i === ldata.length - 1) {
							newPrevId = ldata[index].id;
						}
					}

					if (_.isNull(newPrevId)) {
						newPrevId = 0;
					}

					console.log('[Changed]card ' + sourceModel.id + ' goes after ' + newPrevId + ' to list ' + lid);

					$scope.cardOrderChange(sourceModel.id, newPrevId, lid);


				} else if (isList && destNodes.isParent(sourceNode) && orderChanged) {
					console.log('List order changed');
					for (i = 0, index = 0; !_.isUndefined(bdata[index]); ++i, index = bdata[index].id) {
						if (i === event.dest.index) {
							if (i === 0) {
								newPrevId = 0;
							} else {
								if (event.dest.index > event.source.index) {
									newPrevId = bdata[index].id;
								} else {
									newPrevId = bdata[index].prev_id;
								}
							}
						}
					}

					$scope.listOrderChange(sourceModel.id, newPrevId);
				} else {
					console.log('No change happend');
				}
			}
		};

		$scope.id = $routeParams.boardId;
		$scope.lists = [];

		$scope.getBInfo = function () {
			BoardInfoFactory.get($scope.id).then(function (bdata) {
				$scope.boardInfo = bdata.data.data;
				$scope.$root.color = $scope.boardInfo.color;
			});
		};
		$scope.getBInfo();


		dataFactory.getCardsByBid($routeParams.boardId).then(function (bdata) {
			var listDestroy = function () {
				dataFactory.deleteList(this.id);
			};

			/**
			 * update list fields value
			 * @param fields string the fields need to update, fields are separated by ','.
			 */
			var listSave = function (fields) {
				if (!fields) {
					fields = 'title';
				}
				var newListData = {
					bid: $scope.id,
					type: 'list',
					prev_id: _.isUndefined(this.prev_id) ? 0 : this.prev_id
				};
				var arr = fields.split(',');
				var li = this;
				_.each(arr, function (field) {
					newListData[field] = li[field];
				});
				dataFactory.updateList(this.id, newListData);

				this.editing = false;
			};
			$scope.bdata = bdata;

			$scope.topicId = 'board' + $scope.id;
			var ws = syncService(
				function () {
					console.log('[opened]Opened a WebSocket connection.');
					console.log('[subscribe]Send a ' + $scope.topicId + ' subscribe request to WebSocket server.');
					this.subscribe($scope.topicId, function (topic, data) {
						console.log('[received]topic #' + topic + ' :');
						console.log(data);
						if (data.type === 'list') {
							data.list.cards = {
								length: 0
							};
						}
						switch (data.action) {
							case 'board_update':
								$scope.getBInfo();
								break;

							case 'list_create':
								$scope.$apply(function () {
									$scope.bdata.data.data[data.list.prev_id] = data.list;
									$scope.bdata.data.data.length++;
								});
								break;
							case 'list_update':
								$scope.$apply(function () {
									$scope.bdata.data.data = data.board;
								});
								break;
							case 'list_delete':
								$scope.$apply(function () {
									$scope.bdata.data.data = data.board;
								});
								break;

							case 'card_create':
								var indexs = _.keys($scope.bdata.data.data);
								indexs.pop('length');
								_.each(indexs, function (index) {
									if ($scope.bdata.data.data[index].id == data.card.lid) {
										$scope.$apply(function () {
											$scope.bdata.data.data[index].cards[data.card.prev_id] = data.card;
											$scope.bdata.data.data[index].cards.length++;
										});
									}
								});
								break;
							case 'card_delete':
								$scope.$apply(function () {
									$scope.bdata.data.data = data.board;
								});
								break;
							case 'card_update':
								$scope.$apply(function () {
									$scope.bdata.data.data = data.board;
								});
								break;

						}
					});
				},

				function () {
					console.log('[closed]WebSocket connection closed.');
				}
			);

			$scope.$on('$destroy', function (next, current) {
				//if(_.indexOf(ws.subscriptions, $scope.topicId))
				if (!_.isNull(ws.sessionid())) {
					ws.unsubscribe($scope.topicId);
					console.log('[unsubscribe]Unsubscribe from topic' + $scope.topicId);
					ws.close();
				}
			});

			// jshint ignore:start
$scope.$watch('bdata',
	function () {
		$scope.board = JSON.parse(JSON.stringify($scope.bdata.data.data));
		$scope.lists = [];
		if ($scope.bdata.data.data.length === 0) {
			return;
		}
		for (var index = 0; typeof $scope.board[index] !== "undefined"; index = list.id) {
			var list = $scope.board[index];

			list.editing = false;

			var cards = [];
			for (var cindex = 0; typeof list.cards[cindex] !== "undefined"; cindex = list.cards[cindex].id) {
				cards.push(list.cards[cindex]);
			}
			list.cards = cards;

			list.destroy = listDestroy;
			list.save = listSave;
			$scope.lists.push(list);

			// $scope.lists.sort(function (list1, list2) {
			// 	return list1.sortOrder - list2.sortOrder;
			// });
		}


	}, true);
	// jshint ignore:end
}, handleError);

		$scope.addList = function () {
			if ($scope.lists.length > 30) {
				window.alert('You can\'t add more than 30 lists!');
				return;
			}
			var listName = document.getElementById('listName').value.replace(/^\s+|\s+$/g, "");
			if (listName.length > 0) {
				var prev_id = $scope.lists.length === 0 ? 0 : $scope.lists[$scope.lists.length - 1].id;
				var newListData = {
					bid: $scope.id,
					title: listName,
					type: 'list',
					cards: [],
					creator_id: 0,
					prev_id: prev_id
				};

				dataFactory.createList(newListData).then(function (data) {
					console.log('[api][created]The new list id=' + data.data.data);
				});
			}

			document.getElementById('listName').value = '';
		};


	}
);