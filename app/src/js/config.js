angular.module('PbApp')
	.config(function ($routeProvider, $locationProvider, $httpProvider, $sceProvider) {
		$sceProvider.enabled(false);
		$routeProvider
			.when('/', {
				templateUrl: '/views/dashboard.html',
				controller: 'DashboardCtrl',
				reloadOnSearch: false
			})
			.when('/board/:boardId', {
				templateUrl: '/views/board.html',
				controller: 'BoardCtrl'
			})
			.otherwise({
				redirectTo: '/'
			});
		// use the HTML5 History API
		$locationProvider.html5Mode(true).hashPrefix('!');

		/**
		 * The workhorse; converts an object to x-www-form-urlencoded serialization.
		 * @param {Object} obj
		 * @return {String}
		 */
		var param = function (obj) {
			var query = '';
			var name, value, fullSubName, subValue, innerObj, i;

			for (name in obj) {
				value = obj[name];

				if (value instanceof Array) {
					for (i = 0; i < value.length; ++i) {
						subValue = value[i];
						fullSubName = name + '[' + i + ']';
						innerObj = {};
						innerObj[fullSubName] = subValue;
						query += param(innerObj) + '&';
					}
				} else if (value instanceof Object) {
					for (var subName in value) {
						subValue = value[subName];
						fullSubName = name + '[' + subName + ']';
						innerObj = {};
						innerObj[fullSubName] = subValue;
						query += param(innerObj) + '&';
					}
				} else if (value !== undefined && value !== null) {
					query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
				}
			}
			return query.length ? query.substr(0, query.length - 1) : query;
		};
		$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
		$httpProvider.defaults.transformRequest = [
			function (data) {
				return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
			}
		];
	})
	.config(function ($dropdownProvider) {
		angular.extend($dropdownProvider.defaults, {
			html: true,
			template: '/views/dropdown.tpl.html'
		});
	})
	.config(function ($alertProvider) {
		angular.extend($alertProvider.defaults, {
			title: "[success]",
			content: "Operation done.",
			type: "info",
			show: false,
			container: ".container",
			placement: "top-right",
			animation: 'am-fade-and-slide-top',
			template: "views/alert.tpl.html"
		});
	});
