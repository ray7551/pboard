/**
 * @license AngularJS v1.2.16
 * (c) 2010-2014 Google, Inc. http://angularjs.org
 * License: MIT
 */
(function(window, angular, undefined) {'use strict';

/* jshint maxlen: false */

/**
 * @ngdoc module
 * @name ngAnimate
 * @description
 *
 * # ngAnimate
 *
 * The `ngAnimate` module provides support for JavaScript, CSS3 transition and CSS3 keyframe animation hooks within existing core and custom directives.
 *
 *
 * <div doc-module-components="ngAnimate"></div>
 *
 * # Usage
 *
 * To see animations in action, all that is required is to define the appropriate CSS classes
 * or to register a JavaScript animation via the myModule.animation() function. The directives that support animation automatically are:
 * `ngRepeat`, `ngInclude`, `ngIf`, `ngSwitch`, `ngShow`, `ngHide`, `ngView` and `ngClass`. Custom directives can take advantage of animation
 * by using the `$animate` service.
 *
 * Below is a more detailed breakdown of the supported animation events provided by pre-existing ng directives:
 *
 * | Directive                                                 | Supported Animations                               |
 * |---------------------------------------------------------- |----------------------------------------------------|
 * | {@link ng.directive:ngRepeat#usage_animations ngRepeat}         | enter, leave and move                              |
 * | {@link ngRoute.directive:ngView#usage_animations ngView}        | enter and leave                                    |
 * | {@link ng.directive:ngInclude#usage_animations ngInclude}       | enter and leave                                    |
 * | {@link ng.directive:ngSwitch#usage_animations ngSwitch}         | enter and leave                                    |
 * | {@link ng.directive:ngIf#usage_animations ngIf}                 | enter and leave                                    |
 * | {@link ng.directive:ngClass#usage_animations ngClass}           | add and remove                                     |
 * | {@link ng.directive:ngShow#usage_animations ngShow & ngHide}    | add and remove (the ng-hide class value)           |
 * | {@link ng.directive:form#usage_animations form}                 | add and remove (dirty, pristine, valid, invalid & all other validations)                |
 * | {@link ng.directive:ngModel#usage_animations ngModel}           | add and remove (dirty, pristine, valid, invalid & all other validations)                |
 *
 * You can find out more information about animations upon visiting each directive page.
 *
 * Below is an example of how to apply animations to a directive that supports animation hooks:
 *
 * ```html
 * <style type="text/css">
 * .slide.ng-enter, .slide.ng-leave {
 *   -webkit-transition:0.5s linear all;
 *   transition:0.5s linear all;
 * }
 *
 * .slide.ng-enter { }        /&#42; starting animations for enter &#42;/
 * .slide.ng-enter-active { } /&#42; terminal animations for enter &#42;/
 * .slide.ng-leave { }        /&#42; starting animations for leave &#42;/
 * .slide.ng-leave-active { } /&#42; terminal animations for leave &#42;/
 * </style>
 *
 * <!--
 * the animate service will automatically add .ng-enter and .ng-leave to the element
 * to trigger the CSS transition/animations
 * -->
 * <ANY class="slide" ng-include="..."></ANY>
 * ```
 *
 * Keep in mind that if an animation is running, any child elements cannot be animated until the parent element's
 * animation has completed.
 *
 * <h2>CSS-defined Animations</h2>
 * The animate service will automatically apply two CSS classes to the animated element and these two CSS classes
 * are designed to contain the start and end CSS styling. Both CSS transitions and keyframe animations are supported
 * and can be used to play along with this naming structure.
 *
 * The following code below demonstrates how to perform animations using **CSS transitions** with Angular:
 *
 * ```html
 * <style type="text/css">
 * /&#42;
 *  The animate class is apart of the element and the ng-enter class
 *  is attached to the element once the enter animation event is triggered
 * &#42;/
 * .reveal-animation.ng-enter {
 *  -webkit-transition: 1s linear all; /&#42; Safari/Chrome &#42;/
 *  transition: 1s linear all; /&#42; All other modern browsers and IE10+ &#42;/
 *
 *  /&#42; The animation preparation code &#42;/
 *  opacity: 0;
 * }
 *
 * /&#42;
 *  Keep in mind that you want to combine both CSS
 *  classes together to avoid any CSS-specificity
 *  conflicts
 * &#42;/
 * .reveal-animation.ng-enter.ng-enter-active {
 *  /&#42; The animation code itself &#42;/
 *  opacity: 1;
 * }
 * </style>
 *
 * <div class="view-container">
 *   <div ng-view class="reveal-animation"></div>
 * </div>
 * ```
 *
 * The following code below demonstrates how to perform animations using **CSS animations** with Angular:
 *
 * ```html
 * <style type="text/css">
 * .reveal-animation.ng-enter {
 *   -webkit-animation: enter_sequence 1s linear; /&#42; Safari/Chrome &#42;/
 *   animation: enter_sequence 1s linear; /&#42; IE10+ and Future Browsers &#42;/
 * }
 * @-webkit-keyframes enter_sequence {
 *   from { opacity:0; }
 *   to { opacity:1; }
 * }
 * @keyframes enter_sequence {
 *   from { opacity:0; }
 *   to { opacity:1; }
 * }
 * </style>
 *
 * <div class="view-container">
 *   <div ng-view class="reveal-animation"></div>
 * </div>
 * ```
 *
 * Both CSS3 animations and transitions can be used together and the animate service will figure out the correct duration and delay timing.
 *
 * Upon DOM mutation, the event class is added first (something like `ng-enter`), then the browser prepares itself to add
 * the active class (in this case `ng-enter-active`) which then triggers the animation. The animation module will automatically
 * detect the CSS code to determine when the animation ends. Once the animation is over then both CSS classes will be
 * removed from the DOM. If a browser does not support CSS transitions or CSS animations then the animation will start and end
 * immediately resulting in a DOM element that is at its final state. This final state is when the DOM element
 * has no CSS transition/animation classes applied to it.
 *
 * <h3>CSS Staggering Animations</h3>
 * A Staggering animation is a collection of animations that are issued with a slight delay in between each successive operation resulting in a
 * curtain-like effect. The ngAnimate module, as of 1.2.0, supports staggering animations and the stagger effect can be
 * performed by creating a **ng-EVENT-stagger** CSS class and attaching that class to the base CSS class used for
 * the animation. The style property expected within the stagger class can either be a **transition-delay** or an
 * **animation-delay** property (or both if your animation contains both transitions and keyframe animations).
 *
 * ```css
 * .my-animation.ng-enter {
 *   /&#42; standard transition code &#42;/
 *   -webkit-transition: 1s linear all;
 *   transition: 1s linear all;
 *   opacity:0;
 * }
 * .my-animation.ng-enter-stagger {
 *   /&#42; this will have a 100ms delay between each successive leave animation &#42;/
 *   -webkit-transition-delay: 0.1s;
 *   transition-delay: 0.1s;
 *
 *   /&#42; in case the stagger doesn't work then these two values
 *    must be set to 0 to avoid an accidental CSS inheritance &#42;/
 *   -webkit-transition-duration: 0s;
 *   transition-duration: 0s;
 * }
 * .my-animation.ng-enter.ng-enter-active {
 *   /&#42; standard transition styles &#42;/
 *   opacity:1;
 * }
 * ```
 *
 * Staggering animations work by default in ngRepeat (so long as the CSS class is defined). Outside of ngRepeat, to use staggering animations
 * on your own, they can be triggered by firing multiple calls to the same event on $animate. However, the restrictions surrounding this
 * are that each of the elements must have the same CSS className value as well as the same parent element. A stagger operation
 * will also be reset if more than 10ms has passed after the last animation has been fired.
 *
 * The following code will issue the **ng-leave-stagger** event on the element provided:
 *
 * ```js
 * var kids = parent.children();
 *
 * $animate.leave(kids[0]); //stagger index=0
 * $animate.leave(kids[1]); //stagger index=1
 * $animate.leave(kids[2]); //stagger index=2
 * $animate.leave(kids[3]); //stagger index=3
 * $animate.leave(kids[4]); //stagger index=4
 *
 * $timeout(function() {
 *   //stagger has reset itself
 *   $animate.leave(kids[5]); //stagger index=0
 *   $animate.leave(kids[6]); //stagger index=1
 * }, 100, false);
 * ```
 *
 * Stagger animations are currently only supported within CSS-defined animations.
 *
 * <h2>JavaScript-defined Animations</h2>
 * In the event that you do not want to use CSS3 transitions or CSS3 animations or if you wish to offer animations on browsers that do not
 * yet support CSS transitions/animations, then you can make use of JavaScript animations defined inside of your AngularJS module.
 *
 * ```js
 * //!annotate="YourApp" Your AngularJS Module|Replace this or ngModule with the module that you used to define your application.
 * var ngModule = angular.module('YourApp', ['ngAnimate']);
 * ngModule.animation('.my-crazy-animation', function() {
 *   return {
 *     enter: function(element, done) {
 *       //run the animation here and call done when the animation is complete
 *       return function(cancelled) {
 *         //this (optional) function will be called when the animation
 *         //completes or when the animation is cancelled (the cancelled
 *         //flag will be set to true if cancelled).
 *       };
 *     },
 *     leave: function(element, done) { },
 *     move: function(element, done) { },
 *
 *     //animation that can be triggered before the class is added
 *     beforeAddClass: function(element, className, done) { },
 *
 *     //animation that can be triggered after the class is added
 *     addClass: function(element, className, done) { },
 *
 *     //animation that can be triggered before the class is removed
 *     beforeRemoveClass: function(element, className, done) { },
 *
 *     //animation that can be triggered after the class is removed
 *     removeClass: function(element, className, done) { }
 *   };
 * });
 * ```
 *
 * JavaScript-defined animations are created with a CSS-like class selector and a collection of events which are set to run
 * a javascript callback function. When an animation is triggered, $animate will look for a matching animation which fits
 * the element's CSS class attribute value and then run the matching animation event function (if found).
 * In other words, if the CSS classes present on the animated element match any of the JavaScript animations then the callback function will
 * be executed. It should be also noted that only simple, single class selectors are allowed (compound class selectors are not supported).
 *
 * Within a JavaScript animation, an object containing various event callback animation functions is expected to be returned.
 * As explained above, these callbacks are triggered based on the animation event. Therefore if an enter animation is run,
 * and the JavaScript animation is found, then the enter callback will handle that animation (in addition to the CSS keyframe animation
 * or transition code that is defined via a stylesheet).
 *
 */

angular.module('ngAnimate', ['ng'])

  /**
   * @ngdoc provider
   * @name $animateProvider
   * @description
   *
   * The `$animateProvider` allows developers to register JavaScript animation event handlers directly inside of a module.
   * When an animation is triggered, the $animate service will query the $animate service to find any animations that match
   * the provided name value.
   *
   * Requires the {@link ngAnimate `ngAnimate`} module to be installed.
   *
   * Please visit the {@link ngAnimate `ngAnimate`} module overview page learn more about how to use animations in your application.
   *
   */

  //this private service is only used within CSS-enabled animations
  //IE8 + IE9 do not support rAF natively, but that is fine since they
  //also don't support transitions and keyframes which means that the code
  //below will never be used by the two browsers.
  .factory('$$animateReflow', ['$$rAF', '$document', function($$rAF, $document) {
    var bod = $document[0].body;
    return function(fn) {
      //the returned function acts as the cancellation function
      return $$rAF(function() {
        //the line below will force the browser to perform a repaint
        //so that all the animated elements within the animation frame
        //will be properly updated and drawn on screen. This is
        //required to perform multi-class CSS based animations with
        //Firefox. DO NOT REMOVE THIS LINE.
        var a = bod.offsetWidth + 1;
        fn();
      });
    };
  }])

  .config(['$provide', '$animateProvider', function($provide, $animateProvider) {
    var noop = angular.noop;
    var forEach = angular.forEach;
    var selectors = $animateProvider.$$selectors;

    var ELEMENT_NODE = 1;
    var NG_ANIMATE_STATE = '$$ngAnimateState';
    var NG_ANIMATE_CLASS_NAME = 'ng-animate';
    var rootAnimateState = {running: true};

    function extractElementNode(element) {
      for(var i = 0; i < element.length; i++) {
        var elm = element[i];
        if(elm.nodeType == ELEMENT_NODE) {
          return elm;
        }
      }
    }

    function stripCommentsFromElement(element) {
      return angular.element(extractElementNode(element));
    }

    function isMatchingElement(elm1, elm2) {
      return extractElementNode(elm1) == extractElementNode(elm2);
    }

    $provide.decorator('$animate', ['$delegate', '$injector', '$sniffer', '$rootElement', '$$asyncCallback', '$rootScope', '$document',
                            function($delegate,   $injector,   $sniffer,   $rootElement,   $$asyncCallback,    $rootScope,   $document) {

      var globalAnimationCounter = 0;
      $rootElement.data(NG_ANIMATE_STATE, rootAnimateState);

      // disable animations during bootstrap, but once we bootstrapped, wait again
      // for another digest until enabling animations. The reason why we digest twice
      // is because all structural animations (enter, leave and move) all perform a
      // post digest operation before animating. If we only wait for a single digest
      // to pass then the structural animation would render its animation on page load.
      // (which is what we're trying to avoid when the application first boots up.)
      $rootScope.$$postDigest(function() {
        $rootScope.$$postDigest(function() {
          rootAnimateState.running = false;
        });
      });

      var classNameFilter = $animateProvider.classNameFilter();
      var isAnimatableClassName = !classNameFilter
              ? function() { return true; }
              : function(className) {
                return classNameFilter.test(className);
              };

      function lookup(name) {
        if (name) {
          var matches = [],
              flagMap = {},
              classes = name.substr(1).split('.');

          //the empty string value is the default animation
          //operation which performs CSS transition and keyframe
          //animations sniffing. This is always included for each
          //element animation procedure if the browser supports
          //transitions and/or keyframe animations. The default
          //animation is added to the top of the list to prevent
          //any previous animations from affecting the element styling
          //prior to the element being animated.
          if ($sniffer.transitions || $sniffer.animations) {
            matches.push($injector.get(selectors['']));
          }

          for(var i=0; i < classes.length; i++) {
            var klass = classes[i],
                selectorFactoryName = selectors[klass];
            if(selectorFactoryName && !flagMap[klass]) {
              matches.push($injector.get(selectorFactoryName));
              flagMap[klass] = true;
            }
          }
          return matches;
        }
      }

      function animationRunner(element, animationEvent, className) {
        //transcluded directives may sometimes fire an animation using only comment nodes
        //best to catch this early on to prevent any animation operations from occurring
        var node = element[0];
        if(!node) {
          return;
        }

        var isSetClassOperation = animationEvent == 'setClass';
        var isClassBased = isSetClassOperation ||
                           animationEvent == 'addClass' ||
                           animationEvent == 'removeClass';

        var classNameAdd, classNameRemove;
        if(angular.isArray(className)) {
          classNameAdd = className[0];
          classNameRemove = className[1];
          className = classNameAdd + ' ' + classNameRemove;
        }

        var currentClassName = element.attr('class');
        var classes = currentClassName + ' ' + className;
        if(!isAnimatableClassName(classes)) {
          return;
        }

        var beforeComplete = noop,
            beforeCancel = [],
            before = [],
            afterComplete = noop,
            afterCancel = [],
            after = [];

        var animationLookup = (' ' + classes).replace(/\s+/g,'.');
        forEach(lookup(animationLookup), function(animationFactory) {
          var created = registerAnimation(animationFactory, animationEvent);
          if(!created && isSetClassOperation) {
            registerAnimation(animationFactory, 'addClass');
            registerAnimation(animationFactory, 'removeClass');
          }
        });

        function registerAnimation(animationFactory, event) {
          var afterFn = animationFactory[event];
          var beforeFn = animationFactory['before' + event.charAt(0).toUpperCase() + event.substr(1)];
          if(afterFn || beforeFn) {
            if(event == 'leave') {
              beforeFn = afterFn;
              //when set as null then animation knows to skip this phase
              afterFn = null;
            }
            after.push({
              event : event, fn : afterFn
            });
            before.push({
              event : event, fn : beforeFn
            });
            return true;
          }
        }

        function run(fns, cancellations, allCompleteFn) {
          var animations = [];
          forEach(fns, function(animation) {
            animation.fn && animations.push(animation);
          });

          var count = 0;
          function afterAnimationComplete(index) {
            if(cancellations) {
              (cancellations[index] || noop)();
              if(++count < animations.length) return;
              cancellations = null;
            }
            allCompleteFn();
          }

          //The code below adds directly to the array in order to work with
          //both sync and async animations. Sync animations are when the done()
          //operation is called right away. DO NOT REFACTOR!
          forEach(animations, function(animation, index) {
            var progress = function() {
              afterAnimationComplete(index);
            };
            switch(animation.event) {
              case 'setClass':
                cancellations.push(animation.fn(element, classNameAdd, classNameRemove, progress));
                break;
              case 'addClass':
                cancellations.push(animation.fn(element, classNameAdd || className,     progress));
                break;
              case 'removeClass':
                cancellations.push(animation.fn(element, classNameRemove || className,  progress));
                break;
              default:
                cancellations.push(animation.fn(element, progress));
                break;
            }
          });

          if(cancellations && cancellations.length === 0) {
            allCompleteFn();
          }
        }

        return {
          node : node,
          event : animationEvent,
          className : className,
          isClassBased : isClassBased,
          isSetClassOperation : isSetClassOperation,
          before : function(allCompleteFn) {
            beforeComplete = allCompleteFn;
            run(before, beforeCancel, function() {
              beforeComplete = noop;
              allCompleteFn();
            });
          },
          after : function(allCompleteFn) {
            afterComplete = allCompleteFn;
            run(after, afterCancel, function() {
              afterComplete = noop;
              allCompleteFn();
            });
          },
          cancel : function() {
            if(beforeCancel) {
              forEach(beforeCancel, function(cancelFn) {
                (cancelFn || noop)(true);
              });
              beforeComplete(true);
            }
            if(afterCancel) {
              forEach(afterCancel, function(cancelFn) {
                (cancelFn || noop)(true);
              });
              afterComplete(true);
            }
          }
        };
      }

      /**
       * @ngdoc service
       * @name $animate
       * @function
       *
       * @description
       * The `$animate` service provides animation detection support while performing DOM operations (enter, leave and move) as well as during addClass and removeClass operations.
       * When any of these operations are run, the $animate service
       * will examine any JavaScript-defined animations (which are defined by using the $animateProvider provider object)
       * as well as any CSS-defined animations against the CSS classes present on the element once the DOM operation is run.
       *
       * The `$animate` service is used behind the scenes with pre-existing directives and animation with these directives
       * will work out of the box without any extra configuration.
       *
       * Requires the {@link ngAnimate `ngAnimate`} module to be installed.
       *
       * Please visit the {@link ngAnimate `ngAnimate`} module overview page learn more about how to use animations in your application.
       *
       */
      return {
        /**
         * @ngdoc method
         * @name $animate#enter
         * @function
         *
         * @description
         * Appends the element to the parentElement element that resides in the document and then runs the enter animation. Once
         * the animation is started, the following CSS classes will be present on the element for the duration of the animation:
         *
         * Below is a breakdown of each step that occurs during enter animation:
         *
         * | Animation Step                                                                               | What the element class attribute looks like |
         * |----------------------------------------------------------------------------------------------|---------------------------------------------|
         * | 1. $animate.enter(...) is called                                                             | class="my-animation"                        |
         * | 2. element is inserted into the parentElement element or beside the afterElement element     | class="my-animation"                        |
         * | 3. $animate runs any JavaScript-defined animations on the element                            | class="my-animation ng-animate"             |
         * | 4. the .ng-enter class is added to the element                                               | class="my-animation ng-animate ng-enter"    |
         * | 5. $animate scans the element styles to get the CSS transition/animation duration and delay  | class="my-animation ng-animate ng-enter"    |
         * | 6. $animate waits for 10ms (this performs a reflow)                                          | class="my-animation ng-animate ng-enter"    |
         * | 7. the .ng-enter-active and .ng-animate-active classes are added (this triggers the CSS transition/animation) | class="my-animation ng-animate ng-animate-active ng-enter ng-enter-active" |
         * | 8. $animate waits for X milliseconds for the animation to complete                           | class="my-animation ng-animate ng-animate-active ng-enter ng-enter-active" |
         * | 9. The animation ends and all generated CSS classes are removed from the element             | class="my-animation"                        |
         * | 10. The doneCallback() callback is fired (if provided)                                       | class="my-animation"                        |
         *
         * @param {DOMElement} element the element that will be the focus of the enter animation
         * @param {DOMElement} parentElement the parent element of the element that will be the focus of the enter animation
         * @param {DOMElement} afterElement the sibling element (which is the previous element) of the element that will be the focus of the enter animation
         * @param {function()=} doneCallback the callback function that will be called once the animation is complete
        */
        enter : function(element, parentElement, afterElement, doneCallback) {
          this.enabled(false, element);
          $delegate.enter(element, parentElement, afterElement);
          $rootScope.$$postDigest(function() {
            element = stripCommentsFromElement(element);
            performAnimation('enter', 'ng-enter', element, parentElement, afterElement, noop, doneCallback);
          });
        },

        /**
         * @ngdoc method
         * @name $animate#leave
         * @function
         *
         * @description
         * Runs the leave animation operation and, upon completion, removes the element from the DOM. Once
         * the animation is started, the following CSS classes will be added for the duration of the animation:
         *
         * Below is a breakdown of each step that occurs during leave animation:
         *
         * | Animation Step                                                                               | What the element class attribute looks like |
         * |----------------------------------------------------------------------------------------------|---------------------------------------------|
         * | 1. $animate.leave(...) is called                                                             | class="my-animation"                        |
         * | 2. $animate runs any JavaScript-defined animations on the element                            | class="my-animation ng-animate"             |
         * | 3. the .ng-leave class is added to the element                                               | class="my-animation ng-animate ng-leave"    |
         * | 4. $animate scans the element styles to get the CSS transition/animation duration and delay  | class="my-animation ng-animate ng-leave"    |
         * | 5. $animate waits for 10ms (this performs a reflow)                                          | class="my-animation ng-animate ng-leave"    |
         * | 6. the .ng-leave-active and .ng-animate-active classes is added (this triggers the CSS transition/animation) | class="my-animation ng-animate ng-animate-active ng-leave ng-leave-active" |
         * | 7. $animate waits for X milliseconds for the animation to complete                           | class="my-animation ng-animate ng-animate-active ng-leave ng-leave-active" |
         * | 8. The animation ends and all generated CSS classes are removed from the element             | class="my-animation"                        |
         * | 9. The element is removed from the DOM                                                       | ...                                         |
         * | 10. The doneCallback() callback is fired (if provided)                                       | ...                                         |
         *
         * @param {DOMElement} element the element that will be the focus of the leave animation
         * @param {function()=} doneCallback the callback function that will be called once the animation is complete
        */
        leave : function(element, doneCallback) {
          cancelChildAnimations(element);
          this.enabled(false, element);
          $rootScope.$$postDigest(function() {
            performAnimation('leave', 'ng-leave', stripCommentsFromElement(element), null, null, function() {
              $delegate.leave(element);
            }, doneCallback);
          });
        },

        /**
         * @ngdoc method
         * @name $animate#move
         * @function
         *
         * @description
         * Fires the move DOM operation. Just before the animation starts, the animate service will either append it into the parentElement container or
         * add the element directly after the afterElement element if present. Then the move animation will be run. Once
         * the animation is started, the following CSS classes will be added for the duration of the animation:
         *
         * Below is a breakdown of each step that occurs during move animation:
         *
         * | Animation Step                                                                               | What the element class attribute looks like |
         * |----------------------------------------------------------------------------------------------|---------------------------------------------|
         * | 1. $animate.move(...) is called                                                              | class="my-animation"                        |
         * | 2. element is moved into the parentElement element or beside the afterElement element        | class="my-animation"                        |
         * | 3. $animate runs any JavaScript-defined animations on the element                            | class="my-animation ng-animate"             |
         * | 4. the .ng-move class is added to the element                                                | class="my-animation ng-animate ng-move"     |
         * | 5. $animate scans the element styles to get the CSS transition/animation duration and delay  | class="my-animation ng-animate ng-move"     |
         * | 6. $animate waits for 10ms (this performs a reflow)                                          | class="my-animation ng-animate ng-move"     |
         * | 7. the .ng-move-active and .ng-animate-active classes is added (this triggers the CSS transition/animation) | class="my-animation ng-animate ng-animate-active ng-move ng-move-active" |
         * | 8. $animate waits for X milliseconds for the animation to complete                           | class="my-animation ng-animate ng-animate-active ng-move ng-move-active" |
         * | 9. The animation ends and all generated CSS classes are removed from the element             | class="my-animation"                        |
         * | 10. The doneCallback() callback is fired (if provided)                                       | class="my-animation"                        |
         *
         * @param {DOMElement} element the element that will be the focus of the move animation
         * @param {DOMElement} parentElement the parentElement element of the element that will be the focus of the move animation
         * @param {DOMElement} afterElement the sibling element (which is the previous element) of the element that will be the focus of the move animation
         * @param {function()=} doneCallback the callback function that will be called once the animation is complete
        */
        move : function(element, parentElement, afterElement, doneCallback) {
          cancelChildAnimations(element);
          this.enabled(false, element);
          $delegate.move(element, parentElement, afterElement);
          $rootScope.$$postDigest(function() {
            element = stripCommentsFromElement(element);
            performAnimation('move', 'ng-move', element, parentElement, afterElement, noop, doneCallback);
          });
        },

        /**
         * @ngdoc method
         * @name $animate#addClass
         *
         * @description
         * Triggers a custom animation event based off the className variable and then attaches the className value to the element as a CSS class.
         * Unlike the other animation methods, the animate service will suffix the className value with {@type -add} in order to provide
         * the animate service the setup and active CSS classes in order to trigger the animation (this will be skipped if no CSS transitions
         * or keyframes are defined on the -add or base CSS class).
         *
         * Below is a breakdown of each step that occurs during addClass animation:
         *
         * | Animation Step                                                                                 | What the element class attribute looks like |
         * |------------------------------------------------------------------------------------------------|---------------------------------------------|
         * | 1. $animate.addClass(element, 'super') is called                                               | class="my-animation"                        |
         * | 2. $animate runs any JavaScript-defined animations on the element                              | class="my-animation ng-animate"             |
         * | 3. the .super-add class are added to the element                                               | class="my-animation ng-animate super-add"   |
         * | 4. $animate scans the element styles to get the CSS transition/animation duration and delay    | class="my-animation ng-animate super-add"   |
         * | 5. $animate waits for 10ms (this performs a reflow)                                            | class="my-animation ng-animate super-add"   |
         * | 6. the .super, .super-add-active and .ng-animate-active classes are added (this triggers the CSS transition/animation) | class="my-animation ng-animate ng-animate-active super super-add super-add-active"          |
         * | 7. $animate waits for X milliseconds for the animation to complete                             | class="my-animation super super-add super-add-active"  |
         * | 8. The animation ends and all generated CSS classes are removed from the element               | class="my-animation super"                  |
         * | 9. The super class is kept on the element                                                      | class="my-animation super"                  |
         * | 10. The doneCallback() callback is fired (if provided)                                         | class="my-animation super"                  |
         *
         * @param {DOMElement} element the element that will be animated
         * @param {string} className the CSS class that will be added to the element and then animated
         * @param {function()=} doneCallback the callback function that will be called once the animation is complete
        */
        addClass : function(element, className, doneCallback) {
          element = stripCommentsFromElement(element);
          performAnimation('addClass', className, element, null, null, function() {
            $delegate.addClass(element, className);
          }, doneCallback);
        },

        /**
         * @ngdoc method
         * @name $animate#removeClass
         *
         * @description
         * Triggers a custom animation event based off the className variable and then removes the CSS class provided by the className value
         * from the element. Unlike the other animation methods, the animate service will suffix the className value with {@type -remove} in
         * order to provide the animate service the setup and active CSS classes in order to trigger the animation (this will be skipped if
         * no CSS transitions or keyframes are defined on the -remove or base CSS classes).
         *
         * Below is a breakdown of each step that occurs during removeClass animation:
         *
         * | Animation Step                                                                                | What the element class attribute looks like     |
         * |-----------------------------------------------------------------------------------------------|---------------------------------------------|
         * | 1. $animate.removeClass(element, 'super') is called                                           | class="my-animation super"                  |
         * | 2. $animate runs any JavaScript-defined animations on the element                             | class="my-animation super ng-animate"       |
         * | 3. the .super-remove class are added to the element                                           | class="my-animation super ng-animate super-remove"|
         * | 4. $animate scans the element styles to get the CSS transition/animation duration and delay   | class="my-animation super ng-animate super-remove"   |
         * | 5. $animate waits for 10ms (this performs a reflow)                                           | class="my-animation super ng-animate super-remove"   |
         * | 6. the .super-remove-active and .ng-animate-active classes are added and .super is removed (this triggers the CSS transition/animation) | class="my-animation ng-animate ng-animate-active super-remove super-remove-active"          |
         * | 7. $animate waits for X milliseconds for the animation to complete                            | class="my-animation ng-animate ng-animate-active super-remove super-remove-active"   |
         * | 8. The animation ends and all generated CSS classes are removed from the element              | class="my-animation"                        |
         * | 9. The doneCallback() callback is fired (if provided)                                         | class="my-animation"                        |
         *
         *
         * @param {DOMElement} element the element that will be animated
         * @param {string} className the CSS class that will be animated and then removed from the element
         * @param {function()=} doneCallback the callback function that will be called once the animation is complete
        */
        removeClass : function(element, className, doneCallback) {
          element = stripCommentsFromElement(element);
          performAnimation('removeClass', className, element, null, null, function() {
            $delegate.removeClass(element, className);
          }, doneCallback);
        },

          /**
           *
           * @ngdoc function
           * @name $animate#setClass
           * @function
           * @description Adds and/or removes the given CSS classes to and from the element.
           * Once complete, the done() callback will be fired (if provided).
           * @param {DOMElement} element the element which will it's CSS classes changed
           *   removed from it
           * @param {string} add the CSS classes which will be added to the element
           * @param {string} remove the CSS class which will be removed from the element
           * @param {Function=} done the callback function (if provided) that will be fired after the
           *   CSS classes have been set on the element
           */
        setClass : function(element, add, remove, doneCallback) {
          element = stripCommentsFromElement(element);
          performAnimation('setClass', [add, remove], element, null, null, function() {
            $delegate.setClass(element, add, remove);
          }, doneCallback);
        },

        /**
         * @ngdoc method
         * @name $animate#enabled
         * @function
         *
         * @param {boolean=} value If provided then set the animation on or off.
         * @param {DOMElement=} element If provided then the element will be used to represent the enable/disable operation
         * @return {boolean} Current animation state.
         *
         * @description
         * Globally enables/disables animations.
         *
        */
        enabled : function(value, element) {
          switch(arguments.length) {
            case 2:
              if(value) {
                cleanup(element);
              } else {
                var data = element.data(NG_ANIMATE_STATE) || {};
                data.disabled = true;
                element.data(NG_ANIMATE_STATE, data);
              }
            break;

            case 1:
              rootAnimateState.disabled = !value;
            break;

            default:
              value = !rootAnimateState.disabled;
            break;
          }
          return !!value;
         }
      };

      /*
        all animations call this shared animation triggering function internally.
        The animationEvent variable refers to the JavaScript animation event that will be triggered
        and the className value is the name of the animation that will be applied within the
        CSS code. Element, parentElement and afterElement are provided DOM elements for the animation
        and the onComplete callback will be fired once the animation is fully complete.
      */
      function performAnimation(animationEvent, className, element, parentElement, afterElement, domOperation, doneCallback) {

        var runner = animationRunner(element, animationEvent, className);
        if(!runner) {
          fireDOMOperation();
          fireBeforeCallbackAsync();
          fireAfterCallbackAsync();
          closeAnimation();
          return;
        }

        className = runner.className;
        var elementEvents = angular.element._data(runner.node);
        elementEvents = elementEvents && elementEvents.events;

        if (!parentElement) {
          parentElement = afterElement ? afterElement.parent() : element.parent();
        }

        var ngAnimateState  = element.data(NG_ANIMATE_STATE) || {};
        var runningAnimations     = ngAnimateState.active || {};
        var totalActiveAnimations = ngAnimateState.totalActive || 0;
        var lastAnimation         = ngAnimateState.last;

        //only allow animations if the currently running animation is not structural
        //or if there is no animation running at all
        var skipAnimations = runner.isClassBased ?
          ngAnimateState.disabled || (lastAnimation && !lastAnimation.isClassBased) :
          false;

        //skip the animation if animations are disabled, a parent is already being animated,
        //the element is not currently attached to the document body or then completely close
        //the animation if any matching animations are not found at all.
        //NOTE: IE8 + IE9 should close properly (run closeAnimation()) in case an animation was found.
        if (skipAnimations || animationsDisabled(element, parentElement)) {
          fireDOMOperation();
          fireBeforeCallbackAsync();
          fireAfterCallbackAsync();
          closeAnimation();
          return;
        }

        var skipAnimation = false;
        if(totalActiveAnimations > 0) {
          var animationsToCancel = [];
          if(!runner.isClassBased) {
            if(animationEvent == 'leave' && runningAnimations['ng-leave']) {
              skipAnimation = true;
            } else {
              //cancel all animations when a structural animation takes place
              for(var klass in runningAnimations) {
                animationsToCancel.push(runningAnimations[klass]);
                cleanup(element, klass);
              }
              runningAnimations = {};
              totalActiveAnimations = 0;
            }
          } else if(lastAnimation.event == 'setClass') {
            animationsToCancel.push(lastAnimation);
            cleanup(element, className);
          }
          else if(runningAnimations[className]) {
            var current = runningAnimations[className];
            if(current.event == animationEvent) {
              skipAnimation = true;
            } else {
              animationsToCancel.push(current);
              cleanup(element, className);
            }
          }

          if(animationsToCancel.length > 0) {
            forEach(animationsToCancel, function(operation) {
              operation.cancel();
            });
          }
        }

        if(runner.isClassBased && !runner.isSetClassOperation && !skipAnimation) {
          skipAnimation = (animationEvent == 'addClass') == element.hasClass(className); //opposite of XOR
        }

        if(skipAnimation) {
          fireBeforeCallbackAsync();
          fireAfterCallbackAsync();
          fireDoneCallbackAsync();
          return;
        }

        if(animationEvent == 'leave') {
          //there's no need to ever remove the listener since the element
          //will be removed (destroyed) after the leave animation ends or
          //is cancelled midway
          element.one('$destroy', function(e) {
            var element = angular.element(this);
            var state = element.data(NG_ANIMATE_STATE);
            if(state) {
              var activeLeaveAnimation = state.active['ng-leave'];
              if(activeLeaveAnimation) {
                activeLeaveAnimation.cancel();
                cleanup(element, 'ng-leave');
              }
            }
          });
        }

        //the ng-animate class does nothing, but it's here to allow for
        //parent animations to find and cancel child animations when needed
        element.addClass(NG_ANIMATE_CLASS_NAME);

        var localAnimationCount = globalAnimationCounter++;
        totalActiveAnimations++;
        runningAnimations[className] = runner;

        element.data(NG_ANIMATE_STATE, {
          last : runner,
          active : runningAnimations,
          index : localAnimationCount,
          totalActive : totalActiveAnimations
        });

        //first we run the before animations and when all of those are complete
        //then we perform the DOM operation and run the next set of animations
        fireBeforeCallbackAsync();
        runner.before(function(cancelled) {
          var data = element.data(NG_ANIMATE_STATE);
          cancelled = cancelled ||
                        !data || !data.active[className] ||
                        (runner.isClassBased && data.active[className].event != animationEvent);

          fireDOMOperation();
          if(cancelled === true) {
            closeAnimation();
          } else {
            fireAfterCallbackAsync();
            runner.after(closeAnimation);
          }
        });

        function fireDOMCallback(animationPhase) {
          var eventName = '$animate:' + animationPhase;
          if(elementEvents && elementEvents[eventName] && elementEvents[eventName].length > 0) {
            $$asyncCallback(function() {
              element.triggerHandler(eventName, {
                event : animationEvent,
                className : className
              });
            });
          }
        }

        function fireBeforeCallbackAsync() {
          fireDOMCallback('before');
        }

        function fireAfterCallbackAsync() {
          fireDOMCallback('after');
        }

        function fireDoneCallbackAsync() {
          fireDOMCallback('close');
          if(doneCallback) {
            $$asyncCallback(function() {
              doneCallback();
            });
          }
        }

        //it is less complicated to use a flag than managing and canceling
        //timeouts containing multiple callbacks.
        function fireDOMOperation() {
          if(!fireDOMOperation.hasBeenRun) {
            fireDOMOperation.hasBeenRun = true;
            domOperation();
          }
        }

        function closeAnimation() {
          if(!closeAnimation.hasBeenRun) {
            closeAnimation.hasBeenRun = true;
            var data = element.data(NG_ANIMATE_STATE);
            if(data) {
              /* only structural animations wait for reflow before removing an
                 animation, but class-based animations don't. An example of this
                 failing would be when a parent HTML tag has a ng-class attribute
                 causing ALL directives below to skip animations during the digest */
              if(runner && runner.isClassBased) {
                cleanup(element, className);
              } else {
                $$asyncCallback(function() {
                  var data = element.data(NG_ANIMATE_STATE) || {};
                  if(localAnimationCount == data.index) {
                    cleanup(element, className, animationEvent);
                  }
                });
                element.data(NG_ANIMATE_STATE, data);
              }
            }
            fireDoneCallbackAsync();
          }
        }
      }

      function cancelChildAnimations(element) {
        var node = extractElementNode(element);
        if (node) {
          var nodes = angular.isFunction(node.getElementsByClassName) ?
            node.getElementsByClassName(NG_ANIMATE_CLASS_NAME) :
            node.querySelectorAll('.' + NG_ANIMATE_CLASS_NAME);
          forEach(nodes, function(element) {
            element = angular.element(element);
            var data = element.data(NG_ANIMATE_STATE);
            if(data && data.active) {
              forEach(data.active, function(runner) {
                runner.cancel();
              });
            }
          });
        }
      }

      function cleanup(element, className) {
        if(isMatchingElement(element, $rootElement)) {
          if(!rootAnimateState.disabled) {
            rootAnimateState.running = false;
            rootAnimateState.structural = false;
          }
        } else if(className) {
          var data = element.data(NG_ANIMATE_STATE) || {};

          var removeAnimations = className === true;
          if(!removeAnimations && data.active && data.active[className]) {
            data.totalActive--;
            delete data.active[className];
          }

          if(removeAnimations || !data.totalActive) {
            element.removeClass(NG_ANIMATE_CLASS_NAME);
            element.removeData(NG_ANIMATE_STATE);
          }
        }
      }

      function animationsDisabled(element, parentElement) {
        if (rootAnimateState.disabled) return true;

        if(isMatchingElement(element, $rootElement)) {
          return rootAnimateState.disabled || rootAnimateState.running;
        }

        do {
          //the element did not reach the root element which means that it
          //is not apart of the DOM. Therefore there is no reason to do
          //any animations on it
          if(parentElement.length === 0) break;

          var isRoot = isMatchingElement(parentElement, $rootElement);
          var state = isRoot ? rootAnimateState : parentElement.data(NG_ANIMATE_STATE);
          var result = state && (!!state.disabled || state.running || state.totalActive > 0);
          if(isRoot || result) {
            return result;
          }

          if(isRoot) return true;
        }
        while(parentElement = parentElement.parent());

        return true;
      }
    }]);

    $animateProvider.register('', ['$window', '$sniffer', '$timeout', '$$animateReflow',
                           function($window,   $sniffer,   $timeout,   $$animateReflow) {
      // Detect proper transitionend/animationend event names.
      var CSS_PREFIX = '', TRANSITION_PROP, TRANSITIONEND_EVENT, ANIMATION_PROP, ANIMATIONEND_EVENT;

      // If unprefixed events are not supported but webkit-prefixed are, use the latter.
      // Otherwise, just use W3C names, browsers not supporting them at all will just ignore them.
      // Note: Chrome implements `window.onwebkitanimationend` and doesn't implement `window.onanimationend`
      // but at the same time dispatches the `animationend` event and not `webkitAnimationEnd`.
      // Register both events in case `window.onanimationend` is not supported because of that,
      // do the same for `transitionend` as Safari is likely to exhibit similar behavior.
      // Also, the only modern browser that uses vendor prefixes for transitions/keyframes is webkit
      // therefore there is no reason to test anymore for other vendor prefixes: http://caniuse.com/#search=transition
      if (window.ontransitionend === undefined && window.onwebkittransitionend !== undefined) {
        CSS_PREFIX = '-webkit-';
        TRANSITION_PROP = 'WebkitTransition';
        TRANSITIONEND_EVENT = 'webkitTransitionEnd transitionend';
      } else {
        TRANSITION_PROP = 'transition';
        TRANSITIONEND_EVENT = 'transitionend';
      }

      if (window.onanimationend === undefined && window.onwebkitanimationend !== undefined) {
        CSS_PREFIX = '-webkit-';
        ANIMATION_PROP = 'WebkitAnimation';
        ANIMATIONEND_EVENT = 'webkitAnimationEnd animationend';
      } else {
        ANIMATION_PROP = 'animation';
        ANIMATIONEND_EVENT = 'animationend';
      }

      var DURATION_KEY = 'Duration';
      var PROPERTY_KEY = 'Property';
      var DELAY_KEY = 'Delay';
      var ANIMATION_ITERATION_COUNT_KEY = 'IterationCount';
      var NG_ANIMATE_PARENT_KEY = '$$ngAnimateKey';
      var NG_ANIMATE_CSS_DATA_KEY = '$$ngAnimateCSS3Data';
      var NG_ANIMATE_BLOCK_CLASS_NAME = 'ng-animate-block-transitions';
      var ELAPSED_TIME_MAX_DECIMAL_PLACES = 3;
      var CLOSING_TIME_BUFFER = 1.5;
      var ONE_SECOND = 1000;

      var lookupCache = {};
      var parentCounter = 0;
      var animationReflowQueue = [];
      var cancelAnimationReflow;
      function afterReflow(element, callback) {
        if(cancelAnimationReflow) {
          cancelAnimationReflow();
        }
        animationReflowQueue.push(callback);
        cancelAnimationReflow = $$animateReflow(function() {
          forEach(animationReflowQueue, function(fn) {
            fn();
          });

          animationReflowQueue = [];
          cancelAnimationReflow = null;
          lookupCache = {};
        });
      }

      var closingTimer = null;
      var closingTimestamp = 0;
      var animationElementQueue = [];
      function animationCloseHandler(element, totalTime) {
        var node = extractElementNode(element);
        element = angular.element(node);

        //this item will be garbage collected by the closing
        //animation timeout
        animationElementQueue.push(element);

        //but it may not need to cancel out the existing timeout
        //if the timestamp is less than the previous one
        var futureTimestamp = Date.now() + totalTime;
        if(futureTimestamp <= closingTimestamp) {
          return;
        }

        $timeout.cancel(closingTimer);

        closingTimestamp = futureTimestamp;
        closingTimer = $timeout(function() {
          closeAllAnimations(animationElementQueue);
          animationElementQueue = [];
        }, totalTime, false);
      }

      function closeAllAnimations(elements) {
        forEach(elements, function(element) {
          var elementData = element.data(NG_ANIMATE_CSS_DATA_KEY);
          if(elementData) {
            (elementData.closeAnimationFn || noop)();
          }
        });
      }

      function getElementAnimationDetails(element, cacheKey) {
        var data = cacheKey ? lookupCache[cacheKey] : null;
        if(!data) {
          var transitionDuration = 0;
          var transitionDelay = 0;
          var animationDuration = 0;
          var animationDelay = 0;
          var transitionDelayStyle;
          var animationDelayStyle;
          var transitionDurationStyle;
          var transitionPropertyStyle;

          //we want all the styles defined before and after
          forEach(element, function(element) {
            if (element.nodeType == ELEMENT_NODE) {
              var elementStyles = $window.getComputedStyle(element) || {};

              transitionDurationStyle = elementStyles[TRANSITION_PROP + DURATION_KEY];

              transitionDuration = Math.max(parseMaxTime(transitionDurationStyle), transitionDuration);

              transitionPropertyStyle = elementStyles[TRANSITION_PROP + PROPERTY_KEY];

              transitionDelayStyle = elementStyles[TRANSITION_PROP + DELAY_KEY];

              transitionDelay  = Math.max(parseMaxTime(transitionDelayStyle), transitionDelay);

              animationDelayStyle = elementStyles[ANIMATION_PROP + DELAY_KEY];

              animationDelay   = Math.max(parseMaxTime(animationDelayStyle), animationDelay);

              var aDuration  = parseMaxTime(elementStyles[ANIMATION_PROP + DURATION_KEY]);

              if(aDuration > 0) {
                aDuration *= parseInt(elementStyles[ANIMATION_PROP + ANIMATION_ITERATION_COUNT_KEY], 10) || 1;
              }

              animationDuration = Math.max(aDuration, animationDuration);
            }
          });
          data = {
            total : 0,
            transitionPropertyStyle: transitionPropertyStyle,
            transitionDurationStyle: transitionDurationStyle,
            transitionDelayStyle: transitionDelayStyle,
            transitionDelay: transitionDelay,
            transitionDuration: transitionDuration,
            animationDelayStyle: animationDelayStyle,
            animationDelay: animationDelay,
            animationDuration: animationDuration
          };
          if(cacheKey) {
            lookupCache[cacheKey] = data;
          }
        }
        return data;
      }

      function parseMaxTime(str) {
        var maxValue = 0;
        var values = angular.isString(str) ?
          str.split(/\s*,\s*/) :
          [];
        forEach(values, function(value) {
          maxValue = Math.max(parseFloat(value) || 0, maxValue);
        });
        return maxValue;
      }

      function getCacheKey(element) {
        var parentElement = element.parent();
        var parentID = parentElement.data(NG_ANIMATE_PARENT_KEY);
        if(!parentID) {
          parentElement.data(NG_ANIMATE_PARENT_KEY, ++parentCounter);
          parentID = parentCounter;
        }
        return parentID + '-' + extractElementNode(element).getAttribute('class');
      }

      function animateSetup(animationEvent, element, className, calculationDecorator) {
        var cacheKey = getCacheKey(element);
        var eventCacheKey = cacheKey + ' ' + className;
        var itemIndex = lookupCache[eventCacheKey] ? ++lookupCache[eventCacheKey].total : 0;

        var stagger = {};
        if(itemIndex > 0) {
          var staggerClassName = className + '-stagger';
          var staggerCacheKey = cacheKey + ' ' + staggerClassName;
          var applyClasses = !lookupCache[staggerCacheKey];

          applyClasses && element.addClass(staggerClassName);

          stagger = getElementAnimationDetails(element, staggerCacheKey);

          applyClasses && element.removeClass(staggerClassName);
        }

        /* the animation itself may need to add/remove special CSS classes
         * before calculating the anmation styles */
        calculationDecorator = calculationDecorator ||
                               function(fn) { return fn(); };

        element.addClass(className);

        var formerData = element.data(NG_ANIMATE_CSS_DATA_KEY) || {};

        var timings = calculationDecorator(function() {
          return getElementAnimationDetails(element, eventCacheKey);
        });

        var transitionDuration = timings.transitionDuration;
        var animationDuration = timings.animationDuration;
        if(transitionDuration === 0 && animationDuration === 0) {
          element.removeClass(className);
          return false;
        }

        element.data(NG_ANIMATE_CSS_DATA_KEY, {
          running : formerData.running || 0,
          itemIndex : itemIndex,
          stagger : stagger,
          timings : timings,
          closeAnimationFn : noop
        });

        //temporarily disable the transition so that the enter styles
        //don't animate twice (this is here to avoid a bug in Chrome/FF).
        var isCurrentlyAnimating = formerData.running > 0 || animationEvent == 'setClass';
        if(transitionDuration > 0) {
          blockTransitions(element, className, isCurrentlyAnimating);
        }

        //staggering keyframe animations work by adjusting the `animation-delay` CSS property
        //on the given element, however, the delay value can only calculated after the reflow
        //since by that time $animate knows how many elements are being animated. Therefore,
        //until the reflow occurs the element needs to be blocked (where the keyframe animation
        //is set to `none 0s`). This blocking mechanism should only be set for when a stagger
        //animation is detected and when the element item index is greater than 0.
        if(animationDuration > 0 && stagger.animationDelay > 0 && stagger.animationDuration === 0) {
          blockKeyframeAnimations(element);
        }

        return true;
      }

      function isStructuralAnimation(className) {
        return className == 'ng-enter' || className == 'ng-move' || className == 'ng-leave';
      }

      function blockTransitions(element, className, isAnimating) {
        if(isStructuralAnimation(className) || !isAnimating) {
          extractElementNode(element).style[TRANSITION_PROP + PROPERTY_KEY] = 'none';
        } else {
          element.addClass(NG_ANIMATE_BLOCK_CLASS_NAME);
        }
      }

      function blockKeyframeAnimations(element) {
        extractElementNode(element).style[ANIMATION_PROP] = 'none 0s';
      }

      function unblockTransitions(element, className) {
        var prop = TRANSITION_PROP + PROPERTY_KEY;
        var node = extractElementNode(element);
        if(node.style[prop] && node.style[prop].length > 0) {
          node.style[prop] = '';
        }
        element.removeClass(NG_ANIMATE_BLOCK_CLASS_NAME);
      }

      function unblockKeyframeAnimations(element) {
        var prop = ANIMATION_PROP;
        var node = extractElementNode(element);
        if(node.style[prop] && node.style[prop].length > 0) {
          node.style[prop] = '';
        }
      }

      function animateRun(animationEvent, element, className, activeAnimationComplete) {
        var node = extractElementNode(element);
        var elementData = element.data(NG_ANIMATE_CSS_DATA_KEY);
        if(node.getAttribute('class').indexOf(className) == -1 || !elementData) {
          activeAnimationComplete();
          return;
        }

        var activeClassName = '';
        forEach(className.split(' '), function(klass, i) {
          activeClassName += (i > 0 ? ' ' : '') + klass + '-active';
        });

        var stagger = elementData.stagger;
        var timings = elementData.timings;
        var itemIndex = elementData.itemIndex;
        var maxDuration = Math.max(timings.transitionDuration, timings.animationDuration);
        var maxDelay = Math.max(timings.transitionDelay, timings.animationDelay);
        var maxDelayTime = maxDelay * ONE_SECOND;

        var startTime = Date.now();
        var css3AnimationEvents = ANIMATIONEND_EVENT + ' ' + TRANSITIONEND_EVENT;

        var style = '', appliedStyles = [];
        if(timings.transitionDuration > 0) {
          var propertyStyle = timings.transitionPropertyStyle;
          if(propertyStyle.indexOf('all') == -1) {
            style += CSS_PREFIX + 'transition-property: ' + propertyStyle + ';';
            style += CSS_PREFIX + 'transition-duration: ' + timings.transitionDurationStyle + ';';
            appliedStyles.push(CSS_PREFIX + 'transition-property');
            appliedStyles.push(CSS_PREFIX + 'transition-duration');
          }
        }

        if(itemIndex > 0) {
          if(stagger.transitionDelay > 0 && stagger.transitionDuration === 0) {
            var delayStyle = timings.transitionDelayStyle;
            style += CSS_PREFIX + 'transition-delay: ' +
                     prepareStaggerDelay(delayStyle, stagger.transitionDelay, itemIndex) + '; ';
            appliedStyles.push(CSS_PREFIX + 'transition-delay');
          }

          if(stagger.animationDelay > 0 && stagger.animationDuration === 0) {
            style += CSS_PREFIX + 'animation-delay: ' +
                     prepareStaggerDelay(timings.animationDelayStyle, stagger.animationDelay, itemIndex) + '; ';
            appliedStyles.push(CSS_PREFIX + 'animation-delay');
          }
        }

        if(appliedStyles.length > 0) {
          //the element being animated may sometimes contain comment nodes in
          //the jqLite object, so we're safe to use a single variable to house
          //the styles since there is always only one element being animated
          var oldStyle = node.getAttribute('style') || '';
          node.setAttribute('style', oldStyle + ' ' + style);
        }

        element.on(css3AnimationEvents, onAnimationProgress);
        element.addClass(activeClassName);
        elementData.closeAnimationFn = function() {
          onEnd();
          activeAnimationComplete();
        };

        var staggerTime       = itemIndex * (Math.max(stagger.animationDelay, stagger.transitionDelay) || 0);
        var animationTime     = (maxDelay + maxDuration) * CLOSING_TIME_BUFFER;
        var totalTime         = (staggerTime + animationTime) * ONE_SECOND;

        elementData.running++;
        animationCloseHandler(element, totalTime);
        return onEnd;

        // This will automatically be called by $animate so
        // there is no need to attach this internally to the
        // timeout done method.
        function onEnd(cancelled) {
          element.off(css3AnimationEvents, onAnimationProgress);
          element.removeClass(activeClassName);
          animateClose(element, className);
          var node = extractElementNode(element);
          for (var i in appliedStyles) {
            node.style.removeProperty(appliedStyles[i]);
          }
        }

        function onAnimationProgress(event) {
          event.stopPropagation();
          var ev = event.originalEvent || event;
          var timeStamp = ev.$manualTimeStamp || ev.timeStamp || Date.now();

          /* Firefox (or possibly just Gecko) likes to not round values up
           * when a ms measurement is used for the animation */
          var elapsedTime = parseFloat(ev.elapsedTime.toFixed(ELAPSED_TIME_MAX_DECIMAL_PLACES));

          /* $manualTimeStamp is a mocked timeStamp value which is set
           * within browserTrigger(). This is only here so that tests can
           * mock animations properly. Real events fallback to event.timeStamp,
           * or, if they don't, then a timeStamp is automatically created for them.
           * We're checking to see if the timeStamp surpasses the expected delay,
           * but we're using elapsedTime instead of the timeStamp on the 2nd
           * pre-condition since animations sometimes close off early */
          if(Math.max(timeStamp - startTime, 0) >= maxDelayTime && elapsedTime >= maxDuration) {
            activeAnimationComplete();
          }
        }
      }

      function prepareStaggerDelay(delayStyle, staggerDelay, index) {
        var style = '';
        forEach(delayStyle.split(','), function(val, i) {
          style += (i > 0 ? ',' : '') +
                   (index * staggerDelay + parseInt(val, 10)) + 's';
        });
        return style;
      }

      function animateBefore(animationEvent, element, className, calculationDecorator) {
        if(animateSetup(animationEvent, element, className, calculationDecorator)) {
          return function(cancelled) {
            cancelled && animateClose(element, className);
          };
        }
      }

      function animateAfter(animationEvent, element, className, afterAnimationComplete) {
        if(element.data(NG_ANIMATE_CSS_DATA_KEY)) {
          return animateRun(animationEvent, element, className, afterAnimationComplete);
        } else {
          animateClose(element, className);
          afterAnimationComplete();
        }
      }

      function animate(animationEvent, element, className, animationComplete) {
        //If the animateSetup function doesn't bother returning a
        //cancellation function then it means that there is no animation
        //to perform at all
        var preReflowCancellation = animateBefore(animationEvent, element, className);
        if(!preReflowCancellation) {
          animationComplete();
          return;
        }

        //There are two cancellation functions: one is before the first
        //reflow animation and the second is during the active state
        //animation. The first function will take care of removing the
        //data from the element which will not make the 2nd animation
        //happen in the first place
        var cancel = preReflowCancellation;
        afterReflow(element, function() {
          unblockTransitions(element, className);
          unblockKeyframeAnimations(element);
          //once the reflow is complete then we point cancel to
          //the new cancellation function which will remove all of the
          //animation properties from the active animation
          cancel = animateAfter(animationEvent, element, className, animationComplete);
        });

        return function(cancelled) {
          (cancel || noop)(cancelled);
        };
      }

      function animateClose(element, className) {
        element.removeClass(className);
        var data = element.data(NG_ANIMATE_CSS_DATA_KEY);
        if(data) {
          if(data.running) {
            data.running--;
          }
          if(!data.running || data.running === 0) {
            element.removeData(NG_ANIMATE_CSS_DATA_KEY);
          }
        }
      }

      return {
        enter : function(element, animationCompleted) {
          return animate('enter', element, 'ng-enter', animationCompleted);
        },

        leave : function(element, animationCompleted) {
          return animate('leave', element, 'ng-leave', animationCompleted);
        },

        move : function(element, animationCompleted) {
          return animate('move', element, 'ng-move', animationCompleted);
        },

        beforeSetClass : function(element, add, remove, animationCompleted) {
          var className = suffixClasses(remove, '-remove') + ' ' +
                          suffixClasses(add, '-add');
          var cancellationMethod = animateBefore('setClass', element, className, function(fn) {
            /* when classes are removed from an element then the transition style
             * that is applied is the transition defined on the element without the
             * CSS class being there. This is how CSS3 functions outside of ngAnimate.
             * http://plnkr.co/edit/j8OzgTNxHTb4n3zLyjGW?p=preview */
            var klass = element.attr('class');
            element.removeClass(remove);
            element.addClass(add);
            var timings = fn();
            element.attr('class', klass);
            return timings;
          });

          if(cancellationMethod) {
            afterReflow(element, function() {
              unblockTransitions(element, className);
              unblockKeyframeAnimations(element);
              animationCompleted();
            });
            return cancellationMethod;
          }
          animationCompleted();
        },

        beforeAddClass : function(element, className, animationCompleted) {
          var cancellationMethod = animateBefore('addClass', element, suffixClasses(className, '-add'), function(fn) {

            /* when a CSS class is added to an element then the transition style that
             * is applied is the transition defined on the element when the CSS class
             * is added at the time of the animation. This is how CSS3 functions
             * outside of ngAnimate. */
            element.addClass(className);
            var timings = fn();
            element.removeClass(className);
            return timings;
          });

          if(cancellationMethod) {
            afterReflow(element, function() {
              unblockTransitions(element, className);
              unblockKeyframeAnimations(element);
              animationCompleted();
            });
            return cancellationMethod;
          }
          animationCompleted();
        },

        setClass : function(element, add, remove, animationCompleted) {
          remove = suffixClasses(remove, '-remove');
          add = suffixClasses(add, '-add');
          var className = remove + ' ' + add;
          return animateAfter('setClass', element, className, animationCompleted);
        },

        addClass : function(element, className, animationCompleted) {
          return animateAfter('addClass', element, suffixClasses(className, '-add'), animationCompleted);
        },

        beforeRemoveClass : function(element, className, animationCompleted) {
          var cancellationMethod = animateBefore('removeClass', element, suffixClasses(className, '-remove'), function(fn) {
            /* when classes are removed from an element then the transition style
             * that is applied is the transition defined on the element without the
             * CSS class being there. This is how CSS3 functions outside of ngAnimate.
             * http://plnkr.co/edit/j8OzgTNxHTb4n3zLyjGW?p=preview */
            var klass = element.attr('class');
            element.removeClass(className);
            var timings = fn();
            element.attr('class', klass);
            return timings;
          });

          if(cancellationMethod) {
            afterReflow(element, function() {
              unblockTransitions(element, className);
              unblockKeyframeAnimations(element);
              animationCompleted();
            });
            return cancellationMethod;
          }
          animationCompleted();
        },

        removeClass : function(element, className, animationCompleted) {
          return animateAfter('removeClass', element, suffixClasses(className, '-remove'), animationCompleted);
        }
      };

      function suffixClasses(classes, suffix) {
        var className = '';
        classes = angular.isArray(classes) ? classes : classes.split(/\s+/);
        forEach(classes, function(klass, i) {
          if(klass && klass.length > 0) {
            className += (i > 0 ? ' ' : '') + klass + suffix;
          }
        });
        return className;
      }
    }]);
  }]);


})(window, window.angular);

/**
 * @license
 * Lo-Dash 2.4.1 (Custom Build) <http://lodash.com/>
 * Build: `lodash modern -o ./dist/lodash.js`
 * Copyright 2012-2013 The Dojo Foundation <http://dojofoundation.org/>
 * Based on Underscore.js 1.5.2 <http://underscorejs.org/LICENSE>
 * Copyright 2009-2013 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
 * Available under MIT license <http://lodash.com/license>
 */
;(function() {

  /** Used as a safe reference for `undefined` in pre ES5 environments */
  var undefined;

  /** Used to pool arrays and objects used internally */
  var arrayPool = [],
      objectPool = [];

  /** Used to generate unique IDs */
  var idCounter = 0;

  /** Used to prefix keys to avoid issues with `__proto__` and properties on `Object.prototype` */
  var keyPrefix = +new Date + '';

  /** Used as the size when optimizations are enabled for large arrays */
  var largeArraySize = 75;

  /** Used as the max size of the `arrayPool` and `objectPool` */
  var maxPoolSize = 40;

  /** Used to detect and test whitespace */
  var whitespace = (
    // whitespace
    ' \t\x0B\f\xA0\ufeff' +

    // line terminators
    '\n\r\u2028\u2029' +

    // unicode category "Zs" space separators
    '\u1680\u180e\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u202f\u205f\u3000'
  );

  /** Used to match empty string literals in compiled template source */
  var reEmptyStringLeading = /\b__p \+= '';/g,
      reEmptyStringMiddle = /\b(__p \+=) '' \+/g,
      reEmptyStringTrailing = /(__e\(.*?\)|\b__t\)) \+\n'';/g;

  /**
   * Used to match ES6 template delimiters
   * http://people.mozilla.org/~jorendorff/es6-draft.html#sec-literals-string-literals
   */
  var reEsTemplate = /\$\{([^\\}]*(?:\\.[^\\}]*)*)\}/g;

  /** Used to match regexp flags from their coerced string values */
  var reFlags = /\w*$/;

  /** Used to detected named functions */
  var reFuncName = /^\s*function[ \n\r\t]+\w/;

  /** Used to match "interpolate" template delimiters */
  var reInterpolate = /<%=([\s\S]+?)%>/g;

  /** Used to match leading whitespace and zeros to be removed */
  var reLeadingSpacesAndZeros = RegExp('^[' + whitespace + ']*0+(?=.$)');

  /** Used to ensure capturing order of template delimiters */
  var reNoMatch = /($^)/;

  /** Used to detect functions containing a `this` reference */
  var reThis = /\bthis\b/;

  /** Used to match unescaped characters in compiled string literals */
  var reUnescapedString = /['\n\r\t\u2028\u2029\\]/g;

  /** Used to assign default `context` object properties */
  var contextProps = [
    'Array', 'Boolean', 'Date', 'Function', 'Math', 'Number', 'Object',
    'RegExp', 'String', '_', 'attachEvent', 'clearTimeout', 'isFinite', 'isNaN',
    'parseInt', 'setTimeout'
  ];

  /** Used to make template sourceURLs easier to identify */
  var templateCounter = 0;

  /** `Object#toString` result shortcuts */
  var argsClass = '[object Arguments]',
      arrayClass = '[object Array]',
      boolClass = '[object Boolean]',
      dateClass = '[object Date]',
      funcClass = '[object Function]',
      numberClass = '[object Number]',
      objectClass = '[object Object]',
      regexpClass = '[object RegExp]',
      stringClass = '[object String]';

  /** Used to identify object classifications that `_.clone` supports */
  var cloneableClasses = {};
  cloneableClasses[funcClass] = false;
  cloneableClasses[argsClass] = cloneableClasses[arrayClass] =
  cloneableClasses[boolClass] = cloneableClasses[dateClass] =
  cloneableClasses[numberClass] = cloneableClasses[objectClass] =
  cloneableClasses[regexpClass] = cloneableClasses[stringClass] = true;

  /** Used as an internal `_.debounce` options object */
  var debounceOptions = {
    'leading': false,
    'maxWait': 0,
    'trailing': false
  };

  /** Used as the property descriptor for `__bindData__` */
  var descriptor = {
    'configurable': false,
    'enumerable': false,
    'value': null,
    'writable': false
  };

  /** Used to determine if values are of the language type Object */
  var objectTypes = {
    'boolean': false,
    'function': true,
    'object': true,
    'number': false,
    'string': false,
    'undefined': false
  };

  /** Used to escape characters for inclusion in compiled string literals */
  var stringEscapes = {
    '\\': '\\',
    "'": "'",
    '\n': 'n',
    '\r': 'r',
    '\t': 't',
    '\u2028': 'u2028',
    '\u2029': 'u2029'
  };

  /** Used as a reference to the global object */
  var root = (objectTypes[typeof window] && window) || this;

  /** Detect free variable `exports` */
  var freeExports = objectTypes[typeof exports] && exports && !exports.nodeType && exports;

  /** Detect free variable `module` */
  var freeModule = objectTypes[typeof module] && module && !module.nodeType && module;

  /** Detect the popular CommonJS extension `module.exports` */
  var moduleExports = freeModule && freeModule.exports === freeExports && freeExports;

  /** Detect free variable `global` from Node.js or Browserified code and use it as `root` */
  var freeGlobal = objectTypes[typeof global] && global;
  if (freeGlobal && (freeGlobal.global === freeGlobal || freeGlobal.window === freeGlobal)) {
    root = freeGlobal;
  }

  /*--------------------------------------------------------------------------*/

  /**
   * The base implementation of `_.indexOf` without support for binary searches
   * or `fromIndex` constraints.
   *
   * @private
   * @param {Array} array The array to search.
   * @param {*} value The value to search for.
   * @param {number} [fromIndex=0] The index to search from.
   * @returns {number} Returns the index of the matched value or `-1`.
   */
  function baseIndexOf(array, value, fromIndex) {
    var index = (fromIndex || 0) - 1,
        length = array ? array.length : 0;

    while (++index < length) {
      if (array[index] === value) {
        return index;
      }
    }
    return -1;
  }

  /**
   * An implementation of `_.contains` for cache objects that mimics the return
   * signature of `_.indexOf` by returning `0` if the value is found, else `-1`.
   *
   * @private
   * @param {Object} cache The cache object to inspect.
   * @param {*} value The value to search for.
   * @returns {number} Returns `0` if `value` is found, else `-1`.
   */
  function cacheIndexOf(cache, value) {
    var type = typeof value;
    cache = cache.cache;

    if (type == 'boolean' || value == null) {
      return cache[value] ? 0 : -1;
    }
    if (type != 'number' && type != 'string') {
      type = 'object';
    }
    var key = type == 'number' ? value : keyPrefix + value;
    cache = (cache = cache[type]) && cache[key];

    return type == 'object'
      ? (cache && baseIndexOf(cache, value) > -1 ? 0 : -1)
      : (cache ? 0 : -1);
  }

  /**
   * Adds a given value to the corresponding cache object.
   *
   * @private
   * @param {*} value The value to add to the cache.
   */
  function cachePush(value) {
    var cache = this.cache,
        type = typeof value;

    if (type == 'boolean' || value == null) {
      cache[value] = true;
    } else {
      if (type != 'number' && type != 'string') {
        type = 'object';
      }
      var key = type == 'number' ? value : keyPrefix + value,
          typeCache = cache[type] || (cache[type] = {});

      if (type == 'object') {
        (typeCache[key] || (typeCache[key] = [])).push(value);
      } else {
        typeCache[key] = true;
      }
    }
  }

  /**
   * Used by `_.max` and `_.min` as the default callback when a given
   * collection is a string value.
   *
   * @private
   * @param {string} value The character to inspect.
   * @returns {number} Returns the code unit of given character.
   */
  function charAtCallback(value) {
    return value.charCodeAt(0);
  }

  /**
   * Used by `sortBy` to compare transformed `collection` elements, stable sorting
   * them in ascending order.
   *
   * @private
   * @param {Object} a The object to compare to `b`.
   * @param {Object} b The object to compare to `a`.
   * @returns {number} Returns the sort order indicator of `1` or `-1`.
   */
  function compareAscending(a, b) {
    var ac = a.criteria,
        bc = b.criteria,
        index = -1,
        length = ac.length;

    while (++index < length) {
      var value = ac[index],
          other = bc[index];

      if (value !== other) {
        if (value > other || typeof value == 'undefined') {
          return 1;
        }
        if (value < other || typeof other == 'undefined') {
          return -1;
        }
      }
    }
    // Fixes an `Array#sort` bug in the JS engine embedded in Adobe applications
    // that causes it, under certain circumstances, to return the same value for
    // `a` and `b`. See https://github.com/jashkenas/underscore/pull/1247
    //
    // This also ensures a stable sort in V8 and other engines.
    // See http://code.google.com/p/v8/issues/detail?id=90
    return a.index - b.index;
  }

  /**
   * Creates a cache object to optimize linear searches of large arrays.
   *
   * @private
   * @param {Array} [array=[]] The array to search.
   * @returns {null|Object} Returns the cache object or `null` if caching should not be used.
   */
  function createCache(array) {
    var index = -1,
        length = array.length,
        first = array[0],
        mid = array[(length / 2) | 0],
        last = array[length - 1];

    if (first && typeof first == 'object' &&
        mid && typeof mid == 'object' && last && typeof last == 'object') {
      return false;
    }
    var cache = getObject();
    cache['false'] = cache['null'] = cache['true'] = cache['undefined'] = false;

    var result = getObject();
    result.array = array;
    result.cache = cache;
    result.push = cachePush;

    while (++index < length) {
      result.push(array[index]);
    }
    return result;
  }

  /**
   * Used by `template` to escape characters for inclusion in compiled
   * string literals.
   *
   * @private
   * @param {string} match The matched character to escape.
   * @returns {string} Returns the escaped character.
   */
  function escapeStringChar(match) {
    return '\\' + stringEscapes[match];
  }

  /**
   * Gets an array from the array pool or creates a new one if the pool is empty.
   *
   * @private
   * @returns {Array} The array from the pool.
   */
  function getArray() {
    return arrayPool.pop() || [];
  }

  /**
   * Gets an object from the object pool or creates a new one if the pool is empty.
   *
   * @private
   * @returns {Object} The object from the pool.
   */
  function getObject() {
    return objectPool.pop() || {
      'array': null,
      'cache': null,
      'criteria': null,
      'false': false,
      'index': 0,
      'null': false,
      'number': null,
      'object': null,
      'push': null,
      'string': null,
      'true': false,
      'undefined': false,
      'value': null
    };
  }

  /**
   * Releases the given array back to the array pool.
   *
   * @private
   * @param {Array} [array] The array to release.
   */
  function releaseArray(array) {
    array.length = 0;
    if (arrayPool.length < maxPoolSize) {
      arrayPool.push(array);
    }
  }

  /**
   * Releases the given object back to the object pool.
   *
   * @private
   * @param {Object} [object] The object to release.
   */
  function releaseObject(object) {
    var cache = object.cache;
    if (cache) {
      releaseObject(cache);
    }
    object.array = object.cache = object.criteria = object.object = object.number = object.string = object.value = null;
    if (objectPool.length < maxPoolSize) {
      objectPool.push(object);
    }
  }

  /**
   * Slices the `collection` from the `start` index up to, but not including,
   * the `end` index.
   *
   * Note: This function is used instead of `Array#slice` to support node lists
   * in IE < 9 and to ensure dense arrays are returned.
   *
   * @private
   * @param {Array|Object|string} collection The collection to slice.
   * @param {number} start The start index.
   * @param {number} end The end index.
   * @returns {Array} Returns the new array.
   */
  function slice(array, start, end) {
    start || (start = 0);
    if (typeof end == 'undefined') {
      end = array ? array.length : 0;
    }
    var index = -1,
        length = end - start || 0,
        result = Array(length < 0 ? 0 : length);

    while (++index < length) {
      result[index] = array[start + index];
    }
    return result;
  }

  /*--------------------------------------------------------------------------*/

  /**
   * Create a new `lodash` function using the given context object.
   *
   * @static
   * @memberOf _
   * @category Utilities
   * @param {Object} [context=root] The context object.
   * @returns {Function} Returns the `lodash` function.
   */
  function runInContext(context) {
    // Avoid issues with some ES3 environments that attempt to use values, named
    // after built-in constructors like `Object`, for the creation of literals.
    // ES5 clears this up by stating that literals must use built-in constructors.
    // See http://es5.github.io/#x11.1.5.
    context = context ? _.defaults(root.Object(), context, _.pick(root, contextProps)) : root;

    /** Native constructor references */
    var Array = context.Array,
        Boolean = context.Boolean,
        Date = context.Date,
        Function = context.Function,
        Math = context.Math,
        Number = context.Number,
        Object = context.Object,
        RegExp = context.RegExp,
        String = context.String,
        TypeError = context.TypeError;

    /**
     * Used for `Array` method references.
     *
     * Normally `Array.prototype` would suffice, however, using an array literal
     * avoids issues in Narwhal.
     */
    var arrayRef = [];

    /** Used for native method references */
    var objectProto = Object.prototype;

    /** Used to restore the original `_` reference in `noConflict` */
    var oldDash = context._;

    /** Used to resolve the internal [[Class]] of values */
    var toString = objectProto.toString;

    /** Used to detect if a method is native */
    var reNative = RegExp('^' +
      String(toString)
        .replace(/[.*+?^${}()|[\]\\]/g, '\\$&')
        .replace(/toString| for [^\]]+/g, '.*?') + '$'
    );

    /** Native method shortcuts */
    var ceil = Math.ceil,
        clearTimeout = context.clearTimeout,
        floor = Math.floor,
        fnToString = Function.prototype.toString,
        getPrototypeOf = isNative(getPrototypeOf = Object.getPrototypeOf) && getPrototypeOf,
        hasOwnProperty = objectProto.hasOwnProperty,
        push = arrayRef.push,
        setTimeout = context.setTimeout,
        splice = arrayRef.splice,
        unshift = arrayRef.unshift;

    /** Used to set meta data on functions */
    var defineProperty = (function() {
      // IE 8 only accepts DOM elements
      try {
        var o = {},
            func = isNative(func = Object.defineProperty) && func,
            result = func(o, o, o) && func;
      } catch(e) { }
      return result;
    }());

    /* Native method shortcuts for methods with the same name as other `lodash` methods */
    var nativeCreate = isNative(nativeCreate = Object.create) && nativeCreate,
        nativeIsArray = isNative(nativeIsArray = Array.isArray) && nativeIsArray,
        nativeIsFinite = context.isFinite,
        nativeIsNaN = context.isNaN,
        nativeKeys = isNative(nativeKeys = Object.keys) && nativeKeys,
        nativeMax = Math.max,
        nativeMin = Math.min,
        nativeParseInt = context.parseInt,
        nativeRandom = Math.random;

    /** Used to lookup a built-in constructor by [[Class]] */
    var ctorByClass = {};
    ctorByClass[arrayClass] = Array;
    ctorByClass[boolClass] = Boolean;
    ctorByClass[dateClass] = Date;
    ctorByClass[funcClass] = Function;
    ctorByClass[objectClass] = Object;
    ctorByClass[numberClass] = Number;
    ctorByClass[regexpClass] = RegExp;
    ctorByClass[stringClass] = String;

    /*--------------------------------------------------------------------------*/

    /**
     * Creates a `lodash` object which wraps the given value to enable intuitive
     * method chaining.
     *
     * In addition to Lo-Dash methods, wrappers also have the following `Array` methods:
     * `concat`, `join`, `pop`, `push`, `reverse`, `shift`, `slice`, `sort`, `splice`,
     * and `unshift`
     *
     * Chaining is supported in custom builds as long as the `value` method is
     * implicitly or explicitly included in the build.
     *
     * The chainable wrapper functions are:
     * `after`, `assign`, `bind`, `bindAll`, `bindKey`, `chain`, `compact`,
     * `compose`, `concat`, `countBy`, `create`, `createCallback`, `curry`,
     * `debounce`, `defaults`, `defer`, `delay`, `difference`, `filter`, `flatten`,
     * `forEach`, `forEachRight`, `forIn`, `forInRight`, `forOwn`, `forOwnRight`,
     * `functions`, `groupBy`, `indexBy`, `initial`, `intersection`, `invert`,
     * `invoke`, `keys`, `map`, `max`, `memoize`, `merge`, `min`, `object`, `omit`,
     * `once`, `pairs`, `partial`, `partialRight`, `pick`, `pluck`, `pull`, `push`,
     * `range`, `reject`, `remove`, `rest`, `reverse`, `shuffle`, `slice`, `sort`,
     * `sortBy`, `splice`, `tap`, `throttle`, `times`, `toArray`, `transform`,
     * `union`, `uniq`, `unshift`, `unzip`, `values`, `where`, `without`, `wrap`,
     * and `zip`
     *
     * The non-chainable wrapper functions are:
     * `clone`, `cloneDeep`, `contains`, `escape`, `every`, `find`, `findIndex`,
     * `findKey`, `findLast`, `findLastIndex`, `findLastKey`, `has`, `identity`,
     * `indexOf`, `isArguments`, `isArray`, `isBoolean`, `isDate`, `isElement`,
     * `isEmpty`, `isEqual`, `isFinite`, `isFunction`, `isNaN`, `isNull`, `isNumber`,
     * `isObject`, `isPlainObject`, `isRegExp`, `isString`, `isUndefined`, `join`,
     * `lastIndexOf`, `mixin`, `noConflict`, `parseInt`, `pop`, `random`, `reduce`,
     * `reduceRight`, `result`, `shift`, `size`, `some`, `sortedIndex`, `runInContext`,
     * `template`, `unescape`, `uniqueId`, and `value`
     *
     * The wrapper functions `first` and `last` return wrapped values when `n` is
     * provided, otherwise they return unwrapped values.
     *
     * Explicit chaining can be enabled by using the `_.chain` method.
     *
     * @name _
     * @constructor
     * @category Chaining
     * @param {*} value The value to wrap in a `lodash` instance.
     * @returns {Object} Returns a `lodash` instance.
     * @example
     *
     * var wrapped = _([1, 2, 3]);
     *
     * // returns an unwrapped value
     * wrapped.reduce(function(sum, num) {
     *   return sum + num;
     * });
     * // => 6
     *
     * // returns a wrapped value
     * var squares = wrapped.map(function(num) {
     *   return num * num;
     * });
     *
     * _.isArray(squares);
     * // => false
     *
     * _.isArray(squares.value());
     * // => true
     */
    function lodash(value) {
      // don't wrap if already wrapped, even if wrapped by a different `lodash` constructor
      return (value && typeof value == 'object' && !isArray(value) && hasOwnProperty.call(value, '__wrapped__'))
       ? value
       : new lodashWrapper(value);
    }

    /**
     * A fast path for creating `lodash` wrapper objects.
     *
     * @private
     * @param {*} value The value to wrap in a `lodash` instance.
     * @param {boolean} chainAll A flag to enable chaining for all methods
     * @returns {Object} Returns a `lodash` instance.
     */
    function lodashWrapper(value, chainAll) {
      this.__chain__ = !!chainAll;
      this.__wrapped__ = value;
    }
    // ensure `new lodashWrapper` is an instance of `lodash`
    lodashWrapper.prototype = lodash.prototype;

    /**
     * An object used to flag environments features.
     *
     * @static
     * @memberOf _
     * @type Object
     */
    var support = lodash.support = {};

    /**
     * Detect if functions can be decompiled by `Function#toString`
     * (all but PS3 and older Opera mobile browsers & avoided in Windows 8 apps).
     *
     * @memberOf _.support
     * @type boolean
     */
    support.funcDecomp = !isNative(context.WinRTError) && reThis.test(runInContext);

    /**
     * Detect if `Function#name` is supported (all but IE).
     *
     * @memberOf _.support
     * @type boolean
     */
    support.funcNames = typeof Function.name == 'string';

    /**
     * By default, the template delimiters used by Lo-Dash are similar to those in
     * embedded Ruby (ERB). Change the following template settings to use alternative
     * delimiters.
     *
     * @static
     * @memberOf _
     * @type Object
     */
    lodash.templateSettings = {

      /**
       * Used to detect `data` property values to be HTML-escaped.
       *
       * @memberOf _.templateSettings
       * @type RegExp
       */
      'escape': /<%-([\s\S]+?)%>/g,

      /**
       * Used to detect code to be evaluated.
       *
       * @memberOf _.templateSettings
       * @type RegExp
       */
      'evaluate': /<%([\s\S]+?)%>/g,

      /**
       * Used to detect `data` property values to inject.
       *
       * @memberOf _.templateSettings
       * @type RegExp
       */
      'interpolate': reInterpolate,

      /**
       * Used to reference the data object in the template text.
       *
       * @memberOf _.templateSettings
       * @type string
       */
      'variable': '',

      /**
       * Used to import variables into the compiled template.
       *
       * @memberOf _.templateSettings
       * @type Object
       */
      'imports': {

        /**
         * A reference to the `lodash` function.
         *
         * @memberOf _.templateSettings.imports
         * @type Function
         */
        '_': lodash
      }
    };

    /*--------------------------------------------------------------------------*/

    /**
     * The base implementation of `_.bind` that creates the bound function and
     * sets its meta data.
     *
     * @private
     * @param {Array} bindData The bind data array.
     * @returns {Function} Returns the new bound function.
     */
    function baseBind(bindData) {
      var func = bindData[0],
          partialArgs = bindData[2],
          thisArg = bindData[4];

      function bound() {
        // `Function#bind` spec
        // http://es5.github.io/#x15.3.4.5
        if (partialArgs) {
          // avoid `arguments` object deoptimizations by using `slice` instead
          // of `Array.prototype.slice.call` and not assigning `arguments` to a
          // variable as a ternary expression
          var args = slice(partialArgs);
          push.apply(args, arguments);
        }
        // mimic the constructor's `return` behavior
        // http://es5.github.io/#x13.2.2
        if (this instanceof bound) {
          // ensure `new bound` is an instance of `func`
          var thisBinding = baseCreate(func.prototype),
              result = func.apply(thisBinding, args || arguments);
          return isObject(result) ? result : thisBinding;
        }
        return func.apply(thisArg, args || arguments);
      }
      setBindData(bound, bindData);
      return bound;
    }

    /**
     * The base implementation of `_.clone` without argument juggling or support
     * for `thisArg` binding.
     *
     * @private
     * @param {*} value The value to clone.
     * @param {boolean} [isDeep=false] Specify a deep clone.
     * @param {Function} [callback] The function to customize cloning values.
     * @param {Array} [stackA=[]] Tracks traversed source objects.
     * @param {Array} [stackB=[]] Associates clones with source counterparts.
     * @returns {*} Returns the cloned value.
     */
    function baseClone(value, isDeep, callback, stackA, stackB) {
      if (callback) {
        var result = callback(value);
        if (typeof result != 'undefined') {
          return result;
        }
      }
      // inspect [[Class]]
      var isObj = isObject(value);
      if (isObj) {
        var className = toString.call(value);
        if (!cloneableClasses[className]) {
          return value;
        }
        var ctor = ctorByClass[className];
        switch (className) {
          case boolClass:
          case dateClass:
            return new ctor(+value);

          case numberClass:
          case stringClass:
            return new ctor(value);

          case regexpClass:
            result = ctor(value.source, reFlags.exec(value));
            result.lastIndex = value.lastIndex;
            return result;
        }
      } else {
        return value;
      }
      var isArr = isArray(value);
      if (isDeep) {
        // check for circular references and return corresponding clone
        var initedStack = !stackA;
        stackA || (stackA = getArray());
        stackB || (stackB = getArray());

        var length = stackA.length;
        while (length--) {
          if (stackA[length] == value) {
            return stackB[length];
          }
        }
        result = isArr ? ctor(value.length) : {};
      }
      else {
        result = isArr ? slice(value) : assign({}, value);
      }
      // add array properties assigned by `RegExp#exec`
      if (isArr) {
        if (hasOwnProperty.call(value, 'index')) {
          result.index = value.index;
        }
        if (hasOwnProperty.call(value, 'input')) {
          result.input = value.input;
        }
      }
      // exit for shallow clone
      if (!isDeep) {
        return result;
      }
      // add the source value to the stack of traversed objects
      // and associate it with its clone
      stackA.push(value);
      stackB.push(result);

      // recursively populate clone (susceptible to call stack limits)
      (isArr ? forEach : forOwn)(value, function(objValue, key) {
        result[key] = baseClone(objValue, isDeep, callback, stackA, stackB);
      });

      if (initedStack) {
        releaseArray(stackA);
        releaseArray(stackB);
      }
      return result;
    }

    /**
     * The base implementation of `_.create` without support for assigning
     * properties to the created object.
     *
     * @private
     * @param {Object} prototype The object to inherit from.
     * @returns {Object} Returns the new object.
     */
    function baseCreate(prototype, properties) {
      return isObject(prototype) ? nativeCreate(prototype) : {};
    }
    // fallback for browsers without `Object.create`
    if (!nativeCreate) {
      baseCreate = (function() {
        function Object() {}
        return function(prototype) {
          if (isObject(prototype)) {
            Object.prototype = prototype;
            var result = new Object;
            Object.prototype = null;
          }
          return result || context.Object();
        };
      }());
    }

    /**
     * The base implementation of `_.createCallback` without support for creating
     * "_.pluck" or "_.where" style callbacks.
     *
     * @private
     * @param {*} [func=identity] The value to convert to a callback.
     * @param {*} [thisArg] The `this` binding of the created callback.
     * @param {number} [argCount] The number of arguments the callback accepts.
     * @returns {Function} Returns a callback function.
     */
    function baseCreateCallback(func, thisArg, argCount) {
      if (typeof func != 'function') {
        return identity;
      }
      // exit early for no `thisArg` or already bound by `Function#bind`
      if (typeof thisArg == 'undefined' || !('prototype' in func)) {
        return func;
      }
      var bindData = func.__bindData__;
      if (typeof bindData == 'undefined') {
        if (support.funcNames) {
          bindData = !func.name;
        }
        bindData = bindData || !support.funcDecomp;
        if (!bindData) {
          var source = fnToString.call(func);
          if (!support.funcNames) {
            bindData = !reFuncName.test(source);
          }
          if (!bindData) {
            // checks if `func` references the `this` keyword and stores the result
            bindData = reThis.test(source);
            setBindData(func, bindData);
          }
        }
      }
      // exit early if there are no `this` references or `func` is bound
      if (bindData === false || (bindData !== true && bindData[1] & 1)) {
        return func;
      }
      switch (argCount) {
        case 1: return function(value) {
          return func.call(thisArg, value);
        };
        case 2: return function(a, b) {
          return func.call(thisArg, a, b);
        };
        case 3: return function(value, index, collection) {
          return func.call(thisArg, value, index, collection);
        };
        case 4: return function(accumulator, value, index, collection) {
          return func.call(thisArg, accumulator, value, index, collection);
        };
      }
      return bind(func, thisArg);
    }

    /**
     * The base implementation of `createWrapper` that creates the wrapper and
     * sets its meta data.
     *
     * @private
     * @param {Array} bindData The bind data array.
     * @returns {Function} Returns the new function.
     */
    function baseCreateWrapper(bindData) {
      var func = bindData[0],
          bitmask = bindData[1],
          partialArgs = bindData[2],
          partialRightArgs = bindData[3],
          thisArg = bindData[4],
          arity = bindData[5];

      var isBind = bitmask & 1,
          isBindKey = bitmask & 2,
          isCurry = bitmask & 4,
          isCurryBound = bitmask & 8,
          key = func;

      function bound() {
        var thisBinding = isBind ? thisArg : this;
        if (partialArgs) {
          var args = slice(partialArgs);
          push.apply(args, arguments);
        }
        if (partialRightArgs || isCurry) {
          args || (args = slice(arguments));
          if (partialRightArgs) {
            push.apply(args, partialRightArgs);
          }
          if (isCurry && args.length < arity) {
            bitmask |= 16 & ~32;
            return baseCreateWrapper([func, (isCurryBound ? bitmask : bitmask & ~3), args, null, thisArg, arity]);
          }
        }
        args || (args = arguments);
        if (isBindKey) {
          func = thisBinding[key];
        }
        if (this instanceof bound) {
          thisBinding = baseCreate(func.prototype);
          var result = func.apply(thisBinding, args);
          return isObject(result) ? result : thisBinding;
        }
        return func.apply(thisBinding, args);
      }
      setBindData(bound, bindData);
      return bound;
    }

    /**
     * The base implementation of `_.difference` that accepts a single array
     * of values to exclude.
     *
     * @private
     * @param {Array} array The array to process.
     * @param {Array} [values] The array of values to exclude.
     * @returns {Array} Returns a new array of filtered values.
     */
    function baseDifference(array, values) {
      var index = -1,
          indexOf = getIndexOf(),
          length = array ? array.length : 0,
          isLarge = length >= largeArraySize && indexOf === baseIndexOf,
          result = [];

      if (isLarge) {
        var cache = createCache(values);
        if (cache) {
          indexOf = cacheIndexOf;
          values = cache;
        } else {
          isLarge = false;
        }
      }
      while (++index < length) {
        var value = array[index];
        if (indexOf(values, value) < 0) {
          result.push(value);
        }
      }
      if (isLarge) {
        releaseObject(values);
      }
      return result;
    }

    /**
     * The base implementation of `_.flatten` without support for callback
     * shorthands or `thisArg` binding.
     *
     * @private
     * @param {Array} array The array to flatten.
     * @param {boolean} [isShallow=false] A flag to restrict flattening to a single level.
     * @param {boolean} [isStrict=false] A flag to restrict flattening to arrays and `arguments` objects.
     * @param {number} [fromIndex=0] The index to start from.
     * @returns {Array} Returns a new flattened array.
     */
    function baseFlatten(array, isShallow, isStrict, fromIndex) {
      var index = (fromIndex || 0) - 1,
          length = array ? array.length : 0,
          result = [];

      while (++index < length) {
        var value = array[index];

        if (value && typeof value == 'object' && typeof value.length == 'number'
            && (isArray(value) || isArguments(value))) {
          // recursively flatten arrays (susceptible to call stack limits)
          if (!isShallow) {
            value = baseFlatten(value, isShallow, isStrict);
          }
          var valIndex = -1,
              valLength = value.length,
              resIndex = result.length;

          result.length += valLength;
          while (++valIndex < valLength) {
            result[resIndex++] = value[valIndex];
          }
        } else if (!isStrict) {
          result.push(value);
        }
      }
      return result;
    }

    /**
     * The base implementation of `_.isEqual`, without support for `thisArg` binding,
     * that allows partial "_.where" style comparisons.
     *
     * @private
     * @param {*} a The value to compare.
     * @param {*} b The other value to compare.
     * @param {Function} [callback] The function to customize comparing values.
     * @param {Function} [isWhere=false] A flag to indicate performing partial comparisons.
     * @param {Array} [stackA=[]] Tracks traversed `a` objects.
     * @param {Array} [stackB=[]] Tracks traversed `b` objects.
     * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
     */
    function baseIsEqual(a, b, callback, isWhere, stackA, stackB) {
      // used to indicate that when comparing objects, `a` has at least the properties of `b`
      if (callback) {
        var result = callback(a, b);
        if (typeof result != 'undefined') {
          return !!result;
        }
      }
      // exit early for identical values
      if (a === b) {
        // treat `+0` vs. `-0` as not equal
        return a !== 0 || (1 / a == 1 / b);
      }
      var type = typeof a,
          otherType = typeof b;

      // exit early for unlike primitive values
      if (a === a &&
          !(a && objectTypes[type]) &&
          !(b && objectTypes[otherType])) {
        return false;
      }
      // exit early for `null` and `undefined` avoiding ES3's Function#call behavior
      // http://es5.github.io/#x15.3.4.4
      if (a == null || b == null) {
        return a === b;
      }
      // compare [[Class]] names
      var className = toString.call(a),
          otherClass = toString.call(b);

      if (className == argsClass) {
        className = objectClass;
      }
      if (otherClass == argsClass) {
        otherClass = objectClass;
      }
      if (className != otherClass) {
        return false;
      }
      switch (className) {
        case boolClass:
        case dateClass:
          // coerce dates and booleans to numbers, dates to milliseconds and booleans
          // to `1` or `0` treating invalid dates coerced to `NaN` as not equal
          return +a == +b;

        case numberClass:
          // treat `NaN` vs. `NaN` as equal
          return (a != +a)
            ? b != +b
            // but treat `+0` vs. `-0` as not equal
            : (a == 0 ? (1 / a == 1 / b) : a == +b);

        case regexpClass:
        case stringClass:
          // coerce regexes to strings (http://es5.github.io/#x15.10.6.4)
          // treat string primitives and their corresponding object instances as equal
          return a == String(b);
      }
      var isArr = className == arrayClass;
      if (!isArr) {
        // unwrap any `lodash` wrapped values
        var aWrapped = hasOwnProperty.call(a, '__wrapped__'),
            bWrapped = hasOwnProperty.call(b, '__wrapped__');

        if (aWrapped || bWrapped) {
          return baseIsEqual(aWrapped ? a.__wrapped__ : a, bWrapped ? b.__wrapped__ : b, callback, isWhere, stackA, stackB);
        }
        // exit for functions and DOM nodes
        if (className != objectClass) {
          return false;
        }
        // in older versions of Opera, `arguments` objects have `Array` constructors
        var ctorA = a.constructor,
            ctorB = b.constructor;

        // non `Object` object instances with different constructors are not equal
        if (ctorA != ctorB &&
              !(isFunction(ctorA) && ctorA instanceof ctorA && isFunction(ctorB) && ctorB instanceof ctorB) &&
              ('constructor' in a && 'constructor' in b)
            ) {
          return false;
        }
      }
      // assume cyclic structures are equal
      // the algorithm for detecting cyclic structures is adapted from ES 5.1
      // section 15.12.3, abstract operation `JO` (http://es5.github.io/#x15.12.3)
      var initedStack = !stackA;
      stackA || (stackA = getArray());
      stackB || (stackB = getArray());

      var length = stackA.length;
      while (length--) {
        if (stackA[length] == a) {
          return stackB[length] == b;
        }
      }
      var size = 0;
      result = true;

      // add `a` and `b` to the stack of traversed objects
      stackA.push(a);
      stackB.push(b);

      // recursively compare objects and arrays (susceptible to call stack limits)
      if (isArr) {
        // compare lengths to determine if a deep comparison is necessary
        length = a.length;
        size = b.length;
        result = size == length;

        if (result || isWhere) {
          // deep compare the contents, ignoring non-numeric properties
          while (size--) {
            var index = length,
                value = b[size];

            if (isWhere) {
              while (index--) {
                if ((result = baseIsEqual(a[index], value, callback, isWhere, stackA, stackB))) {
                  break;
                }
              }
            } else if (!(result = baseIsEqual(a[size], value, callback, isWhere, stackA, stackB))) {
              break;
            }
          }
        }
      }
      else {
        // deep compare objects using `forIn`, instead of `forOwn`, to avoid `Object.keys`
        // which, in this case, is more costly
        forIn(b, function(value, key, b) {
          if (hasOwnProperty.call(b, key)) {
            // count the number of properties.
            size++;
            // deep compare each property value.
            return (result = hasOwnProperty.call(a, key) && baseIsEqual(a[key], value, callback, isWhere, stackA, stackB));
          }
        });

        if (result && !isWhere) {
          // ensure both objects have the same number of properties
          forIn(a, function(value, key, a) {
            if (hasOwnProperty.call(a, key)) {
              // `size` will be `-1` if `a` has more properties than `b`
              return (result = --size > -1);
            }
          });
        }
      }
      stackA.pop();
      stackB.pop();

      if (initedStack) {
        releaseArray(stackA);
        releaseArray(stackB);
      }
      return result;
    }

    /**
     * The base implementation of `_.merge` without argument juggling or support
     * for `thisArg` binding.
     *
     * @private
     * @param {Object} object The destination object.
     * @param {Object} source The source object.
     * @param {Function} [callback] The function to customize merging properties.
     * @param {Array} [stackA=[]] Tracks traversed source objects.
     * @param {Array} [stackB=[]] Associates values with source counterparts.
     */
    function baseMerge(object, source, callback, stackA, stackB) {
      (isArray(source) ? forEach : forOwn)(source, function(source, key) {
        var found,
            isArr,
            result = source,
            value = object[key];

        if (source && ((isArr = isArray(source)) || isPlainObject(source))) {
          // avoid merging previously merged cyclic sources
          var stackLength = stackA.length;
          while (stackLength--) {
            if ((found = stackA[stackLength] == source)) {
              value = stackB[stackLength];
              break;
            }
          }
          if (!found) {
            var isShallow;
            if (callback) {
              result = callback(value, source);
              if ((isShallow = typeof result != 'undefined')) {
                value = result;
              }
            }
            if (!isShallow) {
              value = isArr
                ? (isArray(value) ? value : [])
                : (isPlainObject(value) ? value : {});
            }
            // add `source` and associated `value` to the stack of traversed objects
            stackA.push(source);
            stackB.push(value);

            // recursively merge objects and arrays (susceptible to call stack limits)
            if (!isShallow) {
              baseMerge(value, source, callback, stackA, stackB);
            }
          }
        }
        else {
          if (callback) {
            result = callback(value, source);
            if (typeof result == 'undefined') {
              result = source;
            }
          }
          if (typeof result != 'undefined') {
            value = result;
          }
        }
        object[key] = value;
      });
    }

    /**
     * The base implementation of `_.random` without argument juggling or support
     * for returning floating-point numbers.
     *
     * @private
     * @param {number} min The minimum possible value.
     * @param {number} max The maximum possible value.
     * @returns {number} Returns a random number.
     */
    function baseRandom(min, max) {
      return min + floor(nativeRandom() * (max - min + 1));
    }

    /**
     * The base implementation of `_.uniq` without support for callback shorthands
     * or `thisArg` binding.
     *
     * @private
     * @param {Array} array The array to process.
     * @param {boolean} [isSorted=false] A flag to indicate that `array` is sorted.
     * @param {Function} [callback] The function called per iteration.
     * @returns {Array} Returns a duplicate-value-free array.
     */
    function baseUniq(array, isSorted, callback) {
      var index = -1,
          indexOf = getIndexOf(),
          length = array ? array.length : 0,
          result = [];

      var isLarge = !isSorted && length >= largeArraySize && indexOf === baseIndexOf,
          seen = (callback || isLarge) ? getArray() : result;

      if (isLarge) {
        var cache = createCache(seen);
        indexOf = cacheIndexOf;
        seen = cache;
      }
      while (++index < length) {
        var value = array[index],
            computed = callback ? callback(value, index, array) : value;

        if (isSorted
              ? !index || seen[seen.length - 1] !== computed
              : indexOf(seen, computed) < 0
            ) {
          if (callback || isLarge) {
            seen.push(computed);
          }
          result.push(value);
        }
      }
      if (isLarge) {
        releaseArray(seen.array);
        releaseObject(seen);
      } else if (callback) {
        releaseArray(seen);
      }
      return result;
    }

    /**
     * Creates a function that aggregates a collection, creating an object composed
     * of keys generated from the results of running each element of the collection
     * through a callback. The given `setter` function sets the keys and values
     * of the composed object.
     *
     * @private
     * @param {Function} setter The setter function.
     * @returns {Function} Returns the new aggregator function.
     */
    function createAggregator(setter) {
      return function(collection, callback, thisArg) {
        var result = {};
        callback = lodash.createCallback(callback, thisArg, 3);

        var index = -1,
            length = collection ? collection.length : 0;

        if (typeof length == 'number') {
          while (++index < length) {
            var value = collection[index];
            setter(result, value, callback(value, index, collection), collection);
          }
        } else {
          forOwn(collection, function(value, key, collection) {
            setter(result, value, callback(value, key, collection), collection);
          });
        }
        return result;
      };
    }

    /**
     * Creates a function that, when called, either curries or invokes `func`
     * with an optional `this` binding and partially applied arguments.
     *
     * @private
     * @param {Function|string} func The function or method name to reference.
     * @param {number} bitmask The bitmask of method flags to compose.
     *  The bitmask may be composed of the following flags:
     *  1 - `_.bind`
     *  2 - `_.bindKey`
     *  4 - `_.curry`
     *  8 - `_.curry` (bound)
     *  16 - `_.partial`
     *  32 - `_.partialRight`
     * @param {Array} [partialArgs] An array of arguments to prepend to those
     *  provided to the new function.
     * @param {Array} [partialRightArgs] An array of arguments to append to those
     *  provided to the new function.
     * @param {*} [thisArg] The `this` binding of `func`.
     * @param {number} [arity] The arity of `func`.
     * @returns {Function} Returns the new function.
     */
    function createWrapper(func, bitmask, partialArgs, partialRightArgs, thisArg, arity) {
      var isBind = bitmask & 1,
          isBindKey = bitmask & 2,
          isCurry = bitmask & 4,
          isCurryBound = bitmask & 8,
          isPartial = bitmask & 16,
          isPartialRight = bitmask & 32;

      if (!isBindKey && !isFunction(func)) {
        throw new TypeError;
      }
      if (isPartial && !partialArgs.length) {
        bitmask &= ~16;
        isPartial = partialArgs = false;
      }
      if (isPartialRight && !partialRightArgs.length) {
        bitmask &= ~32;
        isPartialRight = partialRightArgs = false;
      }
      var bindData = func && func.__bindData__;
      if (bindData && bindData !== true) {
        // clone `bindData`
        bindData = slice(bindData);
        if (bindData[2]) {
          bindData[2] = slice(bindData[2]);
        }
        if (bindData[3]) {
          bindData[3] = slice(bindData[3]);
        }
        // set `thisBinding` is not previously bound
        if (isBind && !(bindData[1] & 1)) {
          bindData[4] = thisArg;
        }
        // set if previously bound but not currently (subsequent curried functions)
        if (!isBind && bindData[1] & 1) {
          bitmask |= 8;
        }
        // set curried arity if not yet set
        if (isCurry && !(bindData[1] & 4)) {
          bindData[5] = arity;
        }
        // append partial left arguments
        if (isPartial) {
          push.apply(bindData[2] || (bindData[2] = []), partialArgs);
        }
        // append partial right arguments
        if (isPartialRight) {
          unshift.apply(bindData[3] || (bindData[3] = []), partialRightArgs);
        }
        // merge flags
        bindData[1] |= bitmask;
        return createWrapper.apply(null, bindData);
      }
      // fast path for `_.bind`
      var creater = (bitmask == 1 || bitmask === 17) ? baseBind : baseCreateWrapper;
      return creater([func, bitmask, partialArgs, partialRightArgs, thisArg, arity]);
    }

    /**
     * Used by `escape` to convert characters to HTML entities.
     *
     * @private
     * @param {string} match The matched character to escape.
     * @returns {string} Returns the escaped character.
     */
    function escapeHtmlChar(match) {
      return htmlEscapes[match];
    }

    /**
     * Gets the appropriate "indexOf" function. If the `_.indexOf` method is
     * customized, this method returns the custom method, otherwise it returns
     * the `baseIndexOf` function.
     *
     * @private
     * @returns {Function} Returns the "indexOf" function.
     */
    function getIndexOf() {
      var result = (result = lodash.indexOf) === indexOf ? baseIndexOf : result;
      return result;
    }

    /**
     * Checks if `value` is a native function.
     *
     * @private
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if the `value` is a native function, else `false`.
     */
    function isNative(value) {
      return typeof value == 'function' && reNative.test(value);
    }

    /**
     * Sets `this` binding data on a given function.
     *
     * @private
     * @param {Function} func The function to set data on.
     * @param {Array} value The data array to set.
     */
    var setBindData = !defineProperty ? noop : function(func, value) {
      descriptor.value = value;
      defineProperty(func, '__bindData__', descriptor);
    };

    /**
     * A fallback implementation of `isPlainObject` which checks if a given value
     * is an object created by the `Object` constructor, assuming objects created
     * by the `Object` constructor have no inherited enumerable properties and that
     * there are no `Object.prototype` extensions.
     *
     * @private
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a plain object, else `false`.
     */
    function shimIsPlainObject(value) {
      var ctor,
          result;

      // avoid non Object objects, `arguments` objects, and DOM elements
      if (!(value && toString.call(value) == objectClass) ||
          (ctor = value.constructor, isFunction(ctor) && !(ctor instanceof ctor))) {
        return false;
      }
      // In most environments an object's own properties are iterated before
      // its inherited properties. If the last iterated property is an object's
      // own property then there are no inherited enumerable properties.
      forIn(value, function(value, key) {
        result = key;
      });
      return typeof result == 'undefined' || hasOwnProperty.call(value, result);
    }

    /**
     * Used by `unescape` to convert HTML entities to characters.
     *
     * @private
     * @param {string} match The matched character to unescape.
     * @returns {string} Returns the unescaped character.
     */
    function unescapeHtmlChar(match) {
      return htmlUnescapes[match];
    }

    /*--------------------------------------------------------------------------*/

    /**
     * Checks if `value` is an `arguments` object.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if the `value` is an `arguments` object, else `false`.
     * @example
     *
     * (function() { return _.isArguments(arguments); })(1, 2, 3);
     * // => true
     *
     * _.isArguments([1, 2, 3]);
     * // => false
     */
    function isArguments(value) {
      return value && typeof value == 'object' && typeof value.length == 'number' &&
        toString.call(value) == argsClass || false;
    }

    /**
     * Checks if `value` is an array.
     *
     * @static
     * @memberOf _
     * @type Function
     * @category Objects
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if the `value` is an array, else `false`.
     * @example
     *
     * (function() { return _.isArray(arguments); })();
     * // => false
     *
     * _.isArray([1, 2, 3]);
     * // => true
     */
    var isArray = nativeIsArray || function(value) {
      return value && typeof value == 'object' && typeof value.length == 'number' &&
        toString.call(value) == arrayClass || false;
    };

    /**
     * A fallback implementation of `Object.keys` which produces an array of the
     * given object's own enumerable property names.
     *
     * @private
     * @type Function
     * @param {Object} object The object to inspect.
     * @returns {Array} Returns an array of property names.
     */
    var shimKeys = function(object) {
      var index, iterable = object, result = [];
      if (!iterable) return result;
      if (!(objectTypes[typeof object])) return result;
        for (index in iterable) {
          if (hasOwnProperty.call(iterable, index)) {
            result.push(index);
          }
        }
      return result
    };

    /**
     * Creates an array composed of the own enumerable property names of an object.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {Object} object The object to inspect.
     * @returns {Array} Returns an array of property names.
     * @example
     *
     * _.keys({ 'one': 1, 'two': 2, 'three': 3 });
     * // => ['one', 'two', 'three'] (property order is not guaranteed across environments)
     */
    var keys = !nativeKeys ? shimKeys : function(object) {
      if (!isObject(object)) {
        return [];
      }
      return nativeKeys(object);
    };

    /**
     * Used to convert characters to HTML entities:
     *
     * Though the `>` character is escaped for symmetry, characters like `>` and `/`
     * don't require escaping in HTML and have no special meaning unless they're part
     * of a tag or an unquoted attribute value.
     * http://mathiasbynens.be/notes/ambiguous-ampersands (under "semi-related fun fact")
     */
    var htmlEscapes = {
      '&': '&amp;',
      '<': '&lt;',
      '>': '&gt;',
      '"': '&quot;',
      "'": '&#39;'
    };

    /** Used to convert HTML entities to characters */
    var htmlUnescapes = invert(htmlEscapes);

    /** Used to match HTML entities and HTML characters */
    var reEscapedHtml = RegExp('(' + keys(htmlUnescapes).join('|') + ')', 'g'),
        reUnescapedHtml = RegExp('[' + keys(htmlEscapes).join('') + ']', 'g');

    /*--------------------------------------------------------------------------*/

    /**
     * Assigns own enumerable properties of source object(s) to the destination
     * object. Subsequent sources will overwrite property assignments of previous
     * sources. If a callback is provided it will be executed to produce the
     * assigned values. The callback is bound to `thisArg` and invoked with two
     * arguments; (objectValue, sourceValue).
     *
     * @static
     * @memberOf _
     * @type Function
     * @alias extend
     * @category Objects
     * @param {Object} object The destination object.
     * @param {...Object} [source] The source objects.
     * @param {Function} [callback] The function to customize assigning values.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Object} Returns the destination object.
     * @example
     *
     * _.assign({ 'name': 'fred' }, { 'employer': 'slate' });
     * // => { 'name': 'fred', 'employer': 'slate' }
     *
     * var defaults = _.partialRight(_.assign, function(a, b) {
     *   return typeof a == 'undefined' ? b : a;
     * });
     *
     * var object = { 'name': 'barney' };
     * defaults(object, { 'name': 'fred', 'employer': 'slate' });
     * // => { 'name': 'barney', 'employer': 'slate' }
     */
    var assign = function(object, source, guard) {
      var index, iterable = object, result = iterable;
      if (!iterable) return result;
      var args = arguments,
          argsIndex = 0,
          argsLength = typeof guard == 'number' ? 2 : args.length;
      if (argsLength > 3 && typeof args[argsLength - 2] == 'function') {
        var callback = baseCreateCallback(args[--argsLength - 1], args[argsLength--], 2);
      } else if (argsLength > 2 && typeof args[argsLength - 1] == 'function') {
        callback = args[--argsLength];
      }
      while (++argsIndex < argsLength) {
        iterable = args[argsIndex];
        if (iterable && objectTypes[typeof iterable]) {
        var ownIndex = -1,
            ownProps = objectTypes[typeof iterable] && keys(iterable),
            length = ownProps ? ownProps.length : 0;

        while (++ownIndex < length) {
          index = ownProps[ownIndex];
          result[index] = callback ? callback(result[index], iterable[index]) : iterable[index];
        }
        }
      }
      return result
    };

    /**
     * Creates a clone of `value`. If `isDeep` is `true` nested objects will also
     * be cloned, otherwise they will be assigned by reference. If a callback
     * is provided it will be executed to produce the cloned values. If the
     * callback returns `undefined` cloning will be handled by the method instead.
     * The callback is bound to `thisArg` and invoked with one argument; (value).
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} value The value to clone.
     * @param {boolean} [isDeep=false] Specify a deep clone.
     * @param {Function} [callback] The function to customize cloning values.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {*} Returns the cloned value.
     * @example
     *
     * var characters = [
     *   { 'name': 'barney', 'age': 36 },
     *   { 'name': 'fred',   'age': 40 }
     * ];
     *
     * var shallow = _.clone(characters);
     * shallow[0] === characters[0];
     * // => true
     *
     * var deep = _.clone(characters, true);
     * deep[0] === characters[0];
     * // => false
     *
     * _.mixin({
     *   'clone': _.partialRight(_.clone, function(value) {
     *     return _.isElement(value) ? value.cloneNode(false) : undefined;
     *   })
     * });
     *
     * var clone = _.clone(document.body);
     * clone.childNodes.length;
     * // => 0
     */
    function clone(value, isDeep, callback, thisArg) {
      // allows working with "Collections" methods without using their `index`
      // and `collection` arguments for `isDeep` and `callback`
      if (typeof isDeep != 'boolean' && isDeep != null) {
        thisArg = callback;
        callback = isDeep;
        isDeep = false;
      }
      return baseClone(value, isDeep, typeof callback == 'function' && baseCreateCallback(callback, thisArg, 1));
    }

    /**
     * Creates a deep clone of `value`. If a callback is provided it will be
     * executed to produce the cloned values. If the callback returns `undefined`
     * cloning will be handled by the method instead. The callback is bound to
     * `thisArg` and invoked with one argument; (value).
     *
     * Note: This method is loosely based on the structured clone algorithm. Functions
     * and DOM nodes are **not** cloned. The enumerable properties of `arguments` objects and
     * objects created by constructors other than `Object` are cloned to plain `Object` objects.
     * See http://www.w3.org/TR/html5/infrastructure.html#internal-structured-cloning-algorithm.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} value The value to deep clone.
     * @param {Function} [callback] The function to customize cloning values.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {*} Returns the deep cloned value.
     * @example
     *
     * var characters = [
     *   { 'name': 'barney', 'age': 36 },
     *   { 'name': 'fred',   'age': 40 }
     * ];
     *
     * var deep = _.cloneDeep(characters);
     * deep[0] === characters[0];
     * // => false
     *
     * var view = {
     *   'label': 'docs',
     *   'node': element
     * };
     *
     * var clone = _.cloneDeep(view, function(value) {
     *   return _.isElement(value) ? value.cloneNode(true) : undefined;
     * });
     *
     * clone.node == view.node;
     * // => false
     */
    function cloneDeep(value, callback, thisArg) {
      return baseClone(value, true, typeof callback == 'function' && baseCreateCallback(callback, thisArg, 1));
    }

    /**
     * Creates an object that inherits from the given `prototype` object. If a
     * `properties` object is provided its own enumerable properties are assigned
     * to the created object.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {Object} prototype The object to inherit from.
     * @param {Object} [properties] The properties to assign to the object.
     * @returns {Object} Returns the new object.
     * @example
     *
     * function Shape() {
     *   this.x = 0;
     *   this.y = 0;
     * }
     *
     * function Circle() {
     *   Shape.call(this);
     * }
     *
     * Circle.prototype = _.create(Shape.prototype, { 'constructor': Circle });
     *
     * var circle = new Circle;
     * circle instanceof Circle;
     * // => true
     *
     * circle instanceof Shape;
     * // => true
     */
    function create(prototype, properties) {
      var result = baseCreate(prototype);
      return properties ? assign(result, properties) : result;
    }

    /**
     * Assigns own enumerable properties of source object(s) to the destination
     * object for all destination properties that resolve to `undefined`. Once a
     * property is set, additional defaults of the same property will be ignored.
     *
     * @static
     * @memberOf _
     * @type Function
     * @category Objects
     * @param {Object} object The destination object.
     * @param {...Object} [source] The source objects.
     * @param- {Object} [guard] Allows working with `_.reduce` without using its
     *  `key` and `object` arguments as sources.
     * @returns {Object} Returns the destination object.
     * @example
     *
     * var object = { 'name': 'barney' };
     * _.defaults(object, { 'name': 'fred', 'employer': 'slate' });
     * // => { 'name': 'barney', 'employer': 'slate' }
     */
    var defaults = function(object, source, guard) {
      var index, iterable = object, result = iterable;
      if (!iterable) return result;
      var args = arguments,
          argsIndex = 0,
          argsLength = typeof guard == 'number' ? 2 : args.length;
      while (++argsIndex < argsLength) {
        iterable = args[argsIndex];
        if (iterable && objectTypes[typeof iterable]) {
        var ownIndex = -1,
            ownProps = objectTypes[typeof iterable] && keys(iterable),
            length = ownProps ? ownProps.length : 0;

        while (++ownIndex < length) {
          index = ownProps[ownIndex];
          if (typeof result[index] == 'undefined') result[index] = iterable[index];
        }
        }
      }
      return result
    };

    /**
     * This method is like `_.findIndex` except that it returns the key of the
     * first element that passes the callback check, instead of the element itself.
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {Object} object The object to search.
     * @param {Function|Object|string} [callback=identity] The function called per
     *  iteration. If a property name or object is provided it will be used to
     *  create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {string|undefined} Returns the key of the found element, else `undefined`.
     * @example
     *
     * var characters = {
     *   'barney': {  'age': 36, 'blocked': false },
     *   'fred': {    'age': 40, 'blocked': true },
     *   'pebbles': { 'age': 1,  'blocked': false }
     * };
     *
     * _.findKey(characters, function(chr) {
     *   return chr.age < 40;
     * });
     * // => 'barney' (property order is not guaranteed across environments)
     *
     * // using "_.where" callback shorthand
     * _.findKey(characters, { 'age': 1 });
     * // => 'pebbles'
     *
     * // using "_.pluck" callback shorthand
     * _.findKey(characters, 'blocked');
     * // => 'fred'
     */
    function findKey(object, callback, thisArg) {
      var result;
      callback = lodash.createCallback(callback, thisArg, 3);
      forOwn(object, function(value, key, object) {
        if (callback(value, key, object)) {
          result = key;
          return false;
        }
      });
      return result;
    }

    /**
     * This method is like `_.findKey` except that it iterates over elements
     * of a `collection` in the opposite order.
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {Object} object The object to search.
     * @param {Function|Object|string} [callback=identity] The function called per
     *  iteration. If a property name or object is provided it will be used to
     *  create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {string|undefined} Returns the key of the found element, else `undefined`.
     * @example
     *
     * var characters = {
     *   'barney': {  'age': 36, 'blocked': true },
     *   'fred': {    'age': 40, 'blocked': false },
     *   'pebbles': { 'age': 1,  'blocked': true }
     * };
     *
     * _.findLastKey(characters, function(chr) {
     *   return chr.age < 40;
     * });
     * // => returns `pebbles`, assuming `_.findKey` returns `barney`
     *
     * // using "_.where" callback shorthand
     * _.findLastKey(characters, { 'age': 40 });
     * // => 'fred'
     *
     * // using "_.pluck" callback shorthand
     * _.findLastKey(characters, 'blocked');
     * // => 'pebbles'
     */
    function findLastKey(object, callback, thisArg) {
      var result;
      callback = lodash.createCallback(callback, thisArg, 3);
      forOwnRight(object, function(value, key, object) {
        if (callback(value, key, object)) {
          result = key;
          return false;
        }
      });
      return result;
    }

    /**
     * Iterates over own and inherited enumerable properties of an object,
     * executing the callback for each property. The callback is bound to `thisArg`
     * and invoked with three arguments; (value, key, object). Callbacks may exit
     * iteration early by explicitly returning `false`.
     *
     * @static
     * @memberOf _
     * @type Function
     * @category Objects
     * @param {Object} object The object to iterate over.
     * @param {Function} [callback=identity] The function called per iteration.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Object} Returns `object`.
     * @example
     *
     * function Shape() {
     *   this.x = 0;
     *   this.y = 0;
     * }
     *
     * Shape.prototype.move = function(x, y) {
     *   this.x += x;
     *   this.y += y;
     * };
     *
     * _.forIn(new Shape, function(value, key) {
     *   console.log(key);
     * });
     * // => logs 'x', 'y', and 'move' (property order is not guaranteed across environments)
     */
    var forIn = function(collection, callback, thisArg) {
      var index, iterable = collection, result = iterable;
      if (!iterable) return result;
      if (!objectTypes[typeof iterable]) return result;
      callback = callback && typeof thisArg == 'undefined' ? callback : baseCreateCallback(callback, thisArg, 3);
        for (index in iterable) {
          if (callback(iterable[index], index, collection) === false) return result;
        }
      return result
    };

    /**
     * This method is like `_.forIn` except that it iterates over elements
     * of a `collection` in the opposite order.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {Object} object The object to iterate over.
     * @param {Function} [callback=identity] The function called per iteration.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Object} Returns `object`.
     * @example
     *
     * function Shape() {
     *   this.x = 0;
     *   this.y = 0;
     * }
     *
     * Shape.prototype.move = function(x, y) {
     *   this.x += x;
     *   this.y += y;
     * };
     *
     * _.forInRight(new Shape, function(value, key) {
     *   console.log(key);
     * });
     * // => logs 'move', 'y', and 'x' assuming `_.forIn ` logs 'x', 'y', and 'move'
     */
    function forInRight(object, callback, thisArg) {
      var pairs = [];

      forIn(object, function(value, key) {
        pairs.push(key, value);
      });

      var length = pairs.length;
      callback = baseCreateCallback(callback, thisArg, 3);
      while (length--) {
        if (callback(pairs[length--], pairs[length], object) === false) {
          break;
        }
      }
      return object;
    }

    /**
     * Iterates over own enumerable properties of an object, executing the callback
     * for each property. The callback is bound to `thisArg` and invoked with three
     * arguments; (value, key, object). Callbacks may exit iteration early by
     * explicitly returning `false`.
     *
     * @static
     * @memberOf _
     * @type Function
     * @category Objects
     * @param {Object} object The object to iterate over.
     * @param {Function} [callback=identity] The function called per iteration.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Object} Returns `object`.
     * @example
     *
     * _.forOwn({ '0': 'zero', '1': 'one', 'length': 2 }, function(num, key) {
     *   console.log(key);
     * });
     * // => logs '0', '1', and 'length' (property order is not guaranteed across environments)
     */
    var forOwn = function(collection, callback, thisArg) {
      var index, iterable = collection, result = iterable;
      if (!iterable) return result;
      if (!objectTypes[typeof iterable]) return result;
      callback = callback && typeof thisArg == 'undefined' ? callback : baseCreateCallback(callback, thisArg, 3);
        var ownIndex = -1,
            ownProps = objectTypes[typeof iterable] && keys(iterable),
            length = ownProps ? ownProps.length : 0;

        while (++ownIndex < length) {
          index = ownProps[ownIndex];
          if (callback(iterable[index], index, collection) === false) return result;
        }
      return result
    };

    /**
     * This method is like `_.forOwn` except that it iterates over elements
     * of a `collection` in the opposite order.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {Object} object The object to iterate over.
     * @param {Function} [callback=identity] The function called per iteration.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Object} Returns `object`.
     * @example
     *
     * _.forOwnRight({ '0': 'zero', '1': 'one', 'length': 2 }, function(num, key) {
     *   console.log(key);
     * });
     * // => logs 'length', '1', and '0' assuming `_.forOwn` logs '0', '1', and 'length'
     */
    function forOwnRight(object, callback, thisArg) {
      var props = keys(object),
          length = props.length;

      callback = baseCreateCallback(callback, thisArg, 3);
      while (length--) {
        var key = props[length];
        if (callback(object[key], key, object) === false) {
          break;
        }
      }
      return object;
    }

    /**
     * Creates a sorted array of property names of all enumerable properties,
     * own and inherited, of `object` that have function values.
     *
     * @static
     * @memberOf _
     * @alias methods
     * @category Objects
     * @param {Object} object The object to inspect.
     * @returns {Array} Returns an array of property names that have function values.
     * @example
     *
     * _.functions(_);
     * // => ['all', 'any', 'bind', 'bindAll', 'clone', 'compact', 'compose', ...]
     */
    function functions(object) {
      var result = [];
      forIn(object, function(value, key) {
        if (isFunction(value)) {
          result.push(key);
        }
      });
      return result.sort();
    }

    /**
     * Checks if the specified property name exists as a direct property of `object`,
     * instead of an inherited property.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {Object} object The object to inspect.
     * @param {string} key The name of the property to check.
     * @returns {boolean} Returns `true` if key is a direct property, else `false`.
     * @example
     *
     * _.has({ 'a': 1, 'b': 2, 'c': 3 }, 'b');
     * // => true
     */
    function has(object, key) {
      return object ? hasOwnProperty.call(object, key) : false;
    }

    /**
     * Creates an object composed of the inverted keys and values of the given object.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {Object} object The object to invert.
     * @returns {Object} Returns the created inverted object.
     * @example
     *
     * _.invert({ 'first': 'fred', 'second': 'barney' });
     * // => { 'fred': 'first', 'barney': 'second' }
     */
    function invert(object) {
      var index = -1,
          props = keys(object),
          length = props.length,
          result = {};

      while (++index < length) {
        var key = props[index];
        result[object[key]] = key;
      }
      return result;
    }

    /**
     * Checks if `value` is a boolean value.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if the `value` is a boolean value, else `false`.
     * @example
     *
     * _.isBoolean(null);
     * // => false
     */
    function isBoolean(value) {
      return value === true || value === false ||
        value && typeof value == 'object' && toString.call(value) == boolClass || false;
    }

    /**
     * Checks if `value` is a date.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if the `value` is a date, else `false`.
     * @example
     *
     * _.isDate(new Date);
     * // => true
     */
    function isDate(value) {
      return value && typeof value == 'object' && toString.call(value) == dateClass || false;
    }

    /**
     * Checks if `value` is a DOM element.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if the `value` is a DOM element, else `false`.
     * @example
     *
     * _.isElement(document.body);
     * // => true
     */
    function isElement(value) {
      return value && value.nodeType === 1 || false;
    }

    /**
     * Checks if `value` is empty. Arrays, strings, or `arguments` objects with a
     * length of `0` and objects with no own enumerable properties are considered
     * "empty".
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {Array|Object|string} value The value to inspect.
     * @returns {boolean} Returns `true` if the `value` is empty, else `false`.
     * @example
     *
     * _.isEmpty([1, 2, 3]);
     * // => false
     *
     * _.isEmpty({});
     * // => true
     *
     * _.isEmpty('');
     * // => true
     */
    function isEmpty(value) {
      var result = true;
      if (!value) {
        return result;
      }
      var className = toString.call(value),
          length = value.length;

      if ((className == arrayClass || className == stringClass || className == argsClass ) ||
          (className == objectClass && typeof length == 'number' && isFunction(value.splice))) {
        return !length;
      }
      forOwn(value, function() {
        return (result = false);
      });
      return result;
    }

    /**
     * Performs a deep comparison between two values to determine if they are
     * equivalent to each other. If a callback is provided it will be executed
     * to compare values. If the callback returns `undefined` comparisons will
     * be handled by the method instead. The callback is bound to `thisArg` and
     * invoked with two arguments; (a, b).
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} a The value to compare.
     * @param {*} b The other value to compare.
     * @param {Function} [callback] The function to customize comparing values.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
     * @example
     *
     * var object = { 'name': 'fred' };
     * var copy = { 'name': 'fred' };
     *
     * object == copy;
     * // => false
     *
     * _.isEqual(object, copy);
     * // => true
     *
     * var words = ['hello', 'goodbye'];
     * var otherWords = ['hi', 'goodbye'];
     *
     * _.isEqual(words, otherWords, function(a, b) {
     *   var reGreet = /^(?:hello|hi)$/i,
     *       aGreet = _.isString(a) && reGreet.test(a),
     *       bGreet = _.isString(b) && reGreet.test(b);
     *
     *   return (aGreet || bGreet) ? (aGreet == bGreet) : undefined;
     * });
     * // => true
     */
    function isEqual(a, b, callback, thisArg) {
      return baseIsEqual(a, b, typeof callback == 'function' && baseCreateCallback(callback, thisArg, 2));
    }

    /**
     * Checks if `value` is, or can be coerced to, a finite number.
     *
     * Note: This is not the same as native `isFinite` which will return true for
     * booleans and empty strings. See http://es5.github.io/#x15.1.2.5.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if the `value` is finite, else `false`.
     * @example
     *
     * _.isFinite(-101);
     * // => true
     *
     * _.isFinite('10');
     * // => true
     *
     * _.isFinite(true);
     * // => false
     *
     * _.isFinite('');
     * // => false
     *
     * _.isFinite(Infinity);
     * // => false
     */
    function isFinite(value) {
      return nativeIsFinite(value) && !nativeIsNaN(parseFloat(value));
    }

    /**
     * Checks if `value` is a function.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if the `value` is a function, else `false`.
     * @example
     *
     * _.isFunction(_);
     * // => true
     */
    function isFunction(value) {
      return typeof value == 'function';
    }

    /**
     * Checks if `value` is the language type of Object.
     * (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if the `value` is an object, else `false`.
     * @example
     *
     * _.isObject({});
     * // => true
     *
     * _.isObject([1, 2, 3]);
     * // => true
     *
     * _.isObject(1);
     * // => false
     */
    function isObject(value) {
      // check if the value is the ECMAScript language type of Object
      // http://es5.github.io/#x8
      // and avoid a V8 bug
      // http://code.google.com/p/v8/issues/detail?id=2291
      return !!(value && objectTypes[typeof value]);
    }

    /**
     * Checks if `value` is `NaN`.
     *
     * Note: This is not the same as native `isNaN` which will return `true` for
     * `undefined` and other non-numeric values. See http://es5.github.io/#x15.1.2.4.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if the `value` is `NaN`, else `false`.
     * @example
     *
     * _.isNaN(NaN);
     * // => true
     *
     * _.isNaN(new Number(NaN));
     * // => true
     *
     * isNaN(undefined);
     * // => true
     *
     * _.isNaN(undefined);
     * // => false
     */
    function isNaN(value) {
      // `NaN` as a primitive is the only value that is not equal to itself
      // (perform the [[Class]] check first to avoid errors with some host objects in IE)
      return isNumber(value) && value != +value;
    }

    /**
     * Checks if `value` is `null`.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if the `value` is `null`, else `false`.
     * @example
     *
     * _.isNull(null);
     * // => true
     *
     * _.isNull(undefined);
     * // => false
     */
    function isNull(value) {
      return value === null;
    }

    /**
     * Checks if `value` is a number.
     *
     * Note: `NaN` is considered a number. See http://es5.github.io/#x8.5.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if the `value` is a number, else `false`.
     * @example
     *
     * _.isNumber(8.4 * 5);
     * // => true
     */
    function isNumber(value) {
      return typeof value == 'number' ||
        value && typeof value == 'object' && toString.call(value) == numberClass || false;
    }

    /**
     * Checks if `value` is an object created by the `Object` constructor.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a plain object, else `false`.
     * @example
     *
     * function Shape() {
     *   this.x = 0;
     *   this.y = 0;
     * }
     *
     * _.isPlainObject(new Shape);
     * // => false
     *
     * _.isPlainObject([1, 2, 3]);
     * // => false
     *
     * _.isPlainObject({ 'x': 0, 'y': 0 });
     * // => true
     */
    var isPlainObject = !getPrototypeOf ? shimIsPlainObject : function(value) {
      if (!(value && toString.call(value) == objectClass)) {
        return false;
      }
      var valueOf = value.valueOf,
          objProto = isNative(valueOf) && (objProto = getPrototypeOf(valueOf)) && getPrototypeOf(objProto);

      return objProto
        ? (value == objProto || getPrototypeOf(value) == objProto)
        : shimIsPlainObject(value);
    };

    /**
     * Checks if `value` is a regular expression.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if the `value` is a regular expression, else `false`.
     * @example
     *
     * _.isRegExp(/fred/);
     * // => true
     */
    function isRegExp(value) {
      return value && typeof value == 'object' && toString.call(value) == regexpClass || false;
    }

    /**
     * Checks if `value` is a string.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if the `value` is a string, else `false`.
     * @example
     *
     * _.isString('fred');
     * // => true
     */
    function isString(value) {
      return typeof value == 'string' ||
        value && typeof value == 'object' && toString.call(value) == stringClass || false;
    }

    /**
     * Checks if `value` is `undefined`.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if the `value` is `undefined`, else `false`.
     * @example
     *
     * _.isUndefined(void 0);
     * // => true
     */
    function isUndefined(value) {
      return typeof value == 'undefined';
    }

    /**
     * Creates an object with the same keys as `object` and values generated by
     * running each own enumerable property of `object` through the callback.
     * The callback is bound to `thisArg` and invoked with three arguments;
     * (value, key, object).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {Object} object The object to iterate over.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Array} Returns a new object with values of the results of each `callback` execution.
     * @example
     *
     * _.mapValues({ 'a': 1, 'b': 2, 'c': 3} , function(num) { return num * 3; });
     * // => { 'a': 3, 'b': 6, 'c': 9 }
     *
     * var characters = {
     *   'fred': { 'name': 'fred', 'age': 40 },
     *   'pebbles': { 'name': 'pebbles', 'age': 1 }
     * };
     *
     * // using "_.pluck" callback shorthand
     * _.mapValues(characters, 'age');
     * // => { 'fred': 40, 'pebbles': 1 }
     */
    function mapValues(object, callback, thisArg) {
      var result = {};
      callback = lodash.createCallback(callback, thisArg, 3);

      forOwn(object, function(value, key, object) {
        result[key] = callback(value, key, object);
      });
      return result;
    }

    /**
     * Recursively merges own enumerable properties of the source object(s), that
     * don't resolve to `undefined` into the destination object. Subsequent sources
     * will overwrite property assignments of previous sources. If a callback is
     * provided it will be executed to produce the merged values of the destination
     * and source properties. If the callback returns `undefined` merging will
     * be handled by the method instead. The callback is bound to `thisArg` and
     * invoked with two arguments; (objectValue, sourceValue).
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {Object} object The destination object.
     * @param {...Object} [source] The source objects.
     * @param {Function} [callback] The function to customize merging properties.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Object} Returns the destination object.
     * @example
     *
     * var names = {
     *   'characters': [
     *     { 'name': 'barney' },
     *     { 'name': 'fred' }
     *   ]
     * };
     *
     * var ages = {
     *   'characters': [
     *     { 'age': 36 },
     *     { 'age': 40 }
     *   ]
     * };
     *
     * _.merge(names, ages);
     * // => { 'characters': [{ 'name': 'barney', 'age': 36 }, { 'name': 'fred', 'age': 40 }] }
     *
     * var food = {
     *   'fruits': ['apple'],
     *   'vegetables': ['beet']
     * };
     *
     * var otherFood = {
     *   'fruits': ['banana'],
     *   'vegetables': ['carrot']
     * };
     *
     * _.merge(food, otherFood, function(a, b) {
     *   return _.isArray(a) ? a.concat(b) : undefined;
     * });
     * // => { 'fruits': ['apple', 'banana'], 'vegetables': ['beet', 'carrot] }
     */
    function merge(object) {
      var args = arguments,
          length = 2;

      if (!isObject(object)) {
        return object;
      }
      // allows working with `_.reduce` and `_.reduceRight` without using
      // their `index` and `collection` arguments
      if (typeof args[2] != 'number') {
        length = args.length;
      }
      if (length > 3 && typeof args[length - 2] == 'function') {
        var callback = baseCreateCallback(args[--length - 1], args[length--], 2);
      } else if (length > 2 && typeof args[length - 1] == 'function') {
        callback = args[--length];
      }
      var sources = slice(arguments, 1, length),
          index = -1,
          stackA = getArray(),
          stackB = getArray();

      while (++index < length) {
        baseMerge(object, sources[index], callback, stackA, stackB);
      }
      releaseArray(stackA);
      releaseArray(stackB);
      return object;
    }

    /**
     * Creates a shallow clone of `object` excluding the specified properties.
     * Property names may be specified as individual arguments or as arrays of
     * property names. If a callback is provided it will be executed for each
     * property of `object` omitting the properties the callback returns truey
     * for. The callback is bound to `thisArg` and invoked with three arguments;
     * (value, key, object).
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {Object} object The source object.
     * @param {Function|...string|string[]} [callback] The properties to omit or the
     *  function called per iteration.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Object} Returns an object without the omitted properties.
     * @example
     *
     * _.omit({ 'name': 'fred', 'age': 40 }, 'age');
     * // => { 'name': 'fred' }
     *
     * _.omit({ 'name': 'fred', 'age': 40 }, function(value) {
     *   return typeof value == 'number';
     * });
     * // => { 'name': 'fred' }
     */
    function omit(object, callback, thisArg) {
      var result = {};
      if (typeof callback != 'function') {
        var props = [];
        forIn(object, function(value, key) {
          props.push(key);
        });
        props = baseDifference(props, baseFlatten(arguments, true, false, 1));

        var index = -1,
            length = props.length;

        while (++index < length) {
          var key = props[index];
          result[key] = object[key];
        }
      } else {
        callback = lodash.createCallback(callback, thisArg, 3);
        forIn(object, function(value, key, object) {
          if (!callback(value, key, object)) {
            result[key] = value;
          }
        });
      }
      return result;
    }

    /**
     * Creates a two dimensional array of an object's key-value pairs,
     * i.e. `[[key1, value1], [key2, value2]]`.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {Object} object The object to inspect.
     * @returns {Array} Returns new array of key-value pairs.
     * @example
     *
     * _.pairs({ 'barney': 36, 'fred': 40 });
     * // => [['barney', 36], ['fred', 40]] (property order is not guaranteed across environments)
     */
    function pairs(object) {
      var index = -1,
          props = keys(object),
          length = props.length,
          result = Array(length);

      while (++index < length) {
        var key = props[index];
        result[index] = [key, object[key]];
      }
      return result;
    }

    /**
     * Creates a shallow clone of `object` composed of the specified properties.
     * Property names may be specified as individual arguments or as arrays of
     * property names. If a callback is provided it will be executed for each
     * property of `object` picking the properties the callback returns truey
     * for. The callback is bound to `thisArg` and invoked with three arguments;
     * (value, key, object).
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {Object} object The source object.
     * @param {Function|...string|string[]} [callback] The function called per
     *  iteration or property names to pick, specified as individual property
     *  names or arrays of property names.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Object} Returns an object composed of the picked properties.
     * @example
     *
     * _.pick({ 'name': 'fred', '_userid': 'fred1' }, 'name');
     * // => { 'name': 'fred' }
     *
     * _.pick({ 'name': 'fred', '_userid': 'fred1' }, function(value, key) {
     *   return key.charAt(0) != '_';
     * });
     * // => { 'name': 'fred' }
     */
    function pick(object, callback, thisArg) {
      var result = {};
      if (typeof callback != 'function') {
        var index = -1,
            props = baseFlatten(arguments, true, false, 1),
            length = isObject(object) ? props.length : 0;

        while (++index < length) {
          var key = props[index];
          if (key in object) {
            result[key] = object[key];
          }
        }
      } else {
        callback = lodash.createCallback(callback, thisArg, 3);
        forIn(object, function(value, key, object) {
          if (callback(value, key, object)) {
            result[key] = value;
          }
        });
      }
      return result;
    }

    /**
     * An alternative to `_.reduce` this method transforms `object` to a new
     * `accumulator` object which is the result of running each of its own
     * enumerable properties through a callback, with each callback execution
     * potentially mutating the `accumulator` object. The callback is bound to
     * `thisArg` and invoked with four arguments; (accumulator, value, key, object).
     * Callbacks may exit iteration early by explicitly returning `false`.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {Array|Object} object The object to iterate over.
     * @param {Function} [callback=identity] The function called per iteration.
     * @param {*} [accumulator] The custom accumulator value.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {*} Returns the accumulated value.
     * @example
     *
     * var squares = _.transform([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], function(result, num) {
     *   num *= num;
     *   if (num % 2) {
     *     return result.push(num) < 3;
     *   }
     * });
     * // => [1, 9, 25]
     *
     * var mapped = _.transform({ 'a': 1, 'b': 2, 'c': 3 }, function(result, num, key) {
     *   result[key] = num * 3;
     * });
     * // => { 'a': 3, 'b': 6, 'c': 9 }
     */
    function transform(object, callback, accumulator, thisArg) {
      var isArr = isArray(object);
      if (accumulator == null) {
        if (isArr) {
          accumulator = [];
        } else {
          var ctor = object && object.constructor,
              proto = ctor && ctor.prototype;

          accumulator = baseCreate(proto);
        }
      }
      if (callback) {
        callback = lodash.createCallback(callback, thisArg, 4);
        (isArr ? forEach : forOwn)(object, function(value, index, object) {
          return callback(accumulator, value, index, object);
        });
      }
      return accumulator;
    }

    /**
     * Creates an array composed of the own enumerable property values of `object`.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {Object} object The object to inspect.
     * @returns {Array} Returns an array of property values.
     * @example
     *
     * _.values({ 'one': 1, 'two': 2, 'three': 3 });
     * // => [1, 2, 3] (property order is not guaranteed across environments)
     */
    function values(object) {
      var index = -1,
          props = keys(object),
          length = props.length,
          result = Array(length);

      while (++index < length) {
        result[index] = object[props[index]];
      }
      return result;
    }

    /*--------------------------------------------------------------------------*/

    /**
     * Creates an array of elements from the specified indexes, or keys, of the
     * `collection`. Indexes may be specified as individual arguments or as arrays
     * of indexes.
     *
     * @static
     * @memberOf _
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {...(number|number[]|string|string[])} [index] The indexes of `collection`
     *   to retrieve, specified as individual indexes or arrays of indexes.
     * @returns {Array} Returns a new array of elements corresponding to the
     *  provided indexes.
     * @example
     *
     * _.at(['a', 'b', 'c', 'd', 'e'], [0, 2, 4]);
     * // => ['a', 'c', 'e']
     *
     * _.at(['fred', 'barney', 'pebbles'], 0, 2);
     * // => ['fred', 'pebbles']
     */
    function at(collection) {
      var args = arguments,
          index = -1,
          props = baseFlatten(args, true, false, 1),
          length = (args[2] && args[2][args[1]] === collection) ? 1 : props.length,
          result = Array(length);

      while(++index < length) {
        result[index] = collection[props[index]];
      }
      return result;
    }

    /**
     * Checks if a given value is present in a collection using strict equality
     * for comparisons, i.e. `===`. If `fromIndex` is negative, it is used as the
     * offset from the end of the collection.
     *
     * @static
     * @memberOf _
     * @alias include
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {*} target The value to check for.
     * @param {number} [fromIndex=0] The index to search from.
     * @returns {boolean} Returns `true` if the `target` element is found, else `false`.
     * @example
     *
     * _.contains([1, 2, 3], 1);
     * // => true
     *
     * _.contains([1, 2, 3], 1, 2);
     * // => false
     *
     * _.contains({ 'name': 'fred', 'age': 40 }, 'fred');
     * // => true
     *
     * _.contains('pebbles', 'eb');
     * // => true
     */
    function contains(collection, target, fromIndex) {
      var index = -1,
          indexOf = getIndexOf(),
          length = collection ? collection.length : 0,
          result = false;

      fromIndex = (fromIndex < 0 ? nativeMax(0, length + fromIndex) : fromIndex) || 0;
      if (isArray(collection)) {
        result = indexOf(collection, target, fromIndex) > -1;
      } else if (typeof length == 'number') {
        result = (isString(collection) ? collection.indexOf(target, fromIndex) : indexOf(collection, target, fromIndex)) > -1;
      } else {
        forOwn(collection, function(value) {
          if (++index >= fromIndex) {
            return !(result = value === target);
          }
        });
      }
      return result;
    }

    /**
     * Creates an object composed of keys generated from the results of running
     * each element of `collection` through the callback. The corresponding value
     * of each key is the number of times the key was returned by the callback.
     * The callback is bound to `thisArg` and invoked with three arguments;
     * (value, index|key, collection).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Object} Returns the composed aggregate object.
     * @example
     *
     * _.countBy([4.3, 6.1, 6.4], function(num) { return Math.floor(num); });
     * // => { '4': 1, '6': 2 }
     *
     * _.countBy([4.3, 6.1, 6.4], function(num) { return this.floor(num); }, Math);
     * // => { '4': 1, '6': 2 }
     *
     * _.countBy(['one', 'two', 'three'], 'length');
     * // => { '3': 2, '5': 1 }
     */
    var countBy = createAggregator(function(result, value, key) {
      (hasOwnProperty.call(result, key) ? result[key]++ : result[key] = 1);
    });

    /**
     * Checks if the given callback returns truey value for **all** elements of
     * a collection. The callback is bound to `thisArg` and invoked with three
     * arguments; (value, index|key, collection).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @alias all
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {boolean} Returns `true` if all elements passed the callback check,
     *  else `false`.
     * @example
     *
     * _.every([true, 1, null, 'yes']);
     * // => false
     *
     * var characters = [
     *   { 'name': 'barney', 'age': 36 },
     *   { 'name': 'fred',   'age': 40 }
     * ];
     *
     * // using "_.pluck" callback shorthand
     * _.every(characters, 'age');
     * // => true
     *
     * // using "_.where" callback shorthand
     * _.every(characters, { 'age': 36 });
     * // => false
     */
    function every(collection, callback, thisArg) {
      var result = true;
      callback = lodash.createCallback(callback, thisArg, 3);

      var index = -1,
          length = collection ? collection.length : 0;

      if (typeof length == 'number') {
        while (++index < length) {
          if (!(result = !!callback(collection[index], index, collection))) {
            break;
          }
        }
      } else {
        forOwn(collection, function(value, index, collection) {
          return (result = !!callback(value, index, collection));
        });
      }
      return result;
    }

    /**
     * Iterates over elements of a collection, returning an array of all elements
     * the callback returns truey for. The callback is bound to `thisArg` and
     * invoked with three arguments; (value, index|key, collection).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @alias select
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Array} Returns a new array of elements that passed the callback check.
     * @example
     *
     * var evens = _.filter([1, 2, 3, 4, 5, 6], function(num) { return num % 2 == 0; });
     * // => [2, 4, 6]
     *
     * var characters = [
     *   { 'name': 'barney', 'age': 36, 'blocked': false },
     *   { 'name': 'fred',   'age': 40, 'blocked': true }
     * ];
     *
     * // using "_.pluck" callback shorthand
     * _.filter(characters, 'blocked');
     * // => [{ 'name': 'fred', 'age': 40, 'blocked': true }]
     *
     * // using "_.where" callback shorthand
     * _.filter(characters, { 'age': 36 });
     * // => [{ 'name': 'barney', 'age': 36, 'blocked': false }]
     */
    function filter(collection, callback, thisArg) {
      var result = [];
      callback = lodash.createCallback(callback, thisArg, 3);

      var index = -1,
          length = collection ? collection.length : 0;

      if (typeof length == 'number') {
        while (++index < length) {
          var value = collection[index];
          if (callback(value, index, collection)) {
            result.push(value);
          }
        }
      } else {
        forOwn(collection, function(value, index, collection) {
          if (callback(value, index, collection)) {
            result.push(value);
          }
        });
      }
      return result;
    }

    /**
     * Iterates over elements of a collection, returning the first element that
     * the callback returns truey for. The callback is bound to `thisArg` and
     * invoked with three arguments; (value, index|key, collection).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @alias detect, findWhere
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {*} Returns the found element, else `undefined`.
     * @example
     *
     * var characters = [
     *   { 'name': 'barney',  'age': 36, 'blocked': false },
     *   { 'name': 'fred',    'age': 40, 'blocked': true },
     *   { 'name': 'pebbles', 'age': 1,  'blocked': false }
     * ];
     *
     * _.find(characters, function(chr) {
     *   return chr.age < 40;
     * });
     * // => { 'name': 'barney', 'age': 36, 'blocked': false }
     *
     * // using "_.where" callback shorthand
     * _.find(characters, { 'age': 1 });
     * // =>  { 'name': 'pebbles', 'age': 1, 'blocked': false }
     *
     * // using "_.pluck" callback shorthand
     * _.find(characters, 'blocked');
     * // => { 'name': 'fred', 'age': 40, 'blocked': true }
     */
    function find(collection, callback, thisArg) {
      callback = lodash.createCallback(callback, thisArg, 3);

      var index = -1,
          length = collection ? collection.length : 0;

      if (typeof length == 'number') {
        while (++index < length) {
          var value = collection[index];
          if (callback(value, index, collection)) {
            return value;
          }
        }
      } else {
        var result;
        forOwn(collection, function(value, index, collection) {
          if (callback(value, index, collection)) {
            result = value;
            return false;
          }
        });
        return result;
      }
    }

    /**
     * This method is like `_.find` except that it iterates over elements
     * of a `collection` from right to left.
     *
     * @static
     * @memberOf _
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {*} Returns the found element, else `undefined`.
     * @example
     *
     * _.findLast([1, 2, 3, 4], function(num) {
     *   return num % 2 == 1;
     * });
     * // => 3
     */
    function findLast(collection, callback, thisArg) {
      var result;
      callback = lodash.createCallback(callback, thisArg, 3);
      forEachRight(collection, function(value, index, collection) {
        if (callback(value, index, collection)) {
          result = value;
          return false;
        }
      });
      return result;
    }

    /**
     * Iterates over elements of a collection, executing the callback for each
     * element. The callback is bound to `thisArg` and invoked with three arguments;
     * (value, index|key, collection). Callbacks may exit iteration early by
     * explicitly returning `false`.
     *
     * Note: As with other "Collections" methods, objects with a `length` property
     * are iterated like arrays. To avoid this behavior `_.forIn` or `_.forOwn`
     * may be used for object iteration.
     *
     * @static
     * @memberOf _
     * @alias each
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function} [callback=identity] The function called per iteration.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Array|Object|string} Returns `collection`.
     * @example
     *
     * _([1, 2, 3]).forEach(function(num) { console.log(num); }).join(',');
     * // => logs each number and returns '1,2,3'
     *
     * _.forEach({ 'one': 1, 'two': 2, 'three': 3 }, function(num) { console.log(num); });
     * // => logs each number and returns the object (property order is not guaranteed across environments)
     */
    function forEach(collection, callback, thisArg) {
      var index = -1,
          length = collection ? collection.length : 0;

      callback = callback && typeof thisArg == 'undefined' ? callback : baseCreateCallback(callback, thisArg, 3);
      if (typeof length == 'number') {
        while (++index < length) {
          if (callback(collection[index], index, collection) === false) {
            break;
          }
        }
      } else {
        forOwn(collection, callback);
      }
      return collection;
    }

    /**
     * This method is like `_.forEach` except that it iterates over elements
     * of a `collection` from right to left.
     *
     * @static
     * @memberOf _
     * @alias eachRight
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function} [callback=identity] The function called per iteration.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Array|Object|string} Returns `collection`.
     * @example
     *
     * _([1, 2, 3]).forEachRight(function(num) { console.log(num); }).join(',');
     * // => logs each number from right to left and returns '3,2,1'
     */
    function forEachRight(collection, callback, thisArg) {
      var length = collection ? collection.length : 0;
      callback = callback && typeof thisArg == 'undefined' ? callback : baseCreateCallback(callback, thisArg, 3);
      if (typeof length == 'number') {
        while (length--) {
          if (callback(collection[length], length, collection) === false) {
            break;
          }
        }
      } else {
        var props = keys(collection);
        length = props.length;
        forOwn(collection, function(value, key, collection) {
          key = props ? props[--length] : --length;
          return callback(collection[key], key, collection);
        });
      }
      return collection;
    }

    /**
     * Creates an object composed of keys generated from the results of running
     * each element of a collection through the callback. The corresponding value
     * of each key is an array of the elements responsible for generating the key.
     * The callback is bound to `thisArg` and invoked with three arguments;
     * (value, index|key, collection).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`
     *
     * @static
     * @memberOf _
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Object} Returns the composed aggregate object.
     * @example
     *
     * _.groupBy([4.2, 6.1, 6.4], function(num) { return Math.floor(num); });
     * // => { '4': [4.2], '6': [6.1, 6.4] }
     *
     * _.groupBy([4.2, 6.1, 6.4], function(num) { return this.floor(num); }, Math);
     * // => { '4': [4.2], '6': [6.1, 6.4] }
     *
     * // using "_.pluck" callback shorthand
     * _.groupBy(['one', 'two', 'three'], 'length');
     * // => { '3': ['one', 'two'], '5': ['three'] }
     */
    var groupBy = createAggregator(function(result, value, key) {
      (hasOwnProperty.call(result, key) ? result[key] : result[key] = []).push(value);
    });

    /**
     * Creates an object composed of keys generated from the results of running
     * each element of the collection through the given callback. The corresponding
     * value of each key is the last element responsible for generating the key.
     * The callback is bound to `thisArg` and invoked with three arguments;
     * (value, index|key, collection).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Object} Returns the composed aggregate object.
     * @example
     *
     * var keys = [
     *   { 'dir': 'left', 'code': 97 },
     *   { 'dir': 'right', 'code': 100 }
     * ];
     *
     * _.indexBy(keys, 'dir');
     * // => { 'left': { 'dir': 'left', 'code': 97 }, 'right': { 'dir': 'right', 'code': 100 } }
     *
     * _.indexBy(keys, function(key) { return String.fromCharCode(key.code); });
     * // => { 'a': { 'dir': 'left', 'code': 97 }, 'd': { 'dir': 'right', 'code': 100 } }
     *
     * _.indexBy(characters, function(key) { this.fromCharCode(key.code); }, String);
     * // => { 'a': { 'dir': 'left', 'code': 97 }, 'd': { 'dir': 'right', 'code': 100 } }
     */
    var indexBy = createAggregator(function(result, value, key) {
      result[key] = value;
    });

    /**
     * Invokes the method named by `methodName` on each element in the `collection`
     * returning an array of the results of each invoked method. Additional arguments
     * will be provided to each invoked method. If `methodName` is a function it
     * will be invoked for, and `this` bound to, each element in the `collection`.
     *
     * @static
     * @memberOf _
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|string} methodName The name of the method to invoke or
     *  the function invoked per iteration.
     * @param {...*} [arg] Arguments to invoke the method with.
     * @returns {Array} Returns a new array of the results of each invoked method.
     * @example
     *
     * _.invoke([[5, 1, 7], [3, 2, 1]], 'sort');
     * // => [[1, 5, 7], [1, 2, 3]]
     *
     * _.invoke([123, 456], String.prototype.split, '');
     * // => [['1', '2', '3'], ['4', '5', '6']]
     */
    function invoke(collection, methodName) {
      var args = slice(arguments, 2),
          index = -1,
          isFunc = typeof methodName == 'function',
          length = collection ? collection.length : 0,
          result = Array(typeof length == 'number' ? length : 0);

      forEach(collection, function(value) {
        result[++index] = (isFunc ? methodName : value[methodName]).apply(value, args);
      });
      return result;
    }

    /**
     * Creates an array of values by running each element in the collection
     * through the callback. The callback is bound to `thisArg` and invoked with
     * three arguments; (value, index|key, collection).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @alias collect
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Array} Returns a new array of the results of each `callback` execution.
     * @example
     *
     * _.map([1, 2, 3], function(num) { return num * 3; });
     * // => [3, 6, 9]
     *
     * _.map({ 'one': 1, 'two': 2, 'three': 3 }, function(num) { return num * 3; });
     * // => [3, 6, 9] (property order is not guaranteed across environments)
     *
     * var characters = [
     *   { 'name': 'barney', 'age': 36 },
     *   { 'name': 'fred',   'age': 40 }
     * ];
     *
     * // using "_.pluck" callback shorthand
     * _.map(characters, 'name');
     * // => ['barney', 'fred']
     */
    function map(collection, callback, thisArg) {
      var index = -1,
          length = collection ? collection.length : 0;

      callback = lodash.createCallback(callback, thisArg, 3);
      if (typeof length == 'number') {
        var result = Array(length);
        while (++index < length) {
          result[index] = callback(collection[index], index, collection);
        }
      } else {
        result = [];
        forOwn(collection, function(value, key, collection) {
          result[++index] = callback(value, key, collection);
        });
      }
      return result;
    }

    /**
     * Retrieves the maximum value of a collection. If the collection is empty or
     * falsey `-Infinity` is returned. If a callback is provided it will be executed
     * for each value in the collection to generate the criterion by which the value
     * is ranked. The callback is bound to `thisArg` and invoked with three
     * arguments; (value, index, collection).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {*} Returns the maximum value.
     * @example
     *
     * _.max([4, 2, 8, 6]);
     * // => 8
     *
     * var characters = [
     *   { 'name': 'barney', 'age': 36 },
     *   { 'name': 'fred',   'age': 40 }
     * ];
     *
     * _.max(characters, function(chr) { return chr.age; });
     * // => { 'name': 'fred', 'age': 40 };
     *
     * // using "_.pluck" callback shorthand
     * _.max(characters, 'age');
     * // => { 'name': 'fred', 'age': 40 };
     */
    function max(collection, callback, thisArg) {
      var computed = -Infinity,
          result = computed;

      // allows working with functions like `_.map` without using
      // their `index` argument as a callback
      if (typeof callback != 'function' && thisArg && thisArg[callback] === collection) {
        callback = null;
      }
      if (callback == null && isArray(collection)) {
        var index = -1,
            length = collection.length;

        while (++index < length) {
          var value = collection[index];
          if (value > result) {
            result = value;
          }
        }
      } else {
        callback = (callback == null && isString(collection))
          ? charAtCallback
          : lodash.createCallback(callback, thisArg, 3);

        forEach(collection, function(value, index, collection) {
          var current = callback(value, index, collection);
          if (current > computed) {
            computed = current;
            result = value;
          }
        });
      }
      return result;
    }

    /**
     * Retrieves the minimum value of a collection. If the collection is empty or
     * falsey `Infinity` is returned. If a callback is provided it will be executed
     * for each value in the collection to generate the criterion by which the value
     * is ranked. The callback is bound to `thisArg` and invoked with three
     * arguments; (value, index, collection).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {*} Returns the minimum value.
     * @example
     *
     * _.min([4, 2, 8, 6]);
     * // => 2
     *
     * var characters = [
     *   { 'name': 'barney', 'age': 36 },
     *   { 'name': 'fred',   'age': 40 }
     * ];
     *
     * _.min(characters, function(chr) { return chr.age; });
     * // => { 'name': 'barney', 'age': 36 };
     *
     * // using "_.pluck" callback shorthand
     * _.min(characters, 'age');
     * // => { 'name': 'barney', 'age': 36 };
     */
    function min(collection, callback, thisArg) {
      var computed = Infinity,
          result = computed;

      // allows working with functions like `_.map` without using
      // their `index` argument as a callback
      if (typeof callback != 'function' && thisArg && thisArg[callback] === collection) {
        callback = null;
      }
      if (callback == null && isArray(collection)) {
        var index = -1,
            length = collection.length;

        while (++index < length) {
          var value = collection[index];
          if (value < result) {
            result = value;
          }
        }
      } else {
        callback = (callback == null && isString(collection))
          ? charAtCallback
          : lodash.createCallback(callback, thisArg, 3);

        forEach(collection, function(value, index, collection) {
          var current = callback(value, index, collection);
          if (current < computed) {
            computed = current;
            result = value;
          }
        });
      }
      return result;
    }

    /**
     * Retrieves the value of a specified property from all elements in the collection.
     *
     * @static
     * @memberOf _
     * @type Function
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {string} property The name of the property to pluck.
     * @returns {Array} Returns a new array of property values.
     * @example
     *
     * var characters = [
     *   { 'name': 'barney', 'age': 36 },
     *   { 'name': 'fred',   'age': 40 }
     * ];
     *
     * _.pluck(characters, 'name');
     * // => ['barney', 'fred']
     */
    var pluck = map;

    /**
     * Reduces a collection to a value which is the accumulated result of running
     * each element in the collection through the callback, where each successive
     * callback execution consumes the return value of the previous execution. If
     * `accumulator` is not provided the first element of the collection will be
     * used as the initial `accumulator` value. The callback is bound to `thisArg`
     * and invoked with four arguments; (accumulator, value, index|key, collection).
     *
     * @static
     * @memberOf _
     * @alias foldl, inject
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function} [callback=identity] The function called per iteration.
     * @param {*} [accumulator] Initial value of the accumulator.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {*} Returns the accumulated value.
     * @example
     *
     * var sum = _.reduce([1, 2, 3], function(sum, num) {
     *   return sum + num;
     * });
     * // => 6
     *
     * var mapped = _.reduce({ 'a': 1, 'b': 2, 'c': 3 }, function(result, num, key) {
     *   result[key] = num * 3;
     *   return result;
     * }, {});
     * // => { 'a': 3, 'b': 6, 'c': 9 }
     */
    function reduce(collection, callback, accumulator, thisArg) {
      if (!collection) return accumulator;
      var noaccum = arguments.length < 3;
      callback = lodash.createCallback(callback, thisArg, 4);

      var index = -1,
          length = collection.length;

      if (typeof length == 'number') {
        if (noaccum) {
          accumulator = collection[++index];
        }
        while (++index < length) {
          accumulator = callback(accumulator, collection[index], index, collection);
        }
      } else {
        forOwn(collection, function(value, index, collection) {
          accumulator = noaccum
            ? (noaccum = false, value)
            : callback(accumulator, value, index, collection)
        });
      }
      return accumulator;
    }

    /**
     * This method is like `_.reduce` except that it iterates over elements
     * of a `collection` from right to left.
     *
     * @static
     * @memberOf _
     * @alias foldr
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function} [callback=identity] The function called per iteration.
     * @param {*} [accumulator] Initial value of the accumulator.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {*} Returns the accumulated value.
     * @example
     *
     * var list = [[0, 1], [2, 3], [4, 5]];
     * var flat = _.reduceRight(list, function(a, b) { return a.concat(b); }, []);
     * // => [4, 5, 2, 3, 0, 1]
     */
    function reduceRight(collection, callback, accumulator, thisArg) {
      var noaccum = arguments.length < 3;
      callback = lodash.createCallback(callback, thisArg, 4);
      forEachRight(collection, function(value, index, collection) {
        accumulator = noaccum
          ? (noaccum = false, value)
          : callback(accumulator, value, index, collection);
      });
      return accumulator;
    }

    /**
     * The opposite of `_.filter` this method returns the elements of a
     * collection that the callback does **not** return truey for.
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Array} Returns a new array of elements that failed the callback check.
     * @example
     *
     * var odds = _.reject([1, 2, 3, 4, 5, 6], function(num) { return num % 2 == 0; });
     * // => [1, 3, 5]
     *
     * var characters = [
     *   { 'name': 'barney', 'age': 36, 'blocked': false },
     *   { 'name': 'fred',   'age': 40, 'blocked': true }
     * ];
     *
     * // using "_.pluck" callback shorthand
     * _.reject(characters, 'blocked');
     * // => [{ 'name': 'barney', 'age': 36, 'blocked': false }]
     *
     * // using "_.where" callback shorthand
     * _.reject(characters, { 'age': 36 });
     * // => [{ 'name': 'fred', 'age': 40, 'blocked': true }]
     */
    function reject(collection, callback, thisArg) {
      callback = lodash.createCallback(callback, thisArg, 3);
      return filter(collection, function(value, index, collection) {
        return !callback(value, index, collection);
      });
    }

    /**
     * Retrieves a random element or `n` random elements from a collection.
     *
     * @static
     * @memberOf _
     * @category Collections
     * @param {Array|Object|string} collection The collection to sample.
     * @param {number} [n] The number of elements to sample.
     * @param- {Object} [guard] Allows working with functions like `_.map`
     *  without using their `index` arguments as `n`.
     * @returns {Array} Returns the random sample(s) of `collection`.
     * @example
     *
     * _.sample([1, 2, 3, 4]);
     * // => 2
     *
     * _.sample([1, 2, 3, 4], 2);
     * // => [3, 1]
     */
    function sample(collection, n, guard) {
      if (collection && typeof collection.length != 'number') {
        collection = values(collection);
      }
      if (n == null || guard) {
        return collection ? collection[baseRandom(0, collection.length - 1)] : undefined;
      }
      var result = shuffle(collection);
      result.length = nativeMin(nativeMax(0, n), result.length);
      return result;
    }

    /**
     * Creates an array of shuffled values, using a version of the Fisher-Yates
     * shuffle. See http://en.wikipedia.org/wiki/Fisher-Yates_shuffle.
     *
     * @static
     * @memberOf _
     * @category Collections
     * @param {Array|Object|string} collection The collection to shuffle.
     * @returns {Array} Returns a new shuffled collection.
     * @example
     *
     * _.shuffle([1, 2, 3, 4, 5, 6]);
     * // => [4, 1, 6, 3, 5, 2]
     */
    function shuffle(collection) {
      var index = -1,
          length = collection ? collection.length : 0,
          result = Array(typeof length == 'number' ? length : 0);

      forEach(collection, function(value) {
        var rand = baseRandom(0, ++index);
        result[index] = result[rand];
        result[rand] = value;
      });
      return result;
    }

    /**
     * Gets the size of the `collection` by returning `collection.length` for arrays
     * and array-like objects or the number of own enumerable properties for objects.
     *
     * @static
     * @memberOf _
     * @category Collections
     * @param {Array|Object|string} collection The collection to inspect.
     * @returns {number} Returns `collection.length` or number of own enumerable properties.
     * @example
     *
     * _.size([1, 2]);
     * // => 2
     *
     * _.size({ 'one': 1, 'two': 2, 'three': 3 });
     * // => 3
     *
     * _.size('pebbles');
     * // => 7
     */
    function size(collection) {
      var length = collection ? collection.length : 0;
      return typeof length == 'number' ? length : keys(collection).length;
    }

    /**
     * Checks if the callback returns a truey value for **any** element of a
     * collection. The function returns as soon as it finds a passing value and
     * does not iterate over the entire collection. The callback is bound to
     * `thisArg` and invoked with three arguments; (value, index|key, collection).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @alias any
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {boolean} Returns `true` if any element passed the callback check,
     *  else `false`.
     * @example
     *
     * _.some([null, 0, 'yes', false], Boolean);
     * // => true
     *
     * var characters = [
     *   { 'name': 'barney', 'age': 36, 'blocked': false },
     *   { 'name': 'fred',   'age': 40, 'blocked': true }
     * ];
     *
     * // using "_.pluck" callback shorthand
     * _.some(characters, 'blocked');
     * // => true
     *
     * // using "_.where" callback shorthand
     * _.some(characters, { 'age': 1 });
     * // => false
     */
    function some(collection, callback, thisArg) {
      var result;
      callback = lodash.createCallback(callback, thisArg, 3);

      var index = -1,
          length = collection ? collection.length : 0;

      if (typeof length == 'number') {
        while (++index < length) {
          if ((result = callback(collection[index], index, collection))) {
            break;
          }
        }
      } else {
        forOwn(collection, function(value, index, collection) {
          return !(result = callback(value, index, collection));
        });
      }
      return !!result;
    }

    /**
     * Creates an array of elements, sorted in ascending order by the results of
     * running each element in a collection through the callback. This method
     * performs a stable sort, that is, it will preserve the original sort order
     * of equal elements. The callback is bound to `thisArg` and invoked with
     * three arguments; (value, index|key, collection).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an array of property names is provided for `callback` the collection
     * will be sorted by each property value.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Array|Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Array} Returns a new array of sorted elements.
     * @example
     *
     * _.sortBy([1, 2, 3], function(num) { return Math.sin(num); });
     * // => [3, 1, 2]
     *
     * _.sortBy([1, 2, 3], function(num) { return this.sin(num); }, Math);
     * // => [3, 1, 2]
     *
     * var characters = [
     *   { 'name': 'barney',  'age': 36 },
     *   { 'name': 'fred',    'age': 40 },
     *   { 'name': 'barney',  'age': 26 },
     *   { 'name': 'fred',    'age': 30 }
     * ];
     *
     * // using "_.pluck" callback shorthand
     * _.map(_.sortBy(characters, 'age'), _.values);
     * // => [['barney', 26], ['fred', 30], ['barney', 36], ['fred', 40]]
     *
     * // sorting by multiple properties
     * _.map(_.sortBy(characters, ['name', 'age']), _.values);
     * // = > [['barney', 26], ['barney', 36], ['fred', 30], ['fred', 40]]
     */
    function sortBy(collection, callback, thisArg) {
      var index = -1,
          isArr = isArray(callback),
          length = collection ? collection.length : 0,
          result = Array(typeof length == 'number' ? length : 0);

      if (!isArr) {
        callback = lodash.createCallback(callback, thisArg, 3);
      }
      forEach(collection, function(value, key, collection) {
        var object = result[++index] = getObject();
        if (isArr) {
          object.criteria = map(callback, function(key) { return value[key]; });
        } else {
          (object.criteria = getArray())[0] = callback(value, key, collection);
        }
        object.index = index;
        object.value = value;
      });

      length = result.length;
      result.sort(compareAscending);
      while (length--) {
        var object = result[length];
        result[length] = object.value;
        if (!isArr) {
          releaseArray(object.criteria);
        }
        releaseObject(object);
      }
      return result;
    }

    /**
     * Converts the `collection` to an array.
     *
     * @static
     * @memberOf _
     * @category Collections
     * @param {Array|Object|string} collection The collection to convert.
     * @returns {Array} Returns the new converted array.
     * @example
     *
     * (function() { return _.toArray(arguments).slice(1); })(1, 2, 3, 4);
     * // => [2, 3, 4]
     */
    function toArray(collection) {
      if (collection && typeof collection.length == 'number') {
        return slice(collection);
      }
      return values(collection);
    }

    /**
     * Performs a deep comparison of each element in a `collection` to the given
     * `properties` object, returning an array of all elements that have equivalent
     * property values.
     *
     * @static
     * @memberOf _
     * @type Function
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Object} props The object of property values to filter by.
     * @returns {Array} Returns a new array of elements that have the given properties.
     * @example
     *
     * var characters = [
     *   { 'name': 'barney', 'age': 36, 'pets': ['hoppy'] },
     *   { 'name': 'fred',   'age': 40, 'pets': ['baby puss', 'dino'] }
     * ];
     *
     * _.where(characters, { 'age': 36 });
     * // => [{ 'name': 'barney', 'age': 36, 'pets': ['hoppy'] }]
     *
     * _.where(characters, { 'pets': ['dino'] });
     * // => [{ 'name': 'fred', 'age': 40, 'pets': ['baby puss', 'dino'] }]
     */
    var where = filter;

    /*--------------------------------------------------------------------------*/

    /**
     * Creates an array with all falsey values removed. The values `false`, `null`,
     * `0`, `""`, `undefined`, and `NaN` are all falsey.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {Array} array The array to compact.
     * @returns {Array} Returns a new array of filtered values.
     * @example
     *
     * _.compact([0, 1, false, 2, '', 3]);
     * // => [1, 2, 3]
     */
    function compact(array) {
      var index = -1,
          length = array ? array.length : 0,
          result = [];

      while (++index < length) {
        var value = array[index];
        if (value) {
          result.push(value);
        }
      }
      return result;
    }

    /**
     * Creates an array excluding all values of the provided arrays using strict
     * equality for comparisons, i.e. `===`.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {Array} array The array to process.
     * @param {...Array} [values] The arrays of values to exclude.
     * @returns {Array} Returns a new array of filtered values.
     * @example
     *
     * _.difference([1, 2, 3, 4, 5], [5, 2, 10]);
     * // => [1, 3, 4]
     */
    function difference(array) {
      return baseDifference(array, baseFlatten(arguments, true, true, 1));
    }

    /**
     * This method is like `_.find` except that it returns the index of the first
     * element that passes the callback check, instead of the element itself.
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {Array} array The array to search.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {number} Returns the index of the found element, else `-1`.
     * @example
     *
     * var characters = [
     *   { 'name': 'barney',  'age': 36, 'blocked': false },
     *   { 'name': 'fred',    'age': 40, 'blocked': true },
     *   { 'name': 'pebbles', 'age': 1,  'blocked': false }
     * ];
     *
     * _.findIndex(characters, function(chr) {
     *   return chr.age < 20;
     * });
     * // => 2
     *
     * // using "_.where" callback shorthand
     * _.findIndex(characters, { 'age': 36 });
     * // => 0
     *
     * // using "_.pluck" callback shorthand
     * _.findIndex(characters, 'blocked');
     * // => 1
     */
    function findIndex(array, callback, thisArg) {
      var index = -1,
          length = array ? array.length : 0;

      callback = lodash.createCallback(callback, thisArg, 3);
      while (++index < length) {
        if (callback(array[index], index, array)) {
          return index;
        }
      }
      return -1;
    }

    /**
     * This method is like `_.findIndex` except that it iterates over elements
     * of a `collection` from right to left.
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {Array} array The array to search.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {number} Returns the index of the found element, else `-1`.
     * @example
     *
     * var characters = [
     *   { 'name': 'barney',  'age': 36, 'blocked': true },
     *   { 'name': 'fred',    'age': 40, 'blocked': false },
     *   { 'name': 'pebbles', 'age': 1,  'blocked': true }
     * ];
     *
     * _.findLastIndex(characters, function(chr) {
     *   return chr.age > 30;
     * });
     * // => 1
     *
     * // using "_.where" callback shorthand
     * _.findLastIndex(characters, { 'age': 36 });
     * // => 0
     *
     * // using "_.pluck" callback shorthand
     * _.findLastIndex(characters, 'blocked');
     * // => 2
     */
    function findLastIndex(array, callback, thisArg) {
      var length = array ? array.length : 0;
      callback = lodash.createCallback(callback, thisArg, 3);
      while (length--) {
        if (callback(array[length], length, array)) {
          return length;
        }
      }
      return -1;
    }

    /**
     * Gets the first element or first `n` elements of an array. If a callback
     * is provided elements at the beginning of the array are returned as long
     * as the callback returns truey. The callback is bound to `thisArg` and
     * invoked with three arguments; (value, index, array).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @alias head, take
     * @category Arrays
     * @param {Array} array The array to query.
     * @param {Function|Object|number|string} [callback] The function called
     *  per element or the number of elements to return. If a property name or
     *  object is provided it will be used to create a "_.pluck" or "_.where"
     *  style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {*} Returns the first element(s) of `array`.
     * @example
     *
     * _.first([1, 2, 3]);
     * // => 1
     *
     * _.first([1, 2, 3], 2);
     * // => [1, 2]
     *
     * _.first([1, 2, 3], function(num) {
     *   return num < 3;
     * });
     * // => [1, 2]
     *
     * var characters = [
     *   { 'name': 'barney',  'blocked': true,  'employer': 'slate' },
     *   { 'name': 'fred',    'blocked': false, 'employer': 'slate' },
     *   { 'name': 'pebbles', 'blocked': true,  'employer': 'na' }
     * ];
     *
     * // using "_.pluck" callback shorthand
     * _.first(characters, 'blocked');
     * // => [{ 'name': 'barney', 'blocked': true, 'employer': 'slate' }]
     *
     * // using "_.where" callback shorthand
     * _.pluck(_.first(characters, { 'employer': 'slate' }), 'name');
     * // => ['barney', 'fred']
     */
    function first(array, callback, thisArg) {
      var n = 0,
          length = array ? array.length : 0;

      if (typeof callback != 'number' && callback != null) {
        var index = -1;
        callback = lodash.createCallback(callback, thisArg, 3);
        while (++index < length && callback(array[index], index, array)) {
          n++;
        }
      } else {
        n = callback;
        if (n == null || thisArg) {
          return array ? array[0] : undefined;
        }
      }
      return slice(array, 0, nativeMin(nativeMax(0, n), length));
    }

    /**
     * Flattens a nested array (the nesting can be to any depth). If `isShallow`
     * is truey, the array will only be flattened a single level. If a callback
     * is provided each element of the array is passed through the callback before
     * flattening. The callback is bound to `thisArg` and invoked with three
     * arguments; (value, index, array).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {Array} array The array to flatten.
     * @param {boolean} [isShallow=false] A flag to restrict flattening to a single level.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Array} Returns a new flattened array.
     * @example
     *
     * _.flatten([1, [2], [3, [[4]]]]);
     * // => [1, 2, 3, 4];
     *
     * _.flatten([1, [2], [3, [[4]]]], true);
     * // => [1, 2, 3, [[4]]];
     *
     * var characters = [
     *   { 'name': 'barney', 'age': 30, 'pets': ['hoppy'] },
     *   { 'name': 'fred',   'age': 40, 'pets': ['baby puss', 'dino'] }
     * ];
     *
     * // using "_.pluck" callback shorthand
     * _.flatten(characters, 'pets');
     * // => ['hoppy', 'baby puss', 'dino']
     */
    function flatten(array, isShallow, callback, thisArg) {
      // juggle arguments
      if (typeof isShallow != 'boolean' && isShallow != null) {
        thisArg = callback;
        callback = (typeof isShallow != 'function' && thisArg && thisArg[isShallow] === array) ? null : isShallow;
        isShallow = false;
      }
      if (callback != null) {
        array = map(array, callback, thisArg);
      }
      return baseFlatten(array, isShallow);
    }

    /**
     * Gets the index at which the first occurrence of `value` is found using
     * strict equality for comparisons, i.e. `===`. If the array is already sorted
     * providing `true` for `fromIndex` will run a faster binary search.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {Array} array The array to search.
     * @param {*} value The value to search for.
     * @param {boolean|number} [fromIndex=0] The index to search from or `true`
     *  to perform a binary search on a sorted array.
     * @returns {number} Returns the index of the matched value or `-1`.
     * @example
     *
     * _.indexOf([1, 2, 3, 1, 2, 3], 2);
     * // => 1
     *
     * _.indexOf([1, 2, 3, 1, 2, 3], 2, 3);
     * // => 4
     *
     * _.indexOf([1, 1, 2, 2, 3, 3], 2, true);
     * // => 2
     */
    function indexOf(array, value, fromIndex) {
      if (typeof fromIndex == 'number') {
        var length = array ? array.length : 0;
        fromIndex = (fromIndex < 0 ? nativeMax(0, length + fromIndex) : fromIndex || 0);
      } else if (fromIndex) {
        var index = sortedIndex(array, value);
        return array[index] === value ? index : -1;
      }
      return baseIndexOf(array, value, fromIndex);
    }

    /**
     * Gets all but the last element or last `n` elements of an array. If a
     * callback is provided elements at the end of the array are excluded from
     * the result as long as the callback returns truey. The callback is bound
     * to `thisArg` and invoked with three arguments; (value, index, array).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {Array} array The array to query.
     * @param {Function|Object|number|string} [callback=1] The function called
     *  per element or the number of elements to exclude. If a property name or
     *  object is provided it will be used to create a "_.pluck" or "_.where"
     *  style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Array} Returns a slice of `array`.
     * @example
     *
     * _.initial([1, 2, 3]);
     * // => [1, 2]
     *
     * _.initial([1, 2, 3], 2);
     * // => [1]
     *
     * _.initial([1, 2, 3], function(num) {
     *   return num > 1;
     * });
     * // => [1]
     *
     * var characters = [
     *   { 'name': 'barney',  'blocked': false, 'employer': 'slate' },
     *   { 'name': 'fred',    'blocked': true,  'employer': 'slate' },
     *   { 'name': 'pebbles', 'blocked': true,  'employer': 'na' }
     * ];
     *
     * // using "_.pluck" callback shorthand
     * _.initial(characters, 'blocked');
     * // => [{ 'name': 'barney',  'blocked': false, 'employer': 'slate' }]
     *
     * // using "_.where" callback shorthand
     * _.pluck(_.initial(characters, { 'employer': 'na' }), 'name');
     * // => ['barney', 'fred']
     */
    function initial(array, callback, thisArg) {
      var n = 0,
          length = array ? array.length : 0;

      if (typeof callback != 'number' && callback != null) {
        var index = length;
        callback = lodash.createCallback(callback, thisArg, 3);
        while (index-- && callback(array[index], index, array)) {
          n++;
        }
      } else {
        n = (callback == null || thisArg) ? 1 : callback || n;
      }
      return slice(array, 0, nativeMin(nativeMax(0, length - n), length));
    }

    /**
     * Creates an array of unique values present in all provided arrays using
     * strict equality for comparisons, i.e. `===`.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {...Array} [array] The arrays to inspect.
     * @returns {Array} Returns an array of shared values.
     * @example
     *
     * _.intersection([1, 2, 3], [5, 2, 1, 4], [2, 1]);
     * // => [1, 2]
     */
    function intersection() {
      var args = [],
          argsIndex = -1,
          argsLength = arguments.length,
          caches = getArray(),
          indexOf = getIndexOf(),
          trustIndexOf = indexOf === baseIndexOf,
          seen = getArray();

      while (++argsIndex < argsLength) {
        var value = arguments[argsIndex];
        if (isArray(value) || isArguments(value)) {
          args.push(value);
          caches.push(trustIndexOf && value.length >= largeArraySize &&
            createCache(argsIndex ? args[argsIndex] : seen));
        }
      }
      var array = args[0],
          index = -1,
          length = array ? array.length : 0,
          result = [];

      outer:
      while (++index < length) {
        var cache = caches[0];
        value = array[index];

        if ((cache ? cacheIndexOf(cache, value) : indexOf(seen, value)) < 0) {
          argsIndex = argsLength;
          (cache || seen).push(value);
          while (--argsIndex) {
            cache = caches[argsIndex];
            if ((cache ? cacheIndexOf(cache, value) : indexOf(args[argsIndex], value)) < 0) {
              continue outer;
            }
          }
          result.push(value);
        }
      }
      while (argsLength--) {
        cache = caches[argsLength];
        if (cache) {
          releaseObject(cache);
        }
      }
      releaseArray(caches);
      releaseArray(seen);
      return result;
    }

    /**
     * Gets the last element or last `n` elements of an array. If a callback is
     * provided elements at the end of the array are returned as long as the
     * callback returns truey. The callback is bound to `thisArg` and invoked
     * with three arguments; (value, index, array).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {Array} array The array to query.
     * @param {Function|Object|number|string} [callback] The function called
     *  per element or the number of elements to return. If a property name or
     *  object is provided it will be used to create a "_.pluck" or "_.where"
     *  style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {*} Returns the last element(s) of `array`.
     * @example
     *
     * _.last([1, 2, 3]);
     * // => 3
     *
     * _.last([1, 2, 3], 2);
     * // => [2, 3]
     *
     * _.last([1, 2, 3], function(num) {
     *   return num > 1;
     * });
     * // => [2, 3]
     *
     * var characters = [
     *   { 'name': 'barney',  'blocked': false, 'employer': 'slate' },
     *   { 'name': 'fred',    'blocked': true,  'employer': 'slate' },
     *   { 'name': 'pebbles', 'blocked': true,  'employer': 'na' }
     * ];
     *
     * // using "_.pluck" callback shorthand
     * _.pluck(_.last(characters, 'blocked'), 'name');
     * // => ['fred', 'pebbles']
     *
     * // using "_.where" callback shorthand
     * _.last(characters, { 'employer': 'na' });
     * // => [{ 'name': 'pebbles', 'blocked': true, 'employer': 'na' }]
     */
    function last(array, callback, thisArg) {
      var n = 0,
          length = array ? array.length : 0;

      if (typeof callback != 'number' && callback != null) {
        var index = length;
        callback = lodash.createCallback(callback, thisArg, 3);
        while (index-- && callback(array[index], index, array)) {
          n++;
        }
      } else {
        n = callback;
        if (n == null || thisArg) {
          return array ? array[length - 1] : undefined;
        }
      }
      return slice(array, nativeMax(0, length - n));
    }

    /**
     * Gets the index at which the last occurrence of `value` is found using strict
     * equality for comparisons, i.e. `===`. If `fromIndex` is negative, it is used
     * as the offset from the end of the collection.
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {Array} array The array to search.
     * @param {*} value The value to search for.
     * @param {number} [fromIndex=array.length-1] The index to search from.
     * @returns {number} Returns the index of the matched value or `-1`.
     * @example
     *
     * _.lastIndexOf([1, 2, 3, 1, 2, 3], 2);
     * // => 4
     *
     * _.lastIndexOf([1, 2, 3, 1, 2, 3], 2, 3);
     * // => 1
     */
    function lastIndexOf(array, value, fromIndex) {
      var index = array ? array.length : 0;
      if (typeof fromIndex == 'number') {
        index = (fromIndex < 0 ? nativeMax(0, index + fromIndex) : nativeMin(fromIndex, index - 1)) + 1;
      }
      while (index--) {
        if (array[index] === value) {
          return index;
        }
      }
      return -1;
    }

    /**
     * Removes all provided values from the given array using strict equality for
     * comparisons, i.e. `===`.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {Array} array The array to modify.
     * @param {...*} [value] The values to remove.
     * @returns {Array} Returns `array`.
     * @example
     *
     * var array = [1, 2, 3, 1, 2, 3];
     * _.pull(array, 2, 3);
     * console.log(array);
     * // => [1, 1]
     */
    function pull(array) {
      var args = arguments,
          argsIndex = 0,
          argsLength = args.length,
          length = array ? array.length : 0;

      while (++argsIndex < argsLength) {
        var index = -1,
            value = args[argsIndex];
        while (++index < length) {
          if (array[index] === value) {
            splice.call(array, index--, 1);
            length--;
          }
        }
      }
      return array;
    }

    /**
     * Creates an array of numbers (positive and/or negative) progressing from
     * `start` up to but not including `end`. If `start` is less than `stop` a
     * zero-length range is created unless a negative `step` is specified.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {number} [start=0] The start of the range.
     * @param {number} end The end of the range.
     * @param {number} [step=1] The value to increment or decrement by.
     * @returns {Array} Returns a new range array.
     * @example
     *
     * _.range(4);
     * // => [0, 1, 2, 3]
     *
     * _.range(1, 5);
     * // => [1, 2, 3, 4]
     *
     * _.range(0, 20, 5);
     * // => [0, 5, 10, 15]
     *
     * _.range(0, -4, -1);
     * // => [0, -1, -2, -3]
     *
     * _.range(1, 4, 0);
     * // => [1, 1, 1]
     *
     * _.range(0);
     * // => []
     */
    function range(start, end, step) {
      start = +start || 0;
      step = typeof step == 'number' ? step : (+step || 1);

      if (end == null) {
        end = start;
        start = 0;
      }
      // use `Array(length)` so engines like Chakra and V8 avoid slower modes
      // http://youtu.be/XAqIpGU8ZZk#t=17m25s
      var index = -1,
          length = nativeMax(0, ceil((end - start) / (step || 1))),
          result = Array(length);

      while (++index < length) {
        result[index] = start;
        start += step;
      }
      return result;
    }

    /**
     * Removes all elements from an array that the callback returns truey for
     * and returns an array of removed elements. The callback is bound to `thisArg`
     * and invoked with three arguments; (value, index, array).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {Array} array The array to modify.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Array} Returns a new array of removed elements.
     * @example
     *
     * var array = [1, 2, 3, 4, 5, 6];
     * var evens = _.remove(array, function(num) { return num % 2 == 0; });
     *
     * console.log(array);
     * // => [1, 3, 5]
     *
     * console.log(evens);
     * // => [2, 4, 6]
     */
    function remove(array, callback, thisArg) {
      var index = -1,
          length = array ? array.length : 0,
          result = [];

      callback = lodash.createCallback(callback, thisArg, 3);
      while (++index < length) {
        var value = array[index];
        if (callback(value, index, array)) {
          result.push(value);
          splice.call(array, index--, 1);
          length--;
        }
      }
      return result;
    }

    /**
     * The opposite of `_.initial` this method gets all but the first element or
     * first `n` elements of an array. If a callback function is provided elements
     * at the beginning of the array are excluded from the result as long as the
     * callback returns truey. The callback is bound to `thisArg` and invoked
     * with three arguments; (value, index, array).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @alias drop, tail
     * @category Arrays
     * @param {Array} array The array to query.
     * @param {Function|Object|number|string} [callback=1] The function called
     *  per element or the number of elements to exclude. If a property name or
     *  object is provided it will be used to create a "_.pluck" or "_.where"
     *  style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Array} Returns a slice of `array`.
     * @example
     *
     * _.rest([1, 2, 3]);
     * // => [2, 3]
     *
     * _.rest([1, 2, 3], 2);
     * // => [3]
     *
     * _.rest([1, 2, 3], function(num) {
     *   return num < 3;
     * });
     * // => [3]
     *
     * var characters = [
     *   { 'name': 'barney',  'blocked': true,  'employer': 'slate' },
     *   { 'name': 'fred',    'blocked': false,  'employer': 'slate' },
     *   { 'name': 'pebbles', 'blocked': true, 'employer': 'na' }
     * ];
     *
     * // using "_.pluck" callback shorthand
     * _.pluck(_.rest(characters, 'blocked'), 'name');
     * // => ['fred', 'pebbles']
     *
     * // using "_.where" callback shorthand
     * _.rest(characters, { 'employer': 'slate' });
     * // => [{ 'name': 'pebbles', 'blocked': true, 'employer': 'na' }]
     */
    function rest(array, callback, thisArg) {
      if (typeof callback != 'number' && callback != null) {
        var n = 0,
            index = -1,
            length = array ? array.length : 0;

        callback = lodash.createCallback(callback, thisArg, 3);
        while (++index < length && callback(array[index], index, array)) {
          n++;
        }
      } else {
        n = (callback == null || thisArg) ? 1 : nativeMax(0, callback);
      }
      return slice(array, n);
    }

    /**
     * Uses a binary search to determine the smallest index at which a value
     * should be inserted into a given sorted array in order to maintain the sort
     * order of the array. If a callback is provided it will be executed for
     * `value` and each element of `array` to compute their sort ranking. The
     * callback is bound to `thisArg` and invoked with one argument; (value).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {Array} array The array to inspect.
     * @param {*} value The value to evaluate.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {number} Returns the index at which `value` should be inserted
     *  into `array`.
     * @example
     *
     * _.sortedIndex([20, 30, 50], 40);
     * // => 2
     *
     * // using "_.pluck" callback shorthand
     * _.sortedIndex([{ 'x': 20 }, { 'x': 30 }, { 'x': 50 }], { 'x': 40 }, 'x');
     * // => 2
     *
     * var dict = {
     *   'wordToNumber': { 'twenty': 20, 'thirty': 30, 'fourty': 40, 'fifty': 50 }
     * };
     *
     * _.sortedIndex(['twenty', 'thirty', 'fifty'], 'fourty', function(word) {
     *   return dict.wordToNumber[word];
     * });
     * // => 2
     *
     * _.sortedIndex(['twenty', 'thirty', 'fifty'], 'fourty', function(word) {
     *   return this.wordToNumber[word];
     * }, dict);
     * // => 2
     */
    function sortedIndex(array, value, callback, thisArg) {
      var low = 0,
          high = array ? array.length : low;

      // explicitly reference `identity` for better inlining in Firefox
      callback = callback ? lodash.createCallback(callback, thisArg, 1) : identity;
      value = callback(value);

      while (low < high) {
        var mid = (low + high) >>> 1;
        (callback(array[mid]) < value)
          ? low = mid + 1
          : high = mid;
      }
      return low;
    }

    /**
     * Creates an array of unique values, in order, of the provided arrays using
     * strict equality for comparisons, i.e. `===`.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {...Array} [array] The arrays to inspect.
     * @returns {Array} Returns an array of combined values.
     * @example
     *
     * _.union([1, 2, 3], [5, 2, 1, 4], [2, 1]);
     * // => [1, 2, 3, 5, 4]
     */
    function union() {
      return baseUniq(baseFlatten(arguments, true, true));
    }

    /**
     * Creates a duplicate-value-free version of an array using strict equality
     * for comparisons, i.e. `===`. If the array is sorted, providing
     * `true` for `isSorted` will use a faster algorithm. If a callback is provided
     * each element of `array` is passed through the callback before uniqueness
     * is computed. The callback is bound to `thisArg` and invoked with three
     * arguments; (value, index, array).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @alias unique
     * @category Arrays
     * @param {Array} array The array to process.
     * @param {boolean} [isSorted=false] A flag to indicate that `array` is sorted.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Array} Returns a duplicate-value-free array.
     * @example
     *
     * _.uniq([1, 2, 1, 3, 1]);
     * // => [1, 2, 3]
     *
     * _.uniq([1, 1, 2, 2, 3], true);
     * // => [1, 2, 3]
     *
     * _.uniq(['A', 'b', 'C', 'a', 'B', 'c'], function(letter) { return letter.toLowerCase(); });
     * // => ['A', 'b', 'C']
     *
     * _.uniq([1, 2.5, 3, 1.5, 2, 3.5], function(num) { return this.floor(num); }, Math);
     * // => [1, 2.5, 3]
     *
     * // using "_.pluck" callback shorthand
     * _.uniq([{ 'x': 1 }, { 'x': 2 }, { 'x': 1 }], 'x');
     * // => [{ 'x': 1 }, { 'x': 2 }]
     */
    function uniq(array, isSorted, callback, thisArg) {
      // juggle arguments
      if (typeof isSorted != 'boolean' && isSorted != null) {
        thisArg = callback;
        callback = (typeof isSorted != 'function' && thisArg && thisArg[isSorted] === array) ? null : isSorted;
        isSorted = false;
      }
      if (callback != null) {
        callback = lodash.createCallback(callback, thisArg, 3);
      }
      return baseUniq(array, isSorted, callback);
    }

    /**
     * Creates an array excluding all provided values using strict equality for
     * comparisons, i.e. `===`.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {Array} array The array to filter.
     * @param {...*} [value] The values to exclude.
     * @returns {Array} Returns a new array of filtered values.
     * @example
     *
     * _.without([1, 2, 1, 0, 3, 1, 4], 0, 1);
     * // => [2, 3, 4]
     */
    function without(array) {
      return baseDifference(array, slice(arguments, 1));
    }

    /**
     * Creates an array that is the symmetric difference of the provided arrays.
     * See http://en.wikipedia.org/wiki/Symmetric_difference.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {...Array} [array] The arrays to inspect.
     * @returns {Array} Returns an array of values.
     * @example
     *
     * _.xor([1, 2, 3], [5, 2, 1, 4]);
     * // => [3, 5, 4]
     *
     * _.xor([1, 2, 5], [2, 3, 5], [3, 4, 5]);
     * // => [1, 4, 5]
     */
    function xor() {
      var index = -1,
          length = arguments.length;

      while (++index < length) {
        var array = arguments[index];
        if (isArray(array) || isArguments(array)) {
          var result = result
            ? baseUniq(baseDifference(result, array).concat(baseDifference(array, result)))
            : array;
        }
      }
      return result || [];
    }

    /**
     * Creates an array of grouped elements, the first of which contains the first
     * elements of the given arrays, the second of which contains the second
     * elements of the given arrays, and so on.
     *
     * @static
     * @memberOf _
     * @alias unzip
     * @category Arrays
     * @param {...Array} [array] Arrays to process.
     * @returns {Array} Returns a new array of grouped elements.
     * @example
     *
     * _.zip(['fred', 'barney'], [30, 40], [true, false]);
     * // => [['fred', 30, true], ['barney', 40, false]]
     */
    function zip() {
      var array = arguments.length > 1 ? arguments : arguments[0],
          index = -1,
          length = array ? max(pluck(array, 'length')) : 0,
          result = Array(length < 0 ? 0 : length);

      while (++index < length) {
        result[index] = pluck(array, index);
      }
      return result;
    }

    /**
     * Creates an object composed from arrays of `keys` and `values`. Provide
     * either a single two dimensional array, i.e. `[[key1, value1], [key2, value2]]`
     * or two arrays, one of `keys` and one of corresponding `values`.
     *
     * @static
     * @memberOf _
     * @alias object
     * @category Arrays
     * @param {Array} keys The array of keys.
     * @param {Array} [values=[]] The array of values.
     * @returns {Object} Returns an object composed of the given keys and
     *  corresponding values.
     * @example
     *
     * _.zipObject(['fred', 'barney'], [30, 40]);
     * // => { 'fred': 30, 'barney': 40 }
     */
    function zipObject(keys, values) {
      var index = -1,
          length = keys ? keys.length : 0,
          result = {};

      if (!values && length && !isArray(keys[0])) {
        values = [];
      }
      while (++index < length) {
        var key = keys[index];
        if (values) {
          result[key] = values[index];
        } else if (key) {
          result[key[0]] = key[1];
        }
      }
      return result;
    }

    /*--------------------------------------------------------------------------*/

    /**
     * Creates a function that executes `func`, with  the `this` binding and
     * arguments of the created function, only after being called `n` times.
     *
     * @static
     * @memberOf _
     * @category Functions
     * @param {number} n The number of times the function must be called before
     *  `func` is executed.
     * @param {Function} func The function to restrict.
     * @returns {Function} Returns the new restricted function.
     * @example
     *
     * var saves = ['profile', 'settings'];
     *
     * var done = _.after(saves.length, function() {
     *   console.log('Done saving!');
     * });
     *
     * _.forEach(saves, function(type) {
     *   asyncSave({ 'type': type, 'complete': done });
     * });
     * // => logs 'Done saving!', after all saves have completed
     */
    function after(n, func) {
      if (!isFunction(func)) {
        throw new TypeError;
      }
      return function() {
        if (--n < 1) {
          return func.apply(this, arguments);
        }
      };
    }

    /**
     * Creates a function that, when called, invokes `func` with the `this`
     * binding of `thisArg` and prepends any additional `bind` arguments to those
     * provided to the bound function.
     *
     * @static
     * @memberOf _
     * @category Functions
     * @param {Function} func The function to bind.
     * @param {*} [thisArg] The `this` binding of `func`.
     * @param {...*} [arg] Arguments to be partially applied.
     * @returns {Function} Returns the new bound function.
     * @example
     *
     * var func = function(greeting) {
     *   return greeting + ' ' + this.name;
     * };
     *
     * func = _.bind(func, { 'name': 'fred' }, 'hi');
     * func();
     * // => 'hi fred'
     */
    function bind(func, thisArg) {
      return arguments.length > 2
        ? createWrapper(func, 17, slice(arguments, 2), null, thisArg)
        : createWrapper(func, 1, null, null, thisArg);
    }

    /**
     * Binds methods of an object to the object itself, overwriting the existing
     * method. Method names may be specified as individual arguments or as arrays
     * of method names. If no method names are provided all the function properties
     * of `object` will be bound.
     *
     * @static
     * @memberOf _
     * @category Functions
     * @param {Object} object The object to bind and assign the bound methods to.
     * @param {...string} [methodName] The object method names to
     *  bind, specified as individual method names or arrays of method names.
     * @returns {Object} Returns `object`.
     * @example
     *
     * var view = {
     *   'label': 'docs',
     *   'onClick': function() { console.log('clicked ' + this.label); }
     * };
     *
     * _.bindAll(view);
     * jQuery('#docs').on('click', view.onClick);
     * // => logs 'clicked docs', when the button is clicked
     */
    function bindAll(object) {
      var funcs = arguments.length > 1 ? baseFlatten(arguments, true, false, 1) : functions(object),
          index = -1,
          length = funcs.length;

      while (++index < length) {
        var key = funcs[index];
        object[key] = createWrapper(object[key], 1, null, null, object);
      }
      return object;
    }

    /**
     * Creates a function that, when called, invokes the method at `object[key]`
     * and prepends any additional `bindKey` arguments to those provided to the bound
     * function. This method differs from `_.bind` by allowing bound functions to
     * reference methods that will be redefined or don't yet exist.
     * See http://michaux.ca/articles/lazy-function-definition-pattern.
     *
     * @static
     * @memberOf _
     * @category Functions
     * @param {Object} object The object the method belongs to.
     * @param {string} key The key of the method.
     * @param {...*} [arg] Arguments to be partially applied.
     * @returns {Function} Returns the new bound function.
     * @example
     *
     * var object = {
     *   'name': 'fred',
     *   'greet': function(greeting) {
     *     return greeting + ' ' + this.name;
     *   }
     * };
     *
     * var func = _.bindKey(object, 'greet', 'hi');
     * func();
     * // => 'hi fred'
     *
     * object.greet = function(greeting) {
     *   return greeting + 'ya ' + this.name + '!';
     * };
     *
     * func();
     * // => 'hiya fred!'
     */
    function bindKey(object, key) {
      return arguments.length > 2
        ? createWrapper(key, 19, slice(arguments, 2), null, object)
        : createWrapper(key, 3, null, null, object);
    }

    /**
     * Creates a function that is the composition of the provided functions,
     * where each function consumes the return value of the function that follows.
     * For example, composing the functions `f()`, `g()`, and `h()` produces `f(g(h()))`.
     * Each function is executed with the `this` binding of the composed function.
     *
     * @static
     * @memberOf _
     * @category Functions
     * @param {...Function} [func] Functions to compose.
     * @returns {Function} Returns the new composed function.
     * @example
     *
     * var realNameMap = {
     *   'pebbles': 'penelope'
     * };
     *
     * var format = function(name) {
     *   name = realNameMap[name.toLowerCase()] || name;
     *   return name.charAt(0).toUpperCase() + name.slice(1).toLowerCase();
     * };
     *
     * var greet = function(formatted) {
     *   return 'Hiya ' + formatted + '!';
     * };
     *
     * var welcome = _.compose(greet, format);
     * welcome('pebbles');
     * // => 'Hiya Penelope!'
     */
    function compose() {
      var funcs = arguments,
          length = funcs.length;

      while (length--) {
        if (!isFunction(funcs[length])) {
          throw new TypeError;
        }
      }
      return function() {
        var args = arguments,
            length = funcs.length;

        while (length--) {
          args = [funcs[length].apply(this, args)];
        }
        return args[0];
      };
    }

    /**
     * Creates a function which accepts one or more arguments of `func` that when
     * invoked either executes `func` returning its result, if all `func` arguments
     * have been provided, or returns a function that accepts one or more of the
     * remaining `func` arguments, and so on. The arity of `func` can be specified
     * if `func.length` is not sufficient.
     *
     * @static
     * @memberOf _
     * @category Functions
     * @param {Function} func The function to curry.
     * @param {number} [arity=func.length] The arity of `func`.
     * @returns {Function} Returns the new curried function.
     * @example
     *
     * var curried = _.curry(function(a, b, c) {
     *   console.log(a + b + c);
     * });
     *
     * curried(1)(2)(3);
     * // => 6
     *
     * curried(1, 2)(3);
     * // => 6
     *
     * curried(1, 2, 3);
     * // => 6
     */
    function curry(func, arity) {
      arity = typeof arity == 'number' ? arity : (+arity || func.length);
      return createWrapper(func, 4, null, null, null, arity);
    }

    /**
     * Creates a function that will delay the execution of `func` until after
     * `wait` milliseconds have elapsed since the last time it was invoked.
     * Provide an options object to indicate that `func` should be invoked on
     * the leading and/or trailing edge of the `wait` timeout. Subsequent calls
     * to the debounced function will return the result of the last `func` call.
     *
     * Note: If `leading` and `trailing` options are `true` `func` will be called
     * on the trailing edge of the timeout only if the the debounced function is
     * invoked more than once during the `wait` timeout.
     *
     * @static
     * @memberOf _
     * @category Functions
     * @param {Function} func The function to debounce.
     * @param {number} wait The number of milliseconds to delay.
     * @param {Object} [options] The options object.
     * @param {boolean} [options.leading=false] Specify execution on the leading edge of the timeout.
     * @param {number} [options.maxWait] The maximum time `func` is allowed to be delayed before it's called.
     * @param {boolean} [options.trailing=true] Specify execution on the trailing edge of the timeout.
     * @returns {Function} Returns the new debounced function.
     * @example
     *
     * // avoid costly calculations while the window size is in flux
     * var lazyLayout = _.debounce(calculateLayout, 150);
     * jQuery(window).on('resize', lazyLayout);
     *
     * // execute `sendMail` when the click event is fired, debouncing subsequent calls
     * jQuery('#postbox').on('click', _.debounce(sendMail, 300, {
     *   'leading': true,
     *   'trailing': false
     * });
     *
     * // ensure `batchLog` is executed once after 1 second of debounced calls
     * var source = new EventSource('/stream');
     * source.addEventListener('message', _.debounce(batchLog, 250, {
     *   'maxWait': 1000
     * }, false);
     */
    function debounce(func, wait, options) {
      var args,
          maxTimeoutId,
          result,
          stamp,
          thisArg,
          timeoutId,
          trailingCall,
          lastCalled = 0,
          maxWait = false,
          trailing = true;

      if (!isFunction(func)) {
        throw new TypeError;
      }
      wait = nativeMax(0, wait) || 0;
      if (options === true) {
        var leading = true;
        trailing = false;
      } else if (isObject(options)) {
        leading = options.leading;
        maxWait = 'maxWait' in options && (nativeMax(wait, options.maxWait) || 0);
        trailing = 'trailing' in options ? options.trailing : trailing;
      }
      var delayed = function() {
        var remaining = wait - (now() - stamp);
        if (remaining <= 0) {
          if (maxTimeoutId) {
            clearTimeout(maxTimeoutId);
          }
          var isCalled = trailingCall;
          maxTimeoutId = timeoutId = trailingCall = undefined;
          if (isCalled) {
            lastCalled = now();
            result = func.apply(thisArg, args);
            if (!timeoutId && !maxTimeoutId) {
              args = thisArg = null;
            }
          }
        } else {
          timeoutId = setTimeout(delayed, remaining);
        }
      };

      var maxDelayed = function() {
        if (timeoutId) {
          clearTimeout(timeoutId);
        }
        maxTimeoutId = timeoutId = trailingCall = undefined;
        if (trailing || (maxWait !== wait)) {
          lastCalled = now();
          result = func.apply(thisArg, args);
          if (!timeoutId && !maxTimeoutId) {
            args = thisArg = null;
          }
        }
      };

      return function() {
        args = arguments;
        stamp = now();
        thisArg = this;
        trailingCall = trailing && (timeoutId || !leading);

        if (maxWait === false) {
          var leadingCall = leading && !timeoutId;
        } else {
          if (!maxTimeoutId && !leading) {
            lastCalled = stamp;
          }
          var remaining = maxWait - (stamp - lastCalled),
              isCalled = remaining <= 0;

          if (isCalled) {
            if (maxTimeoutId) {
              maxTimeoutId = clearTimeout(maxTimeoutId);
            }
            lastCalled = stamp;
            result = func.apply(thisArg, args);
          }
          else if (!maxTimeoutId) {
            maxTimeoutId = setTimeout(maxDelayed, remaining);
          }
        }
        if (isCalled && timeoutId) {
          timeoutId = clearTimeout(timeoutId);
        }
        else if (!timeoutId && wait !== maxWait) {
          timeoutId = setTimeout(delayed, wait);
        }
        if (leadingCall) {
          isCalled = true;
          result = func.apply(thisArg, args);
        }
        if (isCalled && !timeoutId && !maxTimeoutId) {
          args = thisArg = null;
        }
        return result;
      };
    }

    /**
     * Defers executing the `func` function until the current call stack has cleared.
     * Additional arguments will be provided to `func` when it is invoked.
     *
     * @static
     * @memberOf _
     * @category Functions
     * @param {Function} func The function to defer.
     * @param {...*} [arg] Arguments to invoke the function with.
     * @returns {number} Returns the timer id.
     * @example
     *
     * _.defer(function(text) { console.log(text); }, 'deferred');
     * // logs 'deferred' after one or more milliseconds
     */
    function defer(func) {
      if (!isFunction(func)) {
        throw new TypeError;
      }
      var args = slice(arguments, 1);
      return setTimeout(function() { func.apply(undefined, args); }, 1);
    }

    /**
     * Executes the `func` function after `wait` milliseconds. Additional arguments
     * will be provided to `func` when it is invoked.
     *
     * @static
     * @memberOf _
     * @category Functions
     * @param {Function} func The function to delay.
     * @param {number} wait The number of milliseconds to delay execution.
     * @param {...*} [arg] Arguments to invoke the function with.
     * @returns {number} Returns the timer id.
     * @example
     *
     * _.delay(function(text) { console.log(text); }, 1000, 'later');
     * // => logs 'later' after one second
     */
    function delay(func, wait) {
      if (!isFunction(func)) {
        throw new TypeError;
      }
      var args = slice(arguments, 2);
      return setTimeout(function() { func.apply(undefined, args); }, wait);
    }

    /**
     * Creates a function that memoizes the result of `func`. If `resolver` is
     * provided it will be used to determine the cache key for storing the result
     * based on the arguments provided to the memoized function. By default, the
     * first argument provided to the memoized function is used as the cache key.
     * The `func` is executed with the `this` binding of the memoized function.
     * The result cache is exposed as the `cache` property on the memoized function.
     *
     * @static
     * @memberOf _
     * @category Functions
     * @param {Function} func The function to have its output memoized.
     * @param {Function} [resolver] A function used to resolve the cache key.
     * @returns {Function} Returns the new memoizing function.
     * @example
     *
     * var fibonacci = _.memoize(function(n) {
     *   return n < 2 ? n : fibonacci(n - 1) + fibonacci(n - 2);
     * });
     *
     * fibonacci(9)
     * // => 34
     *
     * var data = {
     *   'fred': { 'name': 'fred', 'age': 40 },
     *   'pebbles': { 'name': 'pebbles', 'age': 1 }
     * };
     *
     * // modifying the result cache
     * var get = _.memoize(function(name) { return data[name]; }, _.identity);
     * get('pebbles');
     * // => { 'name': 'pebbles', 'age': 1 }
     *
     * get.cache.pebbles.name = 'penelope';
     * get('pebbles');
     * // => { 'name': 'penelope', 'age': 1 }
     */
    function memoize(func, resolver) {
      if (!isFunction(func)) {
        throw new TypeError;
      }
      var memoized = function() {
        var cache = memoized.cache,
            key = resolver ? resolver.apply(this, arguments) : keyPrefix + arguments[0];

        return hasOwnProperty.call(cache, key)
          ? cache[key]
          : (cache[key] = func.apply(this, arguments));
      }
      memoized.cache = {};
      return memoized;
    }

    /**
     * Creates a function that is restricted to execute `func` once. Repeat calls to
     * the function will return the value of the first call. The `func` is executed
     * with the `this` binding of the created function.
     *
     * @static
     * @memberOf _
     * @category Functions
     * @param {Function} func The function to restrict.
     * @returns {Function} Returns the new restricted function.
     * @example
     *
     * var initialize = _.once(createApplication);
     * initialize();
     * initialize();
     * // `initialize` executes `createApplication` once
     */
    function once(func) {
      var ran,
          result;

      if (!isFunction(func)) {
        throw new TypeError;
      }
      return function() {
        if (ran) {
          return result;
        }
        ran = true;
        result = func.apply(this, arguments);

        // clear the `func` variable so the function may be garbage collected
        func = null;
        return result;
      };
    }

    /**
     * Creates a function that, when called, invokes `func` with any additional
     * `partial` arguments prepended to those provided to the new function. This
     * method is similar to `_.bind` except it does **not** alter the `this` binding.
     *
     * @static
     * @memberOf _
     * @category Functions
     * @param {Function} func The function to partially apply arguments to.
     * @param {...*} [arg] Arguments to be partially applied.
     * @returns {Function} Returns the new partially applied function.
     * @example
     *
     * var greet = function(greeting, name) { return greeting + ' ' + name; };
     * var hi = _.partial(greet, 'hi');
     * hi('fred');
     * // => 'hi fred'
     */
    function partial(func) {
      return createWrapper(func, 16, slice(arguments, 1));
    }

    /**
     * This method is like `_.partial` except that `partial` arguments are
     * appended to those provided to the new function.
     *
     * @static
     * @memberOf _
     * @category Functions
     * @param {Function} func The function to partially apply arguments to.
     * @param {...*} [arg] Arguments to be partially applied.
     * @returns {Function} Returns the new partially applied function.
     * @example
     *
     * var defaultsDeep = _.partialRight(_.merge, _.defaults);
     *
     * var options = {
     *   'variable': 'data',
     *   'imports': { 'jq': $ }
     * };
     *
     * defaultsDeep(options, _.templateSettings);
     *
     * options.variable
     * // => 'data'
     *
     * options.imports
     * // => { '_': _, 'jq': $ }
     */
    function partialRight(func) {
      return createWrapper(func, 32, null, slice(arguments, 1));
    }

    /**
     * Creates a function that, when executed, will only call the `func` function
     * at most once per every `wait` milliseconds. Provide an options object to
     * indicate that `func` should be invoked on the leading and/or trailing edge
     * of the `wait` timeout. Subsequent calls to the throttled function will
     * return the result of the last `func` call.
     *
     * Note: If `leading` and `trailing` options are `true` `func` will be called
     * on the trailing edge of the timeout only if the the throttled function is
     * invoked more than once during the `wait` timeout.
     *
     * @static
     * @memberOf _
     * @category Functions
     * @param {Function} func The function to throttle.
     * @param {number} wait The number of milliseconds to throttle executions to.
     * @param {Object} [options] The options object.
     * @param {boolean} [options.leading=true] Specify execution on the leading edge of the timeout.
     * @param {boolean} [options.trailing=true] Specify execution on the trailing edge of the timeout.
     * @returns {Function} Returns the new throttled function.
     * @example
     *
     * // avoid excessively updating the position while scrolling
     * var throttled = _.throttle(updatePosition, 100);
     * jQuery(window).on('scroll', throttled);
     *
     * // execute `renewToken` when the click event is fired, but not more than once every 5 minutes
     * jQuery('.interactive').on('click', _.throttle(renewToken, 300000, {
     *   'trailing': false
     * }));
     */
    function throttle(func, wait, options) {
      var leading = true,
          trailing = true;

      if (!isFunction(func)) {
        throw new TypeError;
      }
      if (options === false) {
        leading = false;
      } else if (isObject(options)) {
        leading = 'leading' in options ? options.leading : leading;
        trailing = 'trailing' in options ? options.trailing : trailing;
      }
      debounceOptions.leading = leading;
      debounceOptions.maxWait = wait;
      debounceOptions.trailing = trailing;

      return debounce(func, wait, debounceOptions);
    }

    /**
     * Creates a function that provides `value` to the wrapper function as its
     * first argument. Additional arguments provided to the function are appended
     * to those provided to the wrapper function. The wrapper is executed with
     * the `this` binding of the created function.
     *
     * @static
     * @memberOf _
     * @category Functions
     * @param {*} value The value to wrap.
     * @param {Function} wrapper The wrapper function.
     * @returns {Function} Returns the new function.
     * @example
     *
     * var p = _.wrap(_.escape, function(func, text) {
     *   return '<p>' + func(text) + '</p>';
     * });
     *
     * p('Fred, Wilma, & Pebbles');
     * // => '<p>Fred, Wilma, &amp; Pebbles</p>'
     */
    function wrap(value, wrapper) {
      return createWrapper(wrapper, 16, [value]);
    }

    /*--------------------------------------------------------------------------*/

    /**
     * Creates a function that returns `value`.
     *
     * @static
     * @memberOf _
     * @category Utilities
     * @param {*} value The value to return from the new function.
     * @returns {Function} Returns the new function.
     * @example
     *
     * var object = { 'name': 'fred' };
     * var getter = _.constant(object);
     * getter() === object;
     * // => true
     */
    function constant(value) {
      return function() {
        return value;
      };
    }

    /**
     * Produces a callback bound to an optional `thisArg`. If `func` is a property
     * name the created callback will return the property value for a given element.
     * If `func` is an object the created callback will return `true` for elements
     * that contain the equivalent object properties, otherwise it will return `false`.
     *
     * @static
     * @memberOf _
     * @category Utilities
     * @param {*} [func=identity] The value to convert to a callback.
     * @param {*} [thisArg] The `this` binding of the created callback.
     * @param {number} [argCount] The number of arguments the callback accepts.
     * @returns {Function} Returns a callback function.
     * @example
     *
     * var characters = [
     *   { 'name': 'barney', 'age': 36 },
     *   { 'name': 'fred',   'age': 40 }
     * ];
     *
     * // wrap to create custom callback shorthands
     * _.createCallback = _.wrap(_.createCallback, function(func, callback, thisArg) {
     *   var match = /^(.+?)__([gl]t)(.+)$/.exec(callback);
     *   return !match ? func(callback, thisArg) : function(object) {
     *     return match[2] == 'gt' ? object[match[1]] > match[3] : object[match[1]] < match[3];
     *   };
     * });
     *
     * _.filter(characters, 'age__gt38');
     * // => [{ 'name': 'fred', 'age': 40 }]
     */
    function createCallback(func, thisArg, argCount) {
      var type = typeof func;
      if (func == null || type == 'function') {
        return baseCreateCallback(func, thisArg, argCount);
      }
      // handle "_.pluck" style callback shorthands
      if (type != 'object') {
        return property(func);
      }
      var props = keys(func),
          key = props[0],
          a = func[key];

      // handle "_.where" style callback shorthands
      if (props.length == 1 && a === a && !isObject(a)) {
        // fast path the common case of providing an object with a single
        // property containing a primitive value
        return function(object) {
          var b = object[key];
          return a === b && (a !== 0 || (1 / a == 1 / b));
        };
      }
      return function(object) {
        var length = props.length,
            result = false;

        while (length--) {
          if (!(result = baseIsEqual(object[props[length]], func[props[length]], null, true))) {
            break;
          }
        }
        return result;
      };
    }

    /**
     * Converts the characters `&`, `<`, `>`, `"`, and `'` in `string` to their
     * corresponding HTML entities.
     *
     * @static
     * @memberOf _
     * @category Utilities
     * @param {string} string The string to escape.
     * @returns {string} Returns the escaped string.
     * @example
     *
     * _.escape('Fred, Wilma, & Pebbles');
     * // => 'Fred, Wilma, &amp; Pebbles'
     */
    function escape(string) {
      return string == null ? '' : String(string).replace(reUnescapedHtml, escapeHtmlChar);
    }

    /**
     * This method returns the first argument provided to it.
     *
     * @static
     * @memberOf _
     * @category Utilities
     * @param {*} value Any value.
     * @returns {*} Returns `value`.
     * @example
     *
     * var object = { 'name': 'fred' };
     * _.identity(object) === object;
     * // => true
     */
    function identity(value) {
      return value;
    }

    /**
     * Adds function properties of a source object to the destination object.
     * If `object` is a function methods will be added to its prototype as well.
     *
     * @static
     * @memberOf _
     * @category Utilities
     * @param {Function|Object} [object=lodash] object The destination object.
     * @param {Object} source The object of functions to add.
     * @param {Object} [options] The options object.
     * @param {boolean} [options.chain=true] Specify whether the functions added are chainable.
     * @example
     *
     * function capitalize(string) {
     *   return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
     * }
     *
     * _.mixin({ 'capitalize': capitalize });
     * _.capitalize('fred');
     * // => 'Fred'
     *
     * _('fred').capitalize().value();
     * // => 'Fred'
     *
     * _.mixin({ 'capitalize': capitalize }, { 'chain': false });
     * _('fred').capitalize();
     * // => 'Fred'
     */
    function mixin(object, source, options) {
      var chain = true,
          methodNames = source && functions(source);

      if (!source || (!options && !methodNames.length)) {
        if (options == null) {
          options = source;
        }
        ctor = lodashWrapper;
        source = object;
        object = lodash;
        methodNames = functions(source);
      }
      if (options === false) {
        chain = false;
      } else if (isObject(options) && 'chain' in options) {
        chain = options.chain;
      }
      var ctor = object,
          isFunc = isFunction(ctor);

      forEach(methodNames, function(methodName) {
        var func = object[methodName] = source[methodName];
        if (isFunc) {
          ctor.prototype[methodName] = function() {
            var chainAll = this.__chain__,
                value = this.__wrapped__,
                args = [value];

            push.apply(args, arguments);
            var result = func.apply(object, args);
            if (chain || chainAll) {
              if (value === result && isObject(result)) {
                return this;
              }
              result = new ctor(result);
              result.__chain__ = chainAll;
            }
            return result;
          };
        }
      });
    }

    /**
     * Reverts the '_' variable to its previous value and returns a reference to
     * the `lodash` function.
     *
     * @static
     * @memberOf _
     * @category Utilities
     * @returns {Function} Returns the `lodash` function.
     * @example
     *
     * var lodash = _.noConflict();
     */
    function noConflict() {
      context._ = oldDash;
      return this;
    }

    /**
     * A no-operation function.
     *
     * @static
     * @memberOf _
     * @category Utilities
     * @example
     *
     * var object = { 'name': 'fred' };
     * _.noop(object) === undefined;
     * // => true
     */
    function noop() {
      // no operation performed
    }

    /**
     * Gets the number of milliseconds that have elapsed since the Unix epoch
     * (1 January 1970 00:00:00 UTC).
     *
     * @static
     * @memberOf _
     * @category Utilities
     * @example
     *
     * var stamp = _.now();
     * _.defer(function() { console.log(_.now() - stamp); });
     * // => logs the number of milliseconds it took for the deferred function to be called
     */
    var now = isNative(now = Date.now) && now || function() {
      return new Date().getTime();
    };

    /**
     * Converts the given value into an integer of the specified radix.
     * If `radix` is `undefined` or `0` a `radix` of `10` is used unless the
     * `value` is a hexadecimal, in which case a `radix` of `16` is used.
     *
     * Note: This method avoids differences in native ES3 and ES5 `parseInt`
     * implementations. See http://es5.github.io/#E.
     *
     * @static
     * @memberOf _
     * @category Utilities
     * @param {string} value The value to parse.
     * @param {number} [radix] The radix used to interpret the value to parse.
     * @returns {number} Returns the new integer value.
     * @example
     *
     * _.parseInt('08');
     * // => 8
     */
    var parseInt = nativeParseInt(whitespace + '08') == 8 ? nativeParseInt : function(value, radix) {
      // Firefox < 21 and Opera < 15 follow the ES3 specified implementation of `parseInt`
      return nativeParseInt(isString(value) ? value.replace(reLeadingSpacesAndZeros, '') : value, radix || 0);
    };

    /**
     * Creates a "_.pluck" style function, which returns the `key` value of a
     * given object.
     *
     * @static
     * @memberOf _
     * @category Utilities
     * @param {string} key The name of the property to retrieve.
     * @returns {Function} Returns the new function.
     * @example
     *
     * var characters = [
     *   { 'name': 'fred',   'age': 40 },
     *   { 'name': 'barney', 'age': 36 }
     * ];
     *
     * var getName = _.property('name');
     *
     * _.map(characters, getName);
     * // => ['barney', 'fred']
     *
     * _.sortBy(characters, getName);
     * // => [{ 'name': 'barney', 'age': 36 }, { 'name': 'fred',   'age': 40 }]
     */
    function property(key) {
      return function(object) {
        return object[key];
      };
    }

    /**
     * Produces a random number between `min` and `max` (inclusive). If only one
     * argument is provided a number between `0` and the given number will be
     * returned. If `floating` is truey or either `min` or `max` are floats a
     * floating-point number will be returned instead of an integer.
     *
     * @static
     * @memberOf _
     * @category Utilities
     * @param {number} [min=0] The minimum possible value.
     * @param {number} [max=1] The maximum possible value.
     * @param {boolean} [floating=false] Specify returning a floating-point number.
     * @returns {number} Returns a random number.
     * @example
     *
     * _.random(0, 5);
     * // => an integer between 0 and 5
     *
     * _.random(5);
     * // => also an integer between 0 and 5
     *
     * _.random(5, true);
     * // => a floating-point number between 0 and 5
     *
     * _.random(1.2, 5.2);
     * // => a floating-point number between 1.2 and 5.2
     */
    function random(min, max, floating) {
      var noMin = min == null,
          noMax = max == null;

      if (floating == null) {
        if (typeof min == 'boolean' && noMax) {
          floating = min;
          min = 1;
        }
        else if (!noMax && typeof max == 'boolean') {
          floating = max;
          noMax = true;
        }
      }
      if (noMin && noMax) {
        max = 1;
      }
      min = +min || 0;
      if (noMax) {
        max = min;
        min = 0;
      } else {
        max = +max || 0;
      }
      if (floating || min % 1 || max % 1) {
        var rand = nativeRandom();
        return nativeMin(min + (rand * (max - min + parseFloat('1e-' + ((rand +'').length - 1)))), max);
      }
      return baseRandom(min, max);
    }

    /**
     * Resolves the value of property `key` on `object`. If `key` is a function
     * it will be invoked with the `this` binding of `object` and its result returned,
     * else the property value is returned. If `object` is falsey then `undefined`
     * is returned.
     *
     * @static
     * @memberOf _
     * @category Utilities
     * @param {Object} object The object to inspect.
     * @param {string} key The name of the property to resolve.
     * @returns {*} Returns the resolved value.
     * @example
     *
     * var object = {
     *   'cheese': 'crumpets',
     *   'stuff': function() {
     *     return 'nonsense';
     *   }
     * };
     *
     * _.result(object, 'cheese');
     * // => 'crumpets'
     *
     * _.result(object, 'stuff');
     * // => 'nonsense'
     */
    function result(object, key) {
      if (object) {
        var value = object[key];
        return isFunction(value) ? object[key]() : value;
      }
    }

    /**
     * A micro-templating method that handles arbitrary delimiters, preserves
     * whitespace, and correctly escapes quotes within interpolated code.
     *
     * Note: In the development build, `_.template` utilizes sourceURLs for easier
     * debugging. See http://www.html5rocks.com/en/tutorials/developertools/sourcemaps/#toc-sourceurl
     *
     * For more information on precompiling templates see:
     * http://lodash.com/custom-builds
     *
     * For more information on Chrome extension sandboxes see:
     * http://developer.chrome.com/stable/extensions/sandboxingEval.html
     *
     * @static
     * @memberOf _
     * @category Utilities
     * @param {string} text The template text.
     * @param {Object} data The data object used to populate the text.
     * @param {Object} [options] The options object.
     * @param {RegExp} [options.escape] The "escape" delimiter.
     * @param {RegExp} [options.evaluate] The "evaluate" delimiter.
     * @param {Object} [options.imports] An object to import into the template as local variables.
     * @param {RegExp} [options.interpolate] The "interpolate" delimiter.
     * @param {string} [sourceURL] The sourceURL of the template's compiled source.
     * @param {string} [variable] The data object variable name.
     * @returns {Function|string} Returns a compiled function when no `data` object
     *  is given, else it returns the interpolated text.
     * @example
     *
     * // using the "interpolate" delimiter to create a compiled template
     * var compiled = _.template('hello <%= name %>');
     * compiled({ 'name': 'fred' });
     * // => 'hello fred'
     *
     * // using the "escape" delimiter to escape HTML in data property values
     * _.template('<b><%- value %></b>', { 'value': '<script>' });
     * // => '<b>&lt;script&gt;</b>'
     *
     * // using the "evaluate" delimiter to generate HTML
     * var list = '<% _.forEach(people, function(name) { %><li><%- name %></li><% }); %>';
     * _.template(list, { 'people': ['fred', 'barney'] });
     * // => '<li>fred</li><li>barney</li>'
     *
     * // using the ES6 delimiter as an alternative to the default "interpolate" delimiter
     * _.template('hello ${ name }', { 'name': 'pebbles' });
     * // => 'hello pebbles'
     *
     * // using the internal `print` function in "evaluate" delimiters
     * _.template('<% print("hello " + name); %>!', { 'name': 'barney' });
     * // => 'hello barney!'
     *
     * // using a custom template delimiters
     * _.templateSettings = {
     *   'interpolate': /{{([\s\S]+?)}}/g
     * };
     *
     * _.template('hello {{ name }}!', { 'name': 'mustache' });
     * // => 'hello mustache!'
     *
     * // using the `imports` option to import jQuery
     * var list = '<% jq.each(people, function(name) { %><li><%- name %></li><% }); %>';
     * _.template(list, { 'people': ['fred', 'barney'] }, { 'imports': { 'jq': jQuery } });
     * // => '<li>fred</li><li>barney</li>'
     *
     * // using the `sourceURL` option to specify a custom sourceURL for the template
     * var compiled = _.template('hello <%= name %>', null, { 'sourceURL': '/basic/greeting.jst' });
     * compiled(data);
     * // => find the source of "greeting.jst" under the Sources tab or Resources panel of the web inspector
     *
     * // using the `variable` option to ensure a with-statement isn't used in the compiled template
     * var compiled = _.template('hi <%= data.name %>!', null, { 'variable': 'data' });
     * compiled.source;
     * // => function(data) {
     *   var __t, __p = '', __e = _.escape;
     *   __p += 'hi ' + ((__t = ( data.name )) == null ? '' : __t) + '!';
     *   return __p;
     * }
     *
     * // using the `source` property to inline compiled templates for meaningful
     * // line numbers in error messages and a stack trace
     * fs.writeFileSync(path.join(cwd, 'jst.js'), '\
     *   var JST = {\
     *     "main": ' + _.template(mainText).source + '\
     *   };\
     * ');
     */
    function template(text, data, options) {
      // based on John Resig's `tmpl` implementation
      // http://ejohn.org/blog/javascript-micro-templating/
      // and Laura Doktorova's doT.js
      // https://github.com/olado/doT
      var settings = lodash.templateSettings;
      text = String(text || '');

      // avoid missing dependencies when `iteratorTemplate` is not defined
      options = defaults({}, options, settings);

      var imports = defaults({}, options.imports, settings.imports),
          importsKeys = keys(imports),
          importsValues = values(imports);

      var isEvaluating,
          index = 0,
          interpolate = options.interpolate || reNoMatch,
          source = "__p += '";

      // compile the regexp to match each delimiter
      var reDelimiters = RegExp(
        (options.escape || reNoMatch).source + '|' +
        interpolate.source + '|' +
        (interpolate === reInterpolate ? reEsTemplate : reNoMatch).source + '|' +
        (options.evaluate || reNoMatch).source + '|$'
      , 'g');

      text.replace(reDelimiters, function(match, escapeValue, interpolateValue, esTemplateValue, evaluateValue, offset) {
        interpolateValue || (interpolateValue = esTemplateValue);

        // escape characters that cannot be included in string literals
        source += text.slice(index, offset).replace(reUnescapedString, escapeStringChar);

        // replace delimiters with snippets
        if (escapeValue) {
          source += "' +\n__e(" + escapeValue + ") +\n'";
        }
        if (evaluateValue) {
          isEvaluating = true;
          source += "';\n" + evaluateValue + ";\n__p += '";
        }
        if (interpolateValue) {
          source += "' +\n((__t = (" + interpolateValue + ")) == null ? '' : __t) +\n'";
        }
        index = offset + match.length;

        // the JS engine embedded in Adobe products requires returning the `match`
        // string in order to produce the correct `offset` value
        return match;
      });

      source += "';\n";

      // if `variable` is not specified, wrap a with-statement around the generated
      // code to add the data object to the top of the scope chain
      var variable = options.variable,
          hasVariable = variable;

      if (!hasVariable) {
        variable = 'obj';
        source = 'with (' + variable + ') {\n' + source + '\n}\n';
      }
      // cleanup code by stripping empty strings
      source = (isEvaluating ? source.replace(reEmptyStringLeading, '') : source)
        .replace(reEmptyStringMiddle, '$1')
        .replace(reEmptyStringTrailing, '$1;');

      // frame code as the function body
      source = 'function(' + variable + ') {\n' +
        (hasVariable ? '' : variable + ' || (' + variable + ' = {});\n') +
        "var __t, __p = '', __e = _.escape" +
        (isEvaluating
          ? ', __j = Array.prototype.join;\n' +
            "function print() { __p += __j.call(arguments, '') }\n"
          : ';\n'
        ) +
        source +
        'return __p\n}';

      // Use a sourceURL for easier debugging.
      // http://www.html5rocks.com/en/tutorials/developertools/sourcemaps/#toc-sourceurl
      var sourceURL = '\n/*\n//# sourceURL=' + (options.sourceURL || '/lodash/template/source[' + (templateCounter++) + ']') + '\n*/';

      try {
        var result = Function(importsKeys, 'return ' + source + sourceURL).apply(undefined, importsValues);
      } catch(e) {
        e.source = source;
        throw e;
      }
      if (data) {
        return result(data);
      }
      // provide the compiled function's source by its `toString` method, in
      // supported environments, or the `source` property as a convenience for
      // inlining compiled templates during the build process
      result.source = source;
      return result;
    }

    /**
     * Executes the callback `n` times, returning an array of the results
     * of each callback execution. The callback is bound to `thisArg` and invoked
     * with one argument; (index).
     *
     * @static
     * @memberOf _
     * @category Utilities
     * @param {number} n The number of times to execute the callback.
     * @param {Function} callback The function called per iteration.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Array} Returns an array of the results of each `callback` execution.
     * @example
     *
     * var diceRolls = _.times(3, _.partial(_.random, 1, 6));
     * // => [3, 6, 4]
     *
     * _.times(3, function(n) { mage.castSpell(n); });
     * // => calls `mage.castSpell(n)` three times, passing `n` of `0`, `1`, and `2` respectively
     *
     * _.times(3, function(n) { this.cast(n); }, mage);
     * // => also calls `mage.castSpell(n)` three times
     */
    function times(n, callback, thisArg) {
      n = (n = +n) > -1 ? n : 0;
      var index = -1,
          result = Array(n);

      callback = baseCreateCallback(callback, thisArg, 1);
      while (++index < n) {
        result[index] = callback(index);
      }
      return result;
    }

    /**
     * The inverse of `_.escape` this method converts the HTML entities
     * `&amp;`, `&lt;`, `&gt;`, `&quot;`, and `&#39;` in `string` to their
     * corresponding characters.
     *
     * @static
     * @memberOf _
     * @category Utilities
     * @param {string} string The string to unescape.
     * @returns {string} Returns the unescaped string.
     * @example
     *
     * _.unescape('Fred, Barney &amp; Pebbles');
     * // => 'Fred, Barney & Pebbles'
     */
    function unescape(string) {
      return string == null ? '' : String(string).replace(reEscapedHtml, unescapeHtmlChar);
    }

    /**
     * Generates a unique ID. If `prefix` is provided the ID will be appended to it.
     *
     * @static
     * @memberOf _
     * @category Utilities
     * @param {string} [prefix] The value to prefix the ID with.
     * @returns {string} Returns the unique ID.
     * @example
     *
     * _.uniqueId('contact_');
     * // => 'contact_104'
     *
     * _.uniqueId();
     * // => '105'
     */
    function uniqueId(prefix) {
      var id = ++idCounter;
      return String(prefix == null ? '' : prefix) + id;
    }

    /*--------------------------------------------------------------------------*/

    /**
     * Creates a `lodash` object that wraps the given value with explicit
     * method chaining enabled.
     *
     * @static
     * @memberOf _
     * @category Chaining
     * @param {*} value The value to wrap.
     * @returns {Object} Returns the wrapper object.
     * @example
     *
     * var characters = [
     *   { 'name': 'barney',  'age': 36 },
     *   { 'name': 'fred',    'age': 40 },
     *   { 'name': 'pebbles', 'age': 1 }
     * ];
     *
     * var youngest = _.chain(characters)
     *     .sortBy('age')
     *     .map(function(chr) { return chr.name + ' is ' + chr.age; })
     *     .first()
     *     .value();
     * // => 'pebbles is 1'
     */
    function chain(value) {
      value = new lodashWrapper(value);
      value.__chain__ = true;
      return value;
    }

    /**
     * Invokes `interceptor` with the `value` as the first argument and then
     * returns `value`. The purpose of this method is to "tap into" a method
     * chain in order to perform operations on intermediate results within
     * the chain.
     *
     * @static
     * @memberOf _
     * @category Chaining
     * @param {*} value The value to provide to `interceptor`.
     * @param {Function} interceptor The function to invoke.
     * @returns {*} Returns `value`.
     * @example
     *
     * _([1, 2, 3, 4])
     *  .tap(function(array) { array.pop(); })
     *  .reverse()
     *  .value();
     * // => [3, 2, 1]
     */
    function tap(value, interceptor) {
      interceptor(value);
      return value;
    }

    /**
     * Enables explicit method chaining on the wrapper object.
     *
     * @name chain
     * @memberOf _
     * @category Chaining
     * @returns {*} Returns the wrapper object.
     * @example
     *
     * var characters = [
     *   { 'name': 'barney', 'age': 36 },
     *   { 'name': 'fred',   'age': 40 }
     * ];
     *
     * // without explicit chaining
     * _(characters).first();
     * // => { 'name': 'barney', 'age': 36 }
     *
     * // with explicit chaining
     * _(characters).chain()
     *   .first()
     *   .pick('age')
     *   .value();
     * // => { 'age': 36 }
     */
    function wrapperChain() {
      this.__chain__ = true;
      return this;
    }

    /**
     * Produces the `toString` result of the wrapped value.
     *
     * @name toString
     * @memberOf _
     * @category Chaining
     * @returns {string} Returns the string result.
     * @example
     *
     * _([1, 2, 3]).toString();
     * // => '1,2,3'
     */
    function wrapperToString() {
      return String(this.__wrapped__);
    }

    /**
     * Extracts the wrapped value.
     *
     * @name valueOf
     * @memberOf _
     * @alias value
     * @category Chaining
     * @returns {*} Returns the wrapped value.
     * @example
     *
     * _([1, 2, 3]).valueOf();
     * // => [1, 2, 3]
     */
    function wrapperValueOf() {
      return this.__wrapped__;
    }

    /*--------------------------------------------------------------------------*/

    // add functions that return wrapped values when chaining
    lodash.after = after;
    lodash.assign = assign;
    lodash.at = at;
    lodash.bind = bind;
    lodash.bindAll = bindAll;
    lodash.bindKey = bindKey;
    lodash.chain = chain;
    lodash.compact = compact;
    lodash.compose = compose;
    lodash.constant = constant;
    lodash.countBy = countBy;
    lodash.create = create;
    lodash.createCallback = createCallback;
    lodash.curry = curry;
    lodash.debounce = debounce;
    lodash.defaults = defaults;
    lodash.defer = defer;
    lodash.delay = delay;
    lodash.difference = difference;
    lodash.filter = filter;
    lodash.flatten = flatten;
    lodash.forEach = forEach;
    lodash.forEachRight = forEachRight;
    lodash.forIn = forIn;
    lodash.forInRight = forInRight;
    lodash.forOwn = forOwn;
    lodash.forOwnRight = forOwnRight;
    lodash.functions = functions;
    lodash.groupBy = groupBy;
    lodash.indexBy = indexBy;
    lodash.initial = initial;
    lodash.intersection = intersection;
    lodash.invert = invert;
    lodash.invoke = invoke;
    lodash.keys = keys;
    lodash.map = map;
    lodash.mapValues = mapValues;
    lodash.max = max;
    lodash.memoize = memoize;
    lodash.merge = merge;
    lodash.min = min;
    lodash.omit = omit;
    lodash.once = once;
    lodash.pairs = pairs;
    lodash.partial = partial;
    lodash.partialRight = partialRight;
    lodash.pick = pick;
    lodash.pluck = pluck;
    lodash.property = property;
    lodash.pull = pull;
    lodash.range = range;
    lodash.reject = reject;
    lodash.remove = remove;
    lodash.rest = rest;
    lodash.shuffle = shuffle;
    lodash.sortBy = sortBy;
    lodash.tap = tap;
    lodash.throttle = throttle;
    lodash.times = times;
    lodash.toArray = toArray;
    lodash.transform = transform;
    lodash.union = union;
    lodash.uniq = uniq;
    lodash.values = values;
    lodash.where = where;
    lodash.without = without;
    lodash.wrap = wrap;
    lodash.xor = xor;
    lodash.zip = zip;
    lodash.zipObject = zipObject;

    // add aliases
    lodash.collect = map;
    lodash.drop = rest;
    lodash.each = forEach;
    lodash.eachRight = forEachRight;
    lodash.extend = assign;
    lodash.methods = functions;
    lodash.object = zipObject;
    lodash.select = filter;
    lodash.tail = rest;
    lodash.unique = uniq;
    lodash.unzip = zip;

    // add functions to `lodash.prototype`
    mixin(lodash);

    /*--------------------------------------------------------------------------*/

    // add functions that return unwrapped values when chaining
    lodash.clone = clone;
    lodash.cloneDeep = cloneDeep;
    lodash.contains = contains;
    lodash.escape = escape;
    lodash.every = every;
    lodash.find = find;
    lodash.findIndex = findIndex;
    lodash.findKey = findKey;
    lodash.findLast = findLast;
    lodash.findLastIndex = findLastIndex;
    lodash.findLastKey = findLastKey;
    lodash.has = has;
    lodash.identity = identity;
    lodash.indexOf = indexOf;
    lodash.isArguments = isArguments;
    lodash.isArray = isArray;
    lodash.isBoolean = isBoolean;
    lodash.isDate = isDate;
    lodash.isElement = isElement;
    lodash.isEmpty = isEmpty;
    lodash.isEqual = isEqual;
    lodash.isFinite = isFinite;
    lodash.isFunction = isFunction;
    lodash.isNaN = isNaN;
    lodash.isNull = isNull;
    lodash.isNumber = isNumber;
    lodash.isObject = isObject;
    lodash.isPlainObject = isPlainObject;
    lodash.isRegExp = isRegExp;
    lodash.isString = isString;
    lodash.isUndefined = isUndefined;
    lodash.lastIndexOf = lastIndexOf;
    lodash.mixin = mixin;
    lodash.noConflict = noConflict;
    lodash.noop = noop;
    lodash.now = now;
    lodash.parseInt = parseInt;
    lodash.random = random;
    lodash.reduce = reduce;
    lodash.reduceRight = reduceRight;
    lodash.result = result;
    lodash.runInContext = runInContext;
    lodash.size = size;
    lodash.some = some;
    lodash.sortedIndex = sortedIndex;
    lodash.template = template;
    lodash.unescape = unescape;
    lodash.uniqueId = uniqueId;

    // add aliases
    lodash.all = every;
    lodash.any = some;
    lodash.detect = find;
    lodash.findWhere = find;
    lodash.foldl = reduce;
    lodash.foldr = reduceRight;
    lodash.include = contains;
    lodash.inject = reduce;

    mixin(function() {
      var source = {}
      forOwn(lodash, function(func, methodName) {
        if (!lodash.prototype[methodName]) {
          source[methodName] = func;
        }
      });
      return source;
    }(), false);

    /*--------------------------------------------------------------------------*/

    // add functions capable of returning wrapped and unwrapped values when chaining
    lodash.first = first;
    lodash.last = last;
    lodash.sample = sample;

    // add aliases
    lodash.take = first;
    lodash.head = first;

    forOwn(lodash, function(func, methodName) {
      var callbackable = methodName !== 'sample';
      if (!lodash.prototype[methodName]) {
        lodash.prototype[methodName]= function(n, guard) {
          var chainAll = this.__chain__,
              result = func(this.__wrapped__, n, guard);

          return !chainAll && (n == null || (guard && !(callbackable && typeof n == 'function')))
            ? result
            : new lodashWrapper(result, chainAll);
        };
      }
    });

    /*--------------------------------------------------------------------------*/

    /**
     * The semantic version number.
     *
     * @static
     * @memberOf _
     * @type string
     */
    lodash.VERSION = '2.4.1';

    // add "Chaining" functions to the wrapper
    lodash.prototype.chain = wrapperChain;
    lodash.prototype.toString = wrapperToString;
    lodash.prototype.value = wrapperValueOf;
    lodash.prototype.valueOf = wrapperValueOf;

    // add `Array` functions that return unwrapped values
    forEach(['join', 'pop', 'shift'], function(methodName) {
      var func = arrayRef[methodName];
      lodash.prototype[methodName] = function() {
        var chainAll = this.__chain__,
            result = func.apply(this.__wrapped__, arguments);

        return chainAll
          ? new lodashWrapper(result, chainAll)
          : result;
      };
    });

    // add `Array` functions that return the existing wrapped value
    forEach(['push', 'reverse', 'sort', 'unshift'], function(methodName) {
      var func = arrayRef[methodName];
      lodash.prototype[methodName] = function() {
        func.apply(this.__wrapped__, arguments);
        return this;
      };
    });

    // add `Array` functions that return new wrapped values
    forEach(['concat', 'slice', 'splice'], function(methodName) {
      var func = arrayRef[methodName];
      lodash.prototype[methodName] = function() {
        return new lodashWrapper(func.apply(this.__wrapped__, arguments), this.__chain__);
      };
    });

    return lodash;
  }

  /*--------------------------------------------------------------------------*/

  // expose Lo-Dash
  var _ = runInContext();

  // some AMD build optimizers like r.js check for condition patterns like the following:
  if (typeof define == 'function' && typeof define.amd == 'object' && define.amd) {
    // Expose Lo-Dash to the global object even when an AMD loader is present in
    // case Lo-Dash is loaded with a RequireJS shim config.
    // See http://requirejs.org/docs/api.html#config-shim
    root._ = _;

    // define as an anonymous module so, through path mapping, it can be
    // referenced as the "underscore" module
    define(function() {
      return _;
    });
  }
  // check for `exports` after `define` in case a build optimizer adds an `exports` object
  else if (freeExports && freeModule) {
    // in Node.js or RingoJS
    if (moduleExports) {
      (freeModule.exports = _)._ = _;
    }
    // in Narwhal or Rhino -require
    else {
      freeExports._ = _;
    }
  }
  else {
    // in a browser or Rhino
    root._ = _;
  }
}.call(this));

/*
 AutobahnJS - http://autobahn.ws

 Copyright (C) 2011-2014 Tavendo GmbH.
 Licensed under the MIT License.
 See license text at http://www.opensource.org/licenses/mit-license.php

 AutobahnJS includes code from:

 when - http://cujojs.com

 (c) copyright B Cavalier & J Hann
 Licensed under the MIT License at:
 http://www.opensource.org/licenses/mit-license.php

 Crypto-JS - http://code.google.com/p/crypto-js/

 (c) 2009-2012 by Jeff Mott. All rights reserved.
 Licensed under the New BSD License at:
 http://code.google.com/p/crypto-js/wiki/License

 console-normalizer - https://github.com/Zenovations/console-normalizer

 (c) 2012 by Zenovations.
 Licensed under the MIT License at:
 http://www.opensource.org/licenses/mit-license.php

*/
window.define||(window.define=function(c){try{delete window.define}catch(g){window.define=void 0}window.when=c()},window.define.amd={});(function(c){c||(c=window.console={log:function(c,a,b,d,h){},info:function(c,a,b,d,h){},warn:function(c,a,b,d,h){},error:function(c,a,b,d,h){}});Function.prototype.bind||(Function.prototype.bind=function(c){var a=this,b=Array.prototype.slice.call(arguments,1);return function(){return a.apply(c,Array.prototype.concat.apply(b,arguments))}});"object"===typeof c.log&&(c.log=Function.prototype.call.bind(c.log,c),c.info=Function.prototype.call.bind(c.info,c),c.warn=Function.prototype.call.bind(c.warn,c),
c.error=Function.prototype.call.bind(c.error,c));"group"in c||(c.group=function(g){c.info("\n--- "+g+" ---\n")});"groupEnd"in c||(c.groupEnd=function(){c.log("\n")});"time"in c||function(){var g={};c.time=function(a){g[a]=(new Date).getTime()};c.timeEnd=function(a){var b=(new Date).getTime();c.info(a+": "+(a in g?b-g[a]:0)+"ms")}}()})(window.console);/*
 MIT License (c) copyright 2011-2013 original author or authors */
(function(c){c(function(c){function a(a,b,e,c){return(a instanceof d?a:h(a)).then(b,e,c)}function b(a){return new d(a,B.PromiseStatus&&B.PromiseStatus())}function d(a,b){function d(a){if(m){var c=m;m=w;p(function(){q=e(l,a);b&&A(q,b);f(c,q)})}}function c(a){d(new k(a))}function h(a){if(m){var b=m;p(function(){f(b,new z(a))})}}var l,q,m=[];l=this;this._status=b;this.inspect=function(){return q?q.inspect():{state:"pending"}};this._when=function(a,b,e,d,c){function f(h){h._when(a,b,e,d,c)}m?m.push(f):
p(function(){f(q)})};try{a(d,c,h)}catch(n){c(n)}}function h(a){return b(function(b){b(a)})}function f(a,b){for(var e=0;e<a.length;e++)a[e](b)}function e(a,b){if(b===a)return new k(new TypeError);if(b instanceof d)return b;try{var e=b===Object(b)&&b.then;return"function"===typeof e?l(e,b):new t(b)}catch(c){return new k(c)}}function l(a,e){return b(function(b,d){G(a,e,b,d)})}function t(a){this.value=a}function k(a){this.value=a}function z(a){this.value=a}function A(a,b){a.then(function(){b.fulfilled()},
function(a){b.rejected(a)})}function q(a){return a&&"function"===typeof a.then}function m(e,d,c,f,h){return a(e,function(e){return b(function(b,c,f){function h(a){n(a)}function A(a){k(a)}var l,q,D,m,k,n,t,g;t=e.length>>>0;l=Math.max(0,Math.min(d,t));D=[];q=t-l+1;m=[];if(l){n=function(a){m.push(a);--q||(k=n=s,c(m))};k=function(a){D.push(a);--l||(k=n=s,b(D))};for(g=0;g<t;++g)g in e&&a(e[g],A,h,f)}else b(D)}).then(c,f,h)})}function n(a,b,e,d){return u(a,s).then(b,e,d)}function u(b,e,c){return a(b,function(b){return new d(function(d,
f,h){function A(b,q){a(b,e,c).then(function(a){l[q]=a;--k||d(l)},f,h)}var l,q,k,m;k=q=b.length>>>0;l=[];if(k)for(m=0;m<q;m++)m in b?A(b[m],m):--k;else d(l)})})}function y(a){return{state:"fulfilled",value:a}}function x(a){return{state:"rejected",reason:a}}function p(a){1===E.push(a)&&C(v)}function v(){f(E);E=[]}function s(a){return a}function K(a){"function"===typeof B.reportUnhandled?B.reportUnhandled():p(function(){throw a;});throw a;}a.promise=b;a.resolve=h;a.reject=function(b){return a(b,function(a){return new k(a)})};
a.defer=function(){var a,e,d;a={promise:w,resolve:w,reject:w,notify:w,resolver:{resolve:w,reject:w,notify:w}};a.promise=e=b(function(b,c,f){a.resolve=a.resolver.resolve=function(a){if(d)return h(a);d=!0;b(a);return e};a.reject=a.resolver.reject=function(a){if(d)return h(new k(a));d=!0;c(a);return e};a.notify=a.resolver.notify=function(a){f(a);return a}});return a};a.join=function(){return u(arguments,s)};a.all=n;a.map=function(a,b){return u(a,b)};a.reduce=function(b,e){var d=G(H,arguments,1);return a(b,
function(b){var c;c=b.length;d[0]=function(b,d,f){return a(b,function(b){return a(d,function(a){return e(b,a,f,c)})})};return I.apply(b,d)})};a.settle=function(a){return u(a,y,x)};a.any=function(a,b,e,d){return m(a,1,function(a){return b?b(a[0]):a[0]},e,d)};a.some=m;a.isPromise=q;a.isPromiseLike=q;r=d.prototype;r.then=function(a,b,e){var c=this;return new d(function(d,f,h){c._when(d,h,a,b,e)},this._status&&this._status.observed())};r["catch"]=r.otherwise=function(a){return this.then(w,a)};r["finally"]=
r.ensure=function(a){function b(){return h(a())}return"function"===typeof a?this.then(b,b).yield(this):this};r.done=function(a,b){this.then(a,b)["catch"](K)};r.yield=function(a){return this.then(function(){return a})};r.tap=function(a){return this.then(a).yield(this)};r.spread=function(a){return this.then(function(b){return n(b,function(b){return a.apply(w,b)})})};r.always=function(a,b){return this.then(a,a,b)};F=Object.create||function(a){function b(){}b.prototype=a;return new b};t.prototype=F(r);
t.prototype.inspect=function(){return y(this.value)};t.prototype._when=function(a,b,e){try{a("function"===typeof e?e(this.value):this.value)}catch(d){a(new k(d))}};k.prototype=F(r);k.prototype.inspect=function(){return x(this.value)};k.prototype._when=function(a,b,e,d){try{a("function"===typeof d?d(this.value):this)}catch(c){a(new k(c))}};z.prototype=F(r);z.prototype._when=function(a,b,e,d,c){try{b("function"===typeof c?c(this.value):this.value)}catch(f){b(f)}};var r,F,I,H,G,C,E,B,J,w;E=[];B="undefined"!==
typeof console?console:a;if("object"===typeof process&&process.nextTick)C=process.nextTick;else if(r="function"===typeof MutationObserver&&MutationObserver||"function"===typeof WebKitMutationObserver&&WebKitMutationObserver)C=function(a,b,e){var d=a.createElement("div");(new b(e)).observe(d,{attributes:!0});return function(){d.setAttribute("x","x")}}(document,r,v);else try{C=c("vertx").runOnLoop||c("vertx").runOnContext}catch(L){J=setTimeout,C=function(a){J(a,0)}}c=Function.prototype;r=c.call;G=c.bind?
r.bind(r):function(a,b){return a.apply(b,H.call(arguments,2))};c=[];H=c.slice;I=c.reduce||function(a){var b,e,d,c,f;f=0;b=Object(this);c=b.length>>>0;e=arguments;if(1>=e.length)for(;;){if(f in b){d=b[f++];break}if(++f>=c)throw new TypeError;}else d=e[1];for(;f<c;++f)f in b&&(d=a(d,b[f],f,b));return d};return a})})("function"===typeof define&&define.amd?define:function(c){module.exports=c(require)});var CryptoJS=CryptoJS||function(c,g){var a={},b=a.lib={},d=b.Base=function(){function a(){}return{extend:function(b){a.prototype=this;var e=new a;b&&e.mixIn(b);e.hasOwnProperty("init")||(e.init=function(){e.$super.init.apply(this,arguments)});e.init.prototype=e;e.$super=this;return e},create:function(){var a=this.extend();a.init.apply(a,arguments);return a},init:function(){},mixIn:function(a){for(var b in a)a.hasOwnProperty(b)&&(this[b]=a[b]);a.hasOwnProperty("toString")&&(this.toString=a.toString)},
clone:function(){return this.init.prototype.extend(this)}}}(),h=b.WordArray=d.extend({init:function(a,b){a=this.words=a||[];this.sigBytes=b!=g?b:4*a.length},toString:function(a){return(a||e).stringify(this)},concat:function(a){var b=this.words,e=a.words,d=this.sigBytes;a=a.sigBytes;this.clamp();if(d%4)for(var c=0;c<a;c++)b[d+c>>>2]|=(e[c>>>2]>>>24-8*(c%4)&255)<<24-8*((d+c)%4);else if(65535<e.length)for(c=0;c<a;c+=4)b[d+c>>>2]=e[c>>>2];else b.push.apply(b,e);this.sigBytes+=a;return this},clamp:function(){var a=
this.words,b=this.sigBytes;a[b>>>2]&=4294967295<<32-8*(b%4);a.length=c.ceil(b/4)},clone:function(){var a=d.clone.call(this);a.words=this.words.slice(0);return a},random:function(a){for(var b=[],e=0;e<a;e+=4)b.push(4294967296*c.random()|0);return new h.init(b,a)}}),f=a.enc={},e=f.Hex={stringify:function(a){var b=a.words;a=a.sigBytes;for(var e=[],d=0;d<a;d++){var c=b[d>>>2]>>>24-8*(d%4)&255;e.push((c>>>4).toString(16));e.push((c&15).toString(16))}return e.join("")},parse:function(a){for(var b=a.length,
e=[],d=0;d<b;d+=2)e[d>>>3]|=parseInt(a.substr(d,2),16)<<24-4*(d%8);return new h.init(e,b/2)}},l=f.Latin1={stringify:function(a){var b=a.words;a=a.sigBytes;for(var e=[],d=0;d<a;d++)e.push(String.fromCharCode(b[d>>>2]>>>24-8*(d%4)&255));return e.join("")},parse:function(a){for(var b=a.length,e=[],d=0;d<b;d++)e[d>>>2]|=(a.charCodeAt(d)&255)<<24-8*(d%4);return new h.init(e,b)}},t=f.Utf8={stringify:function(a){try{return decodeURIComponent(escape(l.stringify(a)))}catch(b){throw Error("Malformed UTF-8 data");
}},parse:function(a){return l.parse(unescape(encodeURIComponent(a)))}},k=b.BufferedBlockAlgorithm=d.extend({reset:function(){this._data=new h.init;this._nDataBytes=0},_append:function(a){"string"==typeof a&&(a=t.parse(a));this._data.concat(a);this._nDataBytes+=a.sigBytes},_process:function(a){var b=this._data,e=b.words,d=b.sigBytes,f=this.blockSize,l=d/(4*f),l=a?c.ceil(l):c.max((l|0)-this._minBufferSize,0);a=l*f;d=c.min(4*a,d);if(a){for(var k=0;k<a;k+=f)this._doProcessBlock(e,k);k=e.splice(0,a);b.sigBytes-=
d}return new h.init(k,d)},clone:function(){var a=d.clone.call(this);a._data=this._data.clone();return a},_minBufferSize:0});b.Hasher=k.extend({cfg:d.extend(),init:function(a){this.cfg=this.cfg.extend(a);this.reset()},reset:function(){k.reset.call(this);this._doReset()},update:function(a){this._append(a);this._process();return this},finalize:function(a){a&&this._append(a);return this._doFinalize()},blockSize:16,_createHelper:function(a){return function(b,e){return(new a.init(e)).finalize(b)}},_createHmacHelper:function(a){return function(b,
e){return(new z.HMAC.init(a,e)).finalize(b)}}});var z=a.algo={};return a}(Math);(function(){var c=CryptoJS,g=c.lib.WordArray;c.enc.Base64={stringify:function(a){var b=a.words,d=a.sigBytes,c=this._map;a.clamp();a=[];for(var f=0;f<d;f+=3)for(var e=(b[f>>>2]>>>24-8*(f%4)&255)<<16|(b[f+1>>>2]>>>24-8*((f+1)%4)&255)<<8|b[f+2>>>2]>>>24-8*((f+2)%4)&255,l=0;4>l&&f+0.75*l<d;l++)a.push(c.charAt(e>>>6*(3-l)&63));if(b=c.charAt(64))for(;a.length%4;)a.push(b);return a.join("")},parse:function(a){var b=a.length,d=this._map,c=d.charAt(64);c&&(c=a.indexOf(c),-1!=c&&(b=c));for(var c=[],f=0,e=0;e<
b;e++)if(e%4){var l=d.indexOf(a.charAt(e-1))<<2*(e%4),t=d.indexOf(a.charAt(e))>>>6-2*(e%4);c[f>>>2]|=(l|t)<<24-8*(f%4);f++}return g.create(c,f)},_map:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="}})();(function(){var c=CryptoJS,g=c.enc.Utf8;c.algo.HMAC=c.lib.Base.extend({init:function(a,b){a=this._hasher=new a.init;"string"==typeof b&&(b=g.parse(b));var d=a.blockSize,c=4*d;b.sigBytes>c&&(b=a.finalize(b));b.clamp();for(var f=this._oKey=b.clone(),e=this._iKey=b.clone(),l=f.words,t=e.words,k=0;k<d;k++)l[k]^=1549556828,t[k]^=909522486;f.sigBytes=e.sigBytes=c;this.reset()},reset:function(){var a=this._hasher;a.reset();a.update(this._iKey)},update:function(a){this._hasher.update(a);return this},finalize:function(a){var b=
this._hasher;a=b.finalize(a);b.reset();return b.finalize(this._oKey.clone().concat(a))}})})();(function(c){var g=CryptoJS,a=g.lib,b=a.WordArray,d=a.Hasher,a=g.algo,h=[],f=[];(function(){function a(b){for(var e=c.sqrt(b),d=2;d<=e;d++)if(!(b%d))return!1;return!0}function b(a){return 4294967296*(a-(a|0))|0}for(var e=2,d=0;64>d;)a(e)&&(8>d&&(h[d]=b(c.pow(e,0.5))),f[d]=b(c.pow(e,1/3)),d++),e++})();var e=[],a=a.SHA256=d.extend({_doReset:function(){this._hash=new b.init(h.slice(0))},_doProcessBlock:function(a,b){for(var d=this._hash.words,c=d[0],h=d[1],g=d[2],m=d[3],n=d[4],u=d[5],y=d[6],x=d[7],p=
0;64>p;p++){if(16>p)e[p]=a[b+p]|0;else{var v=e[p-15],s=e[p-2];e[p]=((v<<25|v>>>7)^(v<<14|v>>>18)^v>>>3)+e[p-7]+((s<<15|s>>>17)^(s<<13|s>>>19)^s>>>10)+e[p-16]}v=x+((n<<26|n>>>6)^(n<<21|n>>>11)^(n<<7|n>>>25))+(n&u^~n&y)+f[p]+e[p];s=((c<<30|c>>>2)^(c<<19|c>>>13)^(c<<10|c>>>22))+(c&h^c&g^h&g);x=y;y=u;u=n;n=m+v|0;m=g;g=h;h=c;c=v+s|0}d[0]=d[0]+c|0;d[1]=d[1]+h|0;d[2]=d[2]+g|0;d[3]=d[3]+m|0;d[4]=d[4]+n|0;d[5]=d[5]+u|0;d[6]=d[6]+y|0;d[7]=d[7]+x|0},_doFinalize:function(){var a=this._data,b=a.words,d=8*this._nDataBytes,
e=8*a.sigBytes;b[e>>>5]|=128<<24-e%32;b[(e+64>>>9<<4)+14]=c.floor(d/4294967296);b[(e+64>>>9<<4)+15]=d;a.sigBytes=4*b.length;this._process();return this._hash},clone:function(){var a=d.clone.call(this);a._hash=this._hash.clone();return a}});g.SHA256=d._createHelper(a);g.HmacSHA256=d._createHmacHelper(a)})(Math);(function(){var c=CryptoJS,g=c.lib,a=g.Base,b=g.WordArray,g=c.algo,d=g.HMAC,h=g.PBKDF2=a.extend({cfg:a.extend({keySize:4,hasher:g.SHA1,iterations:1}),init:function(a){this.cfg=this.cfg.extend(a)},compute:function(a,e){for(var c=this.cfg,h=d.create(c.hasher,a),g=b.create(),z=b.create([1]),A=g.words,q=z.words,m=c.keySize,c=c.iterations;A.length<m;){var n=h.update(e).finalize(z);h.reset();for(var u=n.words,y=u.length,x=n,p=1;p<c;p++){x=h.finalize(x);h.reset();for(var v=x.words,s=0;s<y;s++)u[s]^=v[s]}g.concat(n);
q[0]++}g.sigBytes=4*m;return g}});c.PBKDF2=function(a,b,d){return h.create(d).compute(a,b)}})();/*
 MIT License (c) 2011-2013 Copyright Tavendo GmbH. */
var AUTOBAHNJS_VERSION="0.8.2",global=this;
(function(c,g){"function"===typeof define&&define.amd?define(["when"],function(a){return c.ab=g(c,a)}):"undefined"!==typeof exports?"undefined"!=typeof module&&module.exports&&(exports=module.exports=g(c,c.when)):c.ab=g(c,c.when)})(global,function(c,g){var a={_version:AUTOBAHNJS_VERSION};(function(){Array.prototype.indexOf||(Array.prototype.indexOf=function(a){if(null===this)throw new TypeError;var d=Object(this),c=d.length>>>0;if(0===c)return-1;var f=0;0<arguments.length&&(f=Number(arguments[1]),
f!==f?f=0:0!==f&&(Infinity!==f&&-Infinity!==f)&&(f=(0<f||-1)*Math.floor(Math.abs(f))));if(f>=c)return-1;for(f=0<=f?f:Math.max(c-Math.abs(f),0);f<c;f++)if(f in d&&d[f]===a)return f;return-1});Array.prototype.forEach||(Array.prototype.forEach=function(a,d){var c,f;if(null===this)throw new TypeError(" this is null or not defined");var e=Object(this),l=e.length>>>0;if("[object Function]"!=={}.toString.call(a))throw new TypeError(a+" is not a function");d&&(c=d);for(f=0;f<l;){var g;f in e&&(g=e[f],a.call(c,
g,f,e));f++}})})();a._sliceUserAgent=function(a,d,c){var f=[],e=navigator.userAgent;a=e.indexOf(a);d=e.indexOf(d,a);0>d&&(d=e.length);c=e.slice(a,d).split(c);e=c[1].split(".");for(d=0;d<e.length;++d)f.push(parseInt(e[d],10));return{name:c[0],version:f}};a.getBrowser=function(){var b=navigator.userAgent;return-1<b.indexOf("Chrome")?a._sliceUserAgent("Chrome"," ","/"):-1<b.indexOf("Safari")?a._sliceUserAgent("Safari"," ","/"):-1<b.indexOf("Firefox")?a._sliceUserAgent("Firefox"," ","/"):-1<b.indexOf("MSIE")?
a._sliceUserAgent("MSIE",";"," "):null};a.getServerUrl=function(a,d){return"file:"===c.location.protocol?d?d:"ws://127.0.0.1/ws":("https:"===c.location.protocol?"wss://":"ws://")+c.location.hostname+(""!==c.location.port?":"+c.location.port:"")+"/"+(a?a:"ws")};a.browserNotSupportedMessage="Browser does not support WebSockets (RFC6455)";a.deriveKey=function(a,d){return d&&d.salt?CryptoJS.PBKDF2(a,d.salt,{keySize:(d.keylen||32)/4,iterations:d.iterations||1E4,hasher:CryptoJS.algo.SHA256}).toString(CryptoJS.enc.Base64):
a};a._idchars="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";a._idlen=16;a._subprotocol="wamp";a._newid=function(){for(var b="",d=0;d<a._idlen;d+=1)b+=a._idchars.charAt(Math.floor(Math.random()*a._idchars.length));return b};a._newidFast=function(){return Math.random().toString(36)};a.log=function(){if(1<arguments.length){console.group("Log Item");for(var a=0;a<arguments.length;a+=1)console.log(arguments[a]);console.groupEnd()}else console.log(arguments[0])};a._debugrpc=!1;a._debugpubsub=
!1;a._debugws=!1;a._debugconnect=!1;a.debug=function(b,d,h){if("console"in c)a._debugrpc=b,a._debugpubsub=b,a._debugws=d,a._debugconnect=h;else throw"browser does not support console object";};a.version=function(){return a._version};a.PrefixMap=function(){this._index={};this._rindex={}};a.PrefixMap.prototype.get=function(a){return this._index[a]};a.PrefixMap.prototype.set=function(a,d){this._index[a]=d;this._rindex[d]=a};a.PrefixMap.prototype.setDefault=function(a){this._index[""]=a;this._rindex[a]=
""};a.PrefixMap.prototype.remove=function(a){var d=this._index[a];d&&(delete this._index[a],delete this._rindex[d])};a.PrefixMap.prototype.resolve=function(a,d){var c=a.indexOf(":");if(0<=c){var f=a.substring(0,c);if(this._index[f])return this._index[f]+a.substring(c+1)}return!0===d?a:null};a.PrefixMap.prototype.shrink=function(a,d){for(var c=a.length;0<c;c-=1){var f=a.substring(0,c);if(f=this._rindex[f])return f+":"+a.substring(c)}return!0===d?a:null};a._MESSAGE_TYPEID_WELCOME=0;a._MESSAGE_TYPEID_PREFIX=
1;a._MESSAGE_TYPEID_CALL=2;a._MESSAGE_TYPEID_CALL_RESULT=3;a._MESSAGE_TYPEID_CALL_ERROR=4;a._MESSAGE_TYPEID_SUBSCRIBE=5;a._MESSAGE_TYPEID_UNSUBSCRIBE=6;a._MESSAGE_TYPEID_PUBLISH=7;a._MESSAGE_TYPEID_EVENT=8;a.CONNECTION_CLOSED=0;a.CONNECTION_LOST=1;a.CONNECTION_RETRIES_EXCEEDED=2;a.CONNECTION_UNREACHABLE=3;a.CONNECTION_UNSUPPORTED=4;a.CONNECTION_UNREACHABLE_SCHEDULED_RECONNECT=5;a.CONNECTION_LOST_SCHEDULED_RECONNECT=6;a.Deferred=g.defer;a._construct=function(a,d){return"WebSocket"in c?d?new WebSocket(a,
d):new WebSocket(a):"MozWebSocket"in c?d?new MozWebSocket(a,d):new MozWebSocket(a):null};a.Session=function(b,d,c,f){var e=this;e._wsuri=b;e._options=f;e._websocket_onopen=d;e._websocket_onclose=c;e._websocket=null;e._websocket_connected=!1;e._session_id=null;e._wamp_version=null;e._server=null;e._calls={};e._subscriptions={};e._prefixes=new a.PrefixMap;e._txcnt=0;e._rxcnt=0;e._websocket=e._options&&e._options.skipSubprotocolAnnounce?a._construct(e._wsuri):a._construct(e._wsuri,[a._subprotocol]);
if(!e._websocket){if(void 0!==c){c(a.CONNECTION_UNSUPPORTED);return}throw a.browserNotSupportedMessage;}e._websocket.onmessage=function(b){a._debugws&&(e._rxcnt+=1,console.group("WS Receive"),console.info(e._wsuri+"  ["+e._session_id+"]"),console.log(e._rxcnt),console.log(b.data),console.groupEnd());b=JSON.parse(b.data);if(b[1]in e._calls){if(b[0]===a._MESSAGE_TYPEID_CALL_RESULT){var d=e._calls[b[1]],c=b[2];if(a._debugrpc&&void 0!==d._ab_callobj){console.group("WAMP Call",d._ab_callobj[2]);console.timeEnd(d._ab_tid);
console.group("Arguments");for(var f=3;f<d._ab_callobj.length;f+=1){var h=d._ab_callobj[f];if(void 0!==h)console.log(h);else break}console.groupEnd();console.group("Result");console.log(c);console.groupEnd();console.groupEnd()}d.resolve(c)}else if(b[0]===a._MESSAGE_TYPEID_CALL_ERROR){d=e._calls[b[1]];c=b[2];f=b[3];h=b[4];if(a._debugrpc&&void 0!==d._ab_callobj){console.group("WAMP Call",d._ab_callobj[2]);console.timeEnd(d._ab_tid);console.group("Arguments");for(var g=3;g<d._ab_callobj.length;g+=1){var m=
d._ab_callobj[g];if(void 0!==m)console.log(m);else break}console.groupEnd();console.group("Error");console.log(c);console.log(f);void 0!==h&&console.log(h);console.groupEnd();console.groupEnd()}void 0!==h?d.reject({uri:c,desc:f,detail:h}):d.reject({uri:c,desc:f})}delete e._calls[b[1]]}else if(b[0]===a._MESSAGE_TYPEID_EVENT){if(d=e._prefixes.resolve(b[1],!0),d in e._subscriptions){var n=b[1],u=b[2];a._debugpubsub&&(console.group("WAMP Event"),console.info(e._wsuri+"  ["+e._session_id+"]"),console.log(n),
console.log(u),console.groupEnd());e._subscriptions[d].forEach(function(a){a(n,u)})}}else if(b[0]===a._MESSAGE_TYPEID_WELCOME)if(null===e._session_id){e._session_id=b[1];e._wamp_version=b[2];e._server=b[3];if(a._debugrpc||a._debugpubsub)console.group("WAMP Welcome"),console.info(e._wsuri+"  ["+e._session_id+"]"),console.log(e._wamp_version),console.log(e._server),console.groupEnd();null!==e._websocket_onopen&&e._websocket_onopen()}else throw"protocol error (welcome message received more than once)";
};e._websocket.onopen=function(b){if(e._websocket.protocol!==a._subprotocol)if("undefined"===typeof e._websocket.protocol)a._debugws&&(console.group("WS Warning"),console.info(e._wsuri),console.log("WebSocket object has no protocol attribute: WAMP subprotocol check skipped!"),console.groupEnd());else if(e._options&&e._options.skipSubprotocolCheck)a._debugws&&(console.group("WS Warning"),console.info(e._wsuri),console.log("Server does not speak WAMP, but subprotocol check disabled by option!"),console.log(e._websocket.protocol),
console.groupEnd());else throw e._websocket.close(1E3,"server does not speak WAMP"),"server does not speak WAMP (but '"+e._websocket.protocol+"' !)";a._debugws&&(console.group("WAMP Connect"),console.info(e._wsuri),console.log(e._websocket.protocol),console.groupEnd());e._websocket_connected=!0};e._websocket.onerror=function(a){};e._websocket.onclose=function(b){a._debugws&&(e._websocket_connected?console.log("Autobahn connection to "+e._wsuri+" lost (code "+b.code+", reason '"+b.reason+"', wasClean "+
b.wasClean+")."):console.log("Autobahn could not connect to "+e._wsuri+" (code "+b.code+", reason '"+b.reason+"', wasClean "+b.wasClean+")."));void 0!==e._websocket_onclose&&(e._websocket_connected?b.wasClean?e._websocket_onclose(a.CONNECTION_CLOSED,"WS-"+b.code+": "+b.reason):e._websocket_onclose(a.CONNECTION_LOST):e._websocket_onclose(a.CONNECTION_UNREACHABLE));e._websocket_connected=!1;e._wsuri=null;e._websocket_onopen=null;e._websocket_onclose=null;e._websocket=null};e.log=function(){e._options&&
"sessionIdent"in e._options?console.group("WAMP Session '"+e._options.sessionIdent+"' ["+e._session_id+"]"):console.group("WAMP Session ["+e._session_id+"]");for(var a=0;a<arguments.length;++a)console.log(arguments[a]);console.groupEnd()}};a.Session.prototype._send=function(b){if(!this._websocket_connected)throw"Autobahn not connected";switch(!0){case c.Prototype&&"undefined"===typeof top.root.__prototype_deleted:case "function"===typeof b.toJSON:b=b.toJSON();break;default:b=JSON.stringify(b)}this._websocket.send(b);
this._txcnt+=1;a._debugws&&(console.group("WS Send"),console.info(this._wsuri+"  ["+this._session_id+"]"),console.log(this._txcnt),console.log(b),console.groupEnd())};a.Session.prototype.close=function(){this._websocket_connected&&this._websocket.close()};a.Session.prototype.sessionid=function(){return this._session_id};a.Session.prototype.wsuri=function(){return this._wsuri};a.Session.prototype.shrink=function(a,d){void 0===d&&(d=!0);return this._prefixes.shrink(a,d)};a.Session.prototype.resolve=
function(a,d){void 0===d&&(d=!0);return this._prefixes.resolve(a,d)};a.Session.prototype.prefix=function(b,d){this._prefixes.set(b,d);if(a._debugrpc||a._debugpubsub)console.group("WAMP Prefix"),console.info(this._wsuri+"  ["+this._session_id+"]"),console.log(b),console.log(d),console.groupEnd();this._send([a._MESSAGE_TYPEID_PREFIX,b,d])};a.Session.prototype.call=function(){for(var b=new a.Deferred,d;!(d=a._newidFast(),!(d in this._calls)););this._calls[d]=b;for(var c=this._prefixes.shrink(arguments[0],
!0),c=[a._MESSAGE_TYPEID_CALL,d,c],f=1;f<arguments.length;f+=1)c.push(arguments[f]);this._send(c);a._debugrpc&&(b._ab_callobj=c,b._ab_tid=this._wsuri+"  ["+this._session_id+"]["+d+"]",console.time(b._ab_tid),console.info());return b.promise.then?b.promise:b};a.Session.prototype.subscribe=function(b,d){var c=this._prefixes.resolve(b,!0);c in this._subscriptions||(a._debugpubsub&&(console.group("WAMP Subscribe"),console.info(this._wsuri+"  ["+this._session_id+"]"),console.log(b),console.log(d),console.groupEnd()),
this._send([a._MESSAGE_TYPEID_SUBSCRIBE,b]),this._subscriptions[c]=[]);if(-1===this._subscriptions[c].indexOf(d))this._subscriptions[c].push(d);else throw"callback "+d+" already subscribed for topic "+c;};a.Session.prototype.unsubscribe=function(b,d){var c=this._prefixes.resolve(b,!0);if(c in this._subscriptions){var f;if(void 0!==d){var e=this._subscriptions[c].indexOf(d);if(-1!==e)f=d,this._subscriptions[c].splice(e,1);else throw"no callback "+d+" subscribed on topic "+c;}else f=this._subscriptions[c].slice(),
this._subscriptions[c]=[];0===this._subscriptions[c].length&&(delete this._subscriptions[c],a._debugpubsub&&(console.group("WAMP Unsubscribe"),console.info(this._wsuri+"  ["+this._session_id+"]"),console.log(b),console.log(f),console.groupEnd()),this._send([a._MESSAGE_TYPEID_UNSUBSCRIBE,b]))}else throw"not subscribed to topic "+c;};a.Session.prototype.publish=function(){var b=arguments[0],d=arguments[1],c=null,f=null,e=null,g=null;if(3<arguments.length){if(!(arguments[2]instanceof Array))throw"invalid argument type(s)";
if(!(arguments[3]instanceof Array))throw"invalid argument type(s)";f=arguments[2];e=arguments[3];g=[a._MESSAGE_TYPEID_PUBLISH,b,d,f,e]}else if(2<arguments.length)if("boolean"===typeof arguments[2])c=arguments[2],g=[a._MESSAGE_TYPEID_PUBLISH,b,d,c];else if(arguments[2]instanceof Array)f=arguments[2],g=[a._MESSAGE_TYPEID_PUBLISH,b,d,f];else throw"invalid argument type(s)";else g=[a._MESSAGE_TYPEID_PUBLISH,b,d];a._debugpubsub&&(console.group("WAMP Publish"),console.info(this._wsuri+"  ["+this._session_id+
"]"),console.log(b),console.log(d),null!==c?console.log(c):null!==f&&(console.log(f),null!==e&&console.log(e)),console.groupEnd());this._send(g)};a.Session.prototype.authreq=function(a,d){return this.call("http://api.wamp.ws/procedure#authreq",a,d)};a.Session.prototype.authsign=function(a,d){d||(d="");return CryptoJS.HmacSHA256(a,d).toString(CryptoJS.enc.Base64)};a.Session.prototype.auth=function(a){return this.call("http://api.wamp.ws/procedure#auth",a)};a._connect=function(b){var d=new a.Session(b.wsuri,
function(){b.connects+=1;b.retryCount=0;b.onConnect(d)},function(d,f){var e=null;switch(d){case a.CONNECTION_CLOSED:b.onHangup(d,"Connection was closed properly ["+f+"]");break;case a.CONNECTION_UNSUPPORTED:b.onHangup(d,"Browser does not support WebSocket.");break;case a.CONNECTION_UNREACHABLE:b.retryCount+=1;if(0===b.connects)b.onHangup(d,"Connection could not be established.");else if(b.retryCount<=b.options.maxRetries)(e=b.onHangup(a.CONNECTION_UNREACHABLE_SCHEDULED_RECONNECT,"Connection unreachable - scheduled reconnect to occur in "+
b.options.retryDelay/1E3+" second(s) - attempt "+b.retryCount+" of "+b.options.maxRetries+".",{delay:b.options.retryDelay,retries:b.retryCount,maxretries:b.options.maxRetries}))?(a._debugconnect&&console.log("Connection unreachable - retrying stopped by app"),b.onHangup(a.CONNECTION_RETRIES_EXCEEDED,"Number of connection retries exceeded.")):(a._debugconnect&&console.log("Connection unreachable - retrying ("+b.retryCount+") .."),c.setTimeout(function(){a._connect(b)},b.options.retryDelay));else b.onHangup(a.CONNECTION_RETRIES_EXCEEDED,
"Number of connection retries exceeded.");break;case a.CONNECTION_LOST:b.retryCount+=1;if(b.retryCount<=b.options.maxRetries)(e=b.onHangup(a.CONNECTION_LOST_SCHEDULED_RECONNECT,"Connection lost - scheduled "+b.retryCount+"th reconnect to occur in "+b.options.retryDelay/1E3+" second(s).",{delay:b.options.retryDelay,retries:b.retryCount,maxretries:b.options.maxRetries}))?(a._debugconnect&&console.log("Connection lost - retrying stopped by app"),b.onHangup(a.CONNECTION_RETRIES_EXCEEDED,"Connection lost.")):
(a._debugconnect&&console.log("Connection lost - retrying ("+b.retryCount+") .."),c.setTimeout(function(){a._connect(b)},b.options.retryDelay));else b.onHangup(a.CONNECTION_RETRIES_EXCEEDED,"Connection lost.");break;default:throw"unhandled close code in ab._connect";}},b.options)};a.connect=function(b,d,c,f){var e={};e.wsuri=b;e.options=f?f:{};void 0===e.options.retryDelay&&(e.options.retryDelay=5E3);void 0===e.options.maxRetries&&(e.options.maxRetries=10);void 0===e.options.skipSubprotocolCheck&&
(e.options.skipSubprotocolCheck=!1);void 0===e.options.skipSubprotocolAnnounce&&(e.options.skipSubprotocolAnnounce=!1);if(d)e.onConnect=d;else throw"onConnect handler required!";e.onHangup=c?c:function(b,d,c){a._debugconnect&&console.log(b,d,c)};e.connects=0;e.retryCount=0;a._connect(e)};a.launch=function(b,d,c){a.connect(b.wsuri,function(c){!b.appkey||""===b.appkey?c.authreq().then(function(){c.auth().then(function(b){d?d(c):a._debugconnect&&c.log("Session opened.")},c.log)},c.log):c.authreq(b.appkey,
b.appextra).then(function(e){var g=null;"function"===typeof b.appsecret?g=b.appsecret(e):(g=a.deriveKey(b.appsecret,JSON.parse(e).authextra),g=c.authsign(e,g));c.auth(g).then(function(b){d?d(c):a._debugconnect&&c.log("Session opened.")},c.log)},c.log)},function(b,d,g){c?c(b,d,g):a._debugconnect&&a.log("Session closed.",b,d,g)},b.sessionConfig)};return a});ab._UA_FIREFOX=/.*Firefox\/([0-9+]*).*/;ab._UA_CHROME=/.*Chrome\/([0-9+]*).*/;ab._UA_CHROMEFRAME=/.*chromeframe\/([0-9]*).*/;ab._UA_WEBKIT=/.*AppleWebKit\/([0-9+.]*)w*.*/;ab._UA_WEBOS=/.*webOS\/([0-9+.]*)w*.*/;ab._matchRegex=function(c,g){var a=g.exec(c);return a?a[1]:a};
ab.lookupWsSupport=function(){var c=navigator.userAgent;if(-1<c.indexOf("MSIE")){if(-1<c.indexOf("MSIE 10"))return[!0,!0,!0];if(-1<c.indexOf("chromeframe")){var g=parseInt(ab._matchRegex(c,ab._UA_CHROMEFRAME));return 14<=g?[!0,!1,!0]:[!1,!1,!1]}if(-1<c.indexOf("MSIE 8")||-1<c.indexOf("MSIE 9"))return[!0,!0,!0]}else{if(-1<c.indexOf("Firefox")){if(g=parseInt(ab._matchRegex(c,ab._UA_FIREFOX))){if(7<=g)return[!0,!1,!0];if(3<=g)return[!0,!0,!0]}return[!1,!1,!0]}if(-1<c.indexOf("Safari")&&-1==c.indexOf("Chrome")){if(g=
ab._matchRegex(c,ab._UA_WEBKIT))return-1<c.indexOf("Windows")&&"534+"==g||-1<c.indexOf("Macintosh")&&(g=g.replace("+","").split("."),535==parseInt(g[0])&&24<=parseInt(g[1])||535<parseInt(g[0]))?[!0,!1,!0]:-1<c.indexOf("webOS")?(g=ab._matchRegex(c,ab._UA_WEBOS).split("."),2==parseInt(g[0])?[!1,!0,!0]:[!1,!1,!1]):[!0,!0,!0]}else if(-1<c.indexOf("Chrome")){if(g=parseInt(ab._matchRegex(c,ab._UA_CHROME)))return 14<=g?[!0,!1,!0]:4<=g?[!0,!0,!0]:[!1,!1,!0]}else if(-1<c.indexOf("Android")){if(-1<c.indexOf("Firefox")||
-1<c.indexOf("CrMo"))return[!0,!1,!0];if(-1<c.indexOf("Opera"))return[!1,!1,!0];if(-1<c.indexOf("CrMo"))return[!0,!0,!0]}else if(-1<c.indexOf("iPhone")||-1<c.indexOf("iPad")||-1<c.indexOf("iPod"))return[!1,!1,!0]}return[!1,!1,!1]};
/*!
 * VERSION: 1.11.7
 * DATE: 2014-04-29
 * UPDATES AND DOCS AT: http://www.greensock.com
 * 
 * Includes all of the following: TweenLite, TweenMax, TimelineLite, TimelineMax, EasePack, CSSPlugin, RoundPropsPlugin, BezierPlugin, AttrPlugin, DirectionalRotationPlugin
 *
 * @license Copyright (c) 2008-2014, GreenSock. All rights reserved.
 * This work is subject to the terms at http://www.greensock.com/terms_of_use.html or for
 * Club GreenSock members, the software agreement that was issued with your membership.
 * 
 * @author: Jack Doyle, jack@greensock.com
 **/
(window._gsQueue||(window._gsQueue=[])).push(function(){"use strict";window._gsDefine("TweenMax",["core.Animation","core.SimpleTimeline","TweenLite"],function(t,e,i){var s=[].slice,r=function(t,e,s){i.call(this,t,e,s),this._cycle=0,this._yoyo=this.vars.yoyo===!0,this._repeat=this.vars.repeat||0,this._repeatDelay=this.vars.repeatDelay||0,this._dirty=!0,this.render=r.prototype.render},n=1e-10,a=i._internals.isSelector,o=i._internals.isArray,h=r.prototype=i.to({},.1,{}),l=[];r.version="1.11.7",h.constructor=r,h.kill()._gc=!1,r.killTweensOf=r.killDelayedCallsTo=i.killTweensOf,r.getTweensOf=i.getTweensOf,r.ticker=i.ticker,h.invalidate=function(){return this._yoyo=this.vars.yoyo===!0,this._repeat=this.vars.repeat||0,this._repeatDelay=this.vars.repeatDelay||0,this._uncache(!0),i.prototype.invalidate.call(this)},h.updateTo=function(t,e){var s,r=this.ratio;e&&this._startTime<this._timeline._time&&(this._startTime=this._timeline._time,this._uncache(!1),this._gc?this._enabled(!0,!1):this._timeline.insert(this,this._startTime-this._delay));for(s in t)this.vars[s]=t[s];if(this._initted)if(e)this._initted=!1;else if(this._gc&&this._enabled(!0,!1),this._notifyPluginsOfEnabled&&this._firstPT&&i._onPluginEvent("_onDisable",this),this._time/this._duration>.998){var n=this._time;this.render(0,!0,!1),this._initted=!1,this.render(n,!0,!1)}else if(this._time>0){this._initted=!1,this._init();for(var a,o=1/(1-r),h=this._firstPT;h;)a=h.s+h.c,h.c*=o,h.s=a-h.c,h=h._next}return this},h.render=function(t,e,i){this._initted||0===this._duration&&this.vars.repeat&&this.invalidate();var s,r,a,o,h,_,u,p,c=this._dirty?this.totalDuration():this._totalDuration,f=this._time,m=this._totalTime,d=this._cycle,g=this._duration;if(t>=c?(this._totalTime=c,this._cycle=this._repeat,this._yoyo&&0!==(1&this._cycle)?(this._time=0,this.ratio=this._ease._calcEnd?this._ease.getRatio(0):0):(this._time=g,this.ratio=this._ease._calcEnd?this._ease.getRatio(1):1),this._reversed||(s=!0,r="onComplete"),0===g&&(p=this._rawPrevTime,this._startTime===this._timeline._duration&&(t=0),(0===t||0>p||p===n)&&p!==t&&(i=!0,p>n&&(r="onReverseComplete")),this._rawPrevTime=p=!e||t||this._rawPrevTime===t?t:n)):1e-7>t?(this._totalTime=this._time=this._cycle=0,this.ratio=this._ease._calcEnd?this._ease.getRatio(0):0,(0!==m||0===g&&this._rawPrevTime>0&&this._rawPrevTime!==n)&&(r="onReverseComplete",s=this._reversed),0>t?(this._active=!1,0===g&&(this._rawPrevTime>=0&&(i=!0),this._rawPrevTime=p=!e||t||this._rawPrevTime===t?t:n)):this._initted||(i=!0)):(this._totalTime=this._time=t,0!==this._repeat&&(o=g+this._repeatDelay,this._cycle=this._totalTime/o>>0,0!==this._cycle&&this._cycle===this._totalTime/o&&this._cycle--,this._time=this._totalTime-this._cycle*o,this._yoyo&&0!==(1&this._cycle)&&(this._time=g-this._time),this._time>g?this._time=g:0>this._time&&(this._time=0)),this._easeType?(h=this._time/g,_=this._easeType,u=this._easePower,(1===_||3===_&&h>=.5)&&(h=1-h),3===_&&(h*=2),1===u?h*=h:2===u?h*=h*h:3===u?h*=h*h*h:4===u&&(h*=h*h*h*h),this.ratio=1===_?1-h:2===_?h:.5>this._time/g?h/2:1-h/2):this.ratio=this._ease.getRatio(this._time/g)),f===this._time&&!i&&d===this._cycle)return m!==this._totalTime&&this._onUpdate&&(e||this._onUpdate.apply(this.vars.onUpdateScope||this,this.vars.onUpdateParams||l)),void 0;if(!this._initted){if(this._init(),!this._initted||this._gc)return;this._time&&!s?this.ratio=this._ease.getRatio(this._time/g):s&&this._ease._calcEnd&&(this.ratio=this._ease.getRatio(0===this._time?0:1))}for(this._active||!this._paused&&this._time!==f&&t>=0&&(this._active=!0),0===m&&(this._startAt&&(t>=0?this._startAt.render(t,e,i):r||(r="_dummyGS")),this.vars.onStart&&(0!==this._totalTime||0===g)&&(e||this.vars.onStart.apply(this.vars.onStartScope||this,this.vars.onStartParams||l))),a=this._firstPT;a;)a.f?a.t[a.p](a.c*this.ratio+a.s):a.t[a.p]=a.c*this.ratio+a.s,a=a._next;this._onUpdate&&(0>t&&this._startAt&&this._startTime&&this._startAt.render(t,e,i),e||(this._totalTime!==m||s)&&this._onUpdate.apply(this.vars.onUpdateScope||this,this.vars.onUpdateParams||l)),this._cycle!==d&&(e||this._gc||this.vars.onRepeat&&this.vars.onRepeat.apply(this.vars.onRepeatScope||this,this.vars.onRepeatParams||l)),r&&(this._gc||(0>t&&this._startAt&&!this._onUpdate&&this._startTime&&this._startAt.render(t,e,i),s&&(this._timeline.autoRemoveChildren&&this._enabled(!1,!1),this._active=!1),!e&&this.vars[r]&&this.vars[r].apply(this.vars[r+"Scope"]||this,this.vars[r+"Params"]||l),0===g&&this._rawPrevTime===n&&p!==n&&(this._rawPrevTime=0)))},r.to=function(t,e,i){return new r(t,e,i)},r.from=function(t,e,i){return i.runBackwards=!0,i.immediateRender=0!=i.immediateRender,new r(t,e,i)},r.fromTo=function(t,e,i,s){return s.startAt=i,s.immediateRender=0!=s.immediateRender&&0!=i.immediateRender,new r(t,e,s)},r.staggerTo=r.allTo=function(t,e,n,h,_,u,p){h=h||0;var c,f,m,d,g=n.delay||0,v=[],y=function(){n.onComplete&&n.onComplete.apply(n.onCompleteScope||this,arguments),_.apply(p||this,u||l)};for(o(t)||("string"==typeof t&&(t=i.selector(t)||t),a(t)&&(t=s.call(t,0))),c=t.length,m=0;c>m;m++){f={};for(d in n)f[d]=n[d];f.delay=g,m===c-1&&_&&(f.onComplete=y),v[m]=new r(t[m],e,f),g+=h}return v},r.staggerFrom=r.allFrom=function(t,e,i,s,n,a,o){return i.runBackwards=!0,i.immediateRender=0!=i.immediateRender,r.staggerTo(t,e,i,s,n,a,o)},r.staggerFromTo=r.allFromTo=function(t,e,i,s,n,a,o,h){return s.startAt=i,s.immediateRender=0!=s.immediateRender&&0!=i.immediateRender,r.staggerTo(t,e,s,n,a,o,h)},r.delayedCall=function(t,e,i,s,n){return new r(e,0,{delay:t,onComplete:e,onCompleteParams:i,onCompleteScope:s,onReverseComplete:e,onReverseCompleteParams:i,onReverseCompleteScope:s,immediateRender:!1,useFrames:n,overwrite:0})},r.set=function(t,e){return new r(t,0,e)},r.isTweening=function(t){return i.getTweensOf(t,!0).length>0};var _=function(t,e){for(var s=[],r=0,n=t._first;n;)n instanceof i?s[r++]=n:(e&&(s[r++]=n),s=s.concat(_(n,e)),r=s.length),n=n._next;return s},u=r.getAllTweens=function(e){return _(t._rootTimeline,e).concat(_(t._rootFramesTimeline,e))};r.killAll=function(t,i,s,r){null==i&&(i=!0),null==s&&(s=!0);var n,a,o,h=u(0!=r),l=h.length,_=i&&s&&r;for(o=0;l>o;o++)a=h[o],(_||a instanceof e||(n=a.target===a.vars.onComplete)&&s||i&&!n)&&(t?a.totalTime(a.totalDuration()):a._enabled(!1,!1))},r.killChildTweensOf=function(t,e){if(null!=t){var n,h,l,_,u,p=i._tweenLookup;if("string"==typeof t&&(t=i.selector(t)||t),a(t)&&(t=s.call(t,0)),o(t))for(_=t.length;--_>-1;)r.killChildTweensOf(t[_],e);else{n=[];for(l in p)for(h=p[l].target.parentNode;h;)h===t&&(n=n.concat(p[l].tweens)),h=h.parentNode;for(u=n.length,_=0;u>_;_++)e&&n[_].totalTime(n[_].totalDuration()),n[_]._enabled(!1,!1)}}};var p=function(t,i,s,r){i=i!==!1,s=s!==!1,r=r!==!1;for(var n,a,o=u(r),h=i&&s&&r,l=o.length;--l>-1;)a=o[l],(h||a instanceof e||(n=a.target===a.vars.onComplete)&&s||i&&!n)&&a.paused(t)};return r.pauseAll=function(t,e,i){p(!0,t,e,i)},r.resumeAll=function(t,e,i){p(!1,t,e,i)},r.globalTimeScale=function(e){var s=t._rootTimeline,r=i.ticker.time;return arguments.length?(e=e||n,s._startTime=r-(r-s._startTime)*s._timeScale/e,s=t._rootFramesTimeline,r=i.ticker.frame,s._startTime=r-(r-s._startTime)*s._timeScale/e,s._timeScale=t._rootTimeline._timeScale=e,e):s._timeScale},h.progress=function(t){return arguments.length?this.totalTime(this.duration()*(this._yoyo&&0!==(1&this._cycle)?1-t:t)+this._cycle*(this._duration+this._repeatDelay),!1):this._time/this.duration()},h.totalProgress=function(t){return arguments.length?this.totalTime(this.totalDuration()*t,!1):this._totalTime/this.totalDuration()},h.time=function(t,e){return arguments.length?(this._dirty&&this.totalDuration(),t>this._duration&&(t=this._duration),this._yoyo&&0!==(1&this._cycle)?t=this._duration-t+this._cycle*(this._duration+this._repeatDelay):0!==this._repeat&&(t+=this._cycle*(this._duration+this._repeatDelay)),this.totalTime(t,e)):this._time},h.duration=function(e){return arguments.length?t.prototype.duration.call(this,e):this._duration},h.totalDuration=function(t){return arguments.length?-1===this._repeat?this:this.duration((t-this._repeat*this._repeatDelay)/(this._repeat+1)):(this._dirty&&(this._totalDuration=-1===this._repeat?999999999999:this._duration*(this._repeat+1)+this._repeatDelay*this._repeat,this._dirty=!1),this._totalDuration)},h.repeat=function(t){return arguments.length?(this._repeat=t,this._uncache(!0)):this._repeat},h.repeatDelay=function(t){return arguments.length?(this._repeatDelay=t,this._uncache(!0)):this._repeatDelay},h.yoyo=function(t){return arguments.length?(this._yoyo=t,this):this._yoyo},r},!0),window._gsDefine("TimelineLite",["core.Animation","core.SimpleTimeline","TweenLite"],function(t,e,i){var s=function(t){e.call(this,t),this._labels={},this.autoRemoveChildren=this.vars.autoRemoveChildren===!0,this.smoothChildTiming=this.vars.smoothChildTiming===!0,this._sortChildren=!0,this._onUpdate=this.vars.onUpdate;var i,s,r=this.vars;for(s in r)i=r[s],a(i)&&-1!==i.join("").indexOf("{self}")&&(r[s]=this._swapSelfInParams(i));a(r.tweens)&&this.add(r.tweens,0,r.align,r.stagger)},r=1e-10,n=i._internals.isSelector,a=i._internals.isArray,o=[],h=window._gsDefine.globals,l=function(t){var e,i={};for(e in t)i[e]=t[e];return i},_=function(t,e,i,s){t._timeline.pause(t._startTime),e&&e.apply(s||t._timeline,i||o)},u=o.slice,p=s.prototype=new e;return s.version="1.11.7",p.constructor=s,p.kill()._gc=!1,p.to=function(t,e,s,r){var n=s.repeat&&h.TweenMax||i;return e?this.add(new n(t,e,s),r):this.set(t,s,r)},p.from=function(t,e,s,r){return this.add((s.repeat&&h.TweenMax||i).from(t,e,s),r)},p.fromTo=function(t,e,s,r,n){var a=r.repeat&&h.TweenMax||i;return e?this.add(a.fromTo(t,e,s,r),n):this.set(t,r,n)},p.staggerTo=function(t,e,r,a,o,h,_,p){var c,f=new s({onComplete:h,onCompleteParams:_,onCompleteScope:p,smoothChildTiming:this.smoothChildTiming});for("string"==typeof t&&(t=i.selector(t)||t),n(t)&&(t=u.call(t,0)),a=a||0,c=0;t.length>c;c++)r.startAt&&(r.startAt=l(r.startAt)),f.to(t[c],e,l(r),c*a);return this.add(f,o)},p.staggerFrom=function(t,e,i,s,r,n,a,o){return i.immediateRender=0!=i.immediateRender,i.runBackwards=!0,this.staggerTo(t,e,i,s,r,n,a,o)},p.staggerFromTo=function(t,e,i,s,r,n,a,o,h){return s.startAt=i,s.immediateRender=0!=s.immediateRender&&0!=i.immediateRender,this.staggerTo(t,e,s,r,n,a,o,h)},p.call=function(t,e,s,r){return this.add(i.delayedCall(0,t,e,s),r)},p.set=function(t,e,s){return s=this._parseTimeOrLabel(s,0,!0),null==e.immediateRender&&(e.immediateRender=s===this._time&&!this._paused),this.add(new i(t,0,e),s)},s.exportRoot=function(t,e){t=t||{},null==t.smoothChildTiming&&(t.smoothChildTiming=!0);var r,n,a=new s(t),o=a._timeline;for(null==e&&(e=!0),o._remove(a,!0),a._startTime=0,a._rawPrevTime=a._time=a._totalTime=o._time,r=o._first;r;)n=r._next,e&&r instanceof i&&r.target===r.vars.onComplete||a.add(r,r._startTime-r._delay),r=n;return o.add(a,0),a},p.add=function(r,n,o,h){var l,_,u,p,c,f;if("number"!=typeof n&&(n=this._parseTimeOrLabel(n,0,!0,r)),!(r instanceof t)){if(r instanceof Array||r&&r.push&&a(r)){for(o=o||"normal",h=h||0,l=n,_=r.length,u=0;_>u;u++)a(p=r[u])&&(p=new s({tweens:p})),this.add(p,l),"string"!=typeof p&&"function"!=typeof p&&("sequence"===o?l=p._startTime+p.totalDuration()/p._timeScale:"start"===o&&(p._startTime-=p.delay())),l+=h;return this._uncache(!0)}if("string"==typeof r)return this.addLabel(r,n);if("function"!=typeof r)throw"Cannot add "+r+" into the timeline; it is not a tween, timeline, function, or string.";r=i.delayedCall(0,r)}if(e.prototype.add.call(this,r,n),(this._gc||this._time===this._duration)&&!this._paused&&this._duration<this.duration())for(c=this,f=c.rawTime()>r._startTime;c._timeline;)f&&c._timeline.smoothChildTiming?c.totalTime(c._totalTime,!0):c._gc&&c._enabled(!0,!1),c=c._timeline;return this},p.remove=function(e){if(e instanceof t)return this._remove(e,!1);if(e instanceof Array||e&&e.push&&a(e)){for(var i=e.length;--i>-1;)this.remove(e[i]);return this}return"string"==typeof e?this.removeLabel(e):this.kill(null,e)},p._remove=function(t,i){e.prototype._remove.call(this,t,i);var s=this._last;return s?this._time>s._startTime+s._totalDuration/s._timeScale&&(this._time=this.duration(),this._totalTime=this._totalDuration):this._time=this._totalTime=this._duration=this._totalDuration=0,this},p.append=function(t,e){return this.add(t,this._parseTimeOrLabel(null,e,!0,t))},p.insert=p.insertMultiple=function(t,e,i,s){return this.add(t,e||0,i,s)},p.appendMultiple=function(t,e,i,s){return this.add(t,this._parseTimeOrLabel(null,e,!0,t),i,s)},p.addLabel=function(t,e){return this._labels[t]=this._parseTimeOrLabel(e),this},p.addPause=function(t,e,i,s){return this.call(_,["{self}",e,i,s],this,t)},p.removeLabel=function(t){return delete this._labels[t],this},p.getLabelTime=function(t){return null!=this._labels[t]?this._labels[t]:-1},p._parseTimeOrLabel=function(e,i,s,r){var n;if(r instanceof t&&r.timeline===this)this.remove(r);else if(r&&(r instanceof Array||r.push&&a(r)))for(n=r.length;--n>-1;)r[n]instanceof t&&r[n].timeline===this&&this.remove(r[n]);if("string"==typeof i)return this._parseTimeOrLabel(i,s&&"number"==typeof e&&null==this._labels[i]?e-this.duration():0,s);if(i=i||0,"string"!=typeof e||!isNaN(e)&&null==this._labels[e])null==e&&(e=this.duration());else{if(n=e.indexOf("="),-1===n)return null==this._labels[e]?s?this._labels[e]=this.duration()+i:i:this._labels[e]+i;i=parseInt(e.charAt(n-1)+"1",10)*Number(e.substr(n+1)),e=n>1?this._parseTimeOrLabel(e.substr(0,n-1),0,s):this.duration()}return Number(e)+i},p.seek=function(t,e){return this.totalTime("number"==typeof t?t:this._parseTimeOrLabel(t),e!==!1)},p.stop=function(){return this.paused(!0)},p.gotoAndPlay=function(t,e){return this.play(t,e)},p.gotoAndStop=function(t,e){return this.pause(t,e)},p.render=function(t,e,i){this._gc&&this._enabled(!0,!1);var s,n,a,h,l,_=this._dirty?this.totalDuration():this._totalDuration,u=this._time,p=this._startTime,c=this._timeScale,f=this._paused;if(t>=_?(this._totalTime=this._time=_,this._reversed||this._hasPausedChild()||(n=!0,h="onComplete",0===this._duration&&(0===t||0>this._rawPrevTime||this._rawPrevTime===r)&&this._rawPrevTime!==t&&this._first&&(l=!0,this._rawPrevTime>r&&(h="onReverseComplete"))),this._rawPrevTime=this._duration||!e||t||this._rawPrevTime===t?t:r,t=_+1e-4):1e-7>t?(this._totalTime=this._time=0,(0!==u||0===this._duration&&this._rawPrevTime!==r&&(this._rawPrevTime>0||0>t&&this._rawPrevTime>=0))&&(h="onReverseComplete",n=this._reversed),0>t?(this._active=!1,0===this._duration&&this._rawPrevTime>=0&&this._first&&(l=!0),this._rawPrevTime=t):(this._rawPrevTime=this._duration||!e||t||this._rawPrevTime===t?t:r,t=0,this._initted||(l=!0))):this._totalTime=this._time=this._rawPrevTime=t,this._time!==u&&this._first||i||l){if(this._initted||(this._initted=!0),this._active||!this._paused&&this._time!==u&&t>0&&(this._active=!0),0===u&&this.vars.onStart&&0!==this._time&&(e||this.vars.onStart.apply(this.vars.onStartScope||this,this.vars.onStartParams||o)),this._time>=u)for(s=this._first;s&&(a=s._next,!this._paused||f);)(s._active||s._startTime<=this._time&&!s._paused&&!s._gc)&&(s._reversed?s.render((s._dirty?s.totalDuration():s._totalDuration)-(t-s._startTime)*s._timeScale,e,i):s.render((t-s._startTime)*s._timeScale,e,i)),s=a;else for(s=this._last;s&&(a=s._prev,!this._paused||f);)(s._active||u>=s._startTime&&!s._paused&&!s._gc)&&(s._reversed?s.render((s._dirty?s.totalDuration():s._totalDuration)-(t-s._startTime)*s._timeScale,e,i):s.render((t-s._startTime)*s._timeScale,e,i)),s=a;this._onUpdate&&(e||this._onUpdate.apply(this.vars.onUpdateScope||this,this.vars.onUpdateParams||o)),h&&(this._gc||(p===this._startTime||c!==this._timeScale)&&(0===this._time||_>=this.totalDuration())&&(n&&(this._timeline.autoRemoveChildren&&this._enabled(!1,!1),this._active=!1),!e&&this.vars[h]&&this.vars[h].apply(this.vars[h+"Scope"]||this,this.vars[h+"Params"]||o)))}},p._hasPausedChild=function(){for(var t=this._first;t;){if(t._paused||t instanceof s&&t._hasPausedChild())return!0;t=t._next}return!1},p.getChildren=function(t,e,s,r){r=r||-9999999999;for(var n=[],a=this._first,o=0;a;)r>a._startTime||(a instanceof i?e!==!1&&(n[o++]=a):(s!==!1&&(n[o++]=a),t!==!1&&(n=n.concat(a.getChildren(!0,e,s)),o=n.length))),a=a._next;return n},p.getTweensOf=function(t,e){for(var s=i.getTweensOf(t),r=s.length,n=[],a=0;--r>-1;)(s[r].timeline===this||e&&this._contains(s[r]))&&(n[a++]=s[r]);return n},p._contains=function(t){for(var e=t.timeline;e;){if(e===this)return!0;e=e.timeline}return!1},p.shiftChildren=function(t,e,i){i=i||0;for(var s,r=this._first,n=this._labels;r;)r._startTime>=i&&(r._startTime+=t),r=r._next;if(e)for(s in n)n[s]>=i&&(n[s]+=t);return this._uncache(!0)},p._kill=function(t,e){if(!t&&!e)return this._enabled(!1,!1);for(var i=e?this.getTweensOf(e):this.getChildren(!0,!0,!1),s=i.length,r=!1;--s>-1;)i[s]._kill(t,e)&&(r=!0);return r},p.clear=function(t){var e=this.getChildren(!1,!0,!0),i=e.length;for(this._time=this._totalTime=0;--i>-1;)e[i]._enabled(!1,!1);return t!==!1&&(this._labels={}),this._uncache(!0)},p.invalidate=function(){for(var t=this._first;t;)t.invalidate(),t=t._next;return this},p._enabled=function(t,i){if(t===this._gc)for(var s=this._first;s;)s._enabled(t,!0),s=s._next;return e.prototype._enabled.call(this,t,i)},p.duration=function(t){return arguments.length?(0!==this.duration()&&0!==t&&this.timeScale(this._duration/t),this):(this._dirty&&this.totalDuration(),this._duration)},p.totalDuration=function(t){if(!arguments.length){if(this._dirty){for(var e,i,s=0,r=this._last,n=999999999999;r;)e=r._prev,r._dirty&&r.totalDuration(),r._startTime>n&&this._sortChildren&&!r._paused?this.add(r,r._startTime-r._delay):n=r._startTime,0>r._startTime&&!r._paused&&(s-=r._startTime,this._timeline.smoothChildTiming&&(this._startTime+=r._startTime/this._timeScale),this.shiftChildren(-r._startTime,!1,-9999999999),n=0),i=r._startTime+r._totalDuration/r._timeScale,i>s&&(s=i),r=e;this._duration=this._totalDuration=s,this._dirty=!1}return this._totalDuration}return 0!==this.totalDuration()&&0!==t&&this.timeScale(this._totalDuration/t),this},p.usesFrames=function(){for(var e=this._timeline;e._timeline;)e=e._timeline;return e===t._rootFramesTimeline},p.rawTime=function(){return this._paused?this._totalTime:(this._timeline.rawTime()-this._startTime)*this._timeScale},s},!0),window._gsDefine("TimelineMax",["TimelineLite","TweenLite","easing.Ease"],function(t,e,i){var s=function(e){t.call(this,e),this._repeat=this.vars.repeat||0,this._repeatDelay=this.vars.repeatDelay||0,this._cycle=0,this._yoyo=this.vars.yoyo===!0,this._dirty=!0},r=1e-10,n=[],a=new i(null,null,1,0),o=s.prototype=new t;return o.constructor=s,o.kill()._gc=!1,s.version="1.11.7",o.invalidate=function(){return this._yoyo=this.vars.yoyo===!0,this._repeat=this.vars.repeat||0,this._repeatDelay=this.vars.repeatDelay||0,this._uncache(!0),t.prototype.invalidate.call(this)},o.addCallback=function(t,i,s,r){return this.add(e.delayedCall(0,t,s,r),i)},o.removeCallback=function(t,e){if(t)if(null==e)this._kill(null,t);else for(var i=this.getTweensOf(t,!1),s=i.length,r=this._parseTimeOrLabel(e);--s>-1;)i[s]._startTime===r&&i[s]._enabled(!1,!1);return this},o.tweenTo=function(t,i){i=i||{};var s,r,o,h={ease:a,overwrite:2,useFrames:this.usesFrames(),immediateRender:!1};for(r in i)h[r]=i[r];return h.time=this._parseTimeOrLabel(t),s=Math.abs(Number(h.time)-this._time)/this._timeScale||.001,o=new e(this,s,h),h.onStart=function(){o.target.paused(!0),o.vars.time!==o.target.time()&&s===o.duration()&&o.duration(Math.abs(o.vars.time-o.target.time())/o.target._timeScale),i.onStart&&i.onStart.apply(i.onStartScope||o,i.onStartParams||n)},o},o.tweenFromTo=function(t,e,i){i=i||{},t=this._parseTimeOrLabel(t),i.startAt={onComplete:this.seek,onCompleteParams:[t],onCompleteScope:this},i.immediateRender=i.immediateRender!==!1;var s=this.tweenTo(e,i);return s.duration(Math.abs(s.vars.time-t)/this._timeScale||.001)},o.render=function(t,e,i){this._gc&&this._enabled(!0,!1);var s,a,o,h,l,_,u=this._dirty?this.totalDuration():this._totalDuration,p=this._duration,c=this._time,f=this._totalTime,m=this._startTime,d=this._timeScale,g=this._rawPrevTime,v=this._paused,y=this._cycle;if(t>=u?(this._locked||(this._totalTime=u,this._cycle=this._repeat),this._reversed||this._hasPausedChild()||(a=!0,h="onComplete",0===this._duration&&(0===t||0>g||g===r)&&g!==t&&this._first&&(l=!0,g>r&&(h="onReverseComplete"))),this._rawPrevTime=this._duration||!e||t||this._rawPrevTime===t?t:r,this._yoyo&&0!==(1&this._cycle)?this._time=t=0:(this._time=p,t=p+1e-4)):1e-7>t?(this._locked||(this._totalTime=this._cycle=0),this._time=0,(0!==c||0===p&&g!==r&&(g>0||0>t&&g>=0)&&!this._locked)&&(h="onReverseComplete",a=this._reversed),0>t?(this._active=!1,0===p&&g>=0&&this._first&&(l=!0),this._rawPrevTime=t):(this._rawPrevTime=p||!e||t||this._rawPrevTime===t?t:r,t=0,this._initted||(l=!0))):(0===p&&0>g&&(l=!0),this._time=this._rawPrevTime=t,this._locked||(this._totalTime=t,0!==this._repeat&&(_=p+this._repeatDelay,this._cycle=this._totalTime/_>>0,0!==this._cycle&&this._cycle===this._totalTime/_&&this._cycle--,this._time=this._totalTime-this._cycle*_,this._yoyo&&0!==(1&this._cycle)&&(this._time=p-this._time),this._time>p?(this._time=p,t=p+1e-4):0>this._time?this._time=t=0:t=this._time))),this._cycle!==y&&!this._locked){var T=this._yoyo&&0!==(1&y),w=T===(this._yoyo&&0!==(1&this._cycle)),x=this._totalTime,b=this._cycle,P=this._rawPrevTime,S=this._time;if(this._totalTime=y*p,y>this._cycle?T=!T:this._totalTime+=p,this._time=c,this._rawPrevTime=0===p?g-1e-4:g,this._cycle=y,this._locked=!0,c=T?0:p,this.render(c,e,0===p),e||this._gc||this.vars.onRepeat&&this.vars.onRepeat.apply(this.vars.onRepeatScope||this,this.vars.onRepeatParams||n),w&&(c=T?p+1e-4:-1e-4,this.render(c,!0,!1)),this._locked=!1,this._paused&&!v)return;this._time=S,this._totalTime=x,this._cycle=b,this._rawPrevTime=P}if(!(this._time!==c&&this._first||i||l))return f!==this._totalTime&&this._onUpdate&&(e||this._onUpdate.apply(this.vars.onUpdateScope||this,this.vars.onUpdateParams||n)),void 0;if(this._initted||(this._initted=!0),this._active||!this._paused&&this._totalTime!==f&&t>0&&(this._active=!0),0===f&&this.vars.onStart&&0!==this._totalTime&&(e||this.vars.onStart.apply(this.vars.onStartScope||this,this.vars.onStartParams||n)),this._time>=c)for(s=this._first;s&&(o=s._next,!this._paused||v);)(s._active||s._startTime<=this._time&&!s._paused&&!s._gc)&&(s._reversed?s.render((s._dirty?s.totalDuration():s._totalDuration)-(t-s._startTime)*s._timeScale,e,i):s.render((t-s._startTime)*s._timeScale,e,i)),s=o;else for(s=this._last;s&&(o=s._prev,!this._paused||v);)(s._active||c>=s._startTime&&!s._paused&&!s._gc)&&(s._reversed?s.render((s._dirty?s.totalDuration():s._totalDuration)-(t-s._startTime)*s._timeScale,e,i):s.render((t-s._startTime)*s._timeScale,e,i)),s=o;this._onUpdate&&(e||this._onUpdate.apply(this.vars.onUpdateScope||this,this.vars.onUpdateParams||n)),h&&(this._locked||this._gc||(m===this._startTime||d!==this._timeScale)&&(0===this._time||u>=this.totalDuration())&&(a&&(this._timeline.autoRemoveChildren&&this._enabled(!1,!1),this._active=!1),!e&&this.vars[h]&&this.vars[h].apply(this.vars[h+"Scope"]||this,this.vars[h+"Params"]||n)))},o.getActive=function(t,e,i){null==t&&(t=!0),null==e&&(e=!0),null==i&&(i=!1);var s,r,n=[],a=this.getChildren(t,e,i),o=0,h=a.length;for(s=0;h>s;s++)r=a[s],r.isActive()&&(n[o++]=r);return n},o.getLabelAfter=function(t){t||0!==t&&(t=this._time);var e,i=this.getLabelsArray(),s=i.length;for(e=0;s>e;e++)if(i[e].time>t)return i[e].name;return null},o.getLabelBefore=function(t){null==t&&(t=this._time);for(var e=this.getLabelsArray(),i=e.length;--i>-1;)if(t>e[i].time)return e[i].name;return null},o.getLabelsArray=function(){var t,e=[],i=0;for(t in this._labels)e[i++]={time:this._labels[t],name:t};return e.sort(function(t,e){return t.time-e.time}),e},o.progress=function(t){return arguments.length?this.totalTime(this.duration()*(this._yoyo&&0!==(1&this._cycle)?1-t:t)+this._cycle*(this._duration+this._repeatDelay),!1):this._time/this.duration()},o.totalProgress=function(t){return arguments.length?this.totalTime(this.totalDuration()*t,!1):this._totalTime/this.totalDuration()},o.totalDuration=function(e){return arguments.length?-1===this._repeat?this:this.duration((e-this._repeat*this._repeatDelay)/(this._repeat+1)):(this._dirty&&(t.prototype.totalDuration.call(this),this._totalDuration=-1===this._repeat?999999999999:this._duration*(this._repeat+1)+this._repeatDelay*this._repeat),this._totalDuration)},o.time=function(t,e){return arguments.length?(this._dirty&&this.totalDuration(),t>this._duration&&(t=this._duration),this._yoyo&&0!==(1&this._cycle)?t=this._duration-t+this._cycle*(this._duration+this._repeatDelay):0!==this._repeat&&(t+=this._cycle*(this._duration+this._repeatDelay)),this.totalTime(t,e)):this._time},o.repeat=function(t){return arguments.length?(this._repeat=t,this._uncache(!0)):this._repeat},o.repeatDelay=function(t){return arguments.length?(this._repeatDelay=t,this._uncache(!0)):this._repeatDelay},o.yoyo=function(t){return arguments.length?(this._yoyo=t,this):this._yoyo},o.currentLabel=function(t){return arguments.length?this.seek(t,!0):this.getLabelBefore(this._time+1e-8)},s},!0),function(){var t=180/Math.PI,e=[],i=[],s=[],r={},n=function(t,e,i,s){this.a=t,this.b=e,this.c=i,this.d=s,this.da=s-t,this.ca=i-t,this.ba=e-t},a=",x,y,z,left,top,right,bottom,marginTop,marginLeft,marginRight,marginBottom,paddingLeft,paddingTop,paddingRight,paddingBottom,backgroundPosition,backgroundPosition_y,",o=function(t,e,i,s){var r={a:t},n={},a={},o={c:s},h=(t+e)/2,l=(e+i)/2,_=(i+s)/2,u=(h+l)/2,p=(l+_)/2,c=(p-u)/8;return r.b=h+(t-h)/4,n.b=u+c,r.c=n.a=(r.b+n.b)/2,n.c=a.a=(u+p)/2,a.b=p-c,o.b=_+(s-_)/4,a.c=o.a=(a.b+o.b)/2,[r,n,a,o]},h=function(t,r,n,a,h){var l,_,u,p,c,f,m,d,g,v,y,T,w,x=t.length-1,b=0,P=t[0].a;for(l=0;x>l;l++)c=t[b],_=c.a,u=c.d,p=t[b+1].d,h?(y=e[l],T=i[l],w=.25*(T+y)*r/(a?.5:s[l]||.5),f=u-(u-_)*(a?.5*r:0!==y?w/y:0),m=u+(p-u)*(a?.5*r:0!==T?w/T:0),d=u-(f+((m-f)*(3*y/(y+T)+.5)/4||0))):(f=u-.5*(u-_)*r,m=u+.5*(p-u)*r,d=u-(f+m)/2),f+=d,m+=d,c.c=g=f,c.b=0!==l?P:P=c.a+.6*(c.c-c.a),c.da=u-_,c.ca=g-_,c.ba=P-_,n?(v=o(_,P,g,u),t.splice(b,1,v[0],v[1],v[2],v[3]),b+=4):b++,P=m;c=t[b],c.b=P,c.c=P+.4*(c.d-P),c.da=c.d-c.a,c.ca=c.c-c.a,c.ba=P-c.a,n&&(v=o(c.a,P,c.c,c.d),t.splice(b,1,v[0],v[1],v[2],v[3]))},l=function(t,s,r,a){var o,h,l,_,u,p,c=[];if(a)for(t=[a].concat(t),h=t.length;--h>-1;)"string"==typeof(p=t[h][s])&&"="===p.charAt(1)&&(t[h][s]=a[s]+Number(p.charAt(0)+p.substr(2)));if(o=t.length-2,0>o)return c[0]=new n(t[0][s],0,0,t[-1>o?0:1][s]),c;for(h=0;o>h;h++)l=t[h][s],_=t[h+1][s],c[h]=new n(l,0,0,_),r&&(u=t[h+2][s],e[h]=(e[h]||0)+(_-l)*(_-l),i[h]=(i[h]||0)+(u-_)*(u-_));return c[h]=new n(t[h][s],0,0,t[h+1][s]),c},_=function(t,n,o,_,u,p){var c,f,m,d,g,v,y,T,w={},x=[],b=p||t[0];u="string"==typeof u?","+u+",":a,null==n&&(n=1);for(f in t[0])x.push(f);if(t.length>1){for(T=t[t.length-1],y=!0,c=x.length;--c>-1;)if(f=x[c],Math.abs(b[f]-T[f])>.05){y=!1;break}y&&(t=t.concat(),p&&t.unshift(p),t.push(t[1]),p=t[t.length-3])}for(e.length=i.length=s.length=0,c=x.length;--c>-1;)f=x[c],r[f]=-1!==u.indexOf(","+f+","),w[f]=l(t,f,r[f],p);for(c=e.length;--c>-1;)e[c]=Math.sqrt(e[c]),i[c]=Math.sqrt(i[c]);if(!_){for(c=x.length;--c>-1;)if(r[f])for(m=w[x[c]],v=m.length-1,d=0;v>d;d++)g=m[d+1].da/i[d]+m[d].da/e[d],s[d]=(s[d]||0)+g*g;for(c=s.length;--c>-1;)s[c]=Math.sqrt(s[c])}for(c=x.length,d=o?4:1;--c>-1;)f=x[c],m=w[f],h(m,n,o,_,r[f]),y&&(m.splice(0,d),m.splice(m.length-d,d));return w},u=function(t,e,i){e=e||"soft";var s,r,a,o,h,l,_,u,p,c,f,m={},d="cubic"===e?3:2,g="soft"===e,v=[];if(g&&i&&(t=[i].concat(t)),null==t||d+1>t.length)throw"invalid Bezier data";for(p in t[0])v.push(p);for(l=v.length;--l>-1;){for(p=v[l],m[p]=h=[],c=0,u=t.length,_=0;u>_;_++)s=null==i?t[_][p]:"string"==typeof(f=t[_][p])&&"="===f.charAt(1)?i[p]+Number(f.charAt(0)+f.substr(2)):Number(f),g&&_>1&&u-1>_&&(h[c++]=(s+h[c-2])/2),h[c++]=s;for(u=c-d+1,c=0,_=0;u>_;_+=d)s=h[_],r=h[_+1],a=h[_+2],o=2===d?0:h[_+3],h[c++]=f=3===d?new n(s,r,a,o):new n(s,(2*r+s)/3,(2*r+a)/3,a);h.length=c}return m},p=function(t,e,i){for(var s,r,n,a,o,h,l,_,u,p,c,f=1/i,m=t.length;--m>-1;)for(p=t[m],n=p.a,a=p.d-n,o=p.c-n,h=p.b-n,s=r=0,_=1;i>=_;_++)l=f*_,u=1-l,s=r-(r=(l*l*a+3*u*(l*o+u*h))*l),c=m*i+_-1,e[c]=(e[c]||0)+s*s},c=function(t,e){e=e>>0||6;var i,s,r,n,a=[],o=[],h=0,l=0,_=e-1,u=[],c=[];for(i in t)p(t[i],a,e);for(r=a.length,s=0;r>s;s++)h+=Math.sqrt(a[s]),n=s%e,c[n]=h,n===_&&(l+=h,n=s/e>>0,u[n]=c,o[n]=l,h=0,c=[]);return{length:l,lengths:o,segments:u}},f=window._gsDefine.plugin({propName:"bezier",priority:-1,version:"1.3.2",API:2,global:!0,init:function(t,e,i){this._target=t,e instanceof Array&&(e={values:e}),this._func={},this._round={},this._props=[],this._timeRes=null==e.timeResolution?6:parseInt(e.timeResolution,10);var s,r,n,a,o,h=e.values||[],l={},p=h[0],f=e.autoRotate||i.vars.orientToBezier;this._autoRotate=f?f instanceof Array?f:[["x","y","rotation",f===!0?0:Number(f)||0]]:null;for(s in p)this._props.push(s);for(n=this._props.length;--n>-1;)s=this._props[n],this._overwriteProps.push(s),r=this._func[s]="function"==typeof t[s],l[s]=r?t[s.indexOf("set")||"function"!=typeof t["get"+s.substr(3)]?s:"get"+s.substr(3)]():parseFloat(t[s]),o||l[s]!==h[0][s]&&(o=l);if(this._beziers="cubic"!==e.type&&"quadratic"!==e.type&&"soft"!==e.type?_(h,isNaN(e.curviness)?1:e.curviness,!1,"thruBasic"===e.type,e.correlate,o):u(h,e.type,l),this._segCount=this._beziers[s].length,this._timeRes){var m=c(this._beziers,this._timeRes);this._length=m.length,this._lengths=m.lengths,this._segments=m.segments,this._l1=this._li=this._s1=this._si=0,this._l2=this._lengths[0],this._curSeg=this._segments[0],this._s2=this._curSeg[0],this._prec=1/this._curSeg.length}if(f=this._autoRotate)for(this._initialRotations=[],f[0]instanceof Array||(this._autoRotate=f=[f]),n=f.length;--n>-1;){for(a=0;3>a;a++)s=f[n][a],this._func[s]="function"==typeof t[s]?t[s.indexOf("set")||"function"!=typeof t["get"+s.substr(3)]?s:"get"+s.substr(3)]:!1;s=f[n][2],this._initialRotations[n]=this._func[s]?this._func[s].call(this._target):this._target[s]}return this._startRatio=i.vars.runBackwards?1:0,!0},set:function(e){var i,s,r,n,a,o,h,l,_,u,p=this._segCount,c=this._func,f=this._target,m=e!==this._startRatio;if(this._timeRes){if(_=this._lengths,u=this._curSeg,e*=this._length,r=this._li,e>this._l2&&p-1>r){for(l=p-1;l>r&&e>=(this._l2=_[++r]););this._l1=_[r-1],this._li=r,this._curSeg=u=this._segments[r],this._s2=u[this._s1=this._si=0]}else if(this._l1>e&&r>0){for(;r>0&&(this._l1=_[--r])>=e;);0===r&&this._l1>e?this._l1=0:r++,this._l2=_[r],this._li=r,this._curSeg=u=this._segments[r],this._s1=u[(this._si=u.length-1)-1]||0,this._s2=u[this._si]}if(i=r,e-=this._l1,r=this._si,e>this._s2&&u.length-1>r){for(l=u.length-1;l>r&&e>=(this._s2=u[++r]););this._s1=u[r-1],this._si=r}else if(this._s1>e&&r>0){for(;r>0&&(this._s1=u[--r])>=e;);0===r&&this._s1>e?this._s1=0:r++,this._s2=u[r],this._si=r}o=(r+(e-this._s1)/(this._s2-this._s1))*this._prec}else i=0>e?0:e>=1?p-1:p*e>>0,o=(e-i*(1/p))*p;for(s=1-o,r=this._props.length;--r>-1;)n=this._props[r],a=this._beziers[n][i],h=(o*o*a.da+3*s*(o*a.ca+s*a.ba))*o+a.a,this._round[n]&&(h=Math.round(h)),c[n]?f[n](h):f[n]=h;if(this._autoRotate){var d,g,v,y,T,w,x,b=this._autoRotate;for(r=b.length;--r>-1;)n=b[r][2],w=b[r][3]||0,x=b[r][4]===!0?1:t,a=this._beziers[b[r][0]],d=this._beziers[b[r][1]],a&&d&&(a=a[i],d=d[i],g=a.a+(a.b-a.a)*o,y=a.b+(a.c-a.b)*o,g+=(y-g)*o,y+=(a.c+(a.d-a.c)*o-y)*o,v=d.a+(d.b-d.a)*o,T=d.b+(d.c-d.b)*o,v+=(T-v)*o,T+=(d.c+(d.d-d.c)*o-T)*o,h=m?Math.atan2(T-v,y-g)*x+w:this._initialRotations[r],c[n]?f[n](h):f[n]=h)}}}),m=f.prototype;f.bezierThrough=_,f.cubicToQuadratic=o,f._autoCSS=!0,f.quadraticToCubic=function(t,e,i){return new n(t,(2*e+t)/3,(2*e+i)/3,i)},f._cssRegister=function(){var t=window._gsDefine.globals.CSSPlugin;if(t){var e=t._internals,i=e._parseToProxy,s=e._setPluginRatio,r=e.CSSPropTween;e._registerComplexSpecialProp("bezier",{parser:function(t,e,n,a,o,h){e instanceof Array&&(e={values:e}),h=new f;
var l,_,u,p=e.values,c=p.length-1,m=[],d={};if(0>c)return o;for(l=0;c>=l;l++)u=i(t,p[l],a,o,h,c!==l),m[l]=u.end;for(_ in e)d[_]=e[_];return d.values=m,o=new r(t,"bezier",0,0,u.pt,2),o.data=u,o.plugin=h,o.setRatio=s,0===d.autoRotate&&(d.autoRotate=!0),!d.autoRotate||d.autoRotate instanceof Array||(l=d.autoRotate===!0?0:Number(d.autoRotate),d.autoRotate=null!=u.end.left?[["left","top","rotation",l,!1]]:null!=u.end.x?[["x","y","rotation",l,!1]]:!1),d.autoRotate&&(a._transform||a._enableTransforms(!1),u.autoRotate=a._target._gsTransform),h._onInitTween(u.proxy,d,a._tween),o}})}},m._roundProps=function(t,e){for(var i=this._overwriteProps,s=i.length;--s>-1;)(t[i[s]]||t.bezier||t.bezierThrough)&&(this._round[i[s]]=e)},m._kill=function(t){var e,i,s=this._props;for(e in this._beziers)if(e in t)for(delete this._beziers[e],delete this._func[e],i=s.length;--i>-1;)s[i]===e&&s.splice(i,1);return this._super._kill.call(this,t)}}(),window._gsDefine("plugins.CSSPlugin",["plugins.TweenPlugin","TweenLite"],function(t,e){var i,s,r,n,a=function(){t.call(this,"css"),this._overwriteProps.length=0,this.setRatio=a.prototype.setRatio},o={},h=a.prototype=new t("css");h.constructor=a,a.version="1.11.7",a.API=2,a.defaultTransformPerspective=0,a.defaultSkewType="compensated",h="px",a.suffixMap={top:h,right:h,bottom:h,left:h,width:h,height:h,fontSize:h,padding:h,margin:h,perspective:h,lineHeight:""};var l,_,u,p,c,f,m=/(?:\d|\-\d|\.\d|\-\.\d)+/g,d=/(?:\d|\-\d|\.\d|\-\.\d|\+=\d|\-=\d|\+=.\d|\-=\.\d)+/g,g=/(?:\+=|\-=|\-|\b)[\d\-\.]+[a-zA-Z0-9]*(?:%|\b)/gi,v=/[^\d\-\.]/g,y=/(?:\d|\-|\+|=|#|\.)*/g,T=/opacity *= *([^)]*)/,w=/opacity:([^;]*)/,x=/alpha\(opacity *=.+?\)/i,b=/^(rgb|hsl)/,P=/([A-Z])/g,S=/-([a-z])/gi,k=/(^(?:url\(\"|url\())|(?:(\"\))$|\)$)/gi,R=function(t,e){return e.toUpperCase()},A=/(?:Left|Right|Width)/i,C=/(M11|M12|M21|M22)=[\d\-\.e]+/gi,O=/progid\:DXImageTransform\.Microsoft\.Matrix\(.+?\)/i,D=/,(?=[^\)]*(?:\(|$))/gi,M=Math.PI/180,I=180/Math.PI,E={},N=document,F=N.createElement("div"),L=N.createElement("img"),X=a._internals={_specialProps:o},z=navigator.userAgent,U=function(){var t,e=z.indexOf("Android"),i=N.createElement("div");return u=-1!==z.indexOf("Safari")&&-1===z.indexOf("Chrome")&&(-1===e||Number(z.substr(e+8,1))>3),c=u&&6>Number(z.substr(z.indexOf("Version/")+8,1)),p=-1!==z.indexOf("Firefox"),/MSIE ([0-9]{1,}[\.0-9]{0,})/.exec(z)&&(f=parseFloat(RegExp.$1)),i.innerHTML="<a style='top:1px;opacity:.55;'>a</a>",t=i.getElementsByTagName("a")[0],t?/^0.55/.test(t.style.opacity):!1}(),Y=function(t){return T.test("string"==typeof t?t:(t.currentStyle?t.currentStyle.filter:t.style.filter)||"")?parseFloat(RegExp.$1)/100:1},j=function(t){window.console&&console.log(t)},B="",q="",V=function(t,e){e=e||F;var i,s,r=e.style;if(void 0!==r[t])return t;for(t=t.charAt(0).toUpperCase()+t.substr(1),i=["O","Moz","ms","Ms","Webkit"],s=5;--s>-1&&void 0===r[i[s]+t];);return s>=0?(q=3===s?"ms":i[s],B="-"+q.toLowerCase()+"-",q+t):null},W=N.defaultView?N.defaultView.getComputedStyle:function(){},G=a.getStyle=function(t,e,i,s,r){var n;return U||"opacity"!==e?(!s&&t.style[e]?n=t.style[e]:(i=i||W(t,null))?n=i[e]||i.getPropertyValue(e)||i.getPropertyValue(e.replace(P,"-$1").toLowerCase()):t.currentStyle&&(n=t.currentStyle[e]),null==r||n&&"none"!==n&&"auto"!==n&&"auto auto"!==n?n:r):Y(t)},$=X.convertToPixels=function(t,i,s,r,n){if("px"===r||!r)return s;if("auto"===r||!s)return 0;var o,h,l,_=A.test(i),u=t,p=F.style,c=0>s;if(c&&(s=-s),"%"===r&&-1!==i.indexOf("border"))o=s/100*(_?t.clientWidth:t.clientHeight);else{if(p.cssText="border:0 solid red;position:"+G(t,"position")+";line-height:0;","%"!==r&&u.appendChild)p[_?"borderLeftWidth":"borderTopWidth"]=s+r;else{if(u=t.parentNode||N.body,h=u._gsCache,l=e.ticker.frame,h&&_&&h.time===l)return h.width*s/100;p[_?"width":"height"]=s+r}u.appendChild(F),o=parseFloat(F[_?"offsetWidth":"offsetHeight"]),u.removeChild(F),_&&"%"===r&&a.cacheWidths!==!1&&(h=u._gsCache=u._gsCache||{},h.time=l,h.width=100*(o/s)),0!==o||n||(o=$(t,i,s,r,!0))}return c?-o:o},Z=X.calculateOffset=function(t,e,i){if("absolute"!==G(t,"position",i))return 0;var s="left"===e?"Left":"Top",r=G(t,"margin"+s,i);return t["offset"+s]-($(t,e,parseFloat(r),r.replace(y,""))||0)},Q=function(t,e){var i,s,r={};if(e=e||W(t,null))if(i=e.length)for(;--i>-1;)r[e[i].replace(S,R)]=e.getPropertyValue(e[i]);else for(i in e)r[i]=e[i];else if(e=t.currentStyle||t.style)for(i in e)"string"==typeof i&&void 0===r[i]&&(r[i.replace(S,R)]=e[i]);return U||(r.opacity=Y(t)),s=Pe(t,e,!1),r.rotation=s.rotation,r.skewX=s.skewX,r.scaleX=s.scaleX,r.scaleY=s.scaleY,r.x=s.x,r.y=s.y,xe&&(r.z=s.z,r.rotationX=s.rotationX,r.rotationY=s.rotationY,r.scaleZ=s.scaleZ),r.filters&&delete r.filters,r},H=function(t,e,i,s,r){var n,a,o,h={},l=t.style;for(a in i)"cssText"!==a&&"length"!==a&&isNaN(a)&&(e[a]!==(n=i[a])||r&&r[a])&&-1===a.indexOf("Origin")&&("number"==typeof n||"string"==typeof n)&&(h[a]="auto"!==n||"left"!==a&&"top"!==a?""!==n&&"auto"!==n&&"none"!==n||"string"!=typeof e[a]||""===e[a].replace(v,"")?n:0:Z(t,a),void 0!==l[a]&&(o=new ue(l,a,l[a],o)));if(s)for(a in s)"className"!==a&&(h[a]=s[a]);return{difs:h,firstMPT:o}},K={width:["Left","Right"],height:["Top","Bottom"]},J=["marginLeft","marginRight","marginTop","marginBottom"],te=function(t,e,i){var s=parseFloat("width"===e?t.offsetWidth:t.offsetHeight),r=K[e],n=r.length;for(i=i||W(t,null);--n>-1;)s-=parseFloat(G(t,"padding"+r[n],i,!0))||0,s-=parseFloat(G(t,"border"+r[n]+"Width",i,!0))||0;return s},ee=function(t,e){(null==t||""===t||"auto"===t||"auto auto"===t)&&(t="0 0");var i=t.split(" "),s=-1!==t.indexOf("left")?"0%":-1!==t.indexOf("right")?"100%":i[0],r=-1!==t.indexOf("top")?"0%":-1!==t.indexOf("bottom")?"100%":i[1];return null==r?r="0":"center"===r&&(r="50%"),("center"===s||isNaN(parseFloat(s))&&-1===(s+"").indexOf("="))&&(s="50%"),e&&(e.oxp=-1!==s.indexOf("%"),e.oyp=-1!==r.indexOf("%"),e.oxr="="===s.charAt(1),e.oyr="="===r.charAt(1),e.ox=parseFloat(s.replace(v,"")),e.oy=parseFloat(r.replace(v,""))),s+" "+r+(i.length>2?" "+i[2]:"")},ie=function(t,e){return"string"==typeof t&&"="===t.charAt(1)?parseInt(t.charAt(0)+"1",10)*parseFloat(t.substr(2)):parseFloat(t)-parseFloat(e)},se=function(t,e){return null==t?e:"string"==typeof t&&"="===t.charAt(1)?parseInt(t.charAt(0)+"1",10)*Number(t.substr(2))+e:parseFloat(t)},re=function(t,e,i,s){var r,n,a,o,h=1e-6;return null==t?o=e:"number"==typeof t?o=t:(r=360,n=t.split("_"),a=Number(n[0].replace(v,""))*(-1===t.indexOf("rad")?1:I)-("="===t.charAt(1)?0:e),n.length&&(s&&(s[i]=e+a),-1!==t.indexOf("short")&&(a%=r,a!==a%(r/2)&&(a=0>a?a+r:a-r)),-1!==t.indexOf("_cw")&&0>a?a=(a+9999999999*r)%r-(0|a/r)*r:-1!==t.indexOf("ccw")&&a>0&&(a=(a-9999999999*r)%r-(0|a/r)*r)),o=e+a),h>o&&o>-h&&(o=0),o},ne={aqua:[0,255,255],lime:[0,255,0],silver:[192,192,192],black:[0,0,0],maroon:[128,0,0],teal:[0,128,128],blue:[0,0,255],navy:[0,0,128],white:[255,255,255],fuchsia:[255,0,255],olive:[128,128,0],yellow:[255,255,0],orange:[255,165,0],gray:[128,128,128],purple:[128,0,128],green:[0,128,0],red:[255,0,0],pink:[255,192,203],cyan:[0,255,255],transparent:[255,255,255,0]},ae=function(t,e,i){return t=0>t?t+1:t>1?t-1:t,0|255*(1>6*t?e+6*(i-e)*t:.5>t?i:2>3*t?e+6*(i-e)*(2/3-t):e)+.5},oe=function(t){var e,i,s,r,n,a;return t&&""!==t?"number"==typeof t?[t>>16,255&t>>8,255&t]:(","===t.charAt(t.length-1)&&(t=t.substr(0,t.length-1)),ne[t]?ne[t]:"#"===t.charAt(0)?(4===t.length&&(e=t.charAt(1),i=t.charAt(2),s=t.charAt(3),t="#"+e+e+i+i+s+s),t=parseInt(t.substr(1),16),[t>>16,255&t>>8,255&t]):"hsl"===t.substr(0,3)?(t=t.match(m),r=Number(t[0])%360/360,n=Number(t[1])/100,a=Number(t[2])/100,i=.5>=a?a*(n+1):a+n-a*n,e=2*a-i,t.length>3&&(t[3]=Number(t[3])),t[0]=ae(r+1/3,e,i),t[1]=ae(r,e,i),t[2]=ae(r-1/3,e,i),t):(t=t.match(m)||ne.transparent,t[0]=Number(t[0]),t[1]=Number(t[1]),t[2]=Number(t[2]),t.length>3&&(t[3]=Number(t[3])),t)):ne.black},he="(?:\\b(?:(?:rgb|rgba|hsl|hsla)\\(.+?\\))|\\B#.+?\\b";for(h in ne)he+="|"+h+"\\b";he=RegExp(he+")","gi");var le=function(t,e,i,s){if(null==t)return function(t){return t};var r,n=e?(t.match(he)||[""])[0]:"",a=t.split(n).join("").match(g)||[],o=t.substr(0,t.indexOf(a[0])),h=")"===t.charAt(t.length-1)?")":"",l=-1!==t.indexOf(" ")?" ":",",_=a.length,u=_>0?a[0].replace(m,""):"";return _?r=e?function(t){var e,p,c,f;if("number"==typeof t)t+=u;else if(s&&D.test(t)){for(f=t.replace(D,"|").split("|"),c=0;f.length>c;c++)f[c]=r(f[c]);return f.join(",")}if(e=(t.match(he)||[n])[0],p=t.split(e).join("").match(g)||[],c=p.length,_>c--)for(;_>++c;)p[c]=i?p[0|(c-1)/2]:a[c];return o+p.join(l)+l+e+h+(-1!==t.indexOf("inset")?" inset":"")}:function(t){var e,n,p;if("number"==typeof t)t+=u;else if(s&&D.test(t)){for(n=t.replace(D,"|").split("|"),p=0;n.length>p;p++)n[p]=r(n[p]);return n.join(",")}if(e=t.match(g)||[],p=e.length,_>p--)for(;_>++p;)e[p]=i?e[0|(p-1)/2]:a[p];return o+e.join(l)+h}:function(t){return t}},_e=function(t){return t=t.split(","),function(e,i,s,r,n,a,o){var h,l=(i+"").split(" ");for(o={},h=0;4>h;h++)o[t[h]]=l[h]=l[h]||l[(h-1)/2>>0];return r.parse(e,o,n,a)}},ue=(X._setPluginRatio=function(t){this.plugin.setRatio(t);for(var e,i,s,r,n=this.data,a=n.proxy,o=n.firstMPT,h=1e-6;o;)e=a[o.v],o.r?e=Math.round(e):h>e&&e>-h&&(e=0),o.t[o.p]=e,o=o._next;if(n.autoRotate&&(n.autoRotate.rotation=a.rotation),1===t)for(o=n.firstMPT;o;){if(i=o.t,i.type){if(1===i.type){for(r=i.xs0+i.s+i.xs1,s=1;i.l>s;s++)r+=i["xn"+s]+i["xs"+(s+1)];i.e=r}}else i.e=i.s+i.xs0;o=o._next}},function(t,e,i,s,r){this.t=t,this.p=e,this.v=i,this.r=r,s&&(s._prev=this,this._next=s)}),pe=(X._parseToProxy=function(t,e,i,s,r,n){var a,o,h,l,_,u=s,p={},c={},f=i._transform,m=E;for(i._transform=null,E=e,s=_=i.parse(t,e,s,r),E=m,n&&(i._transform=f,u&&(u._prev=null,u._prev&&(u._prev._next=null)));s&&s!==u;){if(1>=s.type&&(o=s.p,c[o]=s.s+s.c,p[o]=s.s,n||(l=new ue(s,"s",o,l,s.r),s.c=0),1===s.type))for(a=s.l;--a>0;)h="xn"+a,o=s.p+"_"+h,c[o]=s.data[h],p[o]=s[h],n||(l=new ue(s,h,o,l,s.rxp[h]));s=s._next}return{proxy:p,end:c,firstMPT:l,pt:_}},X.CSSPropTween=function(t,e,s,r,a,o,h,l,_,u,p){this.t=t,this.p=e,this.s=s,this.c=r,this.n=h||e,t instanceof pe||n.push(this.n),this.r=l,this.type=o||0,_&&(this.pr=_,i=!0),this.b=void 0===u?s:u,this.e=void 0===p?s+r:p,a&&(this._next=a,a._prev=this)}),ce=a.parseComplex=function(t,e,i,s,r,n,a,o,h,_){i=i||n||"",a=new pe(t,e,0,0,a,_?2:1,null,!1,o,i,s),s+="";var u,p,c,f,g,v,y,T,w,x,P,S,k=i.split(", ").join(",").split(" "),R=s.split(", ").join(",").split(" "),A=k.length,C=l!==!1;for((-1!==s.indexOf(",")||-1!==i.indexOf(","))&&(k=k.join(" ").replace(D,", ").split(" "),R=R.join(" ").replace(D,", ").split(" "),A=k.length),A!==R.length&&(k=(n||"").split(" "),A=k.length),a.plugin=h,a.setRatio=_,u=0;A>u;u++)if(f=k[u],g=R[u],T=parseFloat(f),T||0===T)a.appendXtra("",T,ie(g,T),g.replace(d,""),C&&-1!==g.indexOf("px"),!0);else if(r&&("#"===f.charAt(0)||ne[f]||b.test(f)))S=","===g.charAt(g.length-1)?"),":")",f=oe(f),g=oe(g),w=f.length+g.length>6,w&&!U&&0===g[3]?(a["xs"+a.l]+=a.l?" transparent":"transparent",a.e=a.e.split(R[u]).join("transparent")):(U||(w=!1),a.appendXtra(w?"rgba(":"rgb(",f[0],g[0]-f[0],",",!0,!0).appendXtra("",f[1],g[1]-f[1],",",!0).appendXtra("",f[2],g[2]-f[2],w?",":S,!0),w&&(f=4>f.length?1:f[3],a.appendXtra("",f,(4>g.length?1:g[3])-f,S,!1)));else if(v=f.match(m)){if(y=g.match(d),!y||y.length!==v.length)return a;for(c=0,p=0;v.length>p;p++)P=v[p],x=f.indexOf(P,c),a.appendXtra(f.substr(c,x-c),Number(P),ie(y[p],P),"",C&&"px"===f.substr(x+P.length,2),0===p),c=x+P.length;a["xs"+a.l]+=f.substr(c)}else a["xs"+a.l]+=a.l?" "+f:f;if(-1!==s.indexOf("=")&&a.data){for(S=a.xs0+a.data.s,u=1;a.l>u;u++)S+=a["xs"+u]+a.data["xn"+u];a.e=S+a["xs"+u]}return a.l||(a.type=-1,a.xs0=a.e),a.xfirst||a},fe=9;for(h=pe.prototype,h.l=h.pr=0;--fe>0;)h["xn"+fe]=0,h["xs"+fe]="";h.xs0="",h._next=h._prev=h.xfirst=h.data=h.plugin=h.setRatio=h.rxp=null,h.appendXtra=function(t,e,i,s,r,n){var a=this,o=a.l;return a["xs"+o]+=n&&o?" "+t:t||"",i||0===o||a.plugin?(a.l++,a.type=a.setRatio?2:1,a["xs"+a.l]=s||"",o>0?(a.data["xn"+o]=e+i,a.rxp["xn"+o]=r,a["xn"+o]=e,a.plugin||(a.xfirst=new pe(a,"xn"+o,e,i,a.xfirst||a,0,a.n,r,a.pr),a.xfirst.xs0=0),a):(a.data={s:e+i},a.rxp={},a.s=e,a.c=i,a.r=r,a)):(a["xs"+o]+=e+(s||""),a)};var me=function(t,e){e=e||{},this.p=e.prefix?V(t)||t:t,o[t]=o[this.p]=this,this.format=e.formatter||le(e.defaultValue,e.color,e.collapsible,e.multi),e.parser&&(this.parse=e.parser),this.clrs=e.color,this.multi=e.multi,this.keyword=e.keyword,this.dflt=e.defaultValue,this.pr=e.priority||0},de=X._registerComplexSpecialProp=function(t,e,i){"object"!=typeof e&&(e={parser:i});var s,r,n=t.split(","),a=e.defaultValue;for(i=i||[a],s=0;n.length>s;s++)e.prefix=0===s&&e.prefix,e.defaultValue=i[s]||a,r=new me(n[s],e)},ge=function(t){if(!o[t]){var e=t.charAt(0).toUpperCase()+t.substr(1)+"Plugin";de(t,{parser:function(t,i,s,r,n,a,h){var l=(window.GreenSockGlobals||window).com.greensock.plugins[e];return l?(l._cssRegister(),o[s].parse(t,i,s,r,n,a,h)):(j("Error: "+e+" js file not loaded."),n)}})}};h=me.prototype,h.parseComplex=function(t,e,i,s,r,n){var a,o,h,l,_,u,p=this.keyword;if(this.multi&&(D.test(i)||D.test(e)?(o=e.replace(D,"|").split("|"),h=i.replace(D,"|").split("|")):p&&(o=[e],h=[i])),h){for(l=h.length>o.length?h.length:o.length,a=0;l>a;a++)e=o[a]=o[a]||this.dflt,i=h[a]=h[a]||this.dflt,p&&(_=e.indexOf(p),u=i.indexOf(p),_!==u&&(i=-1===u?h:o,i[a]+=" "+p));e=o.join(", "),i=h.join(", ")}return ce(t,this.p,e,i,this.clrs,this.dflt,s,this.pr,r,n)},h.parse=function(t,e,i,s,n,a){return this.parseComplex(t.style,this.format(G(t,this.p,r,!1,this.dflt)),this.format(e),n,a)},a.registerSpecialProp=function(t,e,i){de(t,{parser:function(t,s,r,n,a,o){var h=new pe(t,r,0,0,a,2,r,!1,i);return h.plugin=o,h.setRatio=e(t,s,n._tween,r),h},priority:i})};var ve="scaleX,scaleY,scaleZ,x,y,z,skewX,rotation,rotationX,rotationY,perspective".split(","),ye=V("transform"),Te=B+"transform",we=V("transformOrigin"),xe=null!==V("perspective"),be=X.Transform=function(){this.skewY=0},Pe=X.getTransform=function(t,e,i,s){if(t._gsTransform&&i&&!s)return t._gsTransform;var r,n,o,h,l,_,u,p,c,f,m,d,g,v=i?t._gsTransform||new be:new be,y=0>v.scaleX,T=2e-5,w=1e5,x=179.99,b=x*M,P=xe?parseFloat(G(t,we,e,!1,"0 0 0").split(" ")[2])||v.zOrigin||0:0;for(ye?r=G(t,Te,e,!0):t.currentStyle&&(r=t.currentStyle.filter.match(C),r=r&&4===r.length?[r[0].substr(4),Number(r[2].substr(4)),Number(r[1].substr(4)),r[3].substr(4),v.x||0,v.y||0].join(","):""),n=(r||"").match(/(?:\-|\b)[\d\-\.e]+\b/gi)||[],o=n.length;--o>-1;)h=Number(n[o]),n[o]=(l=h-(h|=0))?(0|l*w+(0>l?-.5:.5))/w+h:h;if(16===n.length){var S=n[8],k=n[9],R=n[10],A=n[12],O=n[13],D=n[14];if(v.zOrigin&&(D=-v.zOrigin,A=S*D-n[12],O=k*D-n[13],D=R*D+v.zOrigin-n[14]),!i||s||null==v.rotationX){var E,N,F,L,X,z,U,Y=n[0],j=n[1],B=n[2],q=n[3],V=n[4],W=n[5],$=n[6],Z=n[7],Q=n[11],H=Math.atan2($,R),K=-b>H||H>b;v.rotationX=H*I,H&&(L=Math.cos(-H),X=Math.sin(-H),E=V*L+S*X,N=W*L+k*X,F=$*L+R*X,S=V*-X+S*L,k=W*-X+k*L,R=$*-X+R*L,Q=Z*-X+Q*L,V=E,W=N,$=F),H=Math.atan2(S,Y),v.rotationY=H*I,H&&(z=-b>H||H>b,L=Math.cos(-H),X=Math.sin(-H),E=Y*L-S*X,N=j*L-k*X,F=B*L-R*X,k=j*X+k*L,R=B*X+R*L,Q=q*X+Q*L,Y=E,j=N,B=F),H=Math.atan2(j,W),v.rotation=H*I,H&&(U=-b>H||H>b,L=Math.cos(-H),X=Math.sin(-H),Y=Y*L+V*X,N=j*L+W*X,W=j*-X+W*L,$=B*-X+$*L,j=N),U&&K?v.rotation=v.rotationX=0:U&&z?v.rotation=v.rotationY=0:z&&K&&(v.rotationY=v.rotationX=0),v.scaleX=(0|Math.sqrt(Y*Y+j*j)*w+.5)/w,v.scaleY=(0|Math.sqrt(W*W+k*k)*w+.5)/w,v.scaleZ=(0|Math.sqrt($*$+R*R)*w+.5)/w,v.skewX=0,v.perspective=Q?1/(0>Q?-Q:Q):0,v.x=A,v.y=O,v.z=D}}else if(!(xe&&!s&&n.length&&v.x===n[4]&&v.y===n[5]&&(v.rotationX||v.rotationY)||void 0!==v.x&&"none"===G(t,"display",e))){var J=n.length>=6,te=J?n[0]:1,ee=n[1]||0,ie=n[2]||0,se=J?n[3]:1;v.x=n[4]||0,v.y=n[5]||0,_=Math.sqrt(te*te+ee*ee),u=Math.sqrt(se*se+ie*ie),p=te||ee?Math.atan2(ee,te)*I:v.rotation||0,c=ie||se?Math.atan2(ie,se)*I+p:v.skewX||0,f=_-Math.abs(v.scaleX||0),m=u-Math.abs(v.scaleY||0),Math.abs(c)>90&&270>Math.abs(c)&&(y?(_*=-1,c+=0>=p?180:-180,p+=0>=p?180:-180):(u*=-1,c+=0>=c?180:-180)),d=(p-v.rotation)%180,g=(c-v.skewX)%180,(void 0===v.skewX||f>T||-T>f||m>T||-T>m||d>-x&&x>d&&false|d*w||g>-x&&x>g&&false|g*w)&&(v.scaleX=_,v.scaleY=u,v.rotation=p,v.skewX=c),xe&&(v.rotationX=v.rotationY=v.z=0,v.perspective=parseFloat(a.defaultTransformPerspective)||0,v.scaleZ=1)}v.zOrigin=P;for(o in v)T>v[o]&&v[o]>-T&&(v[o]=0);return i&&(t._gsTransform=v),v},Se=function(t){var e,i,s=this.data,r=-s.rotation*M,n=r+s.skewX*M,a=1e5,o=(0|Math.cos(r)*s.scaleX*a)/a,h=(0|Math.sin(r)*s.scaleX*a)/a,l=(0|Math.sin(n)*-s.scaleY*a)/a,_=(0|Math.cos(n)*s.scaleY*a)/a,u=this.t.style,p=this.t.currentStyle;if(p){i=h,h=-l,l=-i,e=p.filter,u.filter="";var c,m,d=this.t.offsetWidth,g=this.t.offsetHeight,v="absolute"!==p.position,w="progid:DXImageTransform.Microsoft.Matrix(M11="+o+", M12="+h+", M21="+l+", M22="+_,x=s.x,b=s.y;if(null!=s.ox&&(c=(s.oxp?.01*d*s.ox:s.ox)-d/2,m=(s.oyp?.01*g*s.oy:s.oy)-g/2,x+=c-(c*o+m*h),b+=m-(c*l+m*_)),v?(c=d/2,m=g/2,w+=", Dx="+(c-(c*o+m*h)+x)+", Dy="+(m-(c*l+m*_)+b)+")"):w+=", sizingMethod='auto expand')",u.filter=-1!==e.indexOf("DXImageTransform.Microsoft.Matrix(")?e.replace(O,w):w+" "+e,(0===t||1===t)&&1===o&&0===h&&0===l&&1===_&&(v&&-1===w.indexOf("Dx=0, Dy=0")||T.test(e)&&100!==parseFloat(RegExp.$1)||-1===e.indexOf("gradient("&&e.indexOf("Alpha"))&&u.removeAttribute("filter")),!v){var P,S,k,R=8>f?1:-1;for(c=s.ieOffsetX||0,m=s.ieOffsetY||0,s.ieOffsetX=Math.round((d-((0>o?-o:o)*d+(0>h?-h:h)*g))/2+x),s.ieOffsetY=Math.round((g-((0>_?-_:_)*g+(0>l?-l:l)*d))/2+b),fe=0;4>fe;fe++)S=J[fe],P=p[S],i=-1!==P.indexOf("px")?parseFloat(P):$(this.t,S,parseFloat(P),P.replace(y,""))||0,k=i!==s[S]?2>fe?-s.ieOffsetX:-s.ieOffsetY:2>fe?c-s.ieOffsetX:m-s.ieOffsetY,u[S]=(s[S]=Math.round(i-k*(0===fe||2===fe?1:R)))+"px"}}},ke=X.set3DTransformRatio=function(){var t,e,i,s,r,n,a,o,h,l,_,u,c,f,m,d,g,v,y,T,w,x,b,P=this.data,S=this.t.style,k=P.rotation*M,R=P.scaleX,A=P.scaleY,C=P.scaleZ,O=P.perspective;if(p){var D=1e-4;D>R&&R>-D&&(R=C=2e-5),D>A&&A>-D&&(A=C=2e-5),!O||P.z||P.rotationX||P.rotationY||(O=0)}if(k||P.skewX)v=Math.cos(k),y=Math.sin(k),t=v,r=y,P.skewX&&(k-=P.skewX*M,v=Math.cos(k),y=Math.sin(k),"simple"===P.skewType&&(T=Math.tan(P.skewX*M),T=Math.sqrt(1+T*T),v*=T,y*=T)),e=-y,n=v;else{if(!(P.rotationY||P.rotationX||1!==C||O))return S[ye]="translate3d("+P.x+"px,"+P.y+"px,"+P.z+"px)"+(1!==R||1!==A?" scale("+R+","+A+")":""),void 0;t=n=1,e=r=0}_=1,i=s=a=o=h=l=u=c=f=0,m=O?-1/O:0,d=P.zOrigin,g=1e5,k=P.rotationY*M,k&&(v=Math.cos(k),y=Math.sin(k),h=_*-y,c=m*-y,i=t*y,a=r*y,_*=v,m*=v,t*=v,r*=v),k=P.rotationX*M,k&&(v=Math.cos(k),y=Math.sin(k),T=e*v+i*y,w=n*v+a*y,x=l*v+_*y,b=f*v+m*y,i=e*-y+i*v,a=n*-y+a*v,_=l*-y+_*v,m=f*-y+m*v,e=T,n=w,l=x,f=b),1!==C&&(i*=C,a*=C,_*=C,m*=C),1!==A&&(e*=A,n*=A,l*=A,f*=A),1!==R&&(t*=R,r*=R,h*=R,c*=R),d&&(u-=d,s=i*u,o=a*u,u=_*u+d),s=(T=(s+=P.x)-(s|=0))?(0|T*g+(0>T?-.5:.5))/g+s:s,o=(T=(o+=P.y)-(o|=0))?(0|T*g+(0>T?-.5:.5))/g+o:o,u=(T=(u+=P.z)-(u|=0))?(0|T*g+(0>T?-.5:.5))/g+u:u,S[ye]="matrix3d("+[(0|t*g)/g,(0|r*g)/g,(0|h*g)/g,(0|c*g)/g,(0|e*g)/g,(0|n*g)/g,(0|l*g)/g,(0|f*g)/g,(0|i*g)/g,(0|a*g)/g,(0|_*g)/g,(0|m*g)/g,s,o,u,O?1+-u/O:1].join(",")+")"},Re=X.set2DTransformRatio=function(t){var e,i,s,r,n,a=this.data,o=this.t,h=o.style;return a.rotationX||a.rotationY||a.z||a.force3D?(this.setRatio=ke,ke.call(this,t),void 0):(a.rotation||a.skewX?(e=a.rotation*M,i=e-a.skewX*M,s=1e5,r=a.scaleX*s,n=a.scaleY*s,h[ye]="matrix("+(0|Math.cos(e)*r)/s+","+(0|Math.sin(e)*r)/s+","+(0|Math.sin(i)*-n)/s+","+(0|Math.cos(i)*n)/s+","+a.x+","+a.y+")"):h[ye]="matrix("+a.scaleX+",0,0,"+a.scaleY+","+a.x+","+a.y+")",void 0)};de("transform,scale,scaleX,scaleY,scaleZ,x,y,z,rotation,rotationX,rotationY,rotationZ,skewX,skewY,shortRotation,shortRotationX,shortRotationY,shortRotationZ,transformOrigin,transformPerspective,directionalRotation,parseTransform,force3D,skewType",{parser:function(t,e,i,s,n,o,h){if(s._transform)return n;var l,_,u,p,c,f,m,d=s._transform=Pe(t,r,!0,h.parseTransform),g=t.style,v=1e-6,y=ve.length,T=h,w={};if("string"==typeof T.transform&&ye)u=g.cssText,g[ye]=T.transform,g.display="block",l=Pe(t,null,!1),g.cssText=u;else if("object"==typeof T){if(l={scaleX:se(null!=T.scaleX?T.scaleX:T.scale,d.scaleX),scaleY:se(null!=T.scaleY?T.scaleY:T.scale,d.scaleY),scaleZ:se(T.scaleZ,d.scaleZ),x:se(T.x,d.x),y:se(T.y,d.y),z:se(T.z,d.z),perspective:se(T.transformPerspective,d.perspective)},m=T.directionalRotation,null!=m)if("object"==typeof m)for(u in m)T[u]=m[u];else T.rotation=m;l.rotation=re("rotation"in T?T.rotation:"shortRotation"in T?T.shortRotation+"_short":"rotationZ"in T?T.rotationZ:d.rotation,d.rotation,"rotation",w),xe&&(l.rotationX=re("rotationX"in T?T.rotationX:"shortRotationX"in T?T.shortRotationX+"_short":d.rotationX||0,d.rotationX,"rotationX",w),l.rotationY=re("rotationY"in T?T.rotationY:"shortRotationY"in T?T.shortRotationY+"_short":d.rotationY||0,d.rotationY,"rotationY",w)),l.skewX=null==T.skewX?d.skewX:re(T.skewX,d.skewX),l.skewY=null==T.skewY?d.skewY:re(T.skewY,d.skewY),(_=l.skewY-d.skewY)&&(l.skewX+=_,l.rotation+=_)}for(xe&&null!=T.force3D&&(d.force3D=T.force3D,f=!0),d.skewType=T.skewType||d.skewType||a.defaultSkewType,c=d.force3D||d.z||d.rotationX||d.rotationY||l.z||l.rotationX||l.rotationY||l.perspective,c||null==T.scale||(l.scaleZ=1);--y>-1;)i=ve[y],p=l[i]-d[i],(p>v||-v>p||null!=E[i])&&(f=!0,n=new pe(d,i,d[i],p,n),i in w&&(n.e=w[i]),n.xs0=0,n.plugin=o,s._overwriteProps.push(n.n));return p=T.transformOrigin,(p||xe&&c&&d.zOrigin)&&(ye?(f=!0,i=we,p=(p||G(t,i,r,!1,"50% 50%"))+"",n=new pe(g,i,0,0,n,-1,"transformOrigin"),n.b=g[i],n.plugin=o,xe?(u=d.zOrigin,p=p.split(" "),d.zOrigin=(p.length>2&&(0===u||"0px"!==p[2])?parseFloat(p[2]):u)||0,n.xs0=n.e=g[i]=p[0]+" "+(p[1]||"50%")+" 0px",n=new pe(d,"zOrigin",0,0,n,-1,n.n),n.b=u,n.xs0=n.e=d.zOrigin):n.xs0=n.e=g[i]=p):ee(p+"",d)),f&&(s._transformType=c||3===this._transformType?3:2),n},prefix:!0}),de("boxShadow",{defaultValue:"0px 0px 0px 0px #999",prefix:!0,color:!0,multi:!0,keyword:"inset"}),de("borderRadius",{defaultValue:"0px",parser:function(t,e,i,n,a){e=this.format(e);var o,h,l,_,u,p,c,f,m,d,g,v,y,T,w,x,b=["borderTopLeftRadius","borderTopRightRadius","borderBottomRightRadius","borderBottomLeftRadius"],P=t.style;for(m=parseFloat(t.offsetWidth),d=parseFloat(t.offsetHeight),o=e.split(" "),h=0;b.length>h;h++)this.p.indexOf("border")&&(b[h]=V(b[h])),u=_=G(t,b[h],r,!1,"0px"),-1!==u.indexOf(" ")&&(_=u.split(" "),u=_[0],_=_[1]),p=l=o[h],c=parseFloat(u),v=u.substr((c+"").length),y="="===p.charAt(1),y?(f=parseInt(p.charAt(0)+"1",10),p=p.substr(2),f*=parseFloat(p),g=p.substr((f+"").length-(0>f?1:0))||""):(f=parseFloat(p),g=p.substr((f+"").length)),""===g&&(g=s[i]||v),g!==v&&(T=$(t,"borderLeft",c,v),w=$(t,"borderTop",c,v),"%"===g?(u=100*(T/m)+"%",_=100*(w/d)+"%"):"em"===g?(x=$(t,"borderLeft",1,"em"),u=T/x+"em",_=w/x+"em"):(u=T+"px",_=w+"px"),y&&(p=parseFloat(u)+f+g,l=parseFloat(_)+f+g)),a=ce(P,b[h],u+" "+_,p+" "+l,!1,"0px",a);return a},prefix:!0,formatter:le("0px 0px 0px 0px",!1,!0)}),de("backgroundPosition",{defaultValue:"0 0",parser:function(t,e,i,s,n,a){var o,h,l,_,u,p,c="background-position",m=r||W(t,null),d=this.format((m?f?m.getPropertyValue(c+"-x")+" "+m.getPropertyValue(c+"-y"):m.getPropertyValue(c):t.currentStyle.backgroundPositionX+" "+t.currentStyle.backgroundPositionY)||"0 0"),g=this.format(e);if(-1!==d.indexOf("%")!=(-1!==g.indexOf("%"))&&(p=G(t,"backgroundImage").replace(k,""),p&&"none"!==p)){for(o=d.split(" "),h=g.split(" "),L.setAttribute("src",p),l=2;--l>-1;)d=o[l],_=-1!==d.indexOf("%"),_!==(-1!==h[l].indexOf("%"))&&(u=0===l?t.offsetWidth-L.width:t.offsetHeight-L.height,o[l]=_?parseFloat(d)/100*u+"px":100*(parseFloat(d)/u)+"%");d=o.join(" ")}return this.parseComplex(t.style,d,g,n,a)},formatter:ee}),de("backgroundSize",{defaultValue:"0 0",formatter:ee}),de("perspective",{defaultValue:"0px",prefix:!0}),de("perspectiveOrigin",{defaultValue:"50% 50%",prefix:!0}),de("transformStyle",{prefix:!0}),de("backfaceVisibility",{prefix:!0}),de("userSelect",{prefix:!0}),de("margin",{parser:_e("marginTop,marginRight,marginBottom,marginLeft")}),de("padding",{parser:_e("paddingTop,paddingRight,paddingBottom,paddingLeft")}),de("clip",{defaultValue:"rect(0px,0px,0px,0px)",parser:function(t,e,i,s,n,a){var o,h,l;return 9>f?(h=t.currentStyle,l=8>f?" ":",",o="rect("+h.clipTop+l+h.clipRight+l+h.clipBottom+l+h.clipLeft+")",e=this.format(e).split(",").join(l)):(o=this.format(G(t,this.p,r,!1,this.dflt)),e=this.format(e)),this.parseComplex(t.style,o,e,n,a)}}),de("textShadow",{defaultValue:"0px 0px 0px #999",color:!0,multi:!0}),de("autoRound,strictUnits",{parser:function(t,e,i,s,r){return r}}),de("border",{defaultValue:"0px solid #000",parser:function(t,e,i,s,n,a){return this.parseComplex(t.style,this.format(G(t,"borderTopWidth",r,!1,"0px")+" "+G(t,"borderTopStyle",r,!1,"solid")+" "+G(t,"borderTopColor",r,!1,"#000")),this.format(e),n,a)},color:!0,formatter:function(t){var e=t.split(" ");return e[0]+" "+(e[1]||"solid")+" "+(t.match(he)||["#000"])[0]}}),de("borderWidth",{parser:_e("borderTopWidth,borderRightWidth,borderBottomWidth,borderLeftWidth")}),de("float,cssFloat,styleFloat",{parser:function(t,e,i,s,r){var n=t.style,a="cssFloat"in n?"cssFloat":"styleFloat";return new pe(n,a,0,0,r,-1,i,!1,0,n[a],e)}});var Ae=function(t){var e,i=this.t,s=i.filter||G(this.data,"filter"),r=0|this.s+this.c*t;100===r&&(-1===s.indexOf("atrix(")&&-1===s.indexOf("radient(")&&-1===s.indexOf("oader(")?(i.removeAttribute("filter"),e=!G(this.data,"filter")):(i.filter=s.replace(x,""),e=!0)),e||(this.xn1&&(i.filter=s=s||"alpha(opacity="+r+")"),-1===s.indexOf("opacity")?0===r&&this.xn1||(i.filter=s+" alpha(opacity="+r+")"):i.filter=s.replace(T,"opacity="+r))};de("opacity,alpha,autoAlpha",{defaultValue:"1",parser:function(t,e,i,s,n,a){var o=parseFloat(G(t,"opacity",r,!1,"1")),h=t.style,l="autoAlpha"===i;return"string"==typeof e&&"="===e.charAt(1)&&(e=("-"===e.charAt(0)?-1:1)*parseFloat(e.substr(2))+o),l&&1===o&&"hidden"===G(t,"visibility",r)&&0!==e&&(o=0),U?n=new pe(h,"opacity",o,e-o,n):(n=new pe(h,"opacity",100*o,100*(e-o),n),n.xn1=l?1:0,h.zoom=1,n.type=2,n.b="alpha(opacity="+n.s+")",n.e="alpha(opacity="+(n.s+n.c)+")",n.data=t,n.plugin=a,n.setRatio=Ae),l&&(n=new pe(h,"visibility",0,0,n,-1,null,!1,0,0!==o?"inherit":"hidden",0===e?"hidden":"inherit"),n.xs0="inherit",s._overwriteProps.push(n.n),s._overwriteProps.push(i)),n}});var Ce=function(t,e){e&&(t.removeProperty?("ms"===e.substr(0,2)&&(e="M"+e.substr(1)),t.removeProperty(e.replace(P,"-$1").toLowerCase())):t.removeAttribute(e))},Oe=function(t){if(this.t._gsClassPT=this,1===t||0===t){this.t.className=0===t?this.b:this.e;for(var e=this.data,i=this.t.style;e;)e.v?i[e.p]=e.v:Ce(i,e.p),e=e._next;1===t&&this.t._gsClassPT===this&&(this.t._gsClassPT=null)}else this.t.className!==this.e&&(this.t.className=this.e)};de("className",{parser:function(t,e,s,n,a,o,h){var l,_,u,p,c,f=t.className,m=t.style.cssText;if(a=n._classNamePT=new pe(t,s,0,0,a,2),a.setRatio=Oe,a.pr=-11,i=!0,a.b=f,_=Q(t,r),u=t._gsClassPT){for(p={},c=u.data;c;)p[c.p]=1,c=c._next;u.setRatio(1)}return t._gsClassPT=a,a.e="="!==e.charAt(1)?e:f.replace(RegExp("\\s*\\b"+e.substr(2)+"\\b"),"")+("+"===e.charAt(0)?" "+e.substr(2):""),n._tween._duration&&(t.className=a.e,l=H(t,_,Q(t),h,p),t.className=f,a.data=l.firstMPT,t.style.cssText=m,a=a.xfirst=n.parse(t,l.difs,a,o)),a}});var De=function(t){if((1===t||0===t)&&this.data._totalTime===this.data._totalDuration&&"isFromStart"!==this.data.data){var e,i,s,r,n=this.t.style,a=o.transform.parse;if("all"===this.e)n.cssText="",r=!0;else for(e=this.e.split(","),s=e.length;--s>-1;)i=e[s],o[i]&&(o[i].parse===a?r=!0:i="transformOrigin"===i?we:o[i].p),Ce(n,i);r&&(Ce(n,ye),this.t._gsTransform&&delete this.t._gsTransform)}};for(de("clearProps",{parser:function(t,e,s,r,n){return n=new pe(t,s,0,0,n,2),n.setRatio=De,n.e=e,n.pr=-10,n.data=r._tween,i=!0,n}}),h="bezier,throwProps,physicsProps,physics2D".split(","),fe=h.length;fe--;)ge(h[fe]);h=a.prototype,h._firstPT=null,h._onInitTween=function(t,e,o){if(!t.nodeType)return!1;this._target=t,this._tween=o,this._vars=e,l=e.autoRound,i=!1,s=e.suffixMap||a.suffixMap,r=W(t,""),n=this._overwriteProps;var h,p,f,m,d,g,v,y,T,x=t.style;if(_&&""===x.zIndex&&(h=G(t,"zIndex",r),("auto"===h||""===h)&&(x.zIndex=0)),"string"==typeof e&&(m=x.cssText,h=Q(t,r),x.cssText=m+";"+e,h=H(t,h,Q(t)).difs,!U&&w.test(e)&&(h.opacity=parseFloat(RegExp.$1)),e=h,x.cssText=m),this._firstPT=p=this.parse(t,e,null),this._transformType){for(T=3===this._transformType,ye?u&&(_=!0,""===x.zIndex&&(v=G(t,"zIndex",r),("auto"===v||""===v)&&(x.zIndex=0)),c&&(x.WebkitBackfaceVisibility=this._vars.WebkitBackfaceVisibility||(T?"visible":"hidden"))):x.zoom=1,f=p;f&&f._next;)f=f._next;y=new pe(t,"transform",0,0,null,2),this._linkCSSP(y,null,f),y.setRatio=T&&xe?ke:ye?Re:Se,y.data=this._transform||Pe(t,r,!0),n.pop()}if(i){for(;p;){for(g=p._next,f=m;f&&f.pr>p.pr;)f=f._next;(p._prev=f?f._prev:d)?p._prev._next=p:m=p,(p._next=f)?f._prev=p:d=p,p=g}this._firstPT=m}return!0},h.parse=function(t,e,i,n){var a,h,_,u,p,c,f,m,d,g,v=t.style;for(a in e)c=e[a],h=o[a],h?i=h.parse(t,c,a,this,i,n,e):(p=G(t,a,r)+"",d="string"==typeof c,"color"===a||"fill"===a||"stroke"===a||-1!==a.indexOf("Color")||d&&b.test(c)?(d||(c=oe(c),c=(c.length>3?"rgba(":"rgb(")+c.join(",")+")"),i=ce(v,a,p,c,!0,"transparent",i,0,n)):!d||-1===c.indexOf(" ")&&-1===c.indexOf(",")?(_=parseFloat(p),f=_||0===_?p.substr((_+"").length):"",(""===p||"auto"===p)&&("width"===a||"height"===a?(_=te(t,a,r),f="px"):"left"===a||"top"===a?(_=Z(t,a,r),f="px"):(_="opacity"!==a?0:1,f="")),g=d&&"="===c.charAt(1),g?(u=parseInt(c.charAt(0)+"1",10),c=c.substr(2),u*=parseFloat(c),m=c.replace(y,"")):(u=parseFloat(c),m=d?c.substr((u+"").length)||"":""),""===m&&(m=a in s?s[a]:f),c=u||0===u?(g?u+_:u)+m:e[a],f!==m&&""!==m&&(u||0===u)&&_&&(_=$(t,a,_,f),"%"===m?(_/=$(t,a,100,"%")/100,e.strictUnits!==!0&&(p=_+"%")):"em"===m?_/=$(t,a,1,"em"):"px"!==m&&(u=$(t,a,u,m),m="px"),g&&(u||0===u)&&(c=u+_+m)),g&&(u+=_),!_&&0!==_||!u&&0!==u?void 0!==v[a]&&(c||"NaN"!=c+""&&null!=c)?(i=new pe(v,a,u||_||0,0,i,-1,a,!1,0,p,c),i.xs0="none"!==c||"display"!==a&&-1===a.indexOf("Style")?c:p):j("invalid "+a+" tween value: "+e[a]):(i=new pe(v,a,_,u-_,i,0,a,l!==!1&&("px"===m||"zIndex"===a),0,p,c),i.xs0=m)):i=ce(v,a,p,c,!0,null,i,0,n)),n&&i&&!i.plugin&&(i.plugin=n);return i},h.setRatio=function(t){var e,i,s,r=this._firstPT,n=1e-6;if(1!==t||this._tween._time!==this._tween._duration&&0!==this._tween._time)if(t||this._tween._time!==this._tween._duration&&0!==this._tween._time||this._tween._rawPrevTime===-1e-6)for(;r;){if(e=r.c*t+r.s,r.r?e=Math.round(e):n>e&&e>-n&&(e=0),r.type)if(1===r.type)if(s=r.l,2===s)r.t[r.p]=r.xs0+e+r.xs1+r.xn1+r.xs2;else if(3===s)r.t[r.p]=r.xs0+e+r.xs1+r.xn1+r.xs2+r.xn2+r.xs3;else if(4===s)r.t[r.p]=r.xs0+e+r.xs1+r.xn1+r.xs2+r.xn2+r.xs3+r.xn3+r.xs4;else if(5===s)r.t[r.p]=r.xs0+e+r.xs1+r.xn1+r.xs2+r.xn2+r.xs3+r.xn3+r.xs4+r.xn4+r.xs5;else{for(i=r.xs0+e+r.xs1,s=1;r.l>s;s++)i+=r["xn"+s]+r["xs"+(s+1)];r.t[r.p]=i}else-1===r.type?r.t[r.p]=r.xs0:r.setRatio&&r.setRatio(t);else r.t[r.p]=e+r.xs0;r=r._next}else for(;r;)2!==r.type?r.t[r.p]=r.b:r.setRatio(t),r=r._next;else for(;r;)2!==r.type?r.t[r.p]=r.e:r.setRatio(t),r=r._next},h._enableTransforms=function(t){this._transformType=t||3===this._transformType?3:2,this._transform=this._transform||Pe(this._target,r,!0)},h._linkCSSP=function(t,e,i,s){return t&&(e&&(e._prev=t),t._next&&(t._next._prev=t._prev),t._prev?t._prev._next=t._next:this._firstPT===t&&(this._firstPT=t._next,s=!0),i?i._next=t:s||null!==this._firstPT||(this._firstPT=t),t._next=e,t._prev=i),t},h._kill=function(e){var i,s,r,n=e;if(e.autoAlpha||e.alpha){n={};for(s in e)n[s]=e[s];n.opacity=1,n.autoAlpha&&(n.visibility=1)}return e.className&&(i=this._classNamePT)&&(r=i.xfirst,r&&r._prev?this._linkCSSP(r._prev,i._next,r._prev._prev):r===this._firstPT&&(this._firstPT=i._next),i._next&&this._linkCSSP(i._next,i._next._next,r._prev),this._classNamePT=null),t.prototype._kill.call(this,n)};var Me=function(t,e,i){var s,r,n,a;if(t.slice)for(r=t.length;--r>-1;)Me(t[r],e,i);else for(s=t.childNodes,r=s.length;--r>-1;)n=s[r],a=n.type,n.style&&(e.push(Q(n)),i&&i.push(n)),1!==a&&9!==a&&11!==a||!n.childNodes.length||Me(n,e,i)
};return a.cascadeTo=function(t,i,s){var r,n,a,o=e.to(t,i,s),h=[o],l=[],_=[],u=[],p=e._internals.reservedProps;for(t=o._targets||o.target,Me(t,l,u),o.render(i,!0),Me(t,_),o.render(0,!0),o._enabled(!0),r=u.length;--r>-1;)if(n=H(u[r],l[r],_[r]),n.firstMPT){n=n.difs;for(a in s)p[a]&&(n[a]=s[a]);h.push(e.to(u[r],i,n))}return h},t.activate([a]),a},!0),function(){var t=window._gsDefine.plugin({propName:"roundProps",priority:-1,API:2,init:function(t,e,i){return this._tween=i,!0}}),e=t.prototype;e._onInitAllProps=function(){for(var t,e,i,s=this._tween,r=s.vars.roundProps instanceof Array?s.vars.roundProps:s.vars.roundProps.split(","),n=r.length,a={},o=s._propLookup.roundProps;--n>-1;)a[r[n]]=1;for(n=r.length;--n>-1;)for(t=r[n],e=s._firstPT;e;)i=e._next,e.pg?e.t._roundProps(a,!0):e.n===t&&(this._add(e.t,t,e.s,e.c),i&&(i._prev=e._prev),e._prev?e._prev._next=i:s._firstPT===e&&(s._firstPT=i),e._next=e._prev=null,s._propLookup[t]=o),e=i;return!1},e._add=function(t,e,i,s){this._addTween(t,e,i,i+s,e,!0),this._overwriteProps.push(e)}}(),window._gsDefine.plugin({propName:"attr",API:2,version:"0.2.0",init:function(t,e){var i;if("function"!=typeof t.setAttribute)return!1;this._target=t,this._proxy={};for(i in e)this._addTween(this._proxy,i,parseFloat(t.getAttribute(i)),e[i],i)&&this._overwriteProps.push(i);return!0},set:function(t){this._super.setRatio.call(this,t);for(var e,i=this._overwriteProps,s=i.length;--s>-1;)e=i[s],this._target.setAttribute(e,this._proxy[e]+"")}}),window._gsDefine.plugin({propName:"directionalRotation",API:2,version:"0.2.0",init:function(t,e){"object"!=typeof e&&(e={rotation:e}),this.finals={};var i,s,r,n,a,o,h=e.useRadians===!0?2*Math.PI:360,l=1e-6;for(i in e)"useRadians"!==i&&(o=(e[i]+"").split("_"),s=o[0],r=parseFloat("function"!=typeof t[i]?t[i]:t[i.indexOf("set")||"function"!=typeof t["get"+i.substr(3)]?i:"get"+i.substr(3)]()),n=this.finals[i]="string"==typeof s&&"="===s.charAt(1)?r+parseInt(s.charAt(0)+"1",10)*Number(s.substr(2)):Number(s)||0,a=n-r,o.length&&(s=o.join("_"),-1!==s.indexOf("short")&&(a%=h,a!==a%(h/2)&&(a=0>a?a+h:a-h)),-1!==s.indexOf("_cw")&&0>a?a=(a+9999999999*h)%h-(0|a/h)*h:-1!==s.indexOf("ccw")&&a>0&&(a=(a-9999999999*h)%h-(0|a/h)*h)),(a>l||-l>a)&&(this._addTween(t,i,r,r+a,i),this._overwriteProps.push(i)));return!0},set:function(t){var e;if(1!==t)this._super.setRatio.call(this,t);else for(e=this._firstPT;e;)e.f?e.t[e.p](this.finals[e.p]):e.t[e.p]=this.finals[e.p],e=e._next}})._autoCSS=!0,window._gsDefine("easing.Back",["easing.Ease"],function(t){var e,i,s,r=window.GreenSockGlobals||window,n=r.com.greensock,a=2*Math.PI,o=Math.PI/2,h=n._class,l=function(e,i){var s=h("easing."+e,function(){},!0),r=s.prototype=new t;return r.constructor=s,r.getRatio=i,s},_=t.register||function(){},u=function(t,e,i,s){var r=h("easing."+t,{easeOut:new e,easeIn:new i,easeInOut:new s},!0);return _(r,t),r},p=function(t,e,i){this.t=t,this.v=e,i&&(this.next=i,i.prev=this,this.c=i.v-e,this.gap=i.t-t)},c=function(e,i){var s=h("easing."+e,function(t){this._p1=t||0===t?t:1.70158,this._p2=1.525*this._p1},!0),r=s.prototype=new t;return r.constructor=s,r.getRatio=i,r.config=function(t){return new s(t)},s},f=u("Back",c("BackOut",function(t){return(t-=1)*t*((this._p1+1)*t+this._p1)+1}),c("BackIn",function(t){return t*t*((this._p1+1)*t-this._p1)}),c("BackInOut",function(t){return 1>(t*=2)?.5*t*t*((this._p2+1)*t-this._p2):.5*((t-=2)*t*((this._p2+1)*t+this._p2)+2)})),m=h("easing.SlowMo",function(t,e,i){e=e||0===e?e:.7,null==t?t=.7:t>1&&(t=1),this._p=1!==t?e:0,this._p1=(1-t)/2,this._p2=t,this._p3=this._p1+this._p2,this._calcEnd=i===!0},!0),d=m.prototype=new t;return d.constructor=m,d.getRatio=function(t){var e=t+(.5-t)*this._p;return this._p1>t?this._calcEnd?1-(t=1-t/this._p1)*t:e-(t=1-t/this._p1)*t*t*t*e:t>this._p3?this._calcEnd?1-(t=(t-this._p3)/this._p1)*t:e+(t-e)*(t=(t-this._p3)/this._p1)*t*t*t:this._calcEnd?1:e},m.ease=new m(.7,.7),d.config=m.config=function(t,e,i){return new m(t,e,i)},e=h("easing.SteppedEase",function(t){t=t||1,this._p1=1/t,this._p2=t+1},!0),d=e.prototype=new t,d.constructor=e,d.getRatio=function(t){return 0>t?t=0:t>=1&&(t=.999999999),(this._p2*t>>0)*this._p1},d.config=e.config=function(t){return new e(t)},i=h("easing.RoughEase",function(e){e=e||{};for(var i,s,r,n,a,o,h=e.taper||"none",l=[],_=0,u=0|(e.points||20),c=u,f=e.randomize!==!1,m=e.clamp===!0,d=e.template instanceof t?e.template:null,g="number"==typeof e.strength?.4*e.strength:.4;--c>-1;)i=f?Math.random():1/u*c,s=d?d.getRatio(i):i,"none"===h?r=g:"out"===h?(n=1-i,r=n*n*g):"in"===h?r=i*i*g:.5>i?(n=2*i,r=.5*n*n*g):(n=2*(1-i),r=.5*n*n*g),f?s+=Math.random()*r-.5*r:c%2?s+=.5*r:s-=.5*r,m&&(s>1?s=1:0>s&&(s=0)),l[_++]={x:i,y:s};for(l.sort(function(t,e){return t.x-e.x}),o=new p(1,1,null),c=u;--c>-1;)a=l[c],o=new p(a.x,a.y,o);this._prev=new p(0,0,0!==o.t?o:o.next)},!0),d=i.prototype=new t,d.constructor=i,d.getRatio=function(t){var e=this._prev;if(t>e.t){for(;e.next&&t>=e.t;)e=e.next;e=e.prev}else for(;e.prev&&e.t>=t;)e=e.prev;return this._prev=e,e.v+(t-e.t)/e.gap*e.c},d.config=function(t){return new i(t)},i.ease=new i,u("Bounce",l("BounceOut",function(t){return 1/2.75>t?7.5625*t*t:2/2.75>t?7.5625*(t-=1.5/2.75)*t+.75:2.5/2.75>t?7.5625*(t-=2.25/2.75)*t+.9375:7.5625*(t-=2.625/2.75)*t+.984375}),l("BounceIn",function(t){return 1/2.75>(t=1-t)?1-7.5625*t*t:2/2.75>t?1-(7.5625*(t-=1.5/2.75)*t+.75):2.5/2.75>t?1-(7.5625*(t-=2.25/2.75)*t+.9375):1-(7.5625*(t-=2.625/2.75)*t+.984375)}),l("BounceInOut",function(t){var e=.5>t;return t=e?1-2*t:2*t-1,t=1/2.75>t?7.5625*t*t:2/2.75>t?7.5625*(t-=1.5/2.75)*t+.75:2.5/2.75>t?7.5625*(t-=2.25/2.75)*t+.9375:7.5625*(t-=2.625/2.75)*t+.984375,e?.5*(1-t):.5*t+.5})),u("Circ",l("CircOut",function(t){return Math.sqrt(1-(t-=1)*t)}),l("CircIn",function(t){return-(Math.sqrt(1-t*t)-1)}),l("CircInOut",function(t){return 1>(t*=2)?-.5*(Math.sqrt(1-t*t)-1):.5*(Math.sqrt(1-(t-=2)*t)+1)})),s=function(e,i,s){var r=h("easing."+e,function(t,e){this._p1=t||1,this._p2=e||s,this._p3=this._p2/a*(Math.asin(1/this._p1)||0)},!0),n=r.prototype=new t;return n.constructor=r,n.getRatio=i,n.config=function(t,e){return new r(t,e)},r},u("Elastic",s("ElasticOut",function(t){return this._p1*Math.pow(2,-10*t)*Math.sin((t-this._p3)*a/this._p2)+1},.3),s("ElasticIn",function(t){return-(this._p1*Math.pow(2,10*(t-=1))*Math.sin((t-this._p3)*a/this._p2))},.3),s("ElasticInOut",function(t){return 1>(t*=2)?-.5*this._p1*Math.pow(2,10*(t-=1))*Math.sin((t-this._p3)*a/this._p2):.5*this._p1*Math.pow(2,-10*(t-=1))*Math.sin((t-this._p3)*a/this._p2)+1},.45)),u("Expo",l("ExpoOut",function(t){return 1-Math.pow(2,-10*t)}),l("ExpoIn",function(t){return Math.pow(2,10*(t-1))-.001}),l("ExpoInOut",function(t){return 1>(t*=2)?.5*Math.pow(2,10*(t-1)):.5*(2-Math.pow(2,-10*(t-1)))})),u("Sine",l("SineOut",function(t){return Math.sin(t*o)}),l("SineIn",function(t){return-Math.cos(t*o)+1}),l("SineInOut",function(t){return-.5*(Math.cos(Math.PI*t)-1)})),h("easing.EaseLookup",{find:function(e){return t.map[e]}},!0),_(r.SlowMo,"SlowMo","ease,"),_(i,"RoughEase","ease,"),_(e,"SteppedEase","ease,"),f},!0)}),function(t){"use strict";var e=t.GreenSockGlobals||t;if(!e.TweenLite){var i,s,r,n,a,o=function(t){var i,s=t.split("."),r=e;for(i=0;s.length>i;i++)r[s[i]]=r=r[s[i]]||{};return r},h=o("com.greensock"),l=1e-10,_=[].slice,u=function(){},p=function(){var t=Object.prototype.toString,e=t.call([]);return function(i){return null!=i&&(i instanceof Array||"object"==typeof i&&!!i.push&&t.call(i)===e)}}(),c={},f=function(i,s,r,n){this.sc=c[i]?c[i].sc:[],c[i]=this,this.gsClass=null,this.func=r;var a=[];this.check=function(h){for(var l,_,u,p,m=s.length,d=m;--m>-1;)(l=c[s[m]]||new f(s[m],[])).gsClass?(a[m]=l.gsClass,d--):h&&l.sc.push(this);if(0===d&&r)for(_=("com.greensock."+i).split("."),u=_.pop(),p=o(_.join("."))[u]=this.gsClass=r.apply(r,a),n&&(e[u]=p,"function"==typeof define&&define.amd?define((t.GreenSockAMDPath?t.GreenSockAMDPath+"/":"")+i.split(".").join("/"),[],function(){return p}):"undefined"!=typeof module&&module.exports&&(module.exports=p)),m=0;this.sc.length>m;m++)this.sc[m].check()},this.check(!0)},m=t._gsDefine=function(t,e,i,s){return new f(t,e,i,s)},d=h._class=function(t,e,i){return e=e||function(){},m(t,[],function(){return e},i),e};m.globals=e;var g=[0,0,1,1],v=[],y=d("easing.Ease",function(t,e,i,s){this._func=t,this._type=i||0,this._power=s||0,this._params=e?g.concat(e):g},!0),T=y.map={},w=y.register=function(t,e,i,s){for(var r,n,a,o,l=e.split(","),_=l.length,u=(i||"easeIn,easeOut,easeInOut").split(",");--_>-1;)for(n=l[_],r=s?d("easing."+n,null,!0):h.easing[n]||{},a=u.length;--a>-1;)o=u[a],T[n+"."+o]=T[o+n]=r[o]=t.getRatio?t:t[o]||new t};for(r=y.prototype,r._calcEnd=!1,r.getRatio=function(t){if(this._func)return this._params[0]=t,this._func.apply(null,this._params);var e=this._type,i=this._power,s=1===e?1-t:2===e?t:.5>t?2*t:2*(1-t);return 1===i?s*=s:2===i?s*=s*s:3===i?s*=s*s*s:4===i&&(s*=s*s*s*s),1===e?1-s:2===e?s:.5>t?s/2:1-s/2},i=["Linear","Quad","Cubic","Quart","Quint,Strong"],s=i.length;--s>-1;)r=i[s]+",Power"+s,w(new y(null,null,1,s),r,"easeOut",!0),w(new y(null,null,2,s),r,"easeIn"+(0===s?",easeNone":"")),w(new y(null,null,3,s),r,"easeInOut");T.linear=h.easing.Linear.easeIn,T.swing=h.easing.Quad.easeInOut;var x=d("events.EventDispatcher",function(t){this._listeners={},this._eventTarget=t||this});r=x.prototype,r.addEventListener=function(t,e,i,s,r){r=r||0;var o,h,l=this._listeners[t],_=0;for(null==l&&(this._listeners[t]=l=[]),h=l.length;--h>-1;)o=l[h],o.c===e&&o.s===i?l.splice(h,1):0===_&&r>o.pr&&(_=h+1);l.splice(_,0,{c:e,s:i,up:s,pr:r}),this!==n||a||n.wake()},r.removeEventListener=function(t,e){var i,s=this._listeners[t];if(s)for(i=s.length;--i>-1;)if(s[i].c===e)return s.splice(i,1),void 0},r.dispatchEvent=function(t){var e,i,s,r=this._listeners[t];if(r)for(e=r.length,i=this._eventTarget;--e>-1;)s=r[e],s.up?s.c.call(s.s||i,{type:t,target:i}):s.c.call(s.s||i)};var b=t.requestAnimationFrame,P=t.cancelAnimationFrame,S=Date.now||function(){return(new Date).getTime()},k=S();for(i=["ms","moz","webkit","o"],s=i.length;--s>-1&&!b;)b=t[i[s]+"RequestAnimationFrame"],P=t[i[s]+"CancelAnimationFrame"]||t[i[s]+"CancelRequestAnimationFrame"];d("Ticker",function(t,e){var i,s,r,o,h,l=this,_=S(),p=e!==!1&&b,c=function(t){k=S(),l.time=(k-_)/1e3;var e,n=l.time-h;(!i||n>0||t===!0)&&(l.frame++,h+=n+(n>=o?.004:o-n),e=!0),t!==!0&&(r=s(c)),e&&l.dispatchEvent("tick")};x.call(l),l.time=l.frame=0,l.tick=function(){c(!0)},l.sleep=function(){null!=r&&(p&&P?P(r):clearTimeout(r),s=u,r=null,l===n&&(a=!1))},l.wake=function(){null!==r&&l.sleep(),s=0===i?u:p&&b?b:function(t){return setTimeout(t,0|1e3*(h-l.time)+1)},l===n&&(a=!0),c(2)},l.fps=function(t){return arguments.length?(i=t,o=1/(i||60),h=this.time+o,l.wake(),void 0):i},l.useRAF=function(t){return arguments.length?(l.sleep(),p=t,l.fps(i),void 0):p},l.fps(t),setTimeout(function(){p&&(!r||5>l.frame)&&l.useRAF(!1)},1500)}),r=h.Ticker.prototype=new h.events.EventDispatcher,r.constructor=h.Ticker;var R=d("core.Animation",function(t,e){if(this.vars=e=e||{},this._duration=this._totalDuration=t||0,this._delay=Number(e.delay)||0,this._timeScale=1,this._active=e.immediateRender===!0,this.data=e.data,this._reversed=e.reversed===!0,U){a||n.wake();var i=this.vars.useFrames?z:U;i.add(this,i._time),this.vars.paused&&this.paused(!0)}});n=R.ticker=new h.Ticker,r=R.prototype,r._dirty=r._gc=r._initted=r._paused=!1,r._totalTime=r._time=0,r._rawPrevTime=-1,r._next=r._last=r._onUpdate=r._timeline=r.timeline=null,r._paused=!1;var A=function(){a&&S()-k>2e3&&n.wake(),setTimeout(A,2e3)};A(),r.play=function(t,e){return null!=t&&this.seek(t,e),this.reversed(!1).paused(!1)},r.pause=function(t,e){return null!=t&&this.seek(t,e),this.paused(!0)},r.resume=function(t,e){return null!=t&&this.seek(t,e),this.paused(!1)},r.seek=function(t,e){return this.totalTime(Number(t),e!==!1)},r.restart=function(t,e){return this.reversed(!1).paused(!1).totalTime(t?-this._delay:0,e!==!1,!0)},r.reverse=function(t,e){return null!=t&&this.seek(t||this.totalDuration(),e),this.reversed(!0).paused(!1)},r.render=function(){},r.invalidate=function(){return this},r.isActive=function(){var t,e=this._timeline,i=this._startTime;return!e||!this._gc&&!this._paused&&e.isActive()&&(t=e.rawTime())>=i&&i+this.totalDuration()/this._timeScale>t},r._enabled=function(t,e){return a||n.wake(),this._gc=!t,this._active=this.isActive(),e!==!0&&(t&&!this.timeline?this._timeline.add(this,this._startTime-this._delay):!t&&this.timeline&&this._timeline._remove(this,!0)),!1},r._kill=function(){return this._enabled(!1,!1)},r.kill=function(t,e){return this._kill(t,e),this},r._uncache=function(t){for(var e=t?this:this.timeline;e;)e._dirty=!0,e=e.timeline;return this},r._swapSelfInParams=function(t){for(var e=t.length,i=t.concat();--e>-1;)"{self}"===t[e]&&(i[e]=this);return i},r.eventCallback=function(t,e,i,s){if("on"===(t||"").substr(0,2)){var r=this.vars;if(1===arguments.length)return r[t];null==e?delete r[t]:(r[t]=e,r[t+"Params"]=p(i)&&-1!==i.join("").indexOf("{self}")?this._swapSelfInParams(i):i,r[t+"Scope"]=s),"onUpdate"===t&&(this._onUpdate=e)}return this},r.delay=function(t){return arguments.length?(this._timeline.smoothChildTiming&&this.startTime(this._startTime+t-this._delay),this._delay=t,this):this._delay},r.duration=function(t){return arguments.length?(this._duration=this._totalDuration=t,this._uncache(!0),this._timeline.smoothChildTiming&&this._time>0&&this._time<this._duration&&0!==t&&this.totalTime(this._totalTime*(t/this._duration),!0),this):(this._dirty=!1,this._duration)},r.totalDuration=function(t){return this._dirty=!1,arguments.length?this.duration(t):this._totalDuration},r.time=function(t,e){return arguments.length?(this._dirty&&this.totalDuration(),this.totalTime(t>this._duration?this._duration:t,e)):this._time},r.totalTime=function(t,e,i){if(a||n.wake(),!arguments.length)return this._totalTime;if(this._timeline){if(0>t&&!i&&(t+=this.totalDuration()),this._timeline.smoothChildTiming){this._dirty&&this.totalDuration();var s=this._totalDuration,r=this._timeline;if(t>s&&!i&&(t=s),this._startTime=(this._paused?this._pauseTime:r._time)-(this._reversed?s-t:t)/this._timeScale,r._dirty||this._uncache(!1),r._timeline)for(;r._timeline;)r._timeline._time!==(r._startTime+r._totalTime)/r._timeScale&&r.totalTime(r._totalTime,!0),r=r._timeline}this._gc&&this._enabled(!0,!1),(this._totalTime!==t||0===this._duration)&&this.render(t,e,!1)}return this},r.progress=r.totalProgress=function(t,e){return arguments.length?this.totalTime(this.duration()*t,e):this._time/this.duration()},r.startTime=function(t){return arguments.length?(t!==this._startTime&&(this._startTime=t,this.timeline&&this.timeline._sortChildren&&this.timeline.add(this,t-this._delay)),this):this._startTime},r.timeScale=function(t){if(!arguments.length)return this._timeScale;if(t=t||l,this._timeline&&this._timeline.smoothChildTiming){var e=this._pauseTime,i=e||0===e?e:this._timeline.totalTime();this._startTime=i-(i-this._startTime)*this._timeScale/t}return this._timeScale=t,this._uncache(!1)},r.reversed=function(t){return arguments.length?(t!=this._reversed&&(this._reversed=t,this.totalTime(this._timeline&&!this._timeline.smoothChildTiming?this.totalDuration()-this._totalTime:this._totalTime,!0)),this):this._reversed},r.paused=function(t){if(!arguments.length)return this._paused;if(t!=this._paused&&this._timeline){a||t||n.wake();var e=this._timeline,i=e.rawTime(),s=i-this._pauseTime;!t&&e.smoothChildTiming&&(this._startTime+=s,this._uncache(!1)),this._pauseTime=t?i:null,this._paused=t,this._active=this.isActive(),!t&&0!==s&&this._initted&&this.duration()&&this.render(e.smoothChildTiming?this._totalTime:(i-this._startTime)/this._timeScale,!0,!0)}return this._gc&&!t&&this._enabled(!0,!1),this};var C=d("core.SimpleTimeline",function(t){R.call(this,0,t),this.autoRemoveChildren=this.smoothChildTiming=!0});r=C.prototype=new R,r.constructor=C,r.kill()._gc=!1,r._first=r._last=null,r._sortChildren=!1,r.add=r.insert=function(t,e){var i,s;if(t._startTime=Number(e||0)+t._delay,t._paused&&this!==t._timeline&&(t._pauseTime=t._startTime+(this.rawTime()-t._startTime)/t._timeScale),t.timeline&&t.timeline._remove(t,!0),t.timeline=t._timeline=this,t._gc&&t._enabled(!0,!0),i=this._last,this._sortChildren)for(s=t._startTime;i&&i._startTime>s;)i=i._prev;return i?(t._next=i._next,i._next=t):(t._next=this._first,this._first=t),t._next?t._next._prev=t:this._last=t,t._prev=i,this._timeline&&this._uncache(!0),this},r._remove=function(t,e){return t.timeline===this&&(e||t._enabled(!1,!0),t.timeline=null,t._prev?t._prev._next=t._next:this._first===t&&(this._first=t._next),t._next?t._next._prev=t._prev:this._last===t&&(this._last=t._prev),this._timeline&&this._uncache(!0)),this},r.render=function(t,e,i){var s,r=this._first;for(this._totalTime=this._time=this._rawPrevTime=t;r;)s=r._next,(r._active||t>=r._startTime&&!r._paused)&&(r._reversed?r.render((r._dirty?r.totalDuration():r._totalDuration)-(t-r._startTime)*r._timeScale,e,i):r.render((t-r._startTime)*r._timeScale,e,i)),r=s},r.rawTime=function(){return a||n.wake(),this._totalTime};var O=d("TweenLite",function(e,i,s){if(R.call(this,i,s),this.render=O.prototype.render,null==e)throw"Cannot tween a null target.";this.target=e="string"!=typeof e?e:O.selector(e)||e;var r,n,a,o=e.jquery||e.length&&e!==t&&e[0]&&(e[0]===t||e[0].nodeType&&e[0].style&&!e.nodeType),h=this.vars.overwrite;if(this._overwrite=h=null==h?X[O.defaultOverwrite]:"number"==typeof h?h>>0:X[h],(o||e instanceof Array||e.push&&p(e))&&"number"!=typeof e[0])for(this._targets=a=_.call(e,0),this._propLookup=[],this._siblings=[],r=0;a.length>r;r++)n=a[r],n?"string"!=typeof n?n.length&&n!==t&&n[0]&&(n[0]===t||n[0].nodeType&&n[0].style&&!n.nodeType)?(a.splice(r--,1),this._targets=a=a.concat(_.call(n,0))):(this._siblings[r]=Y(n,this,!1),1===h&&this._siblings[r].length>1&&j(n,this,null,1,this._siblings[r])):(n=a[r--]=O.selector(n),"string"==typeof n&&a.splice(r+1,1)):a.splice(r--,1);else this._propLookup={},this._siblings=Y(e,this,!1),1===h&&this._siblings.length>1&&j(e,this,null,1,this._siblings);(this.vars.immediateRender||0===i&&0===this._delay&&this.vars.immediateRender!==!1)&&this.render(-this._delay,!1,!0)},!0),D=function(e){return e.length&&e!==t&&e[0]&&(e[0]===t||e[0].nodeType&&e[0].style&&!e.nodeType)},M=function(t,e){var i,s={};for(i in t)L[i]||i in e&&"x"!==i&&"y"!==i&&"width"!==i&&"height"!==i&&"className"!==i&&"border"!==i||!(!E[i]||E[i]&&E[i]._autoCSS)||(s[i]=t[i],delete t[i]);t.css=s};r=O.prototype=new R,r.constructor=O,r.kill()._gc=!1,r.ratio=0,r._firstPT=r._targets=r._overwrittenProps=r._startAt=null,r._notifyPluginsOfEnabled=!1,O.version="1.11.7",O.defaultEase=r._ease=new y(null,null,1,1),O.defaultOverwrite="auto",O.ticker=n,O.autoSleep=!0,O.selector=t.$||t.jQuery||function(e){return t.$?(O.selector=t.$,t.$(e)):t.document?t.document.getElementById("#"===e.charAt(0)?e.substr(1):e):e};var I=O._internals={isArray:p,isSelector:D},E=O._plugins={},N=O._tweenLookup={},F=0,L=I.reservedProps={ease:1,delay:1,overwrite:1,onComplete:1,onCompleteParams:1,onCompleteScope:1,useFrames:1,runBackwards:1,startAt:1,onUpdate:1,onUpdateParams:1,onUpdateScope:1,onStart:1,onStartParams:1,onStartScope:1,onReverseComplete:1,onReverseCompleteParams:1,onReverseCompleteScope:1,onRepeat:1,onRepeatParams:1,onRepeatScope:1,easeParams:1,yoyo:1,immediateRender:1,repeat:1,repeatDelay:1,data:1,paused:1,reversed:1,autoCSS:1},X={none:0,all:1,auto:2,concurrent:3,allOnStart:4,preexisting:5,"true":1,"false":0},z=R._rootFramesTimeline=new C,U=R._rootTimeline=new C;U._startTime=n.time,z._startTime=n.frame,U._active=z._active=!0,R._updateRoot=function(){if(U.render((n.time-U._startTime)*U._timeScale,!1,!1),z.render((n.frame-z._startTime)*z._timeScale,!1,!1),!(n.frame%120)){var t,e,i;for(i in N){for(e=N[i].tweens,t=e.length;--t>-1;)e[t]._gc&&e.splice(t,1);0===e.length&&delete N[i]}if(i=U._first,(!i||i._paused)&&O.autoSleep&&!z._first&&1===n._listeners.tick.length){for(;i&&i._paused;)i=i._next;i||n.sleep()}}},n.addEventListener("tick",R._updateRoot);var Y=function(t,e,i){var s,r,n=t._gsTweenID;if(N[n||(t._gsTweenID=n="t"+F++)]||(N[n]={target:t,tweens:[]}),e&&(s=N[n].tweens,s[r=s.length]=e,i))for(;--r>-1;)s[r]===e&&s.splice(r,1);return N[n].tweens},j=function(t,e,i,s,r){var n,a,o,h;if(1===s||s>=4){for(h=r.length,n=0;h>n;n++)if((o=r[n])!==e)o._gc||o._enabled(!1,!1)&&(a=!0);else if(5===s)break;return a}var _,u=e._startTime+l,p=[],c=0,f=0===e._duration;for(n=r.length;--n>-1;)(o=r[n])===e||o._gc||o._paused||(o._timeline!==e._timeline?(_=_||B(e,0,f),0===B(o,_,f)&&(p[c++]=o)):u>=o._startTime&&o._startTime+o.totalDuration()/o._timeScale>u&&((f||!o._initted)&&2e-10>=u-o._startTime||(p[c++]=o)));for(n=c;--n>-1;)o=p[n],2===s&&o._kill(i,t)&&(a=!0),(2!==s||!o._firstPT&&o._initted)&&o._enabled(!1,!1)&&(a=!0);return a},B=function(t,e,i){for(var s=t._timeline,r=s._timeScale,n=t._startTime;s._timeline;){if(n+=s._startTime,r*=s._timeScale,s._paused)return-100;s=s._timeline}return n/=r,n>e?n-e:i&&n===e||!t._initted&&2*l>n-e?l:(n+=t.totalDuration()/t._timeScale/r)>e+l?0:n-e-l};r._init=function(){var t,e,i,s,r=this.vars,n=this._overwrittenProps,a=this._duration,o=r.immediateRender,h=r.ease;if(r.startAt){if(this._startAt&&this._startAt.render(-1,!0),r.startAt.overwrite=0,r.startAt.immediateRender=!0,this._startAt=O.to(this.target,0,r.startAt),o)if(this._time>0)this._startAt=null;else if(0!==a)return}else if(r.runBackwards&&0!==a)if(this._startAt)this._startAt.render(-1,!0),this._startAt=null;else{i={};for(s in r)L[s]&&"autoCSS"!==s||(i[s]=r[s]);if(i.overwrite=0,i.data="isFromStart",this._startAt=O.to(this.target,0,i),r.immediateRender){if(0===this._time)return}else this._startAt.render(-1,!0)}if(this._ease=h?h instanceof y?r.easeParams instanceof Array?h.config.apply(h,r.easeParams):h:"function"==typeof h?new y(h,r.easeParams):T[h]||O.defaultEase:O.defaultEase,this._easeType=this._ease._type,this._easePower=this._ease._power,this._firstPT=null,this._targets)for(t=this._targets.length;--t>-1;)this._initProps(this._targets[t],this._propLookup[t]={},this._siblings[t],n?n[t]:null)&&(e=!0);else e=this._initProps(this.target,this._propLookup,this._siblings,n);if(e&&O._onPluginEvent("_onInitAllProps",this),n&&(this._firstPT||"function"!=typeof this.target&&this._enabled(!1,!1)),r.runBackwards)for(i=this._firstPT;i;)i.s+=i.c,i.c=-i.c,i=i._next;this._onUpdate=r.onUpdate,this._initted=!0},r._initProps=function(e,i,s,r){var n,a,o,h,l,_;if(null==e)return!1;this.vars.css||e.style&&e!==t&&e.nodeType&&E.css&&this.vars.autoCSS!==!1&&M(this.vars,e);for(n in this.vars){if(_=this.vars[n],L[n])_&&(_ instanceof Array||_.push&&p(_))&&-1!==_.join("").indexOf("{self}")&&(this.vars[n]=_=this._swapSelfInParams(_,this));else if(E[n]&&(h=new E[n])._onInitTween(e,this.vars[n],this)){for(this._firstPT=l={_next:this._firstPT,t:h,p:"setRatio",s:0,c:1,f:!0,n:n,pg:!0,pr:h._priority},a=h._overwriteProps.length;--a>-1;)i[h._overwriteProps[a]]=this._firstPT;(h._priority||h._onInitAllProps)&&(o=!0),(h._onDisable||h._onEnable)&&(this._notifyPluginsOfEnabled=!0)}else this._firstPT=i[n]=l={_next:this._firstPT,t:e,p:n,f:"function"==typeof e[n],n:n,pg:!1,pr:0},l.s=l.f?e[n.indexOf("set")||"function"!=typeof e["get"+n.substr(3)]?n:"get"+n.substr(3)]():parseFloat(e[n]),l.c="string"==typeof _&&"="===_.charAt(1)?parseInt(_.charAt(0)+"1",10)*Number(_.substr(2)):Number(_)-l.s||0;l&&l._next&&(l._next._prev=l)}return r&&this._kill(r,e)?this._initProps(e,i,s,r):this._overwrite>1&&this._firstPT&&s.length>1&&j(e,this,i,this._overwrite,s)?(this._kill(i,e),this._initProps(e,i,s,r)):o},r.render=function(t,e,i){var s,r,n,a,o=this._time,h=this._duration;if(t>=h)this._totalTime=this._time=h,this.ratio=this._ease._calcEnd?this._ease.getRatio(1):1,this._reversed||(s=!0,r="onComplete"),0===h&&(a=this._rawPrevTime,this._startTime===this._timeline._duration&&(t=0),(0===t||0>a||a===l)&&a!==t&&(i=!0,a>l&&(r="onReverseComplete")),this._rawPrevTime=a=!e||t||this._rawPrevTime===t?t:l);else if(1e-7>t)this._totalTime=this._time=0,this.ratio=this._ease._calcEnd?this._ease.getRatio(0):0,(0!==o||0===h&&this._rawPrevTime>0&&this._rawPrevTime!==l)&&(r="onReverseComplete",s=this._reversed),0>t?(this._active=!1,0===h&&(this._rawPrevTime>=0&&(i=!0),this._rawPrevTime=a=!e||t||this._rawPrevTime===t?t:l)):this._initted||(i=!0);else if(this._totalTime=this._time=t,this._easeType){var _=t/h,u=this._easeType,p=this._easePower;(1===u||3===u&&_>=.5)&&(_=1-_),3===u&&(_*=2),1===p?_*=_:2===p?_*=_*_:3===p?_*=_*_*_:4===p&&(_*=_*_*_*_),this.ratio=1===u?1-_:2===u?_:.5>t/h?_/2:1-_/2}else this.ratio=this._ease.getRatio(t/h);if(this._time!==o||i){if(!this._initted){if(this._init(),!this._initted||this._gc)return;this._time&&!s?this.ratio=this._ease.getRatio(this._time/h):s&&this._ease._calcEnd&&(this.ratio=this._ease.getRatio(0===this._time?0:1))}for(this._active||!this._paused&&this._time!==o&&t>=0&&(this._active=!0),0===o&&(this._startAt&&(t>=0?this._startAt.render(t,e,i):r||(r="_dummyGS")),this.vars.onStart&&(0!==this._time||0===h)&&(e||this.vars.onStart.apply(this.vars.onStartScope||this,this.vars.onStartParams||v))),n=this._firstPT;n;)n.f?n.t[n.p](n.c*this.ratio+n.s):n.t[n.p]=n.c*this.ratio+n.s,n=n._next;this._onUpdate&&(0>t&&this._startAt&&this._startTime&&this._startAt.render(t,e,i),e||(this._time!==o||s)&&this._onUpdate.apply(this.vars.onUpdateScope||this,this.vars.onUpdateParams||v)),r&&(this._gc||(0>t&&this._startAt&&!this._onUpdate&&this._startTime&&this._startAt.render(t,e,i),s&&(this._timeline.autoRemoveChildren&&this._enabled(!1,!1),this._active=!1),!e&&this.vars[r]&&this.vars[r].apply(this.vars[r+"Scope"]||this,this.vars[r+"Params"]||v),0===h&&this._rawPrevTime===l&&a!==l&&(this._rawPrevTime=0)))}},r._kill=function(t,e){if("all"===t&&(t=null),null==t&&(null==e||e===this.target))return this._enabled(!1,!1);e="string"!=typeof e?e||this._targets||this.target:O.selector(e)||e;var i,s,r,n,a,o,h,l;if((p(e)||D(e))&&"number"!=typeof e[0])for(i=e.length;--i>-1;)this._kill(t,e[i])&&(o=!0);else{if(this._targets){for(i=this._targets.length;--i>-1;)if(e===this._targets[i]){a=this._propLookup[i]||{},this._overwrittenProps=this._overwrittenProps||[],s=this._overwrittenProps[i]=t?this._overwrittenProps[i]||{}:"all";break}}else{if(e!==this.target)return!1;a=this._propLookup,s=this._overwrittenProps=t?this._overwrittenProps||{}:"all"}if(a){h=t||a,l=t!==s&&"all"!==s&&t!==a&&("object"!=typeof t||!t._tempKill);for(r in h)(n=a[r])&&(n.pg&&n.t._kill(h)&&(o=!0),n.pg&&0!==n.t._overwriteProps.length||(n._prev?n._prev._next=n._next:n===this._firstPT&&(this._firstPT=n._next),n._next&&(n._next._prev=n._prev),n._next=n._prev=null),delete a[r]),l&&(s[r]=1);!this._firstPT&&this._initted&&this._enabled(!1,!1)}}return o},r.invalidate=function(){return this._notifyPluginsOfEnabled&&O._onPluginEvent("_onDisable",this),this._firstPT=null,this._overwrittenProps=null,this._onUpdate=null,this._startAt=null,this._initted=this._active=this._notifyPluginsOfEnabled=!1,this._propLookup=this._targets?{}:[],this},r._enabled=function(t,e){if(a||n.wake(),t&&this._gc){var i,s=this._targets;if(s)for(i=s.length;--i>-1;)this._siblings[i]=Y(s[i],this,!0);else this._siblings=Y(this.target,this,!0)}return R.prototype._enabled.call(this,t,e),this._notifyPluginsOfEnabled&&this._firstPT?O._onPluginEvent(t?"_onEnable":"_onDisable",this):!1},O.to=function(t,e,i){return new O(t,e,i)},O.from=function(t,e,i){return i.runBackwards=!0,i.immediateRender=0!=i.immediateRender,new O(t,e,i)},O.fromTo=function(t,e,i,s){return s.startAt=i,s.immediateRender=0!=s.immediateRender&&0!=i.immediateRender,new O(t,e,s)},O.delayedCall=function(t,e,i,s,r){return new O(e,0,{delay:t,onComplete:e,onCompleteParams:i,onCompleteScope:s,onReverseComplete:e,onReverseCompleteParams:i,onReverseCompleteScope:s,immediateRender:!1,useFrames:r,overwrite:0})},O.set=function(t,e){return new O(t,0,e)},O.getTweensOf=function(t,e){if(null==t)return[];t="string"!=typeof t?t:O.selector(t)||t;var i,s,r,n;if((p(t)||D(t))&&"number"!=typeof t[0]){for(i=t.length,s=[];--i>-1;)s=s.concat(O.getTweensOf(t[i],e));for(i=s.length;--i>-1;)for(n=s[i],r=i;--r>-1;)n===s[r]&&s.splice(i,1)}else for(s=Y(t).concat(),i=s.length;--i>-1;)(s[i]._gc||e&&!s[i].isActive())&&s.splice(i,1);return s},O.killTweensOf=O.killDelayedCallsTo=function(t,e,i){"object"==typeof e&&(i=e,e=!1);for(var s=O.getTweensOf(t,e),r=s.length;--r>-1;)s[r]._kill(i,t)};var q=d("plugins.TweenPlugin",function(t,e){this._overwriteProps=(t||"").split(","),this._propName=this._overwriteProps[0],this._priority=e||0,this._super=q.prototype},!0);if(r=q.prototype,q.version="1.10.1",q.API=2,r._firstPT=null,r._addTween=function(t,e,i,s,r,n){var a,o;return null!=s&&(a="number"==typeof s||"="!==s.charAt(1)?Number(s)-i:parseInt(s.charAt(0)+"1",10)*Number(s.substr(2)))?(this._firstPT=o={_next:this._firstPT,t:t,p:e,s:i,c:a,f:"function"==typeof t[e],n:r||e,r:n},o._next&&(o._next._prev=o),o):void 0},r.setRatio=function(t){for(var e,i=this._firstPT,s=1e-6;i;)e=i.c*t+i.s,i.r?e=Math.round(e):s>e&&e>-s&&(e=0),i.f?i.t[i.p](e):i.t[i.p]=e,i=i._next},r._kill=function(t){var e,i=this._overwriteProps,s=this._firstPT;if(null!=t[this._propName])this._overwriteProps=[];else for(e=i.length;--e>-1;)null!=t[i[e]]&&i.splice(e,1);for(;s;)null!=t[s.n]&&(s._next&&(s._next._prev=s._prev),s._prev?(s._prev._next=s._next,s._prev=null):this._firstPT===s&&(this._firstPT=s._next)),s=s._next;return!1},r._roundProps=function(t,e){for(var i=this._firstPT;i;)(t[this._propName]||null!=i.n&&t[i.n.split(this._propName+"_").join("")])&&(i.r=e),i=i._next},O._onPluginEvent=function(t,e){var i,s,r,n,a,o=e._firstPT;if("_onInitAllProps"===t){for(;o;){for(a=o._next,s=r;s&&s.pr>o.pr;)s=s._next;(o._prev=s?s._prev:n)?o._prev._next=o:r=o,(o._next=s)?s._prev=o:n=o,o=a}o=e._firstPT=r}for(;o;)o.pg&&"function"==typeof o.t[t]&&o.t[t]()&&(i=!0),o=o._next;return i},q.activate=function(t){for(var e=t.length;--e>-1;)t[e].API===q.API&&(E[(new t[e])._propName]=t[e]);return!0},m.plugin=function(t){if(!(t&&t.propName&&t.init&&t.API))throw"illegal plugin definition.";var e,i=t.propName,s=t.priority||0,r=t.overwriteProps,n={init:"_onInitTween",set:"setRatio",kill:"_kill",round:"_roundProps",initAll:"_onInitAllProps"},a=d("plugins."+i.charAt(0).toUpperCase()+i.substr(1)+"Plugin",function(){q.call(this,i,s),this._overwriteProps=r||[]},t.global===!0),o=a.prototype=new q(i);o.constructor=a,a.API=t.API;for(e in n)"function"==typeof t[e]&&(o[n[e]]=t[e]);return a.version=t.version,q.activate([a]),a},i=t._gsQueue){for(s=0;i.length>s;s++)i[s]();for(r in c)c[r].func||t.console.log("GSAP encountered missing dependency: com.greensock."+r)}a=!1}}(window);
/*
 AngularJS v1.2.16
 (c) 2010-2014 Google, Inc. http://angularjs.org
 License: MIT
*/
(function(n,e,A){'use strict';function x(s,g,k){return{restrict:"ECA",terminal:!0,priority:400,transclude:"element",link:function(a,c,b,f,w){function y(){p&&(p.remove(),p=null);h&&(h.$destroy(),h=null);l&&(k.leave(l,function(){p=null}),p=l,l=null)}function v(){var b=s.current&&s.current.locals;if(e.isDefined(b&&b.$template)){var b=a.$new(),d=s.current;l=w(b,function(d){k.enter(d,null,l||c,function(){!e.isDefined(t)||t&&!a.$eval(t)||g()});y()});h=d.scope=b;h.$emit("$viewContentLoaded");h.$eval(u)}else y()}
var h,l,p,t=b.autoscroll,u=b.onload||"";a.$on("$routeChangeSuccess",v);v()}}}function z(e,g,k){return{restrict:"ECA",priority:-400,link:function(a,c){var b=k.current,f=b.locals;c.html(f.$template);var w=e(c.contents());b.controller&&(f.$scope=a,f=g(b.controller,f),b.controllerAs&&(a[b.controllerAs]=f),c.data("$ngControllerController",f),c.children().data("$ngControllerController",f));w(a)}}}n=e.module("ngRoute",["ng"]).provider("$route",function(){function s(a,c){return e.extend(new (e.extend(function(){},
{prototype:a})),c)}function g(a,e){var b=e.caseInsensitiveMatch,f={originalPath:a,regexp:a},k=f.keys=[];a=a.replace(/([().])/g,"\\$1").replace(/(\/)?:(\w+)([\?\*])?/g,function(a,e,b,c){a="?"===c?c:null;c="*"===c?c:null;k.push({name:b,optional:!!a});e=e||"";return""+(a?"":e)+"(?:"+(a?e:"")+(c&&"(.+?)"||"([^/]+)")+(a||"")+")"+(a||"")}).replace(/([\/$\*])/g,"\\$1");f.regexp=RegExp("^"+a+"$",b?"i":"");return f}var k={};this.when=function(a,c){k[a]=e.extend({reloadOnSearch:!0},c,a&&g(a,c));if(a){var b=
"/"==a[a.length-1]?a.substr(0,a.length-1):a+"/";k[b]=e.extend({redirectTo:a},g(b,c))}return this};this.otherwise=function(a){this.when(null,a);return this};this.$get=["$rootScope","$location","$routeParams","$q","$injector","$http","$templateCache","$sce",function(a,c,b,f,g,n,v,h){function l(){var d=p(),m=r.current;if(d&&m&&d.$$route===m.$$route&&e.equals(d.pathParams,m.pathParams)&&!d.reloadOnSearch&&!u)m.params=d.params,e.copy(m.params,b),a.$broadcast("$routeUpdate",m);else if(d||m)u=!1,a.$broadcast("$routeChangeStart",
d,m),(r.current=d)&&d.redirectTo&&(e.isString(d.redirectTo)?c.path(t(d.redirectTo,d.params)).search(d.params).replace():c.url(d.redirectTo(d.pathParams,c.path(),c.search())).replace()),f.when(d).then(function(){if(d){var a=e.extend({},d.resolve),c,b;e.forEach(a,function(d,c){a[c]=e.isString(d)?g.get(d):g.invoke(d)});e.isDefined(c=d.template)?e.isFunction(c)&&(c=c(d.params)):e.isDefined(b=d.templateUrl)&&(e.isFunction(b)&&(b=b(d.params)),b=h.getTrustedResourceUrl(b),e.isDefined(b)&&(d.loadedTemplateUrl=
b,c=n.get(b,{cache:v}).then(function(a){return a.data})));e.isDefined(c)&&(a.$template=c);return f.all(a)}}).then(function(c){d==r.current&&(d&&(d.locals=c,e.copy(d.params,b)),a.$broadcast("$routeChangeSuccess",d,m))},function(c){d==r.current&&a.$broadcast("$routeChangeError",d,m,c)})}function p(){var a,b;e.forEach(k,function(f,k){var q;if(q=!b){var g=c.path();q=f.keys;var l={};if(f.regexp)if(g=f.regexp.exec(g)){for(var h=1,p=g.length;h<p;++h){var n=q[h-1],r="string"==typeof g[h]?decodeURIComponent(g[h]):
g[h];n&&r&&(l[n.name]=r)}q=l}else q=null;else q=null;q=a=q}q&&(b=s(f,{params:e.extend({},c.search(),a),pathParams:a}),b.$$route=f)});return b||k[null]&&s(k[null],{params:{},pathParams:{}})}function t(a,c){var b=[];e.forEach((a||"").split(":"),function(a,d){if(0===d)b.push(a);else{var e=a.match(/(\w+)(.*)/),f=e[1];b.push(c[f]);b.push(e[2]||"");delete c[f]}});return b.join("")}var u=!1,r={routes:k,reload:function(){u=!0;a.$evalAsync(l)}};a.$on("$locationChangeSuccess",l);return r}]});n.provider("$routeParams",
function(){this.$get=function(){return{}}});n.directive("ngView",x);n.directive("ngView",z);x.$inject=["$route","$anchorScroll","$animate"];z.$inject=["$compile","$controller","$route"]})(window,window.angular);
//# sourceMappingURL=angular-route.min.js.map

!function(t){"use strict";t.module("fx.animations.assist",[]).factory("Assist",["$filter","$window","$timeout",function(a,n,e){return{emit:function(a,n,e){var r=t.element(a).scope();r.$emit(n+" "+e)},parseClassList:function(e){var r,i=e[0].classList,o={trigger:!1,duration:.3,ease:n.Back};return t.forEach(i,function(t){"fx-easing"===t.slice(0,9)&&(r=t.slice(10),o.ease=n[a("cap")(r)]?n[a("cap")(r)]:n.Elastic),"fx-trigger"===t&&(o.trigger=!0),"fx-speed"===t.slice(0,8)&&(o.duration=parseInt(t.slice(9))/1e3)}),o},addTimer:function(t,a,n){var r=this,i=t.stagger?3*t.duration*1e3:1e3*t.duration,o=e(function(){t.trigger&&r.emit(a,t.animation,t.motion),n()},i);a.data(t.timeoutKey,o)},removeTimer:function(t,a,n){e.cancel(n),t.removeData(a)}}}]).filter("cap",[function(){return function(t){return t.charAt(0).toUpperCase()+t.slice(1)}}])}(angular),function(t,a,n){"use strict";var e="$$fxTimer";t.module("fx.animations.create",["fx.animations.assist"]).factory("FadeAnimation",["$timeout","$window","Assist",function(t,n,r){return function(t){var n=t.enter,i=t.leave,o=t.inverse||t.leave,s=t.animation;this.enter=function(t,o){var m=r.parseClassList(t);return m.motion="enter",m.animation=s,m.timeoutKey=e,r.addTimer(m,t,o),n.ease=m.ease.easeOut,a.set(t,i),a.to(t,m.duration,n),function(a){var n=t.data(e);a&&n&&r.removeTimer(t,e,n)}},this.leave=function(t,i){var m=r.parseClassList(t);return m.motion="leave",m.animation=s,m.timeoutKey=e,r.addTimer(m,t,i),o.ease=m.ease.easeIn,a.set(t,n),a.to(t,m.duration,o),function(a){var n=t.data(e);a&&n&&r.removeTimer(t,e,n)}},this.move=this.enter,this.addClass=function(t,n,i){if("ng-hide"===n){var m=r.parseClassList(t);return m.motion="addClass",m.animation=s,m.timeoutKey=e,r.addTimer(m,t,i),a.to(t,m.duration,o),function(a){if(a){var n=t.data(e);n&&r.removeTimer(t,e,n)}}}i()},this.removeClass=function(t,o,m){if("ng-hide"===o){var f=r.parseClassList(t);return f.motion="removeClass",f.animation=s,f.timeoutKey=e,a.set(t,i),a.to(t,f.duration,n),function(a){if(a){var n=t.data(e);n&&r.removeTimer(t,e,n)}}}m()}}}]).factory("BounceAnimation",["$timeout","$window","Assist",function(t,a,r){return function(t){var a=t.first,i=t.mid,o=t.third,s=t.end,m=t.animation,f=.1;this.enter=function(t,c){var u=r.parseClassList(t);u.motion="enter",u.animation=m,u.timeoutKey=e,u.stagger=!0,s.ease=u.ease.easeOut,r.addTimer(u,t,c);var d=new n;return d.to(t,f,a),d.to(t,u.duration,i),d.to(t,u.duration,o),d.to(t,u.duration,s),function(a){if(a){var n=t.data(e);n&&r.removeTimer(t,e,n)}}},this.leave=function(t,c){var u=r.parseClassList(t);u.motion="leave",u.animation=m,u.timeoutKey=e,u.stagger=!0,a.ease=u.ease.easeIn,r.addTimer(u,t,c);var d=new n;return d.to(t,f,s),d.to(t,u.duration,o),d.to(t,u.duration,i),d.to(t,u.duration,a),function(a){if(a){var n=t.data(e);n&&r.removeTimer(t,e,n)}}},this.move=this.enter,this.addClass=function(t,c,u){if("ng-hide"===c){var d=r.parseClassList(t);d.motion="addClass",d.animation=m,d.timeoutKey=e,r.addTimer(d,t,u);var l=new n;return l.to(t,f,s),l.to(t,d.duration,o),l.to(t,d.duration,i),l.to(t,d.duration,a),function(a){if(a){var n=t.data(e);n&&r.removeTimer(t,e,n)}}}u()},this.removeClass=function(t,c,u){if("ng-hide"===c){var d=r.parseClassList(t);d.motion="removeClass",d.animation=m,d.timeoutKey=e;var l=new n;return l.to(t,f,a),l.to(t,d.duration,i),l.to(t,d.duration,o),l.to(t,d.duration,s),function(a){if(a){var n=t.data(e);n&&r.removeTimer(t,e,n)}}}u()}}}]).factory("RotateAnimation",["$timeout","$window","Assist",function(t,n,r){return function(t){var n=t.start,i=t.end,o=t.inverse,s=t.animation;this.enter=function(t,o){var m=r.parseClassList(t);return m.motion="enter",m.animation=s,m.timeoutKey=e,i.ease=m.ease.easeOut,r.addTimer(m,t,o),a.set(t,n),a.to(t,m.duration,i),function(a){if(a){var n=t.data(e);n&&r.removeTimer(t,e,n)}}},this.leave=function(t,n){var m=r.parseClassList(t);return m.motion="leave",m.animation=s,m.timeoutKey=e,o.ease=m.ease.easeIn,r.addTimer(m,t,n),a.set(t,i),a.to(t,m.duration,o),function(a){if(a){var n=t.data(e);n&&r.removeTimer(t,e,n)}}},this.move=this.enter,this.addClass=function(t,o,m){if("ng-hide"===o){var f=r.parseClassList(t);return f.motion="addClass",f.animation=s,f.timeoutKey=e,r.addTimer(f,t,m),a.set(t,i),a.to(t,f.duration,n),function(a){if(a){var n=t.data(e);n&&r.removeTimer(t,e,n)}}}m()},this.removeClass=function(t,o,m){if("ng-hide"===o){var f=r.parseClassList(t);return f.motion="addClass",f.animation=s,f.timeoutKey=e,r.addTimer(f,t,m),a.set(t,n),a.to(t,f.duration,i),function(a){if(a){var n=t.data(e);n&&r.removeTimer(t,e,n)}}}m()}}}]).factory("ZoomAnimation",["$timeout","$window","Assist",function(t,n,r){return function(t){var n=t.start,i=t.end,o=t.animation;this.enter=function(t,s){var m=r.parseClassList(t);return m.motion="enter",m.animation=o,m.timeoutKey=e,i.ease=m.ease.easeOut,r.addTimer(m,t,s),a.set(t,n),a.to(t,m.duration,i),function(a){if(a){var n=t.data(e);n&&r.removeTimer(t,e,n)}}},this.leave=function(t,s){var m=r.parseClassList(t);return m.motion="lave",m.animation=o,m.timeoutKey=e,n.ease=m.ease.easeIn,r.addTimer(m,t,s),a.set(t,i),a.to(t,m.duration,n),function(a){if(a){var n=t.data(e);n&&r.removeTimer(t,e,n)}}},this.move=this.enter,this.removeClass=function(t,s,m){if("ng-hide"===s){var f=r.parseClassList(t);return f.motion="addClass",f.animation=o,f.timeoutKey=e,r.addTimer(f,t,m),a.set(t,n),a.to(t,f.duration,i),function(a){if(a){var n=t.data(e);n&&r.removeTimer(t,e,n)}}}m()},this.addClass=function(t,s,m){if("ng-hide"===s){var f=r.parseClassList(t);return f.motion="addClass",f.animation=o,f.timeoutKey=e,r.addTimer(f,t,m),a.set(t,i),a.to(t,f.duration,n),function(a){if(a){var n=t.data(e);n&&r.removeTimer(t,e,n)}}}m()}}}])}(angular,TweenMax,TimelineMax),function(t){"use strict";t.module("fx.animations.bounces",["fx.animations.create"]).animation(".fx-bounce-normal",["BounceAnimation",function(t){var a={first:{opacity:0,transform:"scale(.3)"},mid:{opacity:1,transform:"scale(1.05)"},third:{transform:"scale(.9)"},end:{opacity:1,transform:"scale(1)"},animation:"bounce-normal"};return new t(a)}]).animation(".fx-bounce-down",["BounceAnimation",function(t){var a={first:{opacity:0,transform:"translateY(-2000px)"},mid:{opacity:1,transform:"translateY(30px)"},third:{transform:"translateY(-10px)"},end:{transform:"translateY(0)"},animation:"bounce-down"};return new t(a)}]).animation(".fx-bounce-left",["BounceAnimation",function(t){var a={first:{opacity:0,transform:"translateX(-2000px)"},mid:{opacity:1,transform:"translateX(30px)"},third:{transform:"translateX(-10px)"},end:{transform:"translateX(0)"},animation:"bounce-left"};return new t(a)}]).animation(".fx-bounce-up",["BounceAnimation",function(t){var a={first:{opacity:0,transform:"translateY(2000px)"},mid:{opacity:1,transform:"translateY(-30px)"},third:{transform:"translateY(10px)"},end:{transform:"translateY(0)"},animation:"bounce-up"};return new t(a)}]).animation(".fx-bounce-right",["BounceAnimation",function(t){var a={first:{opacity:0,transform:"translateX(2000px)"},mid:{opacity:1,transform:"translateX(-30px)"},third:{transform:"translateX(10px)"},end:{transform:"translateX(0)"},animation:"bounce-right"};return new t(a)}])}(angular),function(t){"use strict";t.module("fx.animations.fades",["fx.animations.create"]).animation(".fx-fade-normal",["FadeAnimation",function(t){var a={enter:{opacity:1},leave:{opacity:0},animation:"fade-normal"};return new t(a)}]).animation(".fx-fade-down",["FadeAnimation",function(t){var a={enter:{opacity:1,transform:"translateY(0)"},leave:{opacity:0,transform:"translateY(-20px)"},inverse:{opacity:0,transform:"translateY(20px)"},animation:"fade-down"};return new t(a)}]).animation(".fx-fade-down-big",["FadeAnimation",function(t){var a={enter:{opacity:1,transform:"translateY(0)"},leave:{opacity:0,transform:"translateY(-2000px)"},inverse:{opacity:0,transform:"translateY(2000px)"},animation:"fade-down-big"};return new t(a)}]).animation(".fx-fade-left",["FadeAnimation",function(t){var a={enter:{opacity:1,transform:"translateX(0)"},leave:{opacity:0,transform:"translateX(-20px)"},inverse:{opacity:0,transform:"translateX(20px)"},animation:"fade-left"};return new t(a)}]).animation(".fx-fade-left-big",["FadeAnimation",function(t){var a={enter:{opacity:1,transform:"translateX(0)"},leave:{opacity:0,transform:"translateX(-2000px)"},inverse:{opacity:0,transform:"translateX(2000px)"},animation:"fade-left-big"};return new t(a)}]).animation(".fx-fade-right",["FadeAnimation",function(t){var a={enter:{opacity:1,transform:"translateX(0)"},leave:{opacity:0,transform:"translateX(20px)"},inverse:{opacity:0,transform:"translateX(-20px)"},animation:"fade-right"};return new t(a)}]).animation(".fx-fade-right-big",["FadeAnimation",function(t){var a={enter:{opacity:1,transform:"translateX(0)"},leave:{opacity:0,transform:"translateX(2000px)"},inverse:{opacity:0,transform:"translateX(-2000px)"},animation:"fade-right-big"};return new t(a)}]).animation(".fx-fade-up",["FadeAnimation",function(t){var a={enter:{opacity:1,transform:"translateY(0)"},leave:{opacity:0,transform:"translateY(20px)"},inverse:{opacity:0,transform:"translateY(-20px)"},animation:"fade-up"};return new t(a)}]).animation(".fx-fade-up-big",["FadeAnimation",function(t){var a={enter:{opacity:1,transform:"translateY(0)"},leave:{opacity:0,transform:"translateY(2000px)"},inverse:{opacity:0,transform:"translateY(-2000px)"},animation:"fade-up-big"};return new t(a)}])}(angular),function(t){"use strict";t.module("fx.animations.rotations",["fx.animations.create"]).animation(".fx-rotate-counterclock",["RotateAnimation",function(t){var a={start:{opacity:0,transformOrigin:"center center",transform:"rotate(-200deg)"},end:{opacity:1,transformOrigin:"center center",transform:"rotate(0)"},inverse:{opacity:0,transformOrigin:"center center",transform:"rotate(200deg)"},animation:"rotate-counterclock"};return new t(a)}]).animation(".fx-rotate-clock",["RotateAnimation",function(t){var a={start:{opacity:0,transformOrigin:"center center",transform:"rotate(200deg)"},end:{opacity:1,transformOrigin:"center center",transform:"rotate(0)"},inverse:{opacity:0,transformOrigin:"center center",transform:"rotate(-200deg)"},animation:"rotate-clock"};return new t(a)}]).animation(".fx-rotate-clock-left",["RotateAnimation",function(t){var a={start:{opacity:0,transformOrigin:"left bottom",transform:"rotate(-90deg)"},end:{opacity:1,transformOrigin:"left bottom",transform:"rotate(0)"},inverse:{opacity:0,transformOrigin:"left bottom",transform:"rotate(90deg)"},animation:"rotate-clock-left"};return new t(a)}]).animation(".fx-rotate-counterclock-right",["RotateAnimation",function(t){var a={start:{opacity:0,transformOrigin:"right bottom",transform:"rotate(90deg)"},end:{opacity:1,transformOrigin:"right bottom",transform:"rotate(0)"},inverse:{opacity:0,transformOrigin:"right bottom",transform:"rotate(-90deg)"},animation:"rotate-counterclock-right"};return new t(a)}]).animation(".fx-rotate-counterclock-up",["RotateAnimation",function(t){var a={start:{opacity:0,transformOrigin:"left bottom",transform:"rotate(90deg)"},end:{opacity:1,transformOrigin:"left bottom",transform:"rotate(0)"},inverse:{opacity:0,transformOrigin:"left bottom",transform:"rotate(-90deg)"},animation:"rotate-counterclock-up"};return new t(a)}]).animation(".fx-rotate-clock-up",["RotateAnimation",function(t){var a={start:{opacity:0,transformOrigin:"right bottom",transform:"rotate(-90deg)"},end:{opacity:1,transformOrigin:"right bottom",transform:"rotate(0)"},inverse:{opacity:0,transformOrigin:"right bottom",transform:"rotate(90deg)"},animation:"rotate-clock-up"};return new t(a)}])}(angular),function(t){"use strict";t.module("fx.animations.zooms",["fx.animations.create"]).animation(".fx-zoom-normal",["ZoomAnimation",function(t){var a={start:{opacity:0,transform:"scale(.3)"},end:{opacity:1,transform:"scale(1)"},animation:"zoom-normal"};return new t(a)}]).animation(".fx-zoom-down",["ZoomAnimation",function(t){var a={start:{opacity:0,transform:"scale(.1) translateY(-2000px)"},end:{opacity:1,transform:"scale(1) translateY(0)"},animation:"zoom-down"};return new t(a)}]).animation(".fx-zoom-up",["ZoomAnimation",function(t){var a={start:{opacity:0,transform:"scale(.1) translateY(2000px)"},end:{opacity:1,transform:"scale(1) translateY(0)"},animation:"zoom-up"};return new t(a)}])}(angular),function(t){"use strict";t.module("fx.animations",["fx.animations.fades","fx.animations.bounces","fx.animations.rotations","fx.animations.zooms"])}(angular);
/**
 * @license Angular UI Tree v2.0.6
 * (c) 2010-2014. https://github.com/JimLiu/angular-ui-tree
 * License: MIT
 */
!function(){"use strict";angular.module("ui.tree",[]).constant("treeConfig",{treeClass:"angular-ui-tree",emptyTreeClass:"angular-ui-tree-empty",hiddenClass:"angular-ui-tree-hidden",nodesClass:"angular-ui-tree-nodes",nodeClass:"angular-ui-tree-node",handleClass:"angular-ui-tree-handle",placeHolderClass:"angular-ui-tree-placeholder",dragClass:"angular-ui-tree-drag",dragThreshold:3,levelThreshold:30})}(),function(){"use strict";angular.module("ui.tree").factory("$uiTreeHelper",["$document","$window",function($document,$window){return{nodrag:function(targetElm){return"undefined"!=typeof targetElm.attr("data-nodrag")},eventObj:function(e){var obj=e;return void 0!==e.targetTouches?obj=e.targetTouches.item(0):void 0!==e.originalEvent&&void 0!==e.originalEvent.targetTouches&&(obj=e.originalEvent.targetTouches.item(0)),obj},dragInfo:function(node){return{source:node,sourceInfo:{nodeScope:node,index:node.index(),nodesScope:node.$parentNodesScope},index:node.index(),siblings:node.$parentNodesScope.$nodes.slice(0),parent:node.$parentNodesScope,moveTo:function(parent,siblings,index){this.parent=parent,this.siblings=siblings.slice(0);var i=this.siblings.indexOf(this.source);i>-1&&(this.siblings.splice(i,1),this.source.index()<index&&index--),this.siblings.splice(index,0,this.source),this.index=index},parentNode:function(){return this.parent.$nodeScope},prev:function(){return this.index>0?this.siblings[this.index-1]:null},next:function(){return this.index<this.siblings.length-1?this.siblings[this.index+1]:null},isDirty:function(){return this.source.$parentNodesScope!=this.parent||this.source.index()!=this.index},eventArgs:function(elements,pos){return{source:this.sourceInfo,dest:{index:this.index,nodesScope:this.parent},elements:elements,pos:pos}},apply:function(){var nodeData=this.source.$modelValue;this.source.remove(),this.parent.insertNode(this.index,nodeData)}}},height:function(element){return element.prop("scrollHeight")},width:function(element){return element.prop("scrollWidth")},offset:function(element){var boundingClientRect=element[0].getBoundingClientRect();return{width:element.prop("offsetWidth"),height:element.prop("offsetHeight"),top:boundingClientRect.top+($window.pageYOffset||$document[0].body.scrollTop||$document[0].documentElement.scrollTop),left:boundingClientRect.left+($window.pageXOffset||$document[0].body.scrollLeft||$document[0].documentElement.scrollLeft)}},positionStarted:function(e,target){var pos={};return pos.offsetX=e.pageX-this.offset(target).left,pos.offsetY=e.pageY-this.offset(target).top,pos.startX=pos.lastX=e.pageX,pos.startY=pos.lastY=e.pageY,pos.nowX=pos.nowY=pos.distX=pos.distY=pos.dirAx=0,pos.dirX=pos.dirY=pos.lastDirX=pos.lastDirY=pos.distAxX=pos.distAxY=0,pos},positionMoved:function(e,pos,firstMoving){pos.lastX=pos.nowX,pos.lastY=pos.nowY,pos.nowX=e.pageX,pos.nowY=e.pageY,pos.distX=pos.nowX-pos.lastX,pos.distY=pos.nowY-pos.lastY,pos.lastDirX=pos.dirX,pos.lastDirY=pos.dirY,pos.dirX=0===pos.distX?0:pos.distX>0?1:-1,pos.dirY=0===pos.distY?0:pos.distY>0?1:-1;var newAx=Math.abs(pos.distX)>Math.abs(pos.distY)?1:0;return firstMoving?(pos.dirAx=newAx,void(pos.moving=!0)):(pos.dirAx!==newAx?(pos.distAxX=0,pos.distAxY=0):(pos.distAxX+=Math.abs(pos.distX),0!==pos.dirX&&pos.dirX!==pos.lastDirX&&(pos.distAxX=0),pos.distAxY+=Math.abs(pos.distY),0!==pos.dirY&&pos.dirY!==pos.lastDirY&&(pos.distAxY=0)),void(pos.dirAx=newAx))}}}])}(),function(){"use strict";angular.module("ui.tree").controller("TreeController",["$scope","$element","$attrs","treeConfig",function($scope,$element){this.scope=$scope,$scope.$element=$element,$scope.$nodesScope=null,$scope.$type="uiTree",$scope.$emptyElm=null,$scope.$callbacks=null,$scope.dragEnabled=!0,$scope.maxDepth=0,$scope.isEmpty=function(){return $scope.$nodesScope&&$scope.$nodesScope.$modelValue&&0===$scope.$nodesScope.$modelValue.length},$scope.place=function(placeElm){$scope.$nodesScope.$element.append(placeElm),$scope.$emptyElm.remove()},$scope.resetEmptyElement=function(){0===$scope.$nodesScope.$modelValue.length?$element.append($scope.$emptyElm):$scope.$emptyElm.remove()};var collapseOrExpand=function(scope,collapsed){for(var i=0;i<scope.$nodes.length;i++){collapsed?scope.$nodes[i].collapse():scope.$nodes[i].expand();var subScope=scope.$nodes[i].$childNodesScope;subScope&&collapseOrExpand(subScope,collapsed)}};$scope.collapseAll=function(){collapseOrExpand($scope.$nodesScope,!0)},$scope.expandAll=function(){collapseOrExpand($scope.$nodesScope,!1)}}])}(),function(){"use strict";angular.module("ui.tree").controller("TreeNodesController",["$scope","$element","treeConfig",function($scope,$element){this.scope=$scope,$scope.$element=$element,$scope.$modelValue=null,$scope.$nodes=[],$scope.$nodeScope=null,$scope.$treeScope=null,$scope.$type="uiTreeNodes",$scope.nodrop=!1,$scope.maxDepth=0,$scope.initSubNode=function(subNode){$scope.$nodes.splice(subNode.index(),0,subNode)},$scope.accept=function(sourceNode,destIndex){return $scope.$treeScope.$callbacks.accept(sourceNode,$scope,destIndex)},$scope.isParent=function(node){return node.$parentNodesScope==$scope},$scope.hasChild=function(){return $scope.$nodes.length>0},$scope.safeApply=function(fn){var phase=this.$root.$$phase;"$apply"==phase||"$digest"==phase?fn&&"function"==typeof fn&&fn():this.$apply(fn)},$scope.removeNode=function(node){var index=$scope.$nodes.indexOf(node);return index>-1?($scope.safeApply(function(){$scope.$modelValue.splice(index,1)[0],$scope.$nodes.splice(index,1)[0]}),node):null},$scope.insertNode=function(index,nodeData){$scope.safeApply(function(){$scope.$modelValue.splice(index,0,nodeData)})},$scope.depth=function(){return $scope.$nodeScope?$scope.$nodeScope.depth():0},$scope.outOfDepth=function(sourceNode){var maxDepth=$scope.maxDepth||$scope.$treeScope.maxDepth;return maxDepth>0?$scope.depth()+sourceNode.maxSubDepth()+1>maxDepth:!1}}])}(),function(){"use strict";angular.module("ui.tree").controller("TreeNodeController",["$scope","$element","$attrs","treeConfig",function($scope,$element){this.scope=$scope,$scope.$element=$element,$scope.$modelValue=null,$scope.$parentNodeScope=null,$scope.$childNodesScope=null,$scope.$parentNodesScope=null,$scope.$treeScope=null,$scope.$handleScope=null,$scope.$type="uiTreeNode",$scope.$$apply=!1,$scope.collapsed=!1,$scope.init=function(controllersArr){var treeNodesCtrl=controllersArr[0];$scope.$treeScope=controllersArr[1]?controllersArr[1].scope:null,$scope.$parentNodeScope=treeNodesCtrl.scope.$nodeScope,$scope.$modelValue=treeNodesCtrl.scope.$modelValue[$scope.$index],$scope.$parentNodesScope=treeNodesCtrl.scope,treeNodesCtrl.scope.initSubNode($scope),$element.on("$destroy",function(){})},$scope.index=function(){return $scope.$parentNodesScope.$modelValue.indexOf($scope.$modelValue)},$scope.dragEnabled=function(){return!($scope.$treeScope&&!$scope.$treeScope.dragEnabled)},$scope.isSibling=function(targetNode){return $scope.$parentNodesScope==targetNode.$parentNodesScope},$scope.isChild=function(targetNode){var nodes=$scope.childNodes();return nodes&&nodes.indexOf(targetNode)>-1},$scope.prev=function(){var index=$scope.index();return index>0?$scope.siblings()[index-1]:null},$scope.siblings=function(){return $scope.$parentNodesScope.$nodes},$scope.childNodesCount=function(){return $scope.childNodes()?$scope.childNodes().length:0},$scope.hasChild=function(){return $scope.childNodesCount()>0},$scope.childNodes=function(){return $scope.$childNodesScope?$scope.$childNodesScope.$nodes:null},$scope.accept=function(sourceNode,destIndex){return $scope.$childNodesScope&&$scope.$childNodesScope.accept(sourceNode,destIndex)},$scope.remove=function(){return $scope.$parentNodesScope.removeNode($scope)},$scope.toggle=function(){$scope.collapsed=!$scope.collapsed},$scope.collapse=function(){$scope.collapsed=!0},$scope.expand=function(){$scope.collapsed=!1},$scope.depth=function(){var parentNode=$scope.$parentNodeScope;return parentNode?parentNode.depth()+1:1};var subDepth=0,countSubDepth=function(scope){for(var count=0,i=0;i<scope.$nodes.length;i++){var childNodes=scope.$nodes[i].$childNodesScope;childNodes&&(count=1,countSubDepth(childNodes))}subDepth+=count};$scope.maxSubDepth=function(){return subDepth=0,$scope.$childNodesScope&&countSubDepth($scope.$childNodesScope),subDepth}}])}(),function(){"use strict";angular.module("ui.tree").controller("TreeHandleController",["$scope","$element","$attrs","treeConfig",function($scope,$element){this.scope=$scope,$scope.$element=$element,$scope.$nodeScope=null,$scope.$type="uiTreeHandle"}])}(),function(){"use strict";angular.module("ui.tree").directive("uiTree",["treeConfig","$window",function(treeConfig,$window){return{restrict:"A",scope:!0,controller:"TreeController",link:function(scope,element,attrs){var callbacks={accept:null},config={};angular.extend(config,treeConfig),config.treeClass&&element.addClass(config.treeClass),scope.$emptyElm=angular.element($window.document.createElement("div")),config.emptyTreeClass&&scope.$emptyElm.addClass(config.emptyTreeClass),scope.$watch("$nodesScope.$modelValue.length",function(){scope.$nodesScope.$modelValue&&scope.resetEmptyElement()},!0),scope.$watch(function(){return scope.$eval(attrs.dragEnabled)},function(newVal){"boolean"==typeof newVal&&(scope.dragEnabled=newVal)},!0),scope.$watch(function(){return scope.$eval(attrs.maxDepth)},function(newVal){"number"==typeof newVal&&(scope.maxDepth=newVal)},!0),callbacks.accept=function(sourceNodeScope,destNodesScope){return destNodesScope.nodrop||destNodesScope.outOfDepth(sourceNodeScope)?!1:!0},callbacks.dropped=function(){},callbacks.dragStart=function(){},callbacks.dragMove=function(){},callbacks.dragStop=function(){},scope.$watch(attrs.uiTree,function(newVal){angular.forEach(newVal,function(value,key){callbacks[key]&&"function"==typeof value&&(callbacks[key]=value)}),scope.$callbacks=callbacks},!0)}}}])}(),function(){"use strict";angular.module("ui.tree").directive("uiTreeNodes",["treeConfig","$window",function(treeConfig){return{require:["ngModel","?^uiTreeNode","^uiTree"],restrict:"A",scope:!0,controller:"TreeNodesController",link:function(scope,element,attrs,controllersArr){var config={};angular.extend(config,treeConfig),config.nodesClass&&element.addClass(config.nodesClass);var ngModel=controllersArr[0],treeNodeCtrl=controllersArr[1],treeCtrl=controllersArr[2];treeNodeCtrl?(treeNodeCtrl.scope.$childNodesScope=scope,scope.$nodeScope=treeNodeCtrl.scope):treeCtrl.scope.$nodesScope=scope,scope.$treeScope=treeCtrl.scope,ngModel&&(ngModel.$render=function(){scope.$modelValue=ngModel.$modelValue}),scope.$watch(function(){return scope.$eval(attrs.maxDepth)},function(newVal){"number"==typeof newVal&&(scope.maxDepth=newVal)},!0),scope.$watch(function(){return scope.$eval(attrs.nodrop)},function(newVal){"boolean"==typeof newVal&&(scope.nodrop=newVal)},!0)}}}])}(),function(){"use strict";angular.module("ui.tree").directive("uiTreeNode",["treeConfig","$uiTreeHelper","$window","$document",function(treeConfig,$uiTreeHelper,$window,$document){return{require:["^uiTreeNodes","^uiTree"],restrict:"A",controller:"TreeNodeController",link:function(scope,element,attrs,controllersArr){var config={};angular.extend(config,treeConfig),config.nodeClass&&element.addClass(config.nodeClass),scope.init(controllersArr);var firstMoving,dragInfo,pos,placeElm,hiddenPlaceElm,dragElm,elements,hasTouch="ontouchstart"in window,treeScope=null,dragStart=function(e){if((hasTouch||2!=e.button&&3!=e.which)&&!(e.uiTreeDragging||e.originalEvent&&e.originalEvent.uiTreeDragging)){var eventElm=angular.element(e.target),eventScope=eventElm.scope();if(eventScope&&eventScope.$type&&!("uiTreeNode"!=eventScope.$type&&"uiTreeHandle"!=eventScope.$type||"uiTreeNode"==eventScope.$type&&eventScope.$handleScope)){for(;eventElm&&eventElm[0]&&eventElm[0]!=element;){if($uiTreeHelper.nodrag(eventElm))return;eventElm=eventElm.parent()}e.uiTreeDragging=!0,e.originalEvent&&(e.originalEvent.uiTreeDragging=!0),e.preventDefault();var eventObj=$uiTreeHelper.eventObj(e);firstMoving=!0,dragInfo=$uiTreeHelper.dragInfo(scope);var tagName=scope.$element.prop("tagName");if("tr"===tagName.toLowerCase()){placeElm=angular.element($window.document.createElement(tagName));var tdElm=angular.element($window.document.createElement("td")).addClass(config.placeHolderClass);placeElm.append(tdElm)}else placeElm=angular.element($window.document.createElement(tagName)).addClass(config.placeHolderClass);hiddenPlaceElm=angular.element($window.document.createElement(tagName)),config.hiddenClass&&hiddenPlaceElm.addClass(config.hiddenClass),pos=$uiTreeHelper.positionStarted(eventObj,scope.$element),placeElm.css("height",$uiTreeHelper.height(scope.$element)+"px"),dragElm=angular.element($window.document.createElement(scope.$parentNodesScope.$element.prop("tagName"))).addClass(scope.$parentNodesScope.$element.attr("class")).addClass(config.dragClass),dragElm.css("width",$uiTreeHelper.width(scope.$element)+"px"),dragElm.css("z-index",9999),scope.$element.after(placeElm),scope.$element.after(hiddenPlaceElm),dragElm.append(scope.$element),$document.find("body").append(dragElm),dragElm.css({left:eventObj.pageX-pos.offsetX+"px",top:eventObj.pageY-pos.offsetY+"px"}),elements={placeholder:placeElm,dragging:dragElm},scope.$apply(function(){scope.$callbacks.dragStart(dragInfo.eventArgs(elements,pos))}),angular.element($document).bind("touchend",dragEndEvent),angular.element($document).bind("touchcancel",dragEndEvent),angular.element($document).bind("touchmove",dragMoveEvent),angular.element($document).bind("mouseup",dragEndEvent),angular.element($document).bind("mousemove",dragMoveEvent),angular.element($window.document.body).bind("mouseleave",dragCancelEvent)}}},dragMove=function(e){var prev,eventObj=$uiTreeHelper.eventObj(e);if(dragElm){if(e.preventDefault(),dragElm.css({left:eventObj.pageX-pos.offsetX+"px",top:eventObj.pageY-pos.offsetY+"px"}),$uiTreeHelper.positionMoved(e,pos,firstMoving),firstMoving)return void(firstMoving=!1);if(pos.dirAx&&pos.distAxX>=config.levelThreshold&&(pos.distAxX=0,pos.distX>0&&(prev=dragInfo.prev(),prev&&!prev.collapsed&&prev.accept(scope,prev.childNodesCount())&&(prev.$childNodesScope.$element.append(placeElm),dragInfo.moveTo(prev.$childNodesScope,prev.childNodes(),prev.childNodesCount()))),pos.distX<0)){var next=dragInfo.next();if(!next){var target=dragInfo.parentNode();target&&target.$parentNodesScope.accept(scope,target.index()+1)&&(target.$element.after(placeElm),dragInfo.moveTo(target.$parentNodesScope,target.siblings(),target.index()+1))}}var targetX=($uiTreeHelper.offset(dragElm).left-$uiTreeHelper.offset(placeElm).left>=config.threshold,eventObj.pageX-$window.document.body.scrollLeft),targetY=eventObj.pageY-(window.pageYOffset||$window.document.documentElement.scrollTop);angular.isFunction(dragElm.hide)&&dragElm.hide(),$window.document.elementFromPoint(targetX,targetY);var targetElm=angular.element($window.document.elementFromPoint(targetX,targetY));if(angular.isFunction(dragElm.show)&&dragElm.show(),!pos.dirAx){var targetBefore,targetNode;targetNode=targetElm.scope();var isEmpty=!1;if("uiTree"==targetNode.$type&&targetNode.dragEnabled&&(isEmpty=targetNode.isEmpty()),"uiTreeHandle"==targetNode.$type&&(targetNode=targetNode.$nodeScope),"uiTreeNode"!=targetNode.$type&&!isEmpty)return;if(treeScope&&placeElm.parent()[0]!=treeScope.$element[0]&&(treeScope.resetEmptyElement(),treeScope=null),isEmpty)treeScope=targetNode,targetNode.$nodesScope.accept(scope,0)&&(targetNode.place(placeElm),dragInfo.moveTo(targetNode.$nodesScope,targetNode.$nodesScope.$nodes,0));else if(targetNode.dragEnabled()){targetElm=targetNode.$element;var targetOffset=$uiTreeHelper.offset(targetElm);targetBefore=eventObj.pageY<targetOffset.top+$uiTreeHelper.height(targetElm)/2,targetNode.$parentNodesScope.accept(scope,targetNode.index())?targetBefore?(targetElm[0].parentNode.insertBefore(placeElm[0],targetElm[0]),dragInfo.moveTo(targetNode.$parentNodesScope,targetNode.siblings(),targetNode.index())):(targetElm.after(placeElm),dragInfo.moveTo(targetNode.$parentNodesScope,targetNode.siblings(),targetNode.index()+1)):!targetBefore&&targetNode.accept(scope,targetNode.childNodesCount())&&(targetNode.$childNodesScope.$element.append(placeElm),dragInfo.moveTo(targetNode.$childNodesScope,targetNode.childNodes(),targetNode.childNodesCount()))}}scope.$apply(function(){scope.$callbacks.dragMove(dragInfo.eventArgs(elements,pos))})}},dragEnd=function(e){e.preventDefault(),dragElm&&(hiddenPlaceElm.replaceWith(scope.$element),placeElm.remove(),dragElm.remove(),dragElm=null,scope.$$apply?(dragInfo.apply(),scope.$treeScope.$apply(function(){scope.$callbacks.dropped(dragInfo.eventArgs(elements,pos))})):bindDrag(),scope.$treeScope.$apply(function(){scope.$callbacks.dragStop(dragInfo.eventArgs(elements,pos))}),scope.$$apply=!1,dragInfo=null),angular.element($document).unbind("touchend",dragEndEvent),angular.element($document).unbind("touchcancel",dragEndEvent),angular.element($document).unbind("touchmove",dragMoveEvent),angular.element($document).unbind("mouseup",dragEndEvent),angular.element($document).unbind("mousemove",dragMoveEvent),angular.element($window.document.body).unbind("mouseleave",dragCancelEvent)},dragStartEvent=function(e){scope.dragEnabled()&&dragStart(e)},dragMoveEvent=function(e){dragMove(e)},dragEndEvent=function(e){scope.$$apply=!0,dragEnd(e)},dragCancelEvent=function(e){dragEnd(e)},bindDrag=function(){element.bind("touchstart",dragStartEvent),element.bind("mousedown",dragStartEvent)};bindDrag(),angular.element($window.document.body).bind("keydown",function(e){27==e.keyCode&&(scope.$$apply=!1,dragEnd(e))})}}}])}(),function(){"use strict";angular.module("ui.tree").directive("uiTreeHandle",["treeConfig","$window",function(treeConfig){return{require:"^uiTreeNode",restrict:"A",scope:!0,controller:"TreeHandleController",link:function(scope,element,attrs,treeNodeCtrl){var config={};angular.extend(config,treeConfig),config.handleClass&&element.addClass(config.handleClass),scope!=treeNodeCtrl.scope&&(scope.$nodeScope=treeNodeCtrl.scope,treeNodeCtrl.scope.$handleScope=scope)}}}])}();
/**
 * angular-strap
 * @version v2.0.2 - 2014-04-27
 * @link http://mgcrea.github.io/angular-strap
 * @author Olivier Louvignes (olivier@mg-crea.com)
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */
(function(window, document, undefined) {
'use strict';
// Source: module.js
angular.module('mgcrea.ngStrap', [
  'mgcrea.ngStrap.modal',
  'mgcrea.ngStrap.aside',
  'mgcrea.ngStrap.alert',
  'mgcrea.ngStrap.button',
  'mgcrea.ngStrap.select',
  'mgcrea.ngStrap.datepicker',
  'mgcrea.ngStrap.timepicker',
  'mgcrea.ngStrap.navbar',
  'mgcrea.ngStrap.tooltip',
  'mgcrea.ngStrap.popover',
  'mgcrea.ngStrap.dropdown',
  'mgcrea.ngStrap.typeahead',
  'mgcrea.ngStrap.scrollspy',
  'mgcrea.ngStrap.affix',
  'mgcrea.ngStrap.tab'
]);

// Source: affix.js
angular.module('mgcrea.ngStrap.affix', [
  'mgcrea.ngStrap.helpers.dimensions',
  'mgcrea.ngStrap.helpers.debounce'
]).provider('$affix', function () {
  var defaults = this.defaults = { offsetTop: 'auto' };
  this.$get = [
    '$window',
    'debounce',
    'dimensions',
    function ($window, debounce, dimensions) {
      var bodyEl = angular.element($window.document.body);
      var windowEl = angular.element($window);
      function AffixFactory(element, config) {
        var $affix = {};
        // Common vars
        var options = angular.extend({}, defaults, config);
        var targetEl = options.target;
        // Initial private vars
        var reset = 'affix affix-top affix-bottom', initialAffixTop = 0, initialOffsetTop = 0, offsetTop = 0, offsetBottom = 0, affixed = null, unpin = null;
        var parent = element.parent();
        // Options: custom parent
        if (options.offsetParent) {
          if (options.offsetParent.match(/^\d+$/)) {
            for (var i = 0; i < options.offsetParent * 1 - 1; i++) {
              parent = parent.parent();
            }
          } else {
            parent = angular.element(options.offsetParent);
          }
        }
        $affix.init = function () {
          $affix.$parseOffsets();
          initialOffsetTop = dimensions.offset(element[0]).top + initialAffixTop;
          // Bind events
          targetEl.on('scroll', $affix.checkPosition);
          targetEl.on('click', $affix.checkPositionWithEventLoop);
          windowEl.on('resize', $affix.$debouncedOnResize);
          // Both of these checkPosition() calls are necessary for the case where
          // the user hits refresh after scrolling to the bottom of the page.
          $affix.checkPosition();
          $affix.checkPositionWithEventLoop();
        };
        $affix.destroy = function () {
          // Unbind events
          targetEl.off('scroll', $affix.checkPosition);
          targetEl.off('click', $affix.checkPositionWithEventLoop);
          windowEl.off('resize', $affix.$debouncedOnResize);
        };
        $affix.checkPositionWithEventLoop = function () {
          setTimeout($affix.checkPosition, 1);
        };
        $affix.checkPosition = function () {
          // if (!this.$element.is(':visible')) return
          var scrollTop = getScrollTop();
          var position = dimensions.offset(element[0]);
          var elementHeight = dimensions.height(element[0]);
          // Get required affix class according to position
          var affix = getRequiredAffixClass(unpin, position, elementHeight);
          // Did affix status changed this last check?
          if (affixed === affix)
            return;
          affixed = affix;
          // Add proper affix class
          element.removeClass(reset).addClass('affix' + (affix !== 'middle' ? '-' + affix : ''));
          if (affix === 'top') {
            unpin = null;
            element.css('position', options.offsetParent ? '' : 'relative');
            element.css('top', '');
          } else if (affix === 'bottom') {
            if (options.offsetUnpin) {
              unpin = -(options.offsetUnpin * 1);
            } else {
              // Calculate unpin threshold when affixed to bottom.
              // Hopefully the browser scrolls pixel by pixel.
              unpin = position.top - scrollTop;
            }
            element.css('position', options.offsetParent ? '' : 'relative');
            element.css('top', options.offsetParent ? '' : bodyEl[0].offsetHeight - offsetBottom - elementHeight - initialOffsetTop + 'px');
          } else {
            // affix === 'middle'
            unpin = null;
            element.css('position', 'fixed');
            element.css('top', initialAffixTop + 'px');
          }
        };
        $affix.$onResize = function () {
          $affix.$parseOffsets();
          $affix.checkPosition();
        };
        $affix.$debouncedOnResize = debounce($affix.$onResize, 50);
        $affix.$parseOffsets = function () {
          // Reset position to calculate correct offsetTop
          element.css('position', options.offsetParent ? '' : 'relative');
          if (options.offsetTop) {
            if (options.offsetTop === 'auto') {
              options.offsetTop = '+0';
            }
            if (options.offsetTop.match(/^[-+]\d+$/)) {
              initialAffixTop = -options.offsetTop * 1;
              if (options.offsetParent) {
                offsetTop = dimensions.offset(parent[0]).top + options.offsetTop * 1;
              } else {
                offsetTop = dimensions.offset(element[0]).top - dimensions.css(element[0], 'marginTop', true) + options.offsetTop * 1;
              }
            } else {
              offsetTop = options.offsetTop * 1;
            }
          }
          if (options.offsetBottom) {
            if (options.offsetParent && options.offsetBottom.match(/^[-+]\d+$/)) {
              // add 1 pixel due to rounding problems...
              offsetBottom = getScrollHeight() - (dimensions.offset(parent[0]).top + dimensions.height(parent[0])) + options.offsetBottom * 1 + 1;
            } else {
              offsetBottom = options.offsetBottom * 1;
            }
          }
        };
        // Private methods
        function getRequiredAffixClass(unpin, position, elementHeight) {
          var scrollTop = getScrollTop();
          var scrollHeight = getScrollHeight();
          if (scrollTop <= offsetTop) {
            return 'top';
          } else if (unpin !== null && scrollTop + unpin <= position.top) {
            return 'middle';
          } else if (offsetBottom !== null && position.top + elementHeight + initialAffixTop >= scrollHeight - offsetBottom) {
            return 'bottom';
          } else {
            return 'middle';
          }
        }
        function getScrollTop() {
          return targetEl[0] === $window ? $window.pageYOffset : targetEl[0] === $window;
        }
        function getScrollHeight() {
          return targetEl[0] === $window ? $window.document.body.scrollHeight : targetEl[0].scrollHeight;
        }
        $affix.init();
        return $affix;
      }
      return AffixFactory;
    }
  ];
}).directive('bsAffix', [
  '$affix',
  '$window',
  function ($affix, $window) {
    return {
      restrict: 'EAC',
      require: '^?bsAffixTarget',
      link: function postLink(scope, element, attr, affixTarget) {
        var options = {
            scope: scope,
            offsetTop: 'auto',
            target: affixTarget ? affixTarget.$element : angular.element($window)
          };
        angular.forEach([
          'offsetTop',
          'offsetBottom',
          'offsetParent',
          'offsetUnpin'
        ], function (key) {
          if (angular.isDefined(attr[key]))
            options[key] = attr[key];
        });
        var affix = $affix(element, options);
        scope.$on('$destroy', function () {
          options = null;
          affix = null;
        });
      }
    };
  }
]).directive('bsAffixTarget', function () {
  return {
    controller: [
      '$element',
      function ($element) {
        this.$element = $element;
      }
    ]
  };
});

// Source: alert.js
// @BUG: following snippet won't compile correctly
// @TODO: submit issue to core
// '<span ng-if="title"><strong ng-bind="title"></strong>&nbsp;</span><span ng-bind-html="content"></span>' +
angular.module('mgcrea.ngStrap.alert', ['mgcrea.ngStrap.modal']).provider('$alert', function () {
  var defaults = this.defaults = {
      animation: 'am-fade',
      prefixClass: 'alert',
      placement: null,
      template: 'alert/alert.tpl.html',
      container: false,
      element: null,
      backdrop: false,
      keyboard: true,
      show: true,
      duration: false,
      type: false
    };
  this.$get = [
    '$modal',
    '$timeout',
    function ($modal, $timeout) {
      function AlertFactory(config) {
        var $alert = {};
        // Common vars
        var options = angular.extend({}, defaults, config);
        $alert = $modal(options);
        // Support scope as string options [/*title, content, */type]
        if (options.type) {
          $alert.$scope.type = options.type;
        }
        // Support auto-close duration
        var show = $alert.show;
        if (options.duration) {
          $alert.show = function () {
            show();
            $timeout(function () {
              $alert.hide();
            }, options.duration * 1000);
          };
        }
        return $alert;
      }
      return AlertFactory;
    }
  ];
}).directive('bsAlert', [
  '$window',
  '$location',
  '$sce',
  '$alert',
  function ($window, $location, $sce, $alert) {
    var requestAnimationFrame = $window.requestAnimationFrame || $window.setTimeout;
    return {
      restrict: 'EAC',
      scope: true,
      link: function postLink(scope, element, attr, transclusion) {
        // Directive options
        var options = {
            scope: scope,
            element: element,
            show: false
          };
        angular.forEach([
          'template',
          'placement',
          'keyboard',
          'html',
          'container',
          'animation',
          'duration'
        ], function (key) {
          if (angular.isDefined(attr[key]))
            options[key] = attr[key];
        });
        // Support scope as data-attrs
        angular.forEach([
          'title',
          'content',
          'type'
        ], function (key) {
          attr[key] && attr.$observe(key, function (newValue, oldValue) {
            scope[key] = $sce.trustAsHtml(newValue);
          });
        });
        // Support scope as an object
        attr.bsAlert && scope.$watch(attr.bsAlert, function (newValue, oldValue) {
          if (angular.isObject(newValue)) {
            angular.extend(scope, newValue);
          } else {
            scope.content = newValue;
          }
        }, true);
        // Initialize alert
        var alert = $alert(options);
        // Trigger
        element.on(attr.trigger || 'click', alert.toggle);
        // Garbage collection
        scope.$on('$destroy', function () {
          alert.destroy();
          options = null;
          alert = null;
        });
      }
    };
  }
]);

// Source: aside.js
angular.module('mgcrea.ngStrap.aside', ['mgcrea.ngStrap.modal']).provider('$aside', function () {
  var defaults = this.defaults = {
      animation: 'am-fade-and-slide-right',
      prefixClass: 'aside',
      placement: 'right',
      template: 'aside/aside.tpl.html',
      contentTemplate: false,
      container: false,
      element: null,
      backdrop: true,
      keyboard: true,
      html: false,
      show: true
    };
  this.$get = [
    '$modal',
    function ($modal) {
      function AsideFactory(config) {
        var $aside = {};
        // Common vars
        var options = angular.extend({}, defaults, config);
        $aside = $modal(options);
        return $aside;
      }
      return AsideFactory;
    }
  ];
}).directive('bsAside', [
  '$window',
  '$sce',
  '$aside',
  function ($window, $sce, $aside) {
    var requestAnimationFrame = $window.requestAnimationFrame || $window.setTimeout;
    return {
      restrict: 'EAC',
      scope: true,
      link: function postLink(scope, element, attr, transclusion) {
        // Directive options
        var options = {
            scope: scope,
            element: element,
            show: false
          };
        angular.forEach([
          'template',
          'contentTemplate',
          'placement',
          'backdrop',
          'keyboard',
          'html',
          'container',
          'animation'
        ], function (key) {
          if (angular.isDefined(attr[key]))
            options[key] = attr[key];
        });
        // Support scope as data-attrs
        angular.forEach([
          'title',
          'content'
        ], function (key) {
          attr[key] && attr.$observe(key, function (newValue, oldValue) {
            scope[key] = $sce.trustAsHtml(newValue);
          });
        });
        // Support scope as an object
        attr.bsAside && scope.$watch(attr.bsAside, function (newValue, oldValue) {
          if (angular.isObject(newValue)) {
            angular.extend(scope, newValue);
          } else {
            scope.content = newValue;
          }
        }, true);
        // Initialize aside
        var aside = $aside(options);
        // Trigger
        element.on(attr.trigger || 'click', aside.toggle);
        // Garbage collection
        scope.$on('$destroy', function () {
          aside.destroy();
          options = null;
          aside = null;
        });
      }
    };
  }
]);

// Source: button.js
angular.module('mgcrea.ngStrap.button', []).provider('$button', function () {
  var defaults = this.defaults = {
      activeClass: 'active',
      toggleEvent: 'click'
    };
  this.$get = function () {
    return { defaults: defaults };
  };
}).directive('bsCheckboxGroup', function () {
  return {
    restrict: 'A',
    require: 'ngModel',
    compile: function postLink(element, attr) {
      element.attr('data-toggle', 'buttons');
      element.removeAttr('ng-model');
      var children = element[0].querySelectorAll('input[type="checkbox"]');
      angular.forEach(children, function (child) {
        var childEl = angular.element(child);
        childEl.attr('bs-checkbox', '');
        childEl.attr('ng-model', attr.ngModel + '.' + childEl.attr('value'));
      });
    }
  };
}).directive('bsCheckbox', [
  '$button',
  '$$rAF',
  function ($button, $$rAF) {
    var defaults = $button.defaults;
    var constantValueRegExp = /^(true|false|\d+)$/;
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function postLink(scope, element, attr, controller) {
        var options = defaults;
        // Support label > input[type="checkbox"]
        var isInput = element[0].nodeName === 'INPUT';
        var activeElement = isInput ? element.parent() : element;
        var trueValue = angular.isDefined(attr.trueValue) ? attr.trueValue : true;
        if (constantValueRegExp.test(attr.trueValue)) {
          trueValue = scope.$eval(attr.trueValue);
        }
        var falseValue = angular.isDefined(attr.falseValue) ? attr.falseValue : false;
        if (constantValueRegExp.test(attr.falseValue)) {
          falseValue = scope.$eval(attr.falseValue);
        }
        // Parse exotic values
        var hasExoticValues = typeof trueValue !== 'boolean' || typeof falseValue !== 'boolean';
        if (hasExoticValues) {
          controller.$parsers.push(function (viewValue) {
            // console.warn('$parser', element.attr('ng-model'), 'viewValue', viewValue);
            return viewValue ? trueValue : falseValue;
          });
          // Fix rendering for exotic values
          scope.$watch(attr.ngModel, function (newValue, oldValue) {
            controller.$render();
          });
        }
        // model -> view
        controller.$render = function () {
          // console.warn('$render', element.attr('ng-model'), 'controller.$modelValue', typeof controller.$modelValue, controller.$modelValue, 'controller.$viewValue', typeof controller.$viewValue, controller.$viewValue);
          var isActive = angular.equals(controller.$modelValue, trueValue);
          $$rAF(function () {
            if (isInput)
              element[0].checked = isActive;
            activeElement.toggleClass(options.activeClass, isActive);
          });
        };
        // view -> model
        element.bind(options.toggleEvent, function () {
          scope.$apply(function () {
            // console.warn('!click', element.attr('ng-model'), 'controller.$viewValue', typeof controller.$viewValue, controller.$viewValue, 'controller.$modelValue', typeof controller.$modelValue, controller.$modelValue);
            if (!isInput) {
              controller.$setViewValue(!activeElement.hasClass('active'));
            }
            if (!hasExoticValues) {
              controller.$render();
            }
          });
        });
      }
    };
  }
]).directive('bsRadioGroup', function () {
  return {
    restrict: 'A',
    require: 'ngModel',
    compile: function postLink(element, attr) {
      element.attr('data-toggle', 'buttons');
      element.removeAttr('ng-model');
      var children = element[0].querySelectorAll('input[type="radio"]');
      angular.forEach(children, function (child) {
        angular.element(child).attr('bs-radio', '');
        angular.element(child).attr('ng-model', attr.ngModel);
      });
    }
  };
}).directive('bsRadio', [
  '$button',
  '$$rAF',
  function ($button, $$rAF) {
    var defaults = $button.defaults;
    var constantValueRegExp = /^(true|false|\d+)$/;
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function postLink(scope, element, attr, controller) {
        var options = defaults;
        // Support `label > input[type="radio"]` markup
        var isInput = element[0].nodeName === 'INPUT';
        var activeElement = isInput ? element.parent() : element;
        var value = constantValueRegExp.test(attr.value) ? scope.$eval(attr.value) : attr.value;
        // model -> view
        controller.$render = function () {
          // console.warn('$render', element.attr('value'), 'controller.$modelValue', typeof controller.$modelValue, controller.$modelValue, 'controller.$viewValue', typeof controller.$viewValue, controller.$viewValue);
          var isActive = angular.equals(controller.$modelValue, value);
          $$rAF(function () {
            if (isInput)
              element[0].checked = isActive;
            activeElement.toggleClass(options.activeClass, isActive);
          });
        };
        // view -> model
        element.bind(options.toggleEvent, function () {
          scope.$apply(function () {
            // console.warn('!click', element.attr('value'), 'controller.$viewValue', typeof controller.$viewValue, controller.$viewValue, 'controller.$modelValue', typeof controller.$modelValue, controller.$modelValue);
            controller.$setViewValue(value);
            controller.$render();
          });
        });
      }
    };
  }
]);

// Source: datepicker.js
angular.module('mgcrea.ngStrap.datepicker', [
  'mgcrea.ngStrap.helpers.dateParser',
  'mgcrea.ngStrap.tooltip'
]).provider('$datepicker', function () {
  var defaults = this.defaults = {
      animation: 'am-fade',
      prefixClass: 'datepicker',
      placement: 'bottom-left',
      template: 'datepicker/datepicker.tpl.html',
      trigger: 'focus',
      container: false,
      keyboard: true,
      html: false,
      delay: 0,
      useNative: false,
      dateType: 'date',
      dateFormat: 'shortDate',
      dayFormat: 'dd',
      strictFormat: false,
      autoclose: false,
      minDate: -Infinity,
      maxDate: +Infinity,
      startView: 0,
      minView: 0,
      startWeek: 0
    };
  this.$get = [
    '$window',
    '$document',
    '$rootScope',
    '$sce',
    '$locale',
    'dateFilter',
    'datepickerViews',
    '$tooltip',
    function ($window, $document, $rootScope, $sce, $locale, dateFilter, datepickerViews, $tooltip) {
      var bodyEl = angular.element($window.document.body);
      var isTouch = 'createTouch' in $window.document;
      var isNative = /(ip(a|o)d|iphone|android)/gi.test($window.navigator.userAgent);
      if (!defaults.lang)
        defaults.lang = $locale.id;
      function DatepickerFactory(element, controller, config) {
        var $datepicker = $tooltip(element, angular.extend({}, defaults, config));
        var parentScope = config.scope;
        var options = $datepicker.$options;
        var scope = $datepicker.$scope;
        if (options.startView)
          options.startView -= options.minView;
        // View vars
        var pickerViews = datepickerViews($datepicker);
        $datepicker.$views = pickerViews.views;
        var viewDate = pickerViews.viewDate;
        scope.$mode = options.startView;
        var $picker = $datepicker.$views[scope.$mode];
        // Scope methods
        scope.$select = function (date) {
          $datepicker.select(date);
        };
        scope.$selectPane = function (value) {
          $datepicker.$selectPane(value);
        };
        scope.$toggleMode = function () {
          $datepicker.setMode((scope.$mode + 1) % $datepicker.$views.length);
        };
        // Public methods
        $datepicker.update = function (date) {
          // console.warn('$datepicker.update() newValue=%o', date);
          if (angular.isDate(date) && !isNaN(date.getTime())) {
            $datepicker.$date = date;
            $picker.update.call($picker, date);
          }
          // Build only if pristine
          $datepicker.$build(true);
        };
        $datepicker.select = function (date, keep) {
          // console.warn('$datepicker.select', date, scope.$mode);
          if (!angular.isDate(controller.$dateValue))
            controller.$dateValue = new Date(date);
          controller.$dateValue.setFullYear(date.getFullYear(), date.getMonth(), date.getDate());
          if (!scope.$mode || keep) {
            controller.$setViewValue(controller.$dateValue);
            controller.$render();
            if (options.autoclose && !keep) {
              $datepicker.hide(true);
            }
          } else {
            angular.extend(viewDate, {
              year: date.getFullYear(),
              month: date.getMonth(),
              date: date.getDate()
            });
            $datepicker.setMode(scope.$mode - 1);
            $datepicker.$build();
          }
        };
        $datepicker.setMode = function (mode) {
          // console.warn('$datepicker.setMode', mode);
          scope.$mode = mode;
          $picker = $datepicker.$views[scope.$mode];
          $datepicker.$build();
        };
        // Protected methods
        $datepicker.$build = function (pristine) {
          // console.warn('$datepicker.$build() viewDate=%o', viewDate);
          if (pristine === true && $picker.built)
            return;
          if (pristine === false && !$picker.built)
            return;
          $picker.build.call($picker);
        };
        $datepicker.$updateSelected = function () {
          for (var i = 0, l = scope.rows.length; i < l; i++) {
            angular.forEach(scope.rows[i], updateSelected);
          }
        };
        $datepicker.$isSelected = function (date) {
          return $picker.isSelected(date);
        };
        $datepicker.$selectPane = function (value) {
          var steps = $picker.steps;
          var targetDate = new Date(Date.UTC(viewDate.year + (steps.year || 0) * value, viewDate.month + (steps.month || 0) * value, viewDate.date + (steps.day || 0) * value));
          angular.extend(viewDate, {
            year: targetDate.getUTCFullYear(),
            month: targetDate.getUTCMonth(),
            date: targetDate.getUTCDate()
          });
          $datepicker.$build();
        };
        $datepicker.$onMouseDown = function (evt) {
          // Prevent blur on mousedown on .dropdown-menu
          evt.preventDefault();
          evt.stopPropagation();
          // Emulate click for mobile devices
          if (isTouch) {
            var targetEl = angular.element(evt.target);
            if (targetEl[0].nodeName.toLowerCase() !== 'button') {
              targetEl = targetEl.parent();
            }
            targetEl.triggerHandler('click');
          }
        };
        $datepicker.$onKeyDown = function (evt) {
          if (!/(38|37|39|40|13)/.test(evt.keyCode) || evt.shiftKey || evt.altKey)
            return;
          evt.preventDefault();
          evt.stopPropagation();
          if (evt.keyCode === 13) {
            if (!scope.$mode) {
              return $datepicker.hide(true);
            } else {
              return scope.$apply(function () {
                $datepicker.setMode(scope.$mode - 1);
              });
            }
          }
          // Navigate with keyboard
          $picker.onKeyDown(evt);
          parentScope.$digest();
        };
        // Private
        function updateSelected(el) {
          el.selected = $datepicker.$isSelected(el.date);
        }
        function focusElement() {
          element[0].focus();
        }
        // Overrides
        var _init = $datepicker.init;
        $datepicker.init = function () {
          if (isNative && options.useNative) {
            element.prop('type', 'date');
            element.css('-webkit-appearance', 'textfield');
            return;
          } else if (isTouch) {
            element.prop('type', 'text');
            element.attr('readonly', 'true');
            element.on('click', focusElement);
          }
          _init();
        };
        var _destroy = $datepicker.destroy;
        $datepicker.destroy = function () {
          if (isNative && options.useNative) {
            element.off('click', focusElement);
          }
          _destroy();
        };
        var _show = $datepicker.show;
        $datepicker.show = function () {
          _show();
          setTimeout(function () {
            $datepicker.$element.on(isTouch ? 'touchstart' : 'mousedown', $datepicker.$onMouseDown);
            if (options.keyboard) {
              element.on('keydown', $datepicker.$onKeyDown);
            }
          });
        };
        var _hide = $datepicker.hide;
        $datepicker.hide = function (blur) {
          $datepicker.$element.off(isTouch ? 'touchstart' : 'mousedown', $datepicker.$onMouseDown);
          if (options.keyboard) {
            element.off('keydown', $datepicker.$onKeyDown);
          }
          _hide(blur);
        };
        return $datepicker;
      }
      DatepickerFactory.defaults = defaults;
      return DatepickerFactory;
    }
  ];
}).directive('bsDatepicker', [
  '$window',
  '$parse',
  '$q',
  '$locale',
  'dateFilter',
  '$datepicker',
  '$dateParser',
  '$timeout',
  function ($window, $parse, $q, $locale, dateFilter, $datepicker, $dateParser, $timeout) {
    var defaults = $datepicker.defaults;
    var isNative = /(ip(a|o)d|iphone|android)/gi.test($window.navigator.userAgent);
    var isNumeric = function (n) {
      return !isNaN(parseFloat(n)) && isFinite(n);
    };
    return {
      restrict: 'EAC',
      require: 'ngModel',
      link: function postLink(scope, element, attr, controller) {
        // Directive options
        var options = {
            scope: scope,
            controller: controller
          };
        angular.forEach([
          'placement',
          'container',
          'delay',
          'trigger',
          'keyboard',
          'html',
          'animation',
          'template',
          'autoclose',
          'dateType',
          'dateFormat',
          'dayFormat',
          'strictFormat',
          'startWeek',
          'useNative',
          'lang',
          'startView',
          'minView'
        ], function (key) {
          if (angular.isDefined(attr[key]))
            options[key] = attr[key];
        });
        // Initialize datepicker
        if (isNative && options.useNative)
          options.dateFormat = 'yyyy-MM-dd';
        var datepicker = $datepicker(element, controller, options);
        options = datepicker.$options;
        // Observe attributes for changes
        angular.forEach([
          'minDate',
          'maxDate'
        ], function (key) {
          // console.warn('attr.$observe(%s)', key, attr[key]);
          angular.isDefined(attr[key]) && attr.$observe(key, function (newValue) {
            // console.warn('attr.$observe(%s)=%o', key, newValue);
            if (newValue === 'today') {
              var today = new Date();
              datepicker.$options[key] = +new Date(today.getFullYear(), today.getMonth(), today.getDate() + (key === 'maxDate' ? 1 : 0), 0, 0, 0, key === 'minDate' ? 0 : -1);
            } else if (angular.isString(newValue) && newValue.match(/^".+"$/)) {
              // Support {{ dateObj }}
              datepicker.$options[key] = +new Date(newValue.substr(1, newValue.length - 2));
            } else if (isNumeric(newValue)) {
              datepicker.$options[key] = +new Date(parseInt(newValue, 10));
            } else {
              datepicker.$options[key] = +new Date(newValue);
            }
            // Build only if dirty
            !isNaN(datepicker.$options[key]) && datepicker.$build(false);
          });
        });
        // Watch model for changes
        scope.$watch(attr.ngModel, function (newValue, oldValue) {
          datepicker.update(controller.$dateValue);
        }, true);
        var dateParser = $dateParser({
            format: options.dateFormat,
            lang: options.lang,
            strict: options.strictFormat
          });
        // viewValue -> $parsers -> modelValue
        controller.$parsers.unshift(function (viewValue) {
          // console.warn('$parser("%s"): viewValue=%o', element.attr('ng-model'), viewValue);
          // Null values should correctly reset the model value & validity
          if (!viewValue) {
            controller.$setValidity('date', true);
            return;
          }
          var parsedDate = dateParser.parse(viewValue, controller.$dateValue);
          if (!parsedDate || isNaN(parsedDate.getTime())) {
            controller.$setValidity('date', false);
            return;
          } else {
            var isValid = (isNaN(datepicker.$options.minDate) || parsedDate.getTime() >= datepicker.$options.minDate) && (isNaN(datepicker.$options.maxDate) || parsedDate.getTime() <= datepicker.$options.maxDate);
            controller.$setValidity('date', isValid);
            // Only update the model when we have a valid date
            if (isValid)
              controller.$dateValue = parsedDate;
          }
          if (options.dateType === 'string') {
            return dateFilter(viewValue, options.dateFormat);
          } else if (options.dateType === 'number') {
            return controller.$dateValue.getTime();
          } else if (options.dateType === 'iso') {
            return controller.$dateValue.toISOString();
          } else {
            return new Date(controller.$dateValue);
          }
        });
        // modelValue -> $formatters -> viewValue
        controller.$formatters.push(function (modelValue) {
          // console.warn('$formatter("%s"): modelValue=%o (%o)', element.attr('ng-model'), modelValue, typeof modelValue);
          var date;
          if (angular.isUndefined(modelValue) || modelValue === null) {
            date = NaN;
          } else if (angular.isDate(modelValue)) {
            date = modelValue;
          } else if (options.dateType === 'string') {
            date = dateParser.parse(modelValue);
          } else {
            date = new Date(modelValue);
          }
          // Setup default value?
          // if(isNaN(date.getTime())) {
          //   var today = new Date();
          //   date = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0, 0);
          // }
          controller.$dateValue = date;
          return controller.$dateValue;
        });
        // viewValue -> element
        controller.$render = function () {
          // console.warn('$render("%s"): viewValue=%o', element.attr('ng-model'), controller.$viewValue);
          element.val(!controller.$dateValue || isNaN(controller.$dateValue.getTime()) ? '' : dateFilter(controller.$dateValue, options.dateFormat));
        };
        // Garbage collection
        scope.$on('$destroy', function () {
          datepicker.destroy();
          options = null;
          datepicker = null;
        });
      }
    };
  }
]).provider('datepickerViews', function () {
  var defaults = this.defaults = {
      dayFormat: 'dd',
      daySplit: 7
    };
  // Split array into smaller arrays
  function split(arr, size) {
    var arrays = [];
    while (arr.length > 0) {
      arrays.push(arr.splice(0, size));
    }
    return arrays;
  }
  // Modulus operator
  function mod(n, m) {
    return (n % m + m) % m;
  }
  this.$get = [
    '$locale',
    '$sce',
    'dateFilter',
    function ($locale, $sce, dateFilter) {
      return function (picker) {
        var scope = picker.$scope;
        var options = picker.$options;
        var weekDaysMin = $locale.DATETIME_FORMATS.SHORTDAY;
        var weekDaysLabels = weekDaysMin.slice(options.startWeek).concat(weekDaysMin.slice(0, options.startWeek));
        var weekDaysLabelsHtml = $sce.trustAsHtml('<th class="dow text-center">' + weekDaysLabels.join('</th><th class="dow text-center">') + '</th>');
        var startDate = picker.$date || new Date();
        var viewDate = {
            year: startDate.getFullYear(),
            month: startDate.getMonth(),
            date: startDate.getDate()
          };
        var timezoneOffset = startDate.getTimezoneOffset() * 60000;
        var views = [
            {
              format: options.dayFormat,
              split: 7,
              steps: { month: 1 },
              update: function (date, force) {
                if (!this.built || force || date.getFullYear() !== viewDate.year || date.getMonth() !== viewDate.month) {
                  angular.extend(viewDate, {
                    year: picker.$date.getFullYear(),
                    month: picker.$date.getMonth(),
                    date: picker.$date.getDate()
                  });
                  picker.$build();
                } else if (date.getDate() !== viewDate.date) {
                  viewDate.date = picker.$date.getDate();
                  picker.$updateSelected();
                }
              },
              build: function () {
                var firstDayOfMonth = new Date(viewDate.year, viewDate.month, 1), firstDayOfMonthOffset = firstDayOfMonth.getTimezoneOffset();
                var firstDate = new Date(+firstDayOfMonth - mod(firstDayOfMonth.getDay() - options.startWeek, 7) * 86400000), firstDateOffset = firstDate.getTimezoneOffset();
                // Handle daylight time switch
                if (firstDateOffset !== firstDayOfMonthOffset)
                  firstDate = new Date(+firstDate + (firstDateOffset - firstDayOfMonthOffset) * 60000);
                var days = [], day;
                for (var i = 0; i < 42; i++) {
                  // < 7 * 6
                  day = new Date(firstDate.getFullYear(), firstDate.getMonth(), firstDate.getDate() + i);
                  days.push({
                    date: day,
                    label: dateFilter(day, this.format),
                    selected: picker.$date && this.isSelected(day),
                    muted: day.getMonth() !== viewDate.month,
                    disabled: this.isDisabled(day)
                  });
                }
                scope.title = dateFilter(firstDayOfMonth, 'MMMM yyyy');
                scope.showLabels = true;
                scope.labels = weekDaysLabelsHtml;
                scope.rows = split(days, this.split);
                this.built = true;
              },
              isSelected: function (date) {
                return picker.$date && date.getFullYear() === picker.$date.getFullYear() && date.getMonth() === picker.$date.getMonth() && date.getDate() === picker.$date.getDate();
              },
              isDisabled: function (date) {
                return date.getTime() < options.minDate || date.getTime() > options.maxDate;
              },
              onKeyDown: function (evt) {
                var actualTime = picker.$date.getTime();
                var newDate;
                if (evt.keyCode === 37)
                  newDate = new Date(actualTime - 1 * 86400000);
                else if (evt.keyCode === 38)
                  newDate = new Date(actualTime - 7 * 86400000);
                else if (evt.keyCode === 39)
                  newDate = new Date(actualTime + 1 * 86400000);
                else if (evt.keyCode === 40)
                  newDate = new Date(actualTime + 7 * 86400000);
                if (!this.isDisabled(newDate))
                  picker.select(newDate, true);
              }
            },
            {
              name: 'month',
              format: 'MMM',
              split: 4,
              steps: { year: 1 },
              update: function (date, force) {
                if (!this.built || date.getFullYear() !== viewDate.year) {
                  angular.extend(viewDate, {
                    year: picker.$date.getFullYear(),
                    month: picker.$date.getMonth(),
                    date: picker.$date.getDate()
                  });
                  picker.$build();
                } else if (date.getMonth() !== viewDate.month) {
                  angular.extend(viewDate, {
                    month: picker.$date.getMonth(),
                    date: picker.$date.getDate()
                  });
                  picker.$updateSelected();
                }
              },
              build: function () {
                var firstMonth = new Date(viewDate.year, 0, 1);
                var months = [], month;
                for (var i = 0; i < 12; i++) {
                  month = new Date(viewDate.year, i, 1);
                  months.push({
                    date: month,
                    label: dateFilter(month, this.format),
                    selected: picker.$isSelected(month),
                    disabled: this.isDisabled(month)
                  });
                }
                scope.title = dateFilter(month, 'yyyy');
                scope.showLabels = false;
                scope.rows = split(months, this.split);
                this.built = true;
              },
              isSelected: function (date) {
                return picker.$date && date.getFullYear() === picker.$date.getFullYear() && date.getMonth() === picker.$date.getMonth();
              },
              isDisabled: function (date) {
                var lastDate = +new Date(date.getFullYear(), date.getMonth() + 1, 0);
                return lastDate < options.minDate || date.getTime() > options.maxDate;
              },
              onKeyDown: function (evt) {
                var actualMonth = picker.$date.getMonth();
                var newDate = new Date(picker.$date);
                if (evt.keyCode === 37)
                  newDate.setMonth(actualMonth - 1);
                else if (evt.keyCode === 38)
                  newDate.setMonth(actualMonth - 4);
                else if (evt.keyCode === 39)
                  newDate.setMonth(actualMonth + 1);
                else if (evt.keyCode === 40)
                  newDate.setMonth(actualMonth + 4);
                if (!this.isDisabled(newDate))
                  picker.select(newDate, true);
              }
            },
            {
              name: 'year',
              format: 'yyyy',
              split: 4,
              steps: { year: 12 },
              update: function (date, force) {
                if (!this.built || force || parseInt(date.getFullYear() / 20, 10) !== parseInt(viewDate.year / 20, 10)) {
                  angular.extend(viewDate, {
                    year: picker.$date.getFullYear(),
                    month: picker.$date.getMonth(),
                    date: picker.$date.getDate()
                  });
                  picker.$build();
                } else if (date.getFullYear() !== viewDate.year) {
                  angular.extend(viewDate, {
                    year: picker.$date.getFullYear(),
                    month: picker.$date.getMonth(),
                    date: picker.$date.getDate()
                  });
                  picker.$updateSelected();
                }
              },
              build: function () {
                var firstYear = viewDate.year - viewDate.year % (this.split * 3);
                var years = [], year;
                for (var i = 0; i < 12; i++) {
                  year = new Date(firstYear + i, 0, 1);
                  years.push({
                    date: year,
                    label: dateFilter(year, this.format),
                    selected: picker.$isSelected(year),
                    disabled: this.isDisabled(year)
                  });
                }
                scope.title = years[0].label + '-' + years[years.length - 1].label;
                scope.showLabels = false;
                scope.rows = split(years, this.split);
                this.built = true;
              },
              isSelected: function (date) {
                return picker.$date && date.getFullYear() === picker.$date.getFullYear();
              },
              isDisabled: function (date) {
                var lastDate = +new Date(date.getFullYear() + 1, 0, 0);
                return lastDate < options.minDate || date.getTime() > options.maxDate;
              },
              onKeyDown: function (evt) {
                var actualYear = picker.$date.getFullYear(), newDate = new Date(picker.$date);
                if (evt.keyCode === 37)
                  newDate.setYear(actualYear - 1);
                else if (evt.keyCode === 38)
                  newDate.setYear(actualYear - 4);
                else if (evt.keyCode === 39)
                  newDate.setYear(actualYear + 1);
                else if (evt.keyCode === 40)
                  newDate.setYear(actualYear + 4);
                if (!this.isDisabled(newDate))
                  picker.select(newDate, true);
              }
            }
          ];
        return {
          views: options.minView ? Array.prototype.slice.call(views, options.minView) : views,
          viewDate: viewDate
        };
      };
    }
  ];
});

// Source: dropdown.js
angular.module('mgcrea.ngStrap.dropdown', ['mgcrea.ngStrap.tooltip']).provider('$dropdown', function () {
  var defaults = this.defaults = {
      animation: 'am-fade',
      prefixClass: 'dropdown',
      placement: 'bottom-left',
      template: 'dropdown/dropdown.tpl.html',
      trigger: 'click',
      container: false,
      keyboard: true,
      html: false,
      delay: 0
    };
  this.$get = [
    '$window',
    '$rootScope',
    '$tooltip',
    function ($window, $rootScope, $tooltip) {
      var bodyEl = angular.element($window.document.body);
      var matchesSelector = Element.prototype.matchesSelector || Element.prototype.webkitMatchesSelector || Element.prototype.mozMatchesSelector || Element.prototype.msMatchesSelector || Element.prototype.oMatchesSelector;
      function DropdownFactory(element, config) {
        var $dropdown = {};
        // Common vars
        var options = angular.extend({}, defaults, config);
        var scope = $dropdown.$scope = options.scope && options.scope.$new() || $rootScope.$new();
        $dropdown = $tooltip(element, options);
        // Protected methods
        $dropdown.$onKeyDown = function (evt) {
          if (!/(38|40)/.test(evt.keyCode))
            return;
          evt.preventDefault();
          evt.stopPropagation();
          // Retrieve focused index
          var items = angular.element($dropdown.$element[0].querySelectorAll('li:not(.divider) a'));
          if (!items.length)
            return;
          var index;
          angular.forEach(items, function (el, i) {
            if (matchesSelector && matchesSelector.call(el, ':focus'))
              index = i;
          });
          // Navigate with keyboard
          if (evt.keyCode === 38 && index > 0)
            index--;
          else if (evt.keyCode === 40 && index < items.length - 1)
            index++;
          else if (angular.isUndefined(index))
            index = 0;
          items.eq(index)[0].focus();
        };
        // Overrides
        var show = $dropdown.show;
        $dropdown.show = function () {
          show();
          setTimeout(function () {
            options.keyboard && $dropdown.$element.on('keydown', $dropdown.$onKeyDown);
            bodyEl.on('click', onBodyClick);
          });
        };
        var hide = $dropdown.hide;
        $dropdown.hide = function () {
          options.keyboard && $dropdown.$element.off('keydown', $dropdown.$onKeyDown);
          bodyEl.off('click', onBodyClick);
          hide();
        };
        // Private functions
        function onBodyClick(evt) {
          if (evt.target === element[0])
            return;
          return evt.target !== element[0] && $dropdown.hide();
        }
        return $dropdown;
      }
      return DropdownFactory;
    }
  ];
}).directive('bsDropdown', [
  '$window',
  '$location',
  '$sce',
  '$dropdown',
  function ($window, $location, $sce, $dropdown) {
    return {
      restrict: 'EAC',
      scope: true,
      link: function postLink(scope, element, attr, transclusion) {
        // Directive options
        var options = { scope: scope };
        angular.forEach([
          'placement',
          'container',
          'delay',
          'trigger',
          'keyboard',
          'html',
          'animation',
          'template'
        ], function (key) {
          if (angular.isDefined(attr[key]))
            options[key] = attr[key];
        });
        // Support scope as an object
        attr.bsDropdown && scope.$watch(attr.bsDropdown, function (newValue, oldValue) {
          scope.content = newValue;
        }, true);
        // Initialize dropdown
        var dropdown = $dropdown(element, options);
        // Garbage collection
        scope.$on('$destroy', function () {
          dropdown.destroy();
          options = null;
          dropdown = null;
        });
      }
    };
  }
]);

// Source: date-parser.js
angular.module('mgcrea.ngStrap.helpers.dateParser', []).provider('$dateParser', [
  '$localeProvider',
  function ($localeProvider) {
    var proto = Date.prototype;
    function isNumeric(n) {
      return !isNaN(parseFloat(n)) && isFinite(n);
    }
    var defaults = this.defaults = {
        format: 'shortDate',
        strict: false
      };
    this.$get = [
      '$locale',
      function ($locale) {
        var DateParserFactory = function (config) {
          var options = angular.extend({}, defaults, config);
          var $dateParser = {};
          var regExpMap = {
              'sss': '[0-9]{3}',
              'ss': '[0-5][0-9]',
              's': options.strict ? '[1-5]?[0-9]' : '[0-9]|[0-5][0-9]',
              'mm': '[0-5][0-9]',
              'm': options.strict ? '[1-5]?[0-9]' : '[0-9]|[0-5][0-9]',
              'HH': '[01][0-9]|2[0-3]',
              'H': options.strict ? '1?[0-9]|2[0-3]' : '[01]?[0-9]|2[0-3]',
              'hh': '[0][1-9]|[1][012]',
              'h': options.strict ? '[1-9]|1[012]' : '0?[1-9]|1[012]',
              'a': 'AM|PM',
              'EEEE': $locale.DATETIME_FORMATS.DAY.join('|'),
              'EEE': $locale.DATETIME_FORMATS.SHORTDAY.join('|'),
              'dd': '0[1-9]|[12][0-9]|3[01]',
              'd': options.strict ? '[1-9]|[1-2][0-9]|3[01]' : '0?[1-9]|[1-2][0-9]|3[01]',
              'MMMM': $locale.DATETIME_FORMATS.MONTH.join('|'),
              'MMM': $locale.DATETIME_FORMATS.SHORTMONTH.join('|'),
              'MM': '0[1-9]|1[012]',
              'M': options.strict ? '[1-9]|1[012]' : '0?[1-9]|1[012]',
              'yyyy': '[1]{1}[0-9]{3}|[2]{1}[0-9]{3}',
              'yy': '[0-9]{2}',
              'y': options.strict ? '-?(0|[1-9][0-9]{0,3})' : '-?0*[0-9]{1,4}'
            };
          var setFnMap = {
              'sss': proto.setMilliseconds,
              'ss': proto.setSeconds,
              's': proto.setSeconds,
              'mm': proto.setMinutes,
              'm': proto.setMinutes,
              'HH': proto.setHours,
              'H': proto.setHours,
              'hh': proto.setHours,
              'h': proto.setHours,
              'dd': proto.setDate,
              'd': proto.setDate,
              'a': function (value) {
                var hours = this.getHours();
                return this.setHours(value.match(/pm/i) ? hours + 12 : hours);
              },
              'MMMM': function (value) {
                return this.setMonth($locale.DATETIME_FORMATS.MONTH.indexOf(value));
              },
              'MMM': function (value) {
                return this.setMonth($locale.DATETIME_FORMATS.SHORTMONTH.indexOf(value));
              },
              'MM': function (value) {
                return this.setMonth(1 * value - 1);
              },
              'M': function (value) {
                return this.setMonth(1 * value - 1);
              },
              'yyyy': proto.setFullYear,
              'yy': function (value) {
                return this.setFullYear(2000 + 1 * value);
              },
              'y': proto.setFullYear
            };
          var regex, setMap;
          $dateParser.init = function () {
            $dateParser.$format = $locale.DATETIME_FORMATS[options.format] || options.format;
            regex = regExpForFormat($dateParser.$format);
            setMap = setMapForFormat($dateParser.$format);
          };
          $dateParser.isValid = function (date) {
            if (angular.isDate(date))
              return !isNaN(date.getTime());
            return regex.test(date);
          };
          $dateParser.parse = function (value, baseDate) {
            if (angular.isDate(value))
              return value;
            var matches = regex.exec(value);
            if (!matches)
              return false;
            var date = baseDate || new Date(0, 0, 1);
            for (var i = 0; i < matches.length - 1; i++) {
              setMap[i] && setMap[i].call(date, matches[i + 1]);
            }
            return date;
          };
          // Private functions
          function setMapForFormat(format) {
            var keys = Object.keys(setFnMap), i;
            var map = [], sortedMap = [];
            // Map to setFn
            var clonedFormat = format;
            for (i = 0; i < keys.length; i++) {
              if (format.split(keys[i]).length > 1) {
                var index = clonedFormat.search(keys[i]);
                format = format.split(keys[i]).join('');
                if (setFnMap[keys[i]])
                  map[index] = setFnMap[keys[i]];
              }
            }
            // Sort result map
            angular.forEach(map, function (v) {
              sortedMap.push(v);
            });
            return sortedMap;
          }
          function escapeReservedSymbols(text) {
            return text.replace(/\//g, '[\\/]').replace('/-/g', '[-]').replace(/\./g, '[.]').replace(/\\s/g, '[\\s]');
          }
          function regExpForFormat(format) {
            var keys = Object.keys(regExpMap), i;
            var re = format;
            // Abstract replaces to avoid collisions
            for (i = 0; i < keys.length; i++) {
              re = re.split(keys[i]).join('${' + i + '}');
            }
            // Replace abstracted values
            for (i = 0; i < keys.length; i++) {
              re = re.split('${' + i + '}').join('(' + regExpMap[keys[i]] + ')');
            }
            format = escapeReservedSymbols(format);
            return new RegExp('^' + re + '$', ['i']);
          }
          $dateParser.init();
          return $dateParser;
        };
        return DateParserFactory;
      }
    ];
  }
]);

// Source: debounce.js
angular.module('mgcrea.ngStrap.helpers.debounce', []).constant('debounce', function (func, wait, immediate) {
  var timeout, args, context, timestamp, result;
  return function () {
    context = this;
    args = arguments;
    timestamp = new Date();
    var later = function () {
      var last = new Date() - timestamp;
      if (last < wait) {
        timeout = setTimeout(later, wait - last);
      } else {
        timeout = null;
        if (!immediate)
          result = func.apply(context, args);
      }
    };
    var callNow = immediate && !timeout;
    if (!timeout) {
      timeout = setTimeout(later, wait);
    }
    if (callNow)
      result = func.apply(context, args);
    return result;
  };
}).constant('throttle', function (func, wait, options) {
  var context, args, result;
  var timeout = null;
  var previous = 0;
  options || (options = {});
  var later = function () {
    previous = options.leading === false ? 0 : new Date();
    timeout = null;
    result = func.apply(context, args);
  };
  return function () {
    var now = new Date();
    if (!previous && options.leading === false)
      previous = now;
    var remaining = wait - (now - previous);
    context = this;
    args = arguments;
    if (remaining <= 0) {
      clearTimeout(timeout);
      timeout = null;
      previous = now;
      result = func.apply(context, args);
    } else if (!timeout && options.trailing !== false) {
      timeout = setTimeout(later, remaining);
    }
    return result;
  };
});

// Source: dimensions.js
angular.module('mgcrea.ngStrap.helpers.dimensions', []).factory('dimensions', [
  '$document',
  '$window',
  function ($document, $window) {
    var jqLite = angular.element;
    var fn = {};
    /**
     * Test the element nodeName
     * @param element
     * @param name
     */
    var nodeName = fn.nodeName = function (element, name) {
        return element.nodeName && element.nodeName.toLowerCase() === name.toLowerCase();
      };
    /**
     * Returns the element computed style
     * @param element
     * @param prop
     * @param extra
     */
    fn.css = function (element, prop, extra) {
      var value;
      if (element.currentStyle) {
        //IE
        value = element.currentStyle[prop];
      } else if (window.getComputedStyle) {
        value = window.getComputedStyle(element)[prop];
      } else {
        value = element.style[prop];
      }
      return extra === true ? parseFloat(value) || 0 : value;
    };
    /**
     * Provides read-only equivalent of jQuery's offset function:
     * @required-by bootstrap-tooltip, bootstrap-affix
     * @url http://api.jquery.com/offset/
     * @param element
     */
    fn.offset = function (element) {
      var boxRect = element.getBoundingClientRect();
      var docElement = element.ownerDocument;
      return {
        width: element.offsetWidth,
        height: element.offsetHeight,
        top: boxRect.top + (window.pageYOffset || docElement.documentElement.scrollTop) - (docElement.documentElement.clientTop || 0),
        left: boxRect.left + (window.pageXOffset || docElement.documentElement.scrollLeft) - (docElement.documentElement.clientLeft || 0)
      };
    };
    /**
     * Provides read-only equivalent of jQuery's position function
     * @required-by bootstrap-tooltip, bootstrap-affix
     * @url http://api.jquery.com/offset/
     * @param element
     */
    fn.position = function (element) {
      var offsetParentRect = {
          top: 0,
          left: 0
        }, offsetParentElement, offset;
      // Fixed elements are offset from window (parentOffset = {top:0, left: 0}, because it is it's only offset parent
      if (fn.css(element, 'position') === 'fixed') {
        // We assume that getBoundingClientRect is available when computed position is fixed
        offset = element.getBoundingClientRect();
      } else {
        // Get *real* offsetParentElement
        offsetParentElement = offsetParent(element);
        offset = fn.offset(element);
        // Get correct offsets
        offset = fn.offset(element);
        if (!nodeName(offsetParentElement, 'html')) {
          offsetParentRect = fn.offset(offsetParentElement);
        }
        // Add offsetParent borders
        offsetParentRect.top += fn.css(offsetParentElement, 'borderTopWidth', true);
        offsetParentRect.left += fn.css(offsetParentElement, 'borderLeftWidth', true);
      }
      // Subtract parent offsets and element margins
      return {
        width: element.offsetWidth,
        height: element.offsetHeight,
        top: offset.top - offsetParentRect.top - fn.css(element, 'marginTop', true),
        left: offset.left - offsetParentRect.left - fn.css(element, 'marginLeft', true)
      };
    };
    /**
     * Returns the closest, non-statically positioned offsetParent of a given element
     * @required-by fn.position
     * @param element
     */
    var offsetParent = function offsetParentElement(element) {
      var docElement = element.ownerDocument;
      var offsetParent = element.offsetParent || docElement;
      if (nodeName(offsetParent, '#document'))
        return docElement.documentElement;
      while (offsetParent && !nodeName(offsetParent, 'html') && fn.css(offsetParent, 'position') === 'static') {
        offsetParent = offsetParent.offsetParent;
      }
      return offsetParent || docElement.documentElement;
    };
    /**
     * Provides equivalent of jQuery's height function
     * @required-by bootstrap-affix
     * @url http://api.jquery.com/height/
     * @param element
     * @param outer
     */
    fn.height = function (element, outer) {
      var value = element.offsetHeight;
      if (outer) {
        value += fn.css(element, 'marginTop', true) + fn.css(element, 'marginBottom', true);
      } else {
        value -= fn.css(element, 'paddingTop', true) + fn.css(element, 'paddingBottom', true) + fn.css(element, 'borderTopWidth', true) + fn.css(element, 'borderBottomWidth', true);
      }
      return value;
    };
    /**
     * Provides equivalent of jQuery's height function
     * @required-by bootstrap-affix
     * @url http://api.jquery.com/width/
     * @param element
     * @param outer
     */
    fn.width = function (element, outer) {
      var value = element.offsetWidth;
      if (outer) {
        value += fn.css(element, 'marginLeft', true) + fn.css(element, 'marginRight', true);
      } else {
        value -= fn.css(element, 'paddingLeft', true) + fn.css(element, 'paddingRight', true) + fn.css(element, 'borderLeftWidth', true) + fn.css(element, 'borderRightWidth', true);
      }
      return value;
    };
    return fn;
  }
]);

// Source: parse-options.js
angular.module('mgcrea.ngStrap.helpers.parseOptions', []).provider('$parseOptions', function () {
  var defaults = this.defaults = { regexp: /^\s*(.*?)(?:\s+as\s+(.*?))?(?:\s+group\s+by\s+(.*))?\s+for\s+(?:([\$\w][\$\w]*)|(?:\(\s*([\$\w][\$\w]*)\s*,\s*([\$\w][\$\w]*)\s*\)))\s+in\s+(.*?)(?:\s+track\s+by\s+(.*?))?$/ };
  this.$get = [
    '$parse',
    '$q',
    function ($parse, $q) {
      function ParseOptionsFactory(attr, config) {
        var $parseOptions = {};
        // Common vars
        var options = angular.extend({}, defaults, config);
        $parseOptions.$values = [];
        // Private vars
        var match, displayFn, valueName, keyName, groupByFn, valueFn, valuesFn;
        $parseOptions.init = function () {
          $parseOptions.$match = match = attr.match(options.regexp);
          displayFn = $parse(match[2] || match[1]), valueName = match[4] || match[6], keyName = match[5], groupByFn = $parse(match[3] || ''), valueFn = $parse(match[2] ? match[1] : valueName), valuesFn = $parse(match[7]);
        };
        $parseOptions.valuesFn = function (scope, controller) {
          return $q.when(valuesFn(scope, controller)).then(function (values) {
            $parseOptions.$values = values ? parseValues(values, scope) : {};
            return $parseOptions.$values;
          });
        };
        // Private functions
        function parseValues(values, scope) {
          return values.map(function (match, index) {
            var locals = {}, label, value;
            locals[valueName] = match;
            label = displayFn(scope, locals);
            value = valueFn(scope, locals) || index;
            return {
              label: label,
              value: value
            };
          });
        }
        $parseOptions.init();
        return $parseOptions;
      }
      return ParseOptionsFactory;
    }
  ];
});

// Source: raf.js
angular.version.minor < 3 && angular.version.dot < 14 && angular.module('ng').factory('$$rAF', [
  '$window',
  '$timeout',
  function ($window, $timeout) {
    var requestAnimationFrame = $window.requestAnimationFrame || $window.webkitRequestAnimationFrame || $window.mozRequestAnimationFrame;
    var cancelAnimationFrame = $window.cancelAnimationFrame || $window.webkitCancelAnimationFrame || $window.mozCancelAnimationFrame || $window.webkitCancelRequestAnimationFrame;
    var rafSupported = !!requestAnimationFrame;
    var raf = rafSupported ? function (fn) {
        var id = requestAnimationFrame(fn);
        return function () {
          cancelAnimationFrame(id);
        };
      } : function (fn) {
        var timer = $timeout(fn, 16.66, false);
        // 1000 / 60 = 16.666
        return function () {
          $timeout.cancel(timer);
        };
      };
    raf.supported = rafSupported;
    return raf;
  }
]);  // .factory('$$animateReflow', function($$rAF, $document) {
     //   var bodyEl = $document[0].body;
     //   return function(fn) {
     //     //the returned function acts as the cancellation function
     //     return $$rAF(function() {
     //       //the line below will force the browser to perform a repaint
     //       //so that all the animated elements within the animation frame
     //       //will be properly updated and drawn on screen. This is
     //       //required to perform multi-class CSS based animations with
     //       //Firefox. DO NOT REMOVE THIS LINE.
     //       var a = bodyEl.offsetWidth + 1;
     //       fn();
     //     });
     //   };
     // });

// Source: modal.js
angular.module('mgcrea.ngStrap.modal', ['mgcrea.ngStrap.helpers.dimensions']).provider('$modal', function () {
  var defaults = this.defaults = {
      animation: 'am-fade',
      backdropAnimation: 'am-fade',
      prefixClass: 'modal',
      prefixEvent: 'modal',
      placement: 'top',
      template: 'modal/modal.tpl.html',
      contentTemplate: false,
      container: false,
      element: null,
      backdrop: true,
      keyboard: true,
      html: false,
      show: true
    };
  this.$get = [
    '$window',
    '$rootScope',
    '$compile',
    '$q',
    '$templateCache',
    '$http',
    '$animate',
    '$timeout',
    '$sce',
    'dimensions',
    function ($window, $rootScope, $compile, $q, $templateCache, $http, $animate, $timeout, $sce, dimensions) {
      var forEach = angular.forEach;
      var trim = String.prototype.trim;
      var requestAnimationFrame = $window.requestAnimationFrame || $window.setTimeout;
      var bodyElement = angular.element($window.document.body);
      var htmlReplaceRegExp = /ng-bind="/gi;
      function ModalFactory(config) {
        var $modal = {};
        // Common vars
        var options = $modal.$options = angular.extend({}, defaults, config);
        $modal.$promise = fetchTemplate(options.template);
        var scope = $modal.$scope = options.scope && options.scope.$new() || $rootScope.$new();
        if (!options.element && !options.container) {
          options.container = 'body';
        }
        // Support scope as string options
        forEach([
          'title',
          'content'
        ], function (key) {
          if (options[key])
            scope[key] = $sce.trustAsHtml(options[key]);
        });
        // Provide scope helpers
        scope.$hide = function () {
          scope.$$postDigest(function () {
            $modal.hide();
          });
        };
        scope.$show = function () {
          scope.$$postDigest(function () {
            $modal.show();
          });
        };
        scope.$toggle = function () {
          scope.$$postDigest(function () {
            $modal.toggle();
          });
        };
        // Support contentTemplate option
        if (options.contentTemplate) {
          $modal.$promise = $modal.$promise.then(function (template) {
            var templateEl = angular.element(template);
            return fetchTemplate(options.contentTemplate).then(function (contentTemplate) {
              var contentEl = findElement('[ng-bind="content"]', templateEl[0]).removeAttr('ng-bind').html(contentTemplate);
              // Drop the default footer as you probably don't want it if you use a custom contentTemplate
              if (!config.template)
                contentEl.next().remove();
              return templateEl[0].outerHTML;
            });
          });
        }
        // Fetch, compile then initialize modal
        var modalLinker, modalElement;
        var backdropElement = angular.element('<div class="' + options.prefixClass + '-backdrop"/>');
        $modal.$promise.then(function (template) {
          if (angular.isObject(template))
            template = template.data;
          if (options.html)
            template = template.replace(htmlReplaceRegExp, 'ng-bind-html="');
          template = trim.apply(template);
          modalLinker = $compile(template);
          $modal.init();
        });
        $modal.init = function () {
          // Options: show
          if (options.show) {
            scope.$$postDigest(function () {
              $modal.show();
            });
          }
        };
        $modal.destroy = function () {
          // Remove element
          if (modalElement) {
            modalElement.remove();
            modalElement = null;
          }
          if (backdropElement) {
            backdropElement.remove();
            backdropElement = null;
          }
          // Destroy scope
          scope.$destroy();
        };
        $modal.show = function () {
          scope.$emit(options.prefixEvent + '.show.before', $modal);
          var parent = options.container ? findElement(options.container) : null;
          var after = options.container ? null : options.element;
          // Fetch a cloned element linked from template
          modalElement = $modal.$element = modalLinker(scope, function (clonedElement, scope) {
          });
          // Set the initial positioning.
          modalElement.css({ display: 'block' }).addClass(options.placement);
          // Options: animation
          if (options.animation) {
            if (options.backdrop) {
              backdropElement.addClass(options.backdropAnimation);
            }
            modalElement.addClass(options.animation);
          }
          if (options.backdrop) {
            $animate.enter(backdropElement, bodyElement, null, function () {
            });
          }
          $animate.enter(modalElement, parent, after, function () {
            scope.$emit(options.prefixEvent + '.show', $modal);
          });
          scope.$isShown = true;
          scope.$$phase || scope.$root.$$phase || scope.$digest();
          // Focus once the enter-animation has started
          // Weird PhantomJS bug hack
          var el = modalElement[0];
          requestAnimationFrame(function () {
            el.focus();
          });
          bodyElement.addClass(options.prefixClass + '-open');
          if (options.animation) {
            bodyElement.addClass(options.prefixClass + '-with-' + options.animation);
          }
          // Bind events
          if (options.backdrop) {
            modalElement.on('click', hideOnBackdropClick);
            backdropElement.on('click', hideOnBackdropClick);
          }
          if (options.keyboard) {
            modalElement.on('keyup', $modal.$onKeyUp);
          }
        };
        $modal.hide = function () {
          scope.$emit(options.prefixEvent + '.hide.before', $modal);
          $animate.leave(modalElement, function () {
            scope.$emit(options.prefixEvent + '.hide', $modal);
            bodyElement.removeClass(options.prefixClass + '-open');
            if (options.animation) {
              bodyElement.addClass(options.prefixClass + '-with-' + options.animation);
            }
          });
          if (options.backdrop) {
            $animate.leave(backdropElement, function () {
            });
          }
          scope.$isShown = false;
          scope.$$phase || scope.$root.$$phase || scope.$digest();
          // Unbind events
          if (options.backdrop) {
            modalElement.off('click', hideOnBackdropClick);
            backdropElement.off('click', hideOnBackdropClick);
          }
          if (options.keyboard) {
            modalElement.off('keyup', $modal.$onKeyUp);
          }
        };
        $modal.toggle = function () {
          scope.$isShown ? $modal.hide() : $modal.show();
        };
        $modal.focus = function () {
          modalElement[0].focus();
        };
        // Protected methods
        $modal.$onKeyUp = function (evt) {
          evt.which === 27 && $modal.hide();
        };
        // Private methods
        function hideOnBackdropClick(evt) {
          if (evt.target !== evt.currentTarget)
            return;
          options.backdrop === 'static' ? $modal.focus() : $modal.hide();
        }
        return $modal;
      }
      // Helper functions
      function findElement(query, element) {
        return angular.element((element || document).querySelectorAll(query));
      }
      function fetchTemplate(template) {
        return $q.when($templateCache.get(template) || $http.get(template)).then(function (res) {
          if (angular.isObject(res)) {
            $templateCache.put(template, res.data);
            return res.data;
          }
          return res;
        });
      }
      return ModalFactory;
    }
  ];
}).directive('bsModal', [
  '$window',
  '$location',
  '$sce',
  '$modal',
  function ($window, $location, $sce, $modal) {
    return {
      restrict: 'EAC',
      scope: true,
      link: function postLink(scope, element, attr, transclusion) {
        // Directive options
        var options = {
            scope: scope,
            element: element,
            show: false
          };
        angular.forEach([
          'template',
          'contentTemplate',
          'placement',
          'backdrop',
          'keyboard',
          'html',
          'container',
          'animation'
        ], function (key) {
          if (angular.isDefined(attr[key]))
            options[key] = attr[key];
        });
        // Support scope as data-attrs
        angular.forEach([
          'title',
          'content'
        ], function (key) {
          attr[key] && attr.$observe(key, function (newValue, oldValue) {
            scope[key] = $sce.trustAsHtml(newValue);
          });
        });
        // Support scope as an object
        attr.bsModal && scope.$watch(attr.bsModal, function (newValue, oldValue) {
          if (angular.isObject(newValue)) {
            angular.extend(scope, newValue);
          } else {
            scope.content = newValue;
          }
        }, true);
        // Initialize modal
        var modal = $modal(options);
        // Trigger
        element.on(attr.trigger || 'click', modal.toggle);
        // Garbage collection
        scope.$on('$destroy', function () {
          modal.destroy();
          options = null;
          modal = null;
        });
      }
    };
  }
]);

// Source: navbar.js
angular.module('mgcrea.ngStrap.navbar', []).provider('$navbar', function () {
  var defaults = this.defaults = {
      activeClass: 'active',
      routeAttr: 'data-match-route',
      strict: false
    };
  this.$get = function () {
    return { defaults: defaults };
  };
}).directive('bsNavbar', [
  '$window',
  '$location',
  '$navbar',
  function ($window, $location, $navbar) {
    var defaults = $navbar.defaults;
    return {
      restrict: 'A',
      link: function postLink(scope, element, attr, controller) {
        // Directive options
        var options = angular.copy(defaults);
        angular.forEach(Object.keys(defaults), function (key) {
          if (angular.isDefined(attr[key]))
            options[key] = attr[key];
        });
        // Watch for the $location
        scope.$watch(function () {
          return $location.path();
        }, function (newValue, oldValue) {
          var liElements = element[0].querySelectorAll('li[' + options.routeAttr + ']');
          angular.forEach(liElements, function (li) {
            var liElement = angular.element(li);
            var pattern = liElement.attr(options.routeAttr).replace('/', '\\/');
            if (options.strict) {
              pattern = '^' + pattern + '$';
            }
            var regexp = new RegExp(pattern, ['i']);
            if (regexp.test(newValue)) {
              liElement.addClass(options.activeClass);
            } else {
              liElement.removeClass(options.activeClass);
            }
          });
        });
      }
    };
  }
]);

// Source: select.js
angular.module('mgcrea.ngStrap.select', [
  'mgcrea.ngStrap.tooltip',
  'mgcrea.ngStrap.helpers.parseOptions'
]).provider('$select', function () {
  var defaults = this.defaults = {
      animation: 'am-fade',
      prefixClass: 'select',
      placement: 'bottom-left',
      template: 'select/select.tpl.html',
      trigger: 'focus',
      container: false,
      keyboard: true,
      html: false,
      delay: 0,
      multiple: false,
      sort: true,
      caretHtml: '&nbsp;<span class="caret"></span>',
      placeholder: 'Choose among the following...',
      maxLength: 3,
      maxLengthHtml: 'selected'
    };
  this.$get = [
    '$window',
    '$document',
    '$rootScope',
    '$tooltip',
    function ($window, $document, $rootScope, $tooltip) {
      var bodyEl = angular.element($window.document.body);
      var isTouch = 'createTouch' in $window.document;
      function SelectFactory(element, controller, config) {
        var $select = {};
        // Common vars
        var options = angular.extend({}, defaults, config);
        $select = $tooltip(element, options);
        var parentScope = config.scope;
        var scope = $select.$scope;
        scope.$matches = [];
        scope.$activeIndex = 0;
        scope.$isMultiple = options.multiple;
        scope.$activate = function (index) {
          scope.$$postDigest(function () {
            $select.activate(index);
          });
        };
        scope.$select = function (index, evt) {
          scope.$$postDigest(function () {
            $select.select(index);
          });
        };
        scope.$isVisible = function () {
          return $select.$isVisible();
        };
        scope.$isActive = function (index) {
          return $select.$isActive(index);
        };
        // Public methods
        $select.update = function (matches) {
          scope.$matches = matches;
          $select.$updateActiveIndex();
        };
        $select.activate = function (index) {
          if (options.multiple) {
            scope.$activeIndex.sort();
            $select.$isActive(index) ? scope.$activeIndex.splice(scope.$activeIndex.indexOf(index), 1) : scope.$activeIndex.push(index);
            if (options.sort)
              scope.$activeIndex.sort();
          } else {
            scope.$activeIndex = index;
          }
          return scope.$activeIndex;
        };
        $select.select = function (index) {
          var value = scope.$matches[index].value;
          $select.activate(index);
          if (options.multiple) {
            controller.$setViewValue(scope.$activeIndex.map(function (index) {
              return scope.$matches[index].value;
            }));
          } else {
            controller.$setViewValue(value);
          }
          controller.$render();
          if (parentScope)
            parentScope.$digest();
          // Hide if single select
          if (!options.multiple) {
            $select.hide();
          }
          // Emit event
          scope.$emit('$select.select', value, index);
        };
        // Protected methods
        $select.$updateActiveIndex = function () {
          if (controller.$modelValue && scope.$matches.length) {
            if (options.multiple && angular.isArray(controller.$modelValue)) {
              scope.$activeIndex = controller.$modelValue.map(function (value) {
                return $select.$getIndex(value);
              });
            } else {
              scope.$activeIndex = $select.$getIndex(controller.$modelValue);
            }
          } else if (scope.$activeIndex >= scope.$matches.length) {
            scope.$activeIndex = options.multiple ? [] : 0;
          }
        };
        $select.$isVisible = function () {
          if (!options.minLength || !controller) {
            return scope.$matches.length;
          }
          // minLength support
          return scope.$matches.length && controller.$viewValue.length >= options.minLength;
        };
        $select.$isActive = function (index) {
          if (options.multiple) {
            return scope.$activeIndex.indexOf(index) !== -1;
          } else {
            return scope.$activeIndex === index;
          }
        };
        $select.$getIndex = function (value) {
          var l = scope.$matches.length, i = l;
          if (!l)
            return;
          for (i = l; i--;) {
            if (scope.$matches[i].value === value)
              break;
          }
          if (i < 0)
            return;
          return i;
        };
        $select.$onMouseDown = function (evt) {
          // Prevent blur on mousedown on .dropdown-menu
          evt.preventDefault();
          evt.stopPropagation();
          // Emulate click for mobile devices
          if (isTouch) {
            var targetEl = angular.element(evt.target);
            targetEl.triggerHandler('click');
          }
        };
        $select.$onKeyDown = function (evt) {
          if (!/(9|13|38|40)/.test(evt.keyCode))
            return;
          evt.preventDefault();
          evt.stopPropagation();
          // Select with enter
          if (!options.multiple && (evt.keyCode === 13 || evt.keyCode === 9)) {
            return $select.select(scope.$activeIndex);
          }
          // Navigate with keyboard
          if (evt.keyCode === 38 && scope.$activeIndex > 0)
            scope.$activeIndex--;
          else if (evt.keyCode === 40 && scope.$activeIndex < scope.$matches.length - 1)
            scope.$activeIndex++;
          else if (angular.isUndefined(scope.$activeIndex))
            scope.$activeIndex = 0;
          scope.$digest();
        };
        // Overrides
        var _show = $select.show;
        $select.show = function () {
          _show();
          if (options.multiple) {
            $select.$element.addClass('select-multiple');
          }
          setTimeout(function () {
            $select.$element.on(isTouch ? 'touchstart' : 'mousedown', $select.$onMouseDown);
            if (options.keyboard) {
              element.on('keydown', $select.$onKeyDown);
            }
          });
        };
        var _hide = $select.hide;
        $select.hide = function () {
          $select.$element.off(isTouch ? 'touchstart' : 'mousedown', $select.$onMouseDown);
          if (options.keyboard) {
            element.off('keydown', $select.$onKeyDown);
          }
          _hide(true);
        };
        return $select;
      }
      SelectFactory.defaults = defaults;
      return SelectFactory;
    }
  ];
}).directive('bsSelect', [
  '$window',
  '$parse',
  '$q',
  '$select',
  '$parseOptions',
  function ($window, $parse, $q, $select, $parseOptions) {
    var defaults = $select.defaults;
    return {
      restrict: 'EAC',
      require: 'ngModel',
      link: function postLink(scope, element, attr, controller) {
        // Directive options
        var options = { scope: scope };
        angular.forEach([
          'placement',
          'container',
          'delay',
          'trigger',
          'keyboard',
          'html',
          'animation',
          'template',
          'placeholder',
          'multiple',
          'maxLength',
          'maxLengthHtml'
        ], function (key) {
          if (angular.isDefined(attr[key]))
            options[key] = attr[key];
        });
        // Add support for select markup
        if (element[0].nodeName.toLowerCase() === 'select') {
          var inputEl = element;
          inputEl.css('display', 'none');
          element = angular.element('<button type="button" class="btn btn-default"></button>');
          inputEl.after(element);
        }
        // Build proper ngOptions
        var parsedOptions = $parseOptions(attr.ngOptions);
        // Initialize select
        var select = $select(element, controller, options);
        // Watch ngOptions values before filtering for changes
        var watchedOptions = parsedOptions.$match[7].replace(/\|.+/, '').trim();
        scope.$watch(watchedOptions, function (newValue, oldValue) {
          // console.warn('scope.$watch(%s)', watchedOptions, newValue, oldValue);
          parsedOptions.valuesFn(scope, controller).then(function (values) {
            select.update(values);
            controller.$render();
          });
        }, true);
        // Watch model for changes
        scope.$watch(attr.ngModel, function (newValue, oldValue) {
          // console.warn('scope.$watch(%s)', attr.ngModel, newValue, oldValue);
          select.$updateActiveIndex();
        }, true);
        // Model rendering in view
        controller.$render = function () {
          // console.warn('$render', element.attr('ng-model'), 'controller.$modelValue', typeof controller.$modelValue, controller.$modelValue, 'controller.$viewValue', typeof controller.$viewValue, controller.$viewValue);
          var selected, index;
          if (options.multiple && angular.isArray(controller.$modelValue)) {
            selected = controller.$modelValue.map(function (value) {
              index = select.$getIndex(value);
              return angular.isDefined(index) ? select.$scope.$matches[index].label : false;
            }).filter(angular.isDefined);
            if (selected.length > (options.maxLength || defaults.maxLength)) {
              selected = selected.length + ' ' + (options.maxLengthHtml || defaults.maxLengthHtml);
            } else {
              selected = selected.join(', ');
            }
          } else {
            index = select.$getIndex(controller.$modelValue);
            selected = angular.isDefined(index) ? select.$scope.$matches[index].label : false;
          }
          element.html((selected ? selected : attr.placeholder || defaults.placeholder) + defaults.caretHtml);
        };
        // Garbage collection
        scope.$on('$destroy', function () {
          select.destroy();
          options = null;
          select = null;
        });
      }
    };
  }
]);

// Source: popover.js
angular.module('mgcrea.ngStrap.popover', ['mgcrea.ngStrap.tooltip']).provider('$popover', function () {
  var defaults = this.defaults = {
      animation: 'am-fade',
      placement: 'right',
      template: 'popover/popover.tpl.html',
      contentTemplate: false,
      trigger: 'click',
      keyboard: true,
      html: false,
      title: '',
      content: '',
      delay: 0,
      container: false
    };
  this.$get = [
    '$tooltip',
    function ($tooltip) {
      function PopoverFactory(element, config) {
        // Common vars
        var options = angular.extend({}, defaults, config);
        var $popover = $tooltip(element, options);
        // Support scope as string options [/*title, */content]
        if (options.content) {
          $popover.$scope.content = options.content;
        }
        return $popover;
      }
      return PopoverFactory;
    }
  ];
}).directive('bsPopover', [
  '$window',
  '$location',
  '$sce',
  '$popover',
  function ($window, $location, $sce, $popover) {
    var requestAnimationFrame = $window.requestAnimationFrame || $window.setTimeout;
    return {
      restrict: 'EAC',
      scope: true,
      link: function postLink(scope, element, attr) {
        // Directive options
        var options = { scope: scope };
        angular.forEach([
          'template',
          'contentTemplate',
          'placement',
          'container',
          'delay',
          'trigger',
          'keyboard',
          'html',
          'animation'
        ], function (key) {
          if (angular.isDefined(attr[key]))
            options[key] = attr[key];
        });
        // Support scope as data-attrs
        angular.forEach([
          'title',
          'content'
        ], function (key) {
          attr[key] && attr.$observe(key, function (newValue, oldValue) {
            scope[key] = $sce.trustAsHtml(newValue);
            angular.isDefined(oldValue) && requestAnimationFrame(function () {
              popover && popover.$applyPlacement();
            });
          });
        });
        // Support scope as an object
        attr.bsPopover && scope.$watch(attr.bsPopover, function (newValue, oldValue) {
          if (angular.isObject(newValue)) {
            angular.extend(scope, newValue);
          } else {
            scope.content = newValue;
          }
          angular.isDefined(oldValue) && requestAnimationFrame(function () {
            popover && popover.$applyPlacement();
          });
        }, true);
        // Initialize popover
        var popover = $popover(element, options);
        // Garbage collection
        scope.$on('$destroy', function () {
          popover.destroy();
          options = null;
          popover = null;
        });
      }
    };
  }
]);

// Source: scrollspy.js
angular.module('mgcrea.ngStrap.scrollspy', [
  'mgcrea.ngStrap.helpers.debounce',
  'mgcrea.ngStrap.helpers.dimensions'
]).provider('$scrollspy', function () {
  // Pool of registered spies
  var spies = this.$$spies = {};
  var defaults = this.defaults = {
      debounce: 150,
      throttle: 100,
      offset: 100
    };
  this.$get = [
    '$window',
    '$document',
    '$rootScope',
    'dimensions',
    'debounce',
    'throttle',
    function ($window, $document, $rootScope, dimensions, debounce, throttle) {
      var windowEl = angular.element($window);
      var docEl = angular.element($document.prop('documentElement'));
      var bodyEl = angular.element($window.document.body);
      // Helper functions
      function nodeName(element, name) {
        return element[0].nodeName && element[0].nodeName.toLowerCase() === name.toLowerCase();
      }
      function ScrollSpyFactory(config) {
        // Common vars
        var options = angular.extend({}, defaults, config);
        if (!options.element)
          options.element = bodyEl;
        var isWindowSpy = nodeName(options.element, 'body');
        var scrollEl = isWindowSpy ? windowEl : options.element;
        var scrollId = isWindowSpy ? 'window' : options.id;
        // Use existing spy
        if (spies[scrollId]) {
          spies[scrollId].$$count++;
          return spies[scrollId];
        }
        var $scrollspy = {};
        // Private vars
        var unbindViewContentLoaded, unbindIncludeContentLoaded;
        var trackedElements = $scrollspy.$trackedElements = [];
        var sortedElements = [];
        var activeTarget;
        var debouncedCheckPosition;
        var throttledCheckPosition;
        var debouncedCheckOffsets;
        var viewportHeight;
        var scrollTop;
        $scrollspy.init = function () {
          // Setup internal ref counter
          this.$$count = 1;
          // Bind events
          debouncedCheckPosition = debounce(this.checkPosition, options.debounce);
          throttledCheckPosition = throttle(this.checkPosition, options.throttle);
          scrollEl.on('click', this.checkPositionWithEventLoop);
          windowEl.on('resize', debouncedCheckPosition);
          scrollEl.on('scroll', throttledCheckPosition);
          debouncedCheckOffsets = debounce(this.checkOffsets, options.debounce);
          unbindViewContentLoaded = $rootScope.$on('$viewContentLoaded', debouncedCheckOffsets);
          unbindIncludeContentLoaded = $rootScope.$on('$includeContentLoaded', debouncedCheckOffsets);
          debouncedCheckOffsets();
          // Register spy for reuse
          if (scrollId) {
            spies[scrollId] = $scrollspy;
          }
        };
        $scrollspy.destroy = function () {
          // Check internal ref counter
          this.$$count--;
          if (this.$$count > 0) {
            return;
          }
          // Unbind events
          scrollEl.off('click', this.checkPositionWithEventLoop);
          windowEl.off('resize', debouncedCheckPosition);
          scrollEl.off('scroll', debouncedCheckPosition);
          unbindViewContentLoaded();
          unbindIncludeContentLoaded();
          if (scrollId) {
            delete spies[scrollId];
          }
        };
        $scrollspy.checkPosition = function () {
          // Not ready yet
          if (!sortedElements.length)
            return;
          // Calculate the scroll position
          scrollTop = (isWindowSpy ? $window.pageYOffset : scrollEl.prop('scrollTop')) || 0;
          // Calculate the viewport height for use by the components
          viewportHeight = Math.max($window.innerHeight, docEl.prop('clientHeight'));
          // Activate first element if scroll is smaller
          if (scrollTop < sortedElements[0].offsetTop && activeTarget !== sortedElements[0].target) {
            return $scrollspy.$activateElement(sortedElements[0]);
          }
          // Activate proper element
          for (var i = sortedElements.length; i--;) {
            if (angular.isUndefined(sortedElements[i].offsetTop) || sortedElements[i].offsetTop === null)
              continue;
            if (activeTarget === sortedElements[i].target)
              continue;
            if (scrollTop < sortedElements[i].offsetTop)
              continue;
            if (sortedElements[i + 1] && scrollTop > sortedElements[i + 1].offsetTop)
              continue;
            return $scrollspy.$activateElement(sortedElements[i]);
          }
        };
        $scrollspy.checkPositionWithEventLoop = function () {
          setTimeout(this.checkPosition, 1);
        };
        // Protected methods
        $scrollspy.$activateElement = function (element) {
          if (activeTarget) {
            var activeElement = $scrollspy.$getTrackedElement(activeTarget);
            if (activeElement) {
              activeElement.source.removeClass('active');
              if (nodeName(activeElement.source, 'li') && nodeName(activeElement.source.parent().parent(), 'li')) {
                activeElement.source.parent().parent().removeClass('active');
              }
            }
          }
          activeTarget = element.target;
          element.source.addClass('active');
          if (nodeName(element.source, 'li') && nodeName(element.source.parent().parent(), 'li')) {
            element.source.parent().parent().addClass('active');
          }
        };
        $scrollspy.$getTrackedElement = function (target) {
          return trackedElements.filter(function (obj) {
            return obj.target === target;
          })[0];
        };
        // Track offsets behavior
        $scrollspy.checkOffsets = function () {
          angular.forEach(trackedElements, function (trackedElement) {
            var targetElement = document.querySelector(trackedElement.target);
            trackedElement.offsetTop = targetElement ? dimensions.offset(targetElement).top : null;
            if (options.offset && trackedElement.offsetTop !== null)
              trackedElement.offsetTop -= options.offset * 1;
          });
          sortedElements = trackedElements.filter(function (el) {
            return el.offsetTop !== null;
          }).sort(function (a, b) {
            return a.offsetTop - b.offsetTop;
          });
          debouncedCheckPosition();
        };
        $scrollspy.trackElement = function (target, source) {
          trackedElements.push({
            target: target,
            source: source
          });
        };
        $scrollspy.untrackElement = function (target, source) {
          var toDelete;
          for (var i = trackedElements.length; i--;) {
            if (trackedElements[i].target === target && trackedElements[i].source === source) {
              toDelete = i;
              break;
            }
          }
          trackedElements = trackedElements.splice(toDelete, 1);
        };
        $scrollspy.activate = function (i) {
          trackedElements[i].addClass('active');
        };
        // Initialize plugin
        $scrollspy.init();
        return $scrollspy;
      }
      return ScrollSpyFactory;
    }
  ];
}).directive('bsScrollspy', [
  '$rootScope',
  'debounce',
  'dimensions',
  '$scrollspy',
  function ($rootScope, debounce, dimensions, $scrollspy) {
    return {
      restrict: 'EAC',
      link: function postLink(scope, element, attr) {
        var options = { scope: scope };
        angular.forEach([
          'offset',
          'target'
        ], function (key) {
          if (angular.isDefined(attr[key]))
            options[key] = attr[key];
        });
        var scrollspy = $scrollspy(options);
        scrollspy.trackElement(options.target, element);
        scope.$on('$destroy', function () {
          scrollspy.untrackElement(options.target, element);
          scrollspy.destroy();
          options = null;
          scrollspy = null;
        });
      }
    };
  }
]).directive('bsScrollspyList', [
  '$rootScope',
  'debounce',
  'dimensions',
  '$scrollspy',
  function ($rootScope, debounce, dimensions, $scrollspy) {
    return {
      restrict: 'A',
      compile: function postLink(element, attr) {
        var children = element[0].querySelectorAll('li > a[href]');
        angular.forEach(children, function (child) {
          var childEl = angular.element(child);
          childEl.parent().attr('bs-scrollspy', '').attr('data-target', childEl.attr('href'));
        });
      }
    };
  }
]);

// Source: tab.js
angular.module('mgcrea.ngStrap.tab', []).run([
  '$templateCache',
  function ($templateCache) {
    $templateCache.put('$pane', '{{pane.content}}');
  }
]).provider('$tab', function () {
  var defaults = this.defaults = {
      animation: 'am-fade',
      template: 'tab/tab.tpl.html'
    };
  this.$get = function () {
    return { defaults: defaults };
  };
}).directive('bsTabs', [
  '$window',
  '$animate',
  '$tab',
  function ($window, $animate, $tab) {
    var defaults = $tab.defaults;
    return {
      restrict: 'EAC',
      scope: true,
      require: '?ngModel',
      templateUrl: function (element, attr) {
        return attr.template || defaults.template;
      },
      link: function postLink(scope, element, attr, controller) {
        // Directive options
        var options = defaults;
        angular.forEach(['animation'], function (key) {
          if (angular.isDefined(attr[key]))
            options[key] = attr[key];
        });
        // Require scope as an object
        attr.bsTabs && scope.$watch(attr.bsTabs, function (newValue, oldValue) {
          scope.panes = newValue;
        }, true);
        // Add base class
        element.addClass('tabs');
        // Support animations
        if (options.animation) {
          element.addClass(options.animation);
        }
        scope.active = scope.activePane = 0;
        // view -> model
        scope.setActive = function (index, ev) {
          scope.active = index;
          if (controller) {
            controller.$setViewValue(index);
          }
        };
        // model -> view
        if (controller) {
          controller.$render = function () {
            scope.active = controller.$modelValue * 1;
          };
        }
      }
    };
  }
]);

// Source: timepicker.js
angular.module('mgcrea.ngStrap.timepicker', [
  'mgcrea.ngStrap.helpers.dateParser',
  'mgcrea.ngStrap.tooltip'
]).provider('$timepicker', function () {
  var defaults = this.defaults = {
      animation: 'am-fade',
      prefixClass: 'timepicker',
      placement: 'bottom-left',
      template: 'timepicker/timepicker.tpl.html',
      trigger: 'focus',
      container: false,
      keyboard: true,
      html: false,
      delay: 0,
      useNative: true,
      timeType: 'date',
      timeFormat: 'shortTime',
      autoclose: false,
      minTime: -Infinity,
      maxTime: +Infinity,
      length: 5,
      hourStep: 1,
      minuteStep: 5
    };
  this.$get = [
    '$window',
    '$document',
    '$rootScope',
    '$sce',
    '$locale',
    'dateFilter',
    '$tooltip',
    function ($window, $document, $rootScope, $sce, $locale, dateFilter, $tooltip) {
      var bodyEl = angular.element($window.document.body);
      var isTouch = 'createTouch' in $window.document;
      var isNative = /(ip(a|o)d|iphone|android)/gi.test($window.navigator.userAgent);
      if (!defaults.lang)
        defaults.lang = $locale.id;
      function timepickerFactory(element, controller, config) {
        var $timepicker = $tooltip(element, angular.extend({}, defaults, config));
        var parentScope = config.scope;
        var options = $timepicker.$options;
        var scope = $timepicker.$scope;
        // View vars
        var selectedIndex = 0;
        var startDate = controller.$dateValue || new Date();
        var viewDate = {
            hour: startDate.getHours(),
            meridian: startDate.getHours() < 12,
            minute: startDate.getMinutes(),
            second: startDate.getSeconds(),
            millisecond: startDate.getMilliseconds()
          };
        var format = $locale.DATETIME_FORMATS[options.timeFormat] || options.timeFormat;
        var formats = /(h+)[:]?(m+)[ ]?(a?)/i.exec(format).slice(1);
        // Scope methods
        scope.$select = function (date, index) {
          $timepicker.select(date, index);
        };
        scope.$moveIndex = function (value, index) {
          $timepicker.$moveIndex(value, index);
        };
        scope.$switchMeridian = function (date) {
          $timepicker.switchMeridian(date);
        };
        // Public methods
        $timepicker.update = function (date) {
          // console.warn('$timepicker.update() newValue=%o', date);
          if (angular.isDate(date) && !isNaN(date.getTime())) {
            $timepicker.$date = date;
            angular.extend(viewDate, {
              hour: date.getHours(),
              minute: date.getMinutes(),
              second: date.getSeconds(),
              millisecond: date.getMilliseconds()
            });
            $timepicker.$build();
          } else if (!$timepicker.$isBuilt) {
            $timepicker.$build();
          }
        };
        $timepicker.select = function (date, index, keep) {
          // console.warn('$timepicker.select', date, scope.$mode);
          if (!controller.$dateValue || isNaN(controller.$dateValue.getTime()))
            controller.$dateValue = new Date(1970, 0, 1);
          if (!angular.isDate(date))
            date = new Date(date);
          if (index === 0)
            controller.$dateValue.setHours(date.getHours());
          else if (index === 1)
            controller.$dateValue.setMinutes(date.getMinutes());
          controller.$setViewValue(controller.$dateValue);
          controller.$render();
          if (options.autoclose && !keep) {
            $timepicker.hide(true);
          }
        };
        $timepicker.switchMeridian = function (date) {
          var hours = (date || controller.$dateValue).getHours();
          controller.$dateValue.setHours(hours < 12 ? hours + 12 : hours - 12);
          controller.$setViewValue(controller.$dateValue);
          controller.$render();
        };
        // Protected methods
        $timepicker.$build = function () {
          // console.warn('$timepicker.$build() viewDate=%o', viewDate);
          var i, midIndex = scope.midIndex = parseInt(options.length / 2, 10);
          var hours = [], hour;
          for (i = 0; i < options.length; i++) {
            hour = new Date(1970, 0, 1, viewDate.hour - (midIndex - i) * options.hourStep);
            hours.push({
              date: hour,
              label: dateFilter(hour, formats[0]),
              selected: $timepicker.$date && $timepicker.$isSelected(hour, 0),
              disabled: $timepicker.$isDisabled(hour, 0)
            });
          }
          var minutes = [], minute;
          for (i = 0; i < options.length; i++) {
            minute = new Date(1970, 0, 1, 0, viewDate.minute - (midIndex - i) * options.minuteStep);
            minutes.push({
              date: minute,
              label: dateFilter(minute, formats[1]),
              selected: $timepicker.$date && $timepicker.$isSelected(minute, 1),
              disabled: $timepicker.$isDisabled(minute, 1)
            });
          }
          var rows = [];
          for (i = 0; i < options.length; i++) {
            rows.push([
              hours[i],
              minutes[i]
            ]);
          }
          scope.rows = rows;
          scope.showAM = !!formats[2];
          scope.isAM = ($timepicker.$date || hours[midIndex].date).getHours() < 12;
          $timepicker.$isBuilt = true;
        };
        $timepicker.$isSelected = function (date, index) {
          if (!$timepicker.$date)
            return false;
          else if (index === 0) {
            return date.getHours() === $timepicker.$date.getHours();
          } else if (index === 1) {
            return date.getMinutes() === $timepicker.$date.getMinutes();
          }
        };
        $timepicker.$isDisabled = function (date, index) {
          var selectedTime;
          if (index === 0) {
            selectedTime = date.getTime() + viewDate.minute * 60000;
          } else if (index === 1) {
            selectedTime = date.getTime() + viewDate.hour * 3600000;
          }
          return selectedTime < options.minTime * 1 || selectedTime > options.maxTime * 1;
        };
        $timepicker.$moveIndex = function (value, index) {
          var targetDate;
          if (index === 0) {
            targetDate = new Date(1970, 0, 1, viewDate.hour + value * options.length, viewDate.minute);
            angular.extend(viewDate, { hour: targetDate.getHours() });
          } else if (index === 1) {
            targetDate = new Date(1970, 0, 1, viewDate.hour, viewDate.minute + value * options.length * options.minuteStep);
            angular.extend(viewDate, { minute: targetDate.getMinutes() });
          }
          $timepicker.$build();
        };
        $timepicker.$onMouseDown = function (evt) {
          // Prevent blur on mousedown on .dropdown-menu
          if (evt.target.nodeName.toLowerCase() !== 'input')
            evt.preventDefault();
          evt.stopPropagation();
          // Emulate click for mobile devices
          if (isTouch) {
            var targetEl = angular.element(evt.target);
            if (targetEl[0].nodeName.toLowerCase() !== 'button') {
              targetEl = targetEl.parent();
            }
            targetEl.triggerHandler('click');
          }
        };
        $timepicker.$onKeyDown = function (evt) {
          if (!/(38|37|39|40|13)/.test(evt.keyCode) || evt.shiftKey || evt.altKey)
            return;
          evt.preventDefault();
          evt.stopPropagation();
          // Close on enter
          if (evt.keyCode === 13)
            return $timepicker.hide(true);
          // Navigate with keyboard
          var newDate = new Date($timepicker.$date);
          var hours = newDate.getHours(), hoursLength = dateFilter(newDate, 'h').length;
          var minutes = newDate.getMinutes(), minutesLength = dateFilter(newDate, 'mm').length;
          var lateralMove = /(37|39)/.test(evt.keyCode);
          var count = 2 + !!formats[2] * 1;
          // Navigate indexes (left, right)
          if (lateralMove) {
            if (evt.keyCode === 37)
              selectedIndex = selectedIndex < 1 ? count - 1 : selectedIndex - 1;
            else if (evt.keyCode === 39)
              selectedIndex = selectedIndex < count - 1 ? selectedIndex + 1 : 0;
          }
          // Update values (up, down)
          var selectRange = [
              0,
              hoursLength
            ];
          if (selectedIndex === 0) {
            if (evt.keyCode === 38)
              newDate.setHours(hours - parseInt(options.hourStep, 10));
            else if (evt.keyCode === 40)
              newDate.setHours(hours + parseInt(options.hourStep, 10));
            selectRange = [
              0,
              hoursLength
            ];
          } else if (selectedIndex === 1) {
            if (evt.keyCode === 38)
              newDate.setMinutes(minutes - parseInt(options.minuteStep, 10));
            else if (evt.keyCode === 40)
              newDate.setMinutes(minutes + parseInt(options.minuteStep, 10));
            selectRange = [
              hoursLength + 1,
              hoursLength + 1 + minutesLength
            ];
          } else if (selectedIndex === 2) {
            if (!lateralMove)
              $timepicker.switchMeridian();
            selectRange = [
              hoursLength + 1 + minutesLength + 1,
              hoursLength + 1 + minutesLength + 3
            ];
          }
          $timepicker.select(newDate, selectedIndex, true);
          createSelection(selectRange[0], selectRange[1]);
          parentScope.$digest();
        };
        // Private
        function createSelection(start, end) {
          if (element[0].createTextRange) {
            var selRange = element[0].createTextRange();
            selRange.collapse(true);
            selRange.moveStart('character', start);
            selRange.moveEnd('character', end);
            selRange.select();
          } else if (element[0].setSelectionRange) {
            element[0].setSelectionRange(start, end);
          } else if (angular.isUndefined(element[0].selectionStart)) {
            element[0].selectionStart = start;
            element[0].selectionEnd = end;
          }
        }
        function focusElement() {
          element[0].focus();
        }
        // Overrides
        var _init = $timepicker.init;
        $timepicker.init = function () {
          if (isNative && options.useNative) {
            element.prop('type', 'time');
            element.css('-webkit-appearance', 'textfield');
            return;
          } else if (isTouch) {
            element.prop('type', 'text');
            element.attr('readonly', 'true');
            element.on('click', focusElement);
          }
          _init();
        };
        var _destroy = $timepicker.destroy;
        $timepicker.destroy = function () {
          if (isNative && options.useNative) {
            element.off('click', focusElement);
          }
          _destroy();
        };
        var _show = $timepicker.show;
        $timepicker.show = function () {
          _show();
          setTimeout(function () {
            $timepicker.$element.on(isTouch ? 'touchstart' : 'mousedown', $timepicker.$onMouseDown);
            if (options.keyboard) {
              element.on('keydown', $timepicker.$onKeyDown);
            }
          });
        };
        var _hide = $timepicker.hide;
        $timepicker.hide = function (blur) {
          $timepicker.$element.off(isTouch ? 'touchstart' : 'mousedown', $timepicker.$onMouseDown);
          if (options.keyboard) {
            element.off('keydown', $timepicker.$onKeyDown);
          }
          _hide(blur);
        };
        return $timepicker;
      }
      timepickerFactory.defaults = defaults;
      return timepickerFactory;
    }
  ];
}).directive('bsTimepicker', [
  '$window',
  '$parse',
  '$q',
  '$locale',
  'dateFilter',
  '$timepicker',
  '$dateParser',
  '$timeout',
  function ($window, $parse, $q, $locale, dateFilter, $timepicker, $dateParser, $timeout) {
    var defaults = $timepicker.defaults;
    var isNative = /(ip(a|o)d|iphone|android)/gi.test($window.navigator.userAgent);
    var requestAnimationFrame = $window.requestAnimationFrame || $window.setTimeout;
    return {
      restrict: 'EAC',
      require: 'ngModel',
      link: function postLink(scope, element, attr, controller) {
        // Directive options
        var options = {
            scope: scope,
            controller: controller
          };
        angular.forEach([
          'placement',
          'container',
          'delay',
          'trigger',
          'keyboard',
          'html',
          'animation',
          'template',
          'autoclose',
          'timeType',
          'timeFormat',
          'useNative',
          'hourStep',
          'minuteStep',
          'length'
        ], function (key) {
          if (angular.isDefined(attr[key]))
            options[key] = attr[key];
        });
        // Initialize timepicker
        if (isNative && (options.useNative || defaults.useNative))
          options.timeFormat = 'HH:mm';
        var timepicker = $timepicker(element, controller, options);
        options = timepicker.$options;
        // Initialize parser
        var dateParser = $dateParser({
            format: options.timeFormat,
            lang: options.lang
          });
        // Observe attributes for changes
        angular.forEach([
          'minTime',
          'maxTime'
        ], function (key) {
          // console.warn('attr.$observe(%s)', key, attr[key]);
          angular.isDefined(attr[key]) && attr.$observe(key, function (newValue) {
            if (newValue === 'now') {
              timepicker.$options[key] = new Date().setFullYear(1970, 0, 1);
            } else if (angular.isString(newValue) && newValue.match(/^".+"$/)) {
              timepicker.$options[key] = +new Date(newValue.substr(1, newValue.length - 2));
            } else {
              timepicker.$options[key] = dateParser.parse(newValue, new Date(1970, 0, 1, 0));
            }
            !isNaN(timepicker.$options[key]) && timepicker.$build();
          });
        });
        // Watch model for changes
        scope.$watch(attr.ngModel, function (newValue, oldValue) {
          // console.warn('scope.$watch(%s)', attr.ngModel, newValue, oldValue, controller.$dateValue);
          timepicker.update(controller.$dateValue);
        }, true);
        // viewValue -> $parsers -> modelValue
        controller.$parsers.unshift(function (viewValue) {
          // console.warn('$parser("%s"): viewValue=%o', element.attr('ng-model'), viewValue);
          // Null values should correctly reset the model value & validity
          if (!viewValue) {
            controller.$setValidity('date', true);
            return;
          }
          var parsedTime = dateParser.parse(viewValue, controller.$dateValue);
          if (!parsedTime || isNaN(parsedTime.getTime())) {
            controller.$setValidity('date', false);
          } else {
            var isValid = parsedTime.getTime() >= options.minTime && parsedTime.getTime() <= options.maxTime;
            controller.$setValidity('date', isValid);
            // Only update the model when we have a valid date
            if (isValid)
              controller.$dateValue = parsedTime;
          }
          if (options.timeType === 'string') {
            return dateFilter(viewValue, options.timeFormat);
          } else if (options.timeType === 'number') {
            return controller.$dateValue.getTime();
          } else if (options.timeType === 'iso') {
            return controller.$dateValue.toISOString();
          } else {
            return new Date(controller.$dateValue);
          }
        });
        // modelValue -> $formatters -> viewValue
        controller.$formatters.push(function (modelValue) {
          // console.warn('$formatter("%s"): modelValue=%o (%o)', element.attr('ng-model'), modelValue, typeof modelValue);
          var date;
          if (angular.isUndefined(modelValue) || modelValue === null) {
            date = NaN;
          } else if (angular.isDate(modelValue)) {
            date = modelValue;
          } else if (options.timeType === 'string') {
            date = dateParser.parse(modelValue);
          } else {
            date = new Date(modelValue);
          }
          // Setup default value?
          // if(isNaN(date.getTime())) date = new Date(new Date().setMinutes(0) + 36e5);
          controller.$dateValue = date;
          return controller.$dateValue;
        });
        // viewValue -> element
        controller.$render = function () {
          // console.warn('$render("%s"): viewValue=%o', element.attr('ng-model'), controller.$viewValue);
          element.val(!controller.$dateValue || isNaN(controller.$dateValue.getTime()) ? '' : dateFilter(controller.$dateValue, options.timeFormat));
        };
        // Garbage collection
        scope.$on('$destroy', function () {
          timepicker.destroy();
          options = null;
          timepicker = null;
        });
      }
    };
  }
]);

// Source: tooltip.js
angular.module('mgcrea.ngStrap.tooltip', ['mgcrea.ngStrap.helpers.dimensions']).provider('$tooltip', function () {
  var defaults = this.defaults = {
      animation: 'am-fade',
      prefixClass: 'tooltip',
      prefixEvent: 'tooltip',
      container: false,
      placement: 'top',
      template: 'tooltip/tooltip.tpl.html',
      contentTemplate: false,
      trigger: 'hover focus',
      keyboard: false,
      html: false,
      show: false,
      title: '',
      type: '',
      delay: 0
    };
  this.$get = [
    '$window',
    '$rootScope',
    '$compile',
    '$q',
    '$templateCache',
    '$http',
    '$animate',
    '$timeout',
    'dimensions',
    '$$rAF',
    function ($window, $rootScope, $compile, $q, $templateCache, $http, $animate, $timeout, dimensions, $$rAF) {
      var trim = String.prototype.trim;
      var isTouch = 'createTouch' in $window.document;
      var htmlReplaceRegExp = /ng-bind="/gi;
      function TooltipFactory(element, config) {
        var $tooltip = {};
        // Common vars
        var nodeName = element[0].nodeName.toLowerCase();
        var options = $tooltip.$options = angular.extend({}, defaults, config);
        $tooltip.$promise = fetchTemplate(options.template);
        var scope = $tooltip.$scope = options.scope && options.scope.$new() || $rootScope.$new();
        if (options.delay && angular.isString(options.delay)) {
          options.delay = parseFloat(options.delay);
        }
        // Support scope as string options
        if (options.title) {
          $tooltip.$scope.title = options.title;
        }
        // Provide scope helpers
        scope.$hide = function () {
          scope.$$postDigest(function () {
            $tooltip.hide();
          });
        };
        scope.$show = function () {
          scope.$$postDigest(function () {
            $tooltip.show();
          });
        };
        scope.$toggle = function () {
          scope.$$postDigest(function () {
            $tooltip.toggle();
          });
        };
        $tooltip.$isShown = scope.$isShown = false;
        // Private vars
        var timeout, hoverState;
        // Support contentTemplate option
        if (options.contentTemplate) {
          $tooltip.$promise = $tooltip.$promise.then(function (template) {
            var templateEl = angular.element(template);
            return fetchTemplate(options.contentTemplate).then(function (contentTemplate) {
              var contentEl = findElement('[ng-bind="content"]', templateEl[0]);
              if (!contentEl.length)
                contentEl = findElement('[ng-bind="title"]', templateEl[0]);
              contentEl.removeAttr('ng-bind').html(contentTemplate);
              return templateEl[0].outerHTML;
            });
          });
        }
        // Fetch, compile then initialize tooltip
        var tipLinker, tipElement, tipTemplate, tipContainer;
        $tooltip.$promise.then(function (template) {
          if (angular.isObject(template))
            template = template.data;
          if (options.html)
            template = template.replace(htmlReplaceRegExp, 'ng-bind-html="');
          template = trim.apply(template);
          tipTemplate = template;
          tipLinker = $compile(template);
          $tooltip.init();
        });
        $tooltip.init = function () {
          // Options: delay
          if (options.delay && angular.isNumber(options.delay)) {
            options.delay = {
              show: options.delay,
              hide: options.delay
            };
          }
          // Replace trigger on touch devices ?
          // if(isTouch && options.trigger === defaults.trigger) {
          //   options.trigger.replace(/hover/g, 'click');
          // }
          // Options : container
          if (options.container === 'self') {
            tipContainer = element;
          } else if (options.container) {
            tipContainer = findElement(options.container);
          }
          // Options: trigger
          var triggers = options.trigger.split(' ');
          angular.forEach(triggers, function (trigger) {
            if (trigger === 'click') {
              element.on('click', $tooltip.toggle);
            } else if (trigger !== 'manual') {
              element.on(trigger === 'hover' ? 'mouseenter' : 'focus', $tooltip.enter);
              element.on(trigger === 'hover' ? 'mouseleave' : 'blur', $tooltip.leave);
              nodeName === 'button' && trigger !== 'hover' && element.on(isTouch ? 'touchstart' : 'mousedown', $tooltip.$onFocusElementMouseDown);
            }
          });
          // Options: show
          if (options.show) {
            scope.$$postDigest(function () {
              options.trigger === 'focus' ? element[0].focus() : $tooltip.show();
            });
          }
        };
        $tooltip.destroy = function () {
          // Unbind events
          var triggers = options.trigger.split(' ');
          for (var i = triggers.length; i--;) {
            var trigger = triggers[i];
            if (trigger === 'click') {
              element.off('click', $tooltip.toggle);
            } else if (trigger !== 'manual') {
              element.off(trigger === 'hover' ? 'mouseenter' : 'focus', $tooltip.enter);
              element.off(trigger === 'hover' ? 'mouseleave' : 'blur', $tooltip.leave);
              nodeName === 'button' && trigger !== 'hover' && element.off(isTouch ? 'touchstart' : 'mousedown', $tooltip.$onFocusElementMouseDown);
            }
          }
          // Remove element
          if (tipElement) {
            tipElement.remove();
            tipElement = null;
          }
          // Destroy scope
          scope.$destroy();
        };
        $tooltip.enter = function () {
          clearTimeout(timeout);
          hoverState = 'in';
          if (!options.delay || !options.delay.show) {
            return $tooltip.show();
          }
          timeout = setTimeout(function () {
            if (hoverState === 'in')
              $tooltip.show();
          }, options.delay.show);
        };
        $tooltip.show = function () {
          scope.$emit(options.prefixEvent + '.show.before', $tooltip);
          var parent = options.container ? tipContainer : null;
          var after = options.container ? null : element;
          // Hide any existing tipElement
          if (tipElement)
            tipElement.remove();
          // Fetch a cloned element linked from template
          tipElement = $tooltip.$element = tipLinker(scope, function (clonedElement, scope) {
          });
          // Set the initial positioning.
          tipElement.css({
            top: '0px',
            left: '0px',
            display: 'block'
          }).addClass(options.placement);
          // Options: animation
          if (options.animation)
            tipElement.addClass(options.animation);
          // Options: type
          if (options.type)
            tipElement.addClass(options.prefixClass + '-' + options.type);
          $animate.enter(tipElement, parent, after, function () {
            scope.$emit(options.prefixEvent + '.show', $tooltip);
          });
          $tooltip.$isShown = scope.$isShown = true;
          scope.$$phase || scope.$root.$$phase || scope.$digest();
          $$rAF($tooltip.$applyPlacement);
          // var a = bodyEl.offsetWidth + 1; ?
          // Bind events
          if (options.keyboard) {
            if (options.trigger !== 'focus') {
              $tooltip.focus();
              tipElement.on('keyup', $tooltip.$onKeyUp);
            } else {
              element.on('keyup', $tooltip.$onFocusKeyUp);
            }
          }
        };
        $tooltip.leave = function () {
          clearTimeout(timeout);
          hoverState = 'out';
          if (!options.delay || !options.delay.hide) {
            return $tooltip.hide();
          }
          timeout = setTimeout(function () {
            if (hoverState === 'out') {
              $tooltip.hide();
            }
          }, options.delay.hide);
        };
        $tooltip.hide = function (blur) {
          if (!$tooltip.$isShown)
            return;
          scope.$emit(options.prefixEvent + '.hide.before', $tooltip);
          $animate.leave(tipElement, function () {
            scope.$emit(options.prefixEvent + '.hide', $tooltip);
          });
          $tooltip.$isShown = scope.$isShown = false;
          scope.$$phase || scope.$root.$$phase || scope.$digest();
          // Unbind events
          if (options.keyboard && tipElement !== null) {
            tipElement.off('keyup', $tooltip.$onKeyUp);
          }
          // Allow to blur the input when hidden, like when pressing enter key
          if (blur && options.trigger === 'focus') {
            return element[0].blur();
          }
        };
        $tooltip.toggle = function () {
          $tooltip.$isShown ? $tooltip.leave() : $tooltip.enter();
        };
        $tooltip.focus = function () {
          tipElement[0].focus();
        };
        // Protected methods
        $tooltip.$applyPlacement = function () {
          if (!tipElement)
            return;
          // Get the position of the tooltip element.
          var elementPosition = getPosition();
          // Get the height and width of the tooltip so we can center it.
          var tipWidth = tipElement.prop('offsetWidth'), tipHeight = tipElement.prop('offsetHeight');
          // Get the tooltip's top and left coordinates to center it with this directive.
          var tipPosition = getCalculatedOffset(options.placement, elementPosition, tipWidth, tipHeight);
          // Now set the calculated positioning.
          tipPosition.top += 'px';
          tipPosition.left += 'px';
          tipElement.css(tipPosition);
        };
        $tooltip.$onKeyUp = function (evt) {
          evt.which === 27 && $tooltip.hide();
        };
        $tooltip.$onFocusKeyUp = function (evt) {
          evt.which === 27 && element[0].blur();
        };
        $tooltip.$onFocusElementMouseDown = function (evt) {
          evt.preventDefault();
          evt.stopPropagation();
          // Some browsers do not auto-focus buttons (eg. Safari)
          $tooltip.$isShown ? element[0].blur() : element[0].focus();
        };
        // Private methods
        function getPosition() {
          if (options.container === 'body') {
            return dimensions.offset(element[0]);
          } else {
            return dimensions.position(element[0]);
          }
        }
        function getCalculatedOffset(placement, position, actualWidth, actualHeight) {
          var offset;
          var split = placement.split('-');
          switch (split[0]) {
          case 'right':
            offset = {
              top: position.top + position.height / 2 - actualHeight / 2,
              left: position.left + position.width
            };
            break;
          case 'bottom':
            offset = {
              top: position.top + position.height,
              left: position.left + position.width / 2 - actualWidth / 2
            };
            break;
          case 'left':
            offset = {
              top: position.top + position.height / 2 - actualHeight / 2,
              left: position.left - actualWidth
            };
            break;
          default:
            offset = {
              top: position.top - actualHeight,
              left: position.left + position.width / 2 - actualWidth / 2
            };
            break;
          }
          if (!split[1]) {
            return offset;
          }
          // Add support for corners @todo css
          if (split[0] === 'top' || split[0] === 'bottom') {
            switch (split[1]) {
            case 'left':
              offset.left = position.left;
              break;
            case 'right':
              offset.left = position.left + position.width - actualWidth;
            }
          } else if (split[0] === 'left' || split[0] === 'right') {
            switch (split[1]) {
            case 'top':
              offset.top = position.top - actualHeight;
              break;
            case 'bottom':
              offset.top = position.top + position.height;
            }
          }
          return offset;
        }
        return $tooltip;
      }
      // Helper functions
      function findElement(query, element) {
        return angular.element((element || document).querySelectorAll(query));
      }
      function fetchTemplate(template) {
        return $q.when($templateCache.get(template) || $http.get(template)).then(function (res) {
          if (angular.isObject(res)) {
            $templateCache.put(template, res.data);
            return res.data;
          }
          return res;
        });
      }
      return TooltipFactory;
    }
  ];
}).directive('bsTooltip', [
  '$window',
  '$location',
  '$sce',
  '$tooltip',
  '$$rAF',
  function ($window, $location, $sce, $tooltip, $$rAF) {
    return {
      restrict: 'EAC',
      scope: true,
      link: function postLink(scope, element, attr, transclusion) {
        // Directive options
        var options = { scope: scope };
        angular.forEach([
          'template',
          'contentTemplate',
          'placement',
          'container',
          'delay',
          'trigger',
          'keyboard',
          'html',
          'animation',
          'type'
        ], function (key) {
          if (angular.isDefined(attr[key]))
            options[key] = attr[key];
        });
        // Observe scope attributes for change
        angular.forEach(['title'], function (key) {
          attr[key] && attr.$observe(key, function (newValue, oldValue) {
            scope[key] = $sce.trustAsHtml(newValue);
            angular.isDefined(oldValue) && $$rAF(function () {
              tooltip && tooltip.$applyPlacement();
            });
          });
        });
        // Support scope as an object
        attr.bsTooltip && scope.$watch(attr.bsTooltip, function (newValue, oldValue) {
          if (angular.isObject(newValue)) {
            angular.extend(scope, newValue);
          } else {
            scope.title = newValue;
          }
          angular.isDefined(oldValue) && $$rAF(function () {
            tooltip && tooltip.$applyPlacement();
          });
        }, true);
        // Initialize popover
        var tooltip = $tooltip(element, options);
        // Garbage collection
        scope.$on('$destroy', function () {
          tooltip.destroy();
          options = null;
          tooltip = null;
        });
      }
    };
  }
]);

// Source: typeahead.js
angular.module('mgcrea.ngStrap.typeahead', [
  'mgcrea.ngStrap.tooltip',
  'mgcrea.ngStrap.helpers.parseOptions'
]).provider('$typeahead', function () {
  var defaults = this.defaults = {
      animation: 'am-fade',
      prefixClass: 'typeahead',
      placement: 'bottom-left',
      template: 'typeahead/typeahead.tpl.html',
      trigger: 'focus',
      container: false,
      keyboard: true,
      html: false,
      delay: 0,
      minLength: 1,
      filter: 'filter',
      limit: 6
    };
  this.$get = [
    '$window',
    '$rootScope',
    '$tooltip',
    function ($window, $rootScope, $tooltip) {
      var bodyEl = angular.element($window.document.body);
      function TypeaheadFactory(element, controller, config) {
        var $typeahead = {};
        // Common vars
        var options = angular.extend({}, defaults, config);
        $typeahead = $tooltip(element, options);
        var parentScope = config.scope;
        var scope = $typeahead.$scope;
        scope.$resetMatches = function () {
          scope.$matches = [];
          scope.$activeIndex = 0;
        };
        scope.$resetMatches();
        scope.$activate = function (index) {
          scope.$$postDigest(function () {
            $typeahead.activate(index);
          });
        };
        scope.$select = function (index, evt) {
          scope.$$postDigest(function () {
            $typeahead.select(index);
          });
        };
        scope.$isVisible = function () {
          return $typeahead.$isVisible();
        };
        // Public methods
        $typeahead.update = function (matches) {
          scope.$matches = matches;
          if (scope.$activeIndex >= matches.length) {
            scope.$activeIndex = 0;
          }
        };
        $typeahead.activate = function (index) {
          scope.$activeIndex = index;
        };
        $typeahead.select = function (index) {
          var value = scope.$matches[index].value;
          controller.$setViewValue(value);
          scope.$resetMatches();
          controller.$render();
          if (parentScope)
            parentScope.$digest();
          // Emit event
          scope.$emit('$typeahead.select', value, index);
        };
        // Protected methods
        $typeahead.$isVisible = function () {
          if (!options.minLength || !controller) {
            return !!scope.$matches.length;
          }
          // minLength support
          return scope.$matches.length && angular.isString(controller.$viewValue) && controller.$viewValue.length >= options.minLength;
        };
        $typeahead.$getIndex = function (value) {
          var l = scope.$matches.length, i = l;
          if (!l)
            return;
          for (i = l; i--;) {
            if (scope.$matches[i].value === value)
              break;
          }
          if (i < 0)
            return;
          return i;
        };
        $typeahead.$onMouseDown = function (evt) {
          // Prevent blur on mousedown
          evt.preventDefault();
          evt.stopPropagation();
        };
        $typeahead.$onKeyDown = function (evt) {
          if (!/(38|40|13)/.test(evt.keyCode))
            return;
          evt.preventDefault();
          evt.stopPropagation();
          // Select with enter
          if (evt.keyCode === 13 && scope.$matches.length) {
            $typeahead.select(scope.$activeIndex);
          }  // Navigate with keyboard
          else if (evt.keyCode === 38 && scope.$activeIndex > 0)
            scope.$activeIndex--;
          else if (evt.keyCode === 40 && scope.$activeIndex < scope.$matches.length - 1)
            scope.$activeIndex++;
          else if (angular.isUndefined(scope.$activeIndex))
            scope.$activeIndex = 0;
          scope.$digest();
        };
        // Overrides
        var show = $typeahead.show;
        $typeahead.show = function () {
          show();
          setTimeout(function () {
            $typeahead.$element.on('mousedown', $typeahead.$onMouseDown);
            if (options.keyboard) {
              element.on('keydown', $typeahead.$onKeyDown);
            }
          });
        };
        var hide = $typeahead.hide;
        $typeahead.hide = function () {
          $typeahead.$element.off('mousedown', $typeahead.$onMouseDown);
          if (options.keyboard) {
            element.off('keydown', $typeahead.$onKeyDown);
          }
          hide();
        };
        return $typeahead;
      }
      TypeaheadFactory.defaults = defaults;
      return TypeaheadFactory;
    }
  ];
}).directive('bsTypeahead', [
  '$window',
  '$parse',
  '$q',
  '$typeahead',
  '$parseOptions',
  function ($window, $parse, $q, $typeahead, $parseOptions) {
    var defaults = $typeahead.defaults;
    return {
      restrict: 'EAC',
      require: 'ngModel',
      link: function postLink(scope, element, attr, controller) {
        // Directive options
        var options = { scope: scope };
        angular.forEach([
          'placement',
          'container',
          'delay',
          'trigger',
          'keyboard',
          'html',
          'animation',
          'template',
          'filter',
          'limit',
          'minLength'
        ], function (key) {
          if (angular.isDefined(attr[key]))
            options[key] = attr[key];
        });
        // Build proper ngOptions
        var filter = options.filter || defaults.filter;
        var limit = options.limit || defaults.limit;
        var ngOptions = attr.ngOptions;
        if (filter)
          ngOptions += ' | ' + filter + ':$viewValue';
        if (limit)
          ngOptions += ' | limitTo:' + limit;
        var parsedOptions = $parseOptions(ngOptions);
        // Initialize typeahead
        var typeahead = $typeahead(element, controller, options);
        // Watch model for changes
        scope.$watch(attr.ngModel, function (newValue, oldValue) {
          // console.warn('$watch', element.attr('ng-model'), newValue);
          scope.$modelValue = newValue;
          // Publish modelValue on scope for custom templates
          parsedOptions.valuesFn(scope, controller).then(function (values) {
            if (values.length > limit)
              values = values.slice(0, limit);
            // Do not re-queue an update if a correct value has been selected
            if (values.length === 1 && values[0].value === newValue)
              return;
            typeahead.update(values);
            // Queue a new rendering that will leverage collection loading
            controller.$render();
          });
        });
        // Model rendering in view
        controller.$render = function () {
          // console.warn('$render', element.attr('ng-model'), 'controller.$modelValue', typeof controller.$modelValue, controller.$modelValue, 'controller.$viewValue', typeof controller.$viewValue, controller.$viewValue);
          if (controller.$isEmpty(controller.$viewValue))
            return element.val('');
          var index = typeahead.$getIndex(controller.$modelValue);
          var selected = angular.isDefined(index) ? typeahead.$scope.$matches[index].label : controller.$viewValue;
          selected = angular.isObject(selected) ? selected.label : selected;
          element.val(selected.replace(/<(?:.|\n)*?>/gm, '').trim());
        };
        // Garbage collection
        scope.$on('$destroy', function () {
          typeahead.destroy();
          options = null;
          typeahead = null;
        });
      }
    };
  }
]);

})(window, document);

/**
 * angular-strap
 * @version v2.0.2 - 2014-04-27
 * @link http://mgcrea.github.io/angular-strap
 * @author Olivier Louvignes (olivier@mg-crea.com)
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */
(function(window, document, undefined) {
'use strict';

// Source: alert.tpl.js
angular.module('mgcrea.ngStrap.alert').run([
  '$templateCache',
  function ($templateCache) {
    $templateCache.put('alert/alert.tpl.html', '<div class="alert alert-dismissable" tabindex="-1" ng-class="[type ? \'alert-\' + type : null]"><button type="button" class="close" ng-click="$hide()">&times;</button> <strong ng-bind="title"></strong>&nbsp;<span ng-bind-html="content"></span></div>');
  }
]);

// Source: aside.tpl.js
angular.module('mgcrea.ngStrap.aside').run([
  '$templateCache',
  function ($templateCache) {
    $templateCache.put('aside/aside.tpl.html', '<div class="aside" tabindex="-1" role="dialog"><div class="aside-dialog"><div class="aside-content"><div class="aside-header" ng-show="title"><button type="button" class="close" ng-click="$hide()">&times;</button><h4 class="aside-title" ng-bind="title"></h4></div><div class="aside-body" ng-bind="content"></div><div class="aside-footer"><button type="button" class="btn btn-default" ng-click="$hide()">Close</button></div></div></div></div>');
  }
]);

// Source: datepicker.tpl.js
angular.module('mgcrea.ngStrap.datepicker').run([
  '$templateCache',
  function ($templateCache) {
    $templateCache.put('datepicker/datepicker.tpl.html', '<div class="dropdown-menu datepicker" ng-class="\'datepicker-mode-\' + $mode" style="max-width: 320px"><table style="table-layout: fixed; height: 100%; width: 100%"><thead><tr class="text-center"><th><button tabindex="-1" type="button" class="btn btn-default pull-left" ng-click="$selectPane(-1)"><i class="glyphicon glyphicon-chevron-left"></i></button></th><th colspan="{{ rows[0].length - 2 }}"><button tabindex="-1" type="button" class="btn btn-default btn-block text-strong" ng-click="$toggleMode()"><strong style="text-transform: capitalize" ng-bind="title"></strong></button></th><th><button tabindex="-1" type="button" class="btn btn-default pull-right" ng-click="$selectPane(+1)"><i class="glyphicon glyphicon-chevron-right"></i></button></th></tr><tr ng-show="showLabels" ng-bind-html="labels"></tr></thead><tbody><tr ng-repeat="(i, row) in rows" height="{{ 100 / rows.length }}%"><td class="text-center" ng-repeat="(j, el) in row"><button tabindex="-1" type="button" class="btn btn-default" style="width: 100%" ng-class="{\'btn-primary\': el.selected}" ng-click="$select(el.date)" ng-disabled="el.disabled"><span ng-class="{\'text-muted\': el.muted}" ng-bind="el.label"></span></button></td></tr></tbody></table></div>');
  }
]);

// Source: dropdown.tpl.js
angular.module('mgcrea.ngStrap.dropdown').run([
  '$templateCache',
  function ($templateCache) {
    $templateCache.put('dropdown/dropdown.tpl.html', '<ul tabindex="-1" class="dropdown-menu" role="menu"><li role="presentation" ng-class="{divider: item.divider}" ng-repeat="item in content"><a role="menuitem" tabindex="-1" ng-href="{{item.href}}" ng-if="!item.divider && item.href" ng-bind="item.text"></a> <a role="menuitem" tabindex="-1" href="javascript:void(0)" ng-if="!item.divider && item.click" ng-click="$eval(item.click);$hide()" ng-bind="item.text"></a></li></ul>');
  }
]);

// Source: modal.tpl.js
angular.module('mgcrea.ngStrap.modal').run([
  '$templateCache',
  function ($templateCache) {
    $templateCache.put('modal/modal.tpl.html', '<div class="modal" tabindex="-1" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-header" ng-show="title"><button type="button" class="close" ng-click="$hide()">&times;</button><h4 class="modal-title" ng-bind="title"></h4></div><div class="modal-body" ng-bind="content"></div><div class="modal-footer"><button type="button" class="btn btn-default" ng-click="$hide()">Close</button></div></div></div></div>');
  }
]);

// Source: popover.tpl.js
angular.module('mgcrea.ngStrap.popover').run([
  '$templateCache',
  function ($templateCache) {
    $templateCache.put('popover/popover.tpl.html', '<div class="popover"><div class="arrow"></div><h3 class="popover-title" ng-bind="title" ng-show="title"></h3><div class="popover-content" ng-bind="content"></div></div>');
  }
]);

// Source: select.tpl.js
angular.module('mgcrea.ngStrap.select').run([
  '$templateCache',
  function ($templateCache) {
    $templateCache.put('select/select.tpl.html', '<ul tabindex="-1" class="select dropdown-menu" ng-show="$isVisible()" role="select"><li role="presentation" ng-repeat="match in $matches" ng-class="{active: $isActive($index)}"><a style="cursor: default" role="menuitem" tabindex="-1" ng-click="$select($index, $event)"><span ng-bind="match.label"></span> <i class="glyphicon glyphicon-ok pull-right" ng-if="$isMultiple && $isActive($index)"></i></a></li></ul>');
  }
]);

// Source: tab.tpl.js
angular.module('mgcrea.ngStrap.tab').run([
  '$templateCache',
  function ($templateCache) {
    $templateCache.put('tab/tab.tpl.html', '<ul class="nav nav-tabs"><li ng-repeat="pane in panes" ng-class="{active: $index == active}"><a data-toggle="tab" ng-click="setActive($index, $event)" data-index="{{$index}}" ng-bind-html="pane.title"></a></li></ul><div class="tab-content"><div ng-repeat="pane in panes" class="tab-pane" ng-class="[$index == active ? \'active\' : \'\']" ng-include="pane.template || \'$pane\'"></div></div>');
  }
]);

// Source: timepicker.tpl.js
angular.module('mgcrea.ngStrap.timepicker').run([
  '$templateCache',
  function ($templateCache) {
    $templateCache.put('timepicker/timepicker.tpl.html', '<div class="dropdown-menu timepicker" style="min-width: 0px;width: auto"><table height="100%"><thead><tr class="text-center"><th><button tabindex="-1" type="button" class="btn btn-default pull-left" ng-click="$moveIndex(-1, 0)"><i class="glyphicon glyphicon-chevron-up"></i></button></th><th>&nbsp;</th><th><button tabindex="-1" type="button" class="btn btn-default pull-left" ng-click="$moveIndex(-1, 1)"><i class="glyphicon glyphicon-chevron-up"></i></button></th></tr></thead><tbody><tr ng-repeat="(i, row) in rows"><td class="text-center"><button tabindex="-1" style="width: 100%" type="button" class="btn btn-default" ng-class="{\'btn-primary\': row[0].selected}" ng-click="$select(row[0].date, 0)" ng-disabled="row[0].disabled"><span ng-class="{\'text-muted\': row[0].muted}" ng-bind="row[0].label"></span></button></td><td><span ng-bind="i == midIndex ? \':\' : \' \'"></span></td><td class="text-center"><button tabindex="-1" ng-if="row[1].date" style="width: 100%" type="button" class="btn btn-default" ng-class="{\'btn-primary\': row[1].selected}" ng-click="$select(row[1].date, 1)" ng-disabled="row[1].disabled"><span ng-class="{\'text-muted\': row[1].muted}" ng-bind="row[1].label"></span></button></td><td ng-if="showAM">&nbsp;</td><td ng-if="showAM"><button tabindex="-1" ng-show="i == midIndex - !isAM * 1" style="width: 100%" type="button" ng-class="{\'btn-primary\': !!isAM}" class="btn btn-default" ng-click="$switchMeridian()" ng-disabled="el.disabled">AM</button> <button tabindex="-1" ng-show="i == midIndex + 1 - !isAM * 1" style="width: 100%" type="button" ng-class="{\'btn-primary\': !isAM}" class="btn btn-default" ng-click="$switchMeridian()" ng-disabled="el.disabled">PM</button></td></tr></tbody><tfoot><tr class="text-center"><th><button tabindex="-1" type="button" class="btn btn-default pull-left" ng-click="$moveIndex(1, 0)"><i class="glyphicon glyphicon-chevron-down"></i></button></th><th>&nbsp;</th><th><button tabindex="-1" type="button" class="btn btn-default pull-left" ng-click="$moveIndex(1, 1)"><i class="glyphicon glyphicon-chevron-down"></i></button></th></tr></tfoot></table></div>');
  }
]);

// Source: tooltip.tpl.js
angular.module('mgcrea.ngStrap.tooltip').run([
  '$templateCache',
  function ($templateCache) {
    $templateCache.put('tooltip/tooltip.tpl.html', '<div class="tooltip in" ng-show="title"><div class="tooltip-arrow"></div><div class="tooltip-inner" ng-bind="title"></div></div>');
  }
]);

// Source: typeahead.tpl.js
angular.module('mgcrea.ngStrap.typeahead').run([
  '$templateCache',
  function ($templateCache) {
    $templateCache.put('typeahead/typeahead.tpl.html', '<ul tabindex="-1" class="typeahead dropdown-menu" ng-show="$isVisible()" role="select"><li role="presentation" ng-repeat="match in $matches" ng-class="{active: $index == $activeIndex}"><a role="menuitem" tabindex="-1" ng-click="$select($index, $event)" ng-bind="match.label"></a></li></ul>');
  }
]);


})(window, document);
, document);
=======
!function(t,e,n){"use strict";e.module("ngAnimate",["ng"]).factory("$$animateReflow",["$$rAF","$document",function(t,e){var n=e[0].body;return function(e){return t(function(){n.offsetWidth+1;e()})}}]).config(["$provide","$animateProvider",function(i,r){function o(t){for(var e=0;e<t.length;e++){var n=t[e];if(n.nodeType==f)return n}}function a(t){return e.element(o(t))}function s(t,e){return o(t)==o(e)}var l=e.noop,u=e.forEach,c=r.$$selectors,f=1,p="$$ngAnimateState",h="ng-animate",d={running:!0};i.decorator("$animate",["$delegate","$injector","$sniffer","$rootElement","$$asyncCallback","$rootScope","$document",function(t,n,i,f,m,g){function _(t){if(t){var e=[],r={},o=t.substr(1).split(".");(i.transitions||i.animations)&&e.push(n.get(c[""]));for(var a=0;a<o.length;a++){var s=o[a],l=c[s];l&&!r[s]&&(e.push(n.get(l)),r[s]=!0)}return e}}function v(t,n,i){function r(t,e){var n=t[e],i=t["before"+e.charAt(0).toUpperCase()+e.substr(1)];return n||i?("leave"==e&&(i=n,n=null),w.push({event:e,fn:n}),v.push({event:e,fn:i}),!0):void 0}function o(e,n,r){function o(t){if(n){if((n[t]||l)(),++f<a.length)return;n=null}r()}var a=[];u(e,function(t){t.fn&&a.push(t)});var f=0;u(a,function(e,r){var a=function(){o(r)};switch(e.event){case"setClass":n.push(e.fn(t,s,c,a));break;case"addClass":n.push(e.fn(t,s||i,a));break;case"removeClass":n.push(e.fn(t,c||i,a));break;default:n.push(e.fn(t,a))}}),n&&0===n.length&&r()}var a=t[0];if(a){var s,c,f="setClass"==n,p=f||"addClass"==n||"removeClass"==n;e.isArray(i)&&(s=i[0],c=i[1],i=s+" "+c);var h=t.attr("class"),d=h+" "+i;if(S(d)){var m=l,g=[],v=[],y=l,b=[],w=[],$=(" "+d).replace(/\s+/g,".");return u(_($),function(t){var e=r(t,n);!e&&f&&(r(t,"addClass"),r(t,"removeClass"))}),{node:a,event:n,className:i,isClassBased:p,isSetClassOperation:f,before:function(t){m=t,o(v,g,function(){m=l,t()})},after:function(t){y=t,o(w,b,function(){y=l,t()})},cancel:function(){g&&(u(g,function(t){(t||l)(!0)}),m(!0)),b&&(u(b,function(t){(t||l)(!0)}),y(!0))}}}}}function y(t,n,i,r,o,a,s){function l(e){var r="$animate:"+e;b&&b[r]&&b[r].length>0&&m(function(){i.triggerHandler(r,{event:t,className:n})})}function c(){l("before")}function f(){l("after")}function d(){l("close"),s&&m(function(){s()})}function g(){g.hasBeenRun||(g.hasBeenRun=!0,a())}function _(){if(!_.hasBeenRun){_.hasBeenRun=!0;var e=i.data(p);e&&(y&&y.isClassBased?w(i,n):(m(function(){var e=i.data(p)||{};O==e.index&&w(i,n,t)}),i.data(p,e))),d()}}var y=v(i,t,n);if(!y)return g(),c(),f(),void _();n=y.className;var b=e.element._data(y.node);b=b&&b.events,r||(r=o?o.parent():i.parent());var T=i.data(p)||{},S=T.active||{},C=T.totalActive||0,k=T.last,A=y.isClassBased?T.disabled||k&&!k.isClassBased:!1;if(A||$(i,r))return g(),c(),f(),void _();var D=!1;if(C>0){var E=[];if(y.isClassBased){if("setClass"==k.event)E.push(k),w(i,n);else if(S[n]){var P=S[n];P.event==t?D=!0:(E.push(P),w(i,n))}}else if("leave"==t&&S["ng-leave"])D=!0;else{for(var M in S)E.push(S[M]),w(i,M);S={},C=0}E.length>0&&u(E,function(t){t.cancel()})}if(!y.isClassBased||y.isSetClassOperation||D||(D="addClass"==t==i.hasClass(n)),D)return c(),f(),void d();"leave"==t&&i.one("$destroy",function(){var t=e.element(this),n=t.data(p);if(n){var i=n.active["ng-leave"];i&&(i.cancel(),w(t,"ng-leave"))}}),i.addClass(h);var O=x++;C++,S[n]=y,i.data(p,{last:y,active:S,index:O,totalActive:C}),c(),y.before(function(e){var r=i.data(p);e=e||!r||!r.active[n]||y.isClassBased&&r.active[n].event!=t,g(),e===!0?_():(f(),y.after(_))})}function b(t){var n=o(t);if(n){var i=e.isFunction(n.getElementsByClassName)?n.getElementsByClassName(h):n.querySelectorAll("."+h);u(i,function(t){t=e.element(t);var n=t.data(p);n&&n.active&&u(n.active,function(t){t.cancel()})})}}function w(t,e){if(s(t,f))d.disabled||(d.running=!1,d.structural=!1);else if(e){var n=t.data(p)||{},i=e===!0;!i&&n.active&&n.active[e]&&(n.totalActive--,delete n.active[e]),(i||!n.totalActive)&&(t.removeClass(h),t.removeData(p))}}function $(t,e){if(d.disabled)return!0;if(s(t,f))return d.disabled||d.running;do{if(0===e.length)break;var n=s(e,f),i=n?d:e.data(p),r=i&&(!!i.disabled||i.running||i.totalActive>0);if(n||r)return r;if(n)return!0}while(e=e.parent());return!0}var x=0;f.data(p,d),g.$$postDigest(function(){g.$$postDigest(function(){d.running=!1})});var T=r.classNameFilter(),S=T?function(t){return T.test(t)}:function(){return!0};return{enter:function(e,n,i,r){this.enabled(!1,e),t.enter(e,n,i),g.$$postDigest(function(){e=a(e),y("enter","ng-enter",e,n,i,l,r)})},leave:function(e,n){b(e),this.enabled(!1,e),g.$$postDigest(function(){y("leave","ng-leave",a(e),null,null,function(){t.leave(e)},n)})},move:function(e,n,i,r){b(e),this.enabled(!1,e),t.move(e,n,i),g.$$postDigest(function(){e=a(e),y("move","ng-move",e,n,i,l,r)})},addClass:function(e,n,i){e=a(e),y("addClass",n,e,null,null,function(){t.addClass(e,n)},i)},removeClass:function(e,n,i){e=a(e),y("removeClass",n,e,null,null,function(){t.removeClass(e,n)},i)},setClass:function(e,n,i,r){e=a(e),y("setClass",[n,i],e,null,null,function(){t.setClass(e,n,i)},r)},enabled:function(t,e){switch(arguments.length){case 2:if(t)w(e);else{var n=e.data(p)||{};n.disabled=!0,e.data(p,n)}break;case 1:d.disabled=!t;break;default:t=!d.disabled}return!!t}}}]),r.register("",["$window","$sniffer","$timeout","$$animateReflow",function(i,r,a,s){function c(t,e){N&&N(),q.push(e),N=s(function(){u(q,function(t){t()}),q=[],N=null,z={}})}function p(t,n){var i=o(t);t=e.element(i),Z.push(t);var r=Date.now()+n;G>=r||(a.cancel(K),G=r,K=a(function(){h(Z),Z=[]},n,!1))}function h(t){u(t,function(t){var e=t.data(X);e&&(e.closeAnimationFn||l)()})}function d(t,e){var n=e?z[e]:null;if(!n){var r,o,a,s,l=0,c=0,p=0,h=0;u(t,function(t){if(t.nodeType==f){var e=i.getComputedStyle(t)||{};a=e[E+I],l=Math.max(m(a),l),s=e[E+F],r=e[E+L],c=Math.max(m(r),c),o=e[M+L],h=Math.max(m(o),h);var n=m(e[M+I]);n>0&&(n*=parseInt(e[M+Y],10)||1),p=Math.max(n,p)}}),n={total:0,transitionPropertyStyle:s,transitionDurationStyle:a,transitionDelayStyle:r,transitionDelay:c,transitionDuration:l,animationDelayStyle:o,animationDelay:h,animationDuration:p},e&&(z[e]=n)}return n}function m(t){var n=0,i=e.isString(t)?t.split(/\s*,\s*/):[];return u(i,function(t){n=Math.max(parseFloat(t)||0,n)}),n}function g(t){var e=t.parent(),n=e.data(V);return n||(e.data(V,++W),n=W),n+"-"+o(t).getAttribute("class")}function _(t,e,n,i){var r=g(e),o=r+" "+n,a=z[o]?++z[o].total:0,s={};if(a>0){var u=n+"-stagger",c=r+" "+u,f=!z[c];f&&e.addClass(u),s=d(e,c),f&&e.removeClass(u)}i=i||function(t){return t()},e.addClass(n);var p=e.data(X)||{},h=i(function(){return d(e,o)}),m=h.transitionDuration,_=h.animationDuration;if(0===m&&0===_)return e.removeClass(n),!1;e.data(X,{running:p.running||0,itemIndex:a,stagger:s,timings:h,closeAnimationFn:l});var v=p.running>0||"setClass"==t;return m>0&&y(e,n,v),_>0&&s.animationDelay>0&&0===s.animationDuration&&b(e),!0}function v(t){return"ng-enter"==t||"ng-move"==t||"ng-leave"==t}function y(t,e,n){v(e)||!n?o(t).style[E+F]="none":t.addClass(j)}function b(t){o(t).style[M]="none 0s"}function w(t){var e=E+F,n=o(t);n.style[e]&&n.style[e].length>0&&(n.style[e]=""),t.removeClass(j)}function $(t){var e=M,n=o(t);n.style[e]&&n.style[e].length>0&&(n.style[e]="")}function x(t,e,n,i){function r(){e.off(y,a),e.removeClass(c),A(e,n);var t=o(e);for(var i in w)t.style.removeProperty(w[i])}function a(t){t.stopPropagation();var e=t.originalEvent||t,n=e.$manualTimeStamp||e.timeStamp||Date.now(),r=parseFloat(e.elapsedTime.toFixed(B));Math.max(n-v,0)>=_&&r>=m&&i()}var s=o(e),l=e.data(X);if(-1==s.getAttribute("class").indexOf(n)||!l)return void i();var c="";u(n.split(" "),function(t,e){c+=(e>0?" ":"")+t+"-active"});var f=l.stagger,h=l.timings,d=l.itemIndex,m=Math.max(h.transitionDuration,h.animationDuration),g=Math.max(h.transitionDelay,h.animationDelay),_=g*U,v=Date.now(),y=O+" "+P,b="",w=[];if(h.transitionDuration>0){var $=h.transitionPropertyStyle;-1==$.indexOf("all")&&(b+=R+"transition-property: "+$+";",b+=R+"transition-duration: "+h.transitionDurationStyle+";",w.push(R+"transition-property"),w.push(R+"transition-duration"))}if(d>0){if(f.transitionDelay>0&&0===f.transitionDuration){var x=h.transitionDelayStyle;b+=R+"transition-delay: "+T(x,f.transitionDelay,d)+"; ",w.push(R+"transition-delay")}f.animationDelay>0&&0===f.animationDuration&&(b+=R+"animation-delay: "+T(h.animationDelayStyle,f.animationDelay,d)+"; ",w.push(R+"animation-delay"))}if(w.length>0){var S=s.getAttribute("style")||"";s.setAttribute("style",S+" "+b)}e.on(y,a),e.addClass(c),l.closeAnimationFn=function(){r(),i()};var C=d*(Math.max(f.animationDelay,f.transitionDelay)||0),k=(g+m)*H,D=(C+k)*U;return l.running++,p(e,D),r}function T(t,e,n){var i="";return u(t.split(","),function(t,r){i+=(r>0?",":"")+(n*e+parseInt(t,10))+"s"}),i}function S(t,e,n,i){return _(t,e,n,i)?function(t){t&&A(e,n)}:void 0}function C(t,e,n,i){return e.data(X)?x(t,e,n,i):(A(e,n),void i())}function k(t,e,n,i){var r=S(t,e,n);if(!r)return void i();var o=r;return c(e,function(){w(e,n),$(e),o=C(t,e,n,i)}),function(t){(o||l)(t)}}function A(t,e){t.removeClass(e);var n=t.data(X);n&&(n.running&&n.running--,n.running&&0!==n.running||t.removeData(X))}function D(t,n){var i="";return t=e.isArray(t)?t:t.split(/\s+/),u(t,function(t,e){t&&t.length>0&&(i+=(e>0?" ":"")+t+n)}),i}var E,P,M,O,R="";t.ontransitionend===n&&t.onwebkittransitionend!==n?(R="-webkit-",E="WebkitTransition",P="webkitTransitionEnd transitionend"):(E="transition",P="transitionend"),t.onanimationend===n&&t.onwebkitanimationend!==n?(R="-webkit-",M="WebkitAnimation",O="webkitAnimationEnd animationend"):(M="animation",O="animationend");var N,I="Duration",F="Property",L="Delay",Y="IterationCount",V="$$ngAnimateKey",X="$$ngAnimateCSS3Data",j="ng-animate-block-transitions",B=3,H=1.5,U=1e3,z={},W=0,q=[],K=null,G=0,Z=[];return{enter:function(t,e){return k("enter",t,"ng-enter",e)},leave:function(t,e){return k("leave",t,"ng-leave",e)},move:function(t,e){return k("move",t,"ng-move",e)},beforeSetClass:function(t,e,n,i){var r=D(n,"-remove")+" "+D(e,"-add"),o=S("setClass",t,r,function(i){var r=t.attr("class");t.removeClass(n),t.addClass(e);var o=i();return t.attr("class",r),o});return o?(c(t,function(){w(t,r),$(t),i()}),o):void i()},beforeAddClass:function(t,e,n){var i=S("addClass",t,D(e,"-add"),function(n){t.addClass(e);var i=n();return t.removeClass(e),i});return i?(c(t,function(){w(t,e),$(t),n()}),i):void n()},setClass:function(t,e,n,i){n=D(n,"-remove"),e=D(e,"-add");var r=n+" "+e;return C("setClass",t,r,i)},addClass:function(t,e,n){return C("addClass",t,D(e,"-add"),n)},beforeRemoveClass:function(t,e,n){var i=S("removeClass",t,D(e,"-remove"),function(n){var i=t.attr("class");t.removeClass(e);var r=n();return t.attr("class",i),r});return i?(c(t,function(){w(t,e),$(t),n()}),i):void n()},removeClass:function(t,e,n){return C("removeClass",t,D(e,"-remove"),n)}}}])}])}(window,window.angular),function(){function t(t,e,n){for(var i=(n||0)-1,r=t?t.length:0;++i<r;)if(t[i]===e)return i;return-1}function e(e,n){var i=typeof n;if(e=e.cache,"boolean"==i||null==n)return e[n]?0:-1;"number"!=i&&"string"!=i&&(i="object");var r="number"==i?n:_+n;return e=(e=e[i])&&e[r],"object"==i?e&&t(e,n)>-1?0:-1:e?0:-1}function n(t){var e=this.cache,n=typeof t;if("boolean"==n||null==t)e[t]=!0;else{"number"!=n&&"string"!=n&&(n="object");var i="number"==n?t:_+t,r=e[n]||(e[n]={});"object"==n?(r[i]||(r[i]=[])).push(t):r[i]=!0}}function i(t){return t.charCodeAt(0)}function r(t,e){for(var n=t.criteria,i=e.criteria,r=-1,o=n.length;++r<o;){var a=n[r],s=i[r];if(a!==s){if(a>s||"undefined"==typeof a)return 1;if(s>a||"undefined"==typeof s)return-1}}return t.index-e.index}function o(t){var e=-1,i=t.length,r=t[0],o=t[i/2|0],a=t[i-1];if(r&&"object"==typeof r&&o&&"object"==typeof o&&a&&"object"==typeof a)return!1;var s=l();s["false"]=s["null"]=s["true"]=s.undefined=!1;var u=l();for(u.array=t,u.cache=s,u.push=n;++e<i;)u.push(t[e]);return u}function a(t){return"\\"+W[t]}function s(){return d.pop()||[]}function l(){return m.pop()||{array:null,cache:null,criteria:null,"false":!1,index:0,"null":!1,number:null,object:null,push:null,string:null,"true":!1,undefined:!1,value:null}}function u(t){t.length=0,d.length<y&&d.push(t)}function c(t){var e=t.cache;e&&c(e),t.array=t.cache=t.criteria=t.object=t.number=t.string=t.value=null,m.length<y&&m.push(t)}function f(t,e,n){e||(e=0),"undefined"==typeof n&&(n=t?t.length:0);for(var i=-1,r=n-e||0,o=Array(0>r?0:r);++i<r;)o[i]=t[e+i];return o}function p(n){function d(t){return t&&"object"==typeof t&&!Ji(t)&&Ri.call(t,"__wrapped__")?t:new m(t)}function m(t,e){this.__chain__=!!e,this.__wrapped__=t}function y(t){function e(){if(i){var t=f(i);Ni.apply(t,arguments)}if(this instanceof e){var o=K(n.prototype),a=n.apply(o,t||arguments);return Ee(a)?a:o}return n.apply(r,t||arguments)}var n=t[0],i=t[2],r=t[4];return Zi(e,t),e}function W(t,e,n,i,r){if(n){var o=n(t);if("undefined"!=typeof o)return o}var a=Ee(t);if(!a)return t;var l=ki.call(t);if(!B[l])return t;var c=Ki[l];switch(l){case I:case F:return new c(+t);case Y:case j:return new c(t);case X:return o=c(t.source,S.exec(t)),o.lastIndex=t.lastIndex,o}var p=Ji(t);if(e){var h=!i;i||(i=s()),r||(r=s());for(var d=i.length;d--;)if(i[d]==t)return r[d];o=p?c(t.length):{}}else o=p?f(t):or({},t);return p&&(Ri.call(t,"index")&&(o.index=t.index),Ri.call(t,"input")&&(o.input=t.input)),e?(i.push(t),r.push(o),(p?Ge:lr)(t,function(t,a){o[a]=W(t,e,n,i,r)}),h&&(u(i),u(r)),o):o}function K(t){return Ee(t)?Vi(t):{}}function G(t,e,n){if("function"!=typeof t)return Jn;if("undefined"==typeof e||!("prototype"in t))return t;var i=t.__bindData__;if("undefined"==typeof i&&(Gi.funcNames&&(i=!t.name),i=i||!Gi.funcDecomp,!i)){var r=Mi.call(t);Gi.funcNames||(i=!C.test(r)),i||(i=E.test(r),Zi(t,i))}if(i===!1||i!==!0&&1&i[1])return t;switch(n){case 1:return function(n){return t.call(e,n)};case 2:return function(n,i){return t.call(e,n,i)};case 3:return function(n,i,r){return t.call(e,n,i,r)};case 4:return function(n,i,r,o){return t.call(e,n,i,r,o)}}return Nn(t,e)}function Z(t){function e(){var t=l?a:this;if(r){var d=f(r);Ni.apply(d,arguments)}if((o||c)&&(d||(d=f(arguments)),o&&Ni.apply(d,o),c&&d.length<s))return i|=16,Z([n,p?i:-4&i,d,null,a,s]);if(d||(d=arguments),u&&(n=t[h]),this instanceof e){t=K(n.prototype);var m=n.apply(t,d);return Ee(m)?m:t}return n.apply(t,d)}var n=t[0],i=t[1],r=t[2],o=t[3],a=t[4],s=t[5],l=1&i,u=2&i,c=4&i,p=8&i,h=n;return Zi(e,t),e}function J(n,i){var r=-1,a=le(),s=n?n.length:0,l=s>=v&&a===t,u=[];if(l){var f=o(i);f?(a=e,i=f):l=!1}for(;++r<s;){var p=n[r];a(i,p)<0&&u.push(p)}return l&&c(i),u}function te(t,e,n,i){for(var r=(i||0)-1,o=t?t.length:0,a=[];++r<o;){var s=t[r];if(s&&"object"==typeof s&&"number"==typeof s.length&&(Ji(s)||pe(s))){e||(s=te(s,e,n));var l=-1,u=s.length,c=a.length;for(a.length+=u;++l<u;)a[c++]=s[l]}else n||a.push(s)}return a}function ee(t,e,n,i,r,o){if(n){var a=n(t,e);if("undefined"!=typeof a)return!!a}if(t===e)return 0!==t||1/t==1/e;var l=typeof t,c=typeof e;if(!(t!==t||t&&z[l]||e&&z[c]))return!1;if(null==t||null==e)return t===e;var f=ki.call(t),p=ki.call(e);if(f==R&&(f=V),p==R&&(p=V),f!=p)return!1;switch(f){case I:case F:return+t==+e;case Y:return t!=+t?e!=+e:0==t?1/t==1/e:t==+e;case X:case j:return t==$i(e)}var h=f==N;if(!h){var d=Ri.call(t,"__wrapped__"),m=Ri.call(e,"__wrapped__");if(d||m)return ee(d?t.__wrapped__:t,m?e.__wrapped__:e,n,i,r,o);if(f!=V)return!1;var g=t.constructor,_=e.constructor;if(g!=_&&!(De(g)&&g instanceof g&&De(_)&&_ instanceof _)&&"constructor"in t&&"constructor"in e)return!1}var v=!r;r||(r=s()),o||(o=s());for(var y=r.length;y--;)if(r[y]==t)return o[y]==e;var b=0;if(a=!0,r.push(t),o.push(e),h){if(y=t.length,b=e.length,a=b==y,a||i)for(;b--;){var w=y,$=e[b];if(i)for(;w--&&!(a=ee(t[w],$,n,i,r,o)););else if(!(a=ee(t[b],$,n,i,r,o)))break}}else sr(e,function(e,s,l){return Ri.call(l,s)?(b++,a=Ri.call(t,s)&&ee(t[s],e,n,i,r,o)):void 0}),a&&!i&&sr(t,function(t,e,n){return Ri.call(n,e)?a=--b>-1:void 0});return r.pop(),o.pop(),v&&(u(r),u(o)),a}function ne(t,e,n,i,r){(Ji(e)?Ge:lr)(e,function(e,o){var a,s,l=e,u=t[o];if(e&&((s=Ji(e))||ur(e))){for(var c=i.length;c--;)if(a=i[c]==e){u=r[c];break}if(!a){var f;n&&(l=n(u,e),(f="undefined"!=typeof l)&&(u=l)),f||(u=s?Ji(u)?u:[]:ur(u)?u:{}),i.push(e),r.push(u),f||ne(u,e,n,i,r)}}else n&&(l=n(u,e),"undefined"==typeof l&&(l=e)),"undefined"!=typeof l&&(u=l);t[o]=u})}function ie(t,e){return t+Pi(qi()*(e-t+1))}function re(n,i,r){var a=-1,l=le(),f=n?n.length:0,p=[],h=!i&&f>=v&&l===t,d=r||h?s():p;if(h){var m=o(d);l=e,d=m}for(;++a<f;){var g=n[a],_=r?r(g,a,n):g;(i?!a||d[d.length-1]!==_:l(d,_)<0)&&((r||h)&&d.push(_),p.push(g))}return h?(u(d.array),c(d)):r&&u(d),p}function oe(t){return function(e,n,i){var r={};n=d.createCallback(n,i,3);var o=-1,a=e?e.length:0;if("number"==typeof a)for(;++o<a;){var s=e[o];t(r,s,n(s,o,e),e)}else lr(e,function(e,i,o){t(r,e,n(e,i,o),o)});return r}}function ae(t,e,n,i,r,o){var a=1&e,s=2&e,l=4&e,u=16&e,c=32&e;if(!s&&!De(t))throw new xi;u&&!n.length&&(e&=-17,u=n=!1),c&&!i.length&&(e&=-33,c=i=!1);var p=t&&t.__bindData__;if(p&&p!==!0)return p=f(p),p[2]&&(p[2]=f(p[2])),p[3]&&(p[3]=f(p[3])),!a||1&p[1]||(p[4]=r),!a&&1&p[1]&&(e|=8),!l||4&p[1]||(p[5]=o),u&&Ni.apply(p[2]||(p[2]=[]),n),c&&Li.apply(p[3]||(p[3]=[]),i),p[1]|=e,ae.apply(null,p);var h=1==e||17===e?y:Z;return h([t,e,n,i,r,o])}function se(t){return er[t]}function le(){var e=(e=d.indexOf)===vn?t:e;return e}function ue(t){return"function"==typeof t&&Ai.test(t)}function ce(t){var e,n;return t&&ki.call(t)==V&&(e=t.constructor,!De(e)||e instanceof e)?(sr(t,function(t,e){n=e}),"undefined"==typeof n||Ri.call(t,n)):!1}function fe(t){return nr[t]}function pe(t){return t&&"object"==typeof t&&"number"==typeof t.length&&ki.call(t)==R||!1}function he(t,e,n,i){return"boolean"!=typeof e&&null!=e&&(i=n,n=e,e=!1),W(t,e,"function"==typeof n&&G(n,i,1))}function de(t,e,n){return W(t,!0,"function"==typeof e&&G(e,n,1))}function me(t,e){var n=K(t);return e?or(n,e):n}function ge(t,e,n){var i;return e=d.createCallback(e,n,3),lr(t,function(t,n,r){return e(t,n,r)?(i=n,!1):void 0}),i}function _e(t,e,n){var i;return e=d.createCallback(e,n,3),ye(t,function(t,n,r){return e(t,n,r)?(i=n,!1):void 0}),i}function ve(t,e,n){var i=[];sr(t,function(t,e){i.push(e,t)});var r=i.length;for(e=G(e,n,3);r--&&e(i[r--],i[r],t)!==!1;);return t}function ye(t,e,n){var i=tr(t),r=i.length;for(e=G(e,n,3);r--;){var o=i[r];if(e(t[o],o,t)===!1)break}return t}function be(t){var e=[];return sr(t,function(t,n){De(t)&&e.push(n)}),e.sort()}function we(t,e){return t?Ri.call(t,e):!1}function $e(t){for(var e=-1,n=tr(t),i=n.length,r={};++e<i;){var o=n[e];r[t[o]]=o}return r}function xe(t){return t===!0||t===!1||t&&"object"==typeof t&&ki.call(t)==I||!1}function Te(t){return t&&"object"==typeof t&&ki.call(t)==F||!1}function Se(t){return t&&1===t.nodeType||!1}function Ce(t){var e=!0;if(!t)return e;var n=ki.call(t),i=t.length;return n==N||n==j||n==R||n==V&&"number"==typeof i&&De(t.splice)?!i:(lr(t,function(){return e=!1}),e)}function ke(t,e,n,i){return ee(t,e,"function"==typeof n&&G(n,i,2))}function Ae(t){return ji(t)&&!Bi(parseFloat(t))}function De(t){return"function"==typeof t}function Ee(t){return!(!t||!z[typeof t])}function Pe(t){return Oe(t)&&t!=+t}function Me(t){return null===t}function Oe(t){return"number"==typeof t||t&&"object"==typeof t&&ki.call(t)==Y||!1}function Re(t){return t&&"object"==typeof t&&ki.call(t)==X||!1}function Ne(t){return"string"==typeof t||t&&"object"==typeof t&&ki.call(t)==j||!1}function Ie(t){return"undefined"==typeof t}function Fe(t,e,n){var i={};return e=d.createCallback(e,n,3),lr(t,function(t,n,r){i[n]=e(t,n,r)}),i}function Le(t){var e=arguments,n=2;if(!Ee(t))return t;if("number"!=typeof e[2]&&(n=e.length),n>3&&"function"==typeof e[n-2])var i=G(e[--n-1],e[n--],2);else n>2&&"function"==typeof e[n-1]&&(i=e[--n]);for(var r=f(arguments,1,n),o=-1,a=s(),l=s();++o<n;)ne(t,r[o],i,a,l);return u(a),u(l),t}function Ye(t,e,n){var i={};if("function"!=typeof e){var r=[];sr(t,function(t,e){r.push(e)}),r=J(r,te(arguments,!0,!1,1));for(var o=-1,a=r.length;++o<a;){var s=r[o];i[s]=t[s]}}else e=d.createCallback(e,n,3),sr(t,function(t,n,r){e(t,n,r)||(i[n]=t)});return i}function Ve(t){for(var e=-1,n=tr(t),i=n.length,r=di(i);++e<i;){var o=n[e];r[e]=[o,t[o]]}return r}function Xe(t,e,n){var i={};if("function"!=typeof e)for(var r=-1,o=te(arguments,!0,!1,1),a=Ee(t)?o.length:0;++r<a;){var s=o[r];s in t&&(i[s]=t[s])}else e=d.createCallback(e,n,3),sr(t,function(t,n,r){e(t,n,r)&&(i[n]=t)});return i}function je(t,e,n,i){var r=Ji(t);if(null==n)if(r)n=[];else{var o=t&&t.constructor,a=o&&o.prototype;n=K(a)}return e&&(e=d.createCallback(e,i,4),(r?Ge:lr)(t,function(t,i,r){return e(n,t,i,r)})),n}function Be(t){for(var e=-1,n=tr(t),i=n.length,r=di(i);++e<i;)r[e]=t[n[e]];return r}function He(t){for(var e=arguments,n=-1,i=te(e,!0,!1,1),r=e[2]&&e[2][e[1]]===t?1:i.length,o=di(r);++n<r;)o[n]=t[i[n]];return o}function Ue(t,e,n){var i=-1,r=le(),o=t?t.length:0,a=!1;return n=(0>n?Ui(0,o+n):n)||0,Ji(t)?a=r(t,e,n)>-1:"number"==typeof o?a=(Ne(t)?t.indexOf(e,n):r(t,e,n))>-1:lr(t,function(t){return++i>=n?!(a=t===e):void 0}),a}function ze(t,e,n){var i=!0;e=d.createCallback(e,n,3);var r=-1,o=t?t.length:0;if("number"==typeof o)for(;++r<o&&(i=!!e(t[r],r,t)););else lr(t,function(t,n,r){return i=!!e(t,n,r)});return i}function We(t,e,n){var i=[];e=d.createCallback(e,n,3);var r=-1,o=t?t.length:0;if("number"==typeof o)for(;++r<o;){var a=t[r];e(a,r,t)&&i.push(a)}else lr(t,function(t,n,r){e(t,n,r)&&i.push(t)});return i}function qe(t,e,n){e=d.createCallback(e,n,3);var i=-1,r=t?t.length:0;if("number"!=typeof r){var o;return lr(t,function(t,n,i){return e(t,n,i)?(o=t,!1):void 0}),o}for(;++i<r;){var a=t[i];if(e(a,i,t))return a}}function Ke(t,e,n){var i;return e=d.createCallback(e,n,3),Ze(t,function(t,n,r){return e(t,n,r)?(i=t,!1):void 0}),i}function Ge(t,e,n){var i=-1,r=t?t.length:0;if(e=e&&"undefined"==typeof n?e:G(e,n,3),"number"==typeof r)for(;++i<r&&e(t[i],i,t)!==!1;);else lr(t,e);return t}function Ze(t,e,n){var i=t?t.length:0;if(e=e&&"undefined"==typeof n?e:G(e,n,3),"number"==typeof i)for(;i--&&e(t[i],i,t)!==!1;);else{var r=tr(t);i=r.length,lr(t,function(t,n,o){return n=r?r[--i]:--i,e(o[n],n,o)})}return t}function Je(t,e){var n=f(arguments,2),i=-1,r="function"==typeof e,o=t?t.length:0,a=di("number"==typeof o?o:0);return Ge(t,function(t){a[++i]=(r?e:t[e]).apply(t,n)}),a}function Qe(t,e,n){var i=-1,r=t?t.length:0;if(e=d.createCallback(e,n,3),"number"==typeof r)for(var o=di(r);++i<r;)o[i]=e(t[i],i,t);else o=[],lr(t,function(t,n,r){o[++i]=e(t,n,r)});return o}function tn(t,e,n){var r=-1/0,o=r;if("function"!=typeof e&&n&&n[e]===t&&(e=null),null==e&&Ji(t))for(var a=-1,s=t.length;++a<s;){var l=t[a];l>o&&(o=l)}else e=null==e&&Ne(t)?i:d.createCallback(e,n,3),Ge(t,function(t,n,i){var a=e(t,n,i);a>r&&(r=a,o=t)});return o}function en(t,e,n){var r=1/0,o=r;if("function"!=typeof e&&n&&n[e]===t&&(e=null),null==e&&Ji(t))for(var a=-1,s=t.length;++a<s;){var l=t[a];o>l&&(o=l)}else e=null==e&&Ne(t)?i:d.createCallback(e,n,3),Ge(t,function(t,n,i){var a=e(t,n,i);r>a&&(r=a,o=t)});return o}function nn(t,e,n,i){if(!t)return n;var r=arguments.length<3;e=d.createCallback(e,i,4);var o=-1,a=t.length;if("number"==typeof a)for(r&&(n=t[++o]);++o<a;)n=e(n,t[o],o,t);else lr(t,function(t,i,o){n=r?(r=!1,t):e(n,t,i,o)});return n}function rn(t,e,n,i){var r=arguments.length<3;return e=d.createCallback(e,i,4),Ze(t,function(t,i,o){n=r?(r=!1,t):e(n,t,i,o)}),n}function on(t,e,n){return e=d.createCallback(e,n,3),We(t,function(t,n,i){return!e(t,n,i)})}function an(t,e,n){if(t&&"number"!=typeof t.length&&(t=Be(t)),null==e||n)return t?t[ie(0,t.length-1)]:h;var i=sn(t);return i.length=zi(Ui(0,e),i.length),i}function sn(t){var e=-1,n=t?t.length:0,i=di("number"==typeof n?n:0);return Ge(t,function(t){var n=ie(0,++e);i[e]=i[n],i[n]=t}),i}function ln(t){var e=t?t.length:0;return"number"==typeof e?e:tr(t).length}function un(t,e,n){var i;e=d.createCallback(e,n,3);var r=-1,o=t?t.length:0;if("number"==typeof o)for(;++r<o&&!(i=e(t[r],r,t)););else lr(t,function(t,n,r){return!(i=e(t,n,r))});return!!i}function cn(t,e,n){var i=-1,o=Ji(e),a=t?t.length:0,f=di("number"==typeof a?a:0);for(o||(e=d.createCallback(e,n,3)),Ge(t,function(t,n,r){var a=f[++i]=l();o?a.criteria=Qe(e,function(e){return t[e]}):(a.criteria=s())[0]=e(t,n,r),a.index=i,a.value=t}),a=f.length,f.sort(r);a--;){var p=f[a];f[a]=p.value,o||u(p.criteria),c(p)}return f}function fn(t){return t&&"number"==typeof t.length?f(t):Be(t)}function pn(t){for(var e=-1,n=t?t.length:0,i=[];++e<n;){var r=t[e];r&&i.push(r)}return i}function hn(t){return J(t,te(arguments,!0,!0,1))}function dn(t,e,n){var i=-1,r=t?t.length:0;for(e=d.createCallback(e,n,3);++i<r;)if(e(t[i],i,t))return i;return-1}function mn(t,e,n){var i=t?t.length:0;for(e=d.createCallback(e,n,3);i--;)if(e(t[i],i,t))return i;return-1}function gn(t,e,n){var i=0,r=t?t.length:0;if("number"!=typeof e&&null!=e){var o=-1;for(e=d.createCallback(e,n,3);++o<r&&e(t[o],o,t);)i++}else if(i=e,null==i||n)return t?t[0]:h;return f(t,0,zi(Ui(0,i),r))}function _n(t,e,n,i){return"boolean"!=typeof e&&null!=e&&(i=n,n="function"!=typeof e&&i&&i[e]===t?null:e,e=!1),null!=n&&(t=Qe(t,n,i)),te(t,e)}function vn(e,n,i){if("number"==typeof i){var r=e?e.length:0;i=0>i?Ui(0,r+i):i||0}else if(i){var o=kn(e,n);return e[o]===n?o:-1}return t(e,n,i)}function yn(t,e,n){var i=0,r=t?t.length:0;if("number"!=typeof e&&null!=e){var o=r;for(e=d.createCallback(e,n,3);o--&&e(t[o],o,t);)i++}else i=null==e||n?1:e||i;return f(t,0,zi(Ui(0,r-i),r))}function bn(){for(var n=[],i=-1,r=arguments.length,a=s(),l=le(),f=l===t,p=s();++i<r;){var h=arguments[i];(Ji(h)||pe(h))&&(n.push(h),a.push(f&&h.length>=v&&o(i?n[i]:p)))}var d=n[0],m=-1,g=d?d.length:0,_=[];t:for(;++m<g;){var y=a[0];if(h=d[m],(y?e(y,h):l(p,h))<0){for(i=r,(y||p).push(h);--i;)if(y=a[i],(y?e(y,h):l(n[i],h))<0)continue t;_.push(h)}}for(;r--;)y=a[r],y&&c(y);return u(a),u(p),_}function wn(t,e,n){var i=0,r=t?t.length:0;if("number"!=typeof e&&null!=e){var o=r;for(e=d.createCallback(e,n,3);o--&&e(t[o],o,t);)i++}else if(i=e,null==i||n)return t?t[r-1]:h;return f(t,Ui(0,r-i))}function $n(t,e,n){var i=t?t.length:0;for("number"==typeof n&&(i=(0>n?Ui(0,i+n):zi(n,i-1))+1);i--;)if(t[i]===e)return i;return-1}function xn(t){for(var e=arguments,n=0,i=e.length,r=t?t.length:0;++n<i;)for(var o=-1,a=e[n];++o<r;)t[o]===a&&(Fi.call(t,o--,1),r--);return t}function Tn(t,e,n){t=+t||0,n="number"==typeof n?n:+n||1,null==e&&(e=t,t=0);for(var i=-1,r=Ui(0,Di((e-t)/(n||1))),o=di(r);++i<r;)o[i]=t,t+=n;return o}function Sn(t,e,n){var i=-1,r=t?t.length:0,o=[];for(e=d.createCallback(e,n,3);++i<r;){var a=t[i];e(a,i,t)&&(o.push(a),Fi.call(t,i--,1),r--)}return o}function Cn(t,e,n){if("number"!=typeof e&&null!=e){var i=0,r=-1,o=t?t.length:0;for(e=d.createCallback(e,n,3);++r<o&&e(t[r],r,t);)i++}else i=null==e||n?1:Ui(0,e);return f(t,i)}function kn(t,e,n,i){var r=0,o=t?t.length:r;for(n=n?d.createCallback(n,i,1):Jn,e=n(e);o>r;){var a=r+o>>>1;n(t[a])<e?r=a+1:o=a}return r}function An(){return re(te(arguments,!0,!0))}function Dn(t,e,n,i){return"boolean"!=typeof e&&null!=e&&(i=n,n="function"!=typeof e&&i&&i[e]===t?null:e,e=!1),null!=n&&(n=d.createCallback(n,i,3)),re(t,e,n)}function En(t){return J(t,f(arguments,1))}function Pn(){for(var t=-1,e=arguments.length;++t<e;){var n=arguments[t];if(Ji(n)||pe(n))var i=i?re(J(i,n).concat(J(n,i))):n}return i||[]}function Mn(){for(var t=arguments.length>1?arguments:arguments[0],e=-1,n=t?tn(hr(t,"length")):0,i=di(0>n?0:n);++e<n;)i[e]=hr(t,e);return i}function On(t,e){var n=-1,i=t?t.length:0,r={};for(e||!i||Ji(t[0])||(e=[]);++n<i;){var o=t[n];e?r[o]=e[n]:o&&(r[o[0]]=o[1])}return r}function Rn(t,e){if(!De(e))throw new xi;return function(){return--t<1?e.apply(this,arguments):void 0}}function Nn(t,e){return arguments.length>2?ae(t,17,f(arguments,2),null,e):ae(t,1,null,null,e)}function In(t){for(var e=arguments.length>1?te(arguments,!0,!1,1):be(t),n=-1,i=e.length;++n<i;){var r=e[n];t[r]=ae(t[r],1,null,null,t)}return t}function Fn(t,e){return arguments.length>2?ae(e,19,f(arguments,2),null,t):ae(e,3,null,null,t)}function Ln(){for(var t=arguments,e=t.length;e--;)if(!De(t[e]))throw new xi;return function(){for(var e=arguments,n=t.length;n--;)e=[t[n].apply(this,e)];return e[0]}}function Yn(t,e){return e="number"==typeof e?e:+e||t.length,ae(t,4,null,null,null,e)}function Vn(t,e,n){var i,r,o,a,s,l,u,c=0,f=!1,p=!0;if(!De(t))throw new xi;if(e=Ui(0,e)||0,n===!0){var d=!0;p=!1}else Ee(n)&&(d=n.leading,f="maxWait"in n&&(Ui(e,n.maxWait)||0),p="trailing"in n?n.trailing:p);var m=function(){var n=e-(mr()-a);if(0>=n){r&&Ei(r);var f=u;r=l=u=h,f&&(c=mr(),o=t.apply(s,i),l||r||(i=s=null))}else l=Ii(m,n)},g=function(){l&&Ei(l),r=l=u=h,(p||f!==e)&&(c=mr(),o=t.apply(s,i),l||r||(i=s=null))};return function(){if(i=arguments,a=mr(),s=this,u=p&&(l||!d),f===!1)var n=d&&!l;else{r||d||(c=a);var h=f-(a-c),_=0>=h;_?(r&&(r=Ei(r)),c=a,o=t.apply(s,i)):r||(r=Ii(g,h))}return _&&l?l=Ei(l):l||e===f||(l=Ii(m,e)),n&&(_=!0,o=t.apply(s,i)),!_||l||r||(i=s=null),o}}function Xn(t){if(!De(t))throw new xi;var e=f(arguments,1);return Ii(function(){t.apply(h,e)},1)}function jn(t,e){if(!De(t))throw new xi;var n=f(arguments,2);return Ii(function(){t.apply(h,n)},e)}function Bn(t,e){if(!De(t))throw new xi;var n=function(){var i=n.cache,r=e?e.apply(this,arguments):_+arguments[0];return Ri.call(i,r)?i[r]:i[r]=t.apply(this,arguments)};return n.cache={},n}function Hn(t){var e,n;if(!De(t))throw new xi;return function(){return e?n:(e=!0,n=t.apply(this,arguments),t=null,n)}}function Un(t){return ae(t,16,f(arguments,1))}function zn(t){return ae(t,32,null,f(arguments,1))}function Wn(t,e,n){var i=!0,r=!0;if(!De(t))throw new xi;return n===!1?i=!1:Ee(n)&&(i="leading"in n?n.leading:i,r="trailing"in n?n.trailing:r),H.leading=i,H.maxWait=e,H.trailing=r,Vn(t,e,H)}function qn(t,e){return ae(e,16,[t])}function Kn(t){return function(){return t}}function Gn(t,e,n){var i=typeof t;if(null==t||"function"==i)return G(t,e,n);if("object"!=i)return ni(t);var r=tr(t),o=r[0],a=t[o];return 1!=r.length||a!==a||Ee(a)?function(e){for(var n=r.length,i=!1;n--&&(i=ee(e[r[n]],t[r[n]],null,!0)););return i}:function(t){var e=t[o];return a===e&&(0!==a||1/a==1/e)}}function Zn(t){return null==t?"":$i(t).replace(rr,se)}function Jn(t){return t}function Qn(t,e,n){var i=!0,r=e&&be(e);e&&(n||r.length)||(null==n&&(n=e),o=m,e=t,t=d,r=be(e)),n===!1?i=!1:Ee(n)&&"chain"in n&&(i=n.chain);var o=t,a=De(o);Ge(r,function(n){var r=t[n]=e[n];a&&(o.prototype[n]=function(){var e=this.__chain__,n=this.__wrapped__,a=[n];Ni.apply(a,arguments);var s=r.apply(t,a);if(i||e){if(n===s&&Ee(s))return this;s=new o(s),s.__chain__=e}return s})})}function ti(){return n._=Ci,this}function ei(){}function ni(t){return function(e){return e[t]}}function ii(t,e,n){var i=null==t,r=null==e;if(null==n&&("boolean"==typeof t&&r?(n=t,t=1):r||"boolean"!=typeof e||(n=e,r=!0)),i&&r&&(e=1),t=+t||0,r?(e=t,t=0):e=+e||0,n||t%1||e%1){var o=qi();return zi(t+o*(e-t+parseFloat("1e-"+((o+"").length-1))),e)}return ie(t,e)}function ri(t,e){if(t){var n=t[e];return De(n)?t[e]():n}}function oi(t,e,n){var i=d.templateSettings;t=$i(t||""),n=ar({},n,i);var r,o=ar({},n.imports,i.imports),s=tr(o),l=Be(o),u=0,c=n.interpolate||D,f="__p += '",p=wi((n.escape||D).source+"|"+c.source+"|"+(c===k?T:D).source+"|"+(n.evaluate||D).source+"|$","g");t.replace(p,function(e,n,i,o,s,l){return i||(i=o),f+=t.slice(u,l).replace(P,a),n&&(f+="' +\n__e("+n+") +\n'"),s&&(r=!0,f+="';\n"+s+";\n__p += '"),i&&(f+="' +\n((__t = ("+i+")) == null ? '' : __t) +\n'"),u=l+e.length,e}),f+="';\n";var m=n.variable,g=m;g||(m="obj",f="with ("+m+") {\n"+f+"\n}\n"),f=(r?f.replace(w,""):f).replace($,"$1").replace(x,"$1;"),f="function("+m+") {\n"+(g?"":m+" || ("+m+" = {});\n")+"var __t, __p = '', __e = _.escape"+(r?", __j = Array.prototype.join;\nfunction print() { __p += __j.call(arguments, '') }\n":";\n")+f+"return __p\n}";var _="\n/*\n//# sourceURL="+(n.sourceURL||"/lodash/template/source["+O++ +"]")+"\n*/";try{var v=_i(s,"return "+f+_).apply(h,l)}catch(y){throw y.source=f,y}return e?v(e):(v.source=f,v)}function ai(t,e,n){t=(t=+t)>-1?t:0;var i=-1,r=di(t);
for(e=G(e,n,1);++i<t;)r[i]=e(i);return r}function si(t){return null==t?"":$i(t).replace(ir,fe)}function li(t){var e=++g;return $i(null==t?"":t)+e}function ui(t){return t=new m(t),t.__chain__=!0,t}function ci(t,e){return e(t),t}function fi(){return this.__chain__=!0,this}function pi(){return $i(this.__wrapped__)}function hi(){return this.__wrapped__}n=n?Q.defaults(q.Object(),n,Q.pick(q,M)):q;var di=n.Array,mi=n.Boolean,gi=n.Date,_i=n.Function,vi=n.Math,yi=n.Number,bi=n.Object,wi=n.RegExp,$i=n.String,xi=n.TypeError,Ti=[],Si=bi.prototype,Ci=n._,ki=Si.toString,Ai=wi("^"+$i(ki).replace(/[.*+?^${}()|[\]\\]/g,"\\$&").replace(/toString| for [^\]]+/g,".*?")+"$"),Di=vi.ceil,Ei=n.clearTimeout,Pi=vi.floor,Mi=_i.prototype.toString,Oi=ue(Oi=bi.getPrototypeOf)&&Oi,Ri=Si.hasOwnProperty,Ni=Ti.push,Ii=n.setTimeout,Fi=Ti.splice,Li=Ti.unshift,Yi=function(){try{var t={},e=ue(e=bi.defineProperty)&&e,n=e(t,t,t)&&e}catch(i){}return n}(),Vi=ue(Vi=bi.create)&&Vi,Xi=ue(Xi=di.isArray)&&Xi,ji=n.isFinite,Bi=n.isNaN,Hi=ue(Hi=bi.keys)&&Hi,Ui=vi.max,zi=vi.min,Wi=n.parseInt,qi=vi.random,Ki={};Ki[N]=di,Ki[I]=mi,Ki[F]=gi,Ki[L]=_i,Ki[V]=bi,Ki[Y]=yi,Ki[X]=wi,Ki[j]=$i,m.prototype=d.prototype;var Gi=d.support={};Gi.funcDecomp=!ue(n.WinRTError)&&E.test(p),Gi.funcNames="string"==typeof _i.name,d.templateSettings={escape:/<%-([\s\S]+?)%>/g,evaluate:/<%([\s\S]+?)%>/g,interpolate:k,variable:"",imports:{_:d}},Vi||(K=function(){function t(){}return function(e){if(Ee(e)){t.prototype=e;var i=new t;t.prototype=null}return i||n.Object()}}());var Zi=Yi?function(t,e){U.value=e,Yi(t,"__bindData__",U)}:ei,Ji=Xi||function(t){return t&&"object"==typeof t&&"number"==typeof t.length&&ki.call(t)==N||!1},Qi=function(t){var e,n=t,i=[];if(!n)return i;if(!z[typeof t])return i;for(e in n)Ri.call(n,e)&&i.push(e);return i},tr=Hi?function(t){return Ee(t)?Hi(t):[]}:Qi,er={"&":"&amp;","<":"&lt;",">":"&gt;",'"':"&quot;","'":"&#39;"},nr=$e(er),ir=wi("("+tr(nr).join("|")+")","g"),rr=wi("["+tr(er).join("")+"]","g"),or=function(t,e,n){var i,r=t,o=r;if(!r)return o;var a=arguments,s=0,l="number"==typeof n?2:a.length;if(l>3&&"function"==typeof a[l-2])var u=G(a[--l-1],a[l--],2);else l>2&&"function"==typeof a[l-1]&&(u=a[--l]);for(;++s<l;)if(r=a[s],r&&z[typeof r])for(var c=-1,f=z[typeof r]&&tr(r),p=f?f.length:0;++c<p;)i=f[c],o[i]=u?u(o[i],r[i]):r[i];return o},ar=function(t,e,n){var i,r=t,o=r;if(!r)return o;for(var a=arguments,s=0,l="number"==typeof n?2:a.length;++s<l;)if(r=a[s],r&&z[typeof r])for(var u=-1,c=z[typeof r]&&tr(r),f=c?c.length:0;++u<f;)i=c[u],"undefined"==typeof o[i]&&(o[i]=r[i]);return o},sr=function(t,e,n){var i,r=t,o=r;if(!r)return o;if(!z[typeof r])return o;e=e&&"undefined"==typeof n?e:G(e,n,3);for(i in r)if(e(r[i],i,t)===!1)return o;return o},lr=function(t,e,n){var i,r=t,o=r;if(!r)return o;if(!z[typeof r])return o;e=e&&"undefined"==typeof n?e:G(e,n,3);for(var a=-1,s=z[typeof r]&&tr(r),l=s?s.length:0;++a<l;)if(i=s[a],e(r[i],i,t)===!1)return o;return o},ur=Oi?function(t){if(!t||ki.call(t)!=V)return!1;var e=t.valueOf,n=ue(e)&&(n=Oi(e))&&Oi(n);return n?t==n||Oi(t)==n:ce(t)}:ce,cr=oe(function(t,e,n){Ri.call(t,n)?t[n]++:t[n]=1}),fr=oe(function(t,e,n){(Ri.call(t,n)?t[n]:t[n]=[]).push(e)}),pr=oe(function(t,e,n){t[n]=e}),hr=Qe,dr=We,mr=ue(mr=gi.now)&&mr||function(){return(new gi).getTime()},gr=8==Wi(b+"08")?Wi:function(t,e){return Wi(Ne(t)?t.replace(A,""):t,e||0)};return d.after=Rn,d.assign=or,d.at=He,d.bind=Nn,d.bindAll=In,d.bindKey=Fn,d.chain=ui,d.compact=pn,d.compose=Ln,d.constant=Kn,d.countBy=cr,d.create=me,d.createCallback=Gn,d.curry=Yn,d.debounce=Vn,d.defaults=ar,d.defer=Xn,d.delay=jn,d.difference=hn,d.filter=We,d.flatten=_n,d.forEach=Ge,d.forEachRight=Ze,d.forIn=sr,d.forInRight=ve,d.forOwn=lr,d.forOwnRight=ye,d.functions=be,d.groupBy=fr,d.indexBy=pr,d.initial=yn,d.intersection=bn,d.invert=$e,d.invoke=Je,d.keys=tr,d.map=Qe,d.mapValues=Fe,d.max=tn,d.memoize=Bn,d.merge=Le,d.min=en,d.omit=Ye,d.once=Hn,d.pairs=Ve,d.partial=Un,d.partialRight=zn,d.pick=Xe,d.pluck=hr,d.property=ni,d.pull=xn,d.range=Tn,d.reject=on,d.remove=Sn,d.rest=Cn,d.shuffle=sn,d.sortBy=cn,d.tap=ci,d.throttle=Wn,d.times=ai,d.toArray=fn,d.transform=je,d.union=An,d.uniq=Dn,d.values=Be,d.where=dr,d.without=En,d.wrap=qn,d.xor=Pn,d.zip=Mn,d.zipObject=On,d.collect=Qe,d.drop=Cn,d.each=Ge,d.eachRight=Ze,d.extend=or,d.methods=be,d.object=On,d.select=We,d.tail=Cn,d.unique=Dn,d.unzip=Mn,Qn(d),d.clone=he,d.cloneDeep=de,d.contains=Ue,d.escape=Zn,d.every=ze,d.find=qe,d.findIndex=dn,d.findKey=ge,d.findLast=Ke,d.findLastIndex=mn,d.findLastKey=_e,d.has=we,d.identity=Jn,d.indexOf=vn,d.isArguments=pe,d.isArray=Ji,d.isBoolean=xe,d.isDate=Te,d.isElement=Se,d.isEmpty=Ce,d.isEqual=ke,d.isFinite=Ae,d.isFunction=De,d.isNaN=Pe,d.isNull=Me,d.isNumber=Oe,d.isObject=Ee,d.isPlainObject=ur,d.isRegExp=Re,d.isString=Ne,d.isUndefined=Ie,d.lastIndexOf=$n,d.mixin=Qn,d.noConflict=ti,d.noop=ei,d.now=mr,d.parseInt=gr,d.random=ii,d.reduce=nn,d.reduceRight=rn,d.result=ri,d.runInContext=p,d.size=ln,d.some=un,d.sortedIndex=kn,d.template=oi,d.unescape=si,d.uniqueId=li,d.all=ze,d.any=un,d.detect=qe,d.findWhere=qe,d.foldl=nn,d.foldr=rn,d.include=Ue,d.inject=nn,Qn(function(){var t={};return lr(d,function(e,n){d.prototype[n]||(t[n]=e)}),t}(),!1),d.first=gn,d.last=wn,d.sample=an,d.take=gn,d.head=gn,lr(d,function(t,e){var n="sample"!==e;d.prototype[e]||(d.prototype[e]=function(e,i){var r=this.__chain__,o=t(this.__wrapped__,e,i);return r||null!=e&&(!i||n&&"function"==typeof e)?new m(o,r):o})}),d.VERSION="2.4.1",d.prototype.chain=fi,d.prototype.toString=pi,d.prototype.value=hi,d.prototype.valueOf=hi,Ge(["join","pop","shift"],function(t){var e=Ti[t];d.prototype[t]=function(){var t=this.__chain__,n=e.apply(this.__wrapped__,arguments);return t?new m(n,t):n}}),Ge(["push","reverse","sort","unshift"],function(t){var e=Ti[t];d.prototype[t]=function(){return e.apply(this.__wrapped__,arguments),this}}),Ge(["concat","slice","splice"],function(t){var e=Ti[t];d.prototype[t]=function(){return new m(e.apply(this.__wrapped__,arguments),this.__chain__)}}),d}var h,d=[],m=[],g=0,_=+new Date+"",v=75,y=40,b=" 	\f ﻿\n\r\u2028\u2029 ᠎             　",w=/\b__p \+= '';/g,$=/\b(__p \+=) '' \+/g,x=/(__e\(.*?\)|\b__t\)) \+\n'';/g,T=/\$\{([^\\}]*(?:\\.[^\\}]*)*)\}/g,S=/\w*$/,C=/^\s*function[ \n\r\t]+\w/,k=/<%=([\s\S]+?)%>/g,A=RegExp("^["+b+"]*0+(?=.$)"),D=/($^)/,E=/\bthis\b/,P=/['\n\r\t\u2028\u2029\\]/g,M=["Array","Boolean","Date","Function","Math","Number","Object","RegExp","String","_","attachEvent","clearTimeout","isFinite","isNaN","parseInt","setTimeout"],O=0,R="[object Arguments]",N="[object Array]",I="[object Boolean]",F="[object Date]",L="[object Function]",Y="[object Number]",V="[object Object]",X="[object RegExp]",j="[object String]",B={};B[L]=!1,B[R]=B[N]=B[I]=B[F]=B[Y]=B[V]=B[X]=B[j]=!0;var H={leading:!1,maxWait:0,trailing:!1},U={configurable:!1,enumerable:!1,value:null,writable:!1},z={"boolean":!1,"function":!0,object:!0,number:!1,string:!1,undefined:!1},W={"\\":"\\","'":"'","\n":"n","\r":"r","	":"t","\u2028":"u2028","\u2029":"u2029"},q=z[typeof window]&&window||this,K=z[typeof exports]&&exports&&!exports.nodeType&&exports,G=z[typeof module]&&module&&!module.nodeType&&module,Z=G&&G.exports===K&&K,J=z[typeof global]&&global;!J||J.global!==J&&J.window!==J||(q=J);var Q=p();"function"==typeof define&&"object"==typeof define.amd&&define.amd?(q._=Q,define(function(){return Q})):K&&G?Z?(G.exports=Q)._=Q:K._=Q:q._=Q}.call(this),window.define||(window.define=function(t){try{delete window.define}catch(e){window.define=void 0}window.when=t()},window.define.amd={}),function(t){t||(t=window.console={log:function(){},info:function(){},warn:function(){},error:function(){}}),Function.prototype.bind||(Function.prototype.bind=function(t){var e=this,n=Array.prototype.slice.call(arguments,1);return function(){return e.apply(t,Array.prototype.concat.apply(n,arguments))}}),"object"==typeof t.log&&(t.log=Function.prototype.call.bind(t.log,t),t.info=Function.prototype.call.bind(t.info,t),t.warn=Function.prototype.call.bind(t.warn,t),t.error=Function.prototype.call.bind(t.error,t)),"group"in t||(t.group=function(e){t.info("\n--- "+e+" ---\n")}),"groupEnd"in t||(t.groupEnd=function(){t.log("\n")}),"time"in t||function(){var e={};t.time=function(t){e[t]=(new Date).getTime()},t.timeEnd=function(n){var i=(new Date).getTime();t.info(n+": "+(n in e?i-e[n]:0)+"ms")}}()}(window.console),function(t){t(function(t){function e(t,e,n,o){return(t instanceof i?t:r(t)).then(e,n,o)}function n(t){return new i(t,D.PromiseStatus&&D.PromiseStatus())}function i(t,e){function n(t){if(p){var n=p;p=P,v(function(){l=a(s,t),e&&f(l,e),o(n,l)})}}function i(t){n(new u(t))}function r(t){if(p){var e=p;v(function(){o(e,new c(t))})}}var s,l,p=[];s=this,this._status=e,this.inspect=function(){return l?l.inspect():{state:"pending"}},this._when=function(t,e,n,i,r){function o(o){o._when(t,e,n,i,r)}p?p.push(o):v(function(){o(l)})};try{t(n,i,r)}catch(h){i(h)}}function r(t){return n(function(e){e(t)})}function o(t,e){for(var n=0;n<t.length;n++)t[n](e)}function a(t,e){if(e===t)return new u(new TypeError);if(e instanceof i)return e;try{var n=e===Object(e)&&e.then;return"function"==typeof n?s(n,e):new l(e)}catch(r){return new u(r)}}function s(t,e){return n(function(n,i){C(t,e,n,i)})}function l(t){this.value=t}function u(t){this.value=t}function c(t){this.value=t}function f(t,e){t.then(function(){e.fulfilled()},function(t){e.rejected(t)})}function p(t){return t&&"function"==typeof t.then}function h(t,i,r,o,a){return e(t,function(t){return n(function(n,r,o){function a(t){h(t)}function s(t){p(t)}var l,u,c,f,p,h,d,m;if(d=t.length>>>0,l=Math.max(0,Math.min(i,d)),c=[],u=d-l+1,f=[],l)for(h=function(t){f.push(t),--u||(p=h=b,r(f))},p=function(t){c.push(t),--l||(p=h=b,n(c))},m=0;d>m;++m)m in t&&e(t[m],s,a,o);else n(c)}).then(r,o,a)})}function d(t,e,n,i){return m(t,b).then(e,n,i)}function m(t,n,r){return e(t,function(t){return new i(function(i,o,a){function s(t,s){e(t,n,r).then(function(t){l[s]=t,--c||i(l)},o,a)}var l,u,c,f;if(c=u=t.length>>>0,l=[],c)for(f=0;u>f;f++)f in t?s(t[f],f):--c;else i(l)})})}function g(t){return{state:"fulfilled",value:t}}function _(t){return{state:"rejected",reason:t}}function v(t){1===A.push(t)&&k(y)}function y(){o(A),A=[]}function b(t){return t}function w(t){throw"function"==typeof D.reportUnhandled?D.reportUnhandled():v(function(){throw t}),t}e.promise=n,e.resolve=r,e.reject=function(t){return e(t,function(t){return new u(t)})},e.defer=function(){var t,e,i;return t={promise:P,resolve:P,reject:P,notify:P,resolver:{resolve:P,reject:P,notify:P}},t.promise=e=n(function(n,o,a){t.resolve=t.resolver.resolve=function(t){return i?r(t):(i=!0,n(t),e)},t.reject=t.resolver.reject=function(t){return i?r(new u(t)):(i=!0,o(t),e)},t.notify=t.resolver.notify=function(t){return a(t),t}}),t},e.join=function(){return m(arguments,b)},e.all=d,e.map=function(t,e){return m(t,e)},e.reduce=function(t,n){var i=C(S,arguments,1);return e(t,function(t){var r;return r=t.length,i[0]=function(t,i,o){return e(t,function(t){return e(i,function(e){return n(t,e,o,r)})})},T.apply(t,i)})},e.settle=function(t){return m(t,g,_)},e.any=function(t,e,n,i){return h(t,1,function(t){return e?e(t[0]):t[0]},n,i)},e.some=h,e.isPromise=p,e.isPromiseLike=p,$=i.prototype,$.then=function(t,e,n){var r=this;return new i(function(i,o,a){r._when(i,a,t,e,n)},this._status&&this._status.observed())},$["catch"]=$.otherwise=function(t){return this.then(P,t)},$["finally"]=$.ensure=function(t){function e(){return r(t())}return"function"==typeof t?this.then(e,e).yield(this):this},$.done=function(t,e){this.then(t,e)["catch"](w)},$.yield=function(t){return this.then(function(){return t})},$.tap=function(t){return this.then(t).yield(this)},$.spread=function(t){return this.then(function(e){return d(e,function(e){return t.apply(P,e)})})},$.always=function(t,e){return this.then(t,t,e)},x=Object.create||function(t){function e(){}return e.prototype=t,new e},l.prototype=x($),l.prototype.inspect=function(){return g(this.value)},l.prototype._when=function(t,e,n){try{t("function"==typeof n?n(this.value):this.value)}catch(i){t(new u(i))}},u.prototype=x($),u.prototype.inspect=function(){return _(this.value)},u.prototype._when=function(t,e,n,i){try{t("function"==typeof i?i(this.value):this)}catch(r){t(new u(r))}},c.prototype=x($),c.prototype._when=function(t,e,n,i,r){try{e("function"==typeof r?r(this.value):this.value)}catch(o){e(o)}};var $,x,T,S,C,k,A,D,E,P;if(A=[],D="undefined"!=typeof console?console:e,"object"==typeof process&&process.nextTick)k=process.nextTick;else if($="function"==typeof MutationObserver&&MutationObserver||"function"==typeof WebKitMutationObserver&&WebKitMutationObserver)k=function(t,e,n){var i=t.createElement("div");return new e(n).observe(i,{attributes:!0}),function(){i.setAttribute("x","x")}}(document,$,y);else try{k=t("vertx").runOnLoop||t("vertx").runOnContext}catch(M){E=setTimeout,k=function(t){E(t,0)}}return t=Function.prototype,$=t.call,C=t.bind?$.bind($):function(t,e){return t.apply(e,S.call(arguments,2))},t=[],S=t.slice,T=t.reduce||function(t){var e,n,i,r,o;if(o=0,e=Object(this),r=e.length>>>0,n=arguments,1>=n.length)for(;;){if(o in e){i=e[o++];break}if(++o>=r)throw new TypeError}else i=n[1];for(;r>o;++o)o in e&&(i=t(i,e[o],o,e));return i},e})}("function"==typeof define&&define.amd?define:function(t){module.exports=t(require)});var CryptoJS=CryptoJS||function(t,e){var n={},i=n.lib={},r=i.Base=function(){function t(){}return{extend:function(e){t.prototype=this;var n=new t;return e&&n.mixIn(e),n.hasOwnProperty("init")||(n.init=function(){n.$super.init.apply(this,arguments)}),n.init.prototype=n,n.$super=this,n},create:function(){var t=this.extend();return t.init.apply(t,arguments),t},init:function(){},mixIn:function(t){for(var e in t)t.hasOwnProperty(e)&&(this[e]=t[e]);t.hasOwnProperty("toString")&&(this.toString=t.toString)},clone:function(){return this.init.prototype.extend(this)}}}(),o=i.WordArray=r.extend({init:function(t,n){t=this.words=t||[],this.sigBytes=n!=e?n:4*t.length},toString:function(t){return(t||s).stringify(this)},concat:function(t){var e=this.words,n=t.words,i=this.sigBytes;if(t=t.sigBytes,this.clamp(),i%4)for(var r=0;t>r;r++)e[i+r>>>2]|=(n[r>>>2]>>>24-8*(r%4)&255)<<24-8*((i+r)%4);else if(65535<n.length)for(r=0;t>r;r+=4)e[i+r>>>2]=n[r>>>2];else e.push.apply(e,n);return this.sigBytes+=t,this},clamp:function(){var e=this.words,n=this.sigBytes;e[n>>>2]&=4294967295<<32-8*(n%4),e.length=t.ceil(n/4)},clone:function(){var t=r.clone.call(this);return t.words=this.words.slice(0),t},random:function(e){for(var n=[],i=0;e>i;i+=4)n.push(4294967296*t.random()|0);return new o.init(n,e)}}),a=n.enc={},s=a.Hex={stringify:function(t){var e=t.words;t=t.sigBytes;for(var n=[],i=0;t>i;i++){var r=e[i>>>2]>>>24-8*(i%4)&255;n.push((r>>>4).toString(16)),n.push((15&r).toString(16))}return n.join("")},parse:function(t){for(var e=t.length,n=[],i=0;e>i;i+=2)n[i>>>3]|=parseInt(t.substr(i,2),16)<<24-4*(i%8);return new o.init(n,e/2)}},l=a.Latin1={stringify:function(t){var e=t.words;t=t.sigBytes;for(var n=[],i=0;t>i;i++)n.push(String.fromCharCode(e[i>>>2]>>>24-8*(i%4)&255));return n.join("")},parse:function(t){for(var e=t.length,n=[],i=0;e>i;i++)n[i>>>2]|=(255&t.charCodeAt(i))<<24-8*(i%4);return new o.init(n,e)}},u=a.Utf8={stringify:function(t){try{return decodeURIComponent(escape(l.stringify(t)))}catch(e){throw Error("Malformed UTF-8 data")}},parse:function(t){return l.parse(unescape(encodeURIComponent(t)))}},c=i.BufferedBlockAlgorithm=r.extend({reset:function(){this._data=new o.init,this._nDataBytes=0},_append:function(t){"string"==typeof t&&(t=u.parse(t)),this._data.concat(t),this._nDataBytes+=t.sigBytes},_process:function(e){var n=this._data,i=n.words,r=n.sigBytes,a=this.blockSize,s=r/(4*a),s=e?t.ceil(s):t.max((0|s)-this._minBufferSize,0);if(e=s*a,r=t.min(4*e,r),e){for(var l=0;e>l;l+=a)this._doProcessBlock(i,l);l=i.splice(0,e),n.sigBytes-=r}return new o.init(l,r)},clone:function(){var t=r.clone.call(this);return t._data=this._data.clone(),t},_minBufferSize:0});i.Hasher=c.extend({cfg:r.extend(),init:function(t){this.cfg=this.cfg.extend(t),this.reset()},reset:function(){c.reset.call(this),this._doReset()},update:function(t){return this._append(t),this._process(),this},finalize:function(t){return t&&this._append(t),this._doFinalize()},blockSize:16,_createHelper:function(t){return function(e,n){return new t.init(n).finalize(e)}},_createHmacHelper:function(t){return function(e,n){return new f.HMAC.init(t,n).finalize(e)}}});var f=n.algo={};return n}(Math);!function(){var t=CryptoJS,e=t.lib.WordArray;t.enc.Base64={stringify:function(t){var e=t.words,n=t.sigBytes,i=this._map;t.clamp(),t=[];for(var r=0;n>r;r+=3)for(var o=(e[r>>>2]>>>24-8*(r%4)&255)<<16|(e[r+1>>>2]>>>24-8*((r+1)%4)&255)<<8|e[r+2>>>2]>>>24-8*((r+2)%4)&255,a=0;4>a&&n>r+.75*a;a++)t.push(i.charAt(o>>>6*(3-a)&63));if(e=i.charAt(64))for(;t.length%4;)t.push(e);return t.join("")},parse:function(t){var n=t.length,i=this._map,r=i.charAt(64);r&&(r=t.indexOf(r),-1!=r&&(n=r));for(var r=[],o=0,a=0;n>a;a++)if(a%4){var s=i.indexOf(t.charAt(a-1))<<2*(a%4),l=i.indexOf(t.charAt(a))>>>6-2*(a%4);r[o>>>2]|=(s|l)<<24-8*(o%4),o++}return e.create(r,o)},_map:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="}}(),function(){var t=CryptoJS,e=t.enc.Utf8;t.algo.HMAC=t.lib.Base.extend({init:function(t,n){t=this._hasher=new t.init,"string"==typeof n&&(n=e.parse(n));var i=t.blockSize,r=4*i;n.sigBytes>r&&(n=t.finalize(n)),n.clamp();for(var o=this._oKey=n.clone(),a=this._iKey=n.clone(),s=o.words,l=a.words,u=0;i>u;u++)s[u]^=1549556828,l[u]^=909522486;o.sigBytes=a.sigBytes=r,this.reset()},reset:function(){var t=this._hasher;t.reset(),t.update(this._iKey)},update:function(t){return this._hasher.update(t),this},finalize:function(t){var e=this._hasher;return t=e.finalize(t),e.reset(),e.finalize(this._oKey.clone().concat(t))}})}(),function(t){var e=CryptoJS,n=e.lib,i=n.WordArray,r=n.Hasher,n=e.algo,o=[],a=[];!function(){function e(e){for(var n=t.sqrt(e),i=2;n>=i;i++)if(!(e%i))return!1;return!0}function n(t){return 4294967296*(t-(0|t))|0}for(var i=2,r=0;64>r;)e(i)&&(8>r&&(o[r]=n(t.pow(i,.5))),a[r]=n(t.pow(i,1/3)),r++),i++}();var s=[],n=n.SHA256=r.extend({_doReset:function(){this._hash=new i.init(o.slice(0))},_doProcessBlock:function(t,e){for(var n=this._hash.words,i=n[0],r=n[1],o=n[2],l=n[3],u=n[4],c=n[5],f=n[6],p=n[7],h=0;64>h;h++){if(16>h)s[h]=0|t[e+h];else{var d=s[h-15],m=s[h-2];s[h]=((d<<25|d>>>7)^(d<<14|d>>>18)^d>>>3)+s[h-7]+((m<<15|m>>>17)^(m<<13|m>>>19)^m>>>10)+s[h-16]}d=p+((u<<26|u>>>6)^(u<<21|u>>>11)^(u<<7|u>>>25))+(u&c^~u&f)+a[h]+s[h],m=((i<<30|i>>>2)^(i<<19|i>>>13)^(i<<10|i>>>22))+(i&r^i&o^r&o),p=f,f=c,c=u,u=l+d|0,l=o,o=r,r=i,i=d+m|0}n[0]=n[0]+i|0,n[1]=n[1]+r|0,n[2]=n[2]+o|0,n[3]=n[3]+l|0,n[4]=n[4]+u|0,n[5]=n[5]+c|0,n[6]=n[6]+f|0,n[7]=n[7]+p|0},_doFinalize:function(){var e=this._data,n=e.words,i=8*this._nDataBytes,r=8*e.sigBytes;return n[r>>>5]|=128<<24-r%32,n[(r+64>>>9<<4)+14]=t.floor(i/4294967296),n[(r+64>>>9<<4)+15]=i,e.sigBytes=4*n.length,this._process(),this._hash},clone:function(){var t=r.clone.call(this);return t._hash=this._hash.clone(),t}});e.SHA256=r._createHelper(n),e.HmacSHA256=r._createHmacHelper(n)}(Math),function(){var t=CryptoJS,e=t.lib,n=e.Base,i=e.WordArray,e=t.algo,r=e.HMAC,o=e.PBKDF2=n.extend({cfg:n.extend({keySize:4,hasher:e.SHA1,iterations:1}),init:function(t){this.cfg=this.cfg.extend(t)},compute:function(t,e){for(var n=this.cfg,o=r.create(n.hasher,t),a=i.create(),s=i.create([1]),l=a.words,u=s.words,c=n.keySize,n=n.iterations;l.length<c;){var f=o.update(e).finalize(s);o.reset();for(var p=f.words,h=p.length,d=f,m=1;n>m;m++){d=o.finalize(d),o.reset();for(var g=d.words,_=0;h>_;_++)p[_]^=g[_]}a.concat(f),u[0]++}return a.sigBytes=4*c,a}});t.PBKDF2=function(t,e,n){return o.create(n).compute(t,e)}}();var AUTOBAHNJS_VERSION="0.8.2",global=this;!function(t,e){"function"==typeof define&&define.amd?define(["when"],function(n){return t.ab=e(t,n)}):"undefined"!=typeof exports?"undefined"!=typeof module&&module.exports&&(exports=module.exports=e(t,t.when)):t.ab=e(t,t.when)}(global,function(t,e){var n={_version:AUTOBAHNJS_VERSION};return function(){Array.prototype.indexOf||(Array.prototype.indexOf=function(t){if(null===this)throw new TypeError;var e=Object(this),n=e.length>>>0;if(0===n)return-1;var i=0;if(0<arguments.length&&(i=Number(arguments[1]),i!==i?i=0:0!==i&&1/0!==i&&-1/0!==i&&(i=(i>0||-1)*Math.floor(Math.abs(i)))),i>=n)return-1;for(i=i>=0?i:Math.max(n-Math.abs(i),0);n>i;i++)if(i in e&&e[i]===t)return i;return-1}),Array.prototype.forEach||(Array.prototype.forEach=function(t,e){var n,i;if(null===this)throw new TypeError(" this is null or not defined");var r=Object(this),o=r.length>>>0;if("[object Function]"!=={}.toString.call(t))throw new TypeError(t+" is not a function");for(e&&(n=e),i=0;o>i;){var a;i in r&&(a=r[i],t.call(n,a,i,r)),i++}})}(),n._sliceUserAgent=function(t,e,n){var i=[],r=navigator.userAgent;for(t=r.indexOf(t),e=r.indexOf(e,t),0>e&&(e=r.length),n=r.slice(t,e).split(n),r=n[1].split("."),e=0;e<r.length;++e)i.push(parseInt(r[e],10));return{name:n[0],version:i}},n.getBrowser=function(){var t=navigator.userAgent;return-1<t.indexOf("Chrome")?n._sliceUserAgent("Chrome"," ","/"):-1<t.indexOf("Safari")?n._sliceUserAgent("Safari"," ","/"):-1<t.indexOf("Firefox")?n._sliceUserAgent("Firefox"," ","/"):-1<t.indexOf("MSIE")?n._sliceUserAgent("MSIE",";"," "):null},n.getServerUrl=function(e,n){return"file:"===t.location.protocol?n?n:"ws://127.0.0.1/ws":("https:"===t.location.protocol?"wss://":"ws://")+t.location.hostname+(""!==t.location.port?":"+t.location.port:"")+"/"+(e?e:"ws")},n.browserNotSupportedMessage="Browser does not support WebSockets (RFC6455)",n.deriveKey=function(t,e){return e&&e.salt?CryptoJS.PBKDF2(t,e.salt,{keySize:(e.keylen||32)/4,iterations:e.iterations||1e4,hasher:CryptoJS.algo.SHA256}).toString(CryptoJS.enc.Base64):t},n._idchars="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789",n._idlen=16,n._subprotocol="wamp",n._newid=function(){for(var t="",e=0;e<n._idlen;e+=1)t+=n._idchars.charAt(Math.floor(Math.random()*n._idchars.length));return t},n._newidFast=function(){return Math.random().toString(36)},n.log=function(){if(1<arguments.length){console.group("Log Item");for(var t=0;t<arguments.length;t+=1)console.log(arguments[t]);console.groupEnd()}else console.log(arguments[0])},n._debugrpc=!1,n._debugpubsub=!1,n._debugws=!1,n._debugconnect=!1,n.debug=function(e,i,r){if(!("console"in t))throw"browser does not support console object";n._debugrpc=e,n._debugpubsub=e,n._debugws=i,n._debugconnect=r},n.version=function(){return n._version},n.PrefixMap=function(){this._index={},this._rindex={}},n.PrefixMap.prototype.get=function(t){return this._index[t]},n.PrefixMap.prototype.set=function(t,e){this._index[t]=e,this._rindex[e]=t},n.PrefixMap.prototype.setDefault=function(t){this._index[""]=t,this._rindex[t]=""},n.PrefixMap.prototype.remove=function(t){var e=this._index[t];e&&(delete this._index[t],delete this._rindex[e])},n.PrefixMap.prototype.resolve=function(t,e){var n=t.indexOf(":");if(n>=0){var i=t.substring(0,n);if(this._index[i])return this._index[i]+t.substring(n+1)}return!0===e?t:null},n.PrefixMap.prototype.shrink=function(t,e){for(var n=t.length;n>0;n-=1){var i=t.substring(0,n);if(i=this._rindex[i])return i+":"+t.substring(n)}return!0===e?t:null},n._MESSAGE_TYPEID_WELCOME=0,n._MESSAGE_TYPEID_PREFIX=1,n._MESSAGE_TYPEID_CALL=2,n._MESSAGE_TYPEID_CALL_RESULT=3,n._MESSAGE_TYPEID_CALL_ERROR=4,n._MESSAGE_TYPEID_SUBSCRIBE=5,n._MESSAGE_TYPEID_UNSUBSCRIBE=6,n._MESSAGE_TYPEID_PUBLISH=7,n._MESSAGE_TYPEID_EVENT=8,n.CONNECTION_CLOSED=0,n.CONNECTION_LOST=1,n.CONNECTION_RETRIES_EXCEEDED=2,n.CONNECTION_UNREACHABLE=3,n.CONNECTION_UNSUPPORTED=4,n.CONNECTION_UNREACHABLE_SCHEDULED_RECONNECT=5,n.CONNECTION_LOST_SCHEDULED_RECONNECT=6,n.Deferred=e.defer,n._construct=function(e,n){return"WebSocket"in t?n?new WebSocket(e,n):new WebSocket(e):"MozWebSocket"in t?n?new MozWebSocket(e,n):new MozWebSocket(e):null},n.Session=function(t,e,i,r){var o=this;if(o._wsuri=t,o._options=r,o._websocket_onopen=e,o._websocket_onclose=i,o._websocket=null,o._websocket_connected=!1,o._session_id=null,o._wamp_version=null,o._server=null,o._calls={},o._subscriptions={},o._prefixes=new n.PrefixMap,o._txcnt=0,o._rxcnt=0,o._websocket=o._options&&o._options.skipSubprotocolAnnounce?n._construct(o._wsuri):n._construct(o._wsuri,[n._subprotocol]),!o._websocket){if(void 0!==i)return void i(n.CONNECTION_UNSUPPORTED);throw n.browserNotSupportedMessage}o._websocket.onmessage=function(t){if(n._debugws&&(o._rxcnt+=1,console.group("WS Receive"),console.info(o._wsuri+"  ["+o._session_id+"]"),console.log(o._rxcnt),console.log(t.data),console.groupEnd()),t=JSON.parse(t.data),t[1]in o._calls){if(t[0]===n._MESSAGE_TYPEID_CALL_RESULT){var e=o._calls[t[1]],i=t[2];if(n._debugrpc&&void 0!==e._ab_callobj){console.group("WAMP Call",e._ab_callobj[2]),console.timeEnd(e._ab_tid),console.group("Arguments");for(var r=3;r<e._ab_callobj.length;r+=1){var a=e._ab_callobj[r];if(void 0===a)break;console.log(a)}console.groupEnd(),console.group("Result"),console.log(i),console.groupEnd(),console.groupEnd()}e.resolve(i)}else if(t[0]===n._MESSAGE_TYPEID_CALL_ERROR){if(e=o._calls[t[1]],i=t[2],r=t[3],a=t[4],n._debugrpc&&void 0!==e._ab_callobj){console.group("WAMP Call",e._ab_callobj[2]),console.timeEnd(e._ab_tid),console.group("Arguments");for(var s=3;s<e._ab_callobj.length;s+=1){var l=e._ab_callobj[s];if(void 0===l)break;console.log(l)}console.groupEnd(),console.group("Error"),console.log(i),console.log(r),void 0!==a&&console.log(a),console.groupEnd(),console.groupEnd()}e.reject(void 0!==a?{uri:i,desc:r,detail:a}:{uri:i,desc:r})}delete o._calls[t[1]]}else if(t[0]===n._MESSAGE_TYPEID_EVENT){if(e=o._prefixes.resolve(t[1],!0),e in o._subscriptions){var u=t[1],c=t[2];n._debugpubsub&&(console.group("WAMP Event"),console.info(o._wsuri+"  ["+o._session_id+"]"),console.log(u),console.log(c),console.groupEnd()),o._subscriptions[e].forEach(function(t){t(u,c)})}}else if(t[0]===n._MESSAGE_TYPEID_WELCOME){if(null!==o._session_id)throw"protocol error (welcome message received more than once)";o._session_id=t[1],o._wamp_version=t[2],o._server=t[3],(n._debugrpc||n._debugpubsub)&&(console.group("WAMP Welcome"),console.info(o._wsuri+"  ["+o._session_id+"]"),console.log(o._wamp_version),console.log(o._server),console.groupEnd()),null!==o._websocket_onopen&&o._websocket_onopen()}},o._websocket.onopen=function(){if(o._websocket.protocol!==n._subprotocol)if("undefined"==typeof o._websocket.protocol)n._debugws&&(console.group("WS Warning"),console.info(o._wsuri),console.log("WebSocket object has no protocol attribute: WAMP subprotocol check skipped!"),console.groupEnd());else{if(!o._options||!o._options.skipSubprotocolCheck)throw o._websocket.close(1e3,"server does not speak WAMP"),"server does not speak WAMP (but '"+o._websocket.protocol+"' !)";n._debugws&&(console.group("WS Warning"),console.info(o._wsuri),console.log("Server does not speak WAMP, but subprotocol check disabled by option!"),console.log(o._websocket.protocol),console.groupEnd())}n._debugws&&(console.group("WAMP Connect"),console.info(o._wsuri),console.log(o._websocket.protocol),console.groupEnd()),o._websocket_connected=!0},o._websocket.onerror=function(){},o._websocket.onclose=function(t){n._debugws&&console.log(o._websocket_connected?"Autobahn connection to "+o._wsuri+" lost (code "+t.code+", reason '"+t.reason+"', wasClean "+t.wasClean+").":"Autobahn could not connect to "+o._wsuri+" (code "+t.code+", reason '"+t.reason+"', wasClean "+t.wasClean+")."),void 0!==o._websocket_onclose&&(o._websocket_connected?t.wasClean?o._websocket_onclose(n.CONNECTION_CLOSED,"WS-"+t.code+": "+t.reason):o._websocket_onclose(n.CONNECTION_LOST):o._websocket_onclose(n.CONNECTION_UNREACHABLE)),o._websocket_connected=!1,o._wsuri=null,o._websocket_onopen=null,o._websocket_onclose=null,o._websocket=null},o.log=function(){console.group(o._options&&"sessionIdent"in o._options?"WAMP Session '"+o._options.sessionIdent+"' ["+o._session_id+"]":"WAMP Session ["+o._session_id+"]");for(var t=0;t<arguments.length;++t)console.log(arguments[t]);console.groupEnd()}},n.Session.prototype._send=function(e){if(!this._websocket_connected)throw"Autobahn not connected";switch(!0){case t.Prototype&&"undefined"==typeof top.root.__prototype_deleted:case"function"==typeof e.toJSON:e=e.toJSON();break;default:e=JSON.stringify(e)}this._websocket.send(e),this._txcnt+=1,n._debugws&&(console.group("WS Send"),console.info(this._wsuri+"  ["+this._session_id+"]"),console.log(this._txcnt),console.log(e),console.groupEnd())},n.Session.prototype.close=function(){this._websocket_connected&&this._websocket.close()},n.Session.prototype.sessionid=function(){return this._session_id},n.Session.prototype.wsuri=function(){return this._wsuri},n.Session.prototype.shrink=function(t,e){return void 0===e&&(e=!0),this._prefixes.shrink(t,e)},n.Session.prototype.resolve=function(t,e){return void 0===e&&(e=!0),this._prefixes.resolve(t,e)},n.Session.prototype.prefix=function(t,e){this._prefixes.set(t,e),(n._debugrpc||n._debugpubsub)&&(console.group("WAMP Prefix"),console.info(this._wsuri+"  ["+this._session_id+"]"),console.log(t),console.log(e),console.groupEnd()),this._send([n._MESSAGE_TYPEID_PREFIX,t,e])},n.Session.prototype.call=function(){for(var t,e=new n.Deferred;t=n._newidFast(),!!(t in this._calls););this._calls[t]=e;for(var i=this._prefixes.shrink(arguments[0],!0),i=[n._MESSAGE_TYPEID_CALL,t,i],r=1;r<arguments.length;r+=1)i.push(arguments[r]);return this._send(i),n._debugrpc&&(e._ab_callobj=i,e._ab_tid=this._wsuri+"  ["+this._session_id+"]["+t+"]",console.time(e._ab_tid),console.info()),e.promise.then?e.promise:e},n.Session.prototype.subscribe=function(t,e){var i=this._prefixes.resolve(t,!0);if(i in this._subscriptions||(n._debugpubsub&&(console.group("WAMP Subscribe"),console.info(this._wsuri+"  ["+this._session_id+"]"),console.log(t),console.log(e),console.groupEnd()),this._send([n._MESSAGE_TYPEID_SUBSCRIBE,t]),this._subscriptions[i]=[]),-1!==this._subscriptions[i].indexOf(e))throw"callback "+e+" already subscribed for topic "+i;this._subscriptions[i].push(e)},n.Session.prototype.unsubscribe=function(t,e){var i=this._prefixes.resolve(t,!0);if(!(i in this._subscriptions))throw"not subscribed to topic "+i;var r;if(void 0!==e){var o=this._subscriptions[i].indexOf(e);if(-1===o)throw"no callback "+e+" subscribed on topic "+i;r=e,this._subscriptions[i].splice(o,1)}else r=this._subscriptions[i].slice(),this._subscriptions[i]=[];0===this._subscriptions[i].length&&(delete this._subscriptions[i],n._debugpubsub&&(console.group("WAMP Unsubscribe"),console.info(this._wsuri+"  ["+this._session_id+"]"),console.log(t),console.log(r),console.groupEnd()),this._send([n._MESSAGE_TYPEID_UNSUBSCRIBE,t]))},n.Session.prototype.publish=function(){var t=arguments[0],e=arguments[1],i=null,r=null,o=null,a=null;if(3<arguments.length){if(!(arguments[2]instanceof Array))throw"invalid argument type(s)";if(!(arguments[3]instanceof Array))throw"invalid argument type(s)";r=arguments[2],o=arguments[3],a=[n._MESSAGE_TYPEID_PUBLISH,t,e,r,o]}else if(2<arguments.length)if("boolean"==typeof arguments[2])i=arguments[2],a=[n._MESSAGE_TYPEID_PUBLISH,t,e,i];else{if(!(arguments[2]instanceof Array))throw"invalid argument type(s)";r=arguments[2],a=[n._MESSAGE_TYPEID_PUBLISH,t,e,r]}else a=[n._MESSAGE_TYPEID_PUBLISH,t,e];n._debugpubsub&&(console.group("WAMP Publish"),console.info(this._wsuri+"  ["+this._session_id+"]"),console.log(t),console.log(e),null!==i?console.log(i):null!==r&&(console.log(r),null!==o&&console.log(o)),console.groupEnd()),this._send(a)},n.Session.prototype.authreq=function(t,e){return this.call("http://api.wamp.ws/procedure#authreq",t,e)},n.Session.prototype.authsign=function(t,e){return e||(e=""),CryptoJS.HmacSHA256(t,e).toString(CryptoJS.enc.Base64)},n.Session.prototype.auth=function(t){return this.call("http://api.wamp.ws/procedure#auth",t)},n._connect=function(e){var i=new n.Session(e.wsuri,function(){e.connects+=1,e.retryCount=0,e.onConnect(i)
},function(i,r){var o=null;switch(i){case n.CONNECTION_CLOSED:e.onHangup(i,"Connection was closed properly ["+r+"]");break;case n.CONNECTION_UNSUPPORTED:e.onHangup(i,"Browser does not support WebSocket.");break;case n.CONNECTION_UNREACHABLE:e.retryCount+=1,0===e.connects?e.onHangup(i,"Connection could not be established."):e.retryCount<=e.options.maxRetries?(o=e.onHangup(n.CONNECTION_UNREACHABLE_SCHEDULED_RECONNECT,"Connection unreachable - scheduled reconnect to occur in "+e.options.retryDelay/1e3+" second(s) - attempt "+e.retryCount+" of "+e.options.maxRetries+".",{delay:e.options.retryDelay,retries:e.retryCount,maxretries:e.options.maxRetries}))?(n._debugconnect&&console.log("Connection unreachable - retrying stopped by app"),e.onHangup(n.CONNECTION_RETRIES_EXCEEDED,"Number of connection retries exceeded.")):(n._debugconnect&&console.log("Connection unreachable - retrying ("+e.retryCount+") .."),t.setTimeout(function(){n._connect(e)},e.options.retryDelay)):e.onHangup(n.CONNECTION_RETRIES_EXCEEDED,"Number of connection retries exceeded.");break;case n.CONNECTION_LOST:e.retryCount+=1,e.retryCount<=e.options.maxRetries?(o=e.onHangup(n.CONNECTION_LOST_SCHEDULED_RECONNECT,"Connection lost - scheduled "+e.retryCount+"th reconnect to occur in "+e.options.retryDelay/1e3+" second(s).",{delay:e.options.retryDelay,retries:e.retryCount,maxretries:e.options.maxRetries}))?(n._debugconnect&&console.log("Connection lost - retrying stopped by app"),e.onHangup(n.CONNECTION_RETRIES_EXCEEDED,"Connection lost.")):(n._debugconnect&&console.log("Connection lost - retrying ("+e.retryCount+") .."),t.setTimeout(function(){n._connect(e)},e.options.retryDelay)):e.onHangup(n.CONNECTION_RETRIES_EXCEEDED,"Connection lost.");break;default:throw"unhandled close code in ab._connect"}},e.options)},n.connect=function(t,e,i,r){var o={};if(o.wsuri=t,o.options=r?r:{},void 0===o.options.retryDelay&&(o.options.retryDelay=5e3),void 0===o.options.maxRetries&&(o.options.maxRetries=10),void 0===o.options.skipSubprotocolCheck&&(o.options.skipSubprotocolCheck=!1),void 0===o.options.skipSubprotocolAnnounce&&(o.options.skipSubprotocolAnnounce=!1),!e)throw"onConnect handler required!";o.onConnect=e,o.onHangup=i?i:function(t,e,i){n._debugconnect&&console.log(t,e,i)},o.connects=0,o.retryCount=0,n._connect(o)},n.launch=function(t,e,i){n.connect(t.wsuri,function(i){t.appkey&&""!==t.appkey?i.authreq(t.appkey,t.appextra).then(function(r){var o=null;"function"==typeof t.appsecret?o=t.appsecret(r):(o=n.deriveKey(t.appsecret,JSON.parse(r).authextra),o=i.authsign(r,o)),i.auth(o).then(function(){e?e(i):n._debugconnect&&i.log("Session opened.")},i.log)},i.log):i.authreq().then(function(){i.auth().then(function(){e?e(i):n._debugconnect&&i.log("Session opened.")},i.log)},i.log)},function(t,e,r){i?i(t,e,r):n._debugconnect&&n.log("Session closed.",t,e,r)},t.sessionConfig)},n}),ab._UA_FIREFOX=/.*Firefox\/([0-9+]*).*/,ab._UA_CHROME=/.*Chrome\/([0-9+]*).*/,ab._UA_CHROMEFRAME=/.*chromeframe\/([0-9]*).*/,ab._UA_WEBKIT=/.*AppleWebKit\/([0-9+.]*)w*.*/,ab._UA_WEBOS=/.*webOS\/([0-9+.]*)w*.*/,ab._matchRegex=function(t,e){var n=e.exec(t);return n?n[1]:n},ab.lookupWsSupport=function(){var t=navigator.userAgent;if(-1<t.indexOf("MSIE")){if(-1<t.indexOf("MSIE 10"))return[!0,!0,!0];if(-1<t.indexOf("chromeframe")){var e=parseInt(ab._matchRegex(t,ab._UA_CHROMEFRAME));return e>=14?[!0,!1,!0]:[!1,!1,!1]}if(-1<t.indexOf("MSIE 8")||-1<t.indexOf("MSIE 9"))return[!0,!0,!0]}else{if(-1<t.indexOf("Firefox")){if(e=parseInt(ab._matchRegex(t,ab._UA_FIREFOX))){if(e>=7)return[!0,!1,!0];if(e>=3)return[!0,!0,!0]}return[!1,!1,!0]}if(-1<t.indexOf("Safari")&&-1==t.indexOf("Chrome")){if(e=ab._matchRegex(t,ab._UA_WEBKIT))return-1<t.indexOf("Windows")&&"534+"==e||-1<t.indexOf("Macintosh")&&(e=e.replace("+","").split("."),535==parseInt(e[0])&&24<=parseInt(e[1])||535<parseInt(e[0]))?[!0,!1,!0]:-1<t.indexOf("webOS")?(e=ab._matchRegex(t,ab._UA_WEBOS).split("."),2==parseInt(e[0])?[!1,!0,!0]:[!1,!1,!1]):[!0,!0,!0]}else if(-1<t.indexOf("Chrome")){if(e=parseInt(ab._matchRegex(t,ab._UA_CHROME)))return e>=14?[!0,!1,!0]:e>=4?[!0,!0,!0]:[!1,!1,!0]}else if(-1<t.indexOf("Android")){if(-1<t.indexOf("Firefox")||-1<t.indexOf("CrMo"))return[!0,!1,!0];if(-1<t.indexOf("Opera"))return[!1,!1,!0];if(-1<t.indexOf("CrMo"))return[!0,!0,!0]}else if(-1<t.indexOf("iPhone")||-1<t.indexOf("iPad")||-1<t.indexOf("iPod"))return[!1,!1,!0]}return[!1,!1,!1]},(window._gsQueue||(window._gsQueue=[])).push(function(){"use strict";window._gsDefine("TweenMax",["core.Animation","core.SimpleTimeline","TweenLite"],function(t,e,n){var i=[].slice,r=function(t,e,i){n.call(this,t,e,i),this._cycle=0,this._yoyo=this.vars.yoyo===!0,this._repeat=this.vars.repeat||0,this._repeatDelay=this.vars.repeatDelay||0,this._dirty=!0,this.render=r.prototype.render},o=1e-10,a=n._internals.isSelector,s=n._internals.isArray,l=r.prototype=n.to({},.1,{}),u=[];r.version="1.11.8",l.constructor=r,l.kill()._gc=!1,r.killTweensOf=r.killDelayedCallsTo=n.killTweensOf,r.getTweensOf=n.getTweensOf,r.ticker=n.ticker,l.invalidate=function(){return this._yoyo=this.vars.yoyo===!0,this._repeat=this.vars.repeat||0,this._repeatDelay=this.vars.repeatDelay||0,this._uncache(!0),n.prototype.invalidate.call(this)},l.updateTo=function(t,e){var i,r=this.ratio;e&&this._startTime<this._timeline._time&&(this._startTime=this._timeline._time,this._uncache(!1),this._gc?this._enabled(!0,!1):this._timeline.insert(this,this._startTime-this._delay));for(i in t)this.vars[i]=t[i];if(this._initted)if(e)this._initted=!1;else if(this._gc&&this._enabled(!0,!1),this._notifyPluginsOfEnabled&&this._firstPT&&n._onPluginEvent("_onDisable",this),this._time/this._duration>.998){var o=this._time;this.render(0,!0,!1),this._initted=!1,this.render(o,!0,!1)}else if(this._time>0){this._initted=!1,this._init();for(var a,s=1/(1-r),l=this._firstPT;l;)a=l.s+l.c,l.c*=s,l.s=a-l.c,l=l._next}return this},l.render=function(t,e,n){this._initted||0===this._duration&&this.vars.repeat&&this.invalidate();var i,r,a,s,l,c,f,p,h=this._dirty?this.totalDuration():this._totalDuration,d=this._time,m=this._totalTime,g=this._cycle,_=this._duration;if(t>=h?(this._totalTime=h,this._cycle=this._repeat,this._yoyo&&0!==(1&this._cycle)?(this._time=0,this.ratio=this._ease._calcEnd?this._ease.getRatio(0):0):(this._time=_,this.ratio=this._ease._calcEnd?this._ease.getRatio(1):1),this._reversed||(i=!0,r="onComplete"),0===_&&(p=this._rawPrevTime,this._startTime===this._timeline._duration&&(t=0),(0===t||0>p||p===o)&&p!==t&&(n=!0,p>o&&(r="onReverseComplete")),this._rawPrevTime=p=!e||t||this._rawPrevTime===t?t:o)):1e-7>t?(this._totalTime=this._time=this._cycle=0,this.ratio=this._ease._calcEnd?this._ease.getRatio(0):0,(0!==m||0===_&&this._rawPrevTime>0&&this._rawPrevTime!==o)&&(r="onReverseComplete",i=this._reversed),0>t?(this._active=!1,0===_&&(this._rawPrevTime>=0&&(n=!0),this._rawPrevTime=p=!e||t||this._rawPrevTime===t?t:o)):this._initted||(n=!0)):(this._totalTime=this._time=t,0!==this._repeat&&(s=_+this._repeatDelay,this._cycle=this._totalTime/s>>0,0!==this._cycle&&this._cycle===this._totalTime/s&&this._cycle--,this._time=this._totalTime-this._cycle*s,this._yoyo&&0!==(1&this._cycle)&&(this._time=_-this._time),this._time>_?this._time=_:0>this._time&&(this._time=0)),this._easeType?(l=this._time/_,c=this._easeType,f=this._easePower,(1===c||3===c&&l>=.5)&&(l=1-l),3===c&&(l*=2),1===f?l*=l:2===f?l*=l*l:3===f?l*=l*l*l:4===f&&(l*=l*l*l*l),this.ratio=1===c?1-l:2===c?l:.5>this._time/_?l/2:1-l/2):this.ratio=this._ease.getRatio(this._time/_)),d===this._time&&!n&&g===this._cycle)return void(m!==this._totalTime&&this._onUpdate&&(e||this._onUpdate.apply(this.vars.onUpdateScope||this,this.vars.onUpdateParams||u)));if(!this._initted){if(this._init(),!this._initted||this._gc)return;this._time&&!i?this.ratio=this._ease.getRatio(this._time/_):i&&this._ease._calcEnd&&(this.ratio=this._ease.getRatio(0===this._time?0:1))}for(this._active||!this._paused&&this._time!==d&&t>=0&&(this._active=!0),0===m&&(this._startAt&&(t>=0?this._startAt.render(t,e,n):r||(r="_dummyGS")),this.vars.onStart&&(0!==this._totalTime||0===_)&&(e||this.vars.onStart.apply(this.vars.onStartScope||this,this.vars.onStartParams||u))),a=this._firstPT;a;)a.f?a.t[a.p](a.c*this.ratio+a.s):a.t[a.p]=a.c*this.ratio+a.s,a=a._next;this._onUpdate&&(0>t&&this._startAt&&this._startTime&&this._startAt.render(t,e,n),e||(this._totalTime!==m||i)&&this._onUpdate.apply(this.vars.onUpdateScope||this,this.vars.onUpdateParams||u)),this._cycle!==g&&(e||this._gc||this.vars.onRepeat&&this.vars.onRepeat.apply(this.vars.onRepeatScope||this,this.vars.onRepeatParams||u)),r&&(this._gc||(0>t&&this._startAt&&!this._onUpdate&&this._startTime&&this._startAt.render(t,e,n),i&&(this._timeline.autoRemoveChildren&&this._enabled(!1,!1),this._active=!1),!e&&this.vars[r]&&this.vars[r].apply(this.vars[r+"Scope"]||this,this.vars[r+"Params"]||u),0===_&&this._rawPrevTime===o&&p!==o&&(this._rawPrevTime=0)))},r.to=function(t,e,n){return new r(t,e,n)},r.from=function(t,e,n){return n.runBackwards=!0,n.immediateRender=0!=n.immediateRender,new r(t,e,n)},r.fromTo=function(t,e,n,i){return i.startAt=n,i.immediateRender=0!=i.immediateRender&&0!=n.immediateRender,new r(t,e,i)},r.staggerTo=r.allTo=function(t,e,o,l,c,f,p){l=l||0;var h,d,m,g,_=o.delay||0,v=[],y=function(){o.onComplete&&o.onComplete.apply(o.onCompleteScope||this,arguments),c.apply(p||this,f||u)};for(s(t)||("string"==typeof t&&(t=n.selector(t)||t),a(t)&&(t=i.call(t,0))),h=t.length,m=0;h>m;m++){d={};for(g in o)d[g]=o[g];d.delay=_,m===h-1&&c&&(d.onComplete=y),v[m]=new r(t[m],e,d),_+=l}return v},r.staggerFrom=r.allFrom=function(t,e,n,i,o,a,s){return n.runBackwards=!0,n.immediateRender=0!=n.immediateRender,r.staggerTo(t,e,n,i,o,a,s)},r.staggerFromTo=r.allFromTo=function(t,e,n,i,o,a,s,l){return i.startAt=n,i.immediateRender=0!=i.immediateRender&&0!=n.immediateRender,r.staggerTo(t,e,i,o,a,s,l)},r.delayedCall=function(t,e,n,i,o){return new r(e,0,{delay:t,onComplete:e,onCompleteParams:n,onCompleteScope:i,onReverseComplete:e,onReverseCompleteParams:n,onReverseCompleteScope:i,immediateRender:!1,useFrames:o,overwrite:0})},r.set=function(t,e){return new r(t,0,e)},r.isTweening=function(t){return n.getTweensOf(t,!0).length>0};var c=function(t,e){for(var i=[],r=0,o=t._first;o;)o instanceof n?i[r++]=o:(e&&(i[r++]=o),i=i.concat(c(o,e)),r=i.length),o=o._next;return i},f=r.getAllTweens=function(e){return c(t._rootTimeline,e).concat(c(t._rootFramesTimeline,e))};r.killAll=function(t,n,i,r){null==n&&(n=!0),null==i&&(i=!0);var o,a,s,l=f(0!=r),u=l.length,c=n&&i&&r;for(s=0;u>s;s++)a=l[s],(c||a instanceof e||(o=a.target===a.vars.onComplete)&&i||n&&!o)&&(t?a.totalTime(a.totalDuration()):a._enabled(!1,!1))},r.killChildTweensOf=function(t,e){if(null!=t){var o,l,u,c,f,p=n._tweenLookup;if("string"==typeof t&&(t=n.selector(t)||t),a(t)&&(t=i.call(t,0)),s(t))for(c=t.length;--c>-1;)r.killChildTweensOf(t[c],e);else{o=[];for(u in p)for(l=p[u].target.parentNode;l;)l===t&&(o=o.concat(p[u].tweens)),l=l.parentNode;for(f=o.length,c=0;f>c;c++)e&&o[c].totalTime(o[c].totalDuration()),o[c]._enabled(!1,!1)}}};var p=function(t,n,i,r){n=n!==!1,i=i!==!1,r=r!==!1;for(var o,a,s=f(r),l=n&&i&&r,u=s.length;--u>-1;)a=s[u],(l||a instanceof e||(o=a.target===a.vars.onComplete)&&i||n&&!o)&&a.paused(t)};return r.pauseAll=function(t,e,n){p(!0,t,e,n)},r.resumeAll=function(t,e,n){p(!1,t,e,n)},r.globalTimeScale=function(e){var i=t._rootTimeline,r=n.ticker.time;return arguments.length?(e=e||o,i._startTime=r-(r-i._startTime)*i._timeScale/e,i=t._rootFramesTimeline,r=n.ticker.frame,i._startTime=r-(r-i._startTime)*i._timeScale/e,i._timeScale=t._rootTimeline._timeScale=e,e):i._timeScale},l.progress=function(t){return arguments.length?this.totalTime(this.duration()*(this._yoyo&&0!==(1&this._cycle)?1-t:t)+this._cycle*(this._duration+this._repeatDelay),!1):this._time/this.duration()},l.totalProgress=function(t){return arguments.length?this.totalTime(this.totalDuration()*t,!1):this._totalTime/this.totalDuration()},l.time=function(t,e){return arguments.length?(this._dirty&&this.totalDuration(),t>this._duration&&(t=this._duration),this._yoyo&&0!==(1&this._cycle)?t=this._duration-t+this._cycle*(this._duration+this._repeatDelay):0!==this._repeat&&(t+=this._cycle*(this._duration+this._repeatDelay)),this.totalTime(t,e)):this._time},l.duration=function(e){return arguments.length?t.prototype.duration.call(this,e):this._duration},l.totalDuration=function(t){return arguments.length?-1===this._repeat?this:this.duration((t-this._repeat*this._repeatDelay)/(this._repeat+1)):(this._dirty&&(this._totalDuration=-1===this._repeat?999999999999:this._duration*(this._repeat+1)+this._repeatDelay*this._repeat,this._dirty=!1),this._totalDuration)},l.repeat=function(t){return arguments.length?(this._repeat=t,this._uncache(!0)):this._repeat},l.repeatDelay=function(t){return arguments.length?(this._repeatDelay=t,this._uncache(!0)):this._repeatDelay},l.yoyo=function(t){return arguments.length?(this._yoyo=t,this):this._yoyo},r},!0),window._gsDefine("TimelineLite",["core.Animation","core.SimpleTimeline","TweenLite"],function(t,e,n){var i=function(t){e.call(this,t),this._labels={},this.autoRemoveChildren=this.vars.autoRemoveChildren===!0,this.smoothChildTiming=this.vars.smoothChildTiming===!0,this._sortChildren=!0,this._onUpdate=this.vars.onUpdate;var n,i,r=this.vars;for(i in r)n=r[i],a(n)&&-1!==n.join("").indexOf("{self}")&&(r[i]=this._swapSelfInParams(n));a(r.tweens)&&this.add(r.tweens,0,r.align,r.stagger)},r=1e-10,o=n._internals.isSelector,a=n._internals.isArray,s=[],l=window._gsDefine.globals,u=function(t){var e,n={};for(e in t)n[e]=t[e];return n},c=function(t,e,n,i){t._timeline.pause(t._startTime),e&&e.apply(i||t._timeline,n||s)},f=s.slice,p=i.prototype=new e;return i.version="1.11.8",p.constructor=i,p.kill()._gc=!1,p.to=function(t,e,i,r){var o=i.repeat&&l.TweenMax||n;return e?this.add(new o(t,e,i),r):this.set(t,i,r)},p.from=function(t,e,i,r){return this.add((i.repeat&&l.TweenMax||n).from(t,e,i),r)},p.fromTo=function(t,e,i,r,o){var a=r.repeat&&l.TweenMax||n;return e?this.add(a.fromTo(t,e,i,r),o):this.set(t,r,o)},p.staggerTo=function(t,e,r,a,s,l,c,p){var h,d=new i({onComplete:l,onCompleteParams:c,onCompleteScope:p,smoothChildTiming:this.smoothChildTiming});for("string"==typeof t&&(t=n.selector(t)||t),o(t)&&(t=f.call(t,0)),a=a||0,h=0;t.length>h;h++)r.startAt&&(r.startAt=u(r.startAt)),d.to(t[h],e,u(r),h*a);return this.add(d,s)},p.staggerFrom=function(t,e,n,i,r,o,a,s){return n.immediateRender=0!=n.immediateRender,n.runBackwards=!0,this.staggerTo(t,e,n,i,r,o,a,s)},p.staggerFromTo=function(t,e,n,i,r,o,a,s,l){return i.startAt=n,i.immediateRender=0!=i.immediateRender&&0!=n.immediateRender,this.staggerTo(t,e,i,r,o,a,s,l)},p.call=function(t,e,i,r){return this.add(n.delayedCall(0,t,e,i),r)},p.set=function(t,e,i){return i=this._parseTimeOrLabel(i,0,!0),null==e.immediateRender&&(e.immediateRender=i===this._time&&!this._paused),this.add(new n(t,0,e),i)},i.exportRoot=function(t,e){t=t||{},null==t.smoothChildTiming&&(t.smoothChildTiming=!0);var r,o,a=new i(t),s=a._timeline;for(null==e&&(e=!0),s._remove(a,!0),a._startTime=0,a._rawPrevTime=a._time=a._totalTime=s._time,r=s._first;r;)o=r._next,e&&r instanceof n&&r.target===r.vars.onComplete||a.add(r,r._startTime-r._delay),r=o;return s.add(a,0),a},p.add=function(r,o,s,l){var u,c,f,p,h,d;if("number"!=typeof o&&(o=this._parseTimeOrLabel(o,0,!0,r)),!(r instanceof t)){if(r instanceof Array||r&&r.push&&a(r)){for(s=s||"normal",l=l||0,u=o,c=r.length,f=0;c>f;f++)a(p=r[f])&&(p=new i({tweens:p})),this.add(p,u),"string"!=typeof p&&"function"!=typeof p&&("sequence"===s?u=p._startTime+p.totalDuration()/p._timeScale:"start"===s&&(p._startTime-=p.delay())),u+=l;return this._uncache(!0)}if("string"==typeof r)return this.addLabel(r,o);if("function"!=typeof r)throw"Cannot add "+r+" into the timeline; it is not a tween, timeline, function, or string.";r=n.delayedCall(0,r)}if(e.prototype.add.call(this,r,o),(this._gc||this._time===this._duration)&&!this._paused&&this._duration<this.duration())for(h=this,d=h.rawTime()>r._startTime;h._timeline;)d&&h._timeline.smoothChildTiming?h.totalTime(h._totalTime,!0):h._gc&&h._enabled(!0,!1),h=h._timeline;return this},p.remove=function(e){if(e instanceof t)return this._remove(e,!1);if(e instanceof Array||e&&e.push&&a(e)){for(var n=e.length;--n>-1;)this.remove(e[n]);return this}return"string"==typeof e?this.removeLabel(e):this.kill(null,e)},p._remove=function(t,n){e.prototype._remove.call(this,t,n);var i=this._last;return i?this._time>i._startTime+i._totalDuration/i._timeScale&&(this._time=this.duration(),this._totalTime=this._totalDuration):this._time=this._totalTime=this._duration=this._totalDuration=0,this},p.append=function(t,e){return this.add(t,this._parseTimeOrLabel(null,e,!0,t))},p.insert=p.insertMultiple=function(t,e,n,i){return this.add(t,e||0,n,i)},p.appendMultiple=function(t,e,n,i){return this.add(t,this._parseTimeOrLabel(null,e,!0,t),n,i)},p.addLabel=function(t,e){return this._labels[t]=this._parseTimeOrLabel(e),this},p.addPause=function(t,e,n,i){return this.call(c,["{self}",e,n,i],this,t)},p.removeLabel=function(t){return delete this._labels[t],this},p.getLabelTime=function(t){return null!=this._labels[t]?this._labels[t]:-1},p._parseTimeOrLabel=function(e,n,i,r){var o;if(r instanceof t&&r.timeline===this)this.remove(r);else if(r&&(r instanceof Array||r.push&&a(r)))for(o=r.length;--o>-1;)r[o]instanceof t&&r[o].timeline===this&&this.remove(r[o]);if("string"==typeof n)return this._parseTimeOrLabel(n,i&&"number"==typeof e&&null==this._labels[n]?e-this.duration():0,i);if(n=n||0,"string"!=typeof e||!isNaN(e)&&null==this._labels[e])null==e&&(e=this.duration());else{if(o=e.indexOf("="),-1===o)return null==this._labels[e]?i?this._labels[e]=this.duration()+n:n:this._labels[e]+n;n=parseInt(e.charAt(o-1)+"1",10)*Number(e.substr(o+1)),e=o>1?this._parseTimeOrLabel(e.substr(0,o-1),0,i):this.duration()}return Number(e)+n},p.seek=function(t,e){return this.totalTime("number"==typeof t?t:this._parseTimeOrLabel(t),e!==!1)},p.stop=function(){return this.paused(!0)},p.gotoAndPlay=function(t,e){return this.play(t,e)},p.gotoAndStop=function(t,e){return this.pause(t,e)},p.render=function(t,e,n){this._gc&&this._enabled(!0,!1);var i,o,a,l,u,c=this._dirty?this.totalDuration():this._totalDuration,f=this._time,p=this._startTime,h=this._timeScale,d=this._paused;if(t>=c?(this._totalTime=this._time=c,this._reversed||this._hasPausedChild()||(o=!0,l="onComplete",0===this._duration&&(0===t||0>this._rawPrevTime||this._rawPrevTime===r)&&this._rawPrevTime!==t&&this._first&&(u=!0,this._rawPrevTime>r&&(l="onReverseComplete"))),this._rawPrevTime=this._duration||!e||t||this._rawPrevTime===t?t:r,t=c+1e-4):1e-7>t?(this._totalTime=this._time=0,(0!==f||0===this._duration&&this._rawPrevTime!==r&&(this._rawPrevTime>0||0>t&&this._rawPrevTime>=0))&&(l="onReverseComplete",o=this._reversed),0>t?(this._active=!1,0===this._duration&&this._rawPrevTime>=0&&this._first&&(u=!0),this._rawPrevTime=t):(this._rawPrevTime=this._duration||!e||t||this._rawPrevTime===t?t:r,t=0,this._initted||(u=!0))):this._totalTime=this._time=this._rawPrevTime=t,this._time!==f&&this._first||n||u){if(this._initted||(this._initted=!0),this._active||!this._paused&&this._time!==f&&t>0&&(this._active=!0),0===f&&this.vars.onStart&&0!==this._time&&(e||this.vars.onStart.apply(this.vars.onStartScope||this,this.vars.onStartParams||s)),this._time>=f)for(i=this._first;i&&(a=i._next,!this._paused||d);)(i._active||i._startTime<=this._time&&!i._paused&&!i._gc)&&(i._reversed?i.render((i._dirty?i.totalDuration():i._totalDuration)-(t-i._startTime)*i._timeScale,e,n):i.render((t-i._startTime)*i._timeScale,e,n)),i=a;else for(i=this._last;i&&(a=i._prev,!this._paused||d);)(i._active||f>=i._startTime&&!i._paused&&!i._gc)&&(i._reversed?i.render((i._dirty?i.totalDuration():i._totalDuration)-(t-i._startTime)*i._timeScale,e,n):i.render((t-i._startTime)*i._timeScale,e,n)),i=a;this._onUpdate&&(e||this._onUpdate.apply(this.vars.onUpdateScope||this,this.vars.onUpdateParams||s)),l&&(this._gc||(p===this._startTime||h!==this._timeScale)&&(0===this._time||c>=this.totalDuration())&&(o&&(this._timeline.autoRemoveChildren&&this._enabled(!1,!1),this._active=!1),!e&&this.vars[l]&&this.vars[l].apply(this.vars[l+"Scope"]||this,this.vars[l+"Params"]||s)))}},p._hasPausedChild=function(){for(var t=this._first;t;){if(t._paused||t instanceof i&&t._hasPausedChild())return!0;t=t._next}return!1},p.getChildren=function(t,e,i,r){r=r||-9999999999;for(var o=[],a=this._first,s=0;a;)r>a._startTime||(a instanceof n?e!==!1&&(o[s++]=a):(i!==!1&&(o[s++]=a),t!==!1&&(o=o.concat(a.getChildren(!0,e,i)),s=o.length))),a=a._next;return o},p.getTweensOf=function(t,e){for(var i=n.getTweensOf(t),r=i.length,o=[],a=0;--r>-1;)(i[r].timeline===this||e&&this._contains(i[r]))&&(o[a++]=i[r]);return o},p._contains=function(t){for(var e=t.timeline;e;){if(e===this)return!0;e=e.timeline}return!1},p.shiftChildren=function(t,e,n){n=n||0;for(var i,r=this._first,o=this._labels;r;)r._startTime>=n&&(r._startTime+=t),r=r._next;if(e)for(i in o)o[i]>=n&&(o[i]+=t);return this._uncache(!0)},p._kill=function(t,e){if(!t&&!e)return this._enabled(!1,!1);for(var n=e?this.getTweensOf(e):this.getChildren(!0,!0,!1),i=n.length,r=!1;--i>-1;)n[i]._kill(t,e)&&(r=!0);return r},p.clear=function(t){var e=this.getChildren(!1,!0,!0),n=e.length;for(this._time=this._totalTime=0;--n>-1;)e[n]._enabled(!1,!1);return t!==!1&&(this._labels={}),this._uncache(!0)},p.invalidate=function(){for(var t=this._first;t;)t.invalidate(),t=t._next;return this},p._enabled=function(t,n){if(t===this._gc)for(var i=this._first;i;)i._enabled(t,!0),i=i._next;return e.prototype._enabled.call(this,t,n)},p.duration=function(t){return arguments.length?(0!==this.duration()&&0!==t&&this.timeScale(this._duration/t),this):(this._dirty&&this.totalDuration(),this._duration)},p.totalDuration=function(t){if(!arguments.length){if(this._dirty){for(var e,n,i=0,r=this._last,o=999999999999;r;)e=r._prev,r._dirty&&r.totalDuration(),r._startTime>o&&this._sortChildren&&!r._paused?this.add(r,r._startTime-r._delay):o=r._startTime,0>r._startTime&&!r._paused&&(i-=r._startTime,this._timeline.smoothChildTiming&&(this._startTime+=r._startTime/this._timeScale),this.shiftChildren(-r._startTime,!1,-9999999999),o=0),n=r._startTime+r._totalDuration/r._timeScale,n>i&&(i=n),r=e;this._duration=this._totalDuration=i,this._dirty=!1}return this._totalDuration}return 0!==this.totalDuration()&&0!==t&&this.timeScale(this._totalDuration/t),this},p.usesFrames=function(){for(var e=this._timeline;e._timeline;)e=e._timeline;return e===t._rootFramesTimeline},p.rawTime=function(){return this._paused?this._totalTime:(this._timeline.rawTime()-this._startTime)*this._timeScale},i},!0),window._gsDefine("TimelineMax",["TimelineLite","TweenLite","easing.Ease"],function(t,e,n){var i=function(e){t.call(this,e),this._repeat=this.vars.repeat||0,this._repeatDelay=this.vars.repeatDelay||0,this._cycle=0,this._yoyo=this.vars.yoyo===!0,this._dirty=!0},r=1e-10,o=[],a=new n(null,null,1,0),s=i.prototype=new t;return s.constructor=i,s.kill()._gc=!1,i.version="1.11.8",s.invalidate=function(){return this._yoyo=this.vars.yoyo===!0,this._repeat=this.vars.repeat||0,this._repeatDelay=this.vars.repeatDelay||0,this._uncache(!0),t.prototype.invalidate.call(this)},s.addCallback=function(t,n,i,r){return this.add(e.delayedCall(0,t,i,r),n)},s.removeCallback=function(t,e){if(t)if(null==e)this._kill(null,t);else for(var n=this.getTweensOf(t,!1),i=n.length,r=this._parseTimeOrLabel(e);--i>-1;)n[i]._startTime===r&&n[i]._enabled(!1,!1);return this},s.tweenTo=function(t,n){n=n||{};var i,r,s,l={ease:a,overwrite:n.delay?2:1,useFrames:this.usesFrames(),immediateRender:!1};for(r in n)l[r]=n[r];return l.time=this._parseTimeOrLabel(t),i=Math.abs(Number(l.time)-this._time)/this._timeScale||.001,s=new e(this,i,l),l.onStart=function(){s.target.paused(!0),s.vars.time!==s.target.time()&&i===s.duration()&&s.duration(Math.abs(s.vars.time-s.target.time())/s.target._timeScale),n.onStart&&n.onStart.apply(n.onStartScope||s,n.onStartParams||o)},s},s.tweenFromTo=function(t,e,n){n=n||{},t=this._parseTimeOrLabel(t),n.startAt={onComplete:this.seek,onCompleteParams:[t],onCompleteScope:this},n.immediateRender=n.immediateRender!==!1;var i=this.tweenTo(e,n);return i.duration(Math.abs(i.vars.time-t)/this._timeScale||.001)},s.render=function(t,e,n){this._gc&&this._enabled(!0,!1);var i,a,s,l,u,c,f=this._dirty?this.totalDuration():this._totalDuration,p=this._duration,h=this._time,d=this._totalTime,m=this._startTime,g=this._timeScale,_=this._rawPrevTime,v=this._paused,y=this._cycle;if(t>=f?(this._locked||(this._totalTime=f,this._cycle=this._repeat),this._reversed||this._hasPausedChild()||(a=!0,l="onComplete",0===this._duration&&(0===t||0>_||_===r)&&_!==t&&this._first&&(u=!0,_>r&&(l="onReverseComplete"))),this._rawPrevTime=this._duration||!e||t||this._rawPrevTime===t?t:r,this._yoyo&&0!==(1&this._cycle)?this._time=t=0:(this._time=p,t=p+1e-4)):1e-7>t?(this._locked||(this._totalTime=this._cycle=0),this._time=0,(0!==h||0===p&&_!==r&&(_>0||0>t&&_>=0)&&!this._locked)&&(l="onReverseComplete",a=this._reversed),0>t?(this._active=!1,0===p&&_>=0&&this._first&&(u=!0),this._rawPrevTime=t):(this._rawPrevTime=p||!e||t||this._rawPrevTime===t?t:r,t=0,this._initted||(u=!0))):(0===p&&0>_&&(u=!0),this._time=this._rawPrevTime=t,this._locked||(this._totalTime=t,0!==this._repeat&&(c=p+this._repeatDelay,this._cycle=this._totalTime/c>>0,0!==this._cycle&&this._cycle===this._totalTime/c&&this._cycle--,this._time=this._totalTime-this._cycle*c,this._yoyo&&0!==(1&this._cycle)&&(this._time=p-this._time),this._time>p?(this._time=p,t=p+1e-4):0>this._time?this._time=t=0:t=this._time))),this._cycle!==y&&!this._locked){var b=this._yoyo&&0!==(1&y),w=b===(this._yoyo&&0!==(1&this._cycle)),$=this._totalTime,x=this._cycle,T=this._rawPrevTime,S=this._time;if(this._totalTime=y*p,y>this._cycle?b=!b:this._totalTime+=p,this._time=h,this._rawPrevTime=0===p?_-1e-4:_,this._cycle=y,this._locked=!0,h=b?0:p,this.render(h,e,0===p),e||this._gc||this.vars.onRepeat&&this.vars.onRepeat.apply(this.vars.onRepeatScope||this,this.vars.onRepeatParams||o),w&&(h=b?p+1e-4:-1e-4,this.render(h,!0,!1)),this._locked=!1,this._paused&&!v)return;this._time=S,this._totalTime=$,this._cycle=x,this._rawPrevTime=T}if(!(this._time!==h&&this._first||n||u))return void(d!==this._totalTime&&this._onUpdate&&(e||this._onUpdate.apply(this.vars.onUpdateScope||this,this.vars.onUpdateParams||o)));if(this._initted||(this._initted=!0),this._active||!this._paused&&this._totalTime!==d&&t>0&&(this._active=!0),0===d&&this.vars.onStart&&0!==this._totalTime&&(e||this.vars.onStart.apply(this.vars.onStartScope||this,this.vars.onStartParams||o)),this._time>=h)for(i=this._first;i&&(s=i._next,!this._paused||v);)(i._active||i._startTime<=this._time&&!i._paused&&!i._gc)&&(i._reversed?i.render((i._dirty?i.totalDuration():i._totalDuration)-(t-i._startTime)*i._timeScale,e,n):i.render((t-i._startTime)*i._timeScale,e,n)),i=s;else for(i=this._last;i&&(s=i._prev,!this._paused||v);)(i._active||h>=i._startTime&&!i._paused&&!i._gc)&&(i._reversed?i.render((i._dirty?i.totalDuration():i._totalDuration)-(t-i._startTime)*i._timeScale,e,n):i.render((t-i._startTime)*i._timeScale,e,n)),i=s;this._onUpdate&&(e||this._onUpdate.apply(this.vars.onUpdateScope||this,this.vars.onUpdateParams||o)),l&&(this._locked||this._gc||(m===this._startTime||g!==this._timeScale)&&(0===this._time||f>=this.totalDuration())&&(a&&(this._timeline.autoRemoveChildren&&this._enabled(!1,!1),this._active=!1),!e&&this.vars[l]&&this.vars[l].apply(this.vars[l+"Scope"]||this,this.vars[l+"Params"]||o)))},s.getActive=function(t,e,n){null==t&&(t=!0),null==e&&(e=!0),null==n&&(n=!1);var i,r,o=[],a=this.getChildren(t,e,n),s=0,l=a.length;for(i=0;l>i;i++)r=a[i],r.isActive()&&(o[s++]=r);return o},s.getLabelAfter=function(t){t||0!==t&&(t=this._time);var e,n=this.getLabelsArray(),i=n.length;for(e=0;i>e;e++)if(n[e].time>t)return n[e].name;return null},s.getLabelBefore=function(t){null==t&&(t=this._time);for(var e=this.getLabelsArray(),n=e.length;--n>-1;)if(t>e[n].time)return e[n].name;return null},s.getLabelsArray=function(){var t,e=[],n=0;for(t in this._labels)e[n++]={time:this._labels[t],name:t};return e.sort(function(t,e){return t.time-e.time}),e},s.progress=function(t){return arguments.length?this.totalTime(this.duration()*(this._yoyo&&0!==(1&this._cycle)?1-t:t)+this._cycle*(this._duration+this._repeatDelay),!1):this._time/this.duration()},s.totalProgress=function(t){return arguments.length?this.totalTime(this.totalDuration()*t,!1):this._totalTime/this.totalDuration()},s.totalDuration=function(e){return arguments.length?-1===this._repeat?this:this.duration((e-this._repeat*this._repeatDelay)/(this._repeat+1)):(this._dirty&&(t.prototype.totalDuration.call(this),this._totalDuration=-1===this._repeat?999999999999:this._duration*(this._repeat+1)+this._repeatDelay*this._repeat),this._totalDuration)},s.time=function(t,e){return arguments.length?(this._dirty&&this.totalDuration(),t>this._duration&&(t=this._duration),this._yoyo&&0!==(1&this._cycle)?t=this._duration-t+this._cycle*(this._duration+this._repeatDelay):0!==this._repeat&&(t+=this._cycle*(this._duration+this._repeatDelay)),this.totalTime(t,e)):this._time},s.repeat=function(t){return arguments.length?(this._repeat=t,this._uncache(!0)):this._repeat},s.repeatDelay=function(t){return arguments.length?(this._repeatDelay=t,this._uncache(!0)):this._repeatDelay},s.yoyo=function(t){return arguments.length?(this._yoyo=t,this):this._yoyo},s.currentLabel=function(t){return arguments.length?this.seek(t,!0):this.getLabelBefore(this._time+1e-8)},i},!0),function(){var t=180/Math.PI,e=[],n=[],i=[],r={},o=function(t,e,n,i){this.a=t,this.b=e,this.c=n,this.d=i,this.da=i-t,this.ca=n-t,this.ba=e-t},a=",x,y,z,left,top,right,bottom,marginTop,marginLeft,marginRight,marginBottom,paddingLeft,paddingTop,paddingRight,paddingBottom,backgroundPosition,backgroundPosition_y,",s=function(t,e,n,i){var r={a:t},o={},a={},s={c:i},l=(t+e)/2,u=(e+n)/2,c=(n+i)/2,f=(l+u)/2,p=(u+c)/2,h=(p-f)/8;return r.b=l+(t-l)/4,o.b=f+h,r.c=o.a=(r.b+o.b)/2,o.c=a.a=(f+p)/2,a.b=p-h,s.b=c+(i-c)/4,a.c=s.a=(a.b+s.b)/2,[r,o,a,s]},l=function(t,r,o,a,l){var u,c,f,p,h,d,m,g,_,v,y,b,w,$=t.length-1,x=0,T=t[0].a;for(u=0;$>u;u++)h=t[x],c=h.a,f=h.d,p=t[x+1].d,l?(y=e[u],b=n[u],w=.25*(b+y)*r/(a?.5:i[u]||.5),d=f-(f-c)*(a?.5*r:0!==y?w/y:0),m=f+(p-f)*(a?.5*r:0!==b?w/b:0),g=f-(d+((m-d)*(3*y/(y+b)+.5)/4||0))):(d=f-.5*(f-c)*r,m=f+.5*(p-f)*r,g=f-(d+m)/2),d+=g,m+=g,h.c=_=d,h.b=0!==u?T:T=h.a+.6*(h.c-h.a),h.da=f-c,h.ca=_-c,h.ba=T-c,o?(v=s(c,T,_,f),t.splice(x,1,v[0],v[1],v[2],v[3]),x+=4):x++,T=m;h=t[x],h.b=T,h.c=T+.4*(h.d-T),h.da=h.d-h.a,h.ca=h.c-h.a,h.ba=T-h.a,o&&(v=s(h.a,T,h.c,h.d),t.splice(x,1,v[0],v[1],v[2],v[3]))},u=function(t,i,r,a){var s,l,u,c,f,p,h=[];if(a)for(t=[a].concat(t),l=t.length;--l>-1;)"string"==typeof(p=t[l][i])&&"="===p.charAt(1)&&(t[l][i]=a[i]+Number(p.charAt(0)+p.substr(2)));if(s=t.length-2,0>s)return h[0]=new o(t[0][i],0,0,t[-1>s?0:1][i]),h;for(l=0;s>l;l++)u=t[l][i],c=t[l+1][i],h[l]=new o(u,0,0,c),r&&(f=t[l+2][i],e[l]=(e[l]||0)+(c-u)*(c-u),n[l]=(n[l]||0)+(f-c)*(f-c));return h[l]=new o(t[l][i],0,0,t[l+1][i]),h},c=function(t,o,s,c,f,p){var h,d,m,g,_,v,y,b,w={},$=[],x=p||t[0];f="string"==typeof f?","+f+",":a,null==o&&(o=1);for(d in t[0])$.push(d);if(t.length>1){for(b=t[t.length-1],y=!0,h=$.length;--h>-1;)if(d=$[h],Math.abs(x[d]-b[d])>.05){y=!1;break}y&&(t=t.concat(),p&&t.unshift(p),t.push(t[1]),p=t[t.length-3])}for(e.length=n.length=i.length=0,h=$.length;--h>-1;)d=$[h],r[d]=-1!==f.indexOf(","+d+","),w[d]=u(t,d,r[d],p);for(h=e.length;--h>-1;)e[h]=Math.sqrt(e[h]),n[h]=Math.sqrt(n[h]);if(!c){for(h=$.length;--h>-1;)if(r[d])for(m=w[$[h]],v=m.length-1,g=0;v>g;g++)_=m[g+1].da/n[g]+m[g].da/e[g],i[g]=(i[g]||0)+_*_;for(h=i.length;--h>-1;)i[h]=Math.sqrt(i[h])}for(h=$.length,g=s?4:1;--h>-1;)d=$[h],m=w[d],l(m,o,s,c,r[d]),y&&(m.splice(0,g),m.splice(m.length-g,g));return w},f=function(t,e,n){e=e||"soft";var i,r,a,s,l,u,c,f,p,h,d,m={},g="cubic"===e?3:2,_="soft"===e,v=[];
if(_&&n&&(t=[n].concat(t)),null==t||g+1>t.length)throw"invalid Bezier data";for(p in t[0])v.push(p);for(u=v.length;--u>-1;){for(p=v[u],m[p]=l=[],h=0,f=t.length,c=0;f>c;c++)i=null==n?t[c][p]:"string"==typeof(d=t[c][p])&&"="===d.charAt(1)?n[p]+Number(d.charAt(0)+d.substr(2)):Number(d),_&&c>1&&f-1>c&&(l[h++]=(i+l[h-2])/2),l[h++]=i;for(f=h-g+1,h=0,c=0;f>c;c+=g)i=l[c],r=l[c+1],a=l[c+2],s=2===g?0:l[c+3],l[h++]=d=3===g?new o(i,r,a,s):new o(i,(2*r+i)/3,(2*r+a)/3,a);l.length=h}return m},p=function(t,e,n){for(var i,r,o,a,s,l,u,c,f,p,h,d=1/n,m=t.length;--m>-1;)for(p=t[m],o=p.a,a=p.d-o,s=p.c-o,l=p.b-o,i=r=0,c=1;n>=c;c++)u=d*c,f=1-u,i=r-(r=(u*u*a+3*f*(u*s+f*l))*u),h=m*n+c-1,e[h]=(e[h]||0)+i*i},h=function(t,e){e=e>>0||6;var n,i,r,o,a=[],s=[],l=0,u=0,c=e-1,f=[],h=[];for(n in t)p(t[n],a,e);for(r=a.length,i=0;r>i;i++)l+=Math.sqrt(a[i]),o=i%e,h[o]=l,o===c&&(u+=l,o=i/e>>0,f[o]=h,s[o]=u,l=0,h=[]);return{length:u,lengths:s,segments:f}},d=window._gsDefine.plugin({propName:"bezier",priority:-1,version:"1.3.2",API:2,global:!0,init:function(t,e,n){this._target=t,e instanceof Array&&(e={values:e}),this._func={},this._round={},this._props=[],this._timeRes=null==e.timeResolution?6:parseInt(e.timeResolution,10);var i,r,o,a,s,l=e.values||[],u={},p=l[0],d=e.autoRotate||n.vars.orientToBezier;this._autoRotate=d?d instanceof Array?d:[["x","y","rotation",d===!0?0:Number(d)||0]]:null;for(i in p)this._props.push(i);for(o=this._props.length;--o>-1;)i=this._props[o],this._overwriteProps.push(i),r=this._func[i]="function"==typeof t[i],u[i]=r?t[i.indexOf("set")||"function"!=typeof t["get"+i.substr(3)]?i:"get"+i.substr(3)]():parseFloat(t[i]),s||u[i]!==l[0][i]&&(s=u);if(this._beziers="cubic"!==e.type&&"quadratic"!==e.type&&"soft"!==e.type?c(l,isNaN(e.curviness)?1:e.curviness,!1,"thruBasic"===e.type,e.correlate,s):f(l,e.type,u),this._segCount=this._beziers[i].length,this._timeRes){var m=h(this._beziers,this._timeRes);this._length=m.length,this._lengths=m.lengths,this._segments=m.segments,this._l1=this._li=this._s1=this._si=0,this._l2=this._lengths[0],this._curSeg=this._segments[0],this._s2=this._curSeg[0],this._prec=1/this._curSeg.length}if(d=this._autoRotate)for(this._initialRotations=[],d[0]instanceof Array||(this._autoRotate=d=[d]),o=d.length;--o>-1;){for(a=0;3>a;a++)i=d[o][a],this._func[i]="function"==typeof t[i]?t[i.indexOf("set")||"function"!=typeof t["get"+i.substr(3)]?i:"get"+i.substr(3)]:!1;i=d[o][2],this._initialRotations[o]=this._func[i]?this._func[i].call(this._target):this._target[i]}return this._startRatio=n.vars.runBackwards?1:0,!0},set:function(e){var n,i,r,o,a,s,l,u,c,f,p=this._segCount,h=this._func,d=this._target,m=e!==this._startRatio;if(this._timeRes){if(c=this._lengths,f=this._curSeg,e*=this._length,r=this._li,e>this._l2&&p-1>r){for(u=p-1;u>r&&e>=(this._l2=c[++r]););this._l1=c[r-1],this._li=r,this._curSeg=f=this._segments[r],this._s2=f[this._s1=this._si=0]}else if(this._l1>e&&r>0){for(;r>0&&(this._l1=c[--r])>=e;);0===r&&this._l1>e?this._l1=0:r++,this._l2=c[r],this._li=r,this._curSeg=f=this._segments[r],this._s1=f[(this._si=f.length-1)-1]||0,this._s2=f[this._si]}if(n=r,e-=this._l1,r=this._si,e>this._s2&&f.length-1>r){for(u=f.length-1;u>r&&e>=(this._s2=f[++r]););this._s1=f[r-1],this._si=r}else if(this._s1>e&&r>0){for(;r>0&&(this._s1=f[--r])>=e;);0===r&&this._s1>e?this._s1=0:r++,this._s2=f[r],this._si=r}s=(r+(e-this._s1)/(this._s2-this._s1))*this._prec}else n=0>e?0:e>=1?p-1:p*e>>0,s=(e-n*(1/p))*p;for(i=1-s,r=this._props.length;--r>-1;)o=this._props[r],a=this._beziers[o][n],l=(s*s*a.da+3*i*(s*a.ca+i*a.ba))*s+a.a,this._round[o]&&(l=Math.round(l)),h[o]?d[o](l):d[o]=l;if(this._autoRotate){var g,_,v,y,b,w,$,x=this._autoRotate;for(r=x.length;--r>-1;)o=x[r][2],w=x[r][3]||0,$=x[r][4]===!0?1:t,a=this._beziers[x[r][0]],g=this._beziers[x[r][1]],a&&g&&(a=a[n],g=g[n],_=a.a+(a.b-a.a)*s,y=a.b+(a.c-a.b)*s,_+=(y-_)*s,y+=(a.c+(a.d-a.c)*s-y)*s,v=g.a+(g.b-g.a)*s,b=g.b+(g.c-g.b)*s,v+=(b-v)*s,b+=(g.c+(g.d-g.c)*s-b)*s,l=m?Math.atan2(b-v,y-_)*$+w:this._initialRotations[r],h[o]?d[o](l):d[o]=l)}}}),m=d.prototype;d.bezierThrough=c,d.cubicToQuadratic=s,d._autoCSS=!0,d.quadraticToCubic=function(t,e,n){return new o(t,(2*e+t)/3,(2*e+n)/3,n)},d._cssRegister=function(){var t=window._gsDefine.globals.CSSPlugin;if(t){var e=t._internals,n=e._parseToProxy,i=e._setPluginRatio,r=e.CSSPropTween;e._registerComplexSpecialProp("bezier",{parser:function(t,e,o,a,s,l){e instanceof Array&&(e={values:e}),l=new d;var u,c,f,p=e.values,h=p.length-1,m=[],g={};if(0>h)return s;for(u=0;h>=u;u++)f=n(t,p[u],a,s,l,h!==u),m[u]=f.end;for(c in e)g[c]=e[c];return g.values=m,s=new r(t,"bezier",0,0,f.pt,2),s.data=f,s.plugin=l,s.setRatio=i,0===g.autoRotate&&(g.autoRotate=!0),!g.autoRotate||g.autoRotate instanceof Array||(u=g.autoRotate===!0?0:Number(g.autoRotate),g.autoRotate=null!=f.end.left?[["left","top","rotation",u,!1]]:null!=f.end.x?[["x","y","rotation",u,!1]]:!1),g.autoRotate&&(a._transform||a._enableTransforms(!1),f.autoRotate=a._target._gsTransform),l._onInitTween(f.proxy,g,a._tween),s}})}},m._roundProps=function(t,e){for(var n=this._overwriteProps,i=n.length;--i>-1;)(t[n[i]]||t.bezier||t.bezierThrough)&&(this._round[n[i]]=e)},m._kill=function(t){var e,n,i=this._props;for(e in this._beziers)if(e in t)for(delete this._beziers[e],delete this._func[e],n=i.length;--n>-1;)i[n]===e&&i.splice(n,1);return this._super._kill.call(this,t)}}(),window._gsDefine("plugins.CSSPlugin",["plugins.TweenPlugin","TweenLite"],function(t,e){var n,i,r,o,a=function(){t.call(this,"css"),this._overwriteProps.length=0,this.setRatio=a.prototype.setRatio},s={},l=a.prototype=new t("css");l.constructor=a,a.version="1.11.8",a.API=2,a.defaultTransformPerspective=0,a.defaultSkewType="compensated",l="px",a.suffixMap={top:l,right:l,bottom:l,left:l,width:l,height:l,fontSize:l,padding:l,margin:l,perspective:l,lineHeight:""};var u,c,f,p,h,d,m=/(?:\d|\-\d|\.\d|\-\.\d)+/g,g=/(?:\d|\-\d|\.\d|\-\.\d|\+=\d|\-=\d|\+=.\d|\-=\.\d)+/g,_=/(?:\+=|\-=|\-|\b)[\d\-\.]+[a-zA-Z0-9]*(?:%|\b)/gi,v=/[^\d\-\.]/g,y=/(?:\d|\-|\+|=|#|\.)*/g,b=/opacity *= *([^)]*)/,w=/opacity:([^;]*)/,$=/alpha\(opacity *=.+?\)/i,x=/^(rgb|hsl)/,T=/([A-Z])/g,S=/-([a-z])/gi,C=/(^(?:url\(\"|url\())|(?:(\"\))$|\)$)/gi,k=function(t,e){return e.toUpperCase()},A=/(?:Left|Right|Width)/i,D=/(M11|M12|M21|M22)=[\d\-\.e]+/gi,E=/progid\:DXImageTransform\.Microsoft\.Matrix\(.+?\)/i,P=/,(?=[^\)]*(?:\(|$))/gi,M=Math.PI/180,O=180/Math.PI,R={},N=document,I=N.createElement("div"),F=N.createElement("img"),L=a._internals={_specialProps:s},Y=navigator.userAgent,V=function(){var t,e=Y.indexOf("Android"),n=N.createElement("div");return f=-1!==Y.indexOf("Safari")&&-1===Y.indexOf("Chrome")&&(-1===e||Number(Y.substr(e+8,1))>3),h=f&&6>Number(Y.substr(Y.indexOf("Version/")+8,1)),p=-1!==Y.indexOf("Firefox"),/MSIE ([0-9]{1,}[\.0-9]{0,})/.exec(Y)&&(d=parseFloat(RegExp.$1)),n.innerHTML="<a style='top:1px;opacity:.55;'>a</a>",t=n.getElementsByTagName("a")[0],t?/^0.55/.test(t.style.opacity):!1}(),X=function(t){return b.test("string"==typeof t?t:(t.currentStyle?t.currentStyle.filter:t.style.filter)||"")?parseFloat(RegExp.$1)/100:1},j=function(t){window.console&&console.log(t)},B="",H="",U=function(t,e){e=e||I;var n,i,r=e.style;if(void 0!==r[t])return t;for(t=t.charAt(0).toUpperCase()+t.substr(1),n=["O","Moz","ms","Ms","Webkit"],i=5;--i>-1&&void 0===r[n[i]+t];);return i>=0?(H=3===i?"ms":n[i],B="-"+H.toLowerCase()+"-",H+t):null},z=N.defaultView?N.defaultView.getComputedStyle:function(){},W=a.getStyle=function(t,e,n,i,r){var o;return V||"opacity"!==e?(!i&&t.style[e]?o=t.style[e]:(n=n||z(t,null))?o=n[e]||n.getPropertyValue(e)||n.getPropertyValue(e.replace(T,"-$1").toLowerCase()):t.currentStyle&&(o=t.currentStyle[e]),null==r||o&&"none"!==o&&"auto"!==o&&"auto auto"!==o?o:r):X(t)},q=L.convertToPixels=function(t,n,i,r,o){if("px"===r||!r)return i;if("auto"===r||!i)return 0;var s,l,u,c=A.test(n),f=t,p=I.style,h=0>i;if(h&&(i=-i),"%"===r&&-1!==n.indexOf("border"))s=i/100*(c?t.clientWidth:t.clientHeight);else{if(p.cssText="border:0 solid red;position:"+W(t,"position")+";line-height:0;","%"!==r&&f.appendChild)p[c?"borderLeftWidth":"borderTopWidth"]=i+r;else{if(f=t.parentNode||N.body,l=f._gsCache,u=e.ticker.frame,l&&c&&l.time===u)return l.width*i/100;p[c?"width":"height"]=i+r}f.appendChild(I),s=parseFloat(I[c?"offsetWidth":"offsetHeight"]),f.removeChild(I),c&&"%"===r&&a.cacheWidths!==!1&&(l=f._gsCache=f._gsCache||{},l.time=u,l.width=100*(s/i)),0!==s||o||(s=q(t,n,i,r,!0))}return h?-s:s},K=L.calculateOffset=function(t,e,n){if("absolute"!==W(t,"position",n))return 0;var i="left"===e?"Left":"Top",r=W(t,"margin"+i,n);return t["offset"+i]-(q(t,e,parseFloat(r),r.replace(y,""))||0)},G=function(t,e){var n,i,r={};if(e=e||z(t,null))if(n=e.length)for(;--n>-1;)r[e[n].replace(S,k)]=e.getPropertyValue(e[n]);else for(n in e)r[n]=e[n];else if(e=t.currentStyle||t.style)for(n in e)"string"==typeof n&&void 0===r[n]&&(r[n.replace(S,k)]=e[n]);return V||(r.opacity=X(t)),i=Te(t,e,!1),r.rotation=i.rotation,r.skewX=i.skewX,r.scaleX=i.scaleX,r.scaleY=i.scaleY,r.x=i.x,r.y=i.y,$e&&(r.z=i.z,r.rotationX=i.rotationX,r.rotationY=i.rotationY,r.scaleZ=i.scaleZ),r.filters&&delete r.filters,r},Z=function(t,e,n,i,r){var o,a,s,l={},u=t.style;for(a in n)"cssText"!==a&&"length"!==a&&isNaN(a)&&(e[a]!==(o=n[a])||r&&r[a])&&-1===a.indexOf("Origin")&&("number"==typeof o||"string"==typeof o)&&(l[a]="auto"!==o||"left"!==a&&"top"!==a?""!==o&&"auto"!==o&&"none"!==o||"string"!=typeof e[a]||""===e[a].replace(v,"")?o:0:K(t,a),void 0!==u[a]&&(s=new fe(u,a,u[a],s)));if(i)for(a in i)"className"!==a&&(l[a]=i[a]);return{difs:l,firstMPT:s}},J={width:["Left","Right"],height:["Top","Bottom"]},Q=["marginLeft","marginRight","marginTop","marginBottom"],te=function(t,e,n){var i=parseFloat("width"===e?t.offsetWidth:t.offsetHeight),r=J[e],o=r.length;for(n=n||z(t,null);--o>-1;)i-=parseFloat(W(t,"padding"+r[o],n,!0))||0,i-=parseFloat(W(t,"border"+r[o]+"Width",n,!0))||0;return i},ee=function(t,e){(null==t||""===t||"auto"===t||"auto auto"===t)&&(t="0 0");var n=t.split(" "),i=-1!==t.indexOf("left")?"0%":-1!==t.indexOf("right")?"100%":n[0],r=-1!==t.indexOf("top")?"0%":-1!==t.indexOf("bottom")?"100%":n[1];return null==r?r="0":"center"===r&&(r="50%"),("center"===i||isNaN(parseFloat(i))&&-1===(i+"").indexOf("="))&&(i="50%"),e&&(e.oxp=-1!==i.indexOf("%"),e.oyp=-1!==r.indexOf("%"),e.oxr="="===i.charAt(1),e.oyr="="===r.charAt(1),e.ox=parseFloat(i.replace(v,"")),e.oy=parseFloat(r.replace(v,""))),i+" "+r+(n.length>2?" "+n[2]:"")},ne=function(t,e){return"string"==typeof t&&"="===t.charAt(1)?parseInt(t.charAt(0)+"1",10)*parseFloat(t.substr(2)):parseFloat(t)-parseFloat(e)},ie=function(t,e){return null==t?e:"string"==typeof t&&"="===t.charAt(1)?parseInt(t.charAt(0)+"1",10)*Number(t.substr(2))+e:parseFloat(t)},re=function(t,e,n,i){var r,o,a,s,l=1e-6;return null==t?s=e:"number"==typeof t?s=t:(r=360,o=t.split("_"),a=Number(o[0].replace(v,""))*(-1===t.indexOf("rad")?1:O)-("="===t.charAt(1)?0:e),o.length&&(i&&(i[n]=e+a),-1!==t.indexOf("short")&&(a%=r,a!==a%(r/2)&&(a=0>a?a+r:a-r)),-1!==t.indexOf("_cw")&&0>a?a=(a+9999999999*r)%r-(0|a/r)*r:-1!==t.indexOf("ccw")&&a>0&&(a=(a-9999999999*r)%r-(0|a/r)*r)),s=e+a),l>s&&s>-l&&(s=0),s},oe={aqua:[0,255,255],lime:[0,255,0],silver:[192,192,192],black:[0,0,0],maroon:[128,0,0],teal:[0,128,128],blue:[0,0,255],navy:[0,0,128],white:[255,255,255],fuchsia:[255,0,255],olive:[128,128,0],yellow:[255,255,0],orange:[255,165,0],gray:[128,128,128],purple:[128,0,128],green:[0,128,0],red:[255,0,0],pink:[255,192,203],cyan:[0,255,255],transparent:[255,255,255,0]},ae=function(t,e,n){return t=0>t?t+1:t>1?t-1:t,0|255*(1>6*t?e+6*(n-e)*t:.5>t?n:2>3*t?e+6*(n-e)*(2/3-t):e)+.5},se=function(t){var e,n,i,r,o,a;return t&&""!==t?"number"==typeof t?[t>>16,255&t>>8,255&t]:(","===t.charAt(t.length-1)&&(t=t.substr(0,t.length-1)),oe[t]?oe[t]:"#"===t.charAt(0)?(4===t.length&&(e=t.charAt(1),n=t.charAt(2),i=t.charAt(3),t="#"+e+e+n+n+i+i),t=parseInt(t.substr(1),16),[t>>16,255&t>>8,255&t]):"hsl"===t.substr(0,3)?(t=t.match(m),r=Number(t[0])%360/360,o=Number(t[1])/100,a=Number(t[2])/100,n=.5>=a?a*(o+1):a+o-a*o,e=2*a-n,t.length>3&&(t[3]=Number(t[3])),t[0]=ae(r+1/3,e,n),t[1]=ae(r,e,n),t[2]=ae(r-1/3,e,n),t):(t=t.match(m)||oe.transparent,t[0]=Number(t[0]),t[1]=Number(t[1]),t[2]=Number(t[2]),t.length>3&&(t[3]=Number(t[3])),t)):oe.black},le="(?:\\b(?:(?:rgb|rgba|hsl|hsla)\\(.+?\\))|\\B#.+?\\b";for(l in oe)le+="|"+l+"\\b";le=RegExp(le+")","gi");var ue=function(t,e,n,i){if(null==t)return function(t){return t};var r,o=e?(t.match(le)||[""])[0]:"",a=t.split(o).join("").match(_)||[],s=t.substr(0,t.indexOf(a[0])),l=")"===t.charAt(t.length-1)?")":"",u=-1!==t.indexOf(" ")?" ":",",c=a.length,f=c>0?a[0].replace(m,""):"";return c?r=e?function(t){var e,p,h,d;if("number"==typeof t)t+=f;else if(i&&P.test(t)){for(d=t.replace(P,"|").split("|"),h=0;d.length>h;h++)d[h]=r(d[h]);return d.join(",")}if(e=(t.match(le)||[o])[0],p=t.split(e).join("").match(_)||[],h=p.length,c>h--)for(;c>++h;)p[h]=n?p[0|(h-1)/2]:a[h];return s+p.join(u)+u+e+l+(-1!==t.indexOf("inset")?" inset":"")}:function(t){var e,o,p;if("number"==typeof t)t+=f;else if(i&&P.test(t)){for(o=t.replace(P,"|").split("|"),p=0;o.length>p;p++)o[p]=r(o[p]);return o.join(",")}if(e=t.match(_)||[],p=e.length,c>p--)for(;c>++p;)e[p]=n?e[0|(p-1)/2]:a[p];return s+e.join(u)+l}:function(t){return t}},ce=function(t){return t=t.split(","),function(e,n,i,r,o,a,s){var l,u=(n+"").split(" ");for(s={},l=0;4>l;l++)s[t[l]]=u[l]=u[l]||u[(l-1)/2>>0];return r.parse(e,s,o,a)}},fe=(L._setPluginRatio=function(t){this.plugin.setRatio(t);for(var e,n,i,r,o=this.data,a=o.proxy,s=o.firstMPT,l=1e-6;s;)e=a[s.v],s.r?e=Math.round(e):l>e&&e>-l&&(e=0),s.t[s.p]=e,s=s._next;if(o.autoRotate&&(o.autoRotate.rotation=a.rotation),1===t)for(s=o.firstMPT;s;){if(n=s.t,n.type){if(1===n.type){for(r=n.xs0+n.s+n.xs1,i=1;n.l>i;i++)r+=n["xn"+i]+n["xs"+(i+1)];n.e=r}}else n.e=n.s+n.xs0;s=s._next}},function(t,e,n,i,r){this.t=t,this.p=e,this.v=n,this.r=r,i&&(i._prev=this,this._next=i)}),pe=(L._parseToProxy=function(t,e,n,i,r,o){var a,s,l,u,c,f=i,p={},h={},d=n._transform,m=R;for(n._transform=null,R=e,i=c=n.parse(t,e,i,r),R=m,o&&(n._transform=d,f&&(f._prev=null,f._prev&&(f._prev._next=null)));i&&i!==f;){if(1>=i.type&&(s=i.p,h[s]=i.s+i.c,p[s]=i.s,o||(u=new fe(i,"s",s,u,i.r),i.c=0),1===i.type))for(a=i.l;--a>0;)l="xn"+a,s=i.p+"_"+l,h[s]=i.data[l],p[s]=i[l],o||(u=new fe(i,l,s,u,i.rxp[l]));i=i._next}return{proxy:p,end:h,firstMPT:u,pt:c}},L.CSSPropTween=function(t,e,i,r,a,s,l,u,c,f,p){this.t=t,this.p=e,this.s=i,this.c=r,this.n=l||e,t instanceof pe||o.push(this.n),this.r=u,this.type=s||0,c&&(this.pr=c,n=!0),this.b=void 0===f?i:f,this.e=void 0===p?i+r:p,a&&(this._next=a,a._prev=this)}),he=a.parseComplex=function(t,e,n,i,r,o,a,s,l,c){n=n||o||"",a=new pe(t,e,0,0,a,c?2:1,null,!1,s,n,i),i+="";var f,p,h,d,_,v,y,b,w,$,T,S,C=n.split(", ").join(",").split(" "),k=i.split(", ").join(",").split(" "),A=C.length,D=u!==!1;for((-1!==i.indexOf(",")||-1!==n.indexOf(","))&&(C=C.join(" ").replace(P,", ").split(" "),k=k.join(" ").replace(P,", ").split(" "),A=C.length),A!==k.length&&(C=(o||"").split(" "),A=C.length),a.plugin=l,a.setRatio=c,f=0;A>f;f++)if(d=C[f],_=k[f],b=parseFloat(d),b||0===b)a.appendXtra("",b,ne(_,b),_.replace(g,""),D&&-1!==_.indexOf("px"),!0);else if(r&&("#"===d.charAt(0)||oe[d]||x.test(d)))S=","===_.charAt(_.length-1)?"),":")",d=se(d),_=se(_),w=d.length+_.length>6,w&&!V&&0===_[3]?(a["xs"+a.l]+=a.l?" transparent":"transparent",a.e=a.e.split(k[f]).join("transparent")):(V||(w=!1),a.appendXtra(w?"rgba(":"rgb(",d[0],_[0]-d[0],",",!0,!0).appendXtra("",d[1],_[1]-d[1],",",!0).appendXtra("",d[2],_[2]-d[2],w?",":S,!0),w&&(d=4>d.length?1:d[3],a.appendXtra("",d,(4>_.length?1:_[3])-d,S,!1)));else if(v=d.match(m)){if(y=_.match(g),!y||y.length!==v.length)return a;for(h=0,p=0;v.length>p;p++)T=v[p],$=d.indexOf(T,h),a.appendXtra(d.substr(h,$-h),Number(T),ne(y[p],T),"",D&&"px"===d.substr($+T.length,2),0===p),h=$+T.length;a["xs"+a.l]+=d.substr(h)}else a["xs"+a.l]+=a.l?" "+d:d;if(-1!==i.indexOf("=")&&a.data){for(S=a.xs0+a.data.s,f=1;a.l>f;f++)S+=a["xs"+f]+a.data["xn"+f];a.e=S+a["xs"+f]}return a.l||(a.type=-1,a.xs0=a.e),a.xfirst||a},de=9;for(l=pe.prototype,l.l=l.pr=0;--de>0;)l["xn"+de]=0,l["xs"+de]="";l.xs0="",l._next=l._prev=l.xfirst=l.data=l.plugin=l.setRatio=l.rxp=null,l.appendXtra=function(t,e,n,i,r,o){var a=this,s=a.l;return a["xs"+s]+=o&&s?" "+t:t||"",n||0===s||a.plugin?(a.l++,a.type=a.setRatio?2:1,a["xs"+a.l]=i||"",s>0?(a.data["xn"+s]=e+n,a.rxp["xn"+s]=r,a["xn"+s]=e,a.plugin||(a.xfirst=new pe(a,"xn"+s,e,n,a.xfirst||a,0,a.n,r,a.pr),a.xfirst.xs0=0),a):(a.data={s:e+n},a.rxp={},a.s=e,a.c=n,a.r=r,a)):(a["xs"+s]+=e+(i||""),a)};var me=function(t,e){e=e||{},this.p=e.prefix?U(t)||t:t,s[t]=s[this.p]=this,this.format=e.formatter||ue(e.defaultValue,e.color,e.collapsible,e.multi),e.parser&&(this.parse=e.parser),this.clrs=e.color,this.multi=e.multi,this.keyword=e.keyword,this.dflt=e.defaultValue,this.pr=e.priority||0},ge=L._registerComplexSpecialProp=function(t,e,n){"object"!=typeof e&&(e={parser:n});var i,r,o=t.split(","),a=e.defaultValue;for(n=n||[a],i=0;o.length>i;i++)e.prefix=0===i&&e.prefix,e.defaultValue=n[i]||a,r=new me(o[i],e)},_e=function(t){if(!s[t]){var e=t.charAt(0).toUpperCase()+t.substr(1)+"Plugin";ge(t,{parser:function(t,n,i,r,o,a,l){var u=(window.GreenSockGlobals||window).com.greensock.plugins[e];return u?(u._cssRegister(),s[i].parse(t,n,i,r,o,a,l)):(j("Error: "+e+" js file not loaded."),o)}})}};l=me.prototype,l.parseComplex=function(t,e,n,i,r,o){var a,s,l,u,c,f,p=this.keyword;if(this.multi&&(P.test(n)||P.test(e)?(s=e.replace(P,"|").split("|"),l=n.replace(P,"|").split("|")):p&&(s=[e],l=[n])),l){for(u=l.length>s.length?l.length:s.length,a=0;u>a;a++)e=s[a]=s[a]||this.dflt,n=l[a]=l[a]||this.dflt,p&&(c=e.indexOf(p),f=n.indexOf(p),c!==f&&(n=-1===f?l:s,n[a]+=" "+p));e=s.join(", "),n=l.join(", ")}return he(t,this.p,e,n,this.clrs,this.dflt,i,this.pr,r,o)},l.parse=function(t,e,n,i,o,a){return this.parseComplex(t.style,this.format(W(t,this.p,r,!1,this.dflt)),this.format(e),o,a)},a.registerSpecialProp=function(t,e,n){ge(t,{parser:function(t,i,r,o,a,s){var l=new pe(t,r,0,0,a,2,r,!1,n);return l.plugin=s,l.setRatio=e(t,i,o._tween,r),l},priority:n})};var ve="scaleX,scaleY,scaleZ,x,y,z,skewX,skewY,rotation,rotationX,rotationY,perspective".split(","),ye=U("transform"),be=B+"transform",we=U("transformOrigin"),$e=null!==U("perspective"),xe=L.Transform=function(){this.skewY=0},Te=L.getTransform=function(t,e,n,i){if(t._gsTransform&&n&&!i)return t._gsTransform;var r,o,s,l,u,c,f,p,h,d,m,g,_,v=n?t._gsTransform||new xe:new xe,y=0>v.scaleX,b=2e-5,w=1e5,$=179.99,x=$*M,T=$e?parseFloat(W(t,we,e,!1,"0 0 0").split(" ")[2])||v.zOrigin||0:0;for(ye?r=W(t,be,e,!0):t.currentStyle&&(r=t.currentStyle.filter.match(D),r=r&&4===r.length?[r[0].substr(4),Number(r[2].substr(4)),Number(r[1].substr(4)),r[3].substr(4),v.x||0,v.y||0].join(","):""),o=(r||"").match(/(?:\-|\b)[\d\-\.e]+\b/gi)||[],s=o.length;--s>-1;)l=Number(o[s]),o[s]=(u=l-(l|=0))?(0|u*w+(0>u?-.5:.5))/w+l:l;if(16===o.length){var S=o[8],C=o[9],k=o[10],A=o[12],E=o[13],P=o[14];if(v.zOrigin&&(P=-v.zOrigin,A=S*P-o[12],E=C*P-o[13],P=k*P+v.zOrigin-o[14]),!n||i||null==v.rotationX){var R,N,I,F,L,Y,V,X=o[0],j=o[1],B=o[2],H=o[3],U=o[4],z=o[5],q=o[6],K=o[7],G=o[11],Z=Math.atan2(q,k),J=-x>Z||Z>x;v.rotationX=Z*O,Z&&(F=Math.cos(-Z),L=Math.sin(-Z),R=U*F+S*L,N=z*F+C*L,I=q*F+k*L,S=U*-L+S*F,C=z*-L+C*F,k=q*-L+k*F,G=K*-L+G*F,U=R,z=N,q=I),Z=Math.atan2(S,X),v.rotationY=Z*O,Z&&(Y=-x>Z||Z>x,F=Math.cos(-Z),L=Math.sin(-Z),R=X*F-S*L,N=j*F-C*L,I=B*F-k*L,C=j*L+C*F,k=B*L+k*F,G=H*L+G*F,X=R,j=N,B=I),Z=Math.atan2(j,z),v.rotation=Z*O,Z&&(V=-x>Z||Z>x,F=Math.cos(-Z),L=Math.sin(-Z),X=X*F+U*L,N=j*F+z*L,z=j*-L+z*F,q=B*-L+q*F,j=N),V&&J?v.rotation=v.rotationX=0:V&&Y?v.rotation=v.rotationY=0:Y&&J&&(v.rotationY=v.rotationX=0),v.scaleX=(0|Math.sqrt(X*X+j*j)*w+.5)/w,v.scaleY=(0|Math.sqrt(z*z+C*C)*w+.5)/w,v.scaleZ=(0|Math.sqrt(q*q+k*k)*w+.5)/w,v.skewX=0,v.perspective=G?1/(0>G?-G:G):0,v.x=A,v.y=E,v.z=P}}else if(!($e&&!i&&o.length&&v.x===o[4]&&v.y===o[5]&&(v.rotationX||v.rotationY)||void 0!==v.x&&"none"===W(t,"display",e))){var Q=o.length>=6,te=Q?o[0]:1,ee=o[1]||0,ne=o[2]||0,ie=Q?o[3]:1;v.x=o[4]||0,v.y=o[5]||0,c=Math.sqrt(te*te+ee*ee),f=Math.sqrt(ie*ie+ne*ne),p=te||ee?Math.atan2(ee,te)*O:v.rotation||0,h=ne||ie?Math.atan2(ne,ie)*O+p:v.skewX||0,d=c-Math.abs(v.scaleX||0),m=f-Math.abs(v.scaleY||0),Math.abs(h)>90&&270>Math.abs(h)&&(y?(c*=-1,h+=0>=p?180:-180,p+=0>=p?180:-180):(f*=-1,h+=0>=h?180:-180)),g=(p-v.rotation)%180,_=(h-v.skewX)%180,(void 0===v.skewX||d>b||-b>d||m>b||-b>m||g>-$&&$>g&&!1|g*w||_>-$&&$>_&&!1|_*w)&&(v.scaleX=c,v.scaleY=f,v.rotation=p,v.skewX=h),$e&&(v.rotationX=v.rotationY=v.z=0,v.perspective=parseFloat(a.defaultTransformPerspective)||0,v.scaleZ=1)}v.zOrigin=T;for(s in v)b>v[s]&&v[s]>-b&&(v[s]=0);return n&&(t._gsTransform=v),v},Se=function(t){var e,n,i=this.data,r=-i.rotation*M,o=r+i.skewX*M,a=1e5,s=(0|Math.cos(r)*i.scaleX*a)/a,l=(0|Math.sin(r)*i.scaleX*a)/a,u=(0|Math.sin(o)*-i.scaleY*a)/a,c=(0|Math.cos(o)*i.scaleY*a)/a,f=this.t.style,p=this.t.currentStyle;if(p){n=l,l=-u,u=-n,e=p.filter,f.filter="";var h,m,g=this.t.offsetWidth,_=this.t.offsetHeight,v="absolute"!==p.position,w="progid:DXImageTransform.Microsoft.Matrix(M11="+s+", M12="+l+", M21="+u+", M22="+c,$=i.x,x=i.y;if(null!=i.ox&&(h=(i.oxp?.01*g*i.ox:i.ox)-g/2,m=(i.oyp?.01*_*i.oy:i.oy)-_/2,$+=h-(h*s+m*l),x+=m-(h*u+m*c)),v?(h=g/2,m=_/2,w+=", Dx="+(h-(h*s+m*l)+$)+", Dy="+(m-(h*u+m*c)+x)+")"):w+=", sizingMethod='auto expand')",f.filter=-1!==e.indexOf("DXImageTransform.Microsoft.Matrix(")?e.replace(E,w):w+" "+e,(0===t||1===t)&&1===s&&0===l&&0===u&&1===c&&(v&&-1===w.indexOf("Dx=0, Dy=0")||b.test(e)&&100!==parseFloat(RegExp.$1)||-1===e.indexOf("gradient("&&e.indexOf("Alpha"))&&f.removeAttribute("filter")),!v){var T,S,C,k=8>d?1:-1;for(h=i.ieOffsetX||0,m=i.ieOffsetY||0,i.ieOffsetX=Math.round((g-((0>s?-s:s)*g+(0>l?-l:l)*_))/2+$),i.ieOffsetY=Math.round((_-((0>c?-c:c)*_+(0>u?-u:u)*g))/2+x),de=0;4>de;de++)S=Q[de],T=p[S],n=-1!==T.indexOf("px")?parseFloat(T):q(this.t,S,parseFloat(T),T.replace(y,""))||0,C=n!==i[S]?2>de?-i.ieOffsetX:-i.ieOffsetY:2>de?h-i.ieOffsetX:m-i.ieOffsetY,f[S]=(i[S]=Math.round(n-C*(0===de||2===de?1:k)))+"px"}}},Ce=L.set3DTransformRatio=function(){var t,e,n,i,r,o,a,s,l,u,c,f,h,d,m,g,_,v,y,b,w,$,x,T=this.data,S=this.t.style,C=T.rotation*M,k=T.scaleX,A=T.scaleY,D=T.scaleZ,E=T.perspective;if(p){var P=1e-4;P>k&&k>-P&&(k=D=2e-5),P>A&&A>-P&&(A=D=2e-5),!E||T.z||T.rotationX||T.rotationY||(E=0)}if(C||T.skewX)v=Math.cos(C),y=Math.sin(C),t=v,r=y,T.skewX&&(C-=T.skewX*M,v=Math.cos(C),y=Math.sin(C),"simple"===T.skewType&&(b=Math.tan(T.skewX*M),b=Math.sqrt(1+b*b),v*=b,y*=b)),e=-y,o=v;else{if(!(T.rotationY||T.rotationX||1!==D||E))return void(S[ye]="translate3d("+T.x+"px,"+T.y+"px,"+T.z+"px)"+(1!==k||1!==A?" scale("+k+","+A+")":""));t=o=1,e=r=0}c=1,n=i=a=s=l=u=f=h=d=0,m=E?-1/E:0,g=T.zOrigin,_=1e5,C=T.rotationY*M,C&&(v=Math.cos(C),y=Math.sin(C),l=c*-y,h=m*-y,n=t*y,a=r*y,c*=v,m*=v,t*=v,r*=v),C=T.rotationX*M,C&&(v=Math.cos(C),y=Math.sin(C),b=e*v+n*y,w=o*v+a*y,$=u*v+c*y,x=d*v+m*y,n=e*-y+n*v,a=o*-y+a*v,c=u*-y+c*v,m=d*-y+m*v,e=b,o=w,u=$,d=x),1!==D&&(n*=D,a*=D,c*=D,m*=D),1!==A&&(e*=A,o*=A,u*=A,d*=A),1!==k&&(t*=k,r*=k,l*=k,h*=k),g&&(f-=g,i=n*f,s=a*f,f=c*f+g),i=(b=(i+=T.x)-(i|=0))?(0|b*_+(0>b?-.5:.5))/_+i:i,s=(b=(s+=T.y)-(s|=0))?(0|b*_+(0>b?-.5:.5))/_+s:s,f=(b=(f+=T.z)-(f|=0))?(0|b*_+(0>b?-.5:.5))/_+f:f,S[ye]="matrix3d("+[(0|t*_)/_,(0|r*_)/_,(0|l*_)/_,(0|h*_)/_,(0|e*_)/_,(0|o*_)/_,(0|u*_)/_,(0|d*_)/_,(0|n*_)/_,(0|a*_)/_,(0|c*_)/_,(0|m*_)/_,i,s,f,E?1+-f/E:1].join(",")+")"},ke=L.set2DTransformRatio=function(t){var e,n,i,r,o,a=this.data,s=this.t,l=s.style;return a.rotationX||a.rotationY||a.z||a.force3D?(this.setRatio=Ce,void Ce.call(this,t)):void(a.rotation||a.skewX?(e=a.rotation*M,n=e-a.skewX*M,i=1e5,r=a.scaleX*i,o=a.scaleY*i,l[ye]="matrix("+(0|Math.cos(e)*r)/i+","+(0|Math.sin(e)*r)/i+","+(0|Math.sin(n)*-o)/i+","+(0|Math.cos(n)*o)/i+","+a.x+","+a.y+")"):l[ye]="matrix("+a.scaleX+",0,0,"+a.scaleY+","+a.x+","+a.y+")")};ge("transform,scale,scaleX,scaleY,scaleZ,x,y,z,rotation,rotationX,rotationY,rotationZ,skewX,skewY,shortRotation,shortRotationX,shortRotationY,shortRotationZ,transformOrigin,transformPerspective,directionalRotation,parseTransform,force3D,skewType",{parser:function(t,e,n,i,o,s,l){if(i._transform)return o;var u,c,f,p,h,d,m,g=i._transform=Te(t,r,!0,l.parseTransform),_=t.style,v=1e-6,y=ve.length,b=l,w={};if("string"==typeof b.transform&&ye)f=_.cssText,_[ye]=b.transform,_.display="block",u=Te(t,null,!1),_.cssText=f;else if("object"==typeof b){if(u={scaleX:ie(null!=b.scaleX?b.scaleX:b.scale,g.scaleX),scaleY:ie(null!=b.scaleY?b.scaleY:b.scale,g.scaleY),scaleZ:ie(b.scaleZ,g.scaleZ),x:ie(b.x,g.x),y:ie(b.y,g.y),z:ie(b.z,g.z),perspective:ie(b.transformPerspective,g.perspective)},m=b.directionalRotation,null!=m)if("object"==typeof m)for(f in m)b[f]=m[f];else b.rotation=m;u.rotation=re("rotation"in b?b.rotation:"shortRotation"in b?b.shortRotation+"_short":"rotationZ"in b?b.rotationZ:g.rotation,g.rotation,"rotation",w),$e&&(u.rotationX=re("rotationX"in b?b.rotationX:"shortRotationX"in b?b.shortRotationX+"_short":g.rotationX||0,g.rotationX,"rotationX",w),u.rotationY=re("rotationY"in b?b.rotationY:"shortRotationY"in b?b.shortRotationY+"_short":g.rotationY||0,g.rotationY,"rotationY",w)),u.skewX=null==b.skewX?g.skewX:re(b.skewX,g.skewX),u.skewY=null==b.skewY?g.skewY:re(b.skewY,g.skewY),(c=u.skewY-g.skewY)&&(u.skewX+=c,u.rotation+=c)}for($e&&null!=b.force3D&&(g.force3D=b.force3D,d=!0),g.skewType=b.skewType||g.skewType||a.defaultSkewType,h=g.force3D||g.z||g.rotationX||g.rotationY||u.z||u.rotationX||u.rotationY||u.perspective,h||null==b.scale||(u.scaleZ=1);--y>-1;)n=ve[y],p=u[n]-g[n],(p>v||-v>p||null!=R[n])&&(d=!0,o=new pe(g,n,g[n],p,o),n in w&&(o.e=w[n]),o.xs0=0,o.plugin=s,i._overwriteProps.push(o.n));return p=b.transformOrigin,(p||$e&&h&&g.zOrigin)&&(ye?(d=!0,n=we,p=(p||W(t,n,r,!1,"50% 50%"))+"",o=new pe(_,n,0,0,o,-1,"transformOrigin"),o.b=_[n],o.plugin=s,$e?(f=g.zOrigin,p=p.split(" "),g.zOrigin=(p.length>2&&(0===f||"0px"!==p[2])?parseFloat(p[2]):f)||0,o.xs0=o.e=_[n]=p[0]+" "+(p[1]||"50%")+" 0px",o=new pe(g,"zOrigin",0,0,o,-1,o.n),o.b=f,o.xs0=o.e=g.zOrigin):o.xs0=o.e=_[n]=p):ee(p+"",g)),d&&(i._transformType=h||3===this._transformType?3:2),o},prefix:!0}),ge("boxShadow",{defaultValue:"0px 0px 0px 0px #999",prefix:!0,color:!0,multi:!0,keyword:"inset"}),ge("borderRadius",{defaultValue:"0px",parser:function(t,e,n,o,a){e=this.format(e);var s,l,u,c,f,p,h,d,m,g,_,v,y,b,w,$,x=["borderTopLeftRadius","borderTopRightRadius","borderBottomRightRadius","borderBottomLeftRadius"],T=t.style;for(m=parseFloat(t.offsetWidth),g=parseFloat(t.offsetHeight),s=e.split(" "),l=0;x.length>l;l++)this.p.indexOf("border")&&(x[l]=U(x[l])),f=c=W(t,x[l],r,!1,"0px"),-1!==f.indexOf(" ")&&(c=f.split(" "),f=c[0],c=c[1]),p=u=s[l],h=parseFloat(f),v=f.substr((h+"").length),y="="===p.charAt(1),y?(d=parseInt(p.charAt(0)+"1",10),p=p.substr(2),d*=parseFloat(p),_=p.substr((d+"").length-(0>d?1:0))||""):(d=parseFloat(p),_=p.substr((d+"").length)),""===_&&(_=i[n]||v),_!==v&&(b=q(t,"borderLeft",h,v),w=q(t,"borderTop",h,v),"%"===_?(f=100*(b/m)+"%",c=100*(w/g)+"%"):"em"===_?($=q(t,"borderLeft",1,"em"),f=b/$+"em",c=w/$+"em"):(f=b+"px",c=w+"px"),y&&(p=parseFloat(f)+d+_,u=parseFloat(c)+d+_)),a=he(T,x[l],f+" "+c,p+" "+u,!1,"0px",a);return a},prefix:!0,formatter:ue("0px 0px 0px 0px",!1,!0)}),ge("backgroundPosition",{defaultValue:"0 0",parser:function(t,e,n,i,o,a){var s,l,u,c,f,p,h="background-position",m=r||z(t,null),g=this.format((m?d?m.getPropertyValue(h+"-x")+" "+m.getPropertyValue(h+"-y"):m.getPropertyValue(h):t.currentStyle.backgroundPositionX+" "+t.currentStyle.backgroundPositionY)||"0 0"),_=this.format(e);if(-1!==g.indexOf("%")!=(-1!==_.indexOf("%"))&&(p=W(t,"backgroundImage").replace(C,""),p&&"none"!==p)){for(s=g.split(" "),l=_.split(" "),F.setAttribute("src",p),u=2;--u>-1;)g=s[u],c=-1!==g.indexOf("%"),c!==(-1!==l[u].indexOf("%"))&&(f=0===u?t.offsetWidth-F.width:t.offsetHeight-F.height,s[u]=c?parseFloat(g)/100*f+"px":100*(parseFloat(g)/f)+"%");g=s.join(" ")}return this.parseComplex(t.style,g,_,o,a)},formatter:ee}),ge("backgroundSize",{defaultValue:"0 0",formatter:ee}),ge("perspective",{defaultValue:"0px",prefix:!0}),ge("perspectiveOrigin",{defaultValue:"50% 50%",prefix:!0}),ge("transformStyle",{prefix:!0}),ge("backfaceVisibility",{prefix:!0}),ge("userSelect",{prefix:!0}),ge("margin",{parser:ce("marginTop,marginRight,marginBottom,marginLeft")}),ge("padding",{parser:ce("paddingTop,paddingRight,paddingBottom,paddingLeft")}),ge("clip",{defaultValue:"rect(0px,0px,0px,0px)",parser:function(t,e,n,i,o,a){var s,l,u;return 9>d?(l=t.currentStyle,u=8>d?" ":",",s="rect("+l.clipTop+u+l.clipRight+u+l.clipBottom+u+l.clipLeft+")",e=this.format(e).split(",").join(u)):(s=this.format(W(t,this.p,r,!1,this.dflt)),e=this.format(e)),this.parseComplex(t.style,s,e,o,a)}}),ge("textShadow",{defaultValue:"0px 0px 0px #999",color:!0,multi:!0}),ge("autoRound,strictUnits",{parser:function(t,e,n,i,r){return r}}),ge("border",{defaultValue:"0px solid #000",parser:function(t,e,n,i,o,a){return this.parseComplex(t.style,this.format(W(t,"borderTopWidth",r,!1,"0px")+" "+W(t,"borderTopStyle",r,!1,"solid")+" "+W(t,"borderTopColor",r,!1,"#000")),this.format(e),o,a)},color:!0,formatter:function(t){var e=t.split(" ");return e[0]+" "+(e[1]||"solid")+" "+(t.match(le)||["#000"])[0]}}),ge("borderWidth",{parser:ce("borderTopWidth,borderRightWidth,borderBottomWidth,borderLeftWidth")}),ge("float,cssFloat,styleFloat",{parser:function(t,e,n,i,r){var o=t.style,a="cssFloat"in o?"cssFloat":"styleFloat";return new pe(o,a,0,0,r,-1,n,!1,0,o[a],e)}});var Ae=function(t){var e,n=this.t,i=n.filter||W(this.data,"filter"),r=0|this.s+this.c*t;100===r&&(-1===i.indexOf("atrix(")&&-1===i.indexOf("radient(")&&-1===i.indexOf("oader(")?(n.removeAttribute("filter"),e=!W(this.data,"filter")):(n.filter=i.replace($,""),e=!0)),e||(this.xn1&&(n.filter=i=i||"alpha(opacity="+r+")"),-1===i.indexOf("opacity")?0===r&&this.xn1||(n.filter=i+" alpha(opacity="+r+")"):n.filter=i.replace(b,"opacity="+r))};ge("opacity,alpha,autoAlpha",{defaultValue:"1",parser:function(t,e,n,i,o,a){var s=parseFloat(W(t,"opacity",r,!1,"1")),l=t.style,u="autoAlpha"===n;return"string"==typeof e&&"="===e.charAt(1)&&(e=("-"===e.charAt(0)?-1:1)*parseFloat(e.substr(2))+s),u&&1===s&&"hidden"===W(t,"visibility",r)&&0!==e&&(s=0),V?o=new pe(l,"opacity",s,e-s,o):(o=new pe(l,"opacity",100*s,100*(e-s),o),o.xn1=u?1:0,l.zoom=1,o.type=2,o.b="alpha(opacity="+o.s+")",o.e="alpha(opacity="+(o.s+o.c)+")",o.data=t,o.plugin=a,o.setRatio=Ae),u&&(o=new pe(l,"visibility",0,0,o,-1,null,!1,0,0!==s?"inherit":"hidden",0===e?"hidden":"inherit"),o.xs0="inherit",i._overwriteProps.push(o.n),i._overwriteProps.push(n)),o}});var De=function(t,e){e&&(t.removeProperty?("ms"===e.substr(0,2)&&(e="M"+e.substr(1)),t.removeProperty(e.replace(T,"-$1").toLowerCase())):t.removeAttribute(e))},Ee=function(t){if(this.t._gsClassPT=this,1===t||0===t){this.t.className=0===t?this.b:this.e;for(var e=this.data,n=this.t.style;e;)e.v?n[e.p]=e.v:De(n,e.p),e=e._next;1===t&&this.t._gsClassPT===this&&(this.t._gsClassPT=null)}else this.t.className!==this.e&&(this.t.className=this.e)};ge("className",{parser:function(t,e,i,o,a,s,l){var u,c,f,p,h,d=t.className,m=t.style.cssText;if(a=o._classNamePT=new pe(t,i,0,0,a,2),a.setRatio=Ee,a.pr=-11,n=!0,a.b=d,c=G(t,r),f=t._gsClassPT){for(p={},h=f.data;h;)p[h.p]=1,h=h._next;f.setRatio(1)}return t._gsClassPT=a,a.e="="!==e.charAt(1)?e:d.replace(RegExp("\\s*\\b"+e.substr(2)+"\\b"),"")+("+"===e.charAt(0)?" "+e.substr(2):""),o._tween._duration&&(t.className=a.e,u=Z(t,c,G(t),l,p),t.className=d,a.data=u.firstMPT,t.style.cssText=m,a=a.xfirst=o.parse(t,u.difs,a,s)),a}});var Pe=function(t){if((1===t||0===t)&&this.data._totalTime===this.data._totalDuration&&"isFromStart"!==this.data.data){var e,n,i,r,o=this.t.style,a=s.transform.parse;if("all"===this.e)o.cssText="",r=!0;else for(e=this.e.split(","),i=e.length;--i>-1;)n=e[i],s[n]&&(s[n].parse===a?r=!0:n="transformOrigin"===n?we:s[n].p),De(o,n);r&&(De(o,ye),this.t._gsTransform&&delete this.t._gsTransform)
}};for(ge("clearProps",{parser:function(t,e,i,r,o){return o=new pe(t,i,0,0,o,2),o.setRatio=Pe,o.e=e,o.pr=-10,o.data=r._tween,n=!0,o}}),l="bezier,throwProps,physicsProps,physics2D".split(","),de=l.length;de--;)_e(l[de]);l=a.prototype,l._firstPT=null,l._onInitTween=function(t,e,s){if(!t.nodeType)return!1;this._target=t,this._tween=s,this._vars=e,u=e.autoRound,n=!1,i=e.suffixMap||a.suffixMap,r=z(t,""),o=this._overwriteProps;var l,p,d,m,g,_,v,y,b,$=t.style;if(c&&""===$.zIndex&&(l=W(t,"zIndex",r),("auto"===l||""===l)&&($.zIndex=0)),"string"==typeof e&&(m=$.cssText,l=G(t,r),$.cssText=m+";"+e,l=Z(t,l,G(t)).difs,!V&&w.test(e)&&(l.opacity=parseFloat(RegExp.$1)),e=l,$.cssText=m),this._firstPT=p=this.parse(t,e,null),this._transformType){for(b=3===this._transformType,ye?f&&(c=!0,""===$.zIndex&&(v=W(t,"zIndex",r),("auto"===v||""===v)&&($.zIndex=0)),h&&($.WebkitBackfaceVisibility=this._vars.WebkitBackfaceVisibility||(b?"visible":"hidden"))):$.zoom=1,d=p;d&&d._next;)d=d._next;y=new pe(t,"transform",0,0,null,2),this._linkCSSP(y,null,d),y.setRatio=b&&$e?Ce:ye?ke:Se,y.data=this._transform||Te(t,r,!0),o.pop()}if(n){for(;p;){for(_=p._next,d=m;d&&d.pr>p.pr;)d=d._next;(p._prev=d?d._prev:g)?p._prev._next=p:m=p,(p._next=d)?d._prev=p:g=p,p=_}this._firstPT=m}return!0},l.parse=function(t,e,n,o){var a,l,c,f,p,h,d,m,g,_,v=t.style;for(a in e)h=e[a],l=s[a],l?n=l.parse(t,h,a,this,n,o,e):(p=W(t,a,r)+"",g="string"==typeof h,"color"===a||"fill"===a||"stroke"===a||-1!==a.indexOf("Color")||g&&x.test(h)?(g||(h=se(h),h=(h.length>3?"rgba(":"rgb(")+h.join(",")+")"),n=he(v,a,p,h,!0,"transparent",n,0,o)):!g||-1===h.indexOf(" ")&&-1===h.indexOf(",")?(c=parseFloat(p),d=c||0===c?p.substr((c+"").length):"",(""===p||"auto"===p)&&("width"===a||"height"===a?(c=te(t,a,r),d="px"):"left"===a||"top"===a?(c=K(t,a,r),d="px"):(c="opacity"!==a?0:1,d="")),_=g&&"="===h.charAt(1),_?(f=parseInt(h.charAt(0)+"1",10),h=h.substr(2),f*=parseFloat(h),m=h.replace(y,"")):(f=parseFloat(h),m=g?h.substr((f+"").length)||"":""),""===m&&(m=a in i?i[a]:d),h=f||0===f?(_?f+c:f)+m:e[a],d!==m&&""!==m&&(f||0===f)&&c&&(c=q(t,a,c,d),"%"===m?(c/=q(t,a,100,"%")/100,e.strictUnits!==!0&&(p=c+"%")):"em"===m?c/=q(t,a,1,"em"):"px"!==m&&(f=q(t,a,f,m),m="px"),_&&(f||0===f)&&(h=f+c+m)),_&&(f+=c),!c&&0!==c||!f&&0!==f?void 0!==v[a]&&(h||"NaN"!=h+""&&null!=h)?(n=new pe(v,a,f||c||0,0,n,-1,a,!1,0,p,h),n.xs0="none"!==h||"display"!==a&&-1===a.indexOf("Style")?h:p):j("invalid "+a+" tween value: "+e[a]):(n=new pe(v,a,c,f-c,n,0,a,u!==!1&&("px"===m||"zIndex"===a),0,p,h),n.xs0=m)):n=he(v,a,p,h,!0,null,n,0,o)),o&&n&&!n.plugin&&(n.plugin=o);return n},l.setRatio=function(t){var e,n,i,r=this._firstPT,o=1e-6;if(1!==t||this._tween._time!==this._tween._duration&&0!==this._tween._time)if(t||this._tween._time!==this._tween._duration&&0!==this._tween._time||this._tween._rawPrevTime===-1e-6)for(;r;){if(e=r.c*t+r.s,r.r?e=Math.round(e):o>e&&e>-o&&(e=0),r.type)if(1===r.type)if(i=r.l,2===i)r.t[r.p]=r.xs0+e+r.xs1+r.xn1+r.xs2;else if(3===i)r.t[r.p]=r.xs0+e+r.xs1+r.xn1+r.xs2+r.xn2+r.xs3;else if(4===i)r.t[r.p]=r.xs0+e+r.xs1+r.xn1+r.xs2+r.xn2+r.xs3+r.xn3+r.xs4;else if(5===i)r.t[r.p]=r.xs0+e+r.xs1+r.xn1+r.xs2+r.xn2+r.xs3+r.xn3+r.xs4+r.xn4+r.xs5;else{for(n=r.xs0+e+r.xs1,i=1;r.l>i;i++)n+=r["xn"+i]+r["xs"+(i+1)];r.t[r.p]=n}else-1===r.type?r.t[r.p]=r.xs0:r.setRatio&&r.setRatio(t);else r.t[r.p]=e+r.xs0;r=r._next}else for(;r;)2!==r.type?r.t[r.p]=r.b:r.setRatio(t),r=r._next;else for(;r;)2!==r.type?r.t[r.p]=r.e:r.setRatio(t),r=r._next},l._enableTransforms=function(t){this._transformType=t||3===this._transformType?3:2,this._transform=this._transform||Te(this._target,r,!0)},l._linkCSSP=function(t,e,n,i){return t&&(e&&(e._prev=t),t._next&&(t._next._prev=t._prev),t._prev?t._prev._next=t._next:this._firstPT===t&&(this._firstPT=t._next,i=!0),n?n._next=t:i||null!==this._firstPT||(this._firstPT=t),t._next=e,t._prev=n),t},l._kill=function(e){var n,i,r,o=e;if(e.autoAlpha||e.alpha){o={};for(i in e)o[i]=e[i];o.opacity=1,o.autoAlpha&&(o.visibility=1)}return e.className&&(n=this._classNamePT)&&(r=n.xfirst,r&&r._prev?this._linkCSSP(r._prev,n._next,r._prev._prev):r===this._firstPT&&(this._firstPT=n._next),n._next&&this._linkCSSP(n._next,n._next._next,r._prev),this._classNamePT=null),t.prototype._kill.call(this,o)};var Me=function(t,e,n){var i,r,o,a;if(t.slice)for(r=t.length;--r>-1;)Me(t[r],e,n);else for(i=t.childNodes,r=i.length;--r>-1;)o=i[r],a=o.type,o.style&&(e.push(G(o)),n&&n.push(o)),1!==a&&9!==a&&11!==a||!o.childNodes.length||Me(o,e,n)};return a.cascadeTo=function(t,n,i){var r,o,a,s=e.to(t,n,i),l=[s],u=[],c=[],f=[],p=e._internals.reservedProps;for(t=s._targets||s.target,Me(t,u,f),s.render(n,!0),Me(t,c),s.render(0,!0),s._enabled(!0),r=f.length;--r>-1;)if(o=Z(f[r],u[r],c[r]),o.firstMPT){o=o.difs;for(a in i)p[a]&&(o[a]=i[a]);l.push(e.to(f[r],n,o))}return l},t.activate([a]),a},!0),function(){var t=window._gsDefine.plugin({propName:"roundProps",priority:-1,API:2,init:function(t,e,n){return this._tween=n,!0}}),e=t.prototype;e._onInitAllProps=function(){for(var t,e,n,i=this._tween,r=i.vars.roundProps instanceof Array?i.vars.roundProps:i.vars.roundProps.split(","),o=r.length,a={},s=i._propLookup.roundProps;--o>-1;)a[r[o]]=1;for(o=r.length;--o>-1;)for(t=r[o],e=i._firstPT;e;)n=e._next,e.pg?e.t._roundProps(a,!0):e.n===t&&(this._add(e.t,t,e.s,e.c),n&&(n._prev=e._prev),e._prev?e._prev._next=n:i._firstPT===e&&(i._firstPT=n),e._next=e._prev=null,i._propLookup[t]=s),e=n;return!1},e._add=function(t,e,n,i){this._addTween(t,e,n,n+i,e,!0),this._overwriteProps.push(e)}}(),window._gsDefine.plugin({propName:"attr",API:2,version:"0.3.0",init:function(t,e,n){var i,r,o;if("function"!=typeof t.setAttribute)return!1;this._target=t,this._proxy={},this._start={},this._end={},this._endRatio=n.vars.runBackwards?0:1;for(i in e)this._start[i]=this._proxy[i]=r=t.getAttribute(i),this._end[i]=o=e[i],this._addTween(this._proxy,i,parseFloat(r),o,i),this._overwriteProps.push(i);return!0},set:function(t){this._super.setRatio.call(this,t);for(var e,n=this._overwriteProps,i=n.length,r=0!==t&&1!==t?this._proxy:t===this._endRatio?this._end:this._start;--i>-1;)e=n[i],this._target.setAttribute(e,r[e]+"")}}),window._gsDefine.plugin({propName:"directionalRotation",API:2,version:"0.2.0",init:function(t,e){"object"!=typeof e&&(e={rotation:e}),this.finals={};var n,i,r,o,a,s,l=e.useRadians===!0?2*Math.PI:360,u=1e-6;for(n in e)"useRadians"!==n&&(s=(e[n]+"").split("_"),i=s[0],r=parseFloat("function"!=typeof t[n]?t[n]:t[n.indexOf("set")||"function"!=typeof t["get"+n.substr(3)]?n:"get"+n.substr(3)]()),o=this.finals[n]="string"==typeof i&&"="===i.charAt(1)?r+parseInt(i.charAt(0)+"1",10)*Number(i.substr(2)):Number(i)||0,a=o-r,s.length&&(i=s.join("_"),-1!==i.indexOf("short")&&(a%=l,a!==a%(l/2)&&(a=0>a?a+l:a-l)),-1!==i.indexOf("_cw")&&0>a?a=(a+9999999999*l)%l-(0|a/l)*l:-1!==i.indexOf("ccw")&&a>0&&(a=(a-9999999999*l)%l-(0|a/l)*l)),(a>u||-u>a)&&(this._addTween(t,n,r,r+a,n),this._overwriteProps.push(n)));return!0},set:function(t){var e;if(1!==t)this._super.setRatio.call(this,t);else for(e=this._firstPT;e;)e.f?e.t[e.p](this.finals[e.p]):e.t[e.p]=this.finals[e.p],e=e._next}})._autoCSS=!0,window._gsDefine("easing.Back",["easing.Ease"],function(t){var e,n,i,r=window.GreenSockGlobals||window,o=r.com.greensock,a=2*Math.PI,s=Math.PI/2,l=o._class,u=function(e,n){var i=l("easing."+e,function(){},!0),r=i.prototype=new t;return r.constructor=i,r.getRatio=n,i},c=t.register||function(){},f=function(t,e,n,i){var r=l("easing."+t,{easeOut:new e,easeIn:new n,easeInOut:new i},!0);return c(r,t),r},p=function(t,e,n){this.t=t,this.v=e,n&&(this.next=n,n.prev=this,this.c=n.v-e,this.gap=n.t-t)},h=function(e,n){var i=l("easing."+e,function(t){this._p1=t||0===t?t:1.70158,this._p2=1.525*this._p1},!0),r=i.prototype=new t;return r.constructor=i,r.getRatio=n,r.config=function(t){return new i(t)},i},d=f("Back",h("BackOut",function(t){return(t-=1)*t*((this._p1+1)*t+this._p1)+1}),h("BackIn",function(t){return t*t*((this._p1+1)*t-this._p1)}),h("BackInOut",function(t){return 1>(t*=2)?.5*t*t*((this._p2+1)*t-this._p2):.5*((t-=2)*t*((this._p2+1)*t+this._p2)+2)})),m=l("easing.SlowMo",function(t,e,n){e=e||0===e?e:.7,null==t?t=.7:t>1&&(t=1),this._p=1!==t?e:0,this._p1=(1-t)/2,this._p2=t,this._p3=this._p1+this._p2,this._calcEnd=n===!0},!0),g=m.prototype=new t;return g.constructor=m,g.getRatio=function(t){var e=t+(.5-t)*this._p;return this._p1>t?this._calcEnd?1-(t=1-t/this._p1)*t:e-(t=1-t/this._p1)*t*t*t*e:t>this._p3?this._calcEnd?1-(t=(t-this._p3)/this._p1)*t:e+(t-e)*(t=(t-this._p3)/this._p1)*t*t*t:this._calcEnd?1:e},m.ease=new m(.7,.7),g.config=m.config=function(t,e,n){return new m(t,e,n)},e=l("easing.SteppedEase",function(t){t=t||1,this._p1=1/t,this._p2=t+1},!0),g=e.prototype=new t,g.constructor=e,g.getRatio=function(t){return 0>t?t=0:t>=1&&(t=.999999999),(this._p2*t>>0)*this._p1},g.config=e.config=function(t){return new e(t)},n=l("easing.RoughEase",function(e){e=e||{};for(var n,i,r,o,a,s,l=e.taper||"none",u=[],c=0,f=0|(e.points||20),h=f,d=e.randomize!==!1,m=e.clamp===!0,g=e.template instanceof t?e.template:null,_="number"==typeof e.strength?.4*e.strength:.4;--h>-1;)n=d?Math.random():1/f*h,i=g?g.getRatio(n):n,"none"===l?r=_:"out"===l?(o=1-n,r=o*o*_):"in"===l?r=n*n*_:.5>n?(o=2*n,r=.5*o*o*_):(o=2*(1-n),r=.5*o*o*_),d?i+=Math.random()*r-.5*r:h%2?i+=.5*r:i-=.5*r,m&&(i>1?i=1:0>i&&(i=0)),u[c++]={x:n,y:i};for(u.sort(function(t,e){return t.x-e.x}),s=new p(1,1,null),h=f;--h>-1;)a=u[h],s=new p(a.x,a.y,s);this._prev=new p(0,0,0!==s.t?s:s.next)},!0),g=n.prototype=new t,g.constructor=n,g.getRatio=function(t){var e=this._prev;if(t>e.t){for(;e.next&&t>=e.t;)e=e.next;e=e.prev}else for(;e.prev&&e.t>=t;)e=e.prev;return this._prev=e,e.v+(t-e.t)/e.gap*e.c},g.config=function(t){return new n(t)},n.ease=new n,f("Bounce",u("BounceOut",function(t){return 1/2.75>t?7.5625*t*t:2/2.75>t?7.5625*(t-=1.5/2.75)*t+.75:2.5/2.75>t?7.5625*(t-=2.25/2.75)*t+.9375:7.5625*(t-=2.625/2.75)*t+.984375}),u("BounceIn",function(t){return 1/2.75>(t=1-t)?1-7.5625*t*t:2/2.75>t?1-(7.5625*(t-=1.5/2.75)*t+.75):2.5/2.75>t?1-(7.5625*(t-=2.25/2.75)*t+.9375):1-(7.5625*(t-=2.625/2.75)*t+.984375)}),u("BounceInOut",function(t){var e=.5>t;return t=e?1-2*t:2*t-1,t=1/2.75>t?7.5625*t*t:2/2.75>t?7.5625*(t-=1.5/2.75)*t+.75:2.5/2.75>t?7.5625*(t-=2.25/2.75)*t+.9375:7.5625*(t-=2.625/2.75)*t+.984375,e?.5*(1-t):.5*t+.5})),f("Circ",u("CircOut",function(t){return Math.sqrt(1-(t-=1)*t)}),u("CircIn",function(t){return-(Math.sqrt(1-t*t)-1)}),u("CircInOut",function(t){return 1>(t*=2)?-.5*(Math.sqrt(1-t*t)-1):.5*(Math.sqrt(1-(t-=2)*t)+1)})),i=function(e,n,i){var r=l("easing."+e,function(t,e){this._p1=t||1,this._p2=e||i,this._p3=this._p2/a*(Math.asin(1/this._p1)||0)},!0),o=r.prototype=new t;return o.constructor=r,o.getRatio=n,o.config=function(t,e){return new r(t,e)},r},f("Elastic",i("ElasticOut",function(t){return this._p1*Math.pow(2,-10*t)*Math.sin((t-this._p3)*a/this._p2)+1},.3),i("ElasticIn",function(t){return-(this._p1*Math.pow(2,10*(t-=1))*Math.sin((t-this._p3)*a/this._p2))},.3),i("ElasticInOut",function(t){return 1>(t*=2)?-.5*this._p1*Math.pow(2,10*(t-=1))*Math.sin((t-this._p3)*a/this._p2):.5*this._p1*Math.pow(2,-10*(t-=1))*Math.sin((t-this._p3)*a/this._p2)+1},.45)),f("Expo",u("ExpoOut",function(t){return 1-Math.pow(2,-10*t)}),u("ExpoIn",function(t){return Math.pow(2,10*(t-1))-.001}),u("ExpoInOut",function(t){return 1>(t*=2)?.5*Math.pow(2,10*(t-1)):.5*(2-Math.pow(2,-10*(t-1)))})),f("Sine",u("SineOut",function(t){return Math.sin(t*s)}),u("SineIn",function(t){return-Math.cos(t*s)+1}),u("SineInOut",function(t){return-.5*(Math.cos(Math.PI*t)-1)})),l("easing.EaseLookup",{find:function(e){return t.map[e]}},!0),c(r.SlowMo,"SlowMo","ease,"),c(n,"RoughEase","ease,"),c(e,"SteppedEase","ease,"),d},!0)}),function(t){"use strict";var e=t.GreenSockGlobals||t;if(!e.TweenLite){var n,i,r,o,a,s=function(t){var n,i=t.split("."),r=e;for(n=0;i.length>n;n++)r[i[n]]=r=r[i[n]]||{};return r},l=s("com.greensock"),u=1e-10,c=[].slice,f=function(){},p=function(){var t=Object.prototype.toString,e=t.call([]);return function(n){return null!=n&&(n instanceof Array||"object"==typeof n&&!!n.push&&t.call(n)===e)}}(),h={},d=function(n,i,r,o){this.sc=h[n]?h[n].sc:[],h[n]=this,this.gsClass=null,this.func=r;var a=[];this.check=function(l){for(var u,c,f,p,m=i.length,g=m;--m>-1;)(u=h[i[m]]||new d(i[m],[])).gsClass?(a[m]=u.gsClass,g--):l&&u.sc.push(this);if(0===g&&r)for(c=("com.greensock."+n).split("."),f=c.pop(),p=s(c.join("."))[f]=this.gsClass=r.apply(r,a),o&&(e[f]=p,"function"==typeof define&&define.amd?define((t.GreenSockAMDPath?t.GreenSockAMDPath+"/":"")+n.split(".").join("/"),[],function(){return p}):"undefined"!=typeof module&&module.exports&&(module.exports=p)),m=0;this.sc.length>m;m++)this.sc[m].check()},this.check(!0)},m=t._gsDefine=function(t,e,n,i){return new d(t,e,n,i)},g=l._class=function(t,e,n){return e=e||function(){},m(t,[],function(){return e},n),e};m.globals=e;var _=[0,0,1,1],v=[],y=g("easing.Ease",function(t,e,n,i){this._func=t,this._type=n||0,this._power=i||0,this._params=e?_.concat(e):_},!0),b=y.map={},w=y.register=function(t,e,n,i){for(var r,o,a,s,u=e.split(","),c=u.length,f=(n||"easeIn,easeOut,easeInOut").split(",");--c>-1;)for(o=u[c],r=i?g("easing."+o,null,!0):l.easing[o]||{},a=f.length;--a>-1;)s=f[a],b[o+"."+s]=b[s+o]=r[s]=t.getRatio?t:t[s]||new t};for(r=y.prototype,r._calcEnd=!1,r.getRatio=function(t){if(this._func)return this._params[0]=t,this._func.apply(null,this._params);var e=this._type,n=this._power,i=1===e?1-t:2===e?t:.5>t?2*t:2*(1-t);return 1===n?i*=i:2===n?i*=i*i:3===n?i*=i*i*i:4===n&&(i*=i*i*i*i),1===e?1-i:2===e?i:.5>t?i/2:1-i/2},n=["Linear","Quad","Cubic","Quart","Quint,Strong"],i=n.length;--i>-1;)r=n[i]+",Power"+i,w(new y(null,null,1,i),r,"easeOut",!0),w(new y(null,null,2,i),r,"easeIn"+(0===i?",easeNone":"")),w(new y(null,null,3,i),r,"easeInOut");b.linear=l.easing.Linear.easeIn,b.swing=l.easing.Quad.easeInOut;var $=g("events.EventDispatcher",function(t){this._listeners={},this._eventTarget=t||this});r=$.prototype,r.addEventListener=function(t,e,n,i,r){r=r||0;var s,l,u=this._listeners[t],c=0;for(null==u&&(this._listeners[t]=u=[]),l=u.length;--l>-1;)s=u[l],s.c===e&&s.s===n?u.splice(l,1):0===c&&r>s.pr&&(c=l+1);u.splice(c,0,{c:e,s:n,up:i,pr:r}),this!==o||a||o.wake()},r.removeEventListener=function(t,e){var n,i=this._listeners[t];if(i)for(n=i.length;--n>-1;)if(i[n].c===e)return void i.splice(n,1)},r.dispatchEvent=function(t){var e,n,i,r=this._listeners[t];if(r)for(e=r.length,n=this._eventTarget;--e>-1;)i=r[e],i.up?i.c.call(i.s||n,{type:t,target:n}):i.c.call(i.s||n)};var x=t.requestAnimationFrame,T=t.cancelAnimationFrame,S=Date.now||function(){return(new Date).getTime()},C=S();for(n=["ms","moz","webkit","o"],i=n.length;--i>-1&&!x;)x=t[n[i]+"RequestAnimationFrame"],T=t[n[i]+"CancelAnimationFrame"]||t[n[i]+"CancelRequestAnimationFrame"];g("Ticker",function(t,e){var n,i,r,s,l,u=this,c=S(),p=e!==!1&&x,h=function(t){C=S(),u.time=(C-c)/1e3;var e,o=u.time-l;(!n||o>0||t===!0)&&(u.frame++,l+=o+(o>=s?.004:s-o),e=!0),t!==!0&&(r=i(h)),e&&u.dispatchEvent("tick")};$.call(u),u.time=u.frame=0,u.tick=function(){h(!0)},u.sleep=function(){null!=r&&(p&&T?T(r):clearTimeout(r),i=f,r=null,u===o&&(a=!1))},u.wake=function(){null!==r&&u.sleep(),i=0===n?f:p&&x?x:function(t){return setTimeout(t,0|1e3*(l-u.time)+1)},u===o&&(a=!0),h(2)},u.fps=function(t){return arguments.length?(n=t,s=1/(n||60),l=this.time+s,void u.wake()):n},u.useRAF=function(t){return arguments.length?(u.sleep(),p=t,void u.fps(n)):p},u.fps(t),setTimeout(function(){p&&(!r||5>u.frame)&&u.useRAF(!1)},1500)}),r=l.Ticker.prototype=new l.events.EventDispatcher,r.constructor=l.Ticker;var k=g("core.Animation",function(t,e){if(this.vars=e=e||{},this._duration=this._totalDuration=t||0,this._delay=Number(e.delay)||0,this._timeScale=1,this._active=e.immediateRender===!0,this.data=e.data,this._reversed=e.reversed===!0,V){a||o.wake();var n=this.vars.useFrames?Y:V;n.add(this,n._time),this.vars.paused&&this.paused(!0)}});o=k.ticker=new l.Ticker,r=k.prototype,r._dirty=r._gc=r._initted=r._paused=!1,r._totalTime=r._time=0,r._rawPrevTime=-1,r._next=r._last=r._onUpdate=r._timeline=r.timeline=null,r._paused=!1;var A=function(){a&&S()-C>2e3&&o.wake(),setTimeout(A,2e3)};A(),r.play=function(t,e){return null!=t&&this.seek(t,e),this.reversed(!1).paused(!1)},r.pause=function(t,e){return null!=t&&this.seek(t,e),this.paused(!0)},r.resume=function(t,e){return null!=t&&this.seek(t,e),this.paused(!1)},r.seek=function(t,e){return this.totalTime(Number(t),e!==!1)},r.restart=function(t,e){return this.reversed(!1).paused(!1).totalTime(t?-this._delay:0,e!==!1,!0)},r.reverse=function(t,e){return null!=t&&this.seek(t||this.totalDuration(),e),this.reversed(!0).paused(!1)},r.render=function(){},r.invalidate=function(){return this},r.isActive=function(){var t,e=this._timeline,n=this._startTime;return!e||!this._gc&&!this._paused&&e.isActive()&&(t=e.rawTime())>=n&&n+this.totalDuration()/this._timeScale>t},r._enabled=function(t,e){return a||o.wake(),this._gc=!t,this._active=this.isActive(),e!==!0&&(t&&!this.timeline?this._timeline.add(this,this._startTime-this._delay):!t&&this.timeline&&this._timeline._remove(this,!0)),!1},r._kill=function(){return this._enabled(!1,!1)},r.kill=function(t,e){return this._kill(t,e),this},r._uncache=function(t){for(var e=t?this:this.timeline;e;)e._dirty=!0,e=e.timeline;return this},r._swapSelfInParams=function(t){for(var e=t.length,n=t.concat();--e>-1;)"{self}"===t[e]&&(n[e]=this);return n},r.eventCallback=function(t,e,n,i){if("on"===(t||"").substr(0,2)){var r=this.vars;if(1===arguments.length)return r[t];null==e?delete r[t]:(r[t]=e,r[t+"Params"]=p(n)&&-1!==n.join("").indexOf("{self}")?this._swapSelfInParams(n):n,r[t+"Scope"]=i),"onUpdate"===t&&(this._onUpdate=e)}return this},r.delay=function(t){return arguments.length?(this._timeline.smoothChildTiming&&this.startTime(this._startTime+t-this._delay),this._delay=t,this):this._delay},r.duration=function(t){return arguments.length?(this._duration=this._totalDuration=t,this._uncache(!0),this._timeline.smoothChildTiming&&this._time>0&&this._time<this._duration&&0!==t&&this.totalTime(this._totalTime*(t/this._duration),!0),this):(this._dirty=!1,this._duration)},r.totalDuration=function(t){return this._dirty=!1,arguments.length?this.duration(t):this._totalDuration},r.time=function(t,e){return arguments.length?(this._dirty&&this.totalDuration(),this.totalTime(t>this._duration?this._duration:t,e)):this._time},r.totalTime=function(t,e,n){if(a||o.wake(),!arguments.length)return this._totalTime;if(this._timeline){if(0>t&&!n&&(t+=this.totalDuration()),this._timeline.smoothChildTiming){this._dirty&&this.totalDuration();var i=this._totalDuration,r=this._timeline;if(t>i&&!n&&(t=i),this._startTime=(this._paused?this._pauseTime:r._time)-(this._reversed?i-t:t)/this._timeScale,r._dirty||this._uncache(!1),r._timeline)for(;r._timeline;)r._timeline._time!==(r._startTime+r._totalTime)/r._timeScale&&r.totalTime(r._totalTime,!0),r=r._timeline}this._gc&&this._enabled(!0,!1),(this._totalTime!==t||0===this._duration)&&this.render(t,e,!1)}return this},r.progress=r.totalProgress=function(t,e){return arguments.length?this.totalTime(this.duration()*t,e):this._time/this.duration()},r.startTime=function(t){return arguments.length?(t!==this._startTime&&(this._startTime=t,this.timeline&&this.timeline._sortChildren&&this.timeline.add(this,t-this._delay)),this):this._startTime},r.timeScale=function(t){if(!arguments.length)return this._timeScale;if(t=t||u,this._timeline&&this._timeline.smoothChildTiming){var e=this._pauseTime,n=e||0===e?e:this._timeline.totalTime();this._startTime=n-(n-this._startTime)*this._timeScale/t}return this._timeScale=t,this._uncache(!1)},r.reversed=function(t){return arguments.length?(t!=this._reversed&&(this._reversed=t,this.totalTime(this._timeline&&!this._timeline.smoothChildTiming?this.totalDuration()-this._totalTime:this._totalTime,!0)),this):this._reversed},r.paused=function(t){if(!arguments.length)return this._paused;if(t!=this._paused&&this._timeline){a||t||o.wake();var e=this._timeline,n=e.rawTime(),i=n-this._pauseTime;!t&&e.smoothChildTiming&&(this._startTime+=i,this._uncache(!1)),this._pauseTime=t?n:null,this._paused=t,this._active=this.isActive(),!t&&0!==i&&this._initted&&this.duration()&&this.render(e.smoothChildTiming?this._totalTime:(n-this._startTime)/this._timeScale,!0,!0)}return this._gc&&!t&&this._enabled(!0,!1),this};var D=g("core.SimpleTimeline",function(t){k.call(this,0,t),this.autoRemoveChildren=this.smoothChildTiming=!0});r=D.prototype=new k,r.constructor=D,r.kill()._gc=!1,r._first=r._last=null,r._sortChildren=!1,r.add=r.insert=function(t,e){var n,i;if(t._startTime=Number(e||0)+t._delay,t._paused&&this!==t._timeline&&(t._pauseTime=t._startTime+(this.rawTime()-t._startTime)/t._timeScale),t.timeline&&t.timeline._remove(t,!0),t.timeline=t._timeline=this,t._gc&&t._enabled(!0,!0),n=this._last,this._sortChildren)for(i=t._startTime;n&&n._startTime>i;)n=n._prev;return n?(t._next=n._next,n._next=t):(t._next=this._first,this._first=t),t._next?t._next._prev=t:this._last=t,t._prev=n,this._timeline&&this._uncache(!0),this},r._remove=function(t,e){return t.timeline===this&&(e||t._enabled(!1,!0),t.timeline=null,t._prev?t._prev._next=t._next:this._first===t&&(this._first=t._next),t._next?t._next._prev=t._prev:this._last===t&&(this._last=t._prev),this._timeline&&this._uncache(!0)),this},r.render=function(t,e,n){var i,r=this._first;for(this._totalTime=this._time=this._rawPrevTime=t;r;)i=r._next,(r._active||t>=r._startTime&&!r._paused)&&(r._reversed?r.render((r._dirty?r.totalDuration():r._totalDuration)-(t-r._startTime)*r._timeScale,e,n):r.render((t-r._startTime)*r._timeScale,e,n)),r=i},r.rawTime=function(){return a||o.wake(),this._totalTime};var E=g("TweenLite",function(e,n,i){if(k.call(this,n,i),this.render=E.prototype.render,null==e)throw"Cannot tween a null target.";this.target=e="string"!=typeof e?e:E.selector(e)||e;var r,o,a,s=e.jquery||e.length&&e!==t&&e[0]&&(e[0]===t||e[0].nodeType&&e[0].style&&!e.nodeType),l=this.vars.overwrite;if(this._overwrite=l=null==l?L[E.defaultOverwrite]:"number"==typeof l?l>>0:L[l],(s||e instanceof Array||e.push&&p(e))&&"number"!=typeof e[0])for(this._targets=a=c.call(e,0),this._propLookup=[],this._siblings=[],r=0;a.length>r;r++)o=a[r],o?"string"!=typeof o?o.length&&o!==t&&o[0]&&(o[0]===t||o[0].nodeType&&o[0].style&&!o.nodeType)?(a.splice(r--,1),this._targets=a=a.concat(c.call(o,0))):(this._siblings[r]=X(o,this,!1),1===l&&this._siblings[r].length>1&&j(o,this,null,1,this._siblings[r])):(o=a[r--]=E.selector(o),"string"==typeof o&&a.splice(r+1,1)):a.splice(r--,1);else this._propLookup={},this._siblings=X(e,this,!1),1===l&&this._siblings.length>1&&j(e,this,null,1,this._siblings);(this.vars.immediateRender||0===n&&0===this._delay&&this.vars.immediateRender!==!1)&&this.render(-this._delay,!1,!0)},!0),P=function(e){return e.length&&e!==t&&e[0]&&(e[0]===t||e[0].nodeType&&e[0].style&&!e.nodeType)},M=function(t,e){var n,i={};for(n in t)F[n]||n in e&&"x"!==n&&"y"!==n&&"width"!==n&&"height"!==n&&"className"!==n&&"border"!==n||!(!R[n]||R[n]&&R[n]._autoCSS)||(i[n]=t[n],delete t[n]);t.css=i};r=E.prototype=new k,r.constructor=E,r.kill()._gc=!1,r.ratio=0,r._firstPT=r._targets=r._overwrittenProps=r._startAt=null,r._notifyPluginsOfEnabled=!1,E.version="1.11.8",E.defaultEase=r._ease=new y(null,null,1,1),E.defaultOverwrite="auto",E.ticker=o,E.autoSleep=!0,E.selector=t.$||t.jQuery||function(e){return t.$?(E.selector=t.$,t.$(e)):t.document?t.document.getElementById("#"===e.charAt(0)?e.substr(1):e):e};var O=E._internals={isArray:p,isSelector:P},R=E._plugins={},N=E._tweenLookup={},I=0,F=O.reservedProps={ease:1,delay:1,overwrite:1,onComplete:1,onCompleteParams:1,onCompleteScope:1,useFrames:1,runBackwards:1,startAt:1,onUpdate:1,onUpdateParams:1,onUpdateScope:1,onStart:1,onStartParams:1,onStartScope:1,onReverseComplete:1,onReverseCompleteParams:1,onReverseCompleteScope:1,onRepeat:1,onRepeatParams:1,onRepeatScope:1,easeParams:1,yoyo:1,immediateRender:1,repeat:1,repeatDelay:1,data:1,paused:1,reversed:1,autoCSS:1},L={none:0,all:1,auto:2,concurrent:3,allOnStart:4,preexisting:5,"true":1,"false":0},Y=k._rootFramesTimeline=new D,V=k._rootTimeline=new D;V._startTime=o.time,Y._startTime=o.frame,V._active=Y._active=!0,k._updateRoot=function(){if(V.render((o.time-V._startTime)*V._timeScale,!1,!1),Y.render((o.frame-Y._startTime)*Y._timeScale,!1,!1),!(o.frame%120)){var t,e,n;for(n in N){for(e=N[n].tweens,t=e.length;--t>-1;)e[t]._gc&&e.splice(t,1);0===e.length&&delete N[n]}if(n=V._first,(!n||n._paused)&&E.autoSleep&&!Y._first&&1===o._listeners.tick.length){for(;n&&n._paused;)n=n._next;n||o.sleep()}}},o.addEventListener("tick",k._updateRoot);var X=function(t,e,n){var i,r,o=t._gsTweenID;if(N[o||(t._gsTweenID=o="t"+I++)]||(N[o]={target:t,tweens:[]}),e&&(i=N[o].tweens,i[r=i.length]=e,n))for(;--r>-1;)i[r]===e&&i.splice(r,1);return N[o].tweens},j=function(t,e,n,i,r){var o,a,s,l;if(1===i||i>=4){for(l=r.length,o=0;l>o;o++)if((s=r[o])!==e)s._gc||s._enabled(!1,!1)&&(a=!0);else if(5===i)break;return a}var c,f=e._startTime+u,p=[],h=0,d=0===e._duration;for(o=r.length;--o>-1;)(s=r[o])===e||s._gc||s._paused||(s._timeline!==e._timeline?(c=c||B(e,0,d),0===B(s,c,d)&&(p[h++]=s)):f>=s._startTime&&s._startTime+s.totalDuration()/s._timeScale>f&&((d||!s._initted)&&2e-10>=f-s._startTime||(p[h++]=s)));for(o=h;--o>-1;)s=p[o],2===i&&s._kill(n,t)&&(a=!0),(2!==i||!s._firstPT&&s._initted)&&s._enabled(!1,!1)&&(a=!0);return a},B=function(t,e,n){for(var i=t._timeline,r=i._timeScale,o=t._startTime;i._timeline;){if(o+=i._startTime,r*=i._timeScale,i._paused)return-100;i=i._timeline}return o/=r,o>e?o-e:n&&o===e||!t._initted&&2*u>o-e?u:(o+=t.totalDuration()/t._timeScale/r)>e+u?0:o-e-u};r._init=function(){var t,e,n,i,r=this.vars,o=this._overwrittenProps,a=this._duration,s=r.immediateRender,l=r.ease;if(r.startAt){if(this._startAt&&this._startAt.render(-1,!0),r.startAt.overwrite=0,r.startAt.immediateRender=!0,this._startAt=E.to(this.target,0,r.startAt),s)if(this._time>0)this._startAt=null;else if(0!==a)return}else if(r.runBackwards&&0!==a)if(this._startAt)this._startAt.render(-1,!0),this._startAt=null;else{n={};for(i in r)F[i]&&"autoCSS"!==i||(n[i]=r[i]);if(n.overwrite=0,n.data="isFromStart",this._startAt=E.to(this.target,0,n),r.immediateRender){if(0===this._time)return}else this._startAt.render(-1,!0)}if(this._ease=l?l instanceof y?r.easeParams instanceof Array?l.config.apply(l,r.easeParams):l:"function"==typeof l?new y(l,r.easeParams):b[l]||E.defaultEase:E.defaultEase,this._easeType=this._ease._type,this._easePower=this._ease._power,this._firstPT=null,this._targets)for(t=this._targets.length;--t>-1;)this._initProps(this._targets[t],this._propLookup[t]={},this._siblings[t],o?o[t]:null)&&(e=!0);else e=this._initProps(this.target,this._propLookup,this._siblings,o);if(e&&E._onPluginEvent("_onInitAllProps",this),o&&(this._firstPT||"function"!=typeof this.target&&this._enabled(!1,!1)),r.runBackwards)for(n=this._firstPT;n;)n.s+=n.c,n.c=-n.c,n=n._next;this._onUpdate=r.onUpdate,this._initted=!0},r._initProps=function(e,n,i,r){var o,a,s,l,u,c;if(null==e)return!1;this.vars.css||e.style&&e!==t&&e.nodeType&&R.css&&this.vars.autoCSS!==!1&&M(this.vars,e);for(o in this.vars){if(c=this.vars[o],F[o])c&&(c instanceof Array||c.push&&p(c))&&-1!==c.join("").indexOf("{self}")&&(this.vars[o]=c=this._swapSelfInParams(c,this));else if(R[o]&&(l=new R[o])._onInitTween(e,this.vars[o],this)){for(this._firstPT=u={_next:this._firstPT,t:l,p:"setRatio",s:0,c:1,f:!0,n:o,pg:!0,pr:l._priority},a=l._overwriteProps.length;--a>-1;)n[l._overwriteProps[a]]=this._firstPT;(l._priority||l._onInitAllProps)&&(s=!0),(l._onDisable||l._onEnable)&&(this._notifyPluginsOfEnabled=!0)}else this._firstPT=n[o]=u={_next:this._firstPT,t:e,p:o,f:"function"==typeof e[o],n:o,pg:!1,pr:0},u.s=u.f?e[o.indexOf("set")||"function"!=typeof e["get"+o.substr(3)]?o:"get"+o.substr(3)]():parseFloat(e[o]),u.c="string"==typeof c&&"="===c.charAt(1)?parseInt(c.charAt(0)+"1",10)*Number(c.substr(2)):Number(c)-u.s||0;u&&u._next&&(u._next._prev=u)}return r&&this._kill(r,e)?this._initProps(e,n,i,r):this._overwrite>1&&this._firstPT&&i.length>1&&j(e,this,n,this._overwrite,i)?(this._kill(n,e),this._initProps(e,n,i,r)):s},r.render=function(t,e,n){var i,r,o,a,s=this._time,l=this._duration;if(t>=l)this._totalTime=this._time=l,this.ratio=this._ease._calcEnd?this._ease.getRatio(1):1,this._reversed||(i=!0,r="onComplete"),0===l&&(a=this._rawPrevTime,this._startTime===this._timeline._duration&&(t=0),(0===t||0>a||a===u)&&a!==t&&(n=!0,a>u&&(r="onReverseComplete")),this._rawPrevTime=a=!e||t||this._rawPrevTime===t?t:u);else if(1e-7>t)this._totalTime=this._time=0,this.ratio=this._ease._calcEnd?this._ease.getRatio(0):0,(0!==s||0===l&&this._rawPrevTime>0&&this._rawPrevTime!==u)&&(r="onReverseComplete",i=this._reversed),0>t?(this._active=!1,0===l&&(this._rawPrevTime>=0&&(n=!0),this._rawPrevTime=a=!e||t||this._rawPrevTime===t?t:u)):this._initted||(n=!0);else if(this._totalTime=this._time=t,this._easeType){var c=t/l,f=this._easeType,p=this._easePower;(1===f||3===f&&c>=.5)&&(c=1-c),3===f&&(c*=2),1===p?c*=c:2===p?c*=c*c:3===p?c*=c*c*c:4===p&&(c*=c*c*c*c),this.ratio=1===f?1-c:2===f?c:.5>t/l?c/2:1-c/2}else this.ratio=this._ease.getRatio(t/l);if(this._time!==s||n){if(!this._initted){if(this._init(),!this._initted||this._gc)return;this._time&&!i?this.ratio=this._ease.getRatio(this._time/l):i&&this._ease._calcEnd&&(this.ratio=this._ease.getRatio(0===this._time?0:1))}for(this._active||!this._paused&&this._time!==s&&t>=0&&(this._active=!0),0===s&&(this._startAt&&(t>=0?this._startAt.render(t,e,n):r||(r="_dummyGS")),this.vars.onStart&&(0!==this._time||0===l)&&(e||this.vars.onStart.apply(this.vars.onStartScope||this,this.vars.onStartParams||v))),o=this._firstPT;o;)o.f?o.t[o.p](o.c*this.ratio+o.s):o.t[o.p]=o.c*this.ratio+o.s,o=o._next;this._onUpdate&&(0>t&&this._startAt&&this._startTime&&this._startAt.render(t,e,n),e||(this._time!==s||i)&&this._onUpdate.apply(this.vars.onUpdateScope||this,this.vars.onUpdateParams||v)),r&&(this._gc||(0>t&&this._startAt&&!this._onUpdate&&this._startTime&&this._startAt.render(t,e,n),i&&(this._timeline.autoRemoveChildren&&this._enabled(!1,!1),this._active=!1),!e&&this.vars[r]&&this.vars[r].apply(this.vars[r+"Scope"]||this,this.vars[r+"Params"]||v),0===l&&this._rawPrevTime===u&&a!==u&&(this._rawPrevTime=0)))}},r._kill=function(t,e){if("all"===t&&(t=null),null==t&&(null==e||e===this.target))return this._enabled(!1,!1);e="string"!=typeof e?e||this._targets||this.target:E.selector(e)||e;var n,i,r,o,a,s,l,u;if((p(e)||P(e))&&"number"!=typeof e[0])for(n=e.length;--n>-1;)this._kill(t,e[n])&&(s=!0);else{if(this._targets){for(n=this._targets.length;--n>-1;)if(e===this._targets[n]){a=this._propLookup[n]||{},this._overwrittenProps=this._overwrittenProps||[],i=this._overwrittenProps[n]=t?this._overwrittenProps[n]||{}:"all";break}}else{if(e!==this.target)return!1;a=this._propLookup,i=this._overwrittenProps=t?this._overwrittenProps||{}:"all"}if(a){l=t||a,u=t!==i&&"all"!==i&&t!==a&&("object"!=typeof t||!t._tempKill);for(r in l)(o=a[r])&&(o.pg&&o.t._kill(l)&&(s=!0),o.pg&&0!==o.t._overwriteProps.length||(o._prev?o._prev._next=o._next:o===this._firstPT&&(this._firstPT=o._next),o._next&&(o._next._prev=o._prev),o._next=o._prev=null),delete a[r]),u&&(i[r]=1);!this._firstPT&&this._initted&&this._enabled(!1,!1)}}return s},r.invalidate=function(){return this._notifyPluginsOfEnabled&&E._onPluginEvent("_onDisable",this),this._firstPT=null,this._overwrittenProps=null,this._onUpdate=null,this._startAt=null,this._initted=this._active=this._notifyPluginsOfEnabled=!1,this._propLookup=this._targets?{}:[],this},r._enabled=function(t,e){if(a||o.wake(),t&&this._gc){var n,i=this._targets;if(i)for(n=i.length;--n>-1;)this._siblings[n]=X(i[n],this,!0);else this._siblings=X(this.target,this,!0)}return k.prototype._enabled.call(this,t,e),this._notifyPluginsOfEnabled&&this._firstPT?E._onPluginEvent(t?"_onEnable":"_onDisable",this):!1},E.to=function(t,e,n){return new E(t,e,n)},E.from=function(t,e,n){return n.runBackwards=!0,n.immediateRender=0!=n.immediateRender,new E(t,e,n)
},E.fromTo=function(t,e,n,i){return i.startAt=n,i.immediateRender=0!=i.immediateRender&&0!=n.immediateRender,new E(t,e,i)},E.delayedCall=function(t,e,n,i,r){return new E(e,0,{delay:t,onComplete:e,onCompleteParams:n,onCompleteScope:i,onReverseComplete:e,onReverseCompleteParams:n,onReverseCompleteScope:i,immediateRender:!1,useFrames:r,overwrite:0})},E.set=function(t,e){return new E(t,0,e)},E.getTweensOf=function(t,e){if(null==t)return[];t="string"!=typeof t?t:E.selector(t)||t;var n,i,r,o;if((p(t)||P(t))&&"number"!=typeof t[0]){for(n=t.length,i=[];--n>-1;)i=i.concat(E.getTweensOf(t[n],e));for(n=i.length;--n>-1;)for(o=i[n],r=n;--r>-1;)o===i[r]&&i.splice(n,1)}else for(i=X(t).concat(),n=i.length;--n>-1;)(i[n]._gc||e&&!i[n].isActive())&&i.splice(n,1);return i},E.killTweensOf=E.killDelayedCallsTo=function(t,e,n){"object"==typeof e&&(n=e,e=!1);for(var i=E.getTweensOf(t,e),r=i.length;--r>-1;)i[r]._kill(n,t)};var H=g("plugins.TweenPlugin",function(t,e){this._overwriteProps=(t||"").split(","),this._propName=this._overwriteProps[0],this._priority=e||0,this._super=H.prototype},!0);if(r=H.prototype,H.version="1.10.1",H.API=2,r._firstPT=null,r._addTween=function(t,e,n,i,r,o){var a,s;return null!=i&&(a="number"==typeof i||"="!==i.charAt(1)?Number(i)-n:parseInt(i.charAt(0)+"1",10)*Number(i.substr(2)))?(this._firstPT=s={_next:this._firstPT,t:t,p:e,s:n,c:a,f:"function"==typeof t[e],n:r||e,r:o},s._next&&(s._next._prev=s),s):void 0},r.setRatio=function(t){for(var e,n=this._firstPT,i=1e-6;n;)e=n.c*t+n.s,n.r?e=Math.round(e):i>e&&e>-i&&(e=0),n.f?n.t[n.p](e):n.t[n.p]=e,n=n._next},r._kill=function(t){var e,n=this._overwriteProps,i=this._firstPT;if(null!=t[this._propName])this._overwriteProps=[];else for(e=n.length;--e>-1;)null!=t[n[e]]&&n.splice(e,1);for(;i;)null!=t[i.n]&&(i._next&&(i._next._prev=i._prev),i._prev?(i._prev._next=i._next,i._prev=null):this._firstPT===i&&(this._firstPT=i._next)),i=i._next;return!1},r._roundProps=function(t,e){for(var n=this._firstPT;n;)(t[this._propName]||null!=n.n&&t[n.n.split(this._propName+"_").join("")])&&(n.r=e),n=n._next},E._onPluginEvent=function(t,e){var n,i,r,o,a,s=e._firstPT;if("_onInitAllProps"===t){for(;s;){for(a=s._next,i=r;i&&i.pr>s.pr;)i=i._next;(s._prev=i?i._prev:o)?s._prev._next=s:r=s,(s._next=i)?i._prev=s:o=s,s=a}s=e._firstPT=r}for(;s;)s.pg&&"function"==typeof s.t[t]&&s.t[t]()&&(n=!0),s=s._next;return n},H.activate=function(t){for(var e=t.length;--e>-1;)t[e].API===H.API&&(R[(new t[e])._propName]=t[e]);return!0},m.plugin=function(t){if(!(t&&t.propName&&t.init&&t.API))throw"illegal plugin definition.";var e,n=t.propName,i=t.priority||0,r=t.overwriteProps,o={init:"_onInitTween",set:"setRatio",kill:"_kill",round:"_roundProps",initAll:"_onInitAllProps"},a=g("plugins."+n.charAt(0).toUpperCase()+n.substr(1)+"Plugin",function(){H.call(this,n,i),this._overwriteProps=r||[]},t.global===!0),s=a.prototype=new H(n);s.constructor=a,a.API=t.API;for(e in o)"function"==typeof t[e]&&(s[o[e]]=t[e]);return a.version=t.version,H.activate([a]),a},n=t._gsQueue){for(i=0;n.length>i;i++)n[i]();for(r in h)h[r].func||t.console.log("GSAP encountered missing dependency: com.greensock."+r)}a=!1}}(window),function(t,e){"use strict";function n(t,n,i){return{restrict:"ECA",terminal:!0,priority:400,transclude:"element",link:function(r,o,a,s,l){function u(){h&&(h.remove(),h=null),f&&(f.$destroy(),f=null),p&&(i.leave(p,function(){h=null}),h=p,p=null)}function c(){var a=t.current&&t.current.locals;if(e.isDefined(a&&a.$template)){var a=r.$new(),s=t.current;p=l(a,function(t){i.enter(t,null,p||o,function(){!e.isDefined(d)||d&&!r.$eval(d)||n()}),u()}),f=s.scope=a,f.$emit("$viewContentLoaded"),f.$eval(m)}else u()}var f,p,h,d=a.autoscroll,m=a.onload||"";r.$on("$routeChangeSuccess",c),c()}}}function i(t,e,n){return{restrict:"ECA",priority:-400,link:function(i,r){var o=n.current,a=o.locals;r.html(a.$template);var s=t(r.contents());o.controller&&(a.$scope=i,a=e(o.controller,a),o.controllerAs&&(i[o.controllerAs]=a),r.data("$ngControllerController",a),r.children().data("$ngControllerController",a)),s(i)}}}t=e.module("ngRoute",["ng"]).provider("$route",function(){function t(t,n){return e.extend(new(e.extend(function(){},{prototype:t})),n)}function n(t,e){var n=e.caseInsensitiveMatch,i={originalPath:t,regexp:t},r=i.keys=[];return t=t.replace(/([().])/g,"\\$1").replace(/(\/)?:(\w+)([\?\*])?/g,function(t,e,n,i){return t="?"===i?i:null,i="*"===i?i:null,r.push({name:n,optional:!!t}),e=e||"",""+(t?"":e)+"(?:"+(t?e:"")+(i&&"(.+?)"||"([^/]+)")+(t||"")+")"+(t||"")}).replace(/([\/$\*])/g,"\\$1"),i.regexp=RegExp("^"+t+"$",n?"i":""),i}var i={};this.when=function(t,r){if(i[t]=e.extend({reloadOnSearch:!0},r,t&&n(t,r)),t){var o="/"==t[t.length-1]?t.substr(0,t.length-1):t+"/";i[o]=e.extend({redirectTo:t},n(o,r))}return this},this.otherwise=function(t){return this.when(null,t),this},this.$get=["$rootScope","$location","$routeParams","$q","$injector","$http","$templateCache","$sce",function(n,r,o,a,s,l,u,c){function f(){var t=p(),i=m.current;t&&i&&t.$$route===i.$$route&&e.equals(t.pathParams,i.pathParams)&&!t.reloadOnSearch&&!d?(i.params=t.params,e.copy(i.params,o),n.$broadcast("$routeUpdate",i)):(t||i)&&(d=!1,n.$broadcast("$routeChangeStart",t,i),(m.current=t)&&t.redirectTo&&(e.isString(t.redirectTo)?r.path(h(t.redirectTo,t.params)).search(t.params).replace():r.url(t.redirectTo(t.pathParams,r.path(),r.search())).replace()),a.when(t).then(function(){if(t){var n,i,r=e.extend({},t.resolve);return e.forEach(r,function(t,n){r[n]=e.isString(t)?s.get(t):s.invoke(t)}),e.isDefined(n=t.template)?e.isFunction(n)&&(n=n(t.params)):e.isDefined(i=t.templateUrl)&&(e.isFunction(i)&&(i=i(t.params)),i=c.getTrustedResourceUrl(i),e.isDefined(i)&&(t.loadedTemplateUrl=i,n=l.get(i,{cache:u}).then(function(t){return t.data}))),e.isDefined(n)&&(r.$template=n),a.all(r)}}).then(function(r){t==m.current&&(t&&(t.locals=r,e.copy(t.params,o)),n.$broadcast("$routeChangeSuccess",t,i))},function(e){t==m.current&&n.$broadcast("$routeChangeError",t,i,e)}))}function p(){var n,o;return e.forEach(i,function(i){var a;if(a=!o){var s=r.path();a=i.keys;var l={};if(i.regexp)if(s=i.regexp.exec(s)){for(var u=1,c=s.length;c>u;++u){var f=a[u-1],p="string"==typeof s[u]?decodeURIComponent(s[u]):s[u];f&&p&&(l[f.name]=p)}a=l}else a=null;else a=null;a=n=a}a&&(o=t(i,{params:e.extend({},r.search(),n),pathParams:n}),o.$$route=i)}),o||i[null]&&t(i[null],{params:{},pathParams:{}})}function h(t,n){var i=[];return e.forEach((t||"").split(":"),function(t,e){if(0===e)i.push(t);else{var r=t.match(/(\w+)(.*)/),o=r[1];i.push(n[o]),i.push(r[2]||""),delete n[o]}}),i.join("")}var d=!1,m={routes:i,reload:function(){d=!0,n.$evalAsync(f)}};return n.$on("$locationChangeSuccess",f),m}]}),t.provider("$routeParams",function(){this.$get=function(){return{}}}),t.directive("ngView",n),t.directive("ngView",i),n.$inject=["$route","$anchorScroll","$animate"],i.$inject=["$compile","$controller","$route"]}(window,window.angular),!function(t){"use strict";t.module("fx.animations.assist",[]).factory("Assist",["$filter","$window","$timeout",function(e,n,i){return{emit:function(e,n,i){var r=t.element(e).scope();r.$emit(n+" "+i)},parseClassList:function(i){var r,o=i[0].classList,a={trigger:!1,duration:.3,ease:n.Back};return t.forEach(o,function(t){"fx-easing"===t.slice(0,9)&&(r=t.slice(10),a.ease=n[e("cap")(r)]?n[e("cap")(r)]:n.Elastic),"fx-trigger"===t&&(a.trigger=!0),"fx-speed"===t.slice(0,8)&&(a.duration=parseInt(t.slice(9))/1e3)}),a},addTimer:function(t,e,n){var r=this,o=t.stagger?3*t.duration*1e3:1e3*t.duration,a=i(function(){t.trigger&&r.emit(e,t.animation,t.motion),n()},o);e.data(t.timeoutKey,a)},removeTimer:function(t,e,n){i.cancel(n),t.removeData(e)}}}]).filter("cap",[function(){return function(t){return t.charAt(0).toUpperCase()+t.slice(1)}}])}(angular),function(t,e,n){"use strict";var i="$$fxTimer";t.module("fx.animations.create",["fx.animations.assist"]).factory("FadeAnimation",["$timeout","$window","Assist",function(t,n,r){return function(t){var n=t.enter,o=t.leave,a=t.inverse||t.leave,s=t.animation;this.enter=function(t,a){var l=r.parseClassList(t);return l.motion="enter",l.animation=s,l.timeoutKey=i,r.addTimer(l,t,a),n.ease=l.ease.easeOut,e.set(t,o),e.to(t,l.duration,n),function(e){var n=t.data(i);e&&n&&r.removeTimer(t,i,n)}},this.leave=function(t,o){var l=r.parseClassList(t);return l.motion="leave",l.animation=s,l.timeoutKey=i,r.addTimer(l,t,o),a.ease=l.ease.easeIn,e.set(t,n),e.to(t,l.duration,a),function(e){var n=t.data(i);e&&n&&r.removeTimer(t,i,n)}},this.move=this.enter,this.addClass=function(t,n,o){if("ng-hide"===n){var l=r.parseClassList(t);return l.motion="addClass",l.animation=s,l.timeoutKey=i,r.addTimer(l,t,o),e.to(t,l.duration,a),function(e){if(e){var n=t.data(i);n&&r.removeTimer(t,i,n)}}}o()},this.removeClass=function(t,a,l){if("ng-hide"===a){var u=r.parseClassList(t);return u.motion="removeClass",u.animation=s,u.timeoutKey=i,e.set(t,o),e.to(t,u.duration,n),function(e){if(e){var n=t.data(i);n&&r.removeTimer(t,i,n)}}}l()}}}]).factory("BounceAnimation",["$timeout","$window","Assist",function(t,e,r){return function(t){var e=t.first,o=t.mid,a=t.third,s=t.end,l=t.animation,u=.1;this.enter=function(t,c){var f=r.parseClassList(t);f.motion="enter",f.animation=l,f.timeoutKey=i,f.stagger=!0,s.ease=f.ease.easeOut,r.addTimer(f,t,c);var p=new n;return p.to(t,u,e),p.to(t,f.duration,o),p.to(t,f.duration,a),p.to(t,f.duration,s),function(e){if(e){var n=t.data(i);n&&r.removeTimer(t,i,n)}}},this.leave=function(t,c){var f=r.parseClassList(t);f.motion="leave",f.animation=l,f.timeoutKey=i,f.stagger=!0,e.ease=f.ease.easeIn,r.addTimer(f,t,c);var p=new n;return p.to(t,u,s),p.to(t,f.duration,a),p.to(t,f.duration,o),p.to(t,f.duration,e),function(e){if(e){var n=t.data(i);n&&r.removeTimer(t,i,n)}}},this.move=this.enter,this.addClass=function(t,c,f){if("ng-hide"===c){var p=r.parseClassList(t);p.motion="addClass",p.animation=l,p.timeoutKey=i,r.addTimer(p,t,f);var h=new n;return h.to(t,u,s),h.to(t,p.duration,a),h.to(t,p.duration,o),h.to(t,p.duration,e),function(e){if(e){var n=t.data(i);n&&r.removeTimer(t,i,n)}}}f()},this.removeClass=function(t,c,f){if("ng-hide"===c){var p=r.parseClassList(t);p.motion="removeClass",p.animation=l,p.timeoutKey=i;var h=new n;return h.to(t,u,e),h.to(t,p.duration,o),h.to(t,p.duration,a),h.to(t,p.duration,s),function(e){if(e){var n=t.data(i);n&&r.removeTimer(t,i,n)}}}f()}}}]).factory("RotateAnimation",["$timeout","$window","Assist",function(t,n,r){return function(t){var n=t.start,o=t.end,a=t.inverse,s=t.animation;this.enter=function(t,a){var l=r.parseClassList(t);return l.motion="enter",l.animation=s,l.timeoutKey=i,o.ease=l.ease.easeOut,r.addTimer(l,t,a),e.set(t,n),e.to(t,l.duration,o),function(e){if(e){var n=t.data(i);n&&r.removeTimer(t,i,n)}}},this.leave=function(t,n){var l=r.parseClassList(t);return l.motion="leave",l.animation=s,l.timeoutKey=i,a.ease=l.ease.easeIn,r.addTimer(l,t,n),e.set(t,o),e.to(t,l.duration,a),function(e){if(e){var n=t.data(i);n&&r.removeTimer(t,i,n)}}},this.move=this.enter,this.addClass=function(t,a,l){if("ng-hide"===a){var u=r.parseClassList(t);return u.motion="addClass",u.animation=s,u.timeoutKey=i,r.addTimer(u,t,l),e.set(t,o),e.to(t,u.duration,n),function(e){if(e){var n=t.data(i);n&&r.removeTimer(t,i,n)}}}l()},this.removeClass=function(t,a,l){if("ng-hide"===a){var u=r.parseClassList(t);return u.motion="addClass",u.animation=s,u.timeoutKey=i,r.addTimer(u,t,l),e.set(t,n),e.to(t,u.duration,o),function(e){if(e){var n=t.data(i);n&&r.removeTimer(t,i,n)}}}l()}}}]).factory("ZoomAnimation",["$timeout","$window","Assist",function(t,n,r){return function(t){var n=t.start,o=t.end,a=t.animation;this.enter=function(t,s){var l=r.parseClassList(t);return l.motion="enter",l.animation=a,l.timeoutKey=i,o.ease=l.ease.easeOut,r.addTimer(l,t,s),e.set(t,n),e.to(t,l.duration,o),function(e){if(e){var n=t.data(i);n&&r.removeTimer(t,i,n)}}},this.leave=function(t,s){var l=r.parseClassList(t);return l.motion="lave",l.animation=a,l.timeoutKey=i,n.ease=l.ease.easeIn,r.addTimer(l,t,s),e.set(t,o),e.to(t,l.duration,n),function(e){if(e){var n=t.data(i);n&&r.removeTimer(t,i,n)}}},this.move=this.enter,this.removeClass=function(t,s,l){if("ng-hide"===s){var u=r.parseClassList(t);return u.motion="addClass",u.animation=a,u.timeoutKey=i,r.addTimer(u,t,l),e.set(t,n),e.to(t,u.duration,o),function(e){if(e){var n=t.data(i);n&&r.removeTimer(t,i,n)}}}l()},this.addClass=function(t,s,l){if("ng-hide"===s){var u=r.parseClassList(t);return u.motion="addClass",u.animation=a,u.timeoutKey=i,r.addTimer(u,t,l),e.set(t,o),e.to(t,u.duration,n),function(e){if(e){var n=t.data(i);n&&r.removeTimer(t,i,n)}}}l()}}}])}(angular,TweenMax,TimelineMax),function(t){"use strict";t.module("fx.animations.bounces",["fx.animations.create"]).animation(".fx-bounce-normal",["BounceAnimation",function(t){var e={first:{opacity:0,transform:"scale(.3)"},mid:{opacity:1,transform:"scale(1.05)"},third:{transform:"scale(.9)"},end:{opacity:1,transform:"scale(1)"},animation:"bounce-normal"};return new t(e)}]).animation(".fx-bounce-down",["BounceAnimation",function(t){var e={first:{opacity:0,transform:"translateY(-2000px)"},mid:{opacity:1,transform:"translateY(30px)"},third:{transform:"translateY(-10px)"},end:{transform:"translateY(0)"},animation:"bounce-down"};return new t(e)}]).animation(".fx-bounce-left",["BounceAnimation",function(t){var e={first:{opacity:0,transform:"translateX(-2000px)"},mid:{opacity:1,transform:"translateX(30px)"},third:{transform:"translateX(-10px)"},end:{transform:"translateX(0)"},animation:"bounce-left"};return new t(e)}]).animation(".fx-bounce-up",["BounceAnimation",function(t){var e={first:{opacity:0,transform:"translateY(2000px)"},mid:{opacity:1,transform:"translateY(-30px)"},third:{transform:"translateY(10px)"},end:{transform:"translateY(0)"},animation:"bounce-up"};return new t(e)}]).animation(".fx-bounce-right",["BounceAnimation",function(t){var e={first:{opacity:0,transform:"translateX(2000px)"},mid:{opacity:1,transform:"translateX(-30px)"},third:{transform:"translateX(10px)"},end:{transform:"translateX(0)"},animation:"bounce-right"};return new t(e)}])}(angular),function(t){"use strict";t.module("fx.animations.fades",["fx.animations.create"]).animation(".fx-fade-normal",["FadeAnimation",function(t){var e={enter:{opacity:1},leave:{opacity:0},animation:"fade-normal"};return new t(e)}]).animation(".fx-fade-down",["FadeAnimation",function(t){var e={enter:{opacity:1,transform:"translateY(0)"},leave:{opacity:0,transform:"translateY(-20px)"},inverse:{opacity:0,transform:"translateY(20px)"},animation:"fade-down"};return new t(e)}]).animation(".fx-fade-down-big",["FadeAnimation",function(t){var e={enter:{opacity:1,transform:"translateY(0)"},leave:{opacity:0,transform:"translateY(-2000px)"},inverse:{opacity:0,transform:"translateY(2000px)"},animation:"fade-down-big"};return new t(e)}]).animation(".fx-fade-left",["FadeAnimation",function(t){var e={enter:{opacity:1,transform:"translateX(0)"},leave:{opacity:0,transform:"translateX(-20px)"},inverse:{opacity:0,transform:"translateX(20px)"},animation:"fade-left"};return new t(e)}]).animation(".fx-fade-left-big",["FadeAnimation",function(t){var e={enter:{opacity:1,transform:"translateX(0)"},leave:{opacity:0,transform:"translateX(-2000px)"},inverse:{opacity:0,transform:"translateX(2000px)"},animation:"fade-left-big"};return new t(e)}]).animation(".fx-fade-right",["FadeAnimation",function(t){var e={enter:{opacity:1,transform:"translateX(0)"},leave:{opacity:0,transform:"translateX(20px)"},inverse:{opacity:0,transform:"translateX(-20px)"},animation:"fade-right"};return new t(e)}]).animation(".fx-fade-right-big",["FadeAnimation",function(t){var e={enter:{opacity:1,transform:"translateX(0)"},leave:{opacity:0,transform:"translateX(2000px)"},inverse:{opacity:0,transform:"translateX(-2000px)"},animation:"fade-right-big"};return new t(e)}]).animation(".fx-fade-up",["FadeAnimation",function(t){var e={enter:{opacity:1,transform:"translateY(0)"},leave:{opacity:0,transform:"translateY(20px)"},inverse:{opacity:0,transform:"translateY(-20px)"},animation:"fade-up"};return new t(e)}]).animation(".fx-fade-up-big",["FadeAnimation",function(t){var e={enter:{opacity:1,transform:"translateY(0)"},leave:{opacity:0,transform:"translateY(2000px)"},inverse:{opacity:0,transform:"translateY(-2000px)"},animation:"fade-up-big"};return new t(e)}])}(angular),function(t){"use strict";t.module("fx.animations.rotations",["fx.animations.create"]).animation(".fx-rotate-counterclock",["RotateAnimation",function(t){var e={start:{opacity:0,transformOrigin:"center center",transform:"rotate(-200deg)"},end:{opacity:1,transformOrigin:"center center",transform:"rotate(0)"},inverse:{opacity:0,transformOrigin:"center center",transform:"rotate(200deg)"},animation:"rotate-counterclock"};return new t(e)}]).animation(".fx-rotate-clock",["RotateAnimation",function(t){var e={start:{opacity:0,transformOrigin:"center center",transform:"rotate(200deg)"},end:{opacity:1,transformOrigin:"center center",transform:"rotate(0)"},inverse:{opacity:0,transformOrigin:"center center",transform:"rotate(-200deg)"},animation:"rotate-clock"};return new t(e)}]).animation(".fx-rotate-clock-left",["RotateAnimation",function(t){var e={start:{opacity:0,transformOrigin:"left bottom",transform:"rotate(-90deg)"},end:{opacity:1,transformOrigin:"left bottom",transform:"rotate(0)"},inverse:{opacity:0,transformOrigin:"left bottom",transform:"rotate(90deg)"},animation:"rotate-clock-left"};return new t(e)}]).animation(".fx-rotate-counterclock-right",["RotateAnimation",function(t){var e={start:{opacity:0,transformOrigin:"right bottom",transform:"rotate(90deg)"},end:{opacity:1,transformOrigin:"right bottom",transform:"rotate(0)"},inverse:{opacity:0,transformOrigin:"right bottom",transform:"rotate(-90deg)"},animation:"rotate-counterclock-right"};return new t(e)}]).animation(".fx-rotate-counterclock-up",["RotateAnimation",function(t){var e={start:{opacity:0,transformOrigin:"left bottom",transform:"rotate(90deg)"},end:{opacity:1,transformOrigin:"left bottom",transform:"rotate(0)"},inverse:{opacity:0,transformOrigin:"left bottom",transform:"rotate(-90deg)"},animation:"rotate-counterclock-up"};return new t(e)}]).animation(".fx-rotate-clock-up",["RotateAnimation",function(t){var e={start:{opacity:0,transformOrigin:"right bottom",transform:"rotate(-90deg)"},end:{opacity:1,transformOrigin:"right bottom",transform:"rotate(0)"},inverse:{opacity:0,transformOrigin:"right bottom",transform:"rotate(90deg)"},animation:"rotate-clock-up"};return new t(e)}])}(angular),function(t){"use strict";t.module("fx.animations.zooms",["fx.animations.create"]).animation(".fx-zoom-normal",["ZoomAnimation",function(t){var e={start:{opacity:0,transform:"scale(.3)"},end:{opacity:1,transform:"scale(1)"},animation:"zoom-normal"};return new t(e)}]).animation(".fx-zoom-down",["ZoomAnimation",function(t){var e={start:{opacity:0,transform:"scale(.1) translateY(-2000px)"},end:{opacity:1,transform:"scale(1) translateY(0)"},animation:"zoom-down"};return new t(e)}]).animation(".fx-zoom-up",["ZoomAnimation",function(t){var e={start:{opacity:0,transform:"scale(.1) translateY(2000px)"},end:{opacity:1,transform:"scale(1) translateY(0)"},animation:"zoom-up"};return new t(e)}])}(angular),function(t){"use strict";t.module("fx.animations",["fx.animations.fades","fx.animations.bounces","fx.animations.rotations","fx.animations.zooms"])}(angular),!function(){"use strict";angular.module("ui.tree",[]).constant("treeConfig",{treeClass:"angular-ui-tree",emptyTreeClass:"angular-ui-tree-empty",hiddenClass:"angular-ui-tree-hidden",nodesClass:"angular-ui-tree-nodes",nodeClass:"angular-ui-tree-node",handleClass:"angular-ui-tree-handle",placeHolderClass:"angular-ui-tree-placeholder",dragClass:"angular-ui-tree-drag",dragThreshold:3,levelThreshold:30})}(),function(){"use strict";angular.module("ui.tree").factory("$uiTreeHelper",["$document","$window",function(t,e){return{nodrag:function(t){return"undefined"!=typeof t.attr("data-nodrag")},eventObj:function(t){var e=t;return void 0!==t.targetTouches?e=t.targetTouches.item(0):void 0!==t.originalEvent&&void 0!==t.originalEvent.targetTouches&&(e=t.originalEvent.targetTouches.item(0)),e},dragInfo:function(t){return{source:t,sourceInfo:{nodeScope:t,index:t.index(),nodesScope:t.$parentNodesScope},index:t.index(),siblings:t.$parentNodesScope.$nodes.slice(0),parent:t.$parentNodesScope,moveTo:function(t,e,n){this.parent=t,this.siblings=e.slice(0);var i=this.siblings.indexOf(this.source);i>-1&&(this.siblings.splice(i,1),this.source.index()<n&&n--),this.siblings.splice(n,0,this.source),this.index=n},parentNode:function(){return this.parent.$nodeScope},prev:function(){return this.index>0?this.siblings[this.index-1]:null},next:function(){return this.index<this.siblings.length-1?this.siblings[this.index+1]:null},isDirty:function(){return this.source.$parentNodesScope!=this.parent||this.source.index()!=this.index},eventArgs:function(t,e){return{source:this.sourceInfo,dest:{index:this.index,nodesScope:this.parent},elements:t,pos:e}},apply:function(){var t=this.source.$modelValue;this.source.remove(),this.parent.insertNode(this.index,t)}}},height:function(t){return t.prop("scrollHeight")},width:function(t){return t.prop("scrollWidth")},offset:function(n){var i=n[0].getBoundingClientRect();return{width:n.prop("offsetWidth"),height:n.prop("offsetHeight"),top:i.top+(e.pageYOffset||t[0].body.scrollTop||t[0].documentElement.scrollTop),left:i.left+(e.pageXOffset||t[0].body.scrollLeft||t[0].documentElement.scrollLeft)}},positionStarted:function(t,e){var n={};return n.offsetX=t.pageX-this.offset(e).left,n.offsetY=t.pageY-this.offset(e).top,n.startX=n.lastX=t.pageX,n.startY=n.lastY=t.pageY,n.nowX=n.nowY=n.distX=n.distY=n.dirAx=0,n.dirX=n.dirY=n.lastDirX=n.lastDirY=n.distAxX=n.distAxY=0,n},positionMoved:function(t,e,n){e.lastX=e.nowX,e.lastY=e.nowY,e.nowX=t.pageX,e.nowY=t.pageY,e.distX=e.nowX-e.lastX,e.distY=e.nowY-e.lastY,e.lastDirX=e.dirX,e.lastDirY=e.dirY,e.dirX=0===e.distX?0:e.distX>0?1:-1,e.dirY=0===e.distY?0:e.distY>0?1:-1;var i=Math.abs(e.distX)>Math.abs(e.distY)?1:0;return n?(e.dirAx=i,void(e.moving=!0)):(e.dirAx!==i?(e.distAxX=0,e.distAxY=0):(e.distAxX+=Math.abs(e.distX),0!==e.dirX&&e.dirX!==e.lastDirX&&(e.distAxX=0),e.distAxY+=Math.abs(e.distY),0!==e.dirY&&e.dirY!==e.lastDirY&&(e.distAxY=0)),void(e.dirAx=i))}}}])}(),function(){"use strict";angular.module("ui.tree").controller("TreeController",["$scope","$element","$attrs","treeConfig",function(t,e){this.scope=t,t.$element=e,t.$nodesScope=null,t.$type="uiTree",t.$emptyElm=null,t.$callbacks=null,t.dragEnabled=!0,t.maxDepth=0,t.isEmpty=function(){return t.$nodesScope&&t.$nodesScope.$modelValue&&0===t.$nodesScope.$modelValue.length},t.place=function(e){t.$nodesScope.$element.append(e),t.$emptyElm.remove()},t.resetEmptyElement=function(){0===t.$nodesScope.$modelValue.length?e.append(t.$emptyElm):t.$emptyElm.remove()};var n=function(t,e){for(var i=0;i<t.$nodes.length;i++){e?t.$nodes[i].collapse():t.$nodes[i].expand();var r=t.$nodes[i].$childNodesScope;r&&n(r,e)}};t.collapseAll=function(){n(t.$nodesScope,!0)},t.expandAll=function(){n(t.$nodesScope,!1)}}])}(),function(){"use strict";angular.module("ui.tree").controller("TreeNodesController",["$scope","$element","treeConfig",function(t,e){this.scope=t,t.$element=e,t.$modelValue=null,t.$nodes=[],t.$nodeScope=null,t.$treeScope=null,t.$type="uiTreeNodes",t.nodrop=!1,t.maxDepth=0,t.initSubNode=function(e){t.$nodes.splice(e.index(),0,e)},t.accept=function(e,n){return t.$treeScope.$callbacks.accept(e,t,n)},t.isParent=function(e){return e.$parentNodesScope==t},t.hasChild=function(){return t.$nodes.length>0},t.safeApply=function(t){var e=this.$root.$$phase;"$apply"==e||"$digest"==e?t&&"function"==typeof t&&t():this.$apply(t)},t.removeNode=function(e){var n=t.$nodes.indexOf(e);return n>-1?(t.safeApply(function(){t.$modelValue.splice(n,1)[0],t.$nodes.splice(n,1)[0]}),e):null},t.insertNode=function(e,n){t.safeApply(function(){t.$modelValue.splice(e,0,n)})},t.depth=function(){return t.$nodeScope?t.$nodeScope.depth():0},t.outOfDepth=function(e){var n=t.maxDepth||t.$treeScope.maxDepth;return n>0?t.depth()+e.maxSubDepth()+1>n:!1}}])}(),function(){"use strict";angular.module("ui.tree").controller("TreeNodeController",["$scope","$element","$attrs","treeConfig",function(t,e){this.scope=t,t.$element=e,t.$modelValue=null,t.$parentNodeScope=null,t.$childNodesScope=null,t.$parentNodesScope=null,t.$treeScope=null,t.$handleScope=null,t.$type="uiTreeNode",t.$$apply=!1,t.collapsed=!1,t.init=function(n){var i=n[0];t.$treeScope=n[1]?n[1].scope:null,t.$parentNodeScope=i.scope.$nodeScope,t.$modelValue=i.scope.$modelValue[t.$index],t.$parentNodesScope=i.scope,i.scope.initSubNode(t),e.on("$destroy",function(){})},t.index=function(){return t.$parentNodesScope.$modelValue.indexOf(t.$modelValue)},t.dragEnabled=function(){return!(t.$treeScope&&!t.$treeScope.dragEnabled)},t.isSibling=function(e){return t.$parentNodesScope==e.$parentNodesScope},t.isChild=function(e){var n=t.childNodes();return n&&n.indexOf(e)>-1},t.prev=function(){var e=t.index();return e>0?t.siblings()[e-1]:null},t.siblings=function(){return t.$parentNodesScope.$nodes},t.childNodesCount=function(){return t.childNodes()?t.childNodes().length:0},t.hasChild=function(){return t.childNodesCount()>0},t.childNodes=function(){return t.$childNodesScope?t.$childNodesScope.$nodes:null},t.accept=function(e,n){return t.$childNodesScope&&t.$childNodesScope.accept(e,n)},t.remove=function(){return t.$parentNodesScope.removeNode(t)},t.toggle=function(){t.collapsed=!t.collapsed},t.collapse=function(){t.collapsed=!0},t.expand=function(){t.collapsed=!1},t.depth=function(){var e=t.$parentNodeScope;return e?e.depth()+1:1};var n=0,i=function(t){for(var e=0,r=0;r<t.$nodes.length;r++){var o=t.$nodes[r].$childNodesScope;o&&(e=1,i(o))}n+=e};t.maxSubDepth=function(){return n=0,t.$childNodesScope&&i(t.$childNodesScope),n}}])}(),function(){"use strict";angular.module("ui.tree").controller("TreeHandleController",["$scope","$element","$attrs","treeConfig",function(t,e){this.scope=t,t.$element=e,t.$nodeScope=null,t.$type="uiTreeHandle"}])}(),function(){"use strict";angular.module("ui.tree").directive("uiTree",["treeConfig","$window",function(t,e){return{restrict:"A",scope:!0,controller:"TreeController",link:function(n,i,r){var o={accept:null},a={};angular.extend(a,t),a.treeClass&&i.addClass(a.treeClass),n.$emptyElm=angular.element(e.document.createElement("div")),a.emptyTreeClass&&n.$emptyElm.addClass(a.emptyTreeClass),n.$watch("$nodesScope.$modelValue.length",function(){n.$nodesScope.$modelValue&&n.resetEmptyElement()},!0),n.$watch(function(){return n.$eval(r.dragEnabled)},function(t){"boolean"==typeof t&&(n.dragEnabled=t)},!0),n.$watch(function(){return n.$eval(r.maxDepth)},function(t){"number"==typeof t&&(n.maxDepth=t)},!0),o.accept=function(t,e){return e.nodrop||e.outOfDepth(t)?!1:!0},o.dropped=function(){},o.dragStart=function(){},o.dragMove=function(){},o.dragStop=function(){},n.$watch(r.uiTree,function(t){angular.forEach(t,function(t,e){o[e]&&"function"==typeof t&&(o[e]=t)}),n.$callbacks=o},!0)}}}])}(),function(){"use strict";angular.module("ui.tree").directive("uiTreeNodes",["treeConfig","$window",function(t){return{require:["ngModel","?^uiTreeNode","^uiTree"],restrict:"A",scope:!0,controller:"TreeNodesController",link:function(e,n,i,r){var o={};angular.extend(o,t),o.nodesClass&&n.addClass(o.nodesClass);var a=r[0],s=r[1],l=r[2];s?(s.scope.$childNodesScope=e,e.$nodeScope=s.scope):l.scope.$nodesScope=e,e.$treeScope=l.scope,a&&(a.$render=function(){e.$modelValue=a.$modelValue}),e.$watch(function(){return e.$eval(i.maxDepth)},function(t){"number"==typeof t&&(e.maxDepth=t)},!0),e.$watch(function(){return e.$eval(i.nodrop)},function(t){"boolean"==typeof t&&(e.nodrop=t)},!0)}}}])}(),function(){"use strict";angular.module("ui.tree").directive("uiTreeNode",["treeConfig","$uiTreeHelper","$window","$document",function(t,e,n,i){return{require:["^uiTreeNodes","^uiTree"],restrict:"A",controller:"TreeNodeController",link:function(r,o,a,s){var l={};angular.extend(l,t),l.nodeClass&&o.addClass(l.nodeClass),r.init(s);var u,c,f,p,h,d,m,g="ontouchstart"in window,_=null,v=function(t){if((g||2!=t.button&&3!=t.which)&&!(t.uiTreeDragging||t.originalEvent&&t.originalEvent.uiTreeDragging)){var a=angular.element(t.target),s=a.scope();if(s&&s.$type&&!("uiTreeNode"!=s.$type&&"uiTreeHandle"!=s.$type||"uiTreeNode"==s.$type&&s.$handleScope)){for(;a&&a[0]&&a[0]!=o;){if(e.nodrag(a))return;a=a.parent()}t.uiTreeDragging=!0,t.originalEvent&&(t.originalEvent.uiTreeDragging=!0),t.preventDefault();var _=e.eventObj(t);u=!0,c=e.dragInfo(r);var v=r.$element.prop("tagName");if("tr"===v.toLowerCase()){p=angular.element(n.document.createElement(v));var y=angular.element(n.document.createElement("td")).addClass(l.placeHolderClass);p.append(y)}else p=angular.element(n.document.createElement(v)).addClass(l.placeHolderClass);h=angular.element(n.document.createElement(v)),l.hiddenClass&&h.addClass(l.hiddenClass),f=e.positionStarted(_,r.$element),p.css("height",e.height(r.$element)+"px"),d=angular.element(n.document.createElement(r.$parentNodesScope.$element.prop("tagName"))).addClass(r.$parentNodesScope.$element.attr("class")).addClass(l.dragClass),d.css("width",e.width(r.$element)+"px"),d.css("z-index",9999),r.$element.after(p),r.$element.after(h),d.append(r.$element),i.find("body").append(d),d.css({left:_.pageX-f.offsetX+"px",top:_.pageY-f.offsetY+"px"}),m={placeholder:p,dragging:d},r.$apply(function(){r.$callbacks.dragStart(c.eventArgs(m,f))}),angular.element(i).bind("touchend",x),angular.element(i).bind("touchcancel",x),angular.element(i).bind("touchmove",$),angular.element(i).bind("mouseup",x),angular.element(i).bind("mousemove",$),angular.element(n.document.body).bind("mouseleave",T)}}},y=function(t){var i,o=e.eventObj(t);if(d){if(t.preventDefault(),d.css({left:o.pageX-f.offsetX+"px",top:o.pageY-f.offsetY+"px"}),e.positionMoved(t,f,u),u)return void(u=!1);if(f.dirAx&&f.distAxX>=l.levelThreshold&&(f.distAxX=0,f.distX>0&&(i=c.prev(),i&&!i.collapsed&&i.accept(r,i.childNodesCount())&&(i.$childNodesScope.$element.append(p),c.moveTo(i.$childNodesScope,i.childNodes(),i.childNodesCount()))),f.distX<0)){var a=c.next();if(!a){var s=c.parentNode();s&&s.$parentNodesScope.accept(r,s.index()+1)&&(s.$element.after(p),c.moveTo(s.$parentNodesScope,s.siblings(),s.index()+1))}}var h=(e.offset(d).left-e.offset(p).left>=l.threshold,o.pageX-n.document.body.scrollLeft),g=o.pageY-(window.pageYOffset||n.document.documentElement.scrollTop);angular.isFunction(d.hide)&&d.hide(),n.document.elementFromPoint(h,g);var v=angular.element(n.document.elementFromPoint(h,g));if(angular.isFunction(d.show)&&d.show(),!f.dirAx){var y,b;b=v.scope();var w=!1;if("uiTree"==b.$type&&b.dragEnabled&&(w=b.isEmpty()),"uiTreeHandle"==b.$type&&(b=b.$nodeScope),"uiTreeNode"!=b.$type&&!w)return;if(_&&p.parent()[0]!=_.$element[0]&&(_.resetEmptyElement(),_=null),w)_=b,b.$nodesScope.accept(r,0)&&(b.place(p),c.moveTo(b.$nodesScope,b.$nodesScope.$nodes,0));else if(b.dragEnabled()){v=b.$element;var $=e.offset(v);y=o.pageY<$.top+e.height(v)/2,b.$parentNodesScope.accept(r,b.index())?y?(v[0].parentNode.insertBefore(p[0],v[0]),c.moveTo(b.$parentNodesScope,b.siblings(),b.index())):(v.after(p),c.moveTo(b.$parentNodesScope,b.siblings(),b.index()+1)):!y&&b.accept(r,b.childNodesCount())&&(b.$childNodesScope.$element.append(p),c.moveTo(b.$childNodesScope,b.childNodes(),b.childNodesCount()))}}r.$apply(function(){r.$callbacks.dragMove(c.eventArgs(m,f))})}},b=function(t){t.preventDefault(),d&&(h.replaceWith(r.$element),p.remove(),d.remove(),d=null,r.$$apply?(c.apply(),r.$treeScope.$apply(function(){r.$callbacks.dropped(c.eventArgs(m,f))})):S(),r.$treeScope.$apply(function(){r.$callbacks.dragStop(c.eventArgs(m,f))}),r.$$apply=!1,c=null),angular.element(i).unbind("touchend",x),angular.element(i).unbind("touchcancel",x),angular.element(i).unbind("touchmove",$),angular.element(i).unbind("mouseup",x),angular.element(i).unbind("mousemove",$),angular.element(n.document.body).unbind("mouseleave",T)
},w=function(t){r.dragEnabled()&&v(t)},$=function(t){y(t)},x=function(t){r.$$apply=!0,b(t)},T=function(t){b(t)},S=function(){o.bind("touchstart",w),o.bind("mousedown",w)};S(),angular.element(n.document.body).bind("keydown",function(t){27==t.keyCode&&(r.$$apply=!1,b(t))})}}}])}(),function(){"use strict";angular.module("ui.tree").directive("uiTreeHandle",["treeConfig","$window",function(t){return{require:"^uiTreeNode",restrict:"A",scope:!0,controller:"TreeHandleController",link:function(e,n,i,r){var o={};angular.extend(o,t),o.handleClass&&n.addClass(o.handleClass),e!=r.scope&&(e.$nodeScope=r.scope,r.scope.$handleScope=e)}}}])}(),function(t,e){"use strict";angular.module("mgcrea.ngStrap",["mgcrea.ngStrap.modal","mgcrea.ngStrap.aside","mgcrea.ngStrap.alert","mgcrea.ngStrap.button","mgcrea.ngStrap.select","mgcrea.ngStrap.datepicker","mgcrea.ngStrap.timepicker","mgcrea.ngStrap.navbar","mgcrea.ngStrap.tooltip","mgcrea.ngStrap.popover","mgcrea.ngStrap.dropdown","mgcrea.ngStrap.typeahead","mgcrea.ngStrap.scrollspy","mgcrea.ngStrap.affix","mgcrea.ngStrap.tab"]),angular.module("mgcrea.ngStrap.affix",["mgcrea.ngStrap.helpers.dimensions","mgcrea.ngStrap.helpers.debounce"]).provider("$affix",function(){var t=this.defaults={offsetTop:"auto"};this.$get=["$window","debounce","dimensions",function(e,n,i){function r(r,s){function l(t,e,n){var i=u(),r=c();return _>=i?"top":null!==t&&i+t<=e.top?"middle":null!==v&&e.top+n+m>=r-v?"bottom":"middle"}function u(){return h[0]===e?e.pageYOffset:h[0]===e}function c(){return h[0]===e?e.document.body.scrollHeight:h[0].scrollHeight}var f={},p=angular.extend({},t,s),h=p.target,d="affix affix-top affix-bottom",m=0,g=0,_=0,v=0,y=null,b=null,w=r.parent();if(p.offsetParent)if(p.offsetParent.match(/^\d+$/))for(var $=0;$<1*p.offsetParent-1;$++)w=w.parent();else w=angular.element(p.offsetParent);return f.init=function(){f.$parseOffsets(),g=i.offset(r[0]).top+m,h.on("scroll",f.checkPosition),h.on("click",f.checkPositionWithEventLoop),a.on("resize",f.$debouncedOnResize),f.checkPosition(),f.checkPositionWithEventLoop()},f.destroy=function(){h.off("scroll",f.checkPosition),h.off("click",f.checkPositionWithEventLoop),a.off("resize",f.$debouncedOnResize)},f.checkPositionWithEventLoop=function(){setTimeout(f.checkPosition,1)},f.checkPosition=function(){var t=u(),e=i.offset(r[0]),n=i.height(r[0]),a=l(b,e,n);y!==a&&(y=a,r.removeClass(d).addClass("affix"+("middle"!==a?"-"+a:"")),"top"===a?(b=null,r.css("position",p.offsetParent?"":"relative"),r.css("top","")):"bottom"===a?(b=p.offsetUnpin?-(1*p.offsetUnpin):e.top-t,r.css("position",p.offsetParent?"":"relative"),r.css("top",p.offsetParent?"":o[0].offsetHeight-v-n-g+"px")):(b=null,r.css("position","fixed"),r.css("top",m+"px")))},f.$onResize=function(){f.$parseOffsets(),f.checkPosition()},f.$debouncedOnResize=n(f.$onResize,50),f.$parseOffsets=function(){r.css("position",p.offsetParent?"":"relative"),p.offsetTop&&("auto"===p.offsetTop&&(p.offsetTop="+0"),p.offsetTop.match(/^[-+]\d+$/)?(m=1*-p.offsetTop,_=p.offsetParent?i.offset(w[0]).top+1*p.offsetTop:i.offset(r[0]).top-i.css(r[0],"marginTop",!0)+1*p.offsetTop):_=1*p.offsetTop),p.offsetBottom&&(v=p.offsetParent&&p.offsetBottom.match(/^[-+]\d+$/)?c()-(i.offset(w[0]).top+i.height(w[0]))+1*p.offsetBottom+1:1*p.offsetBottom)},f.init(),f}var o=angular.element(e.document.body),a=angular.element(e);return r}]}).directive("bsAffix",["$affix","$window",function(t,e){return{restrict:"EAC",require:"^?bsAffixTarget",link:function(n,i,r,o){var a={scope:n,offsetTop:"auto",target:o?o.$element:angular.element(e)};angular.forEach(["offsetTop","offsetBottom","offsetParent","offsetUnpin"],function(t){angular.isDefined(r[t])&&(a[t]=r[t])});var s=t(i,a);n.$on("$destroy",function(){a=null,s=null})}}}]).directive("bsAffixTarget",function(){return{controller:["$element",function(t){this.$element=t}]}}),angular.module("mgcrea.ngStrap.alert",["mgcrea.ngStrap.modal"]).provider("$alert",function(){var t=this.defaults={animation:"am-fade",prefixClass:"alert",placement:null,template:"alert/alert.tpl.html",container:!1,element:null,backdrop:!1,keyboard:!0,show:!0,duration:!1,type:!1};this.$get=["$modal","$timeout",function(e,n){function i(i){var r={},o=angular.extend({},t,i);r=e(o),o.type&&(r.$scope.type=o.type);var a=r.show;return o.duration&&(r.show=function(){a(),n(function(){r.hide()},1e3*o.duration)}),r}return i}]}).directive("bsAlert",["$window","$location","$sce","$alert",function(t,e,n,i){t.requestAnimationFrame||t.setTimeout;return{restrict:"EAC",scope:!0,link:function(t,e,r){var o={scope:t,element:e,show:!1};angular.forEach(["template","placement","keyboard","html","container","animation","duration"],function(t){angular.isDefined(r[t])&&(o[t]=r[t])}),angular.forEach(["title","content","type"],function(e){r[e]&&r.$observe(e,function(i){t[e]=n.trustAsHtml(i)})}),r.bsAlert&&t.$watch(r.bsAlert,function(e){angular.isObject(e)?angular.extend(t,e):t.content=e},!0);var a=i(o);e.on(r.trigger||"click",a.toggle),t.$on("$destroy",function(){a.destroy(),o=null,a=null})}}}]),angular.module("mgcrea.ngStrap.aside",["mgcrea.ngStrap.modal"]).provider("$aside",function(){var t=this.defaults={animation:"am-fade-and-slide-right",prefixClass:"aside",placement:"right",template:"aside/aside.tpl.html",contentTemplate:!1,container:!1,element:null,backdrop:!0,keyboard:!0,html:!1,show:!0};this.$get=["$modal",function(e){function n(n){var i={},r=angular.extend({},t,n);return i=e(r)}return n}]}).directive("bsAside",["$window","$sce","$aside",function(t,e,n){t.requestAnimationFrame||t.setTimeout;return{restrict:"EAC",scope:!0,link:function(t,i,r){var o={scope:t,element:i,show:!1};angular.forEach(["template","contentTemplate","placement","backdrop","keyboard","html","container","animation"],function(t){angular.isDefined(r[t])&&(o[t]=r[t])}),angular.forEach(["title","content"],function(n){r[n]&&r.$observe(n,function(i){t[n]=e.trustAsHtml(i)})}),r.bsAside&&t.$watch(r.bsAside,function(e){angular.isObject(e)?angular.extend(t,e):t.content=e},!0);var a=n(o);i.on(r.trigger||"click",a.toggle),t.$on("$destroy",function(){a.destroy(),o=null,a=null})}}}]),angular.module("mgcrea.ngStrap.button",[]).provider("$button",function(){var t=this.defaults={activeClass:"active",toggleEvent:"click"};this.$get=function(){return{defaults:t}}}).directive("bsCheckboxGroup",function(){return{restrict:"A",require:"ngModel",compile:function(t,e){t.attr("data-toggle","buttons"),t.removeAttr("ng-model");var n=t[0].querySelectorAll('input[type="checkbox"]');angular.forEach(n,function(t){var n=angular.element(t);n.attr("bs-checkbox",""),n.attr("ng-model",e.ngModel+"."+n.attr("value"))})}}}).directive("bsCheckbox",["$button","$$rAF",function(t,e){var n=t.defaults,i=/^(true|false|\d+)$/;return{restrict:"A",require:"ngModel",link:function(t,r,o,a){var s=n,l="INPUT"===r[0].nodeName,u=l?r.parent():r,c=angular.isDefined(o.trueValue)?o.trueValue:!0;i.test(o.trueValue)&&(c=t.$eval(o.trueValue));var f=angular.isDefined(o.falseValue)?o.falseValue:!1;i.test(o.falseValue)&&(f=t.$eval(o.falseValue));var p="boolean"!=typeof c||"boolean"!=typeof f;p&&(a.$parsers.push(function(t){return t?c:f}),t.$watch(o.ngModel,function(){a.$render()})),a.$render=function(){var t=angular.equals(a.$modelValue,c);e(function(){l&&(r[0].checked=t),u.toggleClass(s.activeClass,t)})},r.bind(s.toggleEvent,function(){t.$apply(function(){l||a.$setViewValue(!u.hasClass("active")),p||a.$render()})})}}}]).directive("bsRadioGroup",function(){return{restrict:"A",require:"ngModel",compile:function(t,e){t.attr("data-toggle","buttons"),t.removeAttr("ng-model");var n=t[0].querySelectorAll('input[type="radio"]');angular.forEach(n,function(t){angular.element(t).attr("bs-radio",""),angular.element(t).attr("ng-model",e.ngModel)})}}}).directive("bsRadio",["$button","$$rAF",function(t,e){var n=t.defaults,i=/^(true|false|\d+)$/;return{restrict:"A",require:"ngModel",link:function(t,r,o,a){var s=n,l="INPUT"===r[0].nodeName,u=l?r.parent():r,c=i.test(o.value)?t.$eval(o.value):o.value;a.$render=function(){var t=angular.equals(a.$modelValue,c);e(function(){l&&(r[0].checked=t),u.toggleClass(s.activeClass,t)})},r.bind(s.toggleEvent,function(){t.$apply(function(){a.$setViewValue(c),a.$render()})})}}}]),angular.module("mgcrea.ngStrap.datepicker",["mgcrea.ngStrap.helpers.dateParser","mgcrea.ngStrap.tooltip"]).provider("$datepicker",function(){var t=this.defaults={animation:"am-fade",prefixClass:"datepicker",placement:"bottom-left",template:"datepicker/datepicker.tpl.html",trigger:"focus",container:!1,keyboard:!0,html:!1,delay:0,useNative:!1,dateType:"date",dateFormat:"shortDate",dayFormat:"dd",strictFormat:!1,autoclose:!1,minDate:-1/0,maxDate:+1/0,startView:0,minView:0,startWeek:0};this.$get=["$window","$document","$rootScope","$sce","$locale","dateFilter","datepickerViews","$tooltip",function(e,n,i,r,o,a,s,l){function u(e,n,i){function r(t){t.selected=a.$isSelected(t.date)}function o(){e[0].focus()}var a=l(e,angular.extend({},t,i)),u=i.scope,p=a.$options,h=a.$scope;p.startView&&(p.startView-=p.minView);var d=s(a);a.$views=d.views;var m=d.viewDate;h.$mode=p.startView;var g=a.$views[h.$mode];h.$select=function(t){a.select(t)},h.$selectPane=function(t){a.$selectPane(t)},h.$toggleMode=function(){a.setMode((h.$mode+1)%a.$views.length)},a.update=function(t){angular.isDate(t)&&!isNaN(t.getTime())&&(a.$date=t,g.update.call(g,t)),a.$build(!0)},a.select=function(t,e){angular.isDate(n.$dateValue)||(n.$dateValue=new Date(t)),n.$dateValue.setFullYear(t.getFullYear(),t.getMonth(),t.getDate()),!h.$mode||e?(n.$setViewValue(n.$dateValue),n.$render(),p.autoclose&&!e&&a.hide(!0)):(angular.extend(m,{year:t.getFullYear(),month:t.getMonth(),date:t.getDate()}),a.setMode(h.$mode-1),a.$build())},a.setMode=function(t){h.$mode=t,g=a.$views[h.$mode],a.$build()},a.$build=function(t){t===!0&&g.built||(t!==!1||g.built)&&g.build.call(g)},a.$updateSelected=function(){for(var t=0,e=h.rows.length;e>t;t++)angular.forEach(h.rows[t],r)},a.$isSelected=function(t){return g.isSelected(t)},a.$selectPane=function(t){var e=g.steps,n=new Date(Date.UTC(m.year+(e.year||0)*t,m.month+(e.month||0)*t,m.date+(e.day||0)*t));angular.extend(m,{year:n.getUTCFullYear(),month:n.getUTCMonth(),date:n.getUTCDate()}),a.$build()},a.$onMouseDown=function(t){if(t.preventDefault(),t.stopPropagation(),c){var e=angular.element(t.target);"button"!==e[0].nodeName.toLowerCase()&&(e=e.parent()),e.triggerHandler("click")}},a.$onKeyDown=function(t){if(/(38|37|39|40|13)/.test(t.keyCode)&&!t.shiftKey&&!t.altKey){if(t.preventDefault(),t.stopPropagation(),13===t.keyCode)return h.$mode?h.$apply(function(){a.setMode(h.$mode-1)}):a.hide(!0);g.onKeyDown(t),u.$digest()}};var _=a.init;a.init=function(){return f&&p.useNative?(e.prop("type","date"),void e.css("-webkit-appearance","textfield")):(c&&(e.prop("type","text"),e.attr("readonly","true"),e.on("click",o)),void _())};var v=a.destroy;a.destroy=function(){f&&p.useNative&&e.off("click",o),v()};var y=a.show;a.show=function(){y(),setTimeout(function(){a.$element.on(c?"touchstart":"mousedown",a.$onMouseDown),p.keyboard&&e.on("keydown",a.$onKeyDown)})};var b=a.hide;return a.hide=function(t){a.$element.off(c?"touchstart":"mousedown",a.$onMouseDown),p.keyboard&&e.off("keydown",a.$onKeyDown),b(t)},a}var c=(angular.element(e.document.body),"createTouch"in e.document),f=/(ip(a|o)d|iphone|android)/gi.test(e.navigator.userAgent);return t.lang||(t.lang=o.id),u.defaults=t,u}]}).directive("bsDatepicker",["$window","$parse","$q","$locale","dateFilter","$datepicker","$dateParser","$timeout",function(t,e,n,i,r,o,a){var s=(o.defaults,/(ip(a|o)d|iphone|android)/gi.test(t.navigator.userAgent)),l=function(t){return!isNaN(parseFloat(t))&&isFinite(t)};return{restrict:"EAC",require:"ngModel",link:function(t,e,n,i){var u={scope:t,controller:i};angular.forEach(["placement","container","delay","trigger","keyboard","html","animation","template","autoclose","dateType","dateFormat","dayFormat","strictFormat","startWeek","useNative","lang","startView","minView"],function(t){angular.isDefined(n[t])&&(u[t]=n[t])}),s&&u.useNative&&(u.dateFormat="yyyy-MM-dd");var c=o(e,i,u);u=c.$options,angular.forEach(["minDate","maxDate"],function(t){angular.isDefined(n[t])&&n.$observe(t,function(e){if("today"===e){var n=new Date;c.$options[t]=+new Date(n.getFullYear(),n.getMonth(),n.getDate()+("maxDate"===t?1:0),0,0,0,"minDate"===t?0:-1)}else c.$options[t]=angular.isString(e)&&e.match(/^".+"$/)?+new Date(e.substr(1,e.length-2)):l(e)?+new Date(parseInt(e,10)):+new Date(e);!isNaN(c.$options[t])&&c.$build(!1)})}),t.$watch(n.ngModel,function(){c.update(i.$dateValue)},!0);var f=a({format:u.dateFormat,lang:u.lang,strict:u.strictFormat});i.$parsers.unshift(function(t){if(!t)return void i.$setValidity("date",!0);var e=f.parse(t,i.$dateValue);if(!e||isNaN(e.getTime()))return void i.$setValidity("date",!1);var n=(isNaN(c.$options.minDate)||e.getTime()>=c.$options.minDate)&&(isNaN(c.$options.maxDate)||e.getTime()<=c.$options.maxDate);return i.$setValidity("date",n),n&&(i.$dateValue=e),"string"===u.dateType?r(t,u.dateFormat):"number"===u.dateType?i.$dateValue.getTime():"iso"===u.dateType?i.$dateValue.toISOString():new Date(i.$dateValue)}),i.$formatters.push(function(t){var e;return e=angular.isUndefined(t)||null===t?0/0:angular.isDate(t)?t:"string"===u.dateType?f.parse(t):new Date(t),i.$dateValue=e,i.$dateValue}),i.$render=function(){e.val(!i.$dateValue||isNaN(i.$dateValue.getTime())?"":r(i.$dateValue,u.dateFormat))},t.$on("$destroy",function(){c.destroy(),u=null,c=null})}}}]).provider("datepickerViews",function(){function t(t,e){for(var n=[];t.length>0;)n.push(t.splice(0,e));return n}function e(t,e){return(t%e+e)%e}this.defaults={dayFormat:"dd",daySplit:7};this.$get=["$locale","$sce","dateFilter",function(n,i,r){return function(o){var a=o.$scope,s=o.$options,l=n.DATETIME_FORMATS.SHORTDAY,u=l.slice(s.startWeek).concat(l.slice(0,s.startWeek)),c=i.trustAsHtml('<th class="dow text-center">'+u.join('</th><th class="dow text-center">')+"</th>"),f=o.$date||new Date,p={year:f.getFullYear(),month:f.getMonth(),date:f.getDate()},h=(6e4*f.getTimezoneOffset(),[{format:s.dayFormat,split:7,steps:{month:1},update:function(t,e){!this.built||e||t.getFullYear()!==p.year||t.getMonth()!==p.month?(angular.extend(p,{year:o.$date.getFullYear(),month:o.$date.getMonth(),date:o.$date.getDate()}),o.$build()):t.getDate()!==p.date&&(p.date=o.$date.getDate(),o.$updateSelected())},build:function(){var n=new Date(p.year,p.month,1),i=n.getTimezoneOffset(),l=new Date(+n-864e5*e(n.getDay()-s.startWeek,7)),u=l.getTimezoneOffset();u!==i&&(l=new Date(+l+6e4*(u-i)));for(var f,h=[],d=0;42>d;d++)f=new Date(l.getFullYear(),l.getMonth(),l.getDate()+d),h.push({date:f,label:r(f,this.format),selected:o.$date&&this.isSelected(f),muted:f.getMonth()!==p.month,disabled:this.isDisabled(f)});a.title=r(n,"MMMM yyyy"),a.showLabels=!0,a.labels=c,a.rows=t(h,this.split),this.built=!0},isSelected:function(t){return o.$date&&t.getFullYear()===o.$date.getFullYear()&&t.getMonth()===o.$date.getMonth()&&t.getDate()===o.$date.getDate()},isDisabled:function(t){return t.getTime()<s.minDate||t.getTime()>s.maxDate},onKeyDown:function(t){var e,n=o.$date.getTime();37===t.keyCode?e=new Date(n-864e5):38===t.keyCode?e=new Date(n-6048e5):39===t.keyCode?e=new Date(n+864e5):40===t.keyCode&&(e=new Date(n+6048e5)),this.isDisabled(e)||o.select(e,!0)}},{name:"month",format:"MMM",split:4,steps:{year:1},update:function(t){this.built&&t.getFullYear()===p.year?t.getMonth()!==p.month&&(angular.extend(p,{month:o.$date.getMonth(),date:o.$date.getDate()}),o.$updateSelected()):(angular.extend(p,{year:o.$date.getFullYear(),month:o.$date.getMonth(),date:o.$date.getDate()}),o.$build())},build:function(){for(var e,n=(new Date(p.year,0,1),[]),i=0;12>i;i++)e=new Date(p.year,i,1),n.push({date:e,label:r(e,this.format),selected:o.$isSelected(e),disabled:this.isDisabled(e)});a.title=r(e,"yyyy"),a.showLabels=!1,a.rows=t(n,this.split),this.built=!0},isSelected:function(t){return o.$date&&t.getFullYear()===o.$date.getFullYear()&&t.getMonth()===o.$date.getMonth()},isDisabled:function(t){var e=+new Date(t.getFullYear(),t.getMonth()+1,0);return e<s.minDate||t.getTime()>s.maxDate},onKeyDown:function(t){var e=o.$date.getMonth(),n=new Date(o.$date);37===t.keyCode?n.setMonth(e-1):38===t.keyCode?n.setMonth(e-4):39===t.keyCode?n.setMonth(e+1):40===t.keyCode&&n.setMonth(e+4),this.isDisabled(n)||o.select(n,!0)}},{name:"year",format:"yyyy",split:4,steps:{year:12},update:function(t,e){!this.built||e||parseInt(t.getFullYear()/20,10)!==parseInt(p.year/20,10)?(angular.extend(p,{year:o.$date.getFullYear(),month:o.$date.getMonth(),date:o.$date.getDate()}),o.$build()):t.getFullYear()!==p.year&&(angular.extend(p,{year:o.$date.getFullYear(),month:o.$date.getMonth(),date:o.$date.getDate()}),o.$updateSelected())},build:function(){for(var e,n=p.year-p.year%(3*this.split),i=[],s=0;12>s;s++)e=new Date(n+s,0,1),i.push({date:e,label:r(e,this.format),selected:o.$isSelected(e),disabled:this.isDisabled(e)});a.title=i[0].label+"-"+i[i.length-1].label,a.showLabels=!1,a.rows=t(i,this.split),this.built=!0},isSelected:function(t){return o.$date&&t.getFullYear()===o.$date.getFullYear()},isDisabled:function(t){var e=+new Date(t.getFullYear()+1,0,0);return e<s.minDate||t.getTime()>s.maxDate},onKeyDown:function(t){var e=o.$date.getFullYear(),n=new Date(o.$date);37===t.keyCode?n.setYear(e-1):38===t.keyCode?n.setYear(e-4):39===t.keyCode?n.setYear(e+1):40===t.keyCode&&n.setYear(e+4),this.isDisabled(n)||o.select(n,!0)}}]);return{views:s.minView?Array.prototype.slice.call(h,s.minView):h,viewDate:p}}}]}),angular.module("mgcrea.ngStrap.dropdown",["mgcrea.ngStrap.tooltip"]).provider("$dropdown",function(){var t=this.defaults={animation:"am-fade",prefixClass:"dropdown",placement:"bottom-left",template:"dropdown/dropdown.tpl.html",trigger:"click",container:!1,keyboard:!0,html:!1,delay:0};this.$get=["$window","$rootScope","$tooltip",function(e,n,i){function r(e,r){function s(t){return t.target!==e[0]?t.target!==e[0]&&l.hide():void 0}{var l={},u=angular.extend({},t,r);l.$scope=u.scope&&u.scope.$new()||n.$new()}l=i(e,u),l.$onKeyDown=function(t){if(/(38|40)/.test(t.keyCode)){t.preventDefault(),t.stopPropagation();var e=angular.element(l.$element[0].querySelectorAll("li:not(.divider) a"));if(e.length){var n;angular.forEach(e,function(t,e){a&&a.call(t,":focus")&&(n=e)}),38===t.keyCode&&n>0?n--:40===t.keyCode&&n<e.length-1?n++:angular.isUndefined(n)&&(n=0),e.eq(n)[0].focus()}}};var c=l.show;l.show=function(){c(),setTimeout(function(){u.keyboard&&l.$element.on("keydown",l.$onKeyDown),o.on("click",s)})};var f=l.hide;return l.hide=function(){u.keyboard&&l.$element.off("keydown",l.$onKeyDown),o.off("click",s),f()},l}var o=angular.element(e.document.body),a=Element.prototype.matchesSelector||Element.prototype.webkitMatchesSelector||Element.prototype.mozMatchesSelector||Element.prototype.msMatchesSelector||Element.prototype.oMatchesSelector;return r}]}).directive("bsDropdown",["$window","$location","$sce","$dropdown",function(t,e,n,i){return{restrict:"EAC",scope:!0,link:function(t,e,n){var r={scope:t};angular.forEach(["placement","container","delay","trigger","keyboard","html","animation","template"],function(t){angular.isDefined(n[t])&&(r[t]=n[t])}),n.bsDropdown&&t.$watch(n.bsDropdown,function(e){t.content=e},!0);var o=i(e,r);t.$on("$destroy",function(){o.destroy(),r=null,o=null})}}}]),angular.module("mgcrea.ngStrap.helpers.dateParser",[]).provider("$dateParser",["$localeProvider",function(){var t=Date.prototype,e=this.defaults={format:"shortDate",strict:!1};this.$get=["$locale",function(n){var i=function(i){function r(t){var e,n=Object.keys(p),i=[],r=[],o=t;for(e=0;e<n.length;e++)if(t.split(n[e]).length>1){var a=o.search(n[e]);t=t.split(n[e]).join(""),p[n[e]]&&(i[a]=p[n[e]])}return angular.forEach(i,function(t){r.push(t)}),r}function o(t){return t.replace(/\//g,"[\\/]").replace("/-/g","[-]").replace(/\./g,"[.]").replace(/\\s/g,"[\\s]")}function a(t){var e,n=Object.keys(f),i=t;for(e=0;e<n.length;e++)i=i.split(n[e]).join("${"+e+"}");for(e=0;e<n.length;e++)i=i.split("${"+e+"}").join("("+f[n[e]]+")");return t=o(t),new RegExp("^"+i+"$",["i"])}var s,l,u=angular.extend({},e,i),c={},f={sss:"[0-9]{3}",ss:"[0-5][0-9]",s:u.strict?"[1-5]?[0-9]":"[0-9]|[0-5][0-9]",mm:"[0-5][0-9]",m:u.strict?"[1-5]?[0-9]":"[0-9]|[0-5][0-9]",HH:"[01][0-9]|2[0-3]",H:u.strict?"1?[0-9]|2[0-3]":"[01]?[0-9]|2[0-3]",hh:"[0][1-9]|[1][012]",h:u.strict?"[1-9]|1[012]":"0?[1-9]|1[012]",a:"AM|PM",EEEE:n.DATETIME_FORMATS.DAY.join("|"),EEE:n.DATETIME_FORMATS.SHORTDAY.join("|"),dd:"0[1-9]|[12][0-9]|3[01]",d:u.strict?"[1-9]|[1-2][0-9]|3[01]":"0?[1-9]|[1-2][0-9]|3[01]",MMMM:n.DATETIME_FORMATS.MONTH.join("|"),MMM:n.DATETIME_FORMATS.SHORTMONTH.join("|"),MM:"0[1-9]|1[012]",M:u.strict?"[1-9]|1[012]":"0?[1-9]|1[012]",yyyy:"[1]{1}[0-9]{3}|[2]{1}[0-9]{3}",yy:"[0-9]{2}",y:u.strict?"-?(0|[1-9][0-9]{0,3})":"-?0*[0-9]{1,4}"},p={sss:t.setMilliseconds,ss:t.setSeconds,s:t.setSeconds,mm:t.setMinutes,m:t.setMinutes,HH:t.setHours,H:t.setHours,hh:t.setHours,h:t.setHours,dd:t.setDate,d:t.setDate,a:function(t){var e=this.getHours();return this.setHours(t.match(/pm/i)?e+12:e)},MMMM:function(t){return this.setMonth(n.DATETIME_FORMATS.MONTH.indexOf(t))},MMM:function(t){return this.setMonth(n.DATETIME_FORMATS.SHORTMONTH.indexOf(t))},MM:function(t){return this.setMonth(1*t-1)},M:function(t){return this.setMonth(1*t-1)},yyyy:t.setFullYear,yy:function(t){return this.setFullYear(2e3+1*t)},y:t.setFullYear};return c.init=function(){c.$format=n.DATETIME_FORMATS[u.format]||u.format,s=a(c.$format),l=r(c.$format)},c.isValid=function(t){return angular.isDate(t)?!isNaN(t.getTime()):s.test(t)},c.parse=function(t,e){if(angular.isDate(t))return t;var n=s.exec(t);if(!n)return!1;for(var i=e||new Date(0,0,1),r=0;r<n.length-1;r++)l[r]&&l[r].call(i,n[r+1]);return i},c.init(),c};return i}]}]),angular.module("mgcrea.ngStrap.helpers.debounce",[]).constant("debounce",function(t,e,n){var i,r,o,a,s;return function(){o=this,r=arguments,a=new Date;var l=function(){var u=new Date-a;e>u?i=setTimeout(l,e-u):(i=null,n||(s=t.apply(o,r)))},u=n&&!i;return i||(i=setTimeout(l,e)),u&&(s=t.apply(o,r)),s}}).constant("throttle",function(t,e,n){var i,r,o,a=null,s=0;n||(n={});var l=function(){s=n.leading===!1?0:new Date,a=null,o=t.apply(i,r)};return function(){var u=new Date;s||n.leading!==!1||(s=u);var c=e-(u-s);return i=this,r=arguments,0>=c?(clearTimeout(a),a=null,s=u,o=t.apply(i,r)):a||n.trailing===!1||(a=setTimeout(l,c)),o}}),angular.module("mgcrea.ngStrap.helpers.dimensions",[]).factory("dimensions",["$document","$window",function(){var e=(angular.element,{}),n=e.nodeName=function(t,e){return t.nodeName&&t.nodeName.toLowerCase()===e.toLowerCase()};e.css=function(e,n,i){var r;return r=e.currentStyle?e.currentStyle[n]:t.getComputedStyle?t.getComputedStyle(e)[n]:e.style[n],i===!0?parseFloat(r)||0:r},e.offset=function(e){var n=e.getBoundingClientRect(),i=e.ownerDocument;return{width:e.offsetWidth,height:e.offsetHeight,top:n.top+(t.pageYOffset||i.documentElement.scrollTop)-(i.documentElement.clientTop||0),left:n.left+(t.pageXOffset||i.documentElement.scrollLeft)-(i.documentElement.clientLeft||0)}},e.position=function(t){var r,o,a={top:0,left:0};return"fixed"===e.css(t,"position")?o=t.getBoundingClientRect():(r=i(t),o=e.offset(t),o=e.offset(t),n(r,"html")||(a=e.offset(r)),a.top+=e.css(r,"borderTopWidth",!0),a.left+=e.css(r,"borderLeftWidth",!0)),{width:t.offsetWidth,height:t.offsetHeight,top:o.top-a.top-e.css(t,"marginTop",!0),left:o.left-a.left-e.css(t,"marginLeft",!0)}};var i=function(t){var i=t.ownerDocument,r=t.offsetParent||i;if(n(r,"#document"))return i.documentElement;for(;r&&!n(r,"html")&&"static"===e.css(r,"position");)r=r.offsetParent;return r||i.documentElement};return e.height=function(t,n){var i=t.offsetHeight;return n?i+=e.css(t,"marginTop",!0)+e.css(t,"marginBottom",!0):i-=e.css(t,"paddingTop",!0)+e.css(t,"paddingBottom",!0)+e.css(t,"borderTopWidth",!0)+e.css(t,"borderBottomWidth",!0),i},e.width=function(t,n){var i=t.offsetWidth;return n?i+=e.css(t,"marginLeft",!0)+e.css(t,"marginRight",!0):i-=e.css(t,"paddingLeft",!0)+e.css(t,"paddingRight",!0)+e.css(t,"borderLeftWidth",!0)+e.css(t,"borderRightWidth",!0),i},e}]),angular.module("mgcrea.ngStrap.helpers.parseOptions",[]).provider("$parseOptions",function(){var t=this.defaults={regexp:/^\s*(.*?)(?:\s+as\s+(.*?))?(?:\s+group\s+by\s+(.*))?\s+for\s+(?:([\$\w][\$\w]*)|(?:\(\s*([\$\w][\$\w]*)\s*,\s*([\$\w][\$\w]*)\s*\)))\s+in\s+(.*?)(?:\s+track\s+by\s+(.*?))?$/};this.$get=["$parse","$q",function(e,n){function i(i,r){function o(t,e){return t.map(function(t,n){var i,r,o={};return o[c]=t,i=u(e,o),r=h(e,o)||n,{label:i,value:r}})}var a={},s=angular.extend({},t,r);a.$values=[];var l,u,c,f,p,h,d;return a.init=function(){a.$match=l=i.match(s.regexp),u=e(l[2]||l[1]),c=l[4]||l[6],f=l[5],p=e(l[3]||""),h=e(l[2]?l[1]:c),d=e(l[7])},a.valuesFn=function(t,e){return n.when(d(t,e)).then(function(e){return a.$values=e?o(e,t):{},a.$values})},a.init(),a}return i}]}),angular.version.minor<3&&angular.version.dot<14&&angular.module("ng").factory("$$rAF",["$window","$timeout",function(t,e){var n=t.requestAnimationFrame||t.webkitRequestAnimationFrame||t.mozRequestAnimationFrame,i=t.cancelAnimationFrame||t.webkitCancelAnimationFrame||t.mozCancelAnimationFrame||t.webkitCancelRequestAnimationFrame,r=!!n,o=r?function(t){var e=n(t);return function(){i(e)}}:function(t){var n=e(t,16.66,!1);return function(){e.cancel(n)}};return o.supported=r,o}]),angular.module("mgcrea.ngStrap.modal",["mgcrea.ngStrap.helpers.dimensions"]).provider("$modal",function(){var t=this.defaults={animation:"am-fade",backdropAnimation:"am-fade",prefixClass:"modal",prefixEvent:"modal",placement:"top",template:"modal/modal.tpl.html",contentTemplate:!1,container:!1,element:null,backdrop:!0,keyboard:!0,html:!1,show:!0};this.$get=["$window","$rootScope","$compile","$q","$templateCache","$http","$animate","$timeout","$sce","dimensions",function(n,i,r,o,a,s,l,u,c){function f(e){function n(t){t.target===t.currentTarget&&("static"===a.backdrop?o.focus():o.hide())}var o={},a=o.$options=angular.extend({},t,e);o.$promise=h(a.template);var s=o.$scope=a.scope&&a.scope.$new()||i.$new();a.element||a.container||(a.container="body"),d(["title","content"],function(t){a[t]&&(s[t]=c.trustAsHtml(a[t]))}),s.$hide=function(){s.$$postDigest(function(){o.hide()})},s.$show=function(){s.$$postDigest(function(){o.show()})},s.$toggle=function(){s.$$postDigest(function(){o.toggle()})},a.contentTemplate&&(o.$promise=o.$promise.then(function(t){var n=angular.element(t);return h(a.contentTemplate).then(function(t){var i=p('[ng-bind="content"]',n[0]).removeAttr("ng-bind").html(t);return e.template||i.next().remove(),n[0].outerHTML})}));var u,f,y=angular.element('<div class="'+a.prefixClass+'-backdrop"/>');return o.$promise.then(function(t){angular.isObject(t)&&(t=t.data),a.html&&(t=t.replace(v,'ng-bind-html="')),t=m.apply(t),u=r(t),o.init()}),o.init=function(){a.show&&s.$$postDigest(function(){o.show()})},o.destroy=function(){f&&(f.remove(),f=null),y&&(y.remove(),y=null),s.$destroy()},o.show=function(){s.$emit(a.prefixEvent+".show.before",o);var t=a.container?p(a.container):null,e=a.container?null:a.element;f=o.$element=u(s,function(){}),f.css({display:"block"}).addClass(a.placement),a.animation&&(a.backdrop&&y.addClass(a.backdropAnimation),f.addClass(a.animation)),a.backdrop&&l.enter(y,_,null,function(){}),l.enter(f,t,e,function(){s.$emit(a.prefixEvent+".show",o)}),s.$isShown=!0,s.$$phase||s.$root.$$phase||s.$digest();var i=f[0];g(function(){i.focus()}),_.addClass(a.prefixClass+"-open"),a.animation&&_.addClass(a.prefixClass+"-with-"+a.animation),a.backdrop&&(f.on("click",n),y.on("click",n)),a.keyboard&&f.on("keyup",o.$onKeyUp)},o.hide=function(){s.$emit(a.prefixEvent+".hide.before",o),l.leave(f,function(){s.$emit(a.prefixEvent+".hide",o),_.removeClass(a.prefixClass+"-open"),a.animation&&_.addClass(a.prefixClass+"-with-"+a.animation)}),a.backdrop&&l.leave(y,function(){}),s.$isShown=!1,s.$$phase||s.$root.$$phase||s.$digest(),a.backdrop&&(f.off("click",n),y.off("click",n)),a.keyboard&&f.off("keyup",o.$onKeyUp)},o.toggle=function(){s.$isShown?o.hide():o.show()},o.focus=function(){f[0].focus()},o.$onKeyUp=function(t){27===t.which&&o.hide()},o}function p(t,n){return angular.element((n||e).querySelectorAll(t))}function h(t){return o.when(a.get(t)||s.get(t)).then(function(e){return angular.isObject(e)?(a.put(t,e.data),e.data):e})}var d=angular.forEach,m=String.prototype.trim,g=n.requestAnimationFrame||n.setTimeout,_=angular.element(n.document.body),v=/ng-bind="/gi;return f}]}).directive("bsModal",["$window","$location","$sce","$modal",function(t,e,n,i){return{restrict:"EAC",scope:!0,link:function(t,e,r){var o={scope:t,element:e,show:!1};angular.forEach(["template","contentTemplate","placement","backdrop","keyboard","html","container","animation"],function(t){angular.isDefined(r[t])&&(o[t]=r[t])}),angular.forEach(["title","content"],function(e){r[e]&&r.$observe(e,function(i){t[e]=n.trustAsHtml(i)})}),r.bsModal&&t.$watch(r.bsModal,function(e){angular.isObject(e)?angular.extend(t,e):t.content=e},!0);var a=i(o);e.on(r.trigger||"click",a.toggle),t.$on("$destroy",function(){a.destroy(),o=null,a=null})}}}]),angular.module("mgcrea.ngStrap.navbar",[]).provider("$navbar",function(){var t=this.defaults={activeClass:"active",routeAttr:"data-match-route",strict:!1};this.$get=function(){return{defaults:t}}}).directive("bsNavbar",["$window","$location","$navbar",function(t,e,n){var i=n.defaults;return{restrict:"A",link:function(t,n,r){var o=angular.copy(i);angular.forEach(Object.keys(i),function(t){angular.isDefined(r[t])&&(o[t]=r[t])}),t.$watch(function(){return e.path()},function(t){var e=n[0].querySelectorAll("li["+o.routeAttr+"]");angular.forEach(e,function(e){var n=angular.element(e),i=n.attr(o.routeAttr).replace("/","\\/");o.strict&&(i="^"+i+"$");var r=new RegExp(i,["i"]);r.test(t)?n.addClass(o.activeClass):n.removeClass(o.activeClass)})})}}}]),angular.module("mgcrea.ngStrap.select",["mgcrea.ngStrap.tooltip","mgcrea.ngStrap.helpers.parseOptions"]).provider("$select",function(){var t=this.defaults={animation:"am-fade",prefixClass:"select",placement:"bottom-left",template:"select/select.tpl.html",trigger:"focus",container:!1,keyboard:!0,html:!1,delay:0,multiple:!1,sort:!0,caretHtml:'&nbsp;<span class="caret"></span>',placeholder:"Choose among the following...",maxLength:3,maxLengthHtml:"selected"};this.$get=["$window","$document","$rootScope","$tooltip",function(e,n,i,r){function o(e,n,i){var o={},s=angular.extend({},t,i);o=r(e,s);var l=i.scope,u=o.$scope;u.$matches=[],u.$activeIndex=0,u.$isMultiple=s.multiple,u.$activate=function(t){u.$$postDigest(function(){o.activate(t)})},u.$select=function(t){u.$$postDigest(function(){o.select(t)})},u.$isVisible=function(){return o.$isVisible()},u.$isActive=function(t){return o.$isActive(t)},o.update=function(t){u.$matches=t,o.$updateActiveIndex()},o.activate=function(t){return s.multiple?(u.$activeIndex.sort(),o.$isActive(t)?u.$activeIndex.splice(u.$activeIndex.indexOf(t),1):u.$activeIndex.push(t),s.sort&&u.$activeIndex.sort()):u.$activeIndex=t,u.$activeIndex},o.select=function(t){var e=u.$matches[t].value;o.activate(t),n.$setViewValue(s.multiple?u.$activeIndex.map(function(t){return u.$matches[t].value}):e),n.$render(),l&&l.$digest(),s.multiple||o.hide(),u.$emit("$select.select",e,t)},o.$updateActiveIndex=function(){n.$modelValue&&u.$matches.length?u.$activeIndex=s.multiple&&angular.isArray(n.$modelValue)?n.$modelValue.map(function(t){return o.$getIndex(t)}):o.$getIndex(n.$modelValue):u.$activeIndex>=u.$matches.length&&(u.$activeIndex=s.multiple?[]:0)},o.$isVisible=function(){return s.minLength&&n?u.$matches.length&&n.$viewValue.length>=s.minLength:u.$matches.length},o.$isActive=function(t){return s.multiple?-1!==u.$activeIndex.indexOf(t):u.$activeIndex===t},o.$getIndex=function(t){var e=u.$matches.length,n=e;if(e){for(n=e;n--&&u.$matches[n].value!==t;);if(!(0>n))return n}},o.$onMouseDown=function(t){if(t.preventDefault(),t.stopPropagation(),a){var e=angular.element(t.target);
e.triggerHandler("click")}},o.$onKeyDown=function(t){if(/(9|13|38|40)/.test(t.keyCode)){if(t.preventDefault(),t.stopPropagation(),!s.multiple&&(13===t.keyCode||9===t.keyCode))return o.select(u.$activeIndex);38===t.keyCode&&u.$activeIndex>0?u.$activeIndex--:40===t.keyCode&&u.$activeIndex<u.$matches.length-1?u.$activeIndex++:angular.isUndefined(u.$activeIndex)&&(u.$activeIndex=0),u.$digest()}};var c=o.show;o.show=function(){c(),s.multiple&&o.$element.addClass("select-multiple"),setTimeout(function(){o.$element.on(a?"touchstart":"mousedown",o.$onMouseDown),s.keyboard&&e.on("keydown",o.$onKeyDown)})};var f=o.hide;return o.hide=function(){o.$element.off(a?"touchstart":"mousedown",o.$onMouseDown),s.keyboard&&e.off("keydown",o.$onKeyDown),f(!0)},o}var a=(angular.element(e.document.body),"createTouch"in e.document);return o.defaults=t,o}]}).directive("bsSelect",["$window","$parse","$q","$select","$parseOptions",function(t,e,n,i,r){var o=i.defaults;return{restrict:"EAC",require:"ngModel",link:function(t,e,n,a){var s={scope:t};if(angular.forEach(["placement","container","delay","trigger","keyboard","html","animation","template","placeholder","multiple","maxLength","maxLengthHtml"],function(t){angular.isDefined(n[t])&&(s[t]=n[t])}),"select"===e[0].nodeName.toLowerCase()){var l=e;l.css("display","none"),e=angular.element('<button type="button" class="btn btn-default"></button>'),l.after(e)}var u=r(n.ngOptions),c=i(e,a,s),f=u.$match[7].replace(/\|.+/,"").trim();t.$watch(f,function(){u.valuesFn(t,a).then(function(t){c.update(t),a.$render()})},!0),t.$watch(n.ngModel,function(){c.$updateActiveIndex()},!0),a.$render=function(){var t,i;s.multiple&&angular.isArray(a.$modelValue)?(t=a.$modelValue.map(function(t){return i=c.$getIndex(t),angular.isDefined(i)?c.$scope.$matches[i].label:!1}).filter(angular.isDefined),t=t.length>(s.maxLength||o.maxLength)?t.length+" "+(s.maxLengthHtml||o.maxLengthHtml):t.join(", ")):(i=c.$getIndex(a.$modelValue),t=angular.isDefined(i)?c.$scope.$matches[i].label:!1),e.html((t?t:n.placeholder||o.placeholder)+o.caretHtml)},t.$on("$destroy",function(){c.destroy(),s=null,c=null})}}}]),angular.module("mgcrea.ngStrap.popover",["mgcrea.ngStrap.tooltip"]).provider("$popover",function(){var t=this.defaults={animation:"am-fade",placement:"right",template:"popover/popover.tpl.html",contentTemplate:!1,trigger:"click",keyboard:!0,html:!1,title:"",content:"",delay:0,container:!1};this.$get=["$tooltip",function(e){function n(n,i){var r=angular.extend({},t,i),o=e(n,r);return r.content&&(o.$scope.content=r.content),o}return n}]}).directive("bsPopover",["$window","$location","$sce","$popover",function(t,e,n,i){var r=t.requestAnimationFrame||t.setTimeout;return{restrict:"EAC",scope:!0,link:function(t,e,o){var a={scope:t};angular.forEach(["template","contentTemplate","placement","container","delay","trigger","keyboard","html","animation"],function(t){angular.isDefined(o[t])&&(a[t]=o[t])}),angular.forEach(["title","content"],function(e){o[e]&&o.$observe(e,function(i,o){t[e]=n.trustAsHtml(i),angular.isDefined(o)&&r(function(){s&&s.$applyPlacement()})})}),o.bsPopover&&t.$watch(o.bsPopover,function(e,n){angular.isObject(e)?angular.extend(t,e):t.content=e,angular.isDefined(n)&&r(function(){s&&s.$applyPlacement()})},!0);var s=i(e,a);t.$on("$destroy",function(){s.destroy(),a=null,s=null})}}}]),angular.module("mgcrea.ngStrap.scrollspy",["mgcrea.ngStrap.helpers.debounce","mgcrea.ngStrap.helpers.dimensions"]).provider("$scrollspy",function(){var t=this.$$spies={},n=this.defaults={debounce:150,throttle:100,offset:100};this.$get=["$window","$document","$rootScope","dimensions","debounce","throttle",function(i,r,o,a,s,l){function u(t,e){return t[0].nodeName&&t[0].nodeName.toLowerCase()===e.toLowerCase()}function c(r){var c=angular.extend({},n,r);c.element||(c.element=h);var d=u(c.element,"body"),m=d?f:c.element,g=d?"window":c.id;if(t[g])return t[g].$$count++,t[g];var _,v,y,b,w,$,x,T,S={},C=S.$trackedElements=[],k=[];return S.init=function(){this.$$count=1,b=s(this.checkPosition,c.debounce),w=l(this.checkPosition,c.throttle),m.on("click",this.checkPositionWithEventLoop),f.on("resize",b),m.on("scroll",w),$=s(this.checkOffsets,c.debounce),_=o.$on("$viewContentLoaded",$),v=o.$on("$includeContentLoaded",$),$(),g&&(t[g]=S)},S.destroy=function(){this.$$count--,this.$$count>0||(m.off("click",this.checkPositionWithEventLoop),f.off("resize",b),m.off("scroll",b),_(),v(),g&&delete t[g])},S.checkPosition=function(){if(k.length){if(T=(d?i.pageYOffset:m.prop("scrollTop"))||0,x=Math.max(i.innerHeight,p.prop("clientHeight")),T<k[0].offsetTop&&y!==k[0].target)return S.$activateElement(k[0]);for(var t=k.length;t--;)if(!angular.isUndefined(k[t].offsetTop)&&null!==k[t].offsetTop&&y!==k[t].target&&!(T<k[t].offsetTop||k[t+1]&&T>k[t+1].offsetTop))return S.$activateElement(k[t])}},S.checkPositionWithEventLoop=function(){setTimeout(this.checkPosition,1)},S.$activateElement=function(t){if(y){var e=S.$getTrackedElement(y);e&&(e.source.removeClass("active"),u(e.source,"li")&&u(e.source.parent().parent(),"li")&&e.source.parent().parent().removeClass("active"))}y=t.target,t.source.addClass("active"),u(t.source,"li")&&u(t.source.parent().parent(),"li")&&t.source.parent().parent().addClass("active")},S.$getTrackedElement=function(t){return C.filter(function(e){return e.target===t})[0]},S.checkOffsets=function(){angular.forEach(C,function(t){var n=e.querySelector(t.target);t.offsetTop=n?a.offset(n).top:null,c.offset&&null!==t.offsetTop&&(t.offsetTop-=1*c.offset)}),k=C.filter(function(t){return null!==t.offsetTop}).sort(function(t,e){return t.offsetTop-e.offsetTop}),b()},S.trackElement=function(t,e){C.push({target:t,source:e})},S.untrackElement=function(t,e){for(var n,i=C.length;i--;)if(C[i].target===t&&C[i].source===e){n=i;break}C=C.splice(n,1)},S.activate=function(t){C[t].addClass("active")},S.init(),S}var f=angular.element(i),p=angular.element(r.prop("documentElement")),h=angular.element(i.document.body);return c}]}).directive("bsScrollspy",["$rootScope","debounce","dimensions","$scrollspy",function(t,e,n,i){return{restrict:"EAC",link:function(t,e,n){var r={scope:t};angular.forEach(["offset","target"],function(t){angular.isDefined(n[t])&&(r[t]=n[t])});var o=i(r);o.trackElement(r.target,e),t.$on("$destroy",function(){o.untrackElement(r.target,e),o.destroy(),r=null,o=null})}}}]).directive("bsScrollspyList",["$rootScope","debounce","dimensions","$scrollspy",function(){return{restrict:"A",compile:function(t){var e=t[0].querySelectorAll("li > a[href]");angular.forEach(e,function(t){var e=angular.element(t);e.parent().attr("bs-scrollspy","").attr("data-target",e.attr("href"))})}}}]),angular.module("mgcrea.ngStrap.tab",[]).run(["$templateCache",function(t){t.put("$pane","{{pane.content}}")}]).provider("$tab",function(){var t=this.defaults={animation:"am-fade",template:"tab/tab.tpl.html"};this.$get=function(){return{defaults:t}}}).directive("bsTabs",["$window","$animate","$tab",function(t,e,n){var i=n.defaults;return{restrict:"EAC",scope:!0,require:"?ngModel",templateUrl:function(t,e){return e.template||i.template},link:function(t,e,n,r){var o=i;angular.forEach(["animation"],function(t){angular.isDefined(n[t])&&(o[t]=n[t])}),n.bsTabs&&t.$watch(n.bsTabs,function(e){t.panes=e},!0),e.addClass("tabs"),o.animation&&e.addClass(o.animation),t.active=t.activePane=0,t.setActive=function(e){t.active=e,r&&r.$setViewValue(e)},r&&(r.$render=function(){t.active=1*r.$modelValue})}}}]),angular.module("mgcrea.ngStrap.timepicker",["mgcrea.ngStrap.helpers.dateParser","mgcrea.ngStrap.tooltip"]).provider("$timepicker",function(){var t=this.defaults={animation:"am-fade",prefixClass:"timepicker",placement:"bottom-left",template:"timepicker/timepicker.tpl.html",trigger:"focus",container:!1,keyboard:!0,html:!1,delay:0,useNative:!0,timeType:"date",timeFormat:"shortTime",autoclose:!1,minTime:-1/0,maxTime:+1/0,length:5,hourStep:1,minuteStep:5};this.$get=["$window","$document","$rootScope","$sce","$locale","dateFilter","$tooltip",function(e,n,i,r,o,a,s){function l(e,n,i){function r(t,n){if(e[0].createTextRange){var i=e[0].createTextRange();i.collapse(!0),i.moveStart("character",t),i.moveEnd("character",n),i.select()}else e[0].setSelectionRange?e[0].setSelectionRange(t,n):angular.isUndefined(e[0].selectionStart)&&(e[0].selectionStart=t,e[0].selectionEnd=n)}function l(){e[0].focus()}var f=s(e,angular.extend({},t,i)),p=i.scope,h=f.$options,d=f.$scope,m=0,g=n.$dateValue||new Date,_={hour:g.getHours(),meridian:g.getHours()<12,minute:g.getMinutes(),second:g.getSeconds(),millisecond:g.getMilliseconds()},v=o.DATETIME_FORMATS[h.timeFormat]||h.timeFormat,y=/(h+)[:]?(m+)[ ]?(a?)/i.exec(v).slice(1);d.$select=function(t,e){f.select(t,e)},d.$moveIndex=function(t,e){f.$moveIndex(t,e)},d.$switchMeridian=function(t){f.switchMeridian(t)},f.update=function(t){angular.isDate(t)&&!isNaN(t.getTime())?(f.$date=t,angular.extend(_,{hour:t.getHours(),minute:t.getMinutes(),second:t.getSeconds(),millisecond:t.getMilliseconds()}),f.$build()):f.$isBuilt||f.$build()},f.select=function(t,e,i){(!n.$dateValue||isNaN(n.$dateValue.getTime()))&&(n.$dateValue=new Date(1970,0,1)),angular.isDate(t)||(t=new Date(t)),0===e?n.$dateValue.setHours(t.getHours()):1===e&&n.$dateValue.setMinutes(t.getMinutes()),n.$setViewValue(n.$dateValue),n.$render(),h.autoclose&&!i&&f.hide(!0)},f.switchMeridian=function(t){var e=(t||n.$dateValue).getHours();n.$dateValue.setHours(12>e?e+12:e-12),n.$setViewValue(n.$dateValue),n.$render()},f.$build=function(){var t,e,n=d.midIndex=parseInt(h.length/2,10),i=[];for(t=0;t<h.length;t++)e=new Date(1970,0,1,_.hour-(n-t)*h.hourStep),i.push({date:e,label:a(e,y[0]),selected:f.$date&&f.$isSelected(e,0),disabled:f.$isDisabled(e,0)});var r,o=[];for(t=0;t<h.length;t++)r=new Date(1970,0,1,0,_.minute-(n-t)*h.minuteStep),o.push({date:r,label:a(r,y[1]),selected:f.$date&&f.$isSelected(r,1),disabled:f.$isDisabled(r,1)});var s=[];for(t=0;t<h.length;t++)s.push([i[t],o[t]]);d.rows=s,d.showAM=!!y[2],d.isAM=(f.$date||i[n].date).getHours()<12,f.$isBuilt=!0},f.$isSelected=function(t,e){return f.$date?0===e?t.getHours()===f.$date.getHours():1===e?t.getMinutes()===f.$date.getMinutes():void 0:!1},f.$isDisabled=function(t,e){var n;return 0===e?n=t.getTime()+6e4*_.minute:1===e&&(n=t.getTime()+36e5*_.hour),n<1*h.minTime||n>1*h.maxTime},f.$moveIndex=function(t,e){var n;0===e?(n=new Date(1970,0,1,_.hour+t*h.length,_.minute),angular.extend(_,{hour:n.getHours()})):1===e&&(n=new Date(1970,0,1,_.hour,_.minute+t*h.length*h.minuteStep),angular.extend(_,{minute:n.getMinutes()})),f.$build()},f.$onMouseDown=function(t){if("input"!==t.target.nodeName.toLowerCase()&&t.preventDefault(),t.stopPropagation(),u){var e=angular.element(t.target);"button"!==e[0].nodeName.toLowerCase()&&(e=e.parent()),e.triggerHandler("click")}},f.$onKeyDown=function(t){if(/(38|37|39|40|13)/.test(t.keyCode)&&!t.shiftKey&&!t.altKey){if(t.preventDefault(),t.stopPropagation(),13===t.keyCode)return f.hide(!0);var e=new Date(f.$date),n=e.getHours(),i=a(e,"h").length,o=e.getMinutes(),s=a(e,"mm").length,l=/(37|39)/.test(t.keyCode),u=2+1*!!y[2];l&&(37===t.keyCode?m=1>m?u-1:m-1:39===t.keyCode&&(m=u-1>m?m+1:0));var c=[0,i];0===m?(38===t.keyCode?e.setHours(n-parseInt(h.hourStep,10)):40===t.keyCode&&e.setHours(n+parseInt(h.hourStep,10)),c=[0,i]):1===m?(38===t.keyCode?e.setMinutes(o-parseInt(h.minuteStep,10)):40===t.keyCode&&e.setMinutes(o+parseInt(h.minuteStep,10)),c=[i+1,i+1+s]):2===m&&(l||f.switchMeridian(),c=[i+1+s+1,i+1+s+3]),f.select(e,m,!0),r(c[0],c[1]),p.$digest()}};var b=f.init;f.init=function(){return c&&h.useNative?(e.prop("type","time"),void e.css("-webkit-appearance","textfield")):(u&&(e.prop("type","text"),e.attr("readonly","true"),e.on("click",l)),void b())};var w=f.destroy;f.destroy=function(){c&&h.useNative&&e.off("click",l),w()};var $=f.show;f.show=function(){$(),setTimeout(function(){f.$element.on(u?"touchstart":"mousedown",f.$onMouseDown),h.keyboard&&e.on("keydown",f.$onKeyDown)})};var x=f.hide;return f.hide=function(t){f.$element.off(u?"touchstart":"mousedown",f.$onMouseDown),h.keyboard&&e.off("keydown",f.$onKeyDown),x(t)},f}var u=(angular.element(e.document.body),"createTouch"in e.document),c=/(ip(a|o)d|iphone|android)/gi.test(e.navigator.userAgent);return t.lang||(t.lang=o.id),l.defaults=t,l}]}).directive("bsTimepicker",["$window","$parse","$q","$locale","dateFilter","$timepicker","$dateParser","$timeout",function(t,e,n,i,r,o,a){{var s=o.defaults,l=/(ip(a|o)d|iphone|android)/gi.test(t.navigator.userAgent);t.requestAnimationFrame||t.setTimeout}return{restrict:"EAC",require:"ngModel",link:function(t,e,n,i){var u={scope:t,controller:i};angular.forEach(["placement","container","delay","trigger","keyboard","html","animation","template","autoclose","timeType","timeFormat","useNative","hourStep","minuteStep","length"],function(t){angular.isDefined(n[t])&&(u[t]=n[t])}),l&&(u.useNative||s.useNative)&&(u.timeFormat="HH:mm");var c=o(e,i,u);u=c.$options;var f=a({format:u.timeFormat,lang:u.lang});angular.forEach(["minTime","maxTime"],function(t){angular.isDefined(n[t])&&n.$observe(t,function(e){c.$options[t]="now"===e?(new Date).setFullYear(1970,0,1):angular.isString(e)&&e.match(/^".+"$/)?+new Date(e.substr(1,e.length-2)):f.parse(e,new Date(1970,0,1,0)),!isNaN(c.$options[t])&&c.$build()})}),t.$watch(n.ngModel,function(){c.update(i.$dateValue)},!0),i.$parsers.unshift(function(t){if(!t)return void i.$setValidity("date",!0);var e=f.parse(t,i.$dateValue);if(!e||isNaN(e.getTime()))i.$setValidity("date",!1);else{var n=e.getTime()>=u.minTime&&e.getTime()<=u.maxTime;i.$setValidity("date",n),n&&(i.$dateValue=e)}return"string"===u.timeType?r(t,u.timeFormat):"number"===u.timeType?i.$dateValue.getTime():"iso"===u.timeType?i.$dateValue.toISOString():new Date(i.$dateValue)}),i.$formatters.push(function(t){var e;return e=angular.isUndefined(t)||null===t?0/0:angular.isDate(t)?t:"string"===u.timeType?f.parse(t):new Date(t),i.$dateValue=e,i.$dateValue}),i.$render=function(){e.val(!i.$dateValue||isNaN(i.$dateValue.getTime())?"":r(i.$dateValue,u.timeFormat))},t.$on("$destroy",function(){c.destroy(),u=null,c=null})}}}]),angular.module("mgcrea.ngStrap.tooltip",["mgcrea.ngStrap.helpers.dimensions"]).provider("$tooltip",function(){var t=this.defaults={animation:"am-fade",prefixClass:"tooltip",prefixEvent:"tooltip",container:!1,placement:"top",template:"tooltip/tooltip.tpl.html",contentTemplate:!1,trigger:"hover focus",keyboard:!1,html:!1,show:!1,title:"",type:"",delay:0};this.$get=["$window","$rootScope","$compile","$q","$templateCache","$http","$animate","$timeout","dimensions","$$rAF",function(n,i,r,o,a,s,l,u,c,f){function p(e,n){function o(){return"body"===p.container?c.offset(e[0]):c.position(e[0])}function a(t,e,n,i){var r,o=t.split("-");switch(o[0]){case"right":r={top:e.top+e.height/2-i/2,left:e.left+e.width};break;case"bottom":r={top:e.top+e.height,left:e.left+e.width/2-n/2};break;case"left":r={top:e.top+e.height/2-i/2,left:e.left-n};break;default:r={top:e.top-i,left:e.left+e.width/2-n/2}}if(!o[1])return r;if("top"===o[0]||"bottom"===o[0])switch(o[1]){case"left":r.left=e.left;break;case"right":r.left=e.left+e.width-n}else if("left"===o[0]||"right"===o[0])switch(o[1]){case"top":r.top=e.top-i;break;case"bottom":r.top=e.top+e.height}return r}var s={},u=e[0].nodeName.toLowerCase(),p=s.$options=angular.extend({},t,n);s.$promise=d(p.template);var v=s.$scope=p.scope&&p.scope.$new()||i.$new();p.delay&&angular.isString(p.delay)&&(p.delay=parseFloat(p.delay)),p.title&&(s.$scope.title=p.title),v.$hide=function(){v.$$postDigest(function(){s.hide()})},v.$show=function(){v.$$postDigest(function(){s.show()})},v.$toggle=function(){v.$$postDigest(function(){s.toggle()})},s.$isShown=v.$isShown=!1;var y,b;p.contentTemplate&&(s.$promise=s.$promise.then(function(t){var e=angular.element(t);return d(p.contentTemplate).then(function(t){var n=h('[ng-bind="content"]',e[0]);return n.length||(n=h('[ng-bind="title"]',e[0])),n.removeAttr("ng-bind").html(t),e[0].outerHTML})}));var w,$,x,T;return s.$promise.then(function(t){angular.isObject(t)&&(t=t.data),p.html&&(t=t.replace(_,'ng-bind-html="')),t=m.apply(t),x=t,w=r(t),s.init()}),s.init=function(){p.delay&&angular.isNumber(p.delay)&&(p.delay={show:p.delay,hide:p.delay}),"self"===p.container?T=e:p.container&&(T=h(p.container));var t=p.trigger.split(" ");angular.forEach(t,function(t){"click"===t?e.on("click",s.toggle):"manual"!==t&&(e.on("hover"===t?"mouseenter":"focus",s.enter),e.on("hover"===t?"mouseleave":"blur",s.leave),"button"===u&&"hover"!==t&&e.on(g?"touchstart":"mousedown",s.$onFocusElementMouseDown))}),p.show&&v.$$postDigest(function(){"focus"===p.trigger?e[0].focus():s.show()})},s.destroy=function(){for(var t=p.trigger.split(" "),n=t.length;n--;){var i=t[n];"click"===i?e.off("click",s.toggle):"manual"!==i&&(e.off("hover"===i?"mouseenter":"focus",s.enter),e.off("hover"===i?"mouseleave":"blur",s.leave),"button"===u&&"hover"!==i&&e.off(g?"touchstart":"mousedown",s.$onFocusElementMouseDown))}$&&($.remove(),$=null),v.$destroy()},s.enter=function(){return clearTimeout(y),b="in",p.delay&&p.delay.show?void(y=setTimeout(function(){"in"===b&&s.show()},p.delay.show)):s.show()},s.show=function(){v.$emit(p.prefixEvent+".show.before",s);var t=p.container?T:null,n=p.container?null:e;$&&$.remove(),$=s.$element=w(v,function(){}),$.css({top:"0px",left:"0px",display:"block"}).addClass(p.placement),p.animation&&$.addClass(p.animation),p.type&&$.addClass(p.prefixClass+"-"+p.type),l.enter($,t,n,function(){v.$emit(p.prefixEvent+".show",s)}),s.$isShown=v.$isShown=!0,v.$$phase||v.$root.$$phase||v.$digest(),f(s.$applyPlacement),p.keyboard&&("focus"!==p.trigger?(s.focus(),$.on("keyup",s.$onKeyUp)):e.on("keyup",s.$onFocusKeyUp))},s.leave=function(){return clearTimeout(y),b="out",p.delay&&p.delay.hide?void(y=setTimeout(function(){"out"===b&&s.hide()},p.delay.hide)):s.hide()},s.hide=function(t){return s.$isShown?(v.$emit(p.prefixEvent+".hide.before",s),l.leave($,function(){v.$emit(p.prefixEvent+".hide",s)}),s.$isShown=v.$isShown=!1,v.$$phase||v.$root.$$phase||v.$digest(),p.keyboard&&null!==$&&$.off("keyup",s.$onKeyUp),t&&"focus"===p.trigger?e[0].blur():void 0):void 0},s.toggle=function(){s.$isShown?s.leave():s.enter()},s.focus=function(){$[0].focus()},s.$applyPlacement=function(){if($){var t=o(),e=$.prop("offsetWidth"),n=$.prop("offsetHeight"),i=a(p.placement,t,e,n);i.top+="px",i.left+="px",$.css(i)}},s.$onKeyUp=function(t){27===t.which&&s.hide()},s.$onFocusKeyUp=function(t){27===t.which&&e[0].blur()},s.$onFocusElementMouseDown=function(t){t.preventDefault(),t.stopPropagation(),s.$isShown?e[0].blur():e[0].focus()},s}function h(t,n){return angular.element((n||e).querySelectorAll(t))}function d(t){return o.when(a.get(t)||s.get(t)).then(function(e){return angular.isObject(e)?(a.put(t,e.data),e.data):e})}var m=String.prototype.trim,g="createTouch"in n.document,_=/ng-bind="/gi;return p}]}).directive("bsTooltip",["$window","$location","$sce","$tooltip","$$rAF",function(t,e,n,i,r){return{restrict:"EAC",scope:!0,link:function(t,e,o){var a={scope:t};angular.forEach(["template","contentTemplate","placement","container","delay","trigger","keyboard","html","animation","type"],function(t){angular.isDefined(o[t])&&(a[t]=o[t])}),angular.forEach(["title"],function(e){o[e]&&o.$observe(e,function(i,o){t[e]=n.trustAsHtml(i),angular.isDefined(o)&&r(function(){s&&s.$applyPlacement()})})}),o.bsTooltip&&t.$watch(o.bsTooltip,function(e,n){angular.isObject(e)?angular.extend(t,e):t.title=e,angular.isDefined(n)&&r(function(){s&&s.$applyPlacement()})},!0);var s=i(e,a);t.$on("$destroy",function(){s.destroy(),a=null,s=null})}}}]),angular.module("mgcrea.ngStrap.typeahead",["mgcrea.ngStrap.tooltip","mgcrea.ngStrap.helpers.parseOptions"]).provider("$typeahead",function(){var t=this.defaults={animation:"am-fade",prefixClass:"typeahead",placement:"bottom-left",template:"typeahead/typeahead.tpl.html",trigger:"focus",container:!1,keyboard:!0,html:!1,delay:0,minLength:1,filter:"filter",limit:6};this.$get=["$window","$rootScope","$tooltip",function(e,n,i){function r(e,n,r){var o={},a=angular.extend({},t,r);o=i(e,a);var s=r.scope,l=o.$scope;l.$resetMatches=function(){l.$matches=[],l.$activeIndex=0},l.$resetMatches(),l.$activate=function(t){l.$$postDigest(function(){o.activate(t)})},l.$select=function(t){l.$$postDigest(function(){o.select(t)})},l.$isVisible=function(){return o.$isVisible()},o.update=function(t){l.$matches=t,l.$activeIndex>=t.length&&(l.$activeIndex=0)},o.activate=function(t){l.$activeIndex=t},o.select=function(t){var e=l.$matches[t].value;n.$setViewValue(e),l.$resetMatches(),n.$render(),s&&s.$digest(),l.$emit("$typeahead.select",e,t)},o.$isVisible=function(){return a.minLength&&n?l.$matches.length&&angular.isString(n.$viewValue)&&n.$viewValue.length>=a.minLength:!!l.$matches.length},o.$getIndex=function(t){var e=l.$matches.length,n=e;if(e){for(n=e;n--&&l.$matches[n].value!==t;);if(!(0>n))return n}},o.$onMouseDown=function(t){t.preventDefault(),t.stopPropagation()},o.$onKeyDown=function(t){/(38|40|13)/.test(t.keyCode)&&(t.preventDefault(),t.stopPropagation(),13===t.keyCode&&l.$matches.length?o.select(l.$activeIndex):38===t.keyCode&&l.$activeIndex>0?l.$activeIndex--:40===t.keyCode&&l.$activeIndex<l.$matches.length-1?l.$activeIndex++:angular.isUndefined(l.$activeIndex)&&(l.$activeIndex=0),l.$digest())};var u=o.show;o.show=function(){u(),setTimeout(function(){o.$element.on("mousedown",o.$onMouseDown),a.keyboard&&e.on("keydown",o.$onKeyDown)})};var c=o.hide;return o.hide=function(){o.$element.off("mousedown",o.$onMouseDown),a.keyboard&&e.off("keydown",o.$onKeyDown),c()},o}angular.element(e.document.body);return r.defaults=t,r}]}).directive("bsTypeahead",["$window","$parse","$q","$typeahead","$parseOptions",function(t,e,n,i,r){var o=i.defaults;return{restrict:"EAC",require:"ngModel",link:function(t,e,n,a){var s={scope:t};angular.forEach(["placement","container","delay","trigger","keyboard","html","animation","template","filter","limit","minLength"],function(t){angular.isDefined(n[t])&&(s[t]=n[t])});var l=s.filter||o.filter,u=s.limit||o.limit,c=n.ngOptions;l&&(c+=" | "+l+":$viewValue"),u&&(c+=" | limitTo:"+u);var f=r(c),p=i(e,a,s);t.$watch(n.ngModel,function(e){t.$modelValue=e,f.valuesFn(t,a).then(function(t){t.length>u&&(t=t.slice(0,u)),(1!==t.length||t[0].value!==e)&&(p.update(t),a.$render())})}),a.$render=function(){if(a.$isEmpty(a.$viewValue))return e.val("");var t=p.$getIndex(a.$modelValue),n=angular.isDefined(t)?p.$scope.$matches[t].label:a.$viewValue;n=angular.isObject(n)?n.label:n,e.val(n.replace(/<(?:.|\n)*?>/gm,"").trim())},t.$on("$destroy",function(){p.destroy(),s=null,p=null})}}}])}(window,document),function(){"use strict";angular.module("mgcrea.ngStrap.alert").run(["$templateCache",function(t){t.put("alert/alert.tpl.html",'<div class="alert alert-dismissable" tabindex="-1" ng-class="[type ? \'alert-\' + type : null]"><button type="button" class="close" ng-click="$hide()">&times;</button> <strong ng-bind="title"></strong>&nbsp;<span ng-bind-html="content"></span></div>')}]),angular.module("mgcrea.ngStrap.aside").run(["$templateCache",function(t){t.put("aside/aside.tpl.html",'<div class="aside" tabindex="-1" role="dialog"><div class="aside-dialog"><div class="aside-content"><div class="aside-header" ng-show="title"><button type="button" class="close" ng-click="$hide()">&times;</button><h4 class="aside-title" ng-bind="title"></h4></div><div class="aside-body" ng-bind="content"></div><div class="aside-footer"><button type="button" class="btn btn-default" ng-click="$hide()">Close</button></div></div></div></div>')}]),angular.module("mgcrea.ngStrap.datepicker").run(["$templateCache",function(t){t.put("datepicker/datepicker.tpl.html",'<div class="dropdown-menu datepicker" ng-class="\'datepicker-mode-\' + $mode" style="max-width: 320px"><table style="table-layout: fixed; height: 100%; width: 100%"><thead><tr class="text-center"><th><button tabindex="-1" type="button" class="btn btn-default pull-left" ng-click="$selectPane(-1)"><i class="glyphicon glyphicon-chevron-left"></i></button></th><th colspan="{{ rows[0].length - 2 }}"><button tabindex="-1" type="button" class="btn btn-default btn-block text-strong" ng-click="$toggleMode()"><strong style="text-transform: capitalize" ng-bind="title"></strong></button></th><th><button tabindex="-1" type="button" class="btn btn-default pull-right" ng-click="$selectPane(+1)"><i class="glyphicon glyphicon-chevron-right"></i></button></th></tr><tr ng-show="showLabels" ng-bind-html="labels"></tr></thead><tbody><tr ng-repeat="(i, row) in rows" height="{{ 100 / rows.length }}%"><td class="text-center" ng-repeat="(j, el) in row"><button tabindex="-1" type="button" class="btn btn-default" style="width: 100%" ng-class="{\'btn-primary\': el.selected}" ng-click="$select(el.date)" ng-disabled="el.disabled"><span ng-class="{\'text-muted\': el.muted}" ng-bind="el.label"></span></button></td></tr></tbody></table></div>')}]),angular.module("mgcrea.ngStrap.dropdown").run(["$templateCache",function(t){t.put("dropdown/dropdown.tpl.html",'<ul tabindex="-1" class="dropdown-menu" role="menu"><li role="presentation" ng-class="{divider: item.divider}" ng-repeat="item in content"><a role="menuitem" tabindex="-1" ng-href="{{item.href}}" ng-if="!item.divider && item.href" ng-bind="item.text"></a> <a role="menuitem" tabindex="-1" href="javascript:void(0)" ng-if="!item.divider && item.click" ng-click="$eval(item.click);$hide()" ng-bind="item.text"></a></li></ul>')}]),angular.module("mgcrea.ngStrap.modal").run(["$templateCache",function(t){t.put("modal/modal.tpl.html",'<div class="modal" tabindex="-1" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-header" ng-show="title"><button type="button" class="close" ng-click="$hide()">&times;</button><h4 class="modal-title" ng-bind="title"></h4></div><div class="modal-body" ng-bind="content"></div><div class="modal-footer"><button type="button" class="btn btn-default" ng-click="$hide()">Close</button></div></div></div></div>')}]),angular.module("mgcrea.ngStrap.popover").run(["$templateCache",function(t){t.put("popover/popover.tpl.html",'<div class="popover"><div class="arrow"></div><h3 class="popover-title" ng-bind="title" ng-show="title"></h3><div class="popover-content" ng-bind="content"></div></div>')}]),angular.module("mgcrea.ngStrap.select").run(["$templateCache",function(t){t.put("select/select.tpl.html",'<ul tabindex="-1" class="select dropdown-menu" ng-show="$isVisible()" role="select"><li role="presentation" ng-repeat="match in $matches" ng-class="{active: $isActive($index)}"><a style="cursor: default" role="menuitem" tabindex="-1" ng-click="$select($index, $event)"><span ng-bind="match.label"></span> <i class="glyphicon glyphicon-ok pull-right" ng-if="$isMultiple && $isActive($index)"></i></a></li></ul>')}]),angular.module("mgcrea.ngStrap.tab").run(["$templateCache",function(t){t.put("tab/tab.tpl.html",'<ul class="nav nav-tabs"><li ng-repeat="pane in panes" ng-class="{active: $index == active}"><a data-toggle="tab" ng-click="setActive($index, $event)" data-index="{{$index}}" ng-bind-html="pane.title"></a></li></ul><div class="tab-content"><div ng-repeat="pane in panes" class="tab-pane" ng-class="[$index == active ? \'active\' : \'\']" ng-include="pane.template || \'$pane\'"></div></div>')}]),angular.module("mgcrea.ngStrap.timepicker").run(["$templateCache",function(t){t.put("timepicker/timepicker.tpl.html",'<div class="dropdown-menu timepicker" style="min-width: 0px;width: auto"><table height="100%"><thead><tr class="text-center"><th><button tabindex="-1" type="button" class="btn btn-default pull-left" ng-click="$moveIndex(-1, 0)"><i class="glyphicon glyphicon-chevron-up"></i></button></th><th>&nbsp;</th><th><button tabindex="-1" type="button" class="btn btn-default pull-left" ng-click="$moveIndex(-1, 1)"><i class="glyphicon glyphicon-chevron-up"></i></button></th></tr></thead><tbody><tr ng-repeat="(i, row) in rows"><td class="text-center"><button tabindex="-1" style="width: 100%" type="button" class="btn btn-default" ng-class="{\'btn-primary\': row[0].selected}" ng-click="$select(row[0].date, 0)" ng-disabled="row[0].disabled"><span ng-class="{\'text-muted\': row[0].muted}" ng-bind="row[0].label"></span></button></td><td><span ng-bind="i == midIndex ? \':\' : \' \'"></span></td><td class="text-center"><button tabindex="-1" ng-if="row[1].date" style="width: 100%" type="button" class="btn btn-default" ng-class="{\'btn-primary\': row[1].selected}" ng-click="$select(row[1].date, 1)" ng-disabled="row[1].disabled"><span ng-class="{\'text-muted\': row[1].muted}" ng-bind="row[1].label"></span></button></td><td ng-if="showAM">&nbsp;</td><td ng-if="showAM"><button tabindex="-1" ng-show="i == midIndex - !isAM * 1" style="width: 100%" type="button" ng-class="{\'btn-primary\': !!isAM}" class="btn btn-default" ng-click="$switchMeridian()" ng-disabled="el.disabled">AM</button> <button tabindex="-1" ng-show="i == midIndex + 1 - !isAM * 1" style="width: 100%" type="button" ng-class="{\'btn-primary\': !isAM}" class="btn btn-default" ng-click="$switchMeridian()" ng-disabled="el.disabled">PM</button></td></tr></tbody><tfoot><tr class="text-center"><th><button tabindex="-1" type="button" class="btn btn-default pull-left" ng-click="$moveIndex(1, 0)"><i class="glyphicon glyphicon-chevron-down"></i></button></th><th>&nbsp;</th><th><button tabindex="-1" type="button" class="btn btn-default pull-left" ng-click="$moveIndex(1, 1)"><i class="glyphicon glyphicon-chevron-down"></i></button></th></tr></tfoot></table></div>')}]),angular.module("mgcrea.ngStrap.tooltip").run(["$templateCache",function(t){t.put("tooltip/tooltip.tpl.html",'<div class="tooltip in" ng-show="title"><div class="tooltip-arrow"></div><div class="tooltip-inner" ng-bind="title"></div></div>')}]),angular.module("mgcrea.ngStrap.typeahead").run(["$templateCache",function(t){t.put("typeahead/typeahead.tpl.html",'<ul tabindex="-1" class="typeahead dropdown-menu" ng-show="$isVisible()" role="select"><li role="presentation" ng-repeat="match in $matches" ng-class="{active: $index == $activeIndex}"><a role="menuitem" tabindex="-1" ng-click="$select($index, $event)" ng-bind="match.label"></a></li></ul>')}])}(window,document);
>>>>>>> f5ade02af64300ccc5365af2845eec21d7cb8700
